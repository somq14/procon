#include <algorithm>
#include <cmath>
#include <complex>
#include <functional>
#include <iomanip>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <string>
#include <utility>
#include <vector>

using namespace std;

#define int long long int

const int INF = 1e15 + 373;

#define rep(i, s, t) for (int i = s; i < (int)(t); ++i)
#define all(a) (a).begin(), (a).end()

template <typename T>
vector<T> initVector(size_t n0, T e = T()) {
    return vector<T>(n0, e);
}

template <typename T>
using vector2 = vector<vector<T>>;

template <typename T>
vector2<T> initVector2(size_t n0, size_t n1, T e = T()) {
    return vector2<T>(n0, vector<T>(n1, e));
}

template <typename T>
using vector3 = vector<vector<vector<T>>>;

template <typename T>
vector3<T> initVector3(size_t n0, size_t n1, size_t n2, T e = T()) {
    return vector3<T>(n0, vector2<T>(n1, vector<T>(n2, e)));
}

int readInt() {
    int ret;
    cin >> ret;
    return ret;
}

vector<int> readInt(int n) {
    vector<int> ret(n);
    rep(i, 0, n) { cin >> ret[i]; }
    return ret;
}

string readString() {
    string ret;
    cin >> ret;
    return ret;
}

vector<string> readString(int n) {
    vector<string> ret(n);
    rep(i, 0, n) { cin >> ret[i]; }
    return ret;
}

template <typename Iterator>
ostream& join(ostream& os, Iterator first, Iterator last,
              const string& delim = " ") {
    auto it = first;
    while (it != last) {
        os << *it;
        it++;
        if (it != last) {
            os << delim;
        }
    }
    return os;
}

#ifdef ENABLE_DEBUG
#include "debug.h"
#else
#define debug(v) ;
#endif

void main_();

signed main() {
    cin.tie(0);
    ios::sync_with_stdio(false);
    main_();
    return 0;
}

int evalL(const string& s) {
    int stack = 0;
    for (int i = 0; i < (int)s.length(); i++) {
        if (s[i] == '(') {
            stack++;
        } else {
            stack--;
        }
        if (stack < 0) {
            return -1;
        }
    }
    return stack;
}

int evalR(const string& s) {
    int stack = 0;
    for (int i = (int)s.length() - 1; i >= 0; i--) {
        if (s[i] == ')') {
            stack++;
        } else {
            stack--;
        }
        if (stack < 0) {
            return -1;
        }
    }
    return stack;
}

void main_() {
    int n = readInt();
    auto s = readString(n);

    map<int, int> tab;
    for (int i = 0; i < n; i++) {
        int ll = evalL(s[i]);
        int rr = evalR(s[i]);
        debug(s[i]);
        debug(ll);
        debug(rr);
        if (ll == -1 && rr == -1) {
            continue;
        } else if (ll == 0 || rr == 0) {
            if (tab.find(0) == tab.end()) {
                tab[0] = 0;
            }
            tab[0]++;
        } else if (ll > 0) {
            if (tab.find(ll) == tab.end()) {
                tab[ll] = 0;
            }
            tab[ll]++;
        } else if (rr > 0) {
            if (tab.find(-rr) == tab.end()) {
                tab[-rr] = 0;
            }
            tab[-rr]++;
        } else {
            debug("err");
        }
    }

    int ans = 0;
    if (tab.find(0) != tab.end()) {
        ans += tab[0] / 2;
    }
    for (int i = 1; i <= (int)5e5; i++) {
        if (tab.find(i) != tab.end() && tab.find(-i) != tab.end()) {
            ans += min(tab[i], tab[-i]);
        }
    }
    cout << ans << endl;
}

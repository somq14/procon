#include <algorithm>
#include <cmath>
#include <complex>
#include <functional>
#include <iomanip>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <string>
#include <utility>
#include <vector>

using namespace std;

#define int long long int

const int INF = 1e15 + 373;

#define rep(i, s, t) for (int i = s; i < (int)(t); ++i)
#define all(a) (a).begin(), (a).end()

template <typename T>
vector<T> initVector(size_t n0, T e = T()) {
    return vector<T>(n0, e);
}

template <typename T>
using vector2 = vector<vector<T>>;

template <typename T>
vector2<T> initVector2(size_t n0, size_t n1, T e = T()) {
    return vector2<T>(n0, vector<T>(n1, e));
}

template <typename T>
using vector3 = vector<vector<vector<T>>>;

template <typename T>
vector3<T> initVector3(size_t n0, size_t n1, size_t n2, T e = T()) {
    return vector3<T>(n0, vector2<T>(n1, vector<T>(n2, e)));
}

int readInt() {
    int ret;
    cin >> ret;
    return ret;
}

vector<int> readInt(int n) {
    vector<int> ret(n);
    rep(i, 0, n) { cin >> ret[i]; }
    return ret;
}

string readString() {
    string ret;
    cin >> ret;
    return ret;
}

vector<string> readString(int n) {
    vector<string> ret(n);
    rep(i, 0, n) { cin >> ret[i]; }
    return ret;
}

template <typename Iterator>
ostream& join(ostream& os, Iterator first, Iterator last,
              const string& delim = " ") {
    auto it = first;
    while (it != last) {
        os << *it;
        it++;
        if (it != last) {
            os << delim;
        }
    }
    return os;
}

#ifdef ENABLE_DEBUG
#include "debug.h"
#else
#define debug(v) ;
#endif

void main_();

signed main() {
    cin.tie(0);
    ios::sync_with_stdio(false);
    main_();
    return 0;
}

// NOT VERIFIED

const int MOD = (int)(1e9 + 7);

class mint {
   private:
    int x;

    static int safe_mod(int x) { return (x % MOD + MOD) % MOD; }

   public:
    mint() : x(0) {}
    mint(int x) : x(safe_mod(x)) {}

    int get() const { return x; }
    void set(int x) { this->x = safe_mod(x); }

    mint add(mint x) const { return (this->x + x.x) % MOD; }
    mint sub(mint x) const { return (this->x - x.x + MOD) % MOD; }
    mint mul(mint x) const { return this->x * x.x % MOD; }
    mint div(mint x) const { return this->x * x.inv().x % MOD; }
    mint inv() const { return pow(MOD - 2); }

    mint pow(int y) const {
        if (y < 0) {
            return pow(-y).inv();
        }

        int res = 1;
        int xx = x;
        for (int yy = y; yy > 0; yy >>= 1) {
            if (yy & 1) {
                res = res * xx % MOD;
            }
            xx = xx * xx % MOD;
        }
        return res;
    }

    mint operator+() const { return this->x; }
    mint operator-() const { return -this->x; }
    mint operator+(mint x) const { return this->add(x); }
    mint operator-(mint x) const { return this->sub(x); }
    mint operator*(mint x) const { return this->mul(x); }
    mint operator/(mint x) const { return this->div(x); }
    mint operator+=(mint x) { return *this = this->add(x); }
    mint operator-=(mint x) { return *this = this->sub(x); }
    mint operator*=(mint x) { return *this = this->mul(x); }
    mint operator/=(mint x) { return *this = this->div(x); }

    friend istream& operator>>(istream& is, mint& x) {
        is >> x.x;
        return is;
    }
    friend ostream& operator<<(ostream& os, const mint& x) {
        os << x.x;
        return os;
    }
};

mint fact(int n) {
    static vector<mint> fact_memo;

    if (n < (int)fact_memo.size()) {
        return fact_memo[n];
    }
    if (fact_memo.size() == 0) {
        fact_memo.push_back(1);
    }
    for (int i = fact_memo.size(); i <= n; i++) {
        fact_memo.push_back(fact_memo[i - 1].mul(i));
    }
    return fact_memo[n];
}

mint comb(int n, int r) { return fact(n) / fact(r) / fact(n - r); }
vector<pair<int, int>> factorize(int n) {
    vector<pair<int, int>> ans;

    for (int i = 2; i * i <= n; i++) {
        if (n % i != 0) {
            continue;
        }

        int cnt = 0;
        while (n % i == 0) {
            n /= i;
            cnt++;
        }
        ans.push_back(make_pair(i, cnt));
    }

    if (n != 1) {
        ans.push_back(make_pair(n, 1));
    }

    return ans;
}

void main_() {
    int n = readInt();
    int k = readInt();

    auto f = factorize(n);
    mint ans = 1;
    for (int i = 0; i < (int)f.size(); i++) {
        auto t = f[i];
        debug(t);

        auto dp = initVector2<mint>(k + 1, t.second + 1);
        dp[0][0] = 1;
        for (int j = 1; j <= t.second; j++) {
            dp[0][j] = mint(t.first) * dp[0][j - 1];
        }
        for (int i = 1; i <= k; i++) {
            mint sum = 0;
            for (int j = 0; j <= t.second; j++) {
                sum = (sum + dp[i - 1][j]);
                dp[i][j] = sum / (j + 1);
            }
        }
        ans = ans * dp[k][t.second];
    }
    cout << ans << endl;
}

#include <algorithm>
#include <cmath>
#include <complex>
#include <functional>
#include <iomanip>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <string>
#include <utility>
#include <vector>

using namespace std;

#define int long long int

const int INF = 1e15 + 373;

#define rep(i, s, t) for (int i = s; i < (int)(t); ++i)
#define all(a) (a).begin(), (a).end()

template <typename T>
vector<T> initVector(size_t n0, T e = T()) {
    return vector<T>(n0, e);
}

template <typename T>
using vector2 = vector<vector<T>>;

template <typename T>
vector2<T> initVector2(size_t n0, size_t n1, T e = T()) {
    return vector2<T>(n0, vector<T>(n1, e));
}

template <typename T>
using vector3 = vector<vector<vector<T>>>;

template <typename T>
vector3<T> initVector3(size_t n0, size_t n1, size_t n2, T e = T()) {
    return vector3<T>(n0, vector2<T>(n1, vector<T>(n2, e)));
}

int readInt() {
    int ret;
    cin >> ret;
    return ret;
}

vector<int> readInt(int n) {
    vector<int> ret(n);
    rep(i, 0, n) { cin >> ret[i]; }
    return ret;
}

string readString() {
    string ret;
    cin >> ret;
    return ret;
}

vector<string> readString(int n) {
    vector<string> ret(n);
    rep(i, 0, n) { cin >> ret[i]; }
    return ret;
}

template <typename Iterator>
ostream& join(ostream& os, Iterator first, Iterator last,
              const string& delim = " ") {
    auto it = first;
    while (it != last) {
        os << *it;
        it++;
        if (it != last) {
            os << delim;
        }
    }
    return os;
}

#ifdef ENABLE_DEBUG
#include "debug.h"
#else
#define debug(v) ;
#endif

void main_();

signed main() {
    cin.tie(0);
    ios::sync_with_stdio(false);
    main_();
    return 0;
}

template <typename T>
class LazySegmentTree {
   private:
    static int calc_size(int n) {
        int m = 1;
        while (m < n) {
            m *= 2;
        }
        return m;
    }

    void eval(int i, int l, int r) {
        if (lazy[i] != 0) {
            node[i] += lazy[i];
            if (r - l > 1) {
                lazy[i * 2 + 1] += lazy[i] / 2;
                lazy[i * 2 + 2] += lazy[i] / 2;
            }
            lazy[i] = 0;
        }
    }

    void add(int s, int t, int x, int i, int l, int r) {
        eval(i, l, r);

        if (t <= l || r <= s) {
            return;
        }

        if (s <= l && r <= t) {
            lazy[i] += (r - l) * x;
            eval(i, l, r);
            return;
        }

        int m = l + (r - l) / 2;
        add(s, t, x, 2 * i + 1, l, m);
        add(s, t, x, 2 * i + 2, m, r);
        node[i] = f(node[2 * i + 1], node[2 * i + 2]);
    }

    T query(int s, int t, int i, int l, int r) {
        eval(i, l, r);

        if (t <= l || r <= s) {
            return init;
        }

        if (s <= l && r <= t) {
            return node[i];
        }

        int m = l + (r - l) / 2;
        T vl = query(s, t, i * 2 + 1, l, m);
        T vr = query(s, t, i * 2 + 2, m, r);
        return f(vl, vr);
    }

   public:
    int n;
    vector<T> node, lazy;
    T init;
    function<T(T, T)> f;

    LazySegmentTree(int n, T init, function<T(T, T)> f)
        : n(calc_size(n)),
          node(calc_size(n) * 2, init),
          lazy(calc_size(n) * 2, 0),
          init(init),
          f(f) {}

    void update(int i, const T& x) {
        node[i + n] = x;
        for (int j = (i + n) / 2; j > 0; j /= 2) {
            node[j] = f(node[j * 2 + 1], node[j * 2 + 2]);
        }
    }

    T query(int s, int t) { return query(s, t, 0, 0, n); }

    void add(int s, int t, int x) { add(s, t, x, 0, 0, n); }
};

int add(int a, int b) { return a + b; }

const int MAX_DEPTH = (int)(3e5 - 1);

struct query {
    int v, d, x;
};

void dfs(const vector2<int>& g, int v, int d, const vector2<query>& qs,
         LazySegmentTree<int>& segtree, vector<int>& ans) {
    rep (i, 0, qs[v].size()) {
        query q = qs[v][i];
        segtree.add(d, min(MAX_DEPTH + 1, d + q.d + 1), q.x);
    }

    ans[v] = segtree.query(d, d + 1);

    rep (i, 0, g[v].size()) {
        int u = g[v][i];
        if (ans[u] != -1) {
            continue;
        }
        dfs(g, u, d + 1, qs, segtree, ans);
    }

    rep (i, 0, qs[v].size()) {
        query q = qs[v][i];
        segtree.add(d, d + q.d + 1, -q.x);
    }
}

void main_() {
    int n = readInt();

    auto g = initVector2(n, 0, 0LL);
    rep(i, 0, n - 1) {
        int x = readInt();
        int y = readInt();
        g[x - 1].push_back(y - 1);
        g[y - 1].push_back(x - 1);
    }

    int m = readInt();
    auto qs = initVector2<query>(n, 0);
    rep(i, 0, m) {
        int v = readInt();
        int d = readInt();
        int x = readInt();
        qs[v - 1].push_back({v - 1, d, x});
    }

    LazySegmentTree<int> segtree(MAX_DEPTH + 1, 0, add);

    vector<int> ans(n);
    fill(all(ans), -1);
    dfs(g, 0, 0, qs, segtree, ans);

    join(cout, all(ans)) << endl;
}

#include <algorithm>
#include <cmath>
#include <complex>
#include <functional>
#include <iomanip>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <string>
#include <utility>
#include <vector>

using namespace std;

#define int long long int

const int INF = 1e15 + 373;

#define rep(i, s, t) for (int i = s; i < (int)(t); ++i)
#define all(a) (a).begin(), (a).end()

template <typename T>
vector<T> initVector(size_t n0, T e = T()) {
    return vector<T>(n0, e);
}

template <typename T>
using vector2 = vector<vector<T>>;

template <typename T>
vector2<T> initVector2(size_t n0, size_t n1, T e = T()) {
    return vector2<T>(n0, vector<T>(n1, e));
}

template <typename T>
using vector3 = vector<vector<vector<T>>>;

template <typename T>
vector3<T> initVector3(size_t n0, size_t n1, size_t n2, T e = T()) {
    return vector3<T>(n0, vector2<T>(n1, vector<T>(n2, e)));
}

int readInt() {
    int ret;
    cin >> ret;
    return ret;
}

vector<int> readInt(int n) {
    vector<int> ret(n);
    rep(i, 0, n) { cin >> ret[i]; }
    return ret;
}

string readString() {
    string ret;
    cin >> ret;
    return ret;
}

vector<string> readString(int n) {
    vector<string> ret(n);
    rep(i, 0, n) { cin >> ret[i]; }
    return ret;
}

template <typename Iterator>
ostream& join(ostream& os, Iterator first, Iterator last,
              const string& delim = " ") {
    auto it = first;
    while (it != last) {
        os << *it;
        it++;
        if (it != last) {
            os << delim;
        }
    }
    return os;
}

#ifdef ENABLE_DEBUG
#include "debug.h"
#else
#define debug(v) ;
#endif

void main_();

signed main() {
    cin.tie(0);
    ios::sync_with_stdio(false);
    main_();
    return 0;
}

class edge {
   public:
    int u;
    int v;
    int w;
    edge() : u(0), v(0), w(0) {}
    edge(int u, int v, int w) : u(u), v(v), w(w) {}

    bool operator==(const edge& e) const {
        return u == e.u && v == e.v && w == e.w;
    }

    bool operator<(const edge& e) const {
        if (w != e.w) {
            return w < e.w;
        }
        return u == e.u ? v < e.v : u < e.u;
    }
};

pair<int, int> updateBest(const pair<int, int>& p, int v) {
    if (v >= p.first) {
        return {v, p.first};
    }
    if (v >= p.second) {
        return {p.first, v};
    }
    return p;
}

int dfs(const vector2<edge>& g, const vector<int>& w, int u, int p, int& opt) {
    if (p != -1 && g[u].size() == 1) {
        opt = max(opt, w[u]);
        return w[u];
    }

    pair<int, int> best = {0, 0};

    for (const edge& e : g[u]) {
        if (e.v == p) {
            continue;
        }
        int c = dfs(g, w, e.v, u, opt);
        if (c - e.w <= 0) {
            continue;
        }
        best = updateBest(best, c - e.w);
    }
    opt = max(opt, best.first + best.second + w[u]);
    return best.first + w[u];
}

void main_() {
    int n = readInt();
    auto w = readInt(n);

    auto g = initVector2<edge>(n, 0);
    for (int i = 0; i < n - 1; i++) {
        int u = readInt() - 1;
        int v = readInt() - 1;
        int w = readInt();
        g[u].push_back(edge(u, v, w));
        g[v].push_back(edge(v, u, w));
    }

    int ans = 0;
    dfs(g, w, 0, -1, ans);
    cout << ans << endl;
}

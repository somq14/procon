#include <algorithm>
#include <cmath>
#include <complex>
#include <functional>
#include <iomanip>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <stack>
#include <string>
#include <utility>
#include <vector>

using namespace std;

#define int long long int

const int INF = 1e15 + 373;

#define rep(i, s, t) for (int i = s; i < (int)(t); ++i)
#define all(a) (a).begin(), (a).end()

template <typename T>
vector<T> initVector(size_t n0, T e = T()) {
    return vector<T>(n0, e);
}

template <typename T>
using vector2 = vector<vector<T>>;

template <typename T>
vector2<T> initVector2(size_t n0, size_t n1, T e = T()) {
    return vector2<T>(n0, vector<T>(n1, e));
}

template <typename T>
using vector3 = vector<vector<vector<T>>>;

template <typename T>
vector3<T> initVector3(size_t n0, size_t n1, size_t n2, T e = T()) {
    return vector3<T>(n0, vector2<T>(n1, vector<T>(n2, e)));
}

int readInt() {
    int ret;
    cin >> ret;
    return ret;
}

vector<int> readInt(int n) {
    vector<int> ret(n);
    rep(i, 0, n) { cin >> ret[i]; }
    return ret;
}

string readString() {
    string ret;
    cin >> ret;
    return ret;
}

vector<string> readString(int n) {
    vector<string> ret(n);
    rep(i, 0, n) { cin >> ret[i]; }
    return ret;
}

template <typename Iterator>
ostream& join(ostream& os, Iterator first, Iterator last,
              const string& delim = " ") {
    auto it = first;
    while (it != last) {
        os << *it;
        it++;
        if (it != last) {
            os << delim;
        }
    }
    return os;
}

#ifdef ENABLE_DEBUG
#include "debug.h"
#else
#define debug(v) ;
#endif

void main_();

signed main() {
    cin.tie(0);
    ios::sync_with_stdio(false);
    main_();
    return 0;
}

vector2<char> decode(int s) {
    auto res = initVector2<char>(4, 4);
    for (int i = 0; i < 4; i++) {
        for (int j = 0; j < 4; j++) {
            res[i][j] = (s & 1 << (i * 4 + j)) != 0;
        }
    }
    return res;
}

int encode(const vector2<char>& b) {
    int res = 0;
    for (int i = 0; i < 4; i++) {
        for (int j = 0; j < 4; j++) {
            if (b[i][j]) {
                res |= 1 << (i * 4 + j);
            }
        }
    }
    return res;
}

vector2<char> remove(const vector2<char>& b) {
    auto res = b;
    for (int y = 0; y < 4; y++) {
        bool all = true;
        for (int x = 0; x < 4; x++) {
            if (!b[y][x]) {
                all = false;
                break;
            }
        }
        if (all) {
            for (int x = 0; x < 4; x++) {
                res[y][x] = false;
            }
        }
    }

    for (int x = 0; x < 4; x++) {
        bool all = true;
        for (int y = 0; y < 4; y++) {
            if (!b[y][x]) {
                all = false;
                break;
            }
        }
        if (all) {
            for (int y = 0; y < 4; y++) {
                res[y][x] = false;
            }
        }
    }
    return res;
}

void main_() {
    auto s = readString();
    int n = s.size();

    auto g = initVector3<int>(2, 1 << 16, 0);
    auto p = initVector3<pair<int, int>>(2, 1 << 16, 0);
    // 0
    for (int j = 0; j < (1 << 16); j++) {
        auto b = decode(j);
        for (int y = 0; y < 4; y++) {
            for (int x = 0; x < 4; x++) {
                if (!b[y][x] && y + 1 < 4 && !b[y + 1][x]) {
                    b[y][x] = true;
                    b[y + 1][x] = true;

                    int next = encode(remove(b));
                    g[0][j].push_back(next);
                    p[0][j].push_back({y, x});

                    b[y][x] = false;
                    b[y + 1][x] = false;
                }
            }
        }
    }
    // 1
    for (int j = 0; j < (1 << 16); j++) {
        auto b = decode(j);
        for (int y = 0; y < 4; y++) {
            for (int x = 0; x < 4; x++) {
                if (!b[y][x] && x + 1 < 4 && !b[y][x + 1]) {
                    b[y][x] = true;
                    b[y][x + 1] = true;

                    int next = encode(remove(b));
                    g[1][j].push_back(next);
                    p[1][j].push_back({y, x});

                    b[y][x] = false;
                    b[y][x + 1] = false;
                }
            }
        }
    }

    auto dp = initVector2<char>(n + 1, 1 << 16);
    auto opt = initVector2<int>(n + 1, 1 << 16);
    for (int j = 0; j < (1 << 16); j++) {
        dp[n][j] = true;
    }

    for (int i = n - 1; i >= 0; i--) {
        const int type = s[i] == '1';
        for (int j = 0; j < (1 << 16); j++) {
            dp[i][j] = false;
            opt[i][j] = -1;
            for (int k = 0; k < (int)g[type][j].size(); k++) {
                int next = g[type][j][k];
                if (dp[i + 1][next]) {
                    dp[i][j] = true;
                    opt[i][j] = k;
                    break;
                }
            }
        }
    }

    if (!dp[0][0]) {
        cout << "err" << endl;
        return;
    }

    int state = 0;
    vector<pair<int, int>> ans;
    for (int i = 0; i < n; i++) {
        int k = opt[i][state];
        auto q = p[s[i] == '1'][state][k];
        ans.push_back({q.first, q.second});
        state = g[s[i] == '1'][state][k];
    }

    for (int i = 0; i < (int)ans.size(); i++) {
        cout << ans[i].first + 1 << " " << ans[i].second + 1 << endl;
    }
}

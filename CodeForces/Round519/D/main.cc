#include <algorithm>
#include <cmath>
#include <complex>
#include <cstdint>
#include <functional>
#include <iomanip>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <string>
#include <utility>
#include <vector>

using namespace std;

#define int long long int

const int INF = 1e15 + 373;

#define rep(i, n) for (int i = 0; i < (int)(n); ++i)

template <typename T>
using vector2 = vector<vector<T>>;

template <typename T>
vector2<T> initVector2(size_t n0, size_t n1, T e = T()) {
    return vector2<T>(n0, vector<T>(n1, e));
}

template <typename T>
using vector3 = vector<vector<vector<T>>>;

template <typename T>
vector3<T> initVector3(size_t n0, size_t n1, size_t n2, T e = T()) {
    return vector3<T>(n0, vector2<T>(n1, vector<T>(n2, e)));
}

int readInt() {
    int ret;
    cin >> ret;
    return ret;
}

vector<int> readInt(int n) {
    vector<int> ret(n);
    rep(i, n) { cin >> ret[i]; }
    return ret;
}

string readString() {
    string ret;
    cin >> ret;
    return ret;
}

vector<string> readString(int n) {
    vector<string> ret(n);
    rep(i, n) { cin >> ret[i]; }
    return ret;
}

#ifdef ENABLE_DEBUG
#include "debug.h"
#else
#define debug(v) ;
#endif

bool check(int n, int m, const vector2<int>& a, const vector2<int>& p, int j) {
    int a0 = a[0][j - 1];
    int a1 = a[0][j];
    for (int i = 1; i < m; i++) {
        if (p[i][a0] + 1 > n || a[i][p[i][a0] + 1] != a1) {
            return false;
        }
    }
    return true;
}

signed main() {
    cin.tie(0);
    ios::sync_with_stdio(false);

    int n;
    int m;
    scanf("%lld", &n);
    scanf("%lld", &m);

    auto a = initVector2(m, n, 0LL);
    rep(i, m) {
        rep(j, n) {
            scanf("%lld", &a[i][j]);
            a[i][j] -= 1;
        }
    }
    debug(a);

    auto p = initVector2(m, n, 0LL);
    rep(i, m) {
        rep(j, n) { p[i][a[i][j]] = j; }
    }
    debug(p);

    int ans = 0;
    int i = 0;
    int j = 0;
    while (i < n) {
        ans += j - i;
        while (j < n && (i == j || check(n, m, a, p, j))) {
            j += 1;
            ans += 1;
        }

        i += 1;
        j = max(i, j);
    }

    printf("%lld\n", ans);

    return 0;
}

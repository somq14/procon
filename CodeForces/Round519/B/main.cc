#include <algorithm>
#include <cmath>
#include <complex>
#include <cstdint>
#include <functional>
#include <iomanip>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <string>
#include <utility>
#include <vector>

using namespace std;

#define int int64_t

const int INF = 1e15 + 373;

#define rep(i, n) for (int i = 0; i < (int)(n); ++i)

template <typename T>
using vector2 = vector<vector<T>>;

template <typename T>
vector2<T> initVector2(size_t n0, size_t n1, T e = T()) {
    return vector2<T>(n0, vector<T>(n1, e));
}

template <typename T>
using vector3 = vector<vector<vector<T>>>;

template <typename T>
vector3<T> initVector3(size_t n0, size_t n1, size_t n2, T e = T()) {
    return vector3<T>(n0, vector2<T>(n1, vector<T>(n2, e)));
}

int readInt() {
    int ret;
    cin >> ret;
    return ret;
}

vector<int> readInt(int n) {
    vector<int> ret(n);
    rep(i, n) { cin >> ret[i]; }
    return ret;
}

string readString() {
    string ret;
    cin >> ret;
    return ret;
}

vector<string> readString(int n) {
    vector<string> ret(n);
    rep(i, n) { cin >> ret[i]; }
    return ret;
}

#ifdef ENABLE_DEBUG
#include "debug.h"
#else
#define debug(v) ;
#endif
bool eval(const vector<int>& a, int l) {
    int n = a.size();
    for (int i = l; i < n; i += l) {
        for (int j = 0; j < l; j++) {
            if (i + j >= n) {
                break;
            }
            if (a[j] != a[i + j]) {
                return false;
            }
        }
    }
    return true;
}

signed main() {
    int n = readInt();
    vector<int> a(n + 1);
    a[0] = 0;
    for (int i = 1; i <= n; i++) {
        cin >> a[i];
    }

    vector<int> d(n);
    for (int i = 0; i < n; i++) {
        d[i] = a[i + 1] - a[i];
    }
    debug(d);

    vector<int> ans(0);
    for (int i = 1; i <= n; i++) {
        if (eval(d, i)) {
            ans.push_back(i);
        }
    }

    cout << ans.size() << endl;
    cout << ans[0];
    for (size_t i = 1; i < ans.size(); i++) {
        cout << " " << ans[i];
    }
    cout << endl;

    return 0;
}

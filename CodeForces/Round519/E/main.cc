#include <algorithm>
#include <cmath>
#include <complex>
#include <cstdint>
#include <functional>
#include <iomanip>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <string>
#include <utility>
#include <vector>

using namespace std;

#define int long long int

const int INF = 1e15 + 373;

#define rep(i, n) for (int i = 0; i < (int)(n); ++i)

template <typename T>
using vector2 = vector<vector<T>>;

template <typename T>
vector2<T> initVector2(size_t n0, size_t n1, T e = T()) {
    return vector2<T>(n0, vector<T>(n1, e));
}

template <typename T>
using vector3 = vector<vector<vector<T>>>;

template <typename T>
vector3<T> initVector3(size_t n0, size_t n1, size_t n2, T e = T()) {
    return vector3<T>(n0, vector2<T>(n1, vector<T>(n2, e)));
}

int readInt() {
    int ret;
    cin >> ret;
    return ret;
}

vector<int> readInt(int n) {
    vector<int> ret(n);
    rep(i, n) { cin >> ret[i]; }
    return ret;
}

string readString() {
    string ret;
    cin >> ret;
    return ret;
}

vector<string> readString(int n) {
    vector<string> ret(n);
    rep(i, n) { cin >> ret[i]; }
    return ret;
}

#ifdef ENABLE_DEBUG
#include "debug.h"
#else
#define debug(v) ;
#endif

int lookupCum(const vector<int>& c, int s, int t) {
    return c[t] - c[s];
}

signed main() {
    cin.tie(0);
    ios::sync_with_stdio(false);

    int n;
    int m;
    scanf("%lld", &n);
    scanf("%lld", &m);

    vector2<int> a = initVector2(n, 2, 0LL);
    for (int i = 0; i < n; i++) {
        scanf("%lld", &a[i][0]);
        scanf("%lld", &a[i][1]);
    }

    vector2<int> ng = initVector2(m, 2, 0LL);
    for (int i = 0; i < m; i++) {
        scanf("%lld", &ng[i][0]);
        scanf("%lld", &ng[i][1]);
        ng[i][0] -= 1;
        ng[i][1] -= 1;
    }

    vector<pair<int, int>> d0(n);
    vector<pair<int, int>> d1(n);
    vector<int> dd(n);
    for (int i = 0; i < n; i++) {
        dd[i] = a[i][1] - a[i][0];
        d0[i] = {a[i][1] - a[i][0], a[i][0]};
        d1[i] = {a[i][1] - a[i][0], a[i][1]};
    }
    sort(dd.begin(), dd.end());
    sort(d0.begin(), d0.end());
    sort(d1.begin(), d1.end());

    vector<int> cum0(n + 1, 0);
    vector<int> cum1(n + 1, 0);
    for (int i = 1; i <= n; i++) {
        cum0[i] = cum0[i - 1] + d0[i - 1].second;
        cum1[i] = cum1[i - 1] + d1[i - 1].second;
    }

    vector<int> ans(n, 0);
    for (int i = 0; i < n; i++) {
        int d = a[i][1] - a[i][0];
        int p = lower_bound(dd.begin(), dd.end(), d) - dd.begin();
        ans[i] = 0;
        ans[i] += lookupCum(cum0, p, n) + (n - p) * a[i][1];
        ans[i] += lookupCum(cum1, 0, p) + (p - 0) * a[i][0];
        ans[i] -= a[i][0] + a[i][1];
    }

    for (int i = 0; i < m; i++) {
        int i0 = ng[i][0];
        int i1 = ng[i][1];
        ans[i0] -= min(a[i0][0] + a[i1][1], a[i0][1] + a[i1][0]);
        ans[i1] -= min(a[i0][0] + a[i1][1], a[i0][1] + a[i1][0]);
    }

    printf("%lld", ans[0]);
    for (int i = 1; i < n; i++) {
        printf(" %lld", ans[i]);
    }
    printf("\n");

    return 0;
}

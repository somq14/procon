#include <algorithm>
#include <cmath>
#include <complex>
#include <functional>
#include <iomanip>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <string>
#include <utility>
#include <vector>

using namespace std;

using ll = long long int;
#define int ll

const int INF = 1e15 + 373;

#define rep(i, n) for (int i = 0; i < (int)(n); ++i)

template <typename T>
using vector2 = vector<vector<T>>;

template <typename T>
vector2<T> init_vector2(size_t n0, size_t n1, T e = T()) {
    return vector2<T>(n0, vector<T>(n1, e));
}

template <typename T>
using vector3 = vector<vector<vector<T>>>;

template <typename T>
vector3<T> init_vector3(size_t n0, size_t n1, size_t n2, T e = T()) {
    return vector3<T>(n0, vector2<T>(n1, vector<T>(n2, e)));
}

int readInt() {
    int ret;
    cin >> ret;
    return ret;
}

vector<int> readInt(int n) {
    vector<int> ret(n);
    rep(i, n) { cin >> ret[i]; }
    return ret;
}

#ifdef ENABLE_DEBUG
#include "debug.h"
#else
#define debug(v) ;
#endif

signed main() {
    int n = readInt();
    int m = readInt();

    auto g = init_vector2(n, n, false);
    rep(i, m) {
        int a = readInt() - 1;
        int b = readInt() - 1;
        g[a][b] = true;
        g[b][a] = true;
    }

    debug(g);

    auto ps = init_vector2(n, 0, 0LL);

    int x = 0;
    for (int v = 0; v < n; v++) {
        ps[v].push_back(x);
        x += 1;
    }

    for (int v = 0; v < n; v++) {
        for (int u = v + 1; u < n; u++) {
            if (g[v][u]) {
                ps[v].push_back(x);
                ps[u].push_back(x);
                x++;
            }
        }
    }

    for (int v = 0; v < n; v++) {
        cout << ps[v].size() << endl;
        for (int x : ps[v]) {
            cout << x + 1 << " " << v + 1 << endl;
        }
    }
    return 0;
}

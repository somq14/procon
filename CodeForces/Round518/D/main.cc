#include <algorithm>
#include <cmath>
#include <complex>
#include <functional>
#include <iomanip>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <string>
#include <utility>
#include <vector>

using namespace std;

using ll = long long int;
#define int ll

const int INF = 1e15 + 373;

#define rep(i, n) for (int i = 0; i < (int)(n); ++i)

template <typename T>
using vector2 = vector<vector<T>>;

template <typename T>
vector2<T> init_vector2(size_t n0, size_t n1, T e = T()) {
    return vector2<T>(n0, vector<T>(n1, e));
}

template <typename T>
using vector3 = vector<vector<vector<T>>>;

template <typename T>
vector3<T> init_vector3(size_t n0, size_t n1, size_t n2, T e = T()) {
    return vector3<T>(n0, vector2<T>(n1, vector<T>(n2, e)));
}

int readInt() {
    int ret;
    cin >> ret;
    return ret;
}

vector<int> readInt(int n) {
    vector<int> ret(n);
    rep(i, n) { cin >> ret[i]; }
    return ret;
}

#ifdef ENABLE_DEBUG
#include "debug.h"
#else
#define debug(v) ;
#endif

const int MOD = 998244353;

int addM(int a, int b) { return (a + b) % MOD; }

int subM(int a, int b) { return (a - b + 2 * MOD) % MOD; }

void addRange(vector<uint32_t>& a, int s, int t, int c) {
    a[s] = addM(a[s], c);
    a[t] = subM(a[t], c);
}

signed main() {
    int n = readInt();
    auto a = readInt(n);
    debug(a);

    auto dp = init_vector3<uint32_t>(n, 2, 202, 0);
    if (a[0] == -1) {
        fill(dp[0][1].begin(), dp[0][1].end(), 0);
        fill(dp[0][0].begin(), dp[0][0].end(), 1);
    } else {
        fill(dp[0][1].begin(), dp[0][1].end(), 0);
        fill(dp[0][0].begin(), dp[0][0].end(), 0);
        dp[0][0][a[0]] = 1;
    }

    for (int i = 0; i < n - 1; i++) {
        // b == 0
        {
            for (int j = 1; j <= 200; j++) {
                const int comb = dp[i][0][j];
                if (a[i + 1] == -1) {
                    addRange(dp[i + 1][1], j, j + 1, comb);
                    addRange(dp[i + 1][0], j + 1, 201, comb);
                } else if (a[i + 1] == j) {
                    addRange(dp[i + 1][1], a[i + 1], a[i + 1] + 1, comb);
                } else if (a[i + 1] > j) {
                    addRange(dp[i + 1][0], a[i + 1], a[i + 1] + 1, comb);
                }
            }
        }
        // b == 1
        {
            for (int j = 1; j <= 200; j++) {
                const int comb = dp[i][1][j];
                if (a[i + 1] == -1) {
                    addRange(dp[i + 1][0], j + 1, 201, comb);
                    addRange(dp[i + 1][1], 1, j + 1, comb);
                } else if (a[i + 1] == j) {
                    addRange(dp[i + 1][1], a[i + 1], a[i + 1] + 1, comb);
                } else if (a[i + 1] > j) {
                    addRange(dp[i + 1][0], a[i + 1], a[i + 1] + 1, comb);
                } else if (a[i + 1] < j) {
                    addRange(dp[i + 1][1], a[i + 1], a[i + 1] + 1, comb);
                }
            }
        }

        for (int j = 0; j <= 200; j++) {
            dp[i + 1][0][j + 1] = addM(dp[i + 1][0][j + 1], dp[i + 1][0][j]);
            dp[i + 1][1][j + 1] = addM(dp[i + 1][1][j + 1], dp[i + 1][1][j]);
        }
    }

    int ans = 0;
    for (int i = 1; i <= 200; i++) {
        ans = addM(ans, dp[n - 1][1][i]);
    }
    cout << ans << endl;
    return 0;
}

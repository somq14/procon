#include <algorithm>
#include <cmath>
#include <complex>
#include <functional>
#include <iomanip>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <string>
#include <utility>
#include <vector>

using namespace std;

using ll = long long int;
#define int ll

const int INF = 1e15 + 373;

#define rep(i, n) for (int i = 0; i < (int)(n); ++i)

template <typename T>
using vector2 = vector<vector<T>>;

template <typename T>
vector2<T> init_vector2(size_t n0, size_t n1, T e = T()) {
    return vector2<T>(n0, vector<T>(n1, e));
}

template <typename T>
using vector3 = vector<vector<vector<T>>>;

template <typename T>
vector3<T> init_vector3(size_t n0, size_t n1, size_t n2, T e = T()) {
    return vector3<T>(n0, vector2<T>(n1, vector<T>(n2, e)));
}

int readInt() {
    int ret;
    cin >> ret;
    return ret;
}

vector<int> readInt(int n) {
    vector<int> ret(n);
    rep(i, n) { cin >> ret[i]; }
    return ret;
}

#ifdef ENABLE_DEBUG
#include "debug.h"
#else
#define debug(v) ;
#endif

signed main() {
    int n = readInt();
    int m = readInt();
    int k = readInt();
    int l = readInt();

    int coinsPerOne = ((k + l) + (m - 1)) / m;
    if (coinsPerOne * m > n) {
        cout << -1 << endl;
    } else {
        cout << coinsPerOne << endl;
    }

    return 0;
}

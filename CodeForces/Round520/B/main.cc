#include <algorithm>
#include <cmath>
#include <complex>
#include <functional>
#include <iomanip>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <string>
#include <utility>
#include <vector>

using namespace std;

#define int long long int

const int INF = 1e15 + 373;

#define rep(i, s, t) for (int i = s; i < (int)(t); ++i)
#define all(a) (a).begin(), (a).end()

template <typename T>
vector<T> initVector(size_t n0, T e = T()) {
    return vector<T>(n0, e);
}

template <typename T>
using vector2 = vector<vector<T>>;

template <typename T>
vector2<T> initVector2(size_t n0, size_t n1, T e = T()) {
    return vector2<T>(n0, vector<T>(n1, e));
}

template <typename T>
using vector3 = vector<vector<vector<T>>>;

template <typename T>
vector3<T> initVector3(size_t n0, size_t n1, size_t n2, T e = T()) {
    return vector3<T>(n0, vector2<T>(n1, vector<T>(n2, e)));
}

int readInt() {
    int ret;
    cin >> ret;
    return ret;
}

vector<int> readInt(int n) {
    vector<int> ret(n);
    rep(i, 0, n) { cin >> ret[i]; }
    return ret;
}

string readString() {
    string ret;
    cin >> ret;
    return ret;
}

vector<string> readString(int n) {
    vector<string> ret(n);
    rep(i, 0, n) { cin >> ret[i]; }
    return ret;
}

template <typename Iterator>
ostream& join(ostream& os, Iterator first, Iterator last,
              const string& delim = " ") {
    auto it = first;
    while (it != last) {
        os << *it;
        it++;
        if (it != last) {
            os << delim;
        }
    }
    return os;
}

#ifdef ENABLE_DEBUG
#include "debug.h"
#else
#define debug(v) ;
#endif

void main_();

signed main() {
    cin.tie(0);
    ios::sync_with_stdio(false);
    main_();
    return 0;
}

void main_() {
    int n = readInt();

    if (n == 1) {
        cout << 1 << " " << 0 << endl;
        return;
    }

    vector<int> a;

    int ans = 1;
    int m = n;
    rep(i, 2, n + 1) {
        int cnt = 0;
        while (m % i == 0) {
            m /= i;
            cnt++;
        }
        if (cnt > 0) {
            a.push_back(cnt);
            ans *= i;
        }
    }

    int k = a.size();

    int amax = 0;
    rep(i, 0, k) { amax = max(amax, a[i]); }

    int wid = 1;
    int j = 1;
    while (j < amax) {
        j *= 2;
        wid += 1;
    }

    bool same = true;
    rep(i, 0, k - 1) {
        if (a[i] != a[i + 1]) {
            same = false;
            break;
        }
    }

    if ((amax == (1LL << (wid - 1))) and same) {
        cout << ans << " " << (wid - 1) << endl;
    } else {
        cout << ans << " " << wid << endl;
    }
}

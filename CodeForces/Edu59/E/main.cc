#include <algorithm>
#include <cmath>
#include <complex>
#include <functional>
#include <iomanip>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <string>
#include <utility>
#include <vector>

using namespace std;

#define int long long int

const int INF = 1e15 + 373;

#define rep(i, s, t) for (int i = s; i < (int)(t); ++i)
#define all(a) (a).begin(), (a).end()

template <typename T>
vector<T> initVector(size_t n0, T e = T()) {
    return vector<T>(n0, e);
}

template <typename T>
using vector2 = vector<vector<T>>;

template <typename T>
vector2<T> initVector2(size_t n0, size_t n1, T e = T()) {
    return vector2<T>(n0, vector<T>(n1, e));
}

template <typename T>
using vector3 = vector<vector<vector<T>>>;

template <typename T>
vector3<T> initVector3(size_t n0, size_t n1, size_t n2, T e = T()) {
    return vector3<T>(n0, vector2<T>(n1, vector<T>(n2, e)));
}

int readInt() {
    int ret;
    cin >> ret;
    return ret;
}

vector<int> readInt(int n) {
    vector<int> ret(n);
    rep(i, 0, n) { cin >> ret[i]; }
    return ret;
}

string readString() {
    string ret;
    cin >> ret;
    return ret;
}

vector<string> readString(int n) {
    vector<string> ret(n);
    rep(i, 0, n) { cin >> ret[i]; }
    return ret;
}

template <typename Iterator>
ostream& join(ostream& os, Iterator first, Iterator last,
              const string& delim = " ") {
    auto it = first;
    while (it != last) {
        os << *it;
        it++;
        if (it != last) {
            os << delim;
        }
    }
    return os;
}

#ifdef ENABLE_DEBUG
#include "debug.h"
#else
#define debug(v) ;
#endif

void main_();

signed main() {
    cin.tie(0);
    ios::sync_with_stdio(false);
    main_();
    return 0;
}

int solve(int n, const vector<int>& a, const string& x, int s, int t, int c,
          vector3<int>& dp) {
    if (dp[s][t][c] >= 0) {
        return dp[s][t][c];
    }

    int m = t - s;
    if (m <= 0) {
        dp[s][t][c] = 0;
        return 0;
    }

    if (m == 1) {
        dp[s][t][c] = a[c + 1];
        return dp[s][t][c];
    }

    int ans = a[c + 1] + solve(n, a, x, s + 1, t, 0, dp);

    for (int i = s + 1; i < t; i++) {
        if (x[s] == x[i]) {
            int sol = solve(n, a, x, s + 1, i, 0, dp) +
                      solve(n, a, x, i, t, c + 1, dp);
            ans = max(sol, ans);
        }
    }
    dp[s][t][c] = ans;
    return ans;
}

void main_() {
    int n = readInt();
    auto x = readString();

    vector<int> a(n + 1);
    a[0] = 0;
    for (int i = 1; i <= n; i++) {
        a[i] = readInt();
    }

    auto dp = initVector3<int>(n + 1, n + 1, n + 1, -1L);
    cout << solve(n, a, x, 0, n, 0, dp) << endl;
}

#include <algorithm>
#include <cmath>
#include <complex>
#include <functional>
#include <iomanip>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <string>
#include <utility>
#include <vector>

using namespace std;

#define int long long int

const int INF = 1e15 + 373;

#define rep(i, s, t) for (int i = s; i < (int)(t); ++i)
#define all(a) (a).begin(), (a).end()

template <typename T>
vector<T> initVector(size_t n0, T e = T()) {
    return vector<T>(n0, e);
}

template <typename T>
using vector2 = vector<vector<T>>;

template <typename T>
vector2<T> initVector2(size_t n0, size_t n1, T e = T()) {
    return vector2<T>(n0, vector<T>(n1, e));
}

template <typename T>
using vector3 = vector<vector<vector<T>>>;

template <typename T>
vector3<T> initVector3(size_t n0, size_t n1, size_t n2, T e = T()) {
    return vector3<T>(n0, vector2<T>(n1, vector<T>(n2, e)));
}

int readInt() {
    int ret;
    cin >> ret;
    return ret;
}

vector<int> readInt(int n) {
    vector<int> ret(n);
    rep(i, 0, n) { cin >> ret[i]; }
    return ret;
}

string readString() {
    string ret;
    cin >> ret;
    return ret;
}

vector<string> readString(int n) {
    vector<string> ret(n);
    rep(i, 0, n) { cin >> ret[i]; }
    return ret;
}

template <typename Iterator>
ostream& join(ostream& os, Iterator first, Iterator last,
              const string& delim = " ") {
    auto it = first;
    while (it != last) {
        os << *it;
        it++;
        if (it != last) {
            os << delim;
        }
    }
    return os;
}

#ifdef ENABLE_DEBUG
#include "debug.h"
#else
#define debug(v) ;
#endif

vector<int> dividers(int n) {
    vector<int> res;
    for (int i = 1; i * i <= n; i++) {
        if (n % i != 0) {
            continue;
        }
        res.push_back(i);
        if (n / i > i) {
            res.push_back(n / i);
        }
    }
    sort(res.begin(), res.end());
    return res;
}

int cumsum2(const vector2<int32_t>& c, int sy, int sx, int ty, int tx) {
    return c[ty][tx] - c[ty][sx] - c[sy][tx] + c[sy][sx];
}

void main_();

signed main() {
    cin.tie(0);
    ios::sync_with_stdio(false);
    main_();
    return 0;
}

bool eval(int n, const vector2<int32_t>& a, int d) {
    for (int i = 0; i < n / d; i++) {
        for (int j = 0; j < n / d; j++) {
            int sum = cumsum2(a, d * i, d * j, d * i + d, d * j + d);
            if (sum != 0 and sum != d * d) {
                return false;
            }
        }
    }
    return true;
}

void main_() {
    int n = readInt();

    auto a = initVector2<int32_t>(n + 1, n + 1);
    for (int i = 0; i < n; i++) {
        string s = readString();
        for (int j = 0; j < n / 4; j++) {
            char c = s[j];
            int v = -1;
            if ('0' <= c && c <= '9') {
                v = c - '0';
            } else if ('A' <= c && c <= 'F') {
                v = 10 + (c - 'A');
            }
            for (int k = 3; k >= 0; k--) {
                if (v & (1 << k)) {
                    a[1 + i][1 + 4 * j + (3 - k)] = 1;
                }
            }
        }
    }

    for (int i = 0; i <= n; i++) {
        for (int j = 0; j < n; j++) {
            a[i][j + 1] += a[i][j];
        }
    }
    for (int j = 0; j <= n; j++) {
        for (int i = 0; i < n; i++) {
            a[i + 1][j] += a[i][j];
        }
    }

    auto d = dividers(n);
    int ans = 1;
    for (int i = d.size() - 1; i >= 1; i--) {
        if (eval(n, a, d[i])) {
            ans = d[i];
            break;
        }
    }
    cout << ans << endl;
}

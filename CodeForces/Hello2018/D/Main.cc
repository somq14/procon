#include <algorithm>
#include <cmath>
#include <complex>
#include <iomanip>
#include <iostream>
#include <queue>
#include <set>
#include <stack>
#include <string>
#include <utility>
#include <vector>

using std::cin;
using std::cout;
using std::endl;
using std::cerr;

using std::string;
using std::to_string;
using std::vector;
using std::set;
using std::queue;
using std::stack;
using std::priority_queue;
using std::pair;
using std::make_pair;

using std::min;
using std::max;
using std::sort;
using std::abs;

using std::fixed;
using std::setprecision;
using std::setw;

typedef long long int ll;
const int MOD = 1e9 + 7;
const int INF = 1e9 + 314;

class problem {
   public:
    int i;
    int a;
    int t;
    problem(int i, int a, int t) : i(i), a(a), t(t) {}
    problem() : i(0), a(0), t(0) {}

    bool operator<(const problem& p) { return t < p.t; }
};

bool eval(int n, const vector<problem>& ps, int t, int p, vector<int>& ans) {

    vector<problem> a;
    for (int i = 0; i < n; i++) {
        if (p <= ps[i].a) {
            a.push_back(ps[i]);
        }
    }

    if ((int)a.size() < p) {
        return false;
    }

    sort(a.begin(), a.end());

    int tsum = 0;
    for (int i = 0; i < p; i++) {
        tsum += a[i].t;
    }

    if (tsum > t) {
        return false;
    }

    ans.resize(0);
    for (int i = 0; i < p; i++) {
        ans.push_back(a[i].i);
    }

    return true;
}

int main() {
    int n, t;
    cin >> n >> t;

    vector<problem> ps(n);
    for (int i = 0; i < n; i++) {
        ps[i].i = i + 1;
        cin >> ps[i].a >> ps[i].t;
    }

    // [0, ub)
    int lb = 0;
    int ub = n + 1;
    vector<int> ans;
    while (ub - lb > 1) {
        int mid = lb + (ub - lb) / 2;
        if (eval(n, ps, t, mid, ans)) {
            lb = mid;
        } else {
            ub = mid;
        }
    }

    eval(n, ps, t, lb, ans);
    cout << lb << endl;
    cout << lb << endl;
    for (int i = 0; i < lb; i++) {
        cout << ans[i] << " ";
    }
    cout << endl;
    return 0;
}

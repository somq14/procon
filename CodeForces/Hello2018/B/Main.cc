#include <algorithm>
#include <cmath>
#include <complex>
#include <iomanip>
#include <iostream>
#include <queue>
#include <set>
#include <stack>
#include <string>
#include <utility>
#include <vector>

using std::cin;
using std::cout;
using std::endl;
using std::cerr;

using std::string;
using std::to_string;
using std::vector;
using std::set;
using std::queue;
using std::stack;
using std::priority_queue;
using std::pair;
using std::make_pair;

using std::min;
using std::max;
using std::sort;
using std::abs;

using std::fixed;
using std::setprecision;
using std::setw;

typedef long long int ll;
const int MOD = 1e9 + 7;
const int INF = 1e9 + 314;

int main() {
    int n;
    cin >> n;

    vector<int> p(n);
    p[0] = -1;
    for (int i = 1; i < n; i++) {
        cin >> p[i];
        p[i]--;
    }

    vector<int> c(n);
    for(int i = 1; i < n; i++){
        c[p[i]]++;
    }

    bool ans = true;
    for(int i = 0; i < n; i++){
        if(c[i] == 0){
            continue;
        }
        int leaf_cnt = 0;
        for(int j = 0; j < n; j++){
            if(p[j] == i && c[j] == 0){
                leaf_cnt++;
            }
        }
        if(leaf_cnt < 3){
            ans = false;
            break;
        }
    }

    cout << (ans ? "Yes" : "No") << endl;

    return 0;
}

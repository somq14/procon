#include <algorithm>
#include <cmath>
#include <complex>
#include <iomanip>
#include <iostream>
#include <queue>
#include <set>
#include <stack>
#include <string>
#include <utility>
#include <vector>

using std::cin;
using std::cout;
using std::endl;
using std::cerr;

using std::string;
using std::to_string;
using std::vector;
using std::set;
using std::queue;
using std::stack;
using std::priority_queue;
using std::pair;
using std::make_pair;

using std::min;
using std::max;
using std::sort;
using std::abs;

using std::fixed;
using std::setprecision;
using std::setw;

typedef long long int ll;
const int MOD = 1e9 + 7;
const int INF = 1e9 + 314;

class bottle {
   public:
    ll a;
    ll c;
    bottle(ll a, ll c) : a(a), c(c) {}
    bottle() : a(0), c(0) {}
};

ll solve(const vector<bottle>& bs, ll i, ll l) {
    if (l == 0) {
        return 0;
    }

    if (i == 0){
        return bs[i].c * ((l + bs[i].a - 1) / bs[i].a);
    }

    ll r = (l / bs[i].a) * bs[i].c;
    l %= bs[i].a;
    return r + min(bs[i].c, solve(bs, i - 1, l));
}

int main() {
    ll n, l;
    cin >> n >> l;

    vector<ll> c(n);
    for (ll i = 0; i < n; i++) {
        cin >> c[i];
    }

    // i-th bottle  (1 << i) c[i]
    vector<bool> need(n, true);
    for (ll i = 0; i < n; i++) {
        for (ll j = i + 1; j < n; j++) {
            if ((c[i] << (j - i)) < c[j]) {
                need[j] = false;
            }
            if (c[j] < c[i]) {
                need[i] = false;
            }
        }
    }

    vector<bottle> bs;
    for (int i = 0; i < n; i++) {
        if (need[i]) {
            bs.push_back(bottle(1 << i, c[i]));
        }
    }

    cout << solve(bs, bs.size() - 1, l) << endl;

    return 0;
}

#include <algorithm>
#include <cmath>
#include <complex>
#include <iomanip>
#include <iostream>
#include <queue>
#include <set>
#include <stack>
#include <string>
#include <utility>
#include <vector>

using std::cin;
using std::cout;
using std::endl;
using std::cerr;

using std::string;
using std::to_string;
using std::vector;
using std::set;
using std::queue;
using std::stack;
using std::priority_queue;
using std::pair;
using std::make_pair;

using std::min;
using std::max;
using std::sort;
using std::abs;

using std::fixed;
using std::setprecision;
using std::setw;

typedef long long int ll;
const int MOD = 1e9 + 7;
const int INF = 1e9 + 314;

const string SINF = "~";

bool comp_lt(const string& a, const string& b) {
    if (a == b) {
        return false;
    }
    if (a == SINF) {
        return false;
    }
    if (b == SINF) {
        return true;
    }

    if (a.length() == b.length()) {
        return a < b;
    }
    return a.length() < b.length();
}

bool update;

void update_ftab(vector<string>& etab, vector<string>& ttab,
                 vector<string>& ftab, int ind, string str) {
    if (comp_lt(str, ftab[ind])) {
        update = true;
        ftab[ind] = str;
    }
    if (comp_lt(str, etab[ind])) {
        update = true;
        etab[ind] = str;
    }
    if (comp_lt(str, ttab[ind])) {
        update = true;
        ttab[ind] = str;
    }
}

void update_ttab(vector<string>& etab, vector<string>& ttab,
                 vector<string>& ftab, int ind, string str) {
    if (comp_lt(str, ttab[ind])) {
        update = true;
        ttab[ind] = str;
    }
    if (comp_lt(str, etab[ind])) {
        update = true;
        etab[ind] = str;
    }
    string tmp = "(" + str + ")";
    if (comp_lt(tmp, ftab[ind])) {
        update = true;
        ftab[ind] = tmp;
    }
}

void update_etab(vector<string>& etab, vector<string>& ttab,
                 vector<string>& ftab, int ind, string str) {
    if (comp_lt(str, etab[ind])) {
        update = true;
        etab[ind] = str;
    }
    string tmp = "(" + str + ")";
    if (comp_lt(tmp, ttab[ind])) {
        update = true;
        ttab[ind] = tmp;
    }
    if (comp_lt(tmp, ftab[ind])) {
        update = true;
        ftab[ind] = tmp;
    }
}

int main() {
    int n;
    cin >> n;

    vector<string> etab(256, SINF);
    vector<string> ttab(256, SINF);
    vector<string> ftab(256, SINF);

    update_ftab(etab, ttab, ftab, 0b00001111, "x");
    update_ftab(etab, ttab, ftab, 0b00110011, "y");
    update_ftab(etab, ttab, ftab, 0b01010101, "z");

    update = true;
    while (update) {
        update = false;

        // !
        for (int i = 0; i < 256; i++) {
            if (ftab[i] == SINF) {
                continue;
            }
            update_ftab(etab, ttab, ftab, ~i & 0xFF, "!" + ftab[i]);
        }

        // &
        for (int i = 0; i < 256; i++) {
            if (etab[i] == SINF) {
                continue;
            }
            for (int j = 0; j < 256; j++) {
                if (ttab[j] == SINF) {
                    continue;
                }
                update_etab(etab, ttab, ftab, i | j, etab[i] + "|" + ttab[j]);
            }
        }

        // |
        for (int i = 0; i < 256; i++) {
            if (ttab[i] == SINF) {
                continue;
            }
            for (int j = 0; j < 256; j++) {
                if (ftab[j] == SINF) {
                    continue;
                }
                update_ttab(etab, ttab, ftab, i & j, ttab[i] + "&" + ftab[j]);
            }
        }
    }

    for (int i = 0; i < n; i++) {
        string f;
        cin >> f;
        int ind = stoi(f, nullptr, 2);

        string ans = ftab[ind];
        if (comp_lt(etab[ind], ans)) {
            ans = etab[ind];
        }
        if (comp_lt(ttab[ind], ans)) {
            ans = ttab[ind];
        }
        cout << ans << endl;
    }

    return 0;
}

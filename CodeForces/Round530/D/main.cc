#include <algorithm>
#include <cmath>
#include <complex>
#include <functional>
#include <iomanip>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <string>
#include <utility>
#include <vector>

using namespace std;

#define int long long int

const int INF = 1e15 + 373;

#define rep(i, s, t) for (int i = s; i < (int)(t); ++i)
#define all(a) (a).begin(), (a).end()

template <typename T>
vector<T> initVector(size_t n0, T e = T()) {
    return vector<T>(n0, e);
}

template <typename T>
using vector2 = vector<vector<T>>;

template <typename T>
vector2<T> initVector2(size_t n0, size_t n1, T e = T()) {
    return vector2<T>(n0, vector<T>(n1, e));
}

template <typename T>
using vector3 = vector<vector<vector<T>>>;

template <typename T>
vector3<T> initVector3(size_t n0, size_t n1, size_t n2, T e = T()) {
    return vector3<T>(n0, vector2<T>(n1, vector<T>(n2, e)));
}

int readInt() {
    int ret;
    cin >> ret;
    return ret;
}

vector<int> readInt(int n) {
    vector<int> ret(n);
    rep(i, 0, n) { cin >> ret[i]; }
    return ret;
}

string readString() {
    string ret;
    cin >> ret;
    return ret;
}

vector<string> readString(int n) {
    vector<string> ret(n);
    rep(i, 0, n) { cin >> ret[i]; }
    return ret;
}

template <typename Iterator>
ostream& join(ostream& os, Iterator first, Iterator last,
              const string& delim = " ") {
    auto it = first;
    while (it != last) {
        os << *it;
        it++;
        if (it != last) {
            os << delim;
        }
    }
    return os;
}

#ifdef ENABLE_DEBUG
#include "debug.h"
#else
#define debug(v) ;
#endif

void main_();

signed main() {
    cin.tie(0);
    ios::sync_with_stdio(false);
    main_();
    return 0;
}

bool dfs1(const vector2<int>& g, int v, int d, vector<int>& s) {
    if (d % 2 == 0 && s[v] != -1) {
        return false;
    }
    if (d % 2 == 1 && s[v] == -1) {
        return false;
    }

    for (int u : g[v]) {
        if (!dfs1(g, u, d + 1, s)) {
            return false;
        }
    }
    return true;
}

bool dfs2(const vector2<int>& g, int v, int p, vector<int>& s) {
    if (g[v].size() == 0) {
        if (s[v] == -1) {
            s[v] = s[p];
        }
        return true;
    }

    if (s[v] != -1) {
        for (int u : g[v]) {
            if (!dfs2(g, u, v, s)) {
                return false;
            }
        }
        return true;
    }

    for (int u : g[v]) {
        if (!dfs2(g, u, v, s)) {
            return false;
        }
    }

    int cmin = INF;
    for (int u : g[v]) {
        cmin = min(cmin, s[u]);
    }

    // [lb, ub]
    int lb = s[p];
    int ub = cmin;

    if (ub - lb < 0) {
        return false;
    }

    s[v] = ub;
    return true;
}

int dfs3(const vector2<int>& g, int v, int p, const vector<int>& s) {
    int res = s[v] - s[p];
    for (int u : g[v]) {
        res += dfs3(g, u, v, s);
    }
    return res;
}

void main_() {
    int n = readInt();

    auto g = initVector2<int>(n, 0);
    for (int i = 1; i < n; i++) {
        int p = readInt() - 1;
        g[p].push_back(i);
    }

    auto s = readInt(n);
    if (!dfs1(g, 0, 1, s)) {
        cout << -1 << endl;
        return;
    }
    debug(s);

    if (!dfs2(g, 0, 0, s)) {
        cout << -1 << endl;
        return;
    }
    debug(s);

    int ans = dfs3(g, 0, 0, s) + s[0];
    cout << ans << endl;
}

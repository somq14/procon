#include <algorithm>
#include <cmath>
#include <complex>
#include <functional>
#include <iomanip>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <string>
#include <utility>
#include <vector>

using namespace std;

#define int long long int

const int INF = 1e15 + 373;

#define rep(i, s, t) for (int i = s; i < (int)(t); ++i)
#define all(a) (a).begin(), (a).end()

template <typename T>
vector<T> initVector(size_t n0, T e = T()) {
    return vector<T>(n0, e);
}

template <typename T>
using vector2 = vector<vector<T>>;

template <typename T>
vector2<T> initVector2(size_t n0, size_t n1, T e = T()) {
    return vector2<T>(n0, vector<T>(n1, e));
}

template <typename T>
using vector3 = vector<vector<vector<T>>>;

template <typename T>
vector3<T> initVector3(size_t n0, size_t n1, size_t n2, T e = T()) {
    return vector3<T>(n0, vector2<T>(n1, vector<T>(n2, e)));
}

int readInt() {
    int ret;
    cin >> ret;
    return ret;
}

vector<int> readInt(int n) {
    vector<int> ret(n);
    rep(i, 0, n) { cin >> ret[i]; }
    return ret;
}

string readString() {
    string ret;
    cin >> ret;
    return ret;
}

vector<string> readString(int n) {
    vector<string> ret(n);
    rep(i, 0, n) { cin >> ret[i]; }
    return ret;
}

template <typename Iterator>
ostream& join(ostream& os, Iterator first, Iterator last,
              const string& delim = " ") {
    auto it = first;
    while (it != last) {
        os << *it;
        it++;
        if (it != last) {
            os << delim;
        }
    }
    return os;
}

#ifdef ENABLE_DEBUG
#include "debug.h"
#else
#define debug(v) ;
#endif

void main_();

signed main() {
    cin.tie(0);
    ios::sync_with_stdio(false);
    main_();
    return 0;
}

void main_() {
    string s = readString();
    int k = readInt();

    int star = count(all(s), '*');
    int ques = count(all(s), '?');
    int minLen = s.length() - 2 * star - 2 * ques;
    int maxLen = star > 0 ? INF : s.length() - ques;
    if (!(minLen <= k && k <= maxLen)) {
        cout << "Impossible" << endl;
        return;
    }

    if (star > 0) {
        int starLen = k - (s.length() - 2 * star - 2 * ques);
        string ans = "";
        bool starUsed = false;
        for (int i = 0; i < (int)s.length(); i++) {
            if (s[i] == '*' || s[i] == '?') {
                continue;
            }
            if (i + 1 < (int)s.length() && s[i + 1] == '?') {
                continue;
            }
            if (i + 1 < (int)s.length() && s[i + 1] == '*') {
                if (starUsed) {
                    continue;
                }
                starUsed = true;
                for (int j = 0; j < starLen; j++) {
                    ans.push_back(s[i]);
                }
                continue;
            }
            ans.push_back(s[i]);
        }
        cout << ans << endl;
    } else {
        int toRemove = (s.length() - ques) - k;
        string ans = "";
        for (int i = 0; i < (int)s.length(); i++) {
            if (s[i] == '?') {
                continue;
            }
            if (i + 1 < (int)s.length() && s[i + 1] == '?' && toRemove > 0) {
                toRemove--;
                continue;
            }
            ans.push_back(s[i]);
        }
        cout << ans << endl;
    }
}

#include <algorithm>
#include <cmath>
#include <complex>
#include <functional>
#include <iomanip>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <string>
#include <utility>
#include <vector>

using namespace std;

#define int long long int

const int INF = 1e15 + 373;

#define rep(i, s, t) for (int i = s; i < (int)(t); ++i)
#define all(a) (a).begin(), (a).end()

template <typename T>
vector<T> initVector(size_t n0, T e = T()) {
    return vector<T>(n0, e);
}

template <typename T>
using vector2 = vector<vector<T>>;

template <typename T>
vector2<T> initVector2(size_t n0, size_t n1, T e = T()) {
    return vector2<T>(n0, vector<T>(n1, e));
}

template <typename T>
using vector3 = vector<vector<vector<T>>>;

template <typename T>
vector3<T> initVector3(size_t n0, size_t n1, size_t n2, T e = T()) {
    return vector3<T>(n0, vector2<T>(n1, vector<T>(n2, e)));
}

int readInt() {
    int ret;
    cin >> ret;
    return ret;
}

vector<int> readInt(int n) {
    vector<int> ret(n);
    rep(i, 0, n) { cin >> ret[i]; }
    return ret;
}

string readString() {
    string ret;
    cin >> ret;
    return ret;
}

vector<string> readString(int n) {
    vector<string> ret(n);
    rep(i, 0, n) { cin >> ret[i]; }
    return ret;
}

template <typename Iterator>
ostream& join(ostream& os, Iterator first, Iterator last,
              const string& delim = " ") {
    auto it = first;
    while (it != last) {
        os << *it;
        it++;
        if (it != last) {
            os << delim;
        }
    }
    return os;
}

#ifdef ENABLE_DEBUG
#include "debug.h"
#else
#define debug(v) ;
#endif

void main_();

signed main() {
    cin.tie(0);
    ios::sync_with_stdio(false);
    main_();
    return 0;
}

struct Env {
    int s;
    int t;
    int d;
    int w;
    Env() {}
    Env(int s, int t, int d, int w) : s(s), t(t), d(d), w(w) {}
};

void main_() {
    int n = readInt();
    int m = readInt();
    int k = readInt();

    auto es = initVector<Env>(k);
    for (int i = 0; i < k; i++) {
        int s = readInt();
        int t = readInt();
        int d = readInt();
        int w = readInt();
        es[i] = Env(s - 1, t - 1, d - 1, w);
    }
    sort(all(es), [](const Env& e1, const Env& e2) { return e1.s < e2.s; });

    auto f = [&](int i, int j) {
        if (es[i].w == es[j].w) {
            return es[i].d < es[j].d;
        }
        return es[i].w < es[j].w;
    };
    auto que = priority_queue<int, vector<int>, decltype(f)>(f);

    auto sel = initVector<int>(n + 1);
    int ep = 0;
    for (int i = 0; i <= n; i++) {
        while (ep < k and es[ep].s <= i) {
            que.push(ep);
            ep++;
        }

        while (que.size() > 0 and es[que.top()].t < i) {
            que.pop();
        }

        if (que.size() <= 0) {
            sel[i] = -1;
            continue;
        }
        sel[i] = que.top();
    }
    debug(sel);

    auto dp = initVector2<int>(n + 1, m + 1);
    for (int i = n - 1; i >= 0; i--) {
        if (sel[i] == -1) {
            dp[i][0] = dp[i + 1][0];
            continue;
        }
        dp[i][0] = dp[es[sel[i]].d + 1][0] + es[sel[i]].w;
    }

    for (int j = 1; j <= m; j++) {
        for (int i = n - 1; i >= 0; i--) {
            int opt = dp[i + 1][j - 1];
            if (sel[i] == -1) {
                opt = min(opt, dp[i + 1][j]);
                dp[i][j] = opt;
                continue;
            }
            opt = min(opt, dp[es[sel[i]].d + 1][j] + es[sel[i]].w);
            dp[i][j] = opt;
        }
    }

    int ans = INF;
    for (int j = 0; j <= m; j++) {
        ans = min(ans, dp[0][j]);
    }
    cout << ans << endl;
}

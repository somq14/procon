#include <algorithm>
#include <cmath>
#include <complex>
#include <functional>
#include <iomanip>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <string>
#include <utility>
#include <vector>

using namespace std;

#define int long long int

const int INF = 1e15 + 373;

#define rep(i, s, t) for (int i = s; i < (int)(t); ++i)
#define all(a) (a).begin(), (a).end()

template <typename T>
vector<T> initVector(size_t n0, T e = T()) {
    return vector<T>(n0, e);
}

template <typename T>
using vector2 = vector<vector<T>>;

template <typename T>
vector2<T> initVector2(size_t n0, size_t n1, T e = T()) {
    return vector2<T>(n0, vector<T>(n1, e));
}

template <typename T>
using vector3 = vector<vector<vector<T>>>;

template <typename T>
vector3<T> initVector3(size_t n0, size_t n1, size_t n2, T e = T()) {
    return vector3<T>(n0, vector2<T>(n1, vector<T>(n2, e)));
}

int readInt() {
    int ret;
    cin >> ret;
    return ret;
}

vector<int> readInt(int n) {
    vector<int> ret(n);
    rep(i, 0, n) { cin >> ret[i]; }
    return ret;
}

string readString() {
    string ret;
    cin >> ret;
    return ret;
}

vector<string> readString(int n) {
    vector<string> ret(n);
    rep(i, 0, n) { cin >> ret[i]; }
    return ret;
}

template <typename Iterator>
ostream& join(ostream& os, Iterator first, Iterator last,
              const string& delim = " ") {
    auto it = first;
    while (it != last) {
        os << *it;
        it++;
        if (it != last) {
            os << delim;
        }
    }
    return os;
}

#ifdef ENABLE_DEBUG
#include "debug.h"
#else
#define debug(v) ;
#endif

void main_();

signed main() {
    cin.tie(0);
    ios::sync_with_stdio(false);
    main_();
    return 0;
}

struct Food {
    int i;
    int a;
    int c;
    Food() {}
    Food(int i, int a, int c) : i(i), a(a), c(c) {}
};

void main_() {
    int n = readInt();
    int m = readInt();
    auto a = readInt(n);
    auto c = readInt(n);

    auto foods = initVector<Food>(n);
    for (int i = 0; i < n; i++) {
        foods[i] = Food(i, a[i], c[i]);
    }

    sort(all(foods), [](const Food& f1, const Food& f2) {
        if (f1.c == f2.c) {
            return f1.i < f2.i;
        }
        return f1.c < f2.c;
    });

    map<int, int> tab;
    for (int i = 0; i < n; i++) {
        tab[foods[i].i] = i;
    }

    auto ts = initVector<int>(m);
    auto ds = initVector<int>(m);
    for (int i = 0; i < m; i++) {
        ts[i] = readInt() - 1;
        ds[i] = readInt();
    }

    int mInd = 0;
    for (int i = 0; i < m; i++) {
        int cost = 0;

        int t = ts[i];
        int d = ds[i];

        int tCount = min(foods[tab[t]].a, d);
        foods[tab[t]].a -= tCount;
        d -= tCount;
        cost += tCount * foods[tab[t]].c;

        while (mInd < n && d > 0) {
            if (foods[mInd].a <= 0) {
                mInd++;
                continue;
            }
            int consume = min(foods[mInd].a, d);
            foods[mInd].a -= consume;
            d -= consume;
            cost += consume * foods[mInd].c;
        }

        if (d > 0) {
            cost = 0;
        }

        cout << cost << endl;
    }
}

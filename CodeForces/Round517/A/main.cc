#include <algorithm>
#include <cmath>
#include <complex>
#include <functional>
#include <iomanip>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <string>
#include <utility>
#include <vector>

using namespace std;

using ll = long long int;
#define int ll
#define let const auto
#define var auto

const int MOD = 1e9 + 7;
const int INF = 1e15 + 373;

/*------------------------------------------------------------*/
/* statement                                                  */
/*------------------------------------------------------------*/
#define rep(i, n) for (int i = 0; i < (int)(n); ++i)

#ifdef ENABLE_DEBUG
#define debug(v)                          \
    do {                                  \
        cerr << #v << " = " << v << endl; \
    } while (0)
#else
#define debug(v) ;
#endif

/*------------------------------------------------------------*/
/* pair                                                       */
/*------------------------------------------------------------*/
template <typename T1, typename T2>
ostream& operator<<(ostream& os, const pair<T1, T2>& p) {
    os << "(" << p.first << ", " << p.second << ")";
    return os;
}

/*------------------------------------------------------------*/
/* vector                                                     */
/*------------------------------------------------------------*/
template <typename T>
ostream& operator<<(ostream& os, const vector<T>& v) {
    os << "[";
    for (size_t i = 0; i < v.size(); i++) {
        os << v[i];
        if (i != v.size() - 1) {
            os << ", ";
        }
    }
    os << "]";
    return os;
}

ostream& operator<<(ostream& os, const vector<int>& v) {
    os << "[";
    for (size_t i = 0; i < v.size(); i++) {
        os << setw(4) << v[i];
        if (i != v.size() - 1) {
            os << ",";
        }
    }
    os << "]";
    return os;
}

ostream& operator<<(ostream& os, const vector<string>& v) {
    os << "[" << endl;
    for (size_t i = 0; i < v.size(); i++) {
        os << "  " << v[i] << endl;
    }
    os << "]";
    return os;
}

/*------------------------------------------------------------*/
/* vector2                                                    */
/*------------------------------------------------------------*/
template <typename T>
using vector2 = vector<vector<T>>;

template <typename T>
vector2<T> init_vector2(size_t n0, size_t n1, T e = T()) {
    return vector2<T>(n0, vector<T>(n1, e));
}

template <typename T>
ostream& operator<<(ostream& os, const vector2<T>& v) {
    os << "[" << endl;
    for (size_t i = 0; i < v.size(); i++) {
        os << "  " << v[i];
        os << endl;
    }
    os << "]";
    return os;
}

/*----------------------------------------------------------------------------*/
/* vector3                                                                    */
/*----------------------------------------------------------------------------*/
template <typename T>
using vector3 = vector<vector<vector<T>>>;

template <typename T>
vector3<T> init_vector3(size_t n0, size_t n1, size_t n2, T e = T()) {
    return vector3<T>(n0, vector2<T>(n1, vector<T>(n2, e)));
}

/*----------------------------------------------------------------------------*/
/* input                                                                      */
/*----------------------------------------------------------------------------*/
int read_int() {
    int x;
    cin >> x;
    return x;
}

vector<int> read_int_vector(int n) {
    vector<int> a(n);
    rep(i, n) {
        cin >> a[i];
    }
    return a;
}

/*----------------------------------------------------------------------------*/
/* main                                                                       */
/*----------------------------------------------------------------------------*/

signed main() {
    let w = read_int();
    let h = read_int();
    let k = read_int();

    var ans = 0LL;
    rep (i, k) {
        ans += 4 + 2 * (h - 2 - 4 * i) + 2 * (w - 2 - 4 * i);
    }
    cout << ans << endl;

    return 0;
}

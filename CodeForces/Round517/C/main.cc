#include <algorithm>
#include <cmath>
#include <complex>
#include <functional>
#include <iomanip>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <string>
#include <utility>
#include <vector>

using namespace std;

using ll = long long int;
#define int ll
#define let const auto
#define var auto

const int MOD = 1e9 + 7;
const int INF = 1e15 + 373;

/*------------------------------------------------------------*/
/* statement                                                  */
/*------------------------------------------------------------*/
#define rep(i, n) for (int i = 0; i < (int)(n); ++i)

#ifdef ENABLE_DEBUG
#define debug(v)                          \
    do {                                  \
        cerr << #v << " = " << v << endl; \
    } while (0)
#else
#define debug(v) ;
#endif

/*------------------------------------------------------------*/
/* pair                                                       */
/*------------------------------------------------------------*/
template <typename T1, typename T2>
ostream& operator<<(ostream& os, const pair<T1, T2>& p) {
    os << "(" << p.first << ", " << p.second << ")";
    return os;
}

/*------------------------------------------------------------*/
/* vector                                                     */
/*------------------------------------------------------------*/
template <typename T>
ostream& operator<<(ostream& os, const vector<T>& v) {
    os << "[";
    for (size_t i = 0; i < v.size(); i++) {
        os << v[i];
        if (i != v.size() - 1) {
            os << ", ";
        }
    }
    os << "]";
    return os;
}

ostream& operator<<(ostream& os, const vector<int>& v) {
    os << "[";
    for (size_t i = 0; i < v.size(); i++) {
        os << setw(4) << v[i];
        if (i != v.size() - 1) {
            os << ",";
        }
    }
    os << "]";
    return os;
}

ostream& operator<<(ostream& os, const vector<string>& v) {
    os << "[" << endl;
    for (size_t i = 0; i < v.size(); i++) {
        os << "  " << v[i] << endl;
    }
    os << "]";
    return os;
}

/*------------------------------------------------------------*/
/* vector2                                                    */
/*------------------------------------------------------------*/
template <typename T>
using vector2 = vector<vector<T>>;

template <typename T>
vector2<T> init_vector2(size_t n0, size_t n1, T e = T()) {
    return vector2<T>(n0, vector<T>(n1, e));
}

template <typename T>
ostream& operator<<(ostream& os, const vector2<T>& v) {
    os << "[" << endl;
    for (size_t i = 0; i < v.size(); i++) {
        os << "  " << v[i];
        os << endl;
    }
    os << "]";
    return os;
}

/*----------------------------------------------------------------------------*/
/* vector3                                                                    */
/*----------------------------------------------------------------------------*/
template <typename T>
using vector3 = vector<vector<vector<T>>>;

template <typename T>
vector3<T> init_vector3(size_t n0, size_t n1, size_t n2, T e = T()) {
    return vector3<T>(n0, vector2<T>(n1, vector<T>(n2, e)));
}

/*----------------------------------------------------------------------------*/
/* input                                                                      */
/*----------------------------------------------------------------------------*/
int read_int() {
    int x;
    cin >> x;
    return x;
}

vector<int> read_int_vector(int n) {
    vector<int> a(n);
    rep(i, n) { cin >> a[i]; }
    return a;
}

/*----------------------------------------------------------------------------*/
/* main                                                                       */
/*----------------------------------------------------------------------------*/
bool eval(int a, int b, int x) {
    let sum = x * (x + 1) / 2;
    if (a + b < sum) {
        return false;
    }
    let lim = max(a - (a + b - sum), 0LL);
    if (lim == 0LL) {
        return true;
    }

    var aSum = 0LL;
    for (int i = x; i >= 1; i--) {
        if (aSum + i <= a) {
            aSum += i;
        }
    }
    return aSum >= lim;
}

signed main() {
    let a = read_int();
    let b = read_int();

    // [lb, ub)
    var lb = 0LL;
    var ub = 100000LL;
    while (ub - lb > 1) {
        int mid = (lb + ub) / 2;
        if (eval(a, b, mid)) {
            lb = mid;
        } else {
            ub = mid;
        }
    }
    int opt = lb;

    vector<int> notesA(0);
    vector<int> notesB(0);
    int aSum = 0;
    for (int i = opt; i >= 1; i--) {
        if (aSum + i <= a) {
            notesA.push_back(i);
            aSum += i;
        } else {
            notesB.push_back(i);
        }
    }

    cout << notesA.size() << endl;
    rep(i, notesA.size()) {
        cout << notesA[i];
        if (i != notesA.size() - 1) {
            cout << " ";
        }
    }
    cout << endl;

    cout << notesB.size() << endl;
    rep(i, notesB.size()) {
        cout << notesB[i];
        if (i != notesB.size() - 1) {
            cout << " ";
        }
    }
    cout << endl;

    return 0;
}

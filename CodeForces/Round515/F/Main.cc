#include <algorithm>
#include <cmath>
#include <complex>
#include <functional>
#include <iomanip>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <string>
#include <utility>
#include <vector>

using namespace std;

using ll = long long int;
#define int ll

const int MOD = 998244353;
const int INF = 1e15 + 373;

#define rep(i, n) for (int i = 0; i < (int)(n); ++i)

template <typename T>
using vector2 = vector<vector<T>>;
template <typename T>
vector2<T> init_vector2(size_t n0, size_t n1, T e = T()) {
    return vector2<T>(n0, vector<T>(n1, e));
}

template <typename T>
using vector3 = vector<vector<vector<T>>>;
template <typename T>
vector3<T> init_vector3(size_t n0, size_t n1, size_t n2, T e = T()) {
    return vector3<T>(n0, vector2<T>(n1, vector<T>(n2, e)));
}

template <typename T>
ostream& operator<<(ostream& os, const vector<T>& v) {
    os << "[";
    for (T e : v) {
        os << e << ", ";
    }
    os << "]";
    return os;
}

class Point {
   public:
    int y, x;

    Point() : y(0), x(0) {}
    Point(int y, int x) : y(y), x(x) {}

    bool operator==(const Point& p) { return this->y == p.y && this->x == p.x; }

    int distance(const Point& p) {
        return abs(this->y - p.y) + abs(this->x - p.x);
    }

    friend ostream& operator<<(ostream& os, const Point& p) {
        os << "(" << p.x << ", " << p.y << ")";
        return os;
    }
};

signed main() {
    int n;
    cin >> n;

    vector<Point> ps(n + 1);
    rep(i, n) {
        int x, y;
        cin >> x >> y;
        ps[i] = Point(y, x);
    }
    ps[n] = Point(0, 0);

    vector<int> levels(n + 1);
    rep(i, n + 1) { levels[i] = max(ps[i].y, ps[i].x); }

    sort(levels.begin(), levels.end());
    levels.erase(unique(levels.begin(), levels.end()), levels.end());

    map<int, int> levelmap;
    rep(i, levels.size()) { levelmap[levels[i]] = i; }

    vector<Point> minX(levels.size(), Point(0, 0));
    vector<Point> minY(levels.size(), Point(0, 0));

    rep(i, n) {
        int lev = levelmap[max(ps[i].y, ps[i].x)];
        if (minX[lev] == Point(0, 0) || ps[i].x < minX[lev].x ||
            (ps[i].x == minX[lev].x && ps[i].y >= minX[lev].y)) {
            minX[lev] = ps[i];
        }
        if (minY[lev] == Point(0, 0) || ps[i].y < minY[lev].y ||
            (ps[i].y == minY[lev].y && ps[i].x >= minY[lev].x)) {
            minY[lev] = ps[i];
        }
    }

    vector<vector<int>> dp(levels.size(), vector<int>(2));

    dp[levels.size() - 1][0] =
        minX[levels.size() - 1].distance(minY[levels.size() - 1]);
    dp[levels.size() - 1][1] =
        minY[levels.size() - 1].distance(minX[levels.size() - 1]);
    for (int i = levels.size() - 2; i >= 0; i--) {
        dp[i][0] =
            min(minX[i].distance(minY[i]) + minY[i].distance(minX[i + 1]) + dp[i + 1][0],
                minX[i].distance(minY[i]) + minY[i].distance(minY[i + 1]) + dp[i + 1][1]);
        dp[i][1] =
            min(minY[i].distance(minX[i]) + minX[i].distance(minX[i + 1]) + dp[i + 1][0],
                minY[i].distance(minX[i]) + minX[i].distance(minY[i + 1]) + dp[i + 1][1]);
    }

    cout << dp[0][0] << endl;
    return 0;
}

#include <algorithm>
#include <cmath>
#include <complex>
#include <functional>
#include <iomanip>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <string>
#include <utility>
#include <vector>

using namespace std;

using ll = long long int;
#define int ll

const int MOD = 998244353;
const int INF = 1e15 + 373;

#define rep(i, n) for (int i = 0; i < (int)(n); ++i)

template <typename T>
using vector2 = vector<vector<T>>;
template <typename T>
vector2<T> init_vector2(size_t n0, size_t n1, T e = T()) {
    return vector2<T>(n0, vector<T>(n1, e));
}

template <typename T>
using vector3 = vector<vector<vector<T>>>;
template <typename T>
vector3<T> init_vector3(size_t n0, size_t n1, size_t n2, T e = T()) {
    return vector3<T>(n0, vector2<T>(n1, vector<T>(n2, e)));
}

template <typename T>
ostream& operator<<(ostream& os, const vector<T>& v) {
    os << "[";
    for (T e : v) {
        os << e << ", ";
    }
    os << "]";
    return os;
}

template <typename T>
class segment_tree {
   private:
    static int calc_size(int n) {
        int m = 1;
        while (m < n) {
            m *= 2;
        }
        return m;
    }

    T query(int s, int t, int i, int l, int r) const {
        if (t <= l || r <= s) {
            return e;
        }

        if (s <= l && r <= t) {
            return a[i];
        }

        int m = l + (r - l) / 2;
        T vl = query(s, t, i * 2, l, m);
        T vr = query(s, t, i * 2 + 1, m, r);
        return f(vl, vr);
    }

   public:
    int n;
    vector<T> a;
    T e;
    function<T(T, T)> f;

    segment_tree(int n, T e, function<T(T, T)> f)
        : n(calc_size(n)), a(calc_size(n) * 2, e), e(e), f(f) {}

    void update(int i, const T& x) {
        a[i + n] = x;
        for (int j = (i + n) / 2; j > 0; j /= 2) {
            a[j] = f(a[j * 2], a[j * 2 + 1]);
        }
    }

    T query(int s, int t) const { return query(s, t, 1, 0, n); }
};



signed main() {
    int n, m;
    cin >> n >> m;

    string a, b;
    cin >> a;
    cin >> b;

    reverse(a.begin(), a.end());
    reverse(b.begin(), b.end());

    segment_tree<int> segtree(a.size(), 0, [](int x1, int x2){ return (x1 + x2) % MOD; });
    int pow2 = 1;
    rep(i, a.size()) {
        if (a[i] == '1') {
            segtree.update(i, pow2);
        }
        pow2 = pow2 * 2 % MOD;
    }

    int ans = 0;
    rep(i, b.size()) {
        if (b[i] == '1') {
            int comb = segtree.query(0, min(i + 1, (int)a.size()));
            ans = (ans + comb) % MOD;
        }
    }

    cout << ans << endl;

    return 0;
}

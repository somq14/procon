#include <algorithm>
#include <cmath>
#include <complex>
#include <functional>
#include <iomanip>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <string>
#include <utility>
#include <vector>

using namespace std;

using ll = long long int;
#define int ll

const int MOD = 1e9 + 7;
const int INF = 1e15 + 373;

#define rep(i, n) for (int i = 0; i < (n); ++i)

template <typename T>
using vector2 = vector<vector<T>>;
template <typename T>
vector2<T> init_vector2(size_t n0, size_t n1, T e = T()) {
    return vector2<T>(n0, vector<T>(n1, e));
}

template <typename T>
using vector3 = vector<vector<vector<T>>>;
template <typename T>
vector3<T> init_vector3(size_t n0, size_t n1, size_t n2, T e = T()) {
    return vector3<T>(n0, vector2<T>(n1, vector<T>(n2, e)));
}

template <typename T>
ostream& operator<<(ostream& os, const vector<T>& v) {
    os << "[";
    for (T e : v) {
        os << e << ", ";
    }
    os << "]";
    return os;
}


signed main() {
    int q;
    cin >> q;

    vector<int> deq(200002);
    vector<int> pos(200001);
    int s = 0;
    int t = 0;

    rep(i, q) {
        char cmd;
        int x;
        cin >> cmd >> x;
        switch (cmd) {
            case 'L': {
                s = (s - 1 + deq.size()) % deq.size();
                deq[s] = x;
                pos[x] = s;
            } break;
            case 'R': {
                deq[t] = x;
                pos[x] = t;
                t = (t + 1) % deq.size();
            } break;
            case '?': {
                int ansL = (pos[x] - s + deq.size()) % deq.size();
                int ansR = (t - pos[x] + deq.size() - 1) % deq.size();
                cout << min(ansL, ansR) << endl;
            } break;
        }
    }

    return 0;
}

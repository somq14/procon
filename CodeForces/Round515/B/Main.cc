#include <algorithm>
#include <cmath>
#include <complex>
#include <functional>
#include <iomanip>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <string>
#include <utility>
#include <vector>

using namespace std;

using ll = long long int;
#define int ll

const int MOD = 1e9 + 7;
const int INF = 1e15 + 373;

#define rep(i, n) for (int i = 0; i < (n); ++i)

template <typename T>
using vector2 = vector<vector<T>>;
template <typename T>
vector2<T> init_vector2(size_t n0, size_t n1, T e = T()) {
    return vector2<T>(n0, vector<T>(n1, e));
}

template <typename T>
using vector3 = vector<vector<vector<T>>>;
template <typename T>
vector3<T> init_vector3(size_t n0, size_t n1, size_t n2, T e = T()) {
    return vector3<T>(n0, vector2<T>(n1, vector<T>(n2, e)));
}

template <typename T>
ostream& operator<<(ostream& os, const vector<T>& v) {
    os << "[";
    for (T e : v) {
        os << e << ", ";
    }
    os << "]";
    return os;
}

int solve(int n, int r, const vector<int>& a) {
    int ans = 0;

    vector<bool> warmed(n, false);
    rep(i, n) {
        if (warmed[i]) {
            continue;
        }

        int p = -1;

        for (int j = min(n - 1, i + r - 1); j >= max(0LL, i - r + 1); j--) {
            if (a[j]) {
                p = j;
                break;
            }
        }

        if (p == -1) {
            return -1;
        }

        for (int j = p - r + 1; j <= p + r - 1; j++) {
            if (0 <= j && j < n) {
                warmed[j] = true;
            }
        }
        ans += 1;
    }
    return ans;
}

signed main() {
    int n, r;
    cin >> n >> r;

    vector<int> a(n);
    rep(i, n) { cin >> a[i]; }

    cout << solve(n, r, a) << endl;

    return 0;
}

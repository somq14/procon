#include <algorithm>
#include <cassert>
#include <cmath>
#include <complex>
#include <cstdlib>
#include <functional>
#include <iomanip>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <string>
#include <utility>
#include <vector>
#include <ctime>

using namespace std;

#define int long long int

const int INF = 1e15 + 373;

#define rep(i, s, t) for (int i = s; i < (int)(t); ++i)
#define all(a) (a).begin(), (a).end()

template <typename T>
vector<T> initVector(size_t n0, T e = T()) {
    return vector<T>(n0, e);
}

template <typename T>
using vector2 = vector<vector<T>>;

template <typename T>
vector2<T> initVector2(size_t n0, size_t n1, T e = T()) {
    return vector2<T>(n0, vector<T>(n1, e));
}

template <typename T>
using vector3 = vector<vector<vector<T>>>;

template <typename T>
vector3<T> initVector3(size_t n0, size_t n1, size_t n2, T e = T()) {
    return vector3<T>(n0, vector2<T>(n1, vector<T>(n2, e)));
}

int readInt() {
    int ret;
    cin >> ret;
    return ret;
}

vector<int> readInt(int n) {
    vector<int> ret(n);
    rep(i, 0, n) { cin >> ret[i]; }
    return ret;
}

string readString() {
    string ret;
    cin >> ret;
    return ret;
}

vector<string> readString(int n) {
    vector<string> ret(n);
    rep(i, 0, n) { cin >> ret[i]; }
    return ret;
}

template <typename Iterator>
ostream& join(ostream& os, Iterator first, Iterator last,
              const string& delim = " ") {
    auto it = first;
    while (it != last) {
        os << *it;
        it++;
        if (it != last) {
            os << delim;
        }
    }
    return os;
}

#ifdef ENABLE_DEBUG
#include "debug.h"
#else
#define debug(v) ;
#endif

void main_();

signed main() {
    main_();
    return 0;
}

int query1(int c, int d) {
    cout << "? " << c << " " << d << endl;
    fflush(stdout);

    int res;
    cin >> res;
    return res;
}

void answer1(int a, int b) {
    cout << "! " << a << " " << b << endl;
    fflush(stdout);
}

int testA = 0;
int testB = 0;

int query2(int c, int d) {
    if ((testA ^ c) > (testB ^ d)) {
        return 1;
    }
    if ((testA ^ c) < (testB ^ d)) {
        return -1;
    }
    return 0;
}

void answer2(int a, int b) {
    if (a == testA && b == testB) {
        return;
    }
    cout << "Expected : " << testA << " " << testB << endl;
    cout << "Actual   : " << a << " " << b << endl;
    exit(1);
}

#define query query1
#define answer answer1

void solve() {
    int cmp = query(0, 0);

    int a = 0;
    int b = 0;

    for (int i = 29; i >= 0; i--) {
        if (cmp == 0) {
            int q = query(a | (1 << i), b);
            if (q < 0) {
                // one
                a |= 1 << i;
                b |= 1 << i;
            } else if (q > 0) {
                // zero
            } else {
                assert(false);
            }
            continue;
        }

        if (cmp == 1) {
            // a > b
            int qa = query(a | (1 << i), b);
            if (qa == 0) {
                a |= 1 << i;
                cmp = 0;
                continue;
            }
            int qb = query(a, b | (1 << i));
            if (qa == 1 && qb == 1) {
                a |= 1 << i;
                cmp = 1;
                continue;
            }
            if (qa == 1 && qb == -1) {
                cmp = 1;
                continue;
            }
            if (qa == -1 && qb == 1) {
                a |= 1 << i;
                b |= 1 << i;
                cmp = 1;
                continue;
            }
            if (qa == -1 && qb == -1) {
                a |= 1 << i;
                cmp = -1;
                continue;
            }
        }

        if (cmp == -1) {
            // a < b
            int qb = query(a, b | (1 << i));
            if (qb == 0) {
                b |= 1 << i;
                cmp = 0;
                continue;
            }
            int qa = query(a | (1 << i), b);
            if (qb == 1 && qa == 1) {
                b |= 1 << i;
                cmp = 1;
                continue;
            }
            if (qb == 1 && qa == -1) {
                a |= 1 << i;
                b |= 1 << i;
                cmp = -1;
                continue;
            }
            if (qb == -1 && qa == 1) {
                cmp = -1;
                continue;
            }
            if (qb == -1 && qa == -1) {
                b |= 1 << i;
                cmp = -1;
                continue;
            }
        }
    }

    answer(a, b);
}

void main_() {
    /*
    srand(time(NULL));
    for (int i = 0; i < 1000 * 1000; i++) {
        testA = (rand() << 30 ^ rand()) & ((1 << 30) - 1);
        testB = (rand() << 30 ^ rand()) & ((1 << 30) - 1);
        solve();
    }
    */
    solve();
}

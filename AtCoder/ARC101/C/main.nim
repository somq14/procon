import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import logging
import future

const INF* = int(1e18 + 373)

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

proc readInt1*(): int = readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#

proc main() =
  var (n, k) = readInt2()
  var x = readSeq().map(parseInt)

  var hasZero = x.find(0) >= 0
  if hasZero:
    k -= 1
  else:
    x.add(0)
    x.sort(cmp[int])

  let zeroInd = x.find(0)

  if k == 0:
    echo 0
    return

  var ans = INF
  for i in 0..k:
    if zeroInd - i >= 0 and (zeroInd + (k - i)) < x.len():
      let dis = x[zeroInd - i].abs() * 2 + x[zeroInd + (k - i)].abs()
      ans = min(ans, dis)

    if zeroInd + i < x.len() and (zeroInd - (k - i)) >= 0:
      let dis = x[zeroInd + i].abs() * 2 + x[zeroInd - (k - i)].abs()
      ans = min(ans, dis)

  echo ans

main()


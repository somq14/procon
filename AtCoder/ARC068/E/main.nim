import strutils
import sequtils
import algorithm
import math
import queues
import tables
import logging
import future

const INF* = int(1e18 + 373)

when not defined(release):
  addHandler(newFileLogger(stderr, lvlAll, "[ $levelname ] "))

proc readInt1*(): int = stdin.readLine().strip().parseInt()
proc readInt2*(): (int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1])
proc readInt3*(): (int, int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1], v[2])

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#

type LazyAddSegmentTree = object
  n: int
  a: seq[int]
  d: seq[int]

proc initLazyAddSegmentTree(n: int): LazyAddSegmentTree =
  result.n = nextPowerOfTwo(n)
  result.a = newSeq[int](result.n * 2)
  result.a.fill(0)
  result.d = newSeq[int](result.n * 2)
  result.d.fill(0)

proc len(this: LazyAddSegmentTree): int = this.n

proc propagate(this: var LazyAddSegmentTree; i: int) =
  this.d[i * 2] += this.d[i]
  this.d[i * 2 + 1] += this.d[i]
  this.d[i] = 0

proc evaluate(this: var LazyAddSegmentTree; i, si, ti: int): int =
  this.a[i] + (ti - si) * this.d[i]

proc update(this: var LazyAddSegmentTree; sq, tq, x: int; i, si, ti: int) =
  if tq <= si or ti <= sq:
    return

  if sq <= si and ti <= tq:
    this.d[i] += x
    return

  this.propagate(i)

  let mi = (si + ti) div 2
  this.update(sq, tq, x, i * 2, si, mi)
  this.update(sq, tq, x, i * 2 + 1, mi, ti)

  this.a[i] = this.evaluate(i * 2, si, mi) + this.evaluate(i * 2 + 1, mi, ti)

proc update(this: var LazyAddSegmentTree; sq, tq, x: int) =
  this.update(sq, tq, x, 1, 0, this.n)

proc update(this: var LazyAddSegmentTree; i, x: int) =
  this.update(i, i + 1, x, 1, 0, this.n)

proc query(this: var LazyAddSegmentTree; sq, tq: int; i, si, ti: int): int =
  if tq <= si or ti <= sq:
    return 0

  if sq <= si and ti <= tq:
    return this.evaluate(i, si, ti)

  this.propagate(i)

  let mi = (si + ti) div 2
  let sum0 = this.query(sq, tq, i * 2, si, mi)
  let sum1 = this.query(sq, tq, i * 2 + 1, mi, ti)
  this.a[i] = sum0 + sum1
  return this.a[i]

proc query(this: var LazyAddSegmentTree; sq, tq: int): int =
  this.query(sq, tq, 1, 0, this.n)

proc query(this: var LazyAddSegmentTree; i: int): int =
  this.query(i, i + 1, 1, 0, this.n)

proc `$`(this: LazyAddSegmentTree): string =
  result = ""

  var i = 2
  while i <= this.a.len():
    for j in (i div 2)..<i:
      let e = "(" & $j & ", " & $this.a[j] & ", " & $this.d[j] & ")"
      result = result & e
    result = result & "\n"
    i *= 2

#------------------------------------------------------------------------------#
type Range = tuple [s, t: int]

proc len(r: Range): int = r.t - r.s

proc `<`(r1, r2: Range): bool = r1.len() < r2.len()

proc main() =
  let (n, m) = readInt2()

  var rs = newSeq[Range](n)
  for i in 0..<n:
    let (ll, rr) = readInt2()
    rs[i] = (ll, rr + 1)
  rs.sort(cmp[Range])

  var q = initLazyAddSegmentTree(m + 1)
  var j = 0
  for d in 1..m:
    while j < n and rs[j].len() < d:
      q.update(rs[j].s, rs[j].t, 1)
      j += 1

    var cnt = n - j
    var p = 0
    while p <= m:
      cnt += q.query(p)
      p += d

    echo cnt

main()


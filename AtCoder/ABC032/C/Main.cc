#include <algorithm>
#include <complex>
#include <iostream>
#include <queue>
#include <set>
#include <stack>
#include <string>
#include <vector>

using std::cin;
using std::cout;
using std::endl;
using std::cerr;

using std::string;
using std::to_string;
using std::vector;
using std::set;
using std::queue;
using std::stack;
using std::priority_queue;

using std::min;
using std::max;
using std::sort;
using std::abs;

typedef long long int ll;
const ll MOD = 1e9 + 7;
const ll INF = 1e18 + 7;

int main() {
    ll n, k;
    cin >> n >> k;

    vector<ll> a(n);
    for (ll i = 0; i < n; i++) {
        cin >> a[i];
    }

    for(ll i = 0; i < n; i++){
        if(a[i] == 0){
            cout << n << endl;
            return 0;
        }
    }

    ll ans = -1;
    // [i, j)
    ll i = 0;
    ll j = 0;
    ll mul = 1;
    while (i < n) {
        while (j < n && a[j] * mul <= k) {
            mul *= a[j];
            j++;
        }
        // iから始まる最長の区間
        ans = max(ans, j - i);

        if(j - i != 0){
            mul /= a[i];
            i++;
        }else{
            i++;
            j++;
        }
    }

    cout << ans << endl;

    return 0;
}

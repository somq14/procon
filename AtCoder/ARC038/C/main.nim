import strutils
import sequtils
import algorithm
import math
import queues
import tables
import future

const INF* = int(1e18 + 373)

proc readInt1*(): int = stdin.readLine().strip().parseInt()
proc readInt2*(): (int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1])
proc readInt3*(): (int, int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1], v[2])

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#
type Op2[T] = proc(a, b: T): T
type SegmentTree[T] = tuple[ n: int, a: seq[T], e: T, f: Op2[T] ]

proc initSegmentTree[T](n: int, e: T, f: Op2[T]): SegmentTree[T] =
    var m = 1
    while m < n:
        m *= 2

    var a = newSeq[T](2 * m)
    a.fill(e)
    return (m, a, e, f)

proc update[T](this: var SegmentTree[T], i: int, x: T) =
    this.a[this.n + i] = x
    var j = (this.n + i) div 2
    while j > 0:
        this.a[j] = this.f(this.a[j * 2], this.a[j * 2 + 1])
        j = j div 2

proc query[T](this: SegmentTree[T], s, t, i, ll, hh: int): T =
    if t <= ll or hh <= s:
        return this.e
    if s <= ll and hh <= t:
        return this.a[i]

    let m = (ll + hh) div 2
    let vl = this.query(s, t, i * 2, ll, m)
    let vh = this.query(s, t, i * 2 + 1, m, hh)
    return this.f(vl, vh)

proc query[T](this: SegmentTree[T], s, t: int): T = query(this, s, t, 1, 0, this.n)

#------------------------------------------------------------------------------#

type Pred1[T] = T -> bool

# [lb, ub)
proc nibutanLb(lb, ub: int; f: Pred1[int]): int =
  var lb = lb
  var ub = ub
  while ub - lb > 1:
    let mid = (ub + lb) div 2
    if f(mid):
      lb = mid
    else:
      ub = mid
  return lb

#------------------------------------------------------------------------------#

proc minInt(a, b: int): int =
  if a < b: a else: b

proc solve(n: int; c, a: seq[int]): bool =
  var segtree = initSegmentTree[int](n, int(1e18), minInt)

  for i in 0..<n:
    segtree.update(i, -1)
  segtree.update(0, 0)

  var g = newSeq[int](n)
  for i in 1..<n:
    let f = (x: int) => (i - c[i] <= segtree.query(0, x))
    g[i] = nibutanLb(0, n + 1, f)
    segtree.update(g[i], i)

  var sum = 0
  for i in 0..<n:
    if a[i] mod 2 == 1:
      sum = sum xor g[i]

  return sum != 0


proc main() =
  let n = readInt1()

  var c = newSeq[int](n)
  var a = newSeq[int](n)
  for i in 1..<n:
    (c[i], a[i]) = readInt2()

  echo if solve(n, c, a): "First" else: "Second"

main()


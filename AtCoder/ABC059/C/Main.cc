#include <algorithm>
#include <complex>
#include <iomanip>
#include <iostream>
#include <queue>
#include <set>
#include <string>
#include <vector>
#include <cctype>

using std::cin;
using std::cout;
using std::endl;

using std::string;
using std::to_string;
using std::vector;
using std::set;
using std::queue;
using std::priority_queue;

using std::min;
using std::max;
using std::sort;
using std::abs;


int main(void) {
    int n;
    cin >> n;

    vector<long> a(n);
    for(int i = 0; i < n; i++){
        cin >> a[i];
    }

    long sum;
    long cnt;

    sum = a[0] > 0 ? a[0] : 1;
    cnt = a[0] > 0 ? 0    : -a[0] + 1;
    for(int i = 1; i < n; i++){
        // cout << "i " << i << " sum " << sum << " cnt " << cnt << endl;
        if(sum > 0){
            sum += a[i];
            if(sum >= 0){
                cnt += sum + 1;
                sum = -1;
            }
        }else{
            sum += a[i];
            if(sum <= 0){
                cnt += -sum + 1;
                sum = 1;
            }
        }
    }

    long ans = cnt;

    sum = a[0] < 0 ? a[0] : -1;
    cnt = a[0] < 0 ? 0    : a[0] + 1;
    for(int i = 1; i < n; i++){
        // cout << "i " << i << " sum " << sum << " cnt " << cnt << endl;
        if(sum > 0){
            sum += a[i];
            if(sum >= 0){
                cnt += sum + 1;
                sum = -1;
            }
        }else{
            sum += a[i];
            if(sum <= 0){
                cnt += -sum + 1;
                sum = 1;
            }
        }
    }

    ans = min(ans, cnt);

    cout << ans << endl;
    return 0;
}

import strutils
import sequtils
import algorithm
import math
import queues
import tables
import logging
import future

const INF* = int(1e18 + 373)

when not defined(release):
  addHandler(newFileLogger(stderr, lvlAll, "[ $levelname ] "))

proc readInt1*(): int = stdin.readLine().strip().parseInt()
proc readInt2*(): (int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1])
proc readInt3*(): (int, int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1], v[2])

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#

const N = 2048

proc padZero(a: seq[int]): seq[int] =
  result = a
  while result.len() < N:
    result.add(0)

proc trimZero(a: seq[int]): seq[int] =
  var i = N - 1
  while i >= 0 and a[i] == 0:
    i -= 1
  return a[0..i]

proc add(a, b: seq[int]): seq[int] =
  var carry = 0
  result = newSeq[int](N)
  for i in 0..<N:
    let sum = a[i] + b[i] + carry
    result[i] = sum mod 10
    carry = sum div 10

  assert(carry == 0)

proc add(a: seq[int]; k: int): seq[int] =
  assert(0 <= k and k < 10)
  add(a, @[k].padZero())

proc mul(a: seq[int], k: int): seq[int] =
  assert(0 <= k and k < 10)

  var carry = 0
  result = newSeq[int](N)
  for i in 0..<N:
    let prod = a[i] * k + carry
    result[i] = prod mod 10
    carry = prod div 10

  assert(carry == 0)


proc shift(a: seq[int], s: int): seq[int] =
  result = newSeq[int](N)
  for i in countdown(N - 1, s):
    result[i] = a[i - s]


proc mul(a, b: seq[int]): seq[int] =
  result = newSeq[int](N)
  for i in 0..<N:
    let c = a.mul(b[i]).shift(i)
    result = result.add(c)

proc `<`(a, b: seq[int]): bool =
  for i in countdown(N - 1, 0):
    if a[i] == b[i]:
      continue
    if a[i] < b[i]:
      return true
    if a[i] > b[i]:
      return false

  return false

proc `<=`(a, b: seq[int]): bool =
  for i in countdown(N - 1, 0):
    if a[i] == b[i]:
      continue
    if a[i] < b[i]:
      return true
    if a[i] > b[i]:
      return false

  return true

proc solve(a: seq[int]): string =
  let aa = a.mul(a)

  let b = a.add(1)
  let bb = b.mul(b)

  var ans: seq[int] = nil
  var i = N - 2
  while i >= 0:
    let candi0 = aa[i..<N].padZero().shift(i)
    let candi1 = aa[i..<N].padZero().add(1).shift(i)
    if aa <= candi0 and candi0 < bb:
      ans = aa[i..<N].padZero()
      break
    if aa <= candi1 and candi1 < bb:
      ans = aa[i..<N].padZero().add(1)
      break
    i -= 2

  return ans.trimZero().reversed().map(it => $it).join()

proc main() =
  let a = stdin.readLine().map(it => parseInt($it)).reversed().padZero()
  echo solve(a)

main()


import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import future

const INF* = int(1e18 + 373)

proc readLine*(): string =
  stdin.readLine()
proc readSeq*(): seq[string] =
  readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = readLine().strip()
proc readInt1*(): int =
  readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])
proc newSeqWith*[T](n: Natural; e: T): seq[T] =
  result = newSeq[T](n)
  for i in 0..<n:
    result[i] = e
type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] =
  newSeqWith(n1, newSeq[T](n2))
type seq3*[T] = seq[seq[seq[T]]]
proc newSeq3*[T](n1, n2, n3: Natural): seq3[T] =
  newSeqWith(n1, newSeqWith(n2, newSeq[T](n3)))

#------------------------------------------------------------------------------#

proc enumerate(w, i: int; p: var seq[int]; ps: var seq[seq[int]]) =
  if i >= w - 1:
    ps.add(p)
    return

  swap(p[i], p[i + 1])
  enumerate(w, i + 2, p, ps)
  swap(p[i], p[i + 1])

  enumerate(w, i + 1, p, ps)

const MOD = 10^9 + 7

proc main() =
  let (h, w, k) = readInt3()

  var ps: seq[seq[int]] = @[]
  block:
    var p = newSeq[int](w)
    for i in 0..<w:
      p[i] = i
    enumerate(w, 0, p, ps)

  var dp = newSeq2[int](h + 1, w)
  dp[0].fill(0)
  dp[0][0] = 1
  for i in 0..<h:
    for j in 0..<w:
      for p in ps:
        dp[i + 1][p[j]] = (dp[i + 1][p[j]] + dp[i][j]) mod MOD

  echo dp[h][k - 1]

main()


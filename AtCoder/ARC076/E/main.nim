import strutils
import sequtils
import algorithm
import math
import queues
import tables
import logging
import future

const INF* = int(1e18 + 373)

when not defined(release):
  addHandler(newFileLogger(stderr, lvlAll, "[ $levelname ] "))

proc readInt1*(): int = stdin.readLine().strip().parseInt()
proc readInt2*(): (int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1])
proc readInt3*(): (int, int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1], v[2])
proc readInt4*(): (int, int, int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1], v[2], v[3])

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#
type Pos = tuple [x, i: int]

proc onBoundary(r, c, y, x: int): bool =
  y == 0 or y == r or x == 0 or x == c

proc encodePos(r, c, y, x: int): int =
  if y in 0..<r and x == 0:
    return y
  if y == r and x in 0..<c:
    return r + x
  if y in 1..r and x == c:
    return r + c + (r - y)
  if y == 0 and x in 1..c:
    return r + c + r + (c - x)

proc solve(m: int; ps: seq[Pos]): bool =
  var s = newSeq[int](0)
  var used = newSeq[bool](m)
  for p in ps:
    if not used[p.i]:
      s.add(p.i)
      used[p.i] = true
    else:
      if s.pop() != p.i:
        return false
  return true

proc main() =
  let (r, c, n) = readInt3()

  var ps = newSeq[Pos](0)
  var j = 0
  for i in 0..<n:
    var (y1, x1, y2, x2) = readInt4()
    if onBoundary(r, c, y1, x1) and onBoundary(r, c, y2, x2):
      debug y1, " ", x1, " ", encodePos(r, c, y1, x1)
      debug y2, " ", x2, " ", encodePos(r, c, y2, x2)
      ps.add((encodePos(r, c, y1, x1), j))
      ps.add((encodePos(r, c, y2, x2), j))
      j += 1

  ps.sort(cmp[Pos])

  debug ps

  if solve(j, ps):
    echo "YES"
  else:
    echo "NO"

main()


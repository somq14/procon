#include <algorithm>
#include <cctype>
#include <complex>
#include <iomanip>
#include <iostream>
#include <queue>
#include <set>
#include <string>
#include <vector>

using std::cin;
using std::cout;
using std::endl;

using std::string;
using std::to_string;
using std::vector;
using std::set;
using std::queue;
using std::priority_queue;

using std::min;
using std::max;
using std::sort;
using std::abs;

typedef long long ll;
const ll INF = 1e9 + 314;

// no need to have a[mid] ?
bool solve(ll n, ll k, const vector<ll>& a, ll mid) {
    const ll w = a[mid];
    vector<ll> b(a);
    b[mid] = INF;

    vector<ll> dp(k + 1);
    vector<ll> dp_next(k + 1);

    fill(dp.begin(), dp.end(), 0);

    for (ll i = 0; i < n; i++) {
        for (ll j = 1; j <= k; j++) {
            dp_next[j] = dp[j];
            if (j - b[i] > 0) {
                dp_next[j] = max(dp_next[j], b[i] + dp[j - b[i]]);
            }
        }
        dp = dp_next;
    }

    return dp[k] + w < k;
}

signed main(void) {
    ll n, k;
    cin >> n >> k;

    vector<ll> a;
    for (ll i = 0; i < n; i++) {
        ll x;
        cin >> x;
        if (x < k) {
            a.push_back(x);
        }
    }

    sort(a.begin(), a.end());

    // [lb, ub)
    ll ub = a.size();
    ll lb = -1;
    while (ub - lb > 1) {
        ll mid = lb + (ub - lb) / 2;
        if (solve(a.size(), k, a, mid)) {
            lb = mid;
        } else {
            ub = mid;
        }
    }

    cout << (lb + 1) << endl;

    return 0;
}

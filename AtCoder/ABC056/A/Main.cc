#include <algorithm>
#include <cctype>
#include <complex>
#include <iomanip>
#include <iostream>
#include <queue>
#include <set>
#include <string>
#include <vector>

using std::cin;
using std::cout;
using std::endl;

using std::string;
using std::to_string;
using std::vector;
using std::set;
using std::queue;
using std::priority_queue;

using std::min;
using std::max;
using std::sort;
using std::abs;

int main(void) {
    char a, b;
    cin >> a >> b;
    if(a == 'H' && b == 'H'){
        cout << 'H' << endl;
    }
    if(a == 'H' && b == 'D'){
        cout << 'D' << endl;
    }
    if(a == 'D' && b == 'H'){
        cout << 'D' << endl;
    }
    if(a == 'D' && b == 'D'){
        cout << 'H' << endl;
    }
    return 0;
}

import strutils
import sequtils
import algorithm
import math

const INF = int(1e18 + 373)

proc readInt1() : int = stdin.readLine().strip().parseInt()
proc readInt2() : (int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1])
proc readInt3() : (int, int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1], v[2])

#------------------------------------------------------------------------------#

type Person = tuple[y : int, x : int, a : int]

proc `<`(p1 : Person, p2 : Person) : bool =
    if p1.a != p2.a:
        return p1.a < p2.a
    if p1.y != p1.y:
        return p1.y < p2.y
    return p1.x < p2.x

proc solve(n : int, m : int, g : seq[seq[int]]) : int =
    var x = newSeq[Person](n * m)
    for i in 0..<n:
        for j in 0..<m:
            x[i * m + j] = (i, j, g[i][j])
    x.sort(system.cmp[Person])

    var ans = 0

    for i in 0..<n:
        let s = x[i * m].a
        let t = x[i * m + (m - 1)].a

        var p : seq[int] = @[]
        for j in 0..<m:
            if s <= g[i][j] and g[i][j] <= t:
                continue
            p.add(j)

        var q : seq[int] = @[]
        for j in 0..<m:
            if x[i * m + j].y == i:
                continue
            ans += abs(i - x[i * m + j].y)
            q.add(x[i * m + j].x)

        q.sort(system.cmp[int])
        for t in zip(p, q):
            ans += abs(t.a - t.b)

    return ans

proc nextRand(x : int, a : int, p : int) : int =
    return (x + a) mod p

proc main() : void =
    let (n, m) = readInt2()
    let (x0, a, p) = readInt3()

    var g : seq[seq[int]] = repeat(newSeq[int](m), n)
    g[0][0] = x0;
    for i in 1..<(n * m):
        let j = i - 1
        g[i div m][i mod m] = nextRand(g[j div m][j mod m], a, p)

    if n == 1 and m == 1:
        echo 0
    elif a mod p == 0:
        if x0 == nextRand(x0, a, p):
            echo 0
        else:
            echo 2 * (n - 1)
    else:
        echo solve(n, m, g)

main()

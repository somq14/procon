import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import logging
import future

const INF* = int(1e18 + 373)

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

proc readInt1*(): int = readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#
type Range = tuple [ ll, rr: int ]

proc `<`(r1, r2: Range): bool =
  r1.rr < r2.rr

proc main() =
  let (n, m, q) = readInt3()

  var rs = newSeq[Range](m)
  for i in 0..<m:
    let (ll, rr) = readInt2()
    rs[i] = (ll - 1, rr - 1)

  var qs = newSeq[Range](q)
  for i in 0..<q:
    let (pp, qq) = readInt2()
    qs[i] = (pp - 1, qq - 1)

  var dp = newSeq2[int](n + 1, n + 1)
  for s in 0..<n:
    for i in 0..<m:
      if rs[i].ll >= s:
        dp[s][rs[i].rr] += 1

    for t in 0..<n:
      dp[s][t + 1] += dp[s][t]

  for i in 0..<q:
    echo dp[qs[i].ll][qs[i].rr]

main()


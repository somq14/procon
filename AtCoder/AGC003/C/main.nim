import strutils
import sequtils
import algorithm
import math
import queues
import tables
import logging
import future

const INF* = int(1e18 + 373)

when not defined(release):
  addHandler(newFileLogger(stderr, lvlAll, "[ $levelname ] "))

proc readInt1*(): int = stdin.readLine().strip().parseInt()
proc readInt2*(): (int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1])
proc readInt3*(): (int, int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1], v[2])

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#
proc compress[T](a: seq[T]): Table[T, int] =
  result = initTable[T, int]()
  for e in a:
    if e notin result:
      result[e] = result.len()

proc solve(n: int; a: seq[int]): int =
  debug a
  var ans = 0
  for i in 0..<n:
    if i mod 2 == 0 and a[i] mod 2 != 0:
      ans += 1
  return ans

proc main() =
  let n = readInt1()
  let s = readSeq(n).map(parseInt).map(it => it - 1)

  let t = s.sorted(cmp[int]).compress()
  echo solve(n, s.map(it => t[it]))

main()


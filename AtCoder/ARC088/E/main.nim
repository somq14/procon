import strutils
import sequtils
import algorithm
import math
import queues
import tables
import logging
import future

const INF* = int(1e18 + 373)

when not defined(release):
  addHandler(newFileLogger(stderr, lvlAll, "[ $levelname ] "))

proc readInt1*(): int = stdin.readLine().strip().parseInt()
proc readInt2*(): (int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1])
proc readInt3*(): (int, int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1], v[2])

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#
#------------------------------------------------------------------------------#
type Op2[T] = proc(a, b: T): T
type SegmentTree[T] = tuple[ n: int, a: seq[T], e: T, f: Op2[T] ]

proc initSegmentTree[T](n: int, e: T, f: Op2[T]): SegmentTree[T] =
    var m = 1
    while m < n:
        m *= 2

    var a = newSeq[T](2 * m)
    a.fill(e)
    return (m, a, e, f)

proc update[T](this: var SegmentTree[T], i: int, x: T) =
    this.a[this.n + i] = x
    var j = (this.n + i) div 2
    while j > 0:
        this.a[j] = this.f(this.a[j * 2], this.a[j * 2 + 1])
        j = j div 2

proc query[T](this: SegmentTree[T], s, t, i, ll, hh: int): T =
    if t <= ll or hh <= s:
        return this.e
    if s <= ll and hh <= t:
        return this.a[i]

    let m = (ll + hh) div 2
    let vl = this.query(s, t, i * 2, ll, m)
    let vh = this.query(s, t, i * 2 + 1, m, hh)
    return this.f(vl, vh)

proc query[T](this: SegmentTree[T], s, t: int): T = query(this, s, t, 1, 0, this.n)

#------------------------------------------------------------------------------#
#------------------------------------------------------------------------------#

proc compress[T](a: seq[T]): Table[T, int] =
  result = initTable[T, int]()
  for e in a:
    if e notin result:
      result[e] = result.len()

proc uniqued[T](a: seq[T]): seq[T] =
  if a.len() == 0:
    return @[]

  var a = a.sorted(cmp[T])
  result = @[ a[0] ]
  for e in a:
    if a[a.len() - 1] == e:
      continue
    result.add(e)

# require segment tree
proc numberOfInversion(a: seq[int]): int =
  let n = a.len()

  var ind = newSeq[int](n)
  for i in 0..<n:
    ind[a[i]] = i

  result = 0
  var segtree = initSegmentTree[int](n, 0, (x0: int, x1: int) => x0 + x1)
  for i in 0..<n:
    result += segtree.query(ind[i], n)
    segtree.update(ind[i], 1)

proc numberOfInversion[T](a, b: seq[T]): int =
  assert a.len() == b.len()
  assert a.sorted(cmp[T]) == b.sorted(cmp[T])

  let t = b.compress()
  let c = a.map((it: T) => t[it])
  return c.numberOfInversion()

#------------------------------------------------------------------------------#

proc main() =
  let s = stdin.readLine().map(it => it.ord() - 'a'.ord())
  let n = s.len()

  let m = 'z'.ord() - 'a'.ord() + 1
  var ind = newSeq2[int](m, 0)
  for i in 0..<n:
    ind[s[i]].add(i)

  if ind.filter(it => it.len() mod 2 != 0).len() > 1:
    echo "-1"
    return

  var t = newSeq[int](n)
  var id = 1
  for p in ind:
    if p.len() mod 2 != 0:
      t[p[p.len() div 2]] = 0

    var head = 0
    var tail = p.len() - 1
    for j in 0..<p.len() div 2:
      t[p[head]] = -id
      t[p[tail]] = id
      head += 1
      tail -= 1
      id += 1

  let x0 = t.filter(it => it < 0)
  let x1 = t.filter(it => it == 0)
  let x2 = t.filter(it => it > 0)
  let x = x0 & x1 & x2

  debug t
  debug x
  let ans = numberOfInversion(t, x) +
    numberOfInversion(x0.map(it => abs(it)), x2.map(it => abs(it)).reversed())
  echo ans


main()


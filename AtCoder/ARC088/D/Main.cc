#include <algorithm>
#include <complex>
#include <iostream>
#include <queue>
#include <set>
#include <stack>
#include <string>
#include <vector>

using std::cin;
using std::cout;
using std::endl;
using std::cerr;

using std::string;
using std::to_string;
using std::vector;
using std::set;
using std::queue;
using std::stack;
using std::priority_queue;

using std::min;
using std::max;
using std::sort;
using std::abs;

typedef long long int ll;
const int MOD = 1e9 + 7;

int main() {
    string s;
    cin >> s;

    int n = s.length();

    if (s[n / 2] == '1') {
        for (int i = 0; i < n; i++) {
            s[i] = s[i] == '0' ? '1' : '0';
        }
    }

    string a0 = s.substr(0, n / 2);
    std::reverse(a0.begin(), a0.end());

    string a1 = s.substr(n - n / 2, n);

    //cout << a0 << endl;
    //cout << a1 << endl;

    int p0 = n / 2;
    for (int i = 0; i < n / 2; i++) {
        if (a0[i] == '1') {
            p0 = i;
            break;
        }
    }

    int p1 = n / 2;
    for (int i = 0; i < n / 2; i++) {
        if (a1[i] == '1') {
            p1 = i;
            break;
        }
    }

    int p = min(p0, p1);
    int h = n / 2 - p;
    cout << n - h << endl;

    return 0;
}

import strutils
import sequtils
import algorithm
import math
import queues
import tables
import logging
import future

const INF* = int(1e18 + 373)

when not defined(release):
  addHandler(newFileLogger(stderr, lvlAll, "[ $levelname ] "))

proc readInt1*(): int = stdin.readLine().strip().parseInt()
proc readInt2*(): (int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1])
proc readInt3*(): (int, int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1], v[2])

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#
proc solve(n: int; a: seq[int]; m: int): seq[int] =
  var p = -1;
  for i in 0..<n - 1:
    if a[i] + a[i + 1] >= m:
      p = i + 1
      break

  if p == -1:
    return @[]

  var ans = @[ p ]
  for i in p + 1..<n:
    ans.add(i)
  for i in countdown(p - 1, 1):
    ans.add(i)

  return ans.reversed()

proc main() =
  let (n, m) = readInt2()
  let a = readSeq().map(parseInt)

  let ans = solve(n, a, m)
  echo if ans.len() == 0: "Impossible" else: "Possible"
  for e in ans:
    echo e

main()


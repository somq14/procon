import strutils
import sequtils
import algorithm
import math
import queues
import tables
import logging
import future

const INF* = int(1e18 + 373)

when not defined(release):
  addHandler(newFileLogger(stderr, lvlAll, "[ $levelname ] "))

proc readInt1*(): int = stdin.readLine().strip().parseInt()
proc readInt2*(): (int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1])
proc readInt3*(): (int, int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1], v[2])

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#

proc main() =
  let (n, m) = readInt2()
  var x = newSeq[int](m)
  var y = newSeq[int](m)
  for i in 0..<m:
    (x[i], y[i]) = readInt2()
    x[i] -= 1
    y[i] -= 1

  var possible = newSeq[bool](n)
  possible[0] = true
  var amount = newSeq[int](n)
  amount.fill(1)

  for i in 0..<m:
    if not possible[x[i]]:
      amount[x[i]] -= 1
      amount[y[i]] += 1
      continue

    amount[x[i]] -= 1
    if amount[x[i]] == 0:
      possible[x[i]] = false

    amount[y[i]] += 1
    possible[y[i]] = true

  echo possible.filter(it => it).len()

main()


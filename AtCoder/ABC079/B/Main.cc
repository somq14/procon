#include <iostream>
#include <vector>

using std::cin;
using std::cout;
using std::endl;

using std::vector;

int main(){
    int n;
    cin >> n;

    vector<long> a(87);

    a[0] = 2;
    a[1] = 1;
    for(int i = 2; i <= 86; i++){
        a[i] = a[i-1] + a[i-2];
    }

    cout << a[n] << endl;

    return 0;
}


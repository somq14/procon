#include <iostream>
#include <vector>

using std::cin;
using std::cout;
using std::endl;

using std::vector;

int main(){
    int n;
    cin >> n;

    int a, b, c, d;
    d = n % 10;
    c = (n / 10) % 10;
    b = (n / 100) % 10;
    a = (n / 1000) % 10;

    if (a + b + c + d == 7)
        cout << a << "+" << b << "+" << c << "+" << d << "=7" << endl;
    else if (a + b + c - d == 7)
        cout << a << "+" << b << "+" << c << "-" << d << "=7" << endl;
    else if (a + b - c + d == 7)
        cout << a << "+" << b << "-" << c << "+" << d << "=7" << endl;
    else if (a + b - c - d == 7)
        cout << a << "+" << b << "-" << c << "-" << d << "=7" << endl;
    else if (a - b + c + d == 7)
        cout << a << "-" << b << "+" << c << "+" << d << "=7" << endl;
    else if (a - b + c - d == 7)
        cout << a << "-" << b << "+" << c << "-" << d << "=7" << endl;
    else if (a - b - c + d == 7)
        cout << a << "-" << b << "-" << c << "+" << d << "=7" << endl;
    else if (a - b - c - d == 7)
        cout << a << "-" << b << "-" << c << "-" << d << "=7" << endl;

    return 0;
}

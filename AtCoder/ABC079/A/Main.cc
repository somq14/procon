#include <iostream>
#include <vector>

using std::cin;
using std::cout;
using std::endl;

using std::vector;

int main(){
    int n;
    cin >> n;

    int a[4];
    a[0] = n % 10;
    a[1] = (n / 10) % 10;
    a[2] = (n / 100) % 10;
    a[3] = (n / 1000) % 10;

    bool good = (a[0] == a[1] && a[1] == a[2]) || (a[1] == a[2] && a[2] == a[3]);
    cout << (good ? "Yes" : "No") << endl;

    return 0;
}


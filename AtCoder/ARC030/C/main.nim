import strutils
import sequtils
import algorithm
import math
import queues
import tables
import future
import sets

const INF* = int(1e18 + 373)

proc readInt1*(): int = stdin.readLine().strip().parseInt()
proc readInt2*(): (int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1])
proc readInt3*(): (int, int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1], v[2])

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#

proc sccDfs1(n: int; g: seq2[int]; v: int; visited: var seq[bool]; stack: var seq[int]) =
  visited[v] = true

  for u in g[v]:
    if not visited[u]:
      sccDfs1(n, g, u, visited, stack)

  stack.add(v)

proc sccDfs2(n: int; g: seq2[int]; v, compId: int; comp: var seq[int]) =
  comp[v] = compId
  for u in g[v]:
    if comp[u] == -1:
      sccDfs2(n, g, u, compId, comp)

proc stronglyConnectedComponent(n: int; g: seq2[int]): seq[int] =
  var stack = newSeq[int](0)

  var visited = newSeq[bool](n)
  for v in 0..<n:
    if not visited[v]:
      sccDfs1(n, g, v, visited, stack)

  var gInv = newSeqWith(n, newSeq[int](0))
  for v in 0..<n:
    for u in g[v]:
      gInv[u].add(v)

  var compId = 0
  var comp = newSeq[int](n)
  comp.fill(-1)

  while stack.len() > 0:
    let v = stack.pop()
    if comp[v] == -1:
      sccDfs2(n, gInv, v, compId, comp)
      compId += 1

  return comp

proc uniqued*[T](a: seq[T]): seq[T] =
  var s = initSet[T]()
  for e in a:
    s.incl(e)

  result = newSeq[T](0)
  for e in s:
    result.add(e)

#------------------------------------------------------------------------------#
proc solve(n, k: int; g: seq2[int]; c: seq2[char]; v: int; memo: var seq2[string]) =
  if memo[v] != nil:
    return

  for u in g[v]:
    solve(n, k, g, c, u, memo)

  let s = c[v].map((c: char) => $c).join()
  memo[v] = newSeq[string](k + 1)

  for i in 0..min(s.len(), k):
    memo[v][i] = s[0..<i]

  for i in 0..k:
    # calc memo[v][i]
    for j in 0..min(i, s.len()):
      for u in g[v]:
        if memo[u][i - j] == nil:
          continue
        let t = s[0..<j] & memo[u][i - j]
        if memo[v][i] == nil or t < memo[v][i]:
          memo[v][i] = t

proc main() =
  let (n, m, k) = readInt3()
  let c = readSeq().map((s: string) => s[0])

  var g = newSeqWith(n, newSeq[int](0))
  for i in 0..<m:
    let (a, b) = readInt2()
    g[a - 1].add(b - 1)

  let comp = stronglyConnectedComponent(n, g)

  let cn = max(comp) + 1
  var cg = newSeqWith(cn, newSeq[int](0))
  for v in 0..<n:
    for u in g[v]:
      if comp[v] == comp[u]:
        continue
      cg[comp[v]].add(comp[u])
  for v in 0..<cn:
    cg[v] = cg[v].uniqued()

  var cc = newSeqWith(cn, newSeq[char](0))
  for v in 0..<n:
    cc[comp[v]].add(c[v])
  for v in 0..<cn:
    cc[v].sort(cmp[char])

  var memo = newSeq[seq[string]](cn)
  for v in 0..<cn:
    solve(cn, k, cg, cc, v, memo)

  var ans: string = nil
  for v in 0..<cn:
    if memo[v][k] == nil:
      continue
    if ans == nil or memo[v][k] < ans:
      ans = memo[v][k]

  echo if ans == nil: "-1" else: ans

main()


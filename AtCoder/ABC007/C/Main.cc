#include <algorithm>
#include <cmath>
#include <complex>
#include <iomanip>
#include <iostream>
#include <queue>
#include <set>
#include <stack>
#include <string>
#include <utility>
#include <vector>

using std::cin;
using std::cout;
using std::endl;
using std::cerr;

using std::string;
using std::to_string;
using std::vector;
using std::set;
using std::queue;
using std::stack;
using std::priority_queue;
using std::pair;
using std::make_pair;

using std::min;
using std::max;
using std::sort;
using std::abs;

using std::fixed;
using std::setprecision;
using std::setw;

typedef long long int ll;
const int MOD = 1e9 + 7;
const int INF = 1e9 + 314;

class point {
   public:
    int y;
    int x;
    point() : y(0), x(0) {}
    point(int y, int x) : y(y), x(x) {}
};

int solve(int r, int c, const vector<string>& a, const point& s,
          const point& g) {
    static const vector<int> dy = {0, 0, 1, -1};
    static const vector<int> dx = {1, -1, 0, 0};

    vector<vector<bool>> visited(r, vector<bool>(c));

    queue<pair<point, int>> q;
    q.push(make_pair(s, 0));

    while (!q.empty()) {
        const point& v = q.front().first;
        const int d = q.front().second;
        q.pop();

        // cout << "(" << v.x << "," << v.y << ")" << "->" << d << endl;

        if (v.y == g.y && v.x == g.x) {
            return d;
        }

        if (visited[v.y][v.x]) {
            continue;
        }
        visited[v.y][v.x] = true;

        for (int i = 0; i < 4; i++) {
            const int y = v.y + dy[i];
            const int x = v.x + dx[i];
            if (!(0 <= y && y < r && 0 <= x && x < c)) {
                continue;
            }
            if (a[y][x] == '.' && !visited[y][x]) {
                q.push(make_pair(point(y, x), d + 1));
            }
        }
    }

    return -1;
}

int main() {
    int r, c;
    cin >> r >> c;

    point s, g;
    cin >> s.y >> s.x;
    cin >> g.y >> g.x;
    s.y--;
    s.x--;
    g.y--;
    g.x--;

    vector<string> a(r);
    for (int i = 0; i < r; i++) {
        cin >> a[i];
    }

    cout << solve(r, c, a, s, g) << endl;

    return 0;
}

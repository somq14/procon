import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import logging
import future

const INF* = int(1e18 + 373)

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

proc readInt1*(): int = readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#

proc main() =
  let (h, w) = readInt2()
  var a = newSeq[string](h)
  for i in 0..<h:
    a[i] = stdin.readLine()

  var removedRow = newSeq[bool](h)
  for i in 0..<h:
    var all = true
    for j in 0..<w:
      if a[i][j] == '#':
        all = false
        break
    removedRow[i] = all

  var removedCol = newSeq[bool](w)
  for j in 0..<w:
    var all = true
    for i in 0..<h:
      if a[i][j] == '#':
        all = false
        break
    removedCol[j] = all

  for i in 0..<h:
    if removedRow[i]:
      continue
    for j in 0..<w:
      if removedCol[j]:
        continue
      stdout.write(a[i][j])
    echo ""

main()


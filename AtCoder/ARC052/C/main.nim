import strutils
import sequtils
import algorithm
import math
import queues
import tables
import logging
import future

const INF* = int(1e18 + 373)

when not defined(release):
  addHandler(newFileLogger(stderr, lvlAll, "[ $levelname ] "))

proc readInt1*(): int = stdin.readLine().strip().parseInt()
proc readInt2*(): (int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1])
proc readInt3*(): (int, int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1], v[2])

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

# FIXME: Use object
type PriorityQueue[T] = tuple[a: seq[T], n: Natural, cmp: proc(a, b: T): bool]

proc initPriorityQueue[T](cmp: proc(a, b: T): bool): PriorityQueue[T] =
    (newSeq[T](1), Natural(0), cmp)

proc len[T](this: PriorityQueue[T]): Natural = this.n

proc enqueue[T](this: var PriorityQueue[T], e: T) =
    this.n += 1
    this.a.add(e)
    var i = this.a.len() - 1
    while i > 1 and this.cmp(this.a[i], this.a[i div 2]):
        swap(this.a[i], this.a[i div 2])
        i = i div 2

proc dequeue[T](this: var PriorityQueue[T]): T =
    this.n -= 1
    result = this.a[1]
    this.a[1] = this.a.pop()
    var i = 1
    while i < this.a.len():
        var j = i
        if i * 2 < this.a.len() and this.cmp(this.a[i * 2], this.a[j]):
            j = i * 2
        if i * 2 + 1 < this.a.len() and this.cmp(this.a[i * 2 + 1], this.a[j]):
            j = i * 2 + 1
        if i == j:
            break
        swap(this.a[i], this.a[j])
        i = j

# FIXME: throw Exception when this is empty
proc front[T](this: PriorityQueue[T]): T = this.a[1]

#------------------------------------------------------------------------------#
type P = tuple[cost, used, v: int]

proc main() =
  let (n, m) = readInt2()

  var ga = newSeq2[int](n, 0)
  var gb = newSeq2[int](n, 0)
  for i in 0..<m:
    let (c, a, b) = readInt3()
    case c:
    of 0:
      ga[a].add(b)
      ga[b].add(a)
    of 1:
      gb[a].add(b)
      gb[b].add(a)
    else:
      discard

  let f = (a: P, b: P) => (a < b)
  var q = initPriorityQueue[P](f)
  q.enqueue((0, 0, 0))

  var d = newSeqWith(n, initTable[int, int]())
  d[0][0] = 0

  while q.len() > 0:
    let (cost, used, v) = q.dequeue()

    if cost > d[v][used]:
      continue

    for u in ga[v]:
      block aEdgeProcess:
        for t in d[u].pairs():
          if t[0] <= used and t[1] <= cost + 1:
            break aEdgeProcess
        d[u][used] = cost + 1
        q.enqueue((cost + 1, used, u))

    for u in gb[v]:
      block bEdgeProcess:
        for t in d[u].pairs():
          if t[0] <= used + 1 and t[1] <= cost + 1 + used:
            break bEdgeProcess
        d[u][used + 1] = cost + 1 + used
        q.enqueue((cost + 1 + used, used + 1, u))

  for v in 0..<n:
    var ans = INF
    for c in d[v].values():
      ans = min(ans, c)
    echo ans


main()


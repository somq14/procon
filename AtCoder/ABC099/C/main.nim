import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import logging
import future

const INF* = int(1e18 + 373)

when not defined(release):
  addHandler(newFileLogger(stderr, lvlAll, "[ $levelname ] "))

proc readInt1*(): int = stdin.readLine().strip().parseInt()
proc readInt2*(): (int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1])
proc readInt3*(): (int, int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1], v[2])
proc readInt4*(): (int, int, int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1], v[2], v[3])

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#

proc main() =
  let n = readInt1()

  var dp = newSeq[int](n + 1)
  dp.fill(INF)
  dp[0] = 0

  var x = @[ 1 ]

  var pow6 = 6
  for i in 1..<1000:
    if pow6 > n:
      break
    x.add(pow6)
    pow6 *= 6

  var pow9 = 9
  for i in 1..<1000:
    if pow9 > n:
      break
    x.add(pow9)
    pow9 *= 9

  debug x
  for i in 1..n:
    for v in x:
      if v > i:
        continue
      dp[i] = min(dp[i], dp[i - v] + 1)

  echo dp[n]

main()


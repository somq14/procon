import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import logging
import future

const INF* = int(1e18 + 373)

when not defined(release):
  addHandler(newFileLogger(stderr, lvlAll, "[ $levelname ] "))

proc readInt1*(): int = stdin.readLine().strip().parseInt()
proc readInt2*(): (int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1])
proc readInt3*(): (int, int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1], v[2])
proc readInt4*(): (int, int, int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1], v[2], v[3])

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#
proc eval(ds: seq2[int]; cnt: seq[int]; k: int): int =
  let c = cnt.len()

  var ans = 0
  for i in 0..<c:
    if i == k:
      continue
    ans += ds[i][k] * cnt[i]
  return ans

proc main() =
  let (n, c) = readInt2()

  var ds = newSeq2[int](c, 0)
  for i in 0..<c:
    ds[i] = readSeq().map(parseInt)

  var cs = newSeq2[int](n, 0)
  for i in 0..<n:
    cs[i] = readSeq().map(parseInt).map(it => it - 1)

  debug ds
  debug cs

  var cnt = newSeq2[int](3, c)
  for y in 0..<n:
    for x in 0..<n:
      cnt[(y + x) mod 3][cs[y][x]] += 1

  var ans = INF
  for c0 in 0..<c:
    for c1 in 0..<c:
      for c2 in 0..<c:
        if c0 == c1 or c1 == c2 or c2 == c0:
          continue
        var sum = 0
        sum += eval(ds, cnt[0], c0)
        sum += eval(ds, cnt[1], c1)
        sum += eval(ds, cnt[2], c2)
        ans = min(ans, sum)

  echo ans

main()


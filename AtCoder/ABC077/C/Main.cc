#include <algorithm>
#include <iostream>
#include <vector>

using namespace std;

int main() {
    int n;
    vector<int> a;
    vector<int> b;
    vector<int> c;

    cin >> n;
    for (int i = 0; i < n; i++) {
        int tmp;
        cin >> tmp;
        a.push_back(tmp);
    }
    for (int i = 0; i < n; i++) {
        int tmp;
        cin >> tmp;
        b.push_back(tmp);
    }
    for (int i = 0; i < n; i++) {
        int tmp;
        cin >> tmp;
        c.push_back(tmp);
    }

    sort(a.begin(), a.end());
    sort(b.begin(), b.end());
    sort(c.begin(), c.end());

    long count = 0;
    for (int i = 0; i < n; i++) {
        long a_num = lower_bound(a.begin(), a.end(), b[i]) - a.begin();
        long c_num = c.end() - upper_bound(c.begin(), c.end(), b[i]);
        count += a_num * c_num;
    }

    cout << count << endl;

    return 0;
}

#include <algorithm>
#include <cmath>
#include <complex>
#include <iomanip>
#include <iostream>
#include <queue>
#include <set>
#include <stack>
#include <string>
#include <utility>
#include <vector>

using std::cin;
using std::cout;
using std::endl;
using std::cerr;

using std::string;
using std::to_string;
using std::vector;
using std::set;
using std::queue;
using std::stack;
using std::priority_queue;
using std::pair;
using std::make_pair;

using std::min;
using std::max;
using std::sort;
using std::abs;

using std::fixed;
using std::setprecision;
using std::setw;

typedef long long int ll;
const int MOD = 1e9 + 7;
const int INF = 1e9 + 314;

int main() {
    int k;
    cin >> k;

    typedef pair<int, int> P;
    priority_queue<P, vector<P>, std::greater<P>> q;

    vector<int> d(k, INF);
    for (int i = 1; i <= min(9, k - 1); i++) {
        d[i % k] = i % k;
        q.push({d[i % k], i % k});
    }

    while (not q.empty()) {
        const P tmp = q.top();
        const int c = tmp.first;
        const int v = tmp.second;
        q.pop();

        if (c > d[v]) {
            continue;
        }

        for (int i = 0; i <= 9; i++) {
            const int u = (10 * v + i) % k;
            if (d[v] + i < d[u]) {
                d[u] = d[v] + i;
                cerr << "update " << v << " " << u << " " << d[u] << endl;
                q.push({d[u], u});
            }
        }
    }

    cout << d[0] << endl;

    return 0;
}

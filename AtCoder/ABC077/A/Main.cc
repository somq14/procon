#include <iostream>

using namespace std;

int main(){
    string c0;
    string c1;

    cin >> c0 >> c1;

    bool ans =
        (c0[0] == c1[2]) && (c0[1] == c1[1]) && (c0[2] == c1[0]) &&
        (c1[0] == c0[2]) && (c1[1] == c0[1]) && (c1[2] == c0[0]) ;

    cout << (ans ? "YES" : "NO") << endl;

    return 0;
}

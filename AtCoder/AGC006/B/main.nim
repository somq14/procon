import strutils
import sequtils
import algorithm
import math
import queues
import tables
import logging
import future

const INF* = int(1e18 + 373)

when not defined(release):
  addHandler(newFileLogger(stderr, lvlAll, "[ $levelname ] "))

proc readInt1*(): int = stdin.readLine().strip().parseInt()
proc readInt2*(): (int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1])
proc readInt3*(): (int, int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1], v[2])

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#

proc main() =
  let (n, x) = readInt2()
  if x == 1 or x == 2 * n - 1:
    echo "No"
    return

  echo "Yes"

  if n == 2:
    echo "1"
    echo "2"
    echo "3"
    return

  var ans = newSeq[int](2 * n - 1)
  ans.fill(-1)

  let mid = n - 1
  ans[mid] = x
  ans[mid - 1] = x - 1
  ans[mid + 1] = x + 1

  if x - 2 >= 1:
    ans[mid + 2] = x - 2

  if x + 2 <= 2 * n - 1:
    ans[mid - 2] = x + 2

  var notUsed = newSeq[int](0)
  for i in 1..<(x - 2):
    notUsed.add(i)
  for i in (x + 2) + 1..2 * n - 1:
    notUsed.add(i)

  for i in countdown((2 * n - 1) - 1, 0):
    if ans[i] == -1:
      ans[i] = notUsed.pop()

  for i in 0..<2 * n - 1:
    echo ans[i]

main()


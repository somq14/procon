import strutils
import sequtils
import algorithm
import math
import queues
import tables
import logging
import future

const INF* = int(1e18 + 373)

when not defined(release):
  addHandler(newFileLogger(stderr, lvlAll, "[ $levelname ] "))

proc readInt1*(): int = stdin.readLine().strip().parseInt()
proc readInt2*(): (int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1])
proc readInt3*(): (int, int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1], v[2])

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#
const MOD = int(1e9 + 7)

type Op1[T] = proc(a: T, b: T): T
type Op2[T] = proc(a: T, b: T): T

proc repeatedSquares[T](x: T, n: Natural, e: T, f: Op2[T]): T =
    result = e
    var xx = f(x, e)
    var i = n
    while i > 0:
        if (i and 1) != 0:
            result = f(result, xx)
        xx = f(xx, xx)
        i = i shr 1

#------------------------------------------------------------------------------#
proc concat(p1, p2: seq[int]): seq[int] =
  let n = p1.len()
  result = newSeq[int](n)
  for i in 0..<n:
    result[i] = p1[p2[i]]

proc main() =
  let n = readInt1()
  var x = readSeq().map(it => it.parseInt())
  let (m, k) = readInt2()
  let a = readSeq().map(parseInt).map(it => it - 1)

  var p = newSeq[int](n - 1)
  var e = newSeq[int](n - 1)
  for i in 0..<n - 1:
    p[i] = i
    e[i] = i

  for i in 0..<m:
    swap(p[a[i]], p[a[i] - 1])

  let pp = repeatedSquares[seq[int]](p, k, e, concat)

  var d = newSeq[int](n)
  d[0] = x[0]
  for i in 0..<n - 1:
    d[i + 1] = x[pp[i] + 1] - x[pp[i]]

  echo d[0]
  for i in 1..<n:
    d[i] += d[i - 1]
    echo d[i]



main()


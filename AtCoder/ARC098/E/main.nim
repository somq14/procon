import strutils
import sequtils
import algorithm
import math
import queues
import tables
import logging
import future

const INF* = int(1e18 + 373)

when not defined(release):
  addHandler(newFileLogger(stderr, lvlAll, "[ $levelname ] "))

proc readInt1*(): int = stdin.readLine().strip().parseInt()
proc readInt2*(): (int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1])
proc readInt3*(): (int, int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1], v[2])

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#
type Range = tuple [s, t: int]

proc solve(n, k, q: int; a: seq[int]; x: int): int =
  var mark = newSeq[bool](n)
  mark.fill(true)

  for i in 0..<n:
    if a[i] < x:
      mark[i] = false

  var rs = newSeq[Range](0)
  var p = 0
  var erasableCount = 0
  while p < n:
    if not mark[p]:
      p += 1
      continue
    let s = p
    while p < n and mark[p]:
      p += 1
    let t = p

    if t - s >= k:
      rs.add((s, t))
      erasableCount += t - s - k + 1

  if erasableCount < q:
    return INF

  var candi = newSeq[int](0)
  for r in rs:
    let c = (r.t - r.s) - k + 1
    let b = a[r.s..<r.t].sorted(cmp[int])
    for i in 0..<c:
      candi.add(b[i])

  candi.sort(cmp[int])
  return candi[q - 1] - candi[0]

proc main() =
  let (n, k, q) = readInt3()
  let a = readSeq().map(parseInt)

  var ans = INF
  for i in 0..<n:
    ans = min(ans, solve(n, k, q, a, a[i]))
  echo ans



main()


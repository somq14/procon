import strutils
import sequtils
import algorithm
import math
import queues
import tables
import logging
import future

const INF* = int(1e18 + 373)

when not defined(release):
  addHandler(newFileLogger(stderr, lvlAll, "[ $levelname ] "))

proc readInt1*(): int = stdin.readLine().strip().parseInt()
proc readInt2*(): (int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1])
proc readInt3*(): (int, int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1], v[2])

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#

proc main() =
  let n = readInt1()
  let s = stdin.readLine()

  var sumOfW = newSeq[int](n + 1)
  for i in 1..n:
    sumOfW[i] = sumOfW[i - 1]
    if s[i - 1] == 'W':
      sumOfW[i] += 1

  var sumOfE = newSeq[int](n + 1)
  for i in 1..n:
    sumOfE[i] = sumOfE[i - 1]
    if s[i - 1] == 'E':
      sumOfE[i] += 1

  var ans = n
  for i in 0..<n:
    let v = sumOfW[i] + (sumOfE[n] - sumOfE[i + 1])
    ans = min(ans, v)

  echo ans


main()


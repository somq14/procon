#include <cmath>
#include <iostream>
#include <vector>
#include <algorithm>

using std::cin;
using std::cout;
using std::endl;

using std::vector;

using std::abs;
using std::min;
using std::max;

int main() {
    int n, z, w;
    cin >> n >> z >> w;

    vector<int> a(n);
    for (int i = 0; i < n; i++) {
        cin >> a[i];
    }
    a.push_back(z);
    a.push_back(w);

    vector<vector<vector<int>>> dp(
        2, vector<vector<int>>(n, vector<int>(n + 2, 0)));

    for (int t = 0; t <= 1; t++) {
        for (int j = 0; j < n + 2; j++) {
            dp[t][n - 1][j] = abs(a[n - 1] - a[j]);
        }
    }

    for (int i = n - 2; i >= 0; i--) {
        for (int t = 0; t <= 1; t++) {
            int tmp_max = dp[1][i+1][i];
            for (int k = i; k < n - 1; k++) {
                tmp_max= max(tmp_max, dp[1][k+1][k]);
            }
            int tmp_min = dp[0][i+1][i];
            for (int k = i; k < n - 1; k++) {
                tmp_min = min(tmp_min, dp[0][k+1][k]);
            }
            for (int j = 0; j < n + 2; j++) {
                int m;
                if (t == 0) {
                    m = max(tmp_max, abs(a[n - 1] - a[j]));
                } else {
                    m = min(tmp_min, abs(a[n - 1] - a[j]));
                }
                dp[t][i][j] = m;
            }
        }
    }

    cout << dp[0][0][n+1] << endl;
    return 0;
}

#include <iostream>
#include <vector>

using std::cin;
using std::cout;
using std::endl;

using std::vector;

int main(){
    char x, y;
    cin >> x >> y;
    char ans;
    if(x < y){
        ans = '<';
    }else if(x > y){
        ans = '>';
    }else{
        ans = '=';
    }
    cout << ans << endl;
    return 0;
}


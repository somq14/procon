#include <iostream>
#include <vector>

using std::cin;
using std::cout;
using std::endl;

using std::vector;

int main() {
    int m, n;
    cin >> n >> m;

    const int t = 1900 * m + 100 * (n - m);
    cout << t * (1 << m) << endl;

    return 0;
}

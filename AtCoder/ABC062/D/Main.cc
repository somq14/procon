#include <algorithm>
#include <complex>
#include <iomanip>
#include <iostream>
#include <queue>
#include <set>
#include <string>
#include <vector>

using std::cin;
using std::cout;
using std::endl;

using std::string;
using std::to_string;
using std::vector;
using std::set;
using std::queue;
using std::priority_queue;

using std::min;
using std::max;
using std::sort;
using std::abs;

typedef long long ll;

int main(void) {
    int n;
    cin >> n;

    vector<int> left(n);
    vector<int> mid(n);
    vector<int> right(n);
    for (int i = 0; i < n; i++) {
        cin >> left[i];
    }
    for (int i = 0; i < n; i++) {
        cin >> mid[i];
    }
    for (int i = 0; i < n; i++) {
        cin >> right[i];
    }

    vector<ll> lmemo(n + 1);
    priority_queue<int, vector<int>, std::greater<int>> lq;
    ll lsum = 0;
    for (int i = 0; i < n; i++) {
        lsum += left[i];
        lq.push(left[i]);
    }

    lmemo[0] = lsum;
    for (int i = 0; i < n; i++) {
        if (mid[i] > lq.top()) {
            lsum -= lq.top();
            lq.pop();
            lsum += mid[i];
            lq.push(mid[i]);
        }
        lmemo[i + 1] = lsum;
    }

    vector<ll> rmemo(n + 1);
    priority_queue<int, vector<int>, std::less<int>> rq;
    ll rsum = 0;
    for (int i = 0; i < n; i++) {
        rsum += right[i];
        rq.push(right[i]);
    }

    rmemo[0] = rsum;
    for (int i = 0; i < n; i++) {
        if (mid[n - 1 - i] < rq.top()) {
            rsum -= rq.top();
            rq.pop();
            rsum += mid[n - 1 - i];
            rq.push(mid[n - 1 - i]);
        }
        rmemo[i + 1] = rsum;
    }

    ll ans = lmemo[0] - rmemo[n];
    for (int i = 0; i <= n; i++) {
        ans = max(ans, lmemo[i] - rmemo[n - i]);
    }

    cout << ans << endl;

    return 0;
}

#include <algorithm>
#include <complex>
#include <iomanip>
#include <iostream>
#include <queue>
#include <set>
#include <string>
#include <vector>

using std::cin;
using std::cout;
using std::endl;

using std::string;
using std::to_string;
using std::vector;
using std::set;
using std::queue;
using std::priority_queue;

using std::min;
using std::max;
using std::sort;
using std::abs;

typedef long long ll;

int main(void) {

    ll h, w;
    cin >> h >> w;

    if(h % 3 == 0 || w % 3 == 0){
        cout << 0 << endl;
        return 0;
    }

    ll ans = 1e9;
    ans = min(ans, w);
    ans = min(ans, h);

    for(int i = 1; i < w; i++){
        ll r0 = h * (w - i);
        ll r1 = i * (h / 2);
        ll r2 = i * (h - h / 2);
        ll smax = max(r0, max(r1, r2));
        ll smin = min(r0, min(r1, r2));
        ans = min(ans, smax - smin);
    }

    for(int i = 1; i < h; i++){
        ll r0 = w * (h - i);
        ll r1 = i * (w / 2);
        ll r2 = i * (w - w / 2);
        ll smax = max(r0, max(r1, r2));
        ll smin = min(r0, min(r1, r2));
        ans = min(ans, smax - smin);
    }

    cout << ans << endl;

    return 0;
}

#include <algorithm>
#include <complex>
#include <iomanip>
#include <iostream>
#include <queue>
#include <set>
#include <string>
#include <vector>

using std::cin;
using std::cout;
using std::endl;

using std::string;
using std::to_string;
using std::vector;
using std::set;
using std::queue;
using std::priority_queue;

using std::min;
using std::max;
using std::sort;
using std::abs;

int group_no(int x) {
    switch (x) {
        case 1:
        case 3:
        case 5:
        case 7:
        case 8:
        case 10:
        case 12:
            return 0;
        case 4:
        case 6:
        case 9:
        case 11:
            return 1;
        case 2:
            return 2;
    }
    return -1;
}

int main(void) {
    int x, y;
    cin >> x >> y;
    cout << (group_no(x) == group_no(y) ? "Yes" : "No") << endl;
    return 0;
}

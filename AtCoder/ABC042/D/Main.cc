#include <algorithm>
#include <complex>
#include <iostream>
#include <queue>
#include <set>
#include <stack>
#include <string>
#include <vector>

using std::cin;
using std::cout;
using std::endl;
using std::cerr;

using std::string;
using std::to_string;
using std::vector;
using std::set;
using std::queue;
using std::stack;
using std::priority_queue;

using std::min;
using std::max;
using std::sort;
using std::abs;

typedef long long int ll;
const int MOD = 1e9 + 7;

ll pow_mod(ll x, ll n) {
    ll ans = 1;
    ll xx = x;
    for (ll m = n; m > 0; m >>= 1) {
        if (m & 1) {
            ans = ans * xx % MOD;
        }
        xx = xx * xx % MOD;
    }
    return ans;
}

ll inv_mod(ll n) {
    ll ans = pow_mod(n, MOD - 2);
    return ans;
}

vector<ll> fact_memo(200000 + 1, -1);
ll fact_mod(ll n) {
    if (fact_memo[n] > 0) {
        return fact_memo[n];
    }
    if (n == 0) {
        return fact_memo[0] = 1;
    }
    return fact_memo[n] = n * fact_mod(n - 1) % MOD;
}

ll eval(ll x, ll y) {
    ll ans = fact_mod(x + y);
    ans = ans * inv_mod(fact_mod(x)) % MOD;
    ans = ans * inv_mod(fact_mod(y)) % MOD;
    return ans;
}

int main() {
    int h, w, a, b;
    cin >> h >> w >> a >> b;

    ll ans = 0;
    int x = b;
    int y = h - a - 1;
    while (x < w && y >= 0) {
        ll cnt = (eval(x - 0, y - 0) * eval((w - 1) - x, (h - 1) - y)) % MOD;
        ans = (ans + cnt) % MOD;
        x++;
        y--;
    }

    cout << ans << endl;

    return 0;
}

#include <algorithm>
#include <complex>
#include <iostream>
#include <queue>
#include <set>
#include <stack>
#include <string>
#include <vector>

using std::cin;
using std::cout;
using std::endl;
using std::cerr;

using std::string;
using std::to_string;
using std::vector;
using std::set;
using std::queue;
using std::stack;
using std::priority_queue;

using std::min;
using std::max;
using std::sort;
using std::abs;

typedef long long int ll;
const int MOD = 1e9 + 7;

bool eval(int n, int set) {
    for (int v = n; v > 0; v /= 10) {
        int d = v % 10;
        if (set & (1 << d)) {
            return false;
        }
    }
    return true;
}

int main() {
    int n, k;
    cin >> n >> k;

    int set = 0;
    for (int i = 0; i < k; i++) {
        int d;
        cin >> d;
        set |= 1 << d;
    }

    int ans = -1;
    for (int v = n;; v++) {
        if (eval(v, set)) {
            ans = v;
            break;
        }
    }
    cout << ans << endl;

    return 0;
}

import strutils
import sequtils
import algorithm
import math
import queues
import tables
import logging
import future

const INF* = int(1e18 + 373)

when not defined(release):
  addHandler(newFileLogger(stderr, lvlAll, "[ $levelname ] "))

proc readInt1*(): int = stdin.readLine().strip().parseInt()
proc readInt2*(): (int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1])
proc readInt3*(): (int, int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1], v[2])

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))
# FIXME: Use object
type PriorityQueue[T] = tuple[a: seq[T], n: Natural, cmp: proc(a, b: T): bool]

proc initPriorityQueue[T](cmp: proc(a, b: T): bool): PriorityQueue[T] =
    (newSeq[T](1), Natural(0), cmp)

proc len[T](this: PriorityQueue[T]): Natural = this.n

proc enqueue[T](this: var PriorityQueue[T], e: T) =
    this.n += 1
    this.a.add(e)
    var i = this.a.len() - 1
    while i > 1 and this.cmp(this.a[i], this.a[i div 2]):
        swap(this.a[i], this.a[i div 2])
        i = i div 2

proc dequeue[T](this: var PriorityQueue[T]): T =
    this.n -= 1
    result = this.a[1]
    this.a[1] = this.a.pop()
    var i = 1
    while i < this.a.len():
        var j = i
        if i * 2 < this.a.len() and this.cmp(this.a[i * 2], this.a[j]):
            j = i * 2
        if i * 2 + 1 < this.a.len() and this.cmp(this.a[i * 2 + 1], this.a[j]):
            j = i * 2 + 1
        if i == j:
            break
        swap(this.a[i], this.a[j])
        i = j

# FIXME: throw Exception when this is empty
proc front[T](this: PriorityQueue[T]): T = this.a[1]


proc main() =
  let n = readInt1()
  let a = readSeq().map(parseInt)

  var maxInd = newSeq[int](n)
  maxInd[0] = 0
  for i in 1..<n:
    if a[i] > a[maxInd[i - 1]]:
      maxInd[i] = i
    else:
      maxInd[i] = maxInd[i - 1]

  var p = maxInd[n - 1]
  var pCnt = 1

  var q = initPriorityQueue[int]((x: int, y: int) => (x > y))
  for i in p+1..<n:
    q.enqueue(a[i])

  var ans = newSeq[int](n)
  while p > 0:
    var nextP = maxInd[p - 1]

    ans[p] += pCnt * (a[p] - a[nextP])

    while q.len() > 0 and q.front() >= a[nextP]:
      let v = q.dequeue()
      pCnt += 1
      ans[p] += v - a[nextP]

    for i in nextP+1..<p:
      q.enqueue(a[i])

    pCnt += 1
    p = nextP

  ans[0] += pCnt * a[0]
  while q.len() > 0:
    ans[0] += q.dequeue()

  for i in 0..<n:
    echo ans[i]

main()


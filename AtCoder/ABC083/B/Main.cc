#include <algorithm>
#include <complex>
#include <iostream>
#include <queue>
#include <set>
#include <stack>
#include <string>
#include <vector>

using std::cin;
using std::cout;
using std::endl;
using std::cerr;

using std::string;
using std::to_string;
using std::vector;
using std::set;
using std::queue;
using std::stack;
using std::priority_queue;

using std::min;
using std::max;
using std::sort;
using std::abs;

typedef long long int ll;
const int MOD = 1e9 + 7;

int keta_sum(int n) {
    if (n < 10) {
        return n;
    }
    return n % 10 + keta_sum(n / 10);
}

int main() {
    int n, a, b;
    cin >> n >> a >> b;
    int sum = 0;
    for (int i = 1; i <= n; i++) {
        int v = keta_sum(i);
        if (a <= v && v <= b) {
            sum += i;
        }
    }
    cout << sum << endl;
    return 0;
}

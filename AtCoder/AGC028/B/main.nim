import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import logging
import future

const INF* = int(1e18 + 373)

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

proc readInt1*(): int = readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))
#------------------------------------------------------------------------------#
const MOD = int(10^9+7)

type Op1[T] = proc(a: T, b: T): T
type Op2[T] = proc(a: T, b: T): T

proc repeatedSquares[T](x: T, n: Natural, e: T, f: Op2[T]): T =
    result = e
    var xx = f(x, e)
    var i = n
    while i > 0:
        if (i and 1) != 0:
            result = f(result, xx)
        xx = f(xx, xx)
        i = i shr 1

proc addM(a, b: Natural): Natural = (a + b) mod MOD
proc subM(a, b: Natural): Natural = (a - b + MOD) mod MOD
proc mulM(a, b: Natural): Natural = a * b mod MOD
proc powM(a, b: Natural): Natural = repeatedSquares[Natural](a, b, 1, `mulM`)
proc invM(a: Natural): Natural = a.powM(MOD - 2)
proc divM(a, b: Natural): Natural = a.mulM(invM(b))

var memoFactM: seq[Natural] = nil
var memoFactInvM: seq[Natural] = nil

proc buildFactTable(n: Natural) =
  memoFactM = newSeq[Natural](n + 1)
  memoFactM[0] = 1
  for i in 1..n:
    memoFactM[i] = memoFactM[i - 1].mulM(i)

  memoFactInvM = newSeq[Natural](n + 1)
  memoFactInvM[n] = invM(memoFactM[n])
  for i in countdown(n - 1, 0):
    memoFactInvM[i] = memoFactInvM[i + 1].mulM(i + 1)

buildFactTable(10^6)

proc factM(n: Natural): Natural = memoFactM[n]

proc factInvM(n: Natural): Natural = memoFactInvM[n]

proc combM(n, r: Natural): Natural =
  if r > n:
    return 0
  if r > n div 2:
    return combM(n, n - r)

  result = factM(n).mulM(factInvM(n - r)).mulM(factInvM(r))

proc multCombM(n, r: Natural): Natural = combM(n + r - 1, r - 1)

#------------------------------------------------------------------------------#
proc buildCumTab(a: seq[int]): seq[int] =
  let n = a.len()
  var c = newSeq[int](n + 1)
  for i in 1..n:
    c[i] = c[i - 1].addM(a[i - 1])
  return c

# [s, t)
proc lookupCumTab(c: seq[int]; s, t: int): int = c[t].subM(c[s])
#------------------------------------------------------------------------------#

proc main() =
  let n = readInt1()
  let a = readSeq().map(parseInt)

  var b = newSeq[int](n)
  for i in 0..<n:
    b[i] = min(i, n - 1 - i).mulM(a[i])

  let aCum = a.buildCumTab()
  let bCum = b.buildCumTab()

  var ans = 0
  for w in 1..(n - 2):
    let coef = factM(n).mulM(factInvM(w + 2))
    let f = coef.mulM(2).mulM(factM(w))

    let midS = min(w, n - 1 - w)
    let midT = max(w, n - 1 - w)
    let combL = bCum.lookupCumTab(1, midS).mulM(f)
    let combM = aCum.lookupCumTab(midS, midT + 1).mulM(min(n - 1 - w, w)).mulM(f)
    let combR = bCum.lookupCumTab(midT + 1, n - 1).mulM(f)
    let comb = combL.addM(combM).addM(combR)
    #echo midS, " ", midT, " ", f
    #echo "w = ", w, " comb = ", comb, " combL = ", combL, " combM = ", combM, " combR = ", combR
    ans = ans.addM(comb)

  for w in 1..<n:
    let coef = factM(n).mulM(factInvM(w + 1))
    let f = coef.mulM(1).mulM(factM(w))

    let combL = aCum.lookupCumTab(0, w).mulM(f)
    let combR = aCum.lookupCumTab(n - w, n).mulM(f)
    ans = ans.addM(combL).addM(combR)

  let wAll = aCum.lookupCumTab(0, n).mulM(factM(n))
  ans = ans.addM(wAll)
  echo ans

main()


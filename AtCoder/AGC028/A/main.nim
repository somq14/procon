import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import logging
import future

const INF* = int(1e18 + 373)

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

proc readInt1*(): int = readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#
proc gcd(a, b: int): int =
  var a = a
  var b = b
  while b != 0:
    let tmp = a mod b
    a = b
    b = tmp
  return a

proc lcm(a, b: int): int =
  a * b div gcd(a, b)

proc main() =
  let (n, m) = readInt2()
  let s = stdin.readLine()
  let t = stdin.readLine()

  let k = lcm(n, m)
  let stepS = k div n
  let stepT = k div m
  let step = lcm(stepS, stepT)

  var ans = true

  var i = 0
  while i * step < k:
    if s[(i * step) div stepS] != t[(i * step) div stepT]:
      ans = false
      break
    i += 1

  if ans:
    echo k
  else:
    echo -1

main()


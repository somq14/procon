import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import logging
import future

const INF* = int(1e18 + 373)

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

proc readInt1*(): int = readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#
proc solve(n: int; vs: seq[(int, int)]): int =
  var es = newSeq[(int, char, int)](0)
  for i in 0..<n:
    es.add((vs[i][0], 'A', i))
    es.add((vs[i][1], 'B', i))
  es.sort(cmp[(int, char, int)])

  let sumN = es[0..<n].map(it => it[0]).sum()

  # all A
  if es[0..<n].all(it => it[1] == 'A'):
    return sumN
  # all B
  if es[0..<n].all(it => it[1] == 'B'):
    return sumN

  var exists = newSeq[int](n)
  for i in 0..<n:
    exists[es[i][2]] += 1
  if exists.any(it => it >= 2):
    return sumN

  var ans = INF
  for i in 0..<n:
    let v = es[i][2]
    if v != es[n][2]:
      ans = min(ans, sumN - es[i][0] + es[n][0])
    else:
      ans = min(ans, sumN - es[i][0] + es[n + 1][0])

  return ans

proc main() =
  let n = readInt1()
  var vs = newSeq[(int, int)](n)
  for i in 0..<n:
    vs[i] = readInt2()
  echo solve(n, vs)

main()


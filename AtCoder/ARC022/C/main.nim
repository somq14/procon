import strutils
import sequtils
import algorithm
import math
import queues

const INF = int(1e18 + 373)
const MOD = int(1e9 + 7)

proc readInt1() : int = stdin.readLine().strip().parseInt()
proc readInt2() : (int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1])
proc readInt3() : (int, int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1], v[2])

#------------------------------------------------------------------------------#
proc dfs(n: int, g: seq[seq[int]], v, d: int, dep: var seq[int]) =
    dep[v] = d
    for u in g[v]:
        if dep[u] == -1:
            dfs(n, g, u, d + 1, dep)

proc main() =
    var n = readInt1()

    var g = newSeqWith(n, newSeq[int](0))
    for i in 0..<(n - 1):
        let (a, b) = readInt2()
        g[a-1].add(b-1)
        g[b-1].add(a-1)

    var dep1 = newSeq[int](n)
    dep1.fill(-1)
    dfs(n, g, 0, 0, dep1)

    let a = find(dep1, max(dep1))

    var dep2 = newSeq[int](n)
    dep2.fill(-1)
    dfs(n, g, a, 0, dep2)

    let b = find(dep2, max(dep2))

    echo((a + 1), " ", (b + 1))

main()


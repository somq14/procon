import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import logging
import future

const INF* = int(1e18 + 373)

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

proc readInt1*(): int = readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#

proc buildCumTab(a: seq[int]): seq[int] =
  let n = a.len()
  var c = newSeq[int](n + 1)
  for i in 1..n:
    c[i] = c[i - 1] + a[i - 1]
  return c

# [s, t)
proc lookupCumTab(c: seq[int]; s, t: int): int = c[t] - c[s]

#------------------------------------------------------------------------------#
type Pred1[T] = T -> bool

# [lb, ub)
proc nibutanLb(lb, ub: int; f: Pred1[int]): int =
  var lb = lb
  var ub = ub
  while ub - lb > 1:
    let mid = (ub + lb) div 2
    if f(mid):
      lb = mid
    else:
      ub = mid
  return lb

# (lb, ub]
proc nibutanUb(lb, ub: int; f: Pred1[int]): int =
  var lb = lb
  var ub = ub
  while ub - lb > 1:
    let mid = (ub + lb) div 2
    if f(mid):
      ub = mid
    else:
      lb = mid
  return ub
#------------------------------------------------------------------------------#

proc subr(n: int; a, c: seq[int]; s, t: int): seq[(int, int)] =
  var ans = newSeq[(int, int)](0)
  let sum = c.lookupCumTab(s, t)

  let ind = nibutanLb(s + 1, t, x => (c[x] - c[s] <= sum div 2))

  let sumL = c.lookupCumTab(s, ind)
  ans.add((sumL, sum - sumL))

  if ind != t - 1:
    ans.add((sumL + a[ind], sum - sumL - a[ind]))

  return ans

proc main() =
  let n = readInt1()
  let a = readSeq().map(parseInt)

  let c = a.buildCumTab()

  var ans = INF
  for i in 2..n - 2:
    let lsum = c.lookupCumTab(0, i)
    let lind = nibutanLb(1, i, x => (c[x] - c[0] <= lsum div 2))
    let llsum = c.lookupCumTab(0, lind)

    let rsum = c.lookupCumTab(i, n)
    let rind = nibutanLb(i + 1, n, x => (c[x] - c[i] <= rsum div 2))
    let rlsum = c.lookupCumTab(i, rind)

    var sol = 0

    let l1 = lsum - llsum
    let l2 = llsum
    let l3 = l2 + a[lind]
    let l4 = l1 - a[lind]

    let r1 = rsum - rlsum
    let r2 = rlsum
    let r3 = r2 + a[rind]
    let r4 = r1 - a[rind]

    sol = max([l1, r1, l2, r2]) - min([l1, r1, l2, r2])
    ans = min(ans, sol)

    if lind != i - 1:
      sol = max([l3, r1, l4, r2]) - min([l3, r1, l4, r2])
      ans = min(ans, sol)

    if rind != n - 1:
      sol = max([l1, r3, l2, r4]) - min([l1, r3, l2, r4])
      ans = min(ans, sol)

    if lind != i - 1 and rind != n - 1:
      sol = max([l3, r3, l4, r4]) - min([l3, r3, l4, r4])
      ans = min(ans, sol)

  echo ans

main()


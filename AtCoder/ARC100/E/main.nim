import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import logging
import future

const INF* = int(1e18 + 373)

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

proc readInt1*(): int = readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#
proc insert(a: seq[int]; x: (int, int); i: int): (int, int) =
  if x[0] == i or x[1] == i:
    return x
  if a[i] < a[x[1]]:
    return x
  if a[i] >= a[x[0]]:
    return (i, x[0])
  return (x[0], i)

proc update(a: seq[int]; x, y: (int, int)): (int, int) =
  result = insert(a, x, y[0])
  result = insert(a, result, y[1])

proc main() =
  let n = readInt1()
  let a = readSeq().map(parseInt) & @[ -INF ]

  var dp = newSeq[(int, int)](1 shl n)
  dp[0] = (0, 1 shl n)
  for s in 1..<(1 shl n):
    var opt = (s, 1 shl n)
    for i in 0..<n:
      let mask = 1 shl i
      if (s and mask) == 0:
        continue
      opt = update(a, opt, dp[s xor mask])
    dp[s] = opt

  var ans = newSeq[int](1 shl n)
  ans[0] = -INF
  for s in 1..<(1 shl n):
    ans[s] = max(ans[s - 1], a[dp[s][0]] + a[dp[s][1]])
    echo ans[s]

main()


import strutils
import sequtils
import algorithm
import math
import queues
import tables
import logging
import future

const INF* = int(1e18 + 373)

when not defined(release):
  addHandler(newFileLogger(stderr, lvlAll, "[ $levelname ] "))

proc readInt1*(): int = stdin.readLine().strip().parseInt()
proc readInt2*(): (int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1])
proc readInt3*(): (int, int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1], v[2])

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#

proc countOddEven[T](a: seq[T]): (int, int) =
  var t = initTable[T, int]()
  for e in a:
    t[e] = 0
  for e in a:
    t[e] += 1

  var odd_cnt = 0
  var even_cnt = 0
  for v in t.values():
    if v mod 2 == 0:
      even_cnt += 1
    else:
      odd_cnt += 1

  return (even_cnt, odd_cnt)

proc solve(h, w: int; s: seq[string]): bool =
  var cnt = initTable[char, int]()
  for c in 'a'..'z':
    cnt[c] = 0

  for i in 0..<h:
    for j in 0..<w:
      cnt[s[i][j]] += 1

  var odd_cnt = 0
  var even_cnt = 0
  for c in 'a'..'z':
    let v = cnt[c]
    if v mod 2 == 0:
      even_cnt += 1
    else:
      odd_cnt += 1

  if h mod 2 != 0 and w mod 2 != 0:
    if odd_cnt != 1:
      return false

  if h mod 2 == 0 or w mod 2 == 0:
    if odd_cnt != 0:
      return false

  var yy = newSeq[string](w)
  for i in 0..<w:
    var y = ""
    for j in 0..<h:
      y = y & s[j][i]
    yy[i] = y

  for i in 0..<w:
    yy[i].sort(cmp[char])
  yy.sort(cmp[string])

  var xx = newSeq[string](h)
  for i in 0..<h:
    var x = ""
    for j in 0..<w:
      x = x & s[i][j]
    xx[i] = x

  for i in 0..<h:
    xx[i].sort(cmp[char])
  xx.sort(cmp[string])

  let (even_y, odd_y) = countOddEven(yy)
  let (even_x, odd_x) = countOddEven(xx)

  var ok_y = false
  if w mod 2 == 0:
    ok_y = (odd_y == 0)
  else:
    ok_y = (odd_y == 1)

  var ok_x = false
  if h mod 2 == 0:
    ok_x = (odd_x == 0)
  else:
    ok_x = (odd_x == 1)

  return ok_y and ok_x

proc main() =
  let (h, w) = readInt2()
  var s = newSeq[string](h)
  for i in 0..<h:
    s[i] = stdin.readLine()

  if solve(h, w, s):
    echo "YES"
  else:
    echo "NO"

main()


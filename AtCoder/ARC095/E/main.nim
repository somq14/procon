import strutils
import sequtils
import algorithm
import math
import queues
import tables
import logging
import future

const INF* = int(1e18 + 373)

when not defined(release):
  addHandler(newFileLogger(stderr, lvlAll, "[ $levelname ] "))

proc readInt1*(): int = stdin.readLine().strip().parseInt()
proc readInt2*(): (int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1])
proc readInt3*(): (int, int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1], v[2])

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#

proc valid(h, w: int; s: seq[string]; c1, c2: int; ps: seq[int]): bool =
  var j = 0
  while j + 1 < h:
    let r1 = ps[j]
    let r2 = ps[j + 1]
    if s[r1][c1] != s[r2][c2]:
      return false
    if s[r1][c2] != s[r2][c1]:
      return false
    j += 2
  return true

proc backtrack2(h, w: int; s: seq[string]; i: int; ps: seq[int]; used: var seq[bool]): bool =
  if i >= w:
    return true

  if used[i]:
    return backtrack2(h, w, s, i + 1, ps, used)

  used[i] = true

  for j in (i + 1)..<w:
    if used[j]:
      continue

    if not valid(h, w, s, i, j, ps):
      continue

    used[j] = true
    if backtrack2(h, w, s, i + 1, ps, used):
      return true
    used[j] = false

  used[i] = false
  return false

proc solve2(h, w: int; s: seq[string]; ps: seq[int]): bool =
  var used = newSeq[bool](w)
  if w mod 2 == 0:
    return backtrack2(h, w, s, 0, ps, used)

  for i in 0..<w:
    if not valid(h, w, s, i, i, ps):
      continue
    used[i] = true
    if backtrack2(h, w, s, 0, ps, used):
      return true
    used[i] = false

  return false

proc backtrack1(h, w: int; s: seq[string]; i: int; ps: var seq[int]; used: var seq[bool]): bool =
  if i >= h:
    return solve2(h, w, s, ps)

  if used[i]:
    return backtrack1(h, w, s, i + 1, ps, used)

  used[i] = true
  ps.add(i)

  for j in (i + 1)..<h:
    if used[j]:
      continue

    if s[i].sorted(cmp[char]) != s[j].sorted(cmp[char]):
      continue

    used[j] = true
    ps.add(j)

    if backtrack1(h, w, s, i + 1, ps, used):
      return true

    used[j] = false
    discard ps.pop()

  used[i] = false
  discard ps.pop()

  return false

proc solve1(h, w: int; s: seq[string]): bool =
  var ps: seq[int] = @[]
  var used = newSeq[bool](h)

  if h mod 2 == 0:
    return backtrack1(h, w, s, 0, ps, used)

  for i in 0..<h:
    ps.add(i)
    ps.add(i)
    used[i] = true

    if backtrack1(h, w, s, 0, ps, used):
      return true

    discard ps.pop()
    discard ps.pop()
    used[i] = false

  return false

proc main() =
  let (h, w) = readInt2()

  var s = newSeq[string](h)
  for i in 0..<h:
    s[i] = stdin.readLine()

  var ps: seq[int] = @[]
  var used = newSeq[bool](h)
  echo if solve1(h, w, s): "YES" else: "NO"

main()


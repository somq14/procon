import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import logging
import future

const INF* = int(1e18 + 373)

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

proc readInt1*(): int = readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

type seq3*[T] = seq[seq[seq[T]]]
proc newSeq3*[T](n1, n2, n3: Natural): seq3[T] = newSeqWith(n1, newSeqWith(n2, newSeq[T](n3)))

#------------------------------------------------------------------------------#
type Pos = tuple [ y, x: int ]

proc adjust(p: Pos; ds: seq[int]): string =
  let q: Pos = (p.y + p.x, p.y - p.x)
  let sumD = ds.sum()

  var yAdjust = newSeq[int](ds.len())
  yAdjust.fill(1)
  let absY = abs(q.y)
  let diffY = sumD - absY
  assert diffY mod 2 == 0
  for i in 0..31:
    if (diffY and (1 shl (i + 1))) != 0:
      yAdjust[i] = -1
  if q.y < 0:
    yAdjust = yAdjust.map(it => -it)

  var xAdjust = newSeq[int](ds.len())
  xAdjust.fill(1)
  let absX = abs(q.x)
  let diffX = sumD - absX
  assert diffX mod 2 == 0
  for i in 0..31:
    if (diffX and (1 shl (i + 1))) != 0:
      xAdjust[i] = -1
  if q.x < 0:
    xAdjust = xAdjust.map(it => -it)

  var ans = ""
  for i in 0..<ds.len():
    let adjust = (yAdjust[i], xAdjust[i])
    if adjust == (+1, +1):
      ans &= "U"
    elif adjust == (-1, -1):
      ans &= "D"
    elif adjust == (+1, -1):
      ans &= "R"
    elif adjust == (-1, +1):
      ans &= "L"
    else:
      assert false
  return ans

proc simulate(op: string; ds: seq[int]): Pos =
  var y = 0
  var x = 0
  for i in 0..<ds.len():
    case op[i]:
    of 'U':
      y += ds[i]
    of 'D':
      y -= ds[i]
    of 'L':
      x -= ds[i]
    of 'R':
      x += ds[i]
    else:
      assert false
  return (y, x)

proc main() =
  let n = readInt1()
  var ps = newSeq[Pos](n)
  for i in 0..<n:
    let (x, y) = readInt2()
    ps[i] = (y, x)

  let mod2 = (ps[0].y + ps[0].x + 10^10) mod 2
  if not ps.all(it => (it.y + it.x + 10^10) mod 2 == mod2):
    echo -1
    return

  var ds = newSeq[int](0)
  for i in 0..31:
    ds.add(1 shl i)
  ds.add(if mod2 == 0: (1 shl 32) - 1 else: (1 shl 32))

  var ans = newSeq[string](n)
  for i in 0..<n:
    ans[i] = adjust(ps[i], ds)
    assert ps[i] == simulate(ans[i], ds)

  echo ds.len()
  echo ds.map(it => $it).join(" ")
  for i in 0..<n:
    echo ans[i]

main()


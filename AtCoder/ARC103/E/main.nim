import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import logging
import future

const INF* = int(1e18 + 373)

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

proc readInt1*(): int = readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#

proc main() =
  let s = stdin.readLine()
  let n = s.len()

  if s[n - 1] == '1':
    echo -1
    return

  for i in 0..<(n - 1):
    if s[i] != s[n - 2 - i]:
      echo -1
      return

  if s[0] == '0':
    echo -1
    return

  var es = newSeq[(int, int)](0)
  var v = 1
  var r = 1
  for i in 1..(n - 2) div 2:
    if s[i] == '0':
      continue

    let sibling = (i + 1) - (v + 1)

    v += 1
    es.add((r, v))
    let nextR = v
    for _ in 0..<sibling:
      v += 1
      es.add((nextR, v))

    r = nextR

  v += 1
  es.add((r, v))
  r = v

  while v < n:
    v += 1
    es.add((r, v))

  for e in es:
    echo e[0], " ", e[1]

main()


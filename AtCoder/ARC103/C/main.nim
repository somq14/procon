import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import logging
import future

const INF* = int(1e18 + 373)

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

proc readInt1*(): int = readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#

proc main() =
  let n = readInt1()
  let a = readSeq().map(parseInt)

  var oddT = initTable[int, int]()
  var evenT = initTable[int, int]()
  for i in 0..<n:
    if i mod 2 == 0:
      evenT[a[i]] = 0
    else:
      oddT[a[i]] = 0

  for i in 0..<n:
    if i mod 2 == 0:
      evenT[a[i]] += 1
    else:
      oddT[a[i]] += 1

  var evenMax = a[0]
  var oddMax = a[1]
  for i in 0..<n:
    if i mod 2 == 0:
      if evenT[a[i]] > evenT[evenMax]:
        evenMax = a[i]
    else:
      if oddT[a[i]] > oddT[oddMax]:
        oddMax = a[i]

  var evenSecondMax = -1
  var oddSecondMax = -1
  for i in 0..<n:
    if i mod 2 == 0:
      if a[i] == evenMax:
        continue
      if evenSecondMax == -1 or evenT[a[i]] > evenT[evenSecondMax]:
        evenSecondMax = a[i]
    else:
      if a[i] == oddMax:
        continue
      if oddSecondMax == -1 or oddT[a[i]] > oddT[oddSecondMax]:
        oddSecondMax = a[i]

  if evenMax != oddMax:
    let ans = (n div 2 - evenT[evenMax]) + (n div 2 - oddT[oddMax])
    echo ans
  else:
    var ans1 = -1
    if evenSecondMax == -1:
      ans1 = (n div 2) + (n div 2 - oddT[oddMax])
    else:
      ans1 = (n div 2 - evenT[evenSecondMax]) + (n div 2 - oddT[oddMax])

    var ans2 = -1
    if oddSecondMax == -1:
      ans2 = (n div 2 - evenT[evenMax]) + (n div 2)
    else:
      ans2 = (n div 2 - evenT[evenMax]) + (n div 2 - oddT[oddSecondMax])
    echo min(ans1, ans2)


main()


import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import logging
import future

const INF* = int(1e18 + 373)

when not defined(release):
  addHandler(newFileLogger(stderr, lvlAll, "[ $levelname ] "))

proc readInt1*(): int = stdin.readLine().strip().parseInt()
proc readInt2*(): (int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1])
proc readInt3*(): (int, int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1], v[2])
proc readInt4*(): (int, int, int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1], v[2], v[3])

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#
type Pred1[T] = T -> bool

# [lb, ub)
proc nibutanLb(lb, ub: int; f: Pred1[int]): int =
  var lb = lb
  var ub = ub
  while ub - lb > 1:
    let mid = (ub + lb) div 2
    if f(mid):
      lb = mid
    else:
      ub = mid
  return lb

# (lb, ub]
proc nibutanUb(lb, ub: int; f: Pred1[int]): int =
  var lb = lb
  var ub = ub
  while ub - lb > 1:
    let mid = (ub + lb) div 2
    if f(mid):
      ub = mid
    else:
      lb = mid
  return ub
#------------------------------------------------------------------------------#

proc eval(n, m: int; a: seq2[int]; x: int): bool =
  if x >= n:
    return true

  var ban = newSeq[bool](m)
  var siz = newSeq[int](m)
  var pos = newSeq[int](n)

  for i in 0..<n:
    siz[a[i][pos[i]]] += 1

  for groupCount in countdown(m, 2):
    var g = -1
    for i in 0..<m:
      if siz[i] > x:
        g = i
        break

    if g == -1:
      break

    ban[g] = true
    siz[g] = 0

    for i in 0..<n:
      if a[i][pos[i]] != g:
        continue
      while ban[a[i][pos[i]]]:
        pos[i] += 1
      siz[a[i][pos[i]]] += 1

  return siz.all(it => it <= x)

proc main() =
  let (n, m) = readInt2()

  var a = newSeq2[int](n, 0)
  for i in 0..<n:
    a[i] = readSeq().map(parseInt).map(it => it - 1)

  echo nibutanUb(0, n, x => eval(n, m, a, x))

main()


import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import future

const INF* = int(1e18 + 373)

proc readLine*(): string =
  stdin.readLine()
proc readSeq*(): seq[string] =
  readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = readLine().strip()
proc readInt1*(): int =
  readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])
proc newSeqWith*[T](n: Natural; e: T): seq[T] =
  result = newSeq[T](n)
  for i in 0..<n:
    result[i] = e
type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] =
  newSeqWith(n1, newSeq[T](n2))
type seq3*[T] = seq[seq[seq[T]]]
proc newSeq3*[T](n1, n2, n3: Natural): seq3[T] =
  newSeqWith(n1, newSeqWith(n2, newSeq[T](n3)))

#------------------------------------------------------------------------------#
type PriorityQueue[T] = object
  a: seq[T]
  n: Natural
  cmp: (a: T, b: T) -> bool

proc initPriorityQueue[T](cmp: (a: T, b: T) -> bool): PriorityQueue[T] =
  result.a = newSeq[T](1)
  result.n = Natural(0)
  result.cmp = cmp

proc len[T](this: PriorityQueue[T]): Natural =
  this.n

proc enqueue[T](this: var PriorityQueue[T], e: T) =
  this.n += 1
  this.a.add(e)
  var i = this.a.len() - 1
  while i > 1 and this.cmp(this.a[i], this.a[i div 2]):
    swap(this.a[i], this.a[i div 2])
    i = i div 2

proc dequeue[T](this: var PriorityQueue[T]): T =
  this.n -= 1
  result = this.a[1]
  this.a[1] = this.a.pop()
  var i = 1
  while i < this.a.len():
    var j = i
    if i * 2 < this.a.len() and this.cmp(this.a[i * 2], this.a[j]):
      j = i * 2
    if i * 2 + 1 < this.a.len() and this.cmp(this.a[i * 2 + 1], this.a[j]):
      j = i * 2 + 1
    if i == j:
      break
    swap(this.a[i], this.a[j])
    i = j

proc front[T](this: PriorityQueue[T]): T =
  this.a[1]

#------------------------------------------------------------------------------#

proc main() =
  let (x, y, z) = readInt3()
  let n = x + y + z

  var a = newSeq[int](n)
  var b = newSeq[int](n)
  var c = newSeq[int](n)
  for i in 0..<n:
    (a[i], b[i], c[i]) = readInt3()

  for i in 0..<n:
    a[i] -= c[i]
    b[i] -= c[i]

  let base = c.sum()

  var ab = newSeq[(int, int, int)](n)
  for i in 0..<n:
    ab[i] = (a[i] - b[i], a[i], b[i])
  ab.sort(cmp[(int, int, int)])
  ab.reverse()

  var dpL = newSeq[int](n)
  block:
    var queL = initPriorityQueue[int]((a: int, b: int) => (a < b))
    var sumL = 0
    for i in 0..<n:
      queL.enqueue(ab[i][1])
      sumL += ab[i][1]
      while queL.len() > x:
        sumL -= queL.dequeue()
      dpL[i] = sumL

  var dpR = newSeq[int](n)
  block:
    var queR = initPriorityQueue[int]((a: int, b: int) => (a < b))
    var sumR = 0
    for i in countdown(n - 1, 0):
      queR.enqueue(ab[i][2])
      sumR += ab[i][2]
      while queR.len() > y:
        sumR -= queR.dequeue()
      dpR[i] = sumR

  var ans = -INF
  for i in x - 1..<n - y:
    ans = max(ans, base + dpL[i] + dpR[i + 1])

  echo ans

main()


import strutils
import sequtils
import algorithm
import math
import queues
import tables
import logging
import future

const INF* = int(1e18 + 373)

when not defined(release):
  addHandler(newFileLogger(stderr, lvlAll, "[ $levelname ] "))

proc readInt1*(): int = stdin.readLine().strip().parseInt()
proc readInt2*(): (int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1])
proc readInt3*(): (int, int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1], v[2])

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#
#------------------------------------------------------------------------------#
const MOD = int(998244353)

type Op1[T] = proc(a: T, b: T): T
type Op2[T] = proc(a: T, b: T): T

proc repeatedSquares[T](x: T, n: Natural, e: T, f: Op2[T]): T =
    result = e
    var xx = f(x, e)
    var i = n
    while i > 0:
        if (i and 1) != 0:
            result = f(result, xx)
        xx = f(xx, xx)
        i = i shr 1


proc addM(a, b: Natural): Natural = (a + b) mod MOD
proc subM(a, b: Natural): Natural = (a - b + MOD) mod MOD
proc mulM(a, b: Natural): Natural = a * b mod MOD
proc powM(a, b: Natural): Natural = repeatedSquares[Natural](a, b, 1, `mulM`)
proc invM(a: Natural): Natural = a.powM(MOD - 2)
proc divM(a, b: Natural): Natural = a.mulM(invM(b))


var memoFactM = @[Natural(1)]
proc factM(n: Natural): Natural =
    if n < memoFactM.len():
        return memoFactM[n]
    for i in memoFactM.len()..n:
        memoFactM.add(memoFactM[i - 1].mulM(i))
    return memoFactM[n]


proc combM(n, r: Natural): Natural =
    if r > n:
        return 0
    if r > n div 2:
        return combM(n, n - r)

    result = 1
    for i in ((n - r) + 1)..n:
        result = result.mulM(i)
    result = result.divM(factM(r))


proc multCombM(n, r: Natural): Natural = combM(n + r - 1, n - 1)

#------------------------------------------------------------------------------#


# ax + by + c = 0
type Line = tuple [ a, b, c: int ]
type Point = tuple [ y, x: int ]

proc `$`(p: Point): string = "(" & $p.y & ", " & $p.x & ")"

proc on(pp: Point; ll: Line): bool =
  ll.a * pp.x + ll.b * pp.y + ll.c == 0


proc normalizeLine(ll: Line): Line =
  let d = gcd(abs(ll.a), gcd(abs(ll.b), abs(ll.c)))
  let (a, b, c) = (ll.a div d, ll.b div d, ll.c div d)
  if ll.a == 0:
    return if ll.b >= 0: (a, b, c) else: (-a, -b, -c)
  return if ll.a >= 0: (a, b, c) else: (-a, -b, -c)

proc calcLine(p1, p2: Point): Line =
  let ll = (p2.y - p1.y, p1.x - p2.x, -p1.x * p2.y + p1.y * p2.x)
  return ll.normalizeLine()

proc main() =
  let n = readInt1()
  var ps = newSeq[Point](n)
  for i in 0..<n:
    let (x, y) = readInt2()
    ps[i] = (y, x)

  var ls = initTable[Line, seq[Point]]()
  for i in 0..<n:
    for j in (i + 1)..<n:
      let ll = calcLine(ps[i], ps[j])
      if ll notin ls:
        ls[ll] = @[]

  for ll in ls.keys():
    for i in 0..<n:
      if ps[i].on(ll):
        ls[ll].add(ps[i])

  var ans = 2.powM(n)
  ans = ans.subM(combM(n, 0))
  ans = ans.subM(combM(n, 1))
  ans = ans.subM(combM(n, 2))

  for lps in ls.values():
    let m = lps.len()
    for i in 3..m:
      ans = ans.subM(combM(m, i))

  echo ans

main()


import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import logging
import future

const INF* = int(1e18 + 373)

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

proc readInt1*(): int = readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#
#------------------------------------------------------------------------------#
type Pred1[T] = T -> bool

# [lb, ub)
proc nibutanLb(lb, ub: int; f: Pred1[int]): int =
  var lb = lb
  var ub = ub
  while ub - lb > 1:
    let mid = (ub + lb) div 2
    if f(mid):
      lb = mid
    else:
      ub = mid
  return lb

# (lb, ub]
proc nibutanUb(lb, ub: int; f: Pred1[int]): int =
  var lb = lb
  var ub = ub
  while ub - lb > 1:
    let mid = (ub + lb) div 2
    if f(mid):
      ub = mid
    else:
      lb = mid
  return ub
#------------------------------------------------------------------------------#
proc asc(x, a, b, c, t: int): (int, int, int) =
  var aa = a
  var bb = b
  var cc = c
  cc = min(c + t, x)
  if t >= x - (b - a + c):
    let tt = t - (x - (b - a + c))
    bb = b - tt
    if bb < aa:
      bb = aa
  return (aa, bb, cc)

proc des(x, a, b, c, t: int): (int, int, int) =
  var aa = a
  var bb = b
  var cc = c
  cc = max(c - t, 0)
  if t >= c:
    let tt = t - c
    aa = a + tt
    if aa > bb:
      aa = bb
  return (aa, bb, cc)

proc main() =
  let X = readInt1()
  let K = readInt1()
  let R = @[0] & readSeq().map(parseInt)
  let Q = readInt1()
  var T = newSeq[int](Q)
  var A = newSeq[int](Q)
  for i in 0..<Q:
    (T[i], A[i]) = readInt2()

  var a = newSeq[int](K + 1)
  var b = newSeq[int](K + 1)
  var c = newSeq[int](K + 1)
  a[0] = 0
  b[0] = X
  c[0] = 0
  for i in 1..K:
    let t = R[i] - R[i - 1]
    if i mod 2 == 1:
      (a[i], b[i], c[i]) = des(X, a[i - 1], b[i - 1], c[i - 1], t)
    else:
      (a[i], b[i], c[i]) = asc(X, a[i - 1], b[i - 1], c[i - 1], t)

  # echo a
  # echo b
  # echo c
  for i in 0..<Q:
    let ind = nibutanLb(0, K + 1, x => R[x] <= T[i])
    var aa: int
    var bb: int
    var cc: int
    if ind mod 2 == 1:
      (aa, bb, cc) = asc(X, a[ind], b[ind], c[ind], T[i] - R[ind])
    else:
      (aa, bb, cc) = des(X, a[ind], b[ind], c[ind], T[i] - R[ind])

    # echo "a = ", aa
    # echo "b = ", bb
    # echo "c = ", cc

    if A[i] in 0..aa:
      echo cc
    elif A[i] in aa..bb:
      echo cc + (A[i] - aa)
    elif A[i] in bb..X:
      echo cc + bb - aa
    else:
      discard

main()


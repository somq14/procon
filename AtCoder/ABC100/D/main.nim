import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import logging
import future

const INF* = int(1e18 + 373)

when not defined(release):
  addHandler(newFileLogger(stderr, lvlAll, "[ $levelname ] "))

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

proc readInt1*(): int = readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#
proc main() =
  let (n, m) = readInt2()
  var xs = newSeq[int](n)
  var ys = newSeq[int](n)
  var zs = newSeq[int](n)
  for i in 0..<n:
    (xs[i], ys[i], zs[i]) = readInt3()

  var ans = 0
  for signx in @[-1, 1]:
    for signy in @[-1, 1]:
      for signz in @[-1, 1]:
        var a = newSeq[int](n)
        for i in 0..<n:
          a[i] = signx * xs[i] + signy * ys[i] + signz * zs[i]
        a.sort(cmp[int], SortOrder.Descending)
        let val = a[0..<m].sum()
        ans = max(ans, val)
  echo ans

main()


import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import logging
import future

const INF* = int(1e18 + 373)

when not defined(release):
  addHandler(newFileLogger(stderr, lvlAll, "[ $levelname ] "))

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

proc readInt1*(): int = readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#
proc dfs(g: seq2[int]; v: int; order: var seq[int]) =
  for u in g[v]:
    dfs(g, u, order)
  order.add(v)

proc main() =
  let n = readInt1()
  var g = newSeq2[int](n, 0)
  for i in 1..<n:
    let a = readInt1() - 1
    g[a].add(i)

  var order = newSeq[int](0)
  dfs(g, 0, order)

  var dp = newSeq[int](n)
  for v in order:
    let m = g[v].len()
    var h = newSeq[int](m)
    for i in 0..<m:
      h[i] = dp[g[v][i]]
    h.sort(cmp[int])

    var ans = 0
    for i in 0..<m:
      ans = max(ans, h[i] + m - i)
    dp[v] = ans

  echo dp[0]

main()


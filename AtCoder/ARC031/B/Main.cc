#include <algorithm>
#include <cmath>
#include <complex>
#include <iomanip>
#include <iostream>
#include <string>
#include <utility>
#include <vector>

using namespace std;

using ll = long long int;
const int MOD = 1e9 + 7;
const int INF = 1e9 + 373;

#define rep(i, n) for (int i = 0; i < (n); ++i)

template <typename T>
using vector2 = vector<vector<T>>;
template <typename T>
vector2<T> init_vector2(size_t n0, size_t n1, T e = T()) {
    return vector2<T>(n0, vector<T>(n1, e));
}

template <typename T>
using vector3 = vector<vector<vector<T>>>;
template <typename T>
vector3<T> init_vector3(size_t n0, size_t n1, size_t n2, T e = T()) {
    return vector3<T>(n0, vector2<T>(n1, vector<T>(n2, e)));
}

bool eval(const vector<string>& a) {
    rep(i, 10) {
        rep(j, 10) {
            if (a[i][j] == 'o') {
                return false;
            }
        }
    }
    return true;
}

void dfs(vector<string>& a, int i, int j) {
    if (a[i][j] == 'x') {
        return;
    }
    static vector<int> dy = {0, 0, 1, -1};
    static vector<int> dx = {1, -1, 0, 0};
    a[i][j] = 'x';
    rep(k, 4) {
        int y = i + dy[k];
        int x = j + dx[k];
        if (not(0 <= y && y < 10 && 0 <= x && x < 10)) {
            continue;
        }
        if (a[y][x] == 'o') {
            dfs(a, y, x);
        }
    }
}

int main() {
    vector<string> a(10);
    rep(i, 10) { cin >> a[i]; }

    vector<string> w = a;
    rep(i, 10) {
        rep(j, 10) {
            if (a[i][j] == 'o') {
                dfs(w, i, j);
                if (eval(w)) {
                    cout << "YES" << endl;
                    return 0;
                }
                goto out;
            }
        }
    }
out:

    rep(i, 10) {
        rep(j, 10) {
            if (a[i][j] == 'o') {
                continue;
            }
            w = a;
            w[i][j] = 'o';
            dfs(w, i, j);
            if (eval(w)) {
                cout << "YES" << endl;
                return 0;
            }
        }
    }

    cout << "NO" << endl;
    return 0;
}

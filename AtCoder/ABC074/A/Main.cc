#include <iostream>

using std::cin;
using std::cout;
using std::endl;

int main(void) {
    int n, a;
    cin >> n >> a;
    cout << n * n - a << endl;
    return 0;
}

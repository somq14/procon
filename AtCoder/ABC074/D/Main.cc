#include <algorithm>
#include <iostream>
#include <queue>
#include <set>
#include <utility>
#include <vector>

using std::cin;
using std::cout;
using std::endl;

using std::vector;
using std::set;
using std::pair;
using std::make_pair;
using std::priority_queue;

using std::min;

typedef long long int ll;
const ll INF = 1e18;

int main(void) {
    int n;
    cin >> n;

    vector<vector<ll>> e(n, vector<ll>(n, INF));
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < n; j++) {
            cin >> e[i][j];
        }
    }

    for(int k = 0; k < n; k++){
        for(int i = 0; i < n; i++){
            for(int j = 0; j < n; j++){
                if(e[i][k] + e[k][j] < e[i][j]){
                    cout << -1 << endl;
                    return 0;
                }
            }
        }
    }

    ll sum = 0;
    for(int i = 0; i < n; i++){
        for(int j = i + 1; j < n; j++){
            if(e[i][j] == INF){
                continue;
            }

            bool redundant = false;
            for(int k = 0; k < n; k++){
                if(k != i && k != j && e[i][j] == e[i][k] + e[k][j]){
                    redundant = true;
                    break;
                }
            }
            if(!redundant){
                sum += e[i][j];
            }
        }
    }

    cout << sum << endl;

    return 0;
}

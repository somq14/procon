#include <algorithm>
#include <iostream>
#include <vector>
#include <set>

using std::cin;
using std::cout;
using std::endl;

using std::vector;
using std::set;

using std::min;

int main(void) {
    int A, B, C, D;
    int E, F;
    cin >> A >> B >> C >> D >> E >> F;

    set<int> water_set;
    for (int a = 0; a <= F; a += 100 * A) {
        for (int b = 0; a + b <= F; b += 100 * B) {
            water_set.insert(a + b);
        }
    }

    int opt_water = 0;
    int opt_sugar = 0;
    for (int water : water_set) {
        const int limit = min(F - water, E * (water / 100));
        for (int c = 0; c <= limit; c += C) {
            for (int d = 0; c + d <= limit; d += D) {
                int sugar = c + d;
                if(sugar * (opt_sugar + opt_water) >= opt_sugar * (sugar + water)){
                    opt_sugar = sugar;
                    opt_water = water;
                }
            }
        }
    }

    cout << (opt_sugar + opt_water) << " " << opt_sugar << endl;

    return 0;
}

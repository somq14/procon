import strutils
import sequtils
import algorithm
import math
import queues
import tables
import logging

const INF* = int(1e18 + 373)

when not defined(release):
  addHandler(newFileLogger(stderr, lvlAll, "[ $levelname ] "))

proc readInt1*(): int = stdin.readLine().strip().parseInt()
proc readInt2*(): (int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1])
proc readInt3*(): (int, int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1], v[2])

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#

type Edge = tuple [ to, c : int ]

proc solve(g: seq2[Edge]; v, sum: int; d: var seq[int]) =
  d[v] = sum
  for e in g[v]:
    if d[e.to] == -1:
      solve(g, e.to, sum xor e.c, d)

proc main() =
  let (n, x) = readInt2()

  var g = newSeq2[Edge](n, 0)
  for i in 0..<(n - 1):
    let (x, y, c) = readInt3()
    g[x - 1].add((y - 1, c))
    g[y - 1].add((x - 1, c))

  var d = newSeq[int](n)
  d.fill(-1)
  solve(g, 0, 0, d)
  d.sort(cmp[int])

  var cnt = 0
  for i in 0..<n:
    let s = d.lowerBound(d[i] xor x)
    let t = d.lowerBound((d[i] xor x) + 1)
    cnt += t - s

  if x == 0:
    cnt -= n

  let ans = cnt div 2
  echo ans

main()


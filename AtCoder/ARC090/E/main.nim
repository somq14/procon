import strutils
import sequtils
import algorithm
import math
import queues
import tables
import logging
import future

const INF* = int(1e18 + 373)

when not defined(release):
  addHandler(newFileLogger(stderr, lvlAll, "[ $levelname ] "))

proc readInt1*(): int = stdin.readLine().strip().parseInt()
proc readInt2*(): (int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1])
proc readInt3*(): (int, int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1], v[2])

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#
# FIXME: Use object
type PriorityQueue[T] = tuple[a: seq[T], n: Natural, cmp: proc(a, b: T): bool]

proc initPriorityQueue[T](cmp: proc(a, b: T): bool): PriorityQueue[T] =
    (newSeq[T](1), Natural(0), cmp)

proc len[T](this: PriorityQueue[T]): Natural = this.n

proc enqueue[T](this: var PriorityQueue[T], e: T) =
    this.n += 1
    this.a.add(e)
    var i = this.a.len() - 1
    while i > 1 and this.cmp(this.a[i], this.a[i div 2]):
        swap(this.a[i], this.a[i div 2])
        i = i div 2

proc dequeue[T](this: var PriorityQueue[T]): T =
    this.n -= 1
    result = this.a[1]
    this.a[1] = this.a.pop()
    var i = 1
    while i < this.a.len():
        var j = i
        if i * 2 < this.a.len() and this.cmp(this.a[i * 2], this.a[j]):
            j = i * 2
        if i * 2 + 1 < this.a.len() and this.cmp(this.a[i * 2 + 1], this.a[j]):
            j = i * 2 + 1
        if i == j:
            break
        swap(this.a[i], this.a[j])
        i = j

# FIXME: throw Exception when this is empty
proc front[T](this: PriorityQueue[T]): T = this.a[1]

#------------------------------------------------------------------------------#
#------------------------------------------------------------------------------#
const MOD = int(1e9 + 7)

type Op1[T] = proc(a: T, b: T): T
type Op2[T] = proc(a: T, b: T): T

proc repeatedSquares[T](x: T, n: Natural, e: T, f: Op2[T]): T =
    result = e
    var xx = f(x, e)
    var i = n
    while i > 0:
        if (i and 1) != 0:
            result = f(result, xx)
        xx = f(xx, xx)
        i = i shr 1


proc addM(a, b: Natural): Natural = (a + b) mod MOD
proc subM(a, b: Natural): Natural = (a - b + MOD) mod MOD
proc mulM(a, b: Natural): Natural = a * b mod MOD
proc powM(a, b: Natural): Natural = repeatedSquares[Natural](a, b, 1, `mulM`)
proc invM(a: Natural): Natural = a.powM(MOD - 2)
proc divM(a, b: Natural): Natural = a.mulM(invM(b))


var memoFactM = @[Natural(1)]
proc factM(n: Natural): Natural =
    if n < memoFactM.len():
        return memoFactM[n]
    for i in memoFactM.len()..n:
        memoFactM.add(memoFactM[i - 1].mulM(i))
    return memoFactM[n]


proc combM(n, r: Natural): Natural =
    if r > n:
        return 0
    if r > n div 2:
        return combM(n, n - r)

    result = 1
    for i in ((n - r) + 1)..n:
        result = result.mulM(i)
    result = result.divM(factM(r))


proc multCombM(n, r: Natural): Natural = combM(n + r - 1, n - 1)

#------------------------------------------------------------------------------#
type Edge = tuple[ frm, to, c: int ]

proc dijkstra(n: int; g: seq2[Edge]; s: int): (seq[int], seq[int]) =
  var d = newSeq[int](n)
  d.fill(INF)
  d[s] = 0

  var r = newSeq[int](n)
  r[s] = 1

  type P = tuple[c, v: int]
  var q = initPriorityQueue[P]((p1: P, p2: P) => p1.c < p2.c)
  q.enqueue((0, s))

  while q.len() > 0:
    let (c, v) = q.dequeue()
    if c > d[v]:
      continue
    for e in g[v]:
      let alt = d[v] + e.c
      if alt == d[e.to]:
        r[e.to] = r[e.to].addM(r[v])
      elif alt < d[e.to]:
        d[e.to] = alt
        r[e.to] = r[v]
        q.enqueue((alt, e.to))

  return (d, r)


proc subr(n: int; g: seq2[Edge]; s, t: int): (seq[int], seq[int]) =
  var (d, r) = dijkstra(n, g, s)

  var b = newSeq[bool](n)
  b[t] = true

  var q = initQueue[int]()
  q.enqueue(t)

  while q.len() > 0:
    let v = q.dequeue()
    for e in g[v]:
      if not b[e.to] and d[v] - e.c == d[e.to]:
        b[e.to] = true
        q.enqueue(e.to)

  for v in 0..<n:
    if not b[v]:
      r[v] = 0

  return (d, r)


proc solve(n: int; g: seq2[Edge]; s, t: int): int =
  let (d0, r0) = subr(n, g, s, t)
  let (d1, r1) = subr(n, g, t, s)

  var ans = r0[t].mulM(r1[s])
  debug d0
  debug r0
  debug d1
  debug r1

  for v in 0..<n:
    if d0[v] == d1[v]:
      let c = r0[v].mulM(r1[v])
      ans = ans.subM(c.mulM(c))

  for v in 0..<n:
    for e in g[v]:
      var v0 = e.frm
      var v1 = e.to
      if v0 >= v1:
        continue

      let use0 = d0[v0] + e.c == d0[v1] or d0[v1] + e.c == d0[v0]
      let use1 = d1[v0] + e.c == d1[v1] or d1[v1] + e.c == d1[v0]
      if not use0 or not use1:
        continue

      if d0[v0] > d0[v1]:
        swap(v0, v1)

      # [s0, t0)
      let s0 = d0[v0] + 1
      let t0 = d0[v0] + e.c
      # [s1, t1)
      let s1 = d1[v1] + 1
      let t1 = d1[v1] + e.c

      if not (t0 <= s1 or t1 <= s0):
        let c = r0[v0].mulM(r1[v1])
        ans = ans.subM(c.mulM(c))

  return ans


proc main() =
  let (n, m) = readInt2()
  var (s, t) = readInt2()
  s -= 1
  t -= 1

  var g = newSeq2[Edge](n, 0)
  for i in 0..<m:
    var (u, v, d) = readInt3()
    u -= 1
    v -= 1
    g[u].add((u, v, d))
    g[v].add((v, u, d))

  echo solve(n, g, s, t)

main()


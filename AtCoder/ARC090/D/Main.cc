#include <algorithm>
#include <cmath>
#include <complex>
#include <iomanip>
#include <iostream>
#include <numeric>
#include <queue>
#include <string>
#include <utility>
#include <vector>

using namespace std;

using ll = long long int;
const int MOD = 1e9 + 7;
const int INF = 1e9 + 373;

#define rep(i, n) for (int i = 0; i < (n); ++i)

template <typename T>
using vector2 = vector<vector<T>>;
template <typename T>
vector2<T> init_vector2(size_t n0, size_t n1, T e = T()) {
    return vector2<T>(n0, vector<T>(n1, e));
}

template <typename T>
using vector3 = vector<vector<vector<T>>>;
template <typename T>
vector3<T> init_vector3(size_t n0, size_t n1, size_t n2, T e = T()) {
    return vector3<T>(n0, vector2<T>(n1, vector<T>(n2, e)));
}

class edge {
   public:
    ll v;
    ll d;
    edge() : v(0), d(0) {}
    edge(ll v, ll d) : v(v), d(d) {}
};

int main() {
    ll n, m;
    cin >> n >> m;

    vector2<edge> left(n);
    vector2<edge> right(n);
    rep(i, m) {
        ll l, r, d;
        cin >> l >> r >> d;
        l--;
        r--;
        left[l].push_back(edge(r, d));
        right[r].push_back(edge(l, d));
    }

    bool ans = true;

    vector<ll> x(n);
    vector<bool> used(n);

    rep(i, n) {
        if (used[i]) {
            continue;
        }

        x[i] = 0;
        used[i] = true;

        queue<ll> q;
        q.push(i);
        while (not q.empty()) {
            const ll v = q.front();
            q.pop();
            for (const edge& e : left[v]) {
                if (used[e.v]) {
                    if (x[v] + e.d != x[e.v]) {
                        ans = false;
                        goto end;
                    }
                } else {
                    x[e.v] = x[v] + e.d;
                    used[e.v] = true;
                    q.push(e.v);
                }
            }

            for (const edge& e : right[v]) {
                if (used[e.v]) {
                    if (x[v] - e.d != x[e.v]) {
                        ans = false;
                        goto end;
                    }
                } else {
                    x[e.v] = x[v] - e.d;
                    used[e.v] = true;
                    q.push(e.v);
                }
            }
        }
    }

end:
    cout << (ans ? "Yes" : "No") << endl;
    return 0;
}

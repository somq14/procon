import strutils
import sequtils
import algorithm
import math
import queues
import tables
import logging
import future

const INF* = int(1e18 + 373)

when not defined(release):
  addHandler(newFileLogger(stderr, lvlAll, "[ $levelname ] "))

proc readInt1*(): int = stdin.readLine().strip().parseInt()
proc readInt2*(): (int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1])
proc readInt3*(): (int, int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1], v[2])

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#
#
type Event = tuple [pos, kind: int]

proc main() =
  let (n, m) = readInt2()
  let a = readSeq().map(parseInt).map(it => it - 1)

  var r = newSeq[int](2 * m)
  for i in 0..<n - 1:
    if (a[i + 1] - a[i] + m) mod m <= 1:
      continue
    r[a[i] + 2] += 1
    if a[i] <= a[i + 1]:
      r[a[i + 1] + 1] -= 1
    else:
      r[a[i + 1] + m + 1] -= 1

  for i in 1..<2 * m:
    r[i] += r[i - 1]

  for i in 0..<n - 1:
    r[a[i + 1] + 1] += 1
    r[a[i + 1] + 1] -= (a[i + 1] - a[i] + m) mod m

  for i in 0..<m:
    r[i] += r[i + m]

  r = r[0..<m]


  var cost = 0
  for i in 0..<n - 1:
    let c0 = (a[i + 1] - a[i] + m) mod m
    let c1 = 1 + (a[i + 1] - 0 + m) mod m
    cost += min(c0, c1)

  var ans = newSeq[int](m)
  ans[0] = cost
  for i in 1..<m:
    ans[i] = ans[i - 1] - r[i]

  echo min(ans)

main()


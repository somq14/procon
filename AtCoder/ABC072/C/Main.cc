#include <iostream>
#include <algorithm>
#include <vector>
#include <string>

using std::cin;
using std::cout;
using std::endl;

using std::min;
using std::max;

using std::string;

using std::vector;


int main(void) {
    int n;
    cin >> n;

    vector<int> x(1e5);
    for(int i = 0; i < n; i++){
        int a;
        cin >> a;
        x[a]++;
    }

    int ans = 0;
    for(int i = 1; i < 1e5 - 1; i++){
        ans = max(ans, x[i-1] + x[i] + x[i+1]);
    }
    cout << ans << endl;

    return 0;
}

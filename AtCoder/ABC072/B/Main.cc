#include <iostream>
#include <algorithm>
#include <vector>
#include <string>

using std::cin;
using std::cout;
using std::endl;

using std::min;
using std::max;

using std::string;

using std::vector;

int main(void) {
    string s;
    cin >> s;

    string t;
    for(size_t i = 0; i < s.size(); i += 2){
        t.push_back(s[i]);
    }
    cout << t << endl;
    return 0;
}

#include <iostream>
#include <algorithm>
#include <vector>

using std::cin;
using std::cout;
using std::endl;

using std::min;
using std::max;

using std::vector;

int main(void) {
    int x, t;
    cin >> x >> t;
    int ans = max(0, x - t);
    cout << ans << endl;
    return 0;
}

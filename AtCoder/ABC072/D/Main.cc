#include <iostream>
#include <algorithm>
#include <vector>
#include <string>

using std::cin;
using std::cout;
using std::endl;

using std::min;
using std::max;
using std::swap;

using std::string;

using std::vector;


int main(void) {
    int n;
    cin >> n;

    vector<int> p(n);
    for(int i = 0; i < n; i++){
        cin >> p[i];
        p[i]--;
    }

    int cnt = 0;
    for(int i = 0; i < n-1; i++){
        if(p[i] == i){
            swap(p[i], p[i+1]);
            cnt++;
        }
    }
    for(int i = n-1; i > 0; i--){
        if(p[i] == i){
            swap(p[i], p[i-1]);
            cnt++;
        }
    }

    cout << cnt << endl;


    return 0;
}

import strutils
import sequtils
import algorithm
import math
import queues
import tables
import logging
import future

const INF* = int(1e18 + 373)

when not defined(release):
  addHandler(newFileLogger(stderr, lvlAll, "[ $levelname ] "))

proc readInt1*(): int = stdin.readLine().strip().parseInt()
proc readInt2*(): (int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1])
proc readInt3*(): (int, int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1], v[2])

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#
proc shift(n: int; s: seq2[char]; a, b: int): seq2[char] =
  var ans = newSeq2[char](n, n)
  for i in 0..<n:
    for j in 0..<n:
      ans[i][j] = s[(i + a) mod n][(j + b) mod n]
  return ans

proc eval(n: int; s: seq2[char]): bool =
  for i in 0..<n:
    for j in i..<n:
      if s[i][j] != s[j][i]:
        return false
  return true

proc main() =
  let n = readInt1()
  var s = newSeq2[char](n, n)
  for i in 0..<n:
    let line = stdin.readLine()
    for j in 0..<n:
      s[i][j] = line[j]

  var ans = 0
  for d in 0..<n:
    let ss = shift(n, s, 0, d)
    if eval(n, ss):
      ans += n
  echo ans



main()


import strutils
import sequtils
import algorithm
import math
import queues
import tables
import logging
import future

const INF* = int(1e18 + 373)

when not defined(release):
  addHandler(newFileLogger(stderr, lvlAll, "[ $levelname ] "))

proc readInt1*(): int = stdin.readLine().strip().parseInt()
proc readInt2*(): (int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1])
proc readInt3*(): (int, int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1], v[2])

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#
proc solve(n: int; c: string; ll, rr: string): int =
  var dp = newSeq[int](n + 1)
  dp[0] = 0
  dp[1] = 1

  for i in 2..n:
    dp[i] = dp[i - 1] + 1
    let cc = c[i-2] & c[i-1]
    if cc == ll or cc == rr:
      dp[i] = min(dp[i], dp[i - 2] + 1)

  return dp[n]


proc main() =
  let n = readInt1()
  let c = stdin.readLine()

  let cmds = @['A', 'B', 'X', 'Y']

  var ans = INF
  for l0 in cmds:
    for l1 in cmds:
      for r0 in cmds:
        for r1 in cmds:
          let ll = l0 & l1
          let rr = r0 & r1
          ans = min(ans, solve(n, c, ll, rr))

  echo ans


main()


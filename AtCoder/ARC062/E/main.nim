import strutils
import sequtils
import algorithm
import math
import queues
import tables
import logging
import future

const INF* = int(1e18 + 373)

when not defined(release):
  addHandler(newFileLogger(stderr, lvlAll, "[ $levelname ] "))

proc readInt1*(): int = stdin.readLine().strip().parseInt()
proc readInt2*(): (int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1])
proc readInt3*(): (int, int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1], v[2])
proc readInt4*(): (int, int, int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1], v[2], v[3])

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#
type Card = seq[int]

proc `<`(c0, c1: Card): bool =
  for k in 0..<4:
    if c0[k] < c1[k]:
      return true
    if c0[k] > c1[k]:
      return false
  return false

proc normalize(c: Card): Card =
  let candi0 = @[ c[0], c[1], c[2], c[3] ]
  let candi1 = @[ c[1], c[2], c[3], c[0] ]
  let candi2 = @[ c[2], c[3], c[0], c[1] ]
  let candi3 = @[ c[3], c[0], c[1], c[2] ]
  return min([candi0, candi1, candi2, candi3])

proc rotate(c: Card): Card = @[ c[3], c[0], c[1], c[2] ]

proc solve(n: int; cs: seq[Card]; top, bottom: Card): int =
  let side = @[
    @[ top[0], bottom[3], bottom[2], top[1] ].normalize(),
    @[ top[1], bottom[2], bottom[1], top[2] ].normalize(),
    @[ top[2], bottom[1], bottom[0], top[3] ].normalize(),
    @[ top[3], bottom[0], bottom[3], top[0] ].normalize()
  ]

  var ans = 1
  for i in 0..<4:
    let s = side[i]
    var sNext = @[ s[0], s[1], s[2], s[3] + 1 ]
    var cnt = cs.lowerBound(sNext) - cs.lowerBound(s)
    if s == top.normalize():
      cnt -= 1
    if s == bottom.normalize():
      cnt -= 1
    for j in 0..<i:
      if s == side[j]:
        cnt -= 1

    var k = 1
    if s[0] == s[1] and s[1] == s[2] and s[2] == s[3]:
      k = 4
    if s[0] == s[2] and s[1] == s[3] and s[0] != s[1]:
      k = 2
    ans *= k * cnt

  return ans

proc main() =
  let n = readInt1()

  var cs = newSeq[Card](n)
  for i in 0..<n:
    cs[i] = readSeq().map(parseInt).normalize()
  cs.sort(cmp[Card])

  var ans = 0
  for i in 0..<n:
    for j in (i + 1)..<n:
      ans += solve(n, cs, cs[i], cs[j])
      ans += solve(n, cs, cs[i], cs[j].rotate())
      ans += solve(n, cs, cs[i], cs[j].rotate().rotate())
      ans += solve(n, cs, cs[i], cs[j].rotate().rotate().rotate())

  echo ans * 2 div 6


main()


import strutils
import sequtils
import algorithm
import math
import queues
import tables
import logging
import future

const INF* = int(1e18 + 373)

when not defined(release):
  addHandler(newFileLogger(stderr, lvlAll, "[ $levelname ] "))

proc readInt1*(): int = stdin.readLine().strip().parseInt()
proc readInt2*(): (int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1])
proc readInt3*(): (int, int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1], v[2])

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#
type Pos = tuple [ x, y: float ]

proc `+`(p1, p2: Pos): Pos = (p1.x + p2.x, p1.y + p2.y)
proc `-`(p1, p2: Pos): Pos = (p1.x - p2.x, p1.y - p2.y)
proc dot(p1, p2: Pos): float = p1.x * p2.x + p1.y * p2.y
proc det(p1, p2: Pos): float = p1.x * p2.y - p1.y * p2.x
proc abs(p: Pos): float = sqrt(p.x * p.x + p.y * p.y)
proc arg(p: Pos): float = arctan2(p.y, p.x)

proc main() =
  let n = readInt1()
  var ps = newSeq[Pos](n)
  for i in 0..<n:
    let (x, y) = readInt2()
    ps[i] = (y.float, x.float)

  var ans = newSeq[float](n)

  for i in 0..<n:
    var th = newSeq[float](0)
    for j in 0..<n:
      if i == j:
        continue
      th.add((ps[j] - ps[i]).arg())

    th.sort(cmp[float])
    th.add(th[0] + 2 * PI)

    debug th
    var maxTh = 0.0
    for i in 0..<th.len() - 1:
      maxTh = max(maxTh, th[i + 1] - th[i])

    if maxTh <= PI:
      ans[i] = 0.0
      continue

    ans[i] = (maxTh - PI) / (2 * PI)

  for i in 0..<n:
    echo ans[i]


main()


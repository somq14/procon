import strutils
import sequtils
import algorithm
import math
import queues

const INF = int(1e18 + 373)
const MOD = int(1e9 + 7)

proc readInt1(): int = stdin.readLine().strip().parseInt()
proc readInt2(): (int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1])
proc readInt3(): (int, int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1], v[2])
proc readInt(): seq[int] = stdin.readLine().strip().split().map(parseInt)

#------------------------------------------------------------------------------#

proc main() =
    var s = stdin.readLine().strip()
    for i in countdown(s.len()-1, 1):
      if s[i] == '9':
        continue
      s[i] = '9'
      for j in countdown(i - 1, 0):
          if s[j] != '0':
              s[j] = chr(ord(s[j]) - 1)
              break
          s[j] = '9'

    var ans = 0
    for i in 0..<s.len():
      ans += ord(s[i]) - ord('0')
    echo ans

main()


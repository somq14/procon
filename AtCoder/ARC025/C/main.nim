import strutils
import sequtils
import algorithm
import math
import queues

const INF = int(1e18 + 373)
const MOD = int(1e9 + 7)

proc readInt1(): int = stdin.readLine().strip().parseInt()
proc readInt2(): (int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1])
proc readInt3(): (int, int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1], v[2])
proc readInt4(): (int, int, int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1], v[2], v[3])
proc readInt(): seq[int] = stdin.readLine().strip().split().map(parseInt)

#------------------------------------------------------------------------------#

# FIXME: Use object
type PriorityQueue[T] = tuple[a: seq[T], n: Natural, cmp: proc(a, b: T): bool]

proc initPriorityQueue[T](cmp: proc(a, b: T): bool): PriorityQueue[T] =
    (newSeq[T](1), Natural(0), cmp)

proc len[T](this: PriorityQueue[T]): Natural = this.n

proc enqueue[T](this: var PriorityQueue[T], e: T) =
    this.n += 1
    this.a.add(e)
    var i = this.a.len() - 1
    while i > 1 and this.cmp(this.a[i], this.a[i div 2]):
        swap(this.a[i], this.a[i div 2])
        i = i div 2

proc dequeue[T](this: var PriorityQueue[T]): T =
    this.n -= 1
    result = this.a[1]
    this.a[1] = this.a.pop()
    var i = 1
    while i < this.a.len():
        var j = i
        if i * 2 < this.a.len() and this.cmp(this.a[i * 2], this.a[j]):
            j = i * 2
        if i * 2 + 1 < this.a.len() and this.cmp(this.a[i * 2 + 1], this.a[j]):
            j = i * 2 + 1
        if i == j:
            break
        swap(this.a[i], this.a[j])
        i = j

# FIXME: throw Exception when this is empty
proc front[T](this: PriorityQueue[T]): T = this.a[1]

#------------------------------------------------------------------------------#
type Edge = tuple[a, b, c: int]


proc dijkstra(n: int, g: seq[seq[Edge]], s: int): seq[int] =
    var d = newSeq[int](n)
    d.fill(INF)
    d[s] = 0

    type P = tuple[d, v: int]
    proc `<`(p1, p2: P): bool = p1.d < p2.d
    var q = initPriorityQueue[P](`<`)
    q.enqueue((0, s))

    while q.len() > 0:
        let (c, v) = q.dequeue()
        if c > d[v]:
            continue
        for e in g[v]:
            let ab = d[e.a] + e.c
            if ab < d[e.b]:
                d[e.b] = ab
                q.enqueue((ab, e.b))
    return d

proc main() =
    let (n, m, r, t) = readInt4()

    var g = newSeqWith(n, newSeq[Edge]())
    for i in 0..<m:
        let (a, b, c) = readInt3()
        g[a - 1].add((a - 1, b - 1, c))
        g[b - 1].add((b - 1, a - 1, c))

    var ans = 0
    for s in 0..<n:
        let d = dijkstra(n, g, s)
        let rTime = map(d, proc(a: int): float = float(a) / float(r)).sorted(system.cmp[float])
        let tTime = map(d, proc(a: int): float = float(a) / float(t)).sorted(system.cmp[float])

        var j = 1
        for i in rTime[1..^1]:
            while j < tTime.len() and tTime[j] < i:
                j += 1
            ans += (j - 1)
            # consider the case of A == B
            if t > r:
                ans -= 1

    echo ans

main()


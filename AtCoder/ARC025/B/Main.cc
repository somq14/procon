#include <algorithm>
#include <cmath>
#include <complex>
#include <iomanip>
#include <iostream>
#include <string>
#include <utility>
#include <vector>

using namespace std;

using ll = long long int;
const int MOD = 1e9 + 7;
const int INF = 1e9 + 373;

#define rep(i, n) for (int i = 0; i < (n); ++i)

template <typename T>
using vector2 = vector<vector<T>>;
template <typename T>
vector2<T> init_vector2(size_t n0, size_t n1, T e = T()) {
    return vector2<T>(n0, vector<T>(n1, e));
}

template <typename T>
using vector3 = vector<vector<vector<T>>>;
template <typename T>
vector3<T> init_vector3(size_t n0, size_t n1, size_t n2, T e = T()) {
    return vector3<T>(n0, vector2<T>(n1, vector<T>(n2, e)));
}

int main() {
    int h, w;
    cin >> h >> w;

    vector2<int> c = init_vector2<int>(h, w);
    rep(i, h) {
        rep(j, w) { cin >> c[i][j]; }
    }

    vector2<int> a0 = init_vector2<int>(h + 1, w + 1);
    for (int i = 1; i <= h; i++) {
        for (int j = 1; j <= w; j++) {
            a0[i][j] = a0[i - 1][j] + a0[i][j - 1] - a0[i - 1][j - 1];
            if ((i + j) % 2 == 0) {
                a0[i][j] += c[i - 1][j - 1];
            }
        }
    }

    vector2<int> a1 = init_vector2<int>(h + 1, w + 1);
    for (int i = 1; i <= h; i++) {
        for (int j = 1; j <= w; j++) {
            a1[i][j] = a1[i - 1][j] + a1[i][j - 1] - a1[i - 1][j - 1];
            if ((i + j) % 2 == 1) {
                a1[i][j] += c[i - 1][j - 1];
            }
        }
    }

    int ans = 0;
    for (int sy = 0; sy < h; sy++) {
        for (int sx = 0; sx < w; sx++) {
            for (int ty = sy + 1; ty <= h; ty++) {
                for (int tx = sx + 1; tx <= w; tx++) {
                    int sum0 =
                        a0[ty][tx] - a0[sy][tx] - a0[ty][sx] + a0[sy][sx];
                    int sum1 =
                        a1[ty][tx] - a1[sy][tx] - a1[ty][sx] + a1[sy][sx];
                    if (sum0 == sum1) {
                        ans = max(ans, (ty - sy) * (tx - sx));
                    }
                }
            }
        }
    }
    cout << ans << endl;

    return 0;
}

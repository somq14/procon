#include <algorithm>
#include <complex>
#include <iostream>
#include <queue>
#include <set>
#include <stack>
#include <string>
#include <vector>

using std::cin;
using std::cout;
using std::endl;
using std::cerr;

using std::string;
using std::to_string;
using std::vector;
using std::set;
using std::queue;
using std::stack;
using std::priority_queue;

using std::min;
using std::max;
using std::sort;
using std::abs;

typedef long long int ll;
const int MOD = 1e9 + 7;

int main() {
    ll n;
    cin >> n;

    ll x, y;
    cin >> x >> y;
    for (ll i = 0; i < n - 1; i++) {
        ll t, a;
        cin >> t >> a;

        ll r = max((x + t - 1) / t, (y + a - 1) / a);
        x = t * r;
        y = a * r;
    }

    cout << (x + y) << endl;

    return 0;
}

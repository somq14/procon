#include <algorithm>
#include <complex>
#include <iostream>
#include <queue>
#include <set>
#include <stack>
#include <string>
#include <vector>

using std::cin;
using std::cout;
using std::endl;
using std::cerr;

using std::string;
using std::to_string;
using std::vector;
using std::set;
using std::queue;
using std::stack;
using std::priority_queue;

using std::min;
using std::max;
using std::sort;
using std::abs;

typedef long long int ll;
const int MOD = 1e9 + 7;

int main() {
    string s;
    cin >> s;

    int cnt = 0;
    int win  = 0;
    int lose = 0;
    for (size_t i = 0; i < s.length(); i++) {
        if (s[i] == 'p') {
            if(cnt > 0){
                cnt--;
            }else{
                cnt++;
                lose++;
            }
        } else {
            if(cnt > 0){
                win++;
                cnt--;
            }else{
                cnt++;
            }
        }
    }

    cout << (win - lose) << endl;

    return 0;
}

#include <algorithm>
#include <cctype>
#include <complex>
#include <iomanip>
#include <iostream>
#include <queue>
#include <set>
#include <string>
#include <vector>

using std::cin;
using std::cout;
using std::endl;

using std::string;
using std::to_string;
using std::vector;
using std::set;
using std::queue;
using std::priority_queue;

using std::min;
using std::max;
using std::sort;
using std::abs;

int main(void) {
    int n;
    cin >> n;

    vector<long> a(n);
    for (int i = 0; i < n; i++) {
        cin >> a[i];
    }

    int p = 0;
    for (int i = 0; i < n; i++) {
        if (abs(a[i]) > abs(a[p])) {
            p = i;
        }
    }

    vector<int> x;
    vector<int> y;
    if (a[p] > 0) {
        for (int i = 0; i < n; i++) {
            if (a[i] >= 0) {
                continue;
            }
            a[i] += a[p];
            y.push_back(i);
            x.push_back(p);
        }

        for (int i = 0; i < n - 1; i++) {
            if (a[i] > a[i + 1]) {
                a[i + 1] += a[i];
                y.push_back(i + 1);
                x.push_back(i);
            }
        }
    } else {
        for (int i = 0; i < n; i++) {
            if (a[i] <= 0) {
                continue;
            }
            a[i] += a[p];
            y.push_back(i);
            x.push_back(p);
        }

        for (int i = n - 1; i > 0; i--) {
            if (a[i - 1] > a[i]) {
                a[i - 1] += a[i];
                y.push_back(i - 1);
                x.push_back(i);
            }
        }
    }

    int len = x.size();
    cout << len << endl;
    for (int i = 0; i < len; i++) {
        cout << x[i] + 1 << " " << y[i] + 1 << endl;
    }

    return 0;
}

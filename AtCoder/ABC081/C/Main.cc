#include <algorithm>
#include <cctype>
#include <complex>
#include <iomanip>
#include <iostream>
#include <queue>
#include <set>
#include <string>
#include <vector>

using std::cin;
using std::cout;
using std::endl;

using std::string;
using std::to_string;
using std::vector;
using std::set;
using std::queue;
using std::priority_queue;

using std::min;
using std::max;
using std::sort;
using std::abs;

int main(void) {
    const int AMAX = 200000;
    int n, k;
    cin >> n >> k;

    vector<int> a(AMAX + 1);
    for (int i = 0; i < n; i++) {
        int x;
        cin >> x;
        a[x]++;
    }

    sort(a.begin(), a.end());

    int p = -1;
    for(int i = 0; i <= AMAX; i++){
        if(a[i] != 0){
            p = i;
            break;
        }
    }

    int del = max(0UL, (a.size() - p) - k);

    long cnt = 0;
    for(int i = 0; i < del; i++){
        cnt += a[p + i];
    }

    cout << cnt << endl;

    return 0;
}

#include <stdio.h>

int main(void) {
    int r, g;
    scanf("%d%d", &r, &g);
    // (r + x) / 2 = g
    // (r + x) = 2 * g
    // x = 2 * g - r
    printf("%d\n", 2 * g - r);
    return 0;
}

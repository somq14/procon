#include <stdio.h>

int main(void){
    int n, k;
    int i;
    int v;

    scanf("%d%d", &n, &k);

    v = 1;
    for(i = 0; i < n; i++){
        int op_a = v * 2;
        int op_b = v + k;
        v = op_a < op_b ? op_a : op_b;
    }

    printf("%d\n", v);

    return 0;
}

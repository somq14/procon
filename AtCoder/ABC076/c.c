#include <stdio.h>
#include <string.h>

#define N 50

int match(const char *s1, const char *s2, int len) {
    int i;
    for (i = 0; i < len; i++) {
        if (s1[i] != '?' && s2[i] != '?' && s1[i] != s2[i]) {
            return 0;
        }
    }
    return 1;
}

int main(void) {
    int i;
    int match_index;
    int s_len;
    char s[N + 1];
    int t_len;
    char t[N + 1];

    scanf("%s", s);
    s_len = strlen(s);

    scanf("%s", t);
    t_len = strlen(t);

    match_index = -1;
    for (i = s_len - t_len; i >= 0; i--) {
        if (match(s + i, t, t_len)) {
            match_index = i;
            break;
        }
    }

    if (match_index == -1) {
        printf("UNRESTORABLE\n");
    } else {
        for (i = 0; i < t_len; i++) {
            s[match_index + i] = t[i];
        }
        for (i = 0; i < s_len; i++) {
            s[i] = s[i] == '?' ? 'a' : s[i];
        }
        printf("%s\n", s);
    }

    return 0;
}

import strutils
import sequtils
import algorithm
import math
import queues
import tables
import logging
import future

const INF* = int(1e18 + 373)

when not defined(release):
  addHandler(newFileLogger(stderr, lvlAll, "[ $levelname ] "))

proc readInt1*(): int = stdin.readLine().strip().parseInt()
proc readInt2*(): (int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1])
proc readInt3*(): (int, int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1], v[2])
proc readInt4*(): (int, int, int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1], v[2], v[3])

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#
type Range = tuple [ ll, rr: int ]

type Dat = tuple [ v, i: int ]

proc solve1(n: int; rs: seq[Range]): int =
  var used = newSeq[bool](n)
  used.fill(false)

  var rightQueue = newSeq[Dat](n)
  for i in 0..<n:
    rightQueue[i] = (rs[i].ll, i)
  rightQueue.sort(cmp[Dat], Descending)

  var leftQueue = newSeq[Dat](n)
  for i in 0..<n:
    leftQueue[i] = (rs[i].rr, i)
  leftQueue.sort(cmp[Dat], Ascending)

  var ans = 0
  var pos = 0
  var lp = 0
  var rp = 0
  var first = true
  while true:
    while lp < n and used[leftQueue[lp].i]:
      lp += 1
    while rp < n and used[rightQueue[rp].i]:
      rp += 1

    if lp >= n and rp >= n:
      break

    var ld = 0
    if lp < n:
      let r = rs[leftQueue[lp].i]
      if pos in (r.ll)..(r.rr):
        ld = 0
      else:
        ld = min(abs(r.ll - pos), abs(r.rr - pos))

    var rd = 0
    if rp < n:
      let r = rs[rightQueue[rp].i]
      if pos in (r.ll)..(r.rr):
        rd = 0
      else:
        rd = min(abs(r.ll - pos), abs(r.rr - pos))

    if ld == 0 and rd == 0:
      break

    if not first and ld >= rd:
      used[leftQueue[lp].i] = true
      let r = rs[leftQueue[lp].i]
      ans += ld
      if pos notin (r.ll)..(r.rr):
        if abs(r.ll - pos) <= abs(r.rr - pos):
          pos = r.ll
        else:
          pos = r.rr
    else:
      first = false
      used[rightQueue[rp].i] = true
      let r = rs[rightQueue[rp].i]
      ans += rd
      if pos notin (r.ll)..(r.rr):
        if abs(r.ll - pos) <= abs(r.rr - pos):
          pos = r.ll
        else:
          pos = r.rr

  ans += abs(pos)
  return ans

proc solve2(n: int; rs: seq[Range]): int =
  var used = newSeq[bool](n)
  used.fill(false)

  var rightQueue = newSeq[Dat](n)
  for i in 0..<n:
    rightQueue[i] = (rs[i].ll, i)
  rightQueue.sort(cmp[Dat], Descending)

  var leftQueue = newSeq[Dat](n)
  for i in 0..<n:
    leftQueue[i] = (rs[i].rr, i)
  leftQueue.sort(cmp[Dat], Ascending)

  var ans = 0
  var pos = 0
  var lp = 0
  var rp = 0
  var first = true
  while true:
    while lp < n and used[leftQueue[lp].i]:
      lp += 1
    while rp < n and used[rightQueue[rp].i]:
      rp += 1

    if lp >= n and rp >= n:
      break

    var ld = 0
    if lp < n:
      let r = rs[leftQueue[lp].i]
      if pos in (r.ll)..(r.rr):
        ld = 0
      else:
        ld = min(abs(r.ll - pos), abs(r.rr - pos))

    var rd = 0
    if rp < n:
      let r = rs[rightQueue[rp].i]
      if pos in (r.ll)..(r.rr):
        rd = 0
      else:
        rd = min(abs(r.ll - pos), abs(r.rr - pos))

    if ld == 0 and rd == 0:
      break

    if first or ld >= rd:
      first = false
      used[leftQueue[lp].i] = true
      let r = rs[leftQueue[lp].i]
      ans += ld
      if pos notin (r.ll)..(r.rr):
        if abs(r.ll - pos) <= abs(r.rr - pos):
          pos = r.ll
        else:
          pos = r.rr
    else:
      used[rightQueue[rp].i] = true
      let r = rs[rightQueue[rp].i]
      ans += rd
      if pos notin (r.ll)..(r.rr):
        if abs(r.ll - pos) <= abs(r.rr - pos):
          pos = r.ll
        else:
          pos = r.rr

  ans += abs(pos)
  return ans

proc main() =
  let n = readInt1()

  var rs = newSeq[Range](n)
  for i in 0..<n:
    rs[i] = readInt2()

  echo max(solve1(n, rs), solve2(n, rs))


main()


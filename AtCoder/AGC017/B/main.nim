import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import logging
import future

const INF* = int(1e18 + 373)

when not defined(release):
  addHandler(newFileLogger(stderr, lvlAll, "[ $levelname ] "))

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

proc readInt1*(): int = readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#
proc solve(n, a, b, c, d: int): bool =
  let x = b - a

  for i in 0..<n:
    let s1 = i * c
    let t1 = i * d + 1
    let s2 = (n - 1 - i) * c
    let t2 = (n - 1 - i) * d + 1
    let s3 = s2 + x
    let t3 = t2 + x
    if not (t3 <= s1 or t1 <= s3):
      return true
  return false

proc main() =
  let tmp = readSeq().map(parseInt)
  let n = tmp[0]
  let a = tmp[1]
  let b = tmp[2]
  let c = tmp[3]
  let d = tmp[4]
  echo if solve(n, a, b, c, d): "YES" else: "NO"

main()


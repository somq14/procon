import strutils
import sequtils
import algorithm
import math
import queues
import tables
import logging

const INF* = int(1e18 + 373)

when not defined(release):
  addHandler(newFileLogger(stderr, lvlAll, "[ $levelname ] "))

proc readInt1*(): int = stdin.readLine().strip().parseInt()
proc readInt2*(): (int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1])
proc readInt3*(): (int, int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1], v[2])

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#

proc main() =
  var (a, b, c) = readInt3()

  var cnt = 0
  while cnt < int(1e8) and a mod 2 == 0 and b mod 2 == 0 and c mod 2 == 0:
    cnt += 1
    let a2 = a div 2
    let b2 = b div 2
    let c2 = c div 2
    a = b2 + c2
    b = c2 + a2
    c = a2 + b2

  if cnt >= int(1e8):
    echo -1
  else:
    echo cnt

main()


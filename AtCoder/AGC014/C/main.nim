import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import logging
import future

const INF* = int(1e18 + 373)

when not defined(release):
  addHandler(newFileLogger(stderr, lvlAll, "[ $levelname ] "))

proc readInt1*(): int = stdin.readLine().strip().parseInt()
proc readInt2*(): (int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1])
proc readInt3*(): (int, int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1], v[2])
proc readInt4*(): (int, int, int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1], v[2], v[3])

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#
type Pos = tuple [ y, x: int ]

let dy4 = [ 1, -1, 0, 0 ]
let dx4 = [ 0,  0, 1,-1 ]

proc bfs(h, w: int; a: seq2[char]; sy, sx: int): seq2[int] =
  var q = initQueue[Pos]()
  q.enqueue((sy, sx))

  var d = newSeq2[int](h, w)
  for y in 0..<h:
    for x in 0..<w:
      d[y][x] = INF

  d[sy][sx] = 0
  while q.len() > 0:
    let (y, x) = q.dequeue()
    for k in 0..<4:
      let (ny, nx) = (y + dy4[k], x + dx4[k])
      if ny notin 0..<h or nx notin 0..<w:
        continue
      if a[ny][nx] == '.' and d[y][x] + 1 < d[ny][nx]:
        d[ny][nx] = d[y][x] + 1
        q.enqueue((ny, nx))

  return d

proc main() =
  let (h, w, k) = readInt3()

  var a = newSeq2[char](h, 0)
  for i in 0..<h:
    a[i] = stdin.readLine().map(it => it)

  var sy = -1
  var sx = -1
  block search_takahashikun:
    for y in 0..<h:
      for x in 0..<w:
        if a[y][x] == 'S':
          sy = y
          sx = x
          a[sy][sx] = '.'
          break search_takahashikun

  let d = bfs(h, w, a, sy, sx)
  debug d

  var opt = INF
  for y in 0..<h:
    for x in 0..<w:
      if d[y][x] <= k:
        let v = min(@[ y, abs(y - (h - 1)), x, abs(x - (w - 1))])
        opt = min(opt, v)

  let ans = 1 + (opt + k - 1) div k
  echo ans

main()


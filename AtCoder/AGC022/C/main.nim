import strutils
import sequtils
import algorithm
import math
import queues
import tables
import logging
import future

const INF* = int(1e18 + 373)

when not defined(release):
  addHandler(newFileLogger(stderr, lvlAll, "[ $levelname ] "))

proc readInt1*(): int = stdin.readLine().strip().parseInt()
proc readInt2*(): (int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1])
proc readInt3*(): (int, int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1], v[2])

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#

proc warshallfloyd(n: int; g: seq2[int]): seq2[int] =
  var g = g
  for k in 0..<n:
    for i in 0..<n:
      for j in 0..<n:
        let tmp = max(g[i][k], g[k][j])
        if tmp < g[i][j]:
          g[i][j] = tmp
  return g

proc main() =
  let M = 50
  let n = readInt1()
  let a = readSeq().map(parseInt)
  let b = readSeq().map(parseInt)

  var g = newSeq2[int](M + 1, M + 1)
  for i in 0..M:
    for j in 0..M:
      g[i][j] = INF

  for i in 0..M:
    g[i][i] = 0

  for i in 0..M:
    for k in 1..M:
      let j = i mod k
      g[i][j] = min(g[i][j], k)

  var g_min = warshallfloyd(M + 1, g)
  for i in 0..<n:
    if g[a[i]][b[i]] >= INF:
      echo "-1"
      return

  var ans = 0
  while true:
    var max_cost = -1
    for i in 0..<n:
      max_cost = max(max_cost, g_min[a[i]][b[i]])

    if max_cost == 0:
      break

    ans += 1 shl max_cost

    for i in 0..M:
      let j = i mod max_cost
      g[i][j] = 0

    g_min = warshallfloyd(M + 1, g)

  echo ans

main()


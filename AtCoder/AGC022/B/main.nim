import strutils
import sequtils
import algorithm
import math
import queues
import tables
import logging
import future

const INF* = int(1e18 + 373)

when not defined(release):
  addHandler(newFileLogger(stderr, lvlAll, "[ $levelname ] "))

proc readInt1*(): int = stdin.readLine().strip().parseInt()
proc readInt2*(): (int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1])
proc readInt3*(): (int, int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1], v[2])

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#
#------------------------------------------------------------------------------#

const MaxPrime = int(1e5 + 1)

proc calcPrimeTable(maxn: int): seq[bool] =
  result = newSeq[bool](maxn + 1)
  result.fill(true)
  result[0] = false
  result[1] = false

  for i in 2..maxn:
    if not result[i]:
      continue
    var j = i * 2
    while j <= maxn:
      result[j] = false
      j += i

let primeTable = calcPrimeTable(MaxPrime)

proc calcPrimeList(maxn: int): seq[int] =
  result = newSeq[int](0)
  for i in 0..maxn:
    if primeTable[i]:
      result.add(i)

let primeList = calcPrimeList(MaxPrime)

proc factorize(n: int): seq[int] =
  result = newSeq[int](0)
  var m = n
  for p in primeList:
    if m < p:
      break
    while m mod p == 0:
      result.add(p)
      m = m div p
  if m != 1:
    result.add(m)

#------------------------------------------------------------------------------#

proc main() =
  let n = readInt1()

  if n == 3:
    echo "2 5 63"
    return

  var ans = @[2, 3, 5, 20]

  for pat in [[1, 0, 0], [0, 1, 0], [0, 0, 1], [1, 1, 0], [1, 0, 1], [0, 1, 1], [1, 1, 1]]:
    var tmp: seq[int] = @[]
    var tmp_sum = 0
    for i in 21..30000:
      let m2 = if i mod 2 == 0: 1 else: 0
      let m3 = if i mod 3 == 0: 1 else: 0
      let m5 = if i mod 5 == 0: 1 else: 0
      if not (m2 == pat[0] and m3 == pat[1] and m5 == pat[2]):
        continue

      tmp_sum += i
      tmp.add(i)

      if tmp_sum mod 30 == 0:
        if tmp.len() + ans.len() > n:
          break

        for x in tmp:
          ans.add(x)

        tmp = @[]
        tmp_sum = 0

  echo ans.map(it => $it).join(" ")

main()


import strutils
import sequtils
import algorithm
import math
import queues
import tables
import logging
import future

const INF* = int(1e18 + 373)

when not defined(release):
  addHandler(newFileLogger(stderr, lvlAll, "[ $levelname ] "))

proc readInt1*(): int = stdin.readLine().strip().parseInt()
proc readInt2*(): (int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1])
proc readInt3*(): (int, int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1], v[2])

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#

proc main() =
  var s = stdin.readLine()
  let n = s.len()

  for i in 0..<n:
    for j in (i + 1)..<n:
      if s[i] == s[j]:
        echo "-1"
        return

  if n < 26:
    for c in 'a'..'z':
      if s.find(c) == -1:
        echo(s & $c)
        return

  if s == "zyxwvutsrqponmlkjihgfedcba":
    echo "-1"
    return

  var a = newSeq[char](n)
  for i in 0..<n:
    a[i] = s[i]

  for i in countdown(n - 2, 0):
    var p = -1
    for j in (i+1)..<n:
      if a[j] > a[i] and (p == -1 or a[j] < a[p]):
        p = j

    if p == -1:
      continue

    swap(a[i], a[p])
    let a0 = a[0..i]
    let ans = a0.map(it => $it).join()
    echo ans
    return


main()


import strutils
import sequtils
import algorithm
import math
import queues
import tables
import logging
import future

const INF* = int(1e18 + 373)

when not defined(release):
  addHandler(newFileLogger(stderr, lvlAll, "[ $levelname ] "))

proc readInt1*(): int = stdin.readLine().strip().parseInt()
proc readInt2*(): (int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1])
proc readInt3*(): (int, int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1], v[2])

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#

proc main() =
  let (n, d) = readInt2()
  let ds = readSeq().map(parseInt)
  let q = readInt1()
  let qs = readSeq().map(parseInt).map(it => it - 1)

  var dp = newSeq[int](n + 1)
  dp[n] = 1
  for i in countdown(n - 1, 0):
    if ds[i] >= dp[i + 1] * 2:
      dp[i] = dp[i + 1]
    else:
      dp[i] = dp[i + 1] + ds[i]

  var ps = newSeq[int](n + 1)
  ps[0] = d
  for i in 1..n:
    ps[i] = min(ps[i - 1], abs(ps[i - 1] - ds[i - 1]))

  for i in 0..<q:
    if dp[qs[i] + 1] <= ps[qs[i]]:
      echo "YES"
    else:
      echo "NO"

main()


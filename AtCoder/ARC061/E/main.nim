import strutils
import sequtils
import algorithm
import math
import queues
import tables
import logging
import future
import sets

const INF* = int(1e18 + 373)

when not defined(release):
  addHandler(newFileLogger(stderr, lvlAll, "[ $levelname ] "))

proc readInt1*(): int = stdin.readLine().strip().parseInt()
proc readInt2*(): (int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1])
proc readInt3*(): (int, int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1], v[2])

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#
# FIXME: Use object
type PriorityQueue[T] = tuple[a: seq[T], n: Natural, cmp: proc(a, b: T): bool]

proc initPriorityQueue[T](cmp: proc(a, b: T): bool): PriorityQueue[T] =
    (newSeq[T](1), Natural(0), cmp)

proc len[T](this: PriorityQueue[T]): Natural = this.n

proc enqueue[T](this: var PriorityQueue[T], e: T) =
    this.n += 1
    this.a.add(e)
    var i = this.a.len() - 1
    while i > 1 and this.cmp(this.a[i], this.a[i div 2]):
        swap(this.a[i], this.a[i div 2])
        i = i div 2

proc dequeue[T](this: var PriorityQueue[T]): T =
    this.n -= 1
    result = this.a[1]
    this.a[1] = this.a.pop()
    var i = 1
    while i < this.a.len():
        var j = i
        if i * 2 < this.a.len() and this.cmp(this.a[i * 2], this.a[j]):
            j = i * 2
        if i * 2 + 1 < this.a.len() and this.cmp(this.a[i * 2 + 1], this.a[j]):
            j = i * 2 + 1
        if i == j:
            break
        swap(this.a[i], this.a[j])
        i = j

# FIXME: throw Exception when this is empty
proc front[T](this: PriorityQueue[T]): T = this.a[1]
#------------------------------------------------------------------------------#
type Edge = tuple[v, c: int]

proc f(a, b: (int, Edge)): bool = a[0] < b[0]
proc `<`(a, b: Edge): bool = a.c < b.c

proc main() =
  let (n, m) = readInt2()

  var g = newSeq2[Edge](n, 0)
  for i in 0..<m:
    let (p, q, c) = readInt3()
    g[p - 1].add((q - 1, c))
    g[q - 1].add((p - 1, c))

  for i in 0..<n:
    g[i].sort(cmp[Edge])

  var q = initPriorityQueue[(int, Edge)](f)
  q.enqueue((0, (n - 1, -1)))

  var d = initTable[Edge, int](1 shl 20)
  for v in 0..<n:
    d[(v, -1)] = INF
    for e in g[v]:
      d[e] = INF
  d[(n - 1, -1)] = 0

  while q.len() > 0:
    let (e1Cost, e1) = q.dequeue()

    if e1.v == 0 and e1.c == -1:
      break

    if e1Cost > d[e1]:
      continue

    if e1.c != -1:
      let e2Cost = e1Cost
      let si = g[e1.v].lowerBound((0, e1.c))
      let ti = g[e1.v].lowerBound((0, e1.c + 1))
      for i in si..<ti:
        let e2 = g[e1.v][i]
        if e1.c == e2.c and e2Cost < d[e2]:
          d[e2] = e2Cost
          q.enqueue((e2Cost, e2))

      let getoff: Edge = (e1.v, -1)
      if e2Cost < d[getoff]:
        d[getoff] = e2Cost
        q.enqueue((e2Cost, getoff))
    else:
      let e2Cost = e1Cost + 1
      for e2 in g[e1.v]:
        if e2Cost < d[e2]:
          d[e2] = e2Cost
          q.enqueue((e2Cost, e2))

  if d[(0, -1)] >= INF:
    echo -1
  else:
    echo d[(0, -1)]

main()



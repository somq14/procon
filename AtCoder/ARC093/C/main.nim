import strutils
import sequtils
import algorithm
import math
import queues
import tables
import logging
import future

const INF* = int(1e18 + 373)

when not defined(release):
  addHandler(newFileLogger(stderr, lvlAll, "[ $levelname ] "))

proc readInt1*(): int = stdin.readLine().strip().parseInt()
proc readInt2*(): (int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1])
proc readInt3*(): (int, int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1], v[2])

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#

proc main() =
  let n = readInt1()
  let a = @[0].concat(readSeq().map(parseInt)).concat(@[0])

  var dp1 = newSeq[int](n + 2)
  dp1[0] = 0
  for i in 1..(n + 1):
    dp1[i] = dp1[i - 1] + abs(a[i - 1] - a[i])

  var dp2 = newSeq[int](n + 2)
  dp2[n + 1] = 0
  for i in countdown(n, 0):
    dp2[i] = abs(a[i] - a[i + 1]) + dp2[i + 1]

  for i in 1..n:
    let ans = dp1[i - 1] + abs(a[i - 1] - a[i + 1]) + dp2[i + 1]
    echo ans


main()


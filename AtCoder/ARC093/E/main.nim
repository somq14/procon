import strutils
import sequtils
import algorithm
import math
import queues
import tables
import logging
import future

const INF* = int(1e18 + 373)

when not defined(release):
  addHandler(newFileLogger(stderr, lvlAll, "[ $levelname ] "))

proc readInt1*(): int = stdin.readLine().strip().parseInt()
proc readInt2*(): (int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1])
proc readInt3*(): (int, int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1], v[2])

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

type UnionFindTree = tuple[ p, h, w: seq[int] ]

proc initUnionFindTree(n: int): UnionFindTree =
    var p = newSeq[int](n)
    p.fill(-1)
    var h = newSeq[int](n)
    h.fill(0)
    var w = newSeq[int](n)
    w.fill(1)
    return (p, h, w)

proc find(this: var UnionFindTree, v: int): int =
    if this.p[v] == -1:
        return v
    this.p[v] = find(this, this.p[v])
    return this.p[v]

proc same(this: var UnionFindTree, u, v: int): bool =
    this.find(u) == this.find(v)

proc union(this: var UnionFindTree, u, v: int) =
    let uRoot = this.find(u)
    let vRoot = this.find(v)
    if uRoot == vRoot:
        return

    if this.h[uRoot] > this.h[vRoot]:
        this.p[vRoot] = uRoot
        this.w[uRoot] += this.w[vRoot]
    else:
        this.p[uRoot] = vRoot
        this.w[vRoot] += this.w[uRoot]
        if this.h[uRoot] == this.h[vRoot]:
            this.h[vRoot] += 1
#------------------------------------------------------------------------------#
#------------------------------------------------------------------------------#
const MOD = int(1e9 + 7)

type Op1[T] = proc(a: T, b: T): T
type Op2[T] = proc(a: T, b: T): T

proc repeatedSquares[T](x: T, n: Natural, e: T, f: Op2[T]): T =
    result = e
    var xx = f(x, e)
    var i = n
    while i > 0:
        if (i and 1) != 0:
            result = f(result, xx)
        xx = f(xx, xx)
        i = i shr 1


proc addM(a, b: Natural): Natural = (a + b) mod MOD
proc subM(a, b: Natural): Natural = (a - b + MOD) mod MOD
proc mulM(a, b: Natural): Natural = a * b mod MOD
proc powM(a, b: Natural): Natural = repeatedSquares[Natural](a, b, 1, `mulM`)
proc invM(a: Natural): Natural = a.powM(MOD - 2)
proc divM(a, b: Natural): Natural = a.mulM(invM(b))


var memoFactM = @[Natural(1)]
proc factM(n: Natural): Natural =
    if n < memoFactM.len():
        return memoFactM[n]
    for i in memoFactM.len()..n:
        memoFactM.add(memoFactM[i - 1].mulM(i))
    return memoFactM[n]


proc combM(n, r: Natural): Natural =
    if r > n:
        return 0
    if r > n div 2:
        return combM(n, n - r)

    result = 1
    for i in ((n - r) + 1)..n:
        result = result.mulM(i)
    result = result.divM(factM(r))


proc multCombM(n, r: Natural): Natural = combM(n + r - 1, n - 1)

#------------------------------------------------------------------------------#
type Edge = tuple [ w, u, v: int ]

proc minimumSpanningTree(n: int; es: seq[Edge]): int =
  assert es.isSorted(cmp[Edge])
  var uft = initUnionFindTree(n)

  var sum = 0
  for e in es:
    if uft.same(e.u, e.v):
      continue
    uft.union(e.u, e.v)
    sum += e.w
  return sum

proc minimumSpanningTree(n: int; es: seq[Edge]; i: int): int =
  assert es.isSorted(cmp[Edge])
  var uft = initUnionFindTree(n)

  var sum = es[i].w
  uft.union(es[i].u, es[i].v)

  for e in es:
    if uft.same(e.u, e.v):
      continue
    uft.union(e.u, e.v)
    sum += e.w
  return sum

proc main() =
  let (n, m) = readInt2()
  let x = readInt1()

  var es = newSeq[Edge](m)
  for i in 0..<m:
    let (u, v, w) = readInt3()
    es[i] = (w, u - 1, v - 1)
  es.sort(cmp[Edge])

  let opt = minimumSpanningTree(n, es)
  if opt > x:
    echo 0
    return

  var cntLt = 0
  var cntEq = 0
  var cntGt = 0
  for i in 0..<m:
    let v = minimumSpanningTree(n, es, i)
    if v == x:
      cntEq += 1
    elif v < x:
      cntLt += 1
    elif v > x:
      cntGt += 1
    else:
      assert false


  debug cntLt
  debug cntEq
  debug cntGt

  if opt == x:
    var ans = 2.powM(cntGt).mulM(2.powM(cntEq) - 2)
    echo ans
    return

  var ans = 2
  ans = ans.mulM(2.powM(cntGt))
  ans = ans.mulM(2.powM(cntEq).subM(1))
  echo ans

main()


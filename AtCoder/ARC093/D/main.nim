import strutils
import sequtils
import algorithm
import math
import queues
import tables
import logging
import future

const INF* = int(1e18 + 373)

when not defined(release):
  addHandler(newFileLogger(stderr, lvlAll, "[ $levelname ] "))

proc readInt1*(): int = stdin.readLine().strip().parseInt()
proc readInt2*(): (int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1])
proc readInt3*(): (int, int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1], v[2])

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#

proc main() =
  let (w, b) = readInt2()
  let n = 100
  var s = newSeq2[bool](n, n)

  for i in 0..<(n div 2):
    for j in 0..<n:
      s[i][j] = true
  for i in (n div 2)..<n:
    for j in 0..<n:
      s[i][j] = false

  var bcnt = 1
  var wcnt = 1

  var y = 1
  var x = 1
  while bcnt < b:
    s[y][x] = false
    bcnt += 1

    x += 2
    if x >= n - 1:
      x = 1
      y += 2

  y = n - 2
  x = 1
  while wcnt < w:
    s[y][x] = true
    wcnt += 1

    x += 2
    if x >= n - 1:
      x = 1
      y -= 2


  echo n, " ", n
  for i in 0..<n:
    echo s[i].map(x => (if x: "." else: "#")).join("")




main()


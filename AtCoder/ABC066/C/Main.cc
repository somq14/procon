#include <algorithm>
#include <complex>
#include <iomanip>
#include <iostream>
#include <queue>
#include <set>
#include <string>
#include <vector>

using std::cin;
using std::cout;
using std::endl;

using std::string;
using std::to_string;
using std::vector;
using std::set;
using std::queue;
using std::priority_queue;

using std::min;
using std::max;
using std::sort;
using std::abs;

int main(void) {
    int n;
    cin >> n;

    vector<int> odd;
    vector<int> even;
    for(int i = 0; i < n; i++){
        int x;
        cin >> x;
        if(i % 2 == 0){
            even.push_back(x);
        }else{
            odd.push_back(x);
        }
    }

    if((n - 1) % 2 == 0){
        for(int i = even.size() - 1; i >= 0; i--){
            cout << even[i] << " ";
        }
        for(int i = 0; i < odd.size(); i++){
            cout << odd[i] << " ";
        }
    }else{
        for(int i = odd.size() - 1; i >= 0; i--){
            cout << odd[i] << " ";
        }
        for(int i = 0; i < even.size(); i++){
            cout << even[i] << " ";
        }
    }

    cout << endl;

    return 0;
}

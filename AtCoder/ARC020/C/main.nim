import strutils
import sequtils
import algorithm
import math
import queues

const INF = int(1e18 + 373)
var MOD = int(1e9 + 7)

proc readInt1() : int = stdin.readLine().strip().parseInt()
proc readInt2() : (int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1])
proc readInt3() : (int, int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1], v[2])

#------------------------------------------------------------------------------#

type Op1[T] = proc(a: T, b: T): T
type Op2[T] = proc(a: T, b: T): T

proc repeatedSquares[T](x: T, n: Natural, e: T, f: Op2[T]): T =
    result = e
    var xx = f(x, e)
    var i = n
    while i > 0:
        if (i and 1) != 0:
            result = f(result, xx)
        xx = f(xx, xx)
        i = i shr 1

proc addM(a: int, b: int): int = (a + b) mod MOD
proc subM(a: int, b: int): int = (a - b + MOD) mod MOD
proc mulM(a: int, b: int): int = a * b mod MOD
proc powM(a: int, b: int): int = repeatedSquares[int](a, b, 1, `mulM`)

proc mul_mat(a, b: seq[seq[int]]): seq[seq[int]] =
    let n = a.len()

    result = newSeqWith(n, newSeq[int](n))
    for i in 0..<n:
        for j in 0..<n:
            var sum = 0
            for k in 0..<n:
                sum = sum.addM(a[i][k].mulM(b[k][j]))
            result[i][j] = sum


proc solve(n: int, aa: seq[int], ll: seq[int], b: int): int =
    var x = 0
    for i in 0..<n:
        let wid = floor(log10(aa[i].float())) + 1
        let bb = pow(10.0, wid.float()).int()
        let mat = @[@[bb, aa[i]], @[0, 1]]
        let powMat = repeatedSquares(mat, ll[i], @[@[1, 0], @[0, 1]], mul_mat)
        x = (powMat[0][0].mulM(x)).addM(powMat[0][1])
    return x

proc main() =
    let n = readInt1()

    var aa = newSeq[int](n)
    var ll = newSeq[int](n)

    for i in 0..<n:
        (aa[i], ll[i]) = readInt2()

    let b = readInt1()
    MOD = b

    echo solve(n, aa, ll, b)

main()

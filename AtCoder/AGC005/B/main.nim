import strutils
import sequtils
import algorithm
import math
import queues
import tables
import logging
import future

const INF* = int(1e18 + 373)

when not defined(release):
  addHandler(newFileLogger(stderr, lvlAll, "[ $levelname ] "))

proc readInt1*(): int = stdin.readLine().strip().parseInt()
proc readInt2*(): (int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1])
proc readInt3*(): (int, int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1], v[2])

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#
type Op2[T] = proc(a, b: T): T
type SegmentTree[T] = tuple[ n: int, a: seq[T], e: T, f: Op2[T] ]

proc initSegmentTree[T](n: int, e: T, f: Op2[T]): SegmentTree[T] =
    var m = 1
    while m < n:
        m *= 2

    var a = newSeq[T](2 * m)
    a.fill(e)
    return (m, a, e, f)

proc update[T](this: var SegmentTree[T], i: int, x: T) =
    this.a[this.n + i] = x
    var j = (this.n + i) div 2
    while j > 0:
        this.a[j] = this.f(this.a[j * 2], this.a[j * 2 + 1])
        j = j div 2

proc query[T](this: SegmentTree[T], s, t, i, ll, hh: int): T =
    if t <= ll or hh <= s:
        return this.e
    if s <= ll and hh <= t:
        return this.a[i]

    let m = (ll + hh) div 2
    let vl = this.query(s, t, i * 2, ll, m)
    let vh = this.query(s, t, i * 2 + 1, m, hh)
    return this.f(vl, vh)

proc query[T](this: SegmentTree[T], s, t: int): T = query(this, s, t, 1, 0, this.n)

#------------------------------------------------------------------------------#

proc main() =
  let n = readInt1()
  let a = readSeq().map(parseInt).map(it => it - 1)

  var pos = newSeq[int](n)
  for i in 0..<n:
    pos[a[i]] = i

  var segtree = initSegmentTree[int](n, INF, (x1: int, x2: int) => min(x1, x2))
  for i in 0..<n:
    segtree.update(i, a[i])

  var ans = 0

  var stack = newSeq[(int, int)](0)
  stack.add((0, n))
  while stack.len() > 0:
    let (s, t) = stack.pop()
    if t - s <= 0:
      continue

    let v = segtree.query(s, t)
    let p = pos[v]

    var sum = 0
    let siz = t - s
    let lsiz = p - s
    let rsiz = t - (p + 1)
    sum += siz * (siz - 1) div 2 + siz

    if lsiz > 0:
      sum -= lsiz * (lsiz - 1) div 2 + lsiz
    if rsiz > 0:
      sum -= rsiz * (rsiz - 1) div 2 + rsiz

    ans += sum * (v + 1)

    stack.add((s, p))
    stack.add((p + 1, t))

  echo ans

main()


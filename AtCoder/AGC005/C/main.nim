import strutils
import sequtils
import algorithm
import math
import queues
import tables
import logging
import future

const INF* = int(1e18 + 373)

when not defined(release):
  addHandler(newFileLogger(stderr, lvlAll, "[ $levelname ] "))

proc readInt1*(): int = stdin.readLine().strip().parseInt()
proc readInt2*(): (int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1])
proc readInt3*(): (int, int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1], v[2])

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#
proc solve(n: int; a: seq[int]): bool =
  let r = max(a)
  let c = (r + 1) div 2

  var aCount = newSeq[int](r + 1)
  for d in a:
    aCount[d] += 1

  var needs = newSeq[int](0)
  for i in countdown(r, c):
    needs.add(i)
  for i in countdown(r, c):
    needs.add(i)
  if r mod 2 == 0:
    discard needs.pop()

  for d in needs:
    if aCount[d] <= 0:
      return false
    aCount[d] -= 1

  for d in 0..r:
    if aCount[d] > 0 and d notin (c + 1)..r:
      return false

  return true

proc main() =
  let n = readInt1()
  let a = readSeq().map(parseInt).sorted(cmp[int])
  echo if solve(n, a): "Possible" else: "Impossible"

main()


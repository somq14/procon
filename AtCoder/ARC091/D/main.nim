import strutils
import sequtils
import algorithm
import math
import queues
import tables
import logging

const INF* = int(1e18 + 373)

when not defined(release):
  addHandler(newFileLogger(stderr, lvlAll, "[ $levelname ] "))

proc readInt1*(): int = stdin.readLine().strip().parseInt()
proc readInt2*(): (int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1])
proc readInt3*(): (int, int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1], v[2])

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#

proc solve(n, k, b: int): int =
  if b <= k:
    return 0

  result = 0

  let t = (n + 1) div b
  result += t * (b - k)

  let m = (n + 1) mod b
  result += max(0, m - k)

proc main() =
  let (n, k) = readInt2()

  var sum = 0
  for b in 1..n:
    sum += solve(n, k, b)
    if k == 0:
      sum -= 1

  echo sum

main()


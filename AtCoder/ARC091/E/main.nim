import strutils
import sequtils
import algorithm
import math
import queues
import tables
import logging
import future

const INF* = int(1e18 + 373)

when not defined(release):
  addHandler(newFileLogger(stderr, lvlAll, "[ $levelname ] "))

proc readInt1*(): int = stdin.readLine().strip().parseInt()
proc readInt2*(): (int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1])
proc readInt3*(): (int, int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1], v[2])

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#
proc place(a: var seq[int]; b, s, t: int) =
  let n = t - s
  for i in 0..<n:
    a[s + i] = b + n - 1 - i

proc solve(n, a, b: int): seq[int] =
  if a == 1:
    var ans: seq[int] = @[]
    for i in countdown(n - 1, 0):
      ans.add(i)
    return ans

  if b == 1:
    var ans: seq[int] = @[]
    for i in 0..<n:
      ans.add(i)
    return ans

  var sec = newSeq[int](a)
  sec[0] = b

  let k = (n - b) div (a - 1)
  for i in 1..<a:
    sec[i] = k

  var rest = n - b - k * (a - 1)
  for i in 1..<n:
    if rest == 0:
      break
    sec[i] += 1
    rest -= 1

  var ans = newSeq[int](n)
  var j = 0
  for i in 0..<a:
    ans.place(j, j, j + sec[i])
    j += sec[i]

  return ans

proc main() =
  let (n, a, b) = readInt3()

  if not (a + b - 1 <= n and n <= a * b):
    echo -1
    return

  let ans = solve(n, a, b)
  debug ans
  echo ans.map(it => it + 1).map(it => $it).join(" ")

main()


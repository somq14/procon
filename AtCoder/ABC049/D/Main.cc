#include <algorithm>
#include <cctype>
#include <complex>
#include <iomanip>
#include <iostream>
#include <queue>
#include <set>
#include <string>
#include <vector>

using std::cin;
using std::cout;
using std::endl;

using std::string;
using std::to_string;
using std::vector;
using std::set;
using std::queue;
using std::priority_queue;

using std::min;
using std::max;
using std::sort;
using std::abs;

typedef long long int ll;
const ll MOD = 1e9 + 7;

class unionfind {
   public:
    vector<int> a;
    vector<int> h;

    unionfind(int n) : a(n, -1), h(n) {}

    void unite(int v, int u) {
        const int rt_v = find(v);
        const int rt_u = find(u);
        if (rt_v == rt_u) {
            return;
        }

        if (h[rt_v] > h[rt_u]) {
            a[rt_v] = rt_u;
        } else if (h[rt_v] < h[rt_u]) {
            a[rt_u] = rt_v;
        } else {
            a[rt_u] = rt_v;
            h[rt_v]++;
        }
    }

    int find(int v) {
        if (a[v] == -1) {
            return v;
        }
        return a[v] = find(a[v]);
    }

    bool same(int v, int u) { return find(v) == find(u); }
};

class vertex {
   public:
    int id;
    int c0;
    int c1;

    vertex() {}
    vertex(int id, int c0, int c1) : id(id), c0(c0), c1(c1) {}

    bool operator<(const vertex& v) const {
        if (c0 == v.c0) {
            return c1 < v.c1;
        }
        return c0 < v.c0;
    }
};

int main() {
    int n, k, l;
    cin >> n >> k >> l;

    unionfind uf0(n);
    for (int i = 0; i < k; i++) {
        int p, q;
        cin >> p >> q;
        uf0.unite(p - 1, q - 1);
    }

    unionfind uf1(n);
    for (int i = 0; i < l; i++) {
        int r, s;
        cin >> r >> s;
        uf1.unite(r - 1, s - 1);
    }

    vector<vertex> vs(n);
    for (int i = 0; i < n; i++) {
        vs[i] = vertex(i, uf0.find(i), uf1.find(i));
    }

    sort(vs.begin(), vs.end());

    vector<int> ans(n);
    int cnt;
    for (int i = 0; i < n; i += cnt) {
        const int c0 = vs[i].c0;
        const int c1 = vs[i].c1;
        cnt = 0;
        for (int j = i; j < n && c0 == vs[j].c0 && c1 == vs[j].c1; j++) {
            cnt++;
        }

        for (int j = i; j < i + cnt; j++) {
            ans[vs[j].id] = cnt;
        }
    }

    for (int i = 0; i < n; i++) {
        cout << ans[i] << " ";
    }
    cout << endl;

    return 0;
}

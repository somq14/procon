#include <algorithm>
#include <cctype>
#include <complex>
#include <cstring>
#include <iomanip>
#include <iostream>
#include <queue>
#include <regex>
#include <set>
#include <string>
#include <vector>

using std::cin;
using std::cout;
using std::endl;

using std::string;
using std::to_string;
using std::vector;
using std::set;
using std::queue;
using std::priority_queue;

using std::min;
using std::max;
using std::sort;
using std::abs;

int main() {
    string s;
    cin >> s;

    bool possible = true;
    bool dream_flag = false;
    bool erase_flag = false;
    size_t i = 0;
    while (i < s.length()) {
        if (s.substr(i, 5) == "dream") {
            i += 5;
            dream_flag = true;
            erase_flag = false;
        } else if (s.substr(i, 5) == "erase") {
            i += 5;
            dream_flag = false;
            erase_flag = true;
        } else if (dream_flag && s.substr(i, 2) == "er") {
            i += 2;
            dream_flag = false;
            erase_flag = false;
        } else if (erase_flag && s.substr(i, 1) == "r") {
            i += 1;
            dream_flag = false;
            erase_flag = false;
        } else {
            possible = false;
            break;
        }
    }

    cout << (possible ? "YES" : "NO") << endl;

    return 0;
}

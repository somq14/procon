import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import logging
import future

const INF* = int(1e18 + 373)

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

proc readInt1*(): int = readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#
type Event = tuple [ p, t, i: int ]

proc main() =
  let (n, m) = readInt2()

  var es = newSeq[Event](0)
  for i in 0..<m:
    var (a, b) = readInt2()
    if a > b:
      swap(a, b)
    es.add((a - 1, 1, i));
    es.add((b - 1, -1, i));
  es.sort(cmp[Event])

  var opend = newSeq[int](0)
  var marked = newSeq[bool](m)
  var ans = 0
  for e in es:
    if marked[e.i]:
      continue
    if e.t == -1:
      ans += 1
      for j in opend:
        marked[j] = true
      opend = @[]
    else:
      opend.add(e.i)

  echo ans

main()


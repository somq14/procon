import strutils
import sequtils
import algorithm
import math
import queues
import tables
import logging
import future
import times

const INF* = int(1e18 + 373)

when not defined(release):
  addHandler(newFileLogger(stderr, lvlAll, "[ $levelname ] "))

proc readInt1*(): int = stdin.readLine().strip().parseInt()
proc readInt2*(): (int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1])
proc readInt3*(): (int, int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1], v[2])

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#


proc main() =
  let (n, k) = readInt2()
  let w = readSeq(n).map(it => it.split().map(parseInt))

  let n2 = 1 shl n

  var wsum = newSeq[int](n2)
  for s in 0..<n2:
    var sum = 0
    for i in 0..<n:
      if (s and (1 shl i)) == 0:
        continue
      for j in (i + 1)..<n:
        if (s and (1 shl j)) == 0:
          continue
        sum += w[i][j]
    wsum[s] = sum

  var dp = newSeq[int](n2)
  dp[0] = 0

  for s in 1..<n2:
    var opt = k
    var t = (s - 1) and s
    while t != 0:
      let u = s and not t
      assert((u and t) == 0 and (u or t) == s)
      opt = max(opt, k - (wsum[s] - wsum[t] - wsum[u]) + dp[u])
      t = (t - 1) and s
    dp[s] = opt

  echo dp[(1 shl n) - 1]

main()


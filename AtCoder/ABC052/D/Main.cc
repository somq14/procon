#include <algorithm>
#include <cctype>
#include <complex>
#include <iomanip>
#include <iostream>
#include <queue>
#include <set>
#include <string>
#include <vector>

using std::cin;
using std::cout;
using std::endl;

using std::string;
using std::to_string;
using std::vector;
using std::set;
using std::queue;
using std::priority_queue;

using std::min;
using std::max;
using std::sort;
using std::abs;

typedef long long int ll;

int main() {
    ll n, a, b;
    cin >> n >> a >> b;

    vector<ll> x(n);
    for(ll i = 0; i < n; i++){
        cin >> x[i];
    }

    ll ans = 0;
    for(ll i = 0; i < n - 1; i++){
        const ll d = x[i + 1] - x[i];
        ans += min(a * d, b);
    }
    cout << ans << endl;

    return 0;
}

#include <algorithm>
#include <cctype>
#include <complex>
#include <iomanip>
#include <iostream>
#include <queue>
#include <set>
#include <string>
#include <vector>

using std::cin;
using std::cout;
using std::endl;

using std::string;
using std::to_string;
using std::vector;
using std::set;
using std::queue;
using std::priority_queue;

using std::min;
using std::max;
using std::sort;
using std::abs;

typedef long long int ll;

const ll MOD = 1000000007;

int main() {
    ll n;
    cin >> n;

    vector<int> a(n + 1);
    for (int i = 1; i <= n; i++) {
        int m = i;
        for(int j = 2; j <= m; j++){
            while(m % j == 0){
                a[j]++;
                m /= j;
            }
        }
    }

    ll ans = 1;
    for(int i = 2; i <= n; i++){
        ans *= a[i] + 1;
        ans %= MOD;
    }

    cout << ans << endl;

    return 0;
}

#include <algorithm>
#include <cctype>
#include <complex>
#include <iomanip>
#include <iostream>
#include <queue>
#include <set>
#include <string>
#include <vector>

using std::cin;
using std::cout;
using std::endl;

using std::string;
using std::to_string;
using std::vector;
using std::set;
using std::queue;
using std::priority_queue;

using std::min;
using std::max;
using std::sort;
using std::abs;

int main() {
    int a, b, c, d;
    cin >> a >> b >> c >> d;
    int v0 = a * b;
    int v1 = c * d;
    cout << max(v0, v1) << endl;
    return 0;
}

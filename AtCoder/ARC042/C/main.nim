import strutils
import sequtils
import algorithm
import math
import queues
import tables
import logging

const INF* = int(1e18 + 373)

when not defined(release):
  addHandler(newFileLogger(stderr, lvlAll, "[ $levelname ] "))

proc readInt1*(): int = stdin.readLine().strip().parseInt()
proc readInt2*(): (int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1])
proc readInt3*(): (int, int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1], v[2])

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#
type Item = tuple [w, v: int]

proc `<`(a, b: Item): bool =
  if a.w == b.w:
    return a.v < b.v
  return a.w < b.w

proc main() =
  let (n, p) = readInt2()

  var a = newSeq[Item](n)
  for i in 0..<n:
    let (w, v) = readInt2()
    a[i] = (w, v)
  a.sort(cmp[Item], SortOrder.Descending)

  var dpMemo = newSeq[int](n + 1)

  var dp = newSeq[int](p + 1)
  var dpNext = dp

  for i in 1..n:
    for j in 0..p:
      var opt = dp[j]
      if j >= a[i - 1].w:
        opt = max(opt, dp[j - a[i - 1].w] + a[i - 1].v)
      dpNext[j] = opt
    dpMemo[i] = dpNext[p]
    dp = dpNext

  # cum[i] is max of cum[i], cum[i + 1], ..., cum[n - 1]
  var cum = newSeq[int](n + 1)
  cum[n] = 0
  for i in countdown(n - 1, 0):
    cum[i] = max(cum[i + 1], a[i].v)

  var ans = 0
  for i in 0..n:
    ans = max(ans, dpMemo[i] + cum[i])

  echo ans

main()


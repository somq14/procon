import strutils
import sequtils
import algorithm
import math
import queues
import tables
import logging
import future

const INF* = int(1e18 + 373)

when not defined(release):
  addHandler(newFileLogger(stderr, lvlAll, "[ $levelname ] "))

proc readInt1*(): int = stdin.readLine().strip().parseInt()
proc readInt2*(): (int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1])
proc readInt3*(): (int, int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1], v[2])

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#
proc decimalSqrt(n: int): int =
  let sqrtN = sqrt(n.float()).int()
  if (sqrtN + 1) * (sqrtN + 1) <= n:
    return sqrtN + 1
  if sqrtN * sqrtN <= n:
    return sqrtN
  if (sqrtN - 1) * (sqrtN - 1) <= n:
    return sqrtN - 1

proc solve(a, b: int): int =
  var a = a
  var b = b
  if a > b:
    swap(a, b)

  let k = decimalSqrt(a * b - 1)
  var ans = 2 * k
  if k * (k + 1) >= a * b:
    ans -= 1
  if a <= k:
    ans -= 1

  return ans

proc main() =
  let q = readInt1()
  var a = newSeq[int](q)
  var b = newSeq[int](q)
  for i in 0..<q:
    (a[i], b[i]) = readInt2()

  for i in 0..<q:
    echo solve(a[i], b[i])

main()


import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import logging
import future

const INF* = int(1e18 + 373)

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

proc readInt1*(): int = readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

type seq3*[T] = seq[seq[seq[T]]]
proc newSeq3*[T](n1, n2, n3: Natural): seq3[T] = newSeqWith(n1, newSeqWith(n2, newSeq[T](n3)))

type seq4*[T] = seq[seq[seq[seq[T]]]]
proc newSeq4*[T](n1, n2, n3, n4: Natural): seq4[T] =
  newSeqWith(n1, newSeqWith(n2, newSeqWith(n3, newSeq[T](n4))))

#------------------------------------------------------------------------------#

const MOD = 998244353

proc rest(c1, c2: char): char =
  if c1 == 'a' and c2 == 'b' or c1 == 'b' and c2 == 'a':
    return 'c'

  if c1 == 'a' and c2 == 'c' or c1 == 'c' and c2 == 'a':
    return 'b'

  if c1 == 'b' and c2 == 'c' or c1 == 'c' and c2 == 'b':
    return 'a'

  assert false

proc brute(s: string): HashSet[string] =
  var vSet = initSet[string]()
  var vQue = initQueue[string]()

  vSet.incl(s)
  vQue.enqueue(s)

  while vQue.len() > 0:
    let v = vQue.dequeue()

    var u = v & ""
    for i in 0..<v.len() - 1:
      if v[i] == v[i + 1]:
        continue
      u[i] = rest(v[i], v[i + 1])
      u[i + 1] = rest(v[i], v[i + 1])

      if u notin vSet:
        let uu = u & ""
        vSet.incl(uu)
        vQue.enqueue(uu)

      u[i] = v[i]
      u[i + 1] = v[i + 1]

  return vSet

proc mod3(s: string): int =
  result = 0
  for i in 0..<s.len():
    result = (result + s[i].ord() - 'a'.ord()) mod 3


proc indeg(s: string): int =
  result = 0
  for i in 0..<s.len() - 1:
    if s[i] == s[i + 1]:
      result += 1

proc outdeg(s: string): int =
  result = 0
  for i in 0..<s.len() - 1:
    if s[i] != s[i + 1]:
      result += 1

proc fast(s: string, t: string): bool =
  if s.len() != t.len():
    return false
  if s.mod3() != t.mod3():
    return false
  if s == t:
    return true
  if s.outdeg() == 0:
    return false
  if t.indeg() == 0:
    return false
  if s.len() <= 8:
    return t in s.brute()
  return true

proc build(n: int; m: int): string =
  result = ""
  var m = m
  for i in 0..<n:
    case (m div (3^i)) mod 3:
      of 0:
        result = result & "a"
      of 1:
        result = result & "b"
      of 2:
        result = result & "c"
      else:
        assert false

proc main() =
  let s = stdin.readLine()
  let n = s.len()

  if s.len() <= 8:
    echo s.brute().len()
    return

  if s.outdeg() == 0:
    echo 1
    return

  var dp = newSeq4[uint32](n + 1, 3, 3, 2)
  for c in 0..<3:
    dp[1][c][c][0] = 1

  for i in 2..n:
    for j in 0..<3:
      for c in 0..<3:
        block:
          var comb = 0.uint32()
          if c != 0:
            comb = (comb + dp[i - 1][(j - c + 3) mod 3][0][0]) mod MOD
          if c != 1:
            comb = (comb + dp[i - 1][(j - c + 3) mod 3][1][0]) mod MOD
          if c != 2:
            comb = (comb + dp[i - 1][(j - c + 3) mod 3][2][0]) mod MOD
          dp[i][j][c][0] = comb

        block:
          var comb = 0.uint32()
          if c == 0:
            comb = (comb + dp[i - 1][(j - c + 3) mod 3][0][0]) mod MOD
          if c == 1:
            comb = (comb + dp[i - 1][(j - c + 3) mod 3][1][0]) mod MOD
          if c == 2:
            comb = (comb + dp[i - 1][(j - c + 3) mod 3][2][0]) mod MOD
          comb = (comb + dp[i - 1][(j - c + 3) mod 3][0][1]) mod MOD
          comb = (comb + dp[i - 1][(j - c + 3) mod 3][1][1]) mod MOD
          comb = (comb + dp[i - 1][(j - c + 3) mod 3][2][1]) mod MOD
          dp[i][j][c][1] = comb

  let sMod3 = s.mod3()
  var ans = 0.uint32()
  ans = (ans + dp[n][sMod3][0][1]) mod MOD
  ans = (ans + dp[n][sMod3][1][1]) mod MOD
  ans = (ans + dp[n][sMod3][2][1]) mod MOD
  if s.indeg() == 0:
    ans = (ans + 1) mod MOD
  echo ans

main()


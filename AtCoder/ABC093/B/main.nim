import strutils
import sequtils
import algorithm
import math
import queues
import tables
import logging
import future
import sets

const INF* = int(1e18 + 373)

when not defined(release):
  addHandler(newFileLogger(stderr, lvlAll, "[ $levelname ] "))

proc readInt1*(): int = stdin.readLine().strip().parseInt()
proc readInt2*(): (int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1])
proc readInt3*(): (int, int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1], v[2])

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#
proc uniqued[T](a: seq[T]): seq[T] =
  var s = initSet[T]()
  for e in a:
    s.incl(e)

  result = @[]
  for e in s:
    result.add(e)
  result.sort(cmp[T])

proc main() =
  let (a, b, k) = readInt3()

  var ans = newSeq[int](0)
  for i in a..<a+k:
    if i in a..b:
      ans.add(i)
  for i in b-k+1..b:
    if i in a..b:
      ans.add(i)

  for e in ans.uniqued():
    echo e

main()


import strutils
import sequtils
import algorithm
import math
import queues
import tables
import logging
import future

const INF* = int(1e18 + 373)

when not defined(release):
  addHandler(newFileLogger(stderr, lvlAll, "[ $levelname ] "))

proc readInt1*(): int = stdin.readLine().strip().parseInt()
proc readInt2*(): (int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1])
proc readInt3*(): (int, int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1], v[2])

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#
type Pair = tuple[ a, b: int]

proc subr(n: int; ps: seq[Pair]; x: int): int =
  if ps.map(it => min(it.a, it.b)).sum() > x:
    return -1

  var dp = newSeq2[int](n + 1, x + 1)
  dp[0].fill(0)
  for i in 1..n:
    for j in 0..x:
      var opt = 0
      if j >= ps[i - 1].a:
        opt = max(opt, dp[i - 1][j - ps[i - 1].a] + ps[i - 1].a)
      if j >= ps[i - 1].b:
        opt = max(opt, dp[i - 1][j - ps[i - 1].b] + ps[i - 1].b)
      dp[i][j] = opt

  return dp[n][x]


proc solve(n: int; g: seq2[int]; x: seq[int]; v: int = 0): Pair =
  if g[v].len() == 0:
    return (0, x[v])

  var ps = newSeq[Pair](0)
  for u in g[v]:
    let ans = solve(n, g, x, u)
    if ans == (-1, -1):
      return ans
    ps.add(ans)

  let opt = subr(ps.len(), ps, x[v])
  if opt == -1:
    return (-1, -1)

  let sumAll = ps.map(it => it.a + it.b).sum()
  return (x[v], sumAll - opt)


proc main() =
  let n = readInt1()
  let p = @[0].concat(readSeq().map(parseInt).map(it => it - 1))
  let x = readSeq().map(parseInt)

  var g = newSeq2[int](n, 0)
  for i in 1..<n:
    g[p[i]].add(i)

  echo if solve(n, g, x) != (-1, -1): "POSSIBLE" else: "IMPOSSIBLE"

main()


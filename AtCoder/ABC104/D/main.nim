import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import logging
import future

const INF* = int(1e18 + 373)

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

proc readInt1*(): int = readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#

const MOD = int(1e9 + 7)

proc main() =
  let s = stdin.readLine()
  let n = s.len()

  var dp = newSeq2[int](n + 1, 3 + 1)
  dp[n].fill(0)
  dp[n][3] = 1

  for i in countdown(n - 1, 0):
    for j in 0..3:
      dp[i][j] = 0
      if s[i] == '?':
        dp[i][j] += 3 * dp[i + 1][j]
      else:
        dp[i][j] += 1 * dp[i + 1][j]

      if (j == 0 and s[i] == 'A') or
         (j == 1 and s[i] == 'B') or
         (j == 2 and s[i] == 'C') or
         (j != 3 and s[i] == '?'):
        dp[i][j] += dp[i + 1][j + 1]

      dp[i][j] = dp[i][j] mod MOD

  echo dp[0][0]


main()


import strutils
import sequtils
import algorithm
import math
import queues
import tables
import logging

const INF* = int(1e18 + 373)

when not defined(release):
  addHandler(newFileLogger(stderr, lvlAll, "[ $levelname ] "))

proc readInt1*(): int = stdin.readLine().strip().parseInt()
proc readInt2*(): (int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1])
proc readInt3*(): (int, int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1], v[2])

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#
type Beam = tuple[t, x, w: int]

proc bundle(a: seq[Beam]): seq[Beam] =
  let a = a.sorted(cmp[Beam])

  result = newSeq[Beam](0)

  var i = 0
  while i < a.len():
    let t = a[i].t
    let x = a[i].x
    var w = 1
    i += 1

    while i < a.len() and t == a[i].t and a[i - 1].x + 1 == a[i].x:
      w += 1
      i += 1

    result.add((t, x, w))

proc solve(n: int; a: seq[Beam]): int =
  let a = a.sorted(cmp[Beam])

  var dp = newSeq[int](n)
  dp.fill(0)

  for b in a:
    let dpLeft = if b.x <= 0: INF else: dp[b.x - 1]
    let dpRight = if b.x + b.w >= n: INF else: dp[b.x + b.w]

    for i in (b.x)..<(b.x + b.w):
      var opt = INF
      opt = min(opt, dpLeft + i - (b.x - 1))
      opt = min(opt, dpRight + (b.x + b.w) - i)
      dp[i] = opt

  return min(dp)


proc main() =
  let (w, h, q) = readInt3()

  var xbeams = newSeq[Beam](0)
  var ybeams = newSeq[Beam](0)
  for i in 0..<q:
    let (t, d, x) = readInt3()
    let b = (t, x - 1, 1)
    if d == 0:
      ybeams.add(b)
    else:
      xbeams.add(b)

  let ans = solve(h, bundle(xbeams)) + solve(w, bundle(ybeams))
  echo if ans >= INF: -1 else: ans

main()


import strutils
import sequtils
import algorithm
import math
import queues
import tables
import logging
import future

const INF* = int(1e18 + 373)

when not defined(release):
  addHandler(newFileLogger(stderr, lvlAll, "[ $levelname ] "))

proc readInt1*(): int = stdin.readLine().strip().parseInt()
proc readInt2*(): (int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1])
proc readInt3*(): (int, int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1], v[2])

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#
proc main() =
  let (n, x) = readInt2()

  var ans = x

  var rest = n
  var side = n - x

  while rest > 0 and side > 0:
    debug("ans  ", ans)
    debug("side ", side)
    debug("rest ", rest)

    ans += 2 * (rest div side) * side
    if rest mod side == 0:
      ans -= side

    let nextSide = rest mod side
    let nextRest = side

    side = nextSide
    rest = nextRest

  debug("ans  ", ans)
  debug("side ", side)
  debug("rest ", rest)
  echo(ans - (n - x))

main()


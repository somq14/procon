import strutils
import sequtils
import algorithm
import math
import queues
import tables
import logging
import future

const INF* = int(1e18 + 373)

when not defined(release):
  addHandler(newFileLogger(stderr, lvlAll, "[ $levelname ] "))

proc readInt1*(): int = stdin.readLine().strip().parseInt()
proc readInt2*(): (int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1])
proc readInt3*(): (int, int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1], v[2])

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#
proc removeVertex(g: var seq2[int]; v: int) =
  let u = g[v][0]
  g[v] = @[]
  let ind = g[u].find(v)
  g[u].delete(ind, ind)

proc findVertex(g: seq2[int]): int =
  for v in 0..<g.len():
    if g[v].len() > 0:
      return v
  return -1

proc searchDistance(g: seq2[int]; v, d: int; ans: var seq[int]) =
  ans[v] = d
  for u in g[v]:
    if ans[u] == -1:
      searchDistance(g, u, d + 1, ans)

proc searchDistance(g: seq2[int]; s: int): seq[int] =
  var ans = newSeq[int](g.len())
  ans.fill(-1)
  searchDistance(g, s, 0, ans)
  return ans

proc main() =
  let (n, k) = readInt2()
  var g = newSeq2[int](n, 0)
  for i in 0..<n - 1:
    let (a, b) = readInt2()
    g[a - 1].add(b - 1)
    g[b - 1].add(a - 1)

  debug g

  var ans = 0
  while true:
    # first sweep
    let d0 = searchDistance(g, findVertex(g))
    var s = d0.find(max(d0))

    # second sweep
    let d1 = searchDistance(g, s)
    var t = d1.find(max(d1))

    if max(d1) <= k:
      break

    let ds = d1
    let dt = searchDistance(g, t)

    let f = (it: int) => it > k
    let target = if ds.filter(f).len() >= dt.filter(f).len(): s else: t
    g.removeVertex(target)
    ans += 1

  echo ans

main()


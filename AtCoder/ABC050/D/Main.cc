#include <algorithm>
#include <cctype>
#include <complex>
#include <iomanip>
#include <iostream>
#include <queue>
#include <set>
#include <string>
#include <vector>

using std::cin;
using std::cout;
using std::endl;

using std::string;
using std::to_string;
using std::vector;
using std::set;
using std::queue;
using std::priority_queue;

using std::min;
using std::max;
using std::sort;
using std::abs;

typedef long long int ll;
const ll MOD = 1e9 + 7;

int main() {
    ll n;
    cin >> n;

    vector<vector<ll>> dp(64, vector<ll>(3));
    dp[63][0] = 1;
    dp[63][1] = 0;
    dp[63][2] = 0;
    for (ll i = 62; i >= 0; i--) {
        if(n & (1L << i)){
            dp[i][0] = 1 * dp[i+1][0] + 0 * dp[i+1][1] + 0 * dp[i+1][2];
            dp[i][1] = 1 * dp[i+1][0] + 1 * dp[i+1][1] + 0 * dp[i+1][2];
            dp[i][2] = 0 * dp[i+1][0] + 2 * dp[i+1][1] + 3 * dp[i+1][2];
        }else{
            dp[i][0] = 1 * dp[i+1][0] + 1 * dp[i+1][1] + 0 * dp[i+1][2];
            dp[i][1] = 0 * dp[i+1][0] + 1 * dp[i+1][1] + 0 * dp[i+1][2];
            dp[i][2] = 0 * dp[i+1][0] + 1 * dp[i+1][1] + 3 * dp[i+1][2];
        }
        dp[i][0] %= MOD;
        dp[i][1] %= MOD;
        dp[i][2] %= MOD;
    }

    ll ans = (dp[0][0] + dp[0][1] + dp[0][2]) % MOD;
    cout << ans << endl;

    return 0;
}

#include <algorithm>
#include <cctype>
#include <complex>
#include <iomanip>
#include <iostream>
#include <queue>
#include <set>
#include <string>
#include <vector>

using std::cin;
using std::cout;
using std::endl;

using std::string;
using std::to_string;
using std::vector;
using std::set;
using std::queue;
using std::priority_queue;

using std::min;
using std::max;
using std::sort;
using std::abs;

const int MOD = 1e9 + 7;

int main() {
    int n;
    cin >> n;

    vector<int> a(n);
    for (int i = 0; i < n; i++) {
        cin >> a[i];
    }

    sort(a.begin(), a.end());

    bool possible = true;
    if (n % 2 == 0) {
        for (int i = 0; i < n; i++) {
            if (a[i] != 2 * (i / 2) + 1) {
                possible = false;
                break;
            }
        }
    } else {
        for (int i = 0; i < n; i++) {
            if (a[i] != 2 * ((i + 1) / 2)) {
                possible = false;
                break;
            }
        }
    }

    if (!possible) {
        cout << 0 << endl;
        return 0;
    }

    int ans = 1;
    for (int i = 0; i < n / 2; i++) {
        ans *= 2;
        ans %= MOD;
    }

    cout << ans << endl;

    return 0;
}

import strutils
import sequtils
import algorithm
import math
import queues
import tables

const INF* = int(1e18 + 373)

proc readInt1*(): int = stdin.readLine().strip().parseInt()
proc readInt2*(): (int, int) =
  let v = stdin.readLine().strip().split().map(parseInt)
  return (v[0], v[1])
proc readInt3*(): (int, int, int) =
  let v = stdin.readLine().strip().split().map(parseInt)
  return (v[0], v[1], v[2])
proc readInt*(): seq[int] = stdin.readLine().strip().split().map(parseInt)

#------------------------------------------------------------------------------#

const MOD = int(1e9 + 7)

type Op1[T] = proc(a: T, b: T): T
type Op2[T] = proc(a: T, b: T): T

proc repeatedSquares[T](x: T, n: Natural, e: T, f: Op2[T]): T =
    result = e
    var xx = f(x, e)
    var i = n
    while i > 0:
        if (i and 1) != 0:
            result = f(result, xx)
        xx = f(xx, xx)
        i = i shr 1

proc addM(a, b: Natural): Natural = (a + b) mod MOD
proc subM(a, b: Natural): Natural = (a - b + MOD) mod MOD
proc mulM(a, b: Natural): Natural = a * b mod MOD
proc powM(a, b: Natural): Natural = repeatedSquares[Natural](a, b, 1, `mulM`)
proc invM(a: Natural): Natural = a.powM(MOD - 2)
proc divM(a, b: Natural): Natural = a.mulM(invM(b))


var memoFactM = @[Natural(1)]
proc factM(n: Natural): Natural =
    if n < memoFactM.len():
        return memoFactM[n]
    for i in memoFactM.len()..n:
        memoFactM.add(memoFactM[i - 1].mulM(i))
    return memoFactM[n]


proc combM(n, r: Natural): Natural =
    if r > n:
        return 0
    if r > n div 2:
        return combM(n, n - r)

    result = 1
    for i in ((n - r) + 1)..n:
        result = result.mulM(i)
    result = result.divM(factM(r))


proc multCombM(n, r: Natural): Natural = combM(n + r - 1, n - 1)

#------------------------------------------------------------------------------#
const MaxPrime = int(1e5 + 1)

proc calcPrimeTable*(maxn: int): seq[bool] =
  result = newSeq[bool](maxn + 1)
  result.fill(true)
  result[0] = false
  result[1] = false

  for i in 2..maxn:
    if not result[i]:
      continue
    var j = i * 2
    while j <= maxn:
      result[j] = false
      j += i

let primeTable* = calcPrimeTable(MaxPrime)

proc calcPrimeList*(maxn: int): seq[int] =
  result = newSeq[int](0)
  for i in 0..maxn:
    if primeTable[i]:
      result.add(i)

let primeList* = calcPrimeList(MaxPrime)

proc factorize*(n: int): seq[int] =
  result = newSeq[int](0)
  var m = n
  for p in primeList:
    if m < p:
      break
    while m mod p == 0:
      result.add(p)
      m = m div p
  if m != 1:
    result.add(m)

#------------------------------------------------------------------------------#

proc solve(a, b: int): int =
  if a < b:
    return 0
  if a == b:
    return 1

  var t = initTable[int, int]()
  for i in (b + 1)..a:
    let facts = factorize(i)
    for f in facts:
      if f notin t:
        t[f] = 0
      t[f] += 1

  result = 1
  for i in t.values():
    result = result.mulM(i + 1)

proc main() =
  let (a, b) = readInt2()
  echo solve(a, b)

main()


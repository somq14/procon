import strutils
import sequtils
import math

proc readInt1() : int = stdin.readLine().parseInt()
proc readInt2() : (int, int) =
    let v = stdin.readLine().split().map(parseInt)
    return (v[0], v[1])
proc readInt3() : (int, int, int) =
    let v = stdin.readLine().split().map(parseInt)
    return (v[0], v[1], v[2])

#------------------------------------------------------------------------------#

const INF : float = 1e100

type Gacha = tuple[k : int, c : int, a : seq[int], p : seq[float]]

proc contains(s : int, i : int) : bool =
    return (s and (1 shl i)) != 0

proc containsTarget(g : Gacha, s : int) : bool =
    for i in g.a:
        if not s.contains(i):
            return true
    return false

proc calcWinProb(g : Gacha, s : int) : float =
    var p : float = 0
    for i in 0..<g.k:
        if not s.contains(g.a[i]):
            p += g.p[i]
    return p

proc calcLoseExp(g : Gacha, s : int) : float =
    let p : float = calcWinProb(g, s)

    var e : float = 0
    var pow_i : float = 1
    for i in 0..<10000:
        e += float(i) * p * pow_i
        pow_i *= 1 - p
    return e

proc solve(n : int, m : int, gs : seq[Gacha]) : float =
    let n2 = 1 shl n

    var dp = newSeq[float](n2)
    dp[n2 - 1] = 0

    for s in countdown(n2 - 2, 0):
        var opt = INF
        for g in gs:
            if not containsTarget(g, s):
                continue

            let losePayment = float(g.c) * calcLoseExp(g, s)
            let winProb = calcWinProb(g, s)

            var payment : float = 0
            for i in 0..<g.k:
                if not s.contains(g.a[i]):
                    payment += (g.p[i] / winProb) * (float(g.c) + dp[s or (1 shl g.a[i])])

            opt = min(opt, payment + losePayment)
        dp[s] = opt

    return dp[0]


proc main() : void =
    let (n, m) = readInt2()

    var gs = newSeq[Gacha](m)
    for i in 0..<m:
        let (k, c) = readInt2()
        var g : Gacha = (k, c, newSeq[int](k), newSeq[float](k))
        for j in 0..<k:
            let (a, p) = readInt2()
            g.a[j] = a - 1
            g.p[j] = float(p) / 100
        gs[i] = g

    echo formatFloat(solve(n, m, gs), ffDecimal)

main()


import strutils
import sequtils
import algorithm
import math

const INF = int(1e18 + 373)

proc readInt1() : int = stdin.readLine().strip().parseInt()
proc readInt2() : (int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1])
proc readInt3() : (int, int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1], v[2])

#------------------------------------------------------------------------------#

proc eval(n : int, h : int, g : seq[seq[bool]], d : seq[int], t : float) : bool =
    var dp = newSeq[seq[float]](n)
    for i in 0..<n:
        dp[i] = newSeq[float](h + 1)
        dp[i].fill(INF)

    dp[n - 1].fill(0)
    dp[n - 1][0] = INF
    for v in countdown(n - 2, 0):
        var maxDmg : int = 0
        var outDeg : float = 0
        for u in (v + 1)..<n:
            if g[v][u]:
                outDeg += 1
                maxDmg = max(maxDmg, d[u])

        for i in 1..h:
            let ret : float = t + float(h - i)
            if outDeg == 0 or maxDmg >= i:
                dp[v][i] = ret
                continue

            var exp : float = 0
            for u in (v + 1)..<n:
                if g[v][u]:
                    exp += (1 + dp[u][i - d[u]]) / float(outDeg)
            dp[v][i] = min(exp, ret)

    return dp[0][h] < t

proc solve(n : int, h : int, g : seq[seq[bool]], d : seq[int]) : float =
    # (lb, ub]
    var lb : float = -1
    var ub : float = 1e7 + 1
    for i in 0..<100:
        let mid = (ub + lb) / 2
        if eval(n, h, g, d, mid):
            ub = mid
        else:
            lb = mid
    return ub

proc main() : void =
    let (n, m, h) = readInt3()

    var g = newSeq[seq[bool]](n)
    for i in 0..<n:
        g[i] = newSeq[bool](n)

    for i in 0..<m:
        let (f, t) = readInt2()
        g[f - 1][t - 1] = true

    var d = newSeq[int](n)
    for i in 0..<n:
        d[i] = readInt1()

    let ans : float = solve(n, h, g, d)
    if ans > 1e7:
        echo -1
    else:
        echo formatFloat(ans, ffDecimal)

main()

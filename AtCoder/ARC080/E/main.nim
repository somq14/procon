import strutils
import sequtils
import algorithm
import math
import queues
import tables
import logging
import future

const INF* = int(1e18 + 373)

when not defined(release):
  addHandler(newFileLogger(stderr, lvlAll, "[ $levelname ] "))

proc readInt1*(): int = stdin.readLine().strip().parseInt()
proc readInt2*(): (int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1])
proc readInt3*(): (int, int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1], v[2])

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#
#------------------------------------------------------------------------------#
type Op2[T] = proc(a, b: T): T
type SegmentTree[T] = tuple[ n: int, a: seq[T], e: T, f: Op2[T] ]

proc initSegmentTree[T](n: int, e: T, f: Op2[T]): SegmentTree[T] =
    var m = 1
    while m < n:
        m *= 2

    var a = newSeq[T](2 * m)
    a.fill(e)
    return (m, a, e, f)

proc update[T](this: var SegmentTree[T], i: int, x: T) =
    this.a[this.n + i] = x
    var j = (this.n + i) div 2
    while j > 0:
        this.a[j] = this.f(this.a[j * 2], this.a[j * 2 + 1])
        j = j div 2

proc query[T](this: SegmentTree[T], s, t, i, ll, hh: int): T =
    if t <= ll or hh <= s:
        return this.e
    if s <= ll and hh <= t:
        return this.a[i]

    let m = (ll + hh) div 2
    let vl = this.query(s, t, i * 2, ll, m)
    let vh = this.query(s, t, i * 2 + 1, m, hh)
    return this.f(vl, vh)

proc query[T](this: SegmentTree[T], s, t: int): T = query(this, s, t, 1, 0, this.n)

#------------------------------------------------------------------------------#
# FIXME: Use object
type PriorityQueue[T] = tuple[a: seq[T], n: Natural, cmp: proc(a, b: T): bool]

proc initPriorityQueue[T](cmp: proc(a, b: T): bool): PriorityQueue[T] =
    (newSeq[T](1), Natural(0), cmp)

proc len[T](this: PriorityQueue[T]): Natural = this.n

proc enqueue[T](this: var PriorityQueue[T], e: T) =
    this.n += 1
    this.a.add(e)
    var i = this.a.len() - 1
    while i > 1 and this.cmp(this.a[i], this.a[i div 2]):
        swap(this.a[i], this.a[i div 2])
        i = i div 2

proc dequeue[T](this: var PriorityQueue[T]): T =
    this.n -= 1
    result = this.a[1]
    this.a[1] = this.a.pop()
    var i = 1
    while i < this.a.len():
        var j = i
        if i * 2 < this.a.len() and this.cmp(this.a[i * 2], this.a[j]):
            j = i * 2
        if i * 2 + 1 < this.a.len() and this.cmp(this.a[i * 2 + 1], this.a[j]):
            j = i * 2 + 1
        if i == j:
            break
        swap(this.a[i], this.a[j])
        i = j

# FIXME: throw Exception when this is empty
proc front[T](this: PriorityQueue[T]): T = this.a[1]
#------------------------------------------------------------------------------#

type P = tuple [ x, i : int ]
type R = tuple [ pri, s, t : int ]

proc minP(p1, p2: P): P =
  if p1.x < p2.x:
    return p1
  else:
    return p2

proc main() =
  let n = readInt1()
  let a = readSeq().map(parseInt)

  var segtreeOdd = initSegmentTree[P](n, (INF, -1), minP)
  var segtreeEvn = initSegmentTree[P](n, (INF, -1), minP)
  for i in 0..<n:
    if i mod 2 == 0:
      segtreeEvn.update(i, (a[i], i))
    else:
      segtreeOdd.update(i, (a[i], i))

  var q = initPriorityQueue[R]((r1: R, r2: R) => r1.pri < r2.pri)
  q.enqueue((1, 0, n))

  var ans = newSeq[int](0)

  while q.len() > 0:
    let r = q.dequeue()
    if r.s mod 2 == 0:
      let (x1, i1) = segtreeEvn.query(r.s, r.t)
      let (x2, i2) = segtreeOdd.query(i1, r.t)
      ans.add(x1)
      ans.add(x2)
      if i1 - r.s > 0:
        q.enqueue((segtreeEvn.query(r.s, i1).x, r.s, i1))
      if i2 - (i1 + 1) > 0:
        q.enqueue((segtreeOdd.query(i1 + 1, i2).x, i1 + 1, i2))
      if r.t - (i2 + 1) > 0:
        q.enqueue((segtreeEvn.query(i2 + 1, r.t).x, i2 + 1, r.t))
    else:
      let (x1, i1) = segtreeOdd.query(r.s, r.t)
      let (x2, i2) = segtreeEvn.query(i1, r.t)
      ans.add(x1)
      ans.add(x2)
      if i1 - r.s > 0:
        q.enqueue((segtreeOdd.query(r.s, i1).x, r.s, i1))
      if i2 - (i1 + 1) > 0:
        q.enqueue((segtreeEvn.query(i1 + 1, i2).x, i1 + 1, i2))
      if r.t - (i2 + 1) > 0:
        q.enqueue((segtreeOdd.query(i2 + 1, r.t).x, i2 + 1, r.t))

  echo ans.map(it => $it).join(" ")

main()


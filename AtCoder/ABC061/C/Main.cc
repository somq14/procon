#include <algorithm>
#include <complex>
#include <iomanip>
#include <iostream>
#include <queue>
#include <set>
#include <string>
#include <vector>

using std::cin;
using std::cout;
using std::endl;

using std::string;
using std::to_string;
using std::vector;
using std::set;
using std::queue;
using std::priority_queue;

using std::min;
using std::max;
using std::sort;
using std::abs;

typedef long long ll;

int main(void) {
    ll n, k;
    cin >> n >> k;

    vector<ll> c(1e5);
    for (ll i = 0; i < n; i++) {
        int a, b;
        cin >> a >> b;
        c[a - 1] += b;
    }

    for(ll i = 0; i < 1e5; i++){
        k -= c[i];
        if(k <= 0){
            cout << i + 1 << endl;
            break;
        }
    }

    return 0;
}

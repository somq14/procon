#include <algorithm>
#include <cctype>
#include <complex>
#include <functional>
#include <iomanip>
#include <iostream>
#include <queue>
#include <set>
#include <string>
#include <vector>

using std::cin;
using std::cout;
using std::endl;

using std::string;
using std::to_string;
using std::vector;
using std::set;
using std::queue;
using std::priority_queue;

using std::min;
using std::max;
using std::sort;
using std::abs;

int main(void) {
    string s, t;
    cin >> s >> t;

    sort(s.begin(), s.end(), std::less<char>());
    sort(t.begin(), t.end(), std::greater<char>());

    bool ans = s < t;

    cout << (ans ? "Yes" : "No") << endl;

    return 0;
}

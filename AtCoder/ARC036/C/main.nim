import strutils
import sequtils
import algorithm
import math
import queues
import tables

const INF* = int(1e18 + 373)

proc readInt1*(): int = stdin.readLine().strip().parseInt()
proc readInt2*(): (int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1])
proc readInt3*(): (int, int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1], v[2])

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#
const MOD = int(1e9 + 7)

type Op1[T] = proc(a: T, b: T): T
type Op2[T] = proc(a: T, b: T): T

proc repeatedSquares[T](x: T, n: Natural, e: T, f: Op2[T]): T =
    result = e
    var xx = f(x, e)
    var i = n
    while i > 0:
        if (i and 1) != 0:
            result = f(result, xx)
        xx = f(xx, xx)
        i = i shr 1


proc addM(a, b: Natural): Natural = (a + b) mod MOD
proc subM(a, b: Natural): Natural = (a - b + MOD) mod MOD
proc mulM(a, b: Natural): Natural = a * b mod MOD
proc powM(a, b: Natural): Natural = repeatedSquares[Natural](a, b, 1, `mulM`)
proc invM(a: Natural): Natural = a.powM(MOD - 2)
proc divM(a, b: Natural): Natural = a.mulM(invM(b))


var memoFactM = @[Natural(1)]
proc factM(n: Natural): Natural =
    if n < memoFactM.len():
        return memoFactM[n]
    for i in memoFactM.len()..n:
        memoFactM.add(memoFactM[i - 1].mulM(i))
    return memoFactM[n]


proc combM(n, r: Natural): Natural =
    if r > n:
        return 0
    if r > n div 2:
        return combM(n, n - r)

    result = 1
    for i in ((n - r) + 1)..n:
        result = result.mulM(i)
    result = result.divM(factM(r))


proc multCombM(n, r: Natural): Natural = combM(n + r - 1, n - 1)

#------------------------------------------------------------------------------#

proc main() =
  let (n, k) = readInt2()
  let s = stdin.readLine().strip()

  var dp = newSeq2[int](k + 1, k + 1)
  dp[0][0] = 1

  var dpNext = dp

  for i in 0..<n:
    for a in 0..k:
      for b in 0..k:
        dpNext[a][b] = 0
    for a in 0..k:
      for b in 0..k:
        case s[i]:
        of '0':
          if b < k:
            dpNext[max(a - 1, 0)][b + 1] = dpNext[max(a - 1, 0)][b + 1].addM(dp[a][b])
        of '1':
          if a < k:
            dpNext[a + 1][max(b - 1, 0)] = dpNext[a + 1][max(b - 1, 0)].addM(dp[a][b])
        of '?':
          if b < k:
            dpNext[max(a - 1, 0)][b + 1] = dpNext[max(a - 1, 0)][b + 1].addM(dp[a][b])
          if a < k:
            dpNext[a + 1][max(b - 1, 0)] = dpNext[a + 1][max(b - 1, 0)].addM(dp[a][b])
        else: discard
    dp = dpNext

  var ans = 0
  for a in 0..k:
    for b in 0..k:
      ans = ans.addM(dp[a][b])

  echo ans

main()


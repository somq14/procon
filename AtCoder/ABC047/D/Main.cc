#include <algorithm>
#include <complex>
#include <iostream>
#include <queue>
#include <set>
#include <stack>
#include <string>
#include <vector>

using std::cin;
using std::cout;
using std::endl;
using std::cerr;

using std::string;
using std::to_string;
using std::vector;
using std::set;
using std::queue;
using std::stack;
using std::priority_queue;

using std::min;
using std::max;
using std::sort;
using std::abs;

typedef long long int ll;
const int MOD = 1e9 + 7;

int main() {
    int n, t;
    cin >> n >> t;

    vector<int> a(n);
    for (int i = 0; i < n; i++) {
        cin >> a[i];
    }

    int m = a[0];
    for(int i = 0; i < n; i++){
        m = min(m, a[i]);
        a[i] = a[i] - m;
    }

    sort(a.begin(), a.end());

    int cnt = 0;
    for(int i = n - 1; i >= 0; i--){
        if(a[i] == a[n-1]){
            cnt++;
        }
    }

    cout << cnt << endl;

    return 0;
}

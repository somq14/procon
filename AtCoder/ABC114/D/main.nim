import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import future

const INF* = int(1e18 + 373)

proc readLine*(): string =
  stdin.readLine()
proc readSeq*(): seq[string] =
  readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = readLine().strip()
proc readInt1*(): int =
  readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])
proc newSeqWith*[T](n: Natural; e: T): seq[T] =
  result = newSeq[T](n)
  for i in 0..<n:
    result[i] = e
type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] =
  newSeqWith(n1, newSeq[T](n2))
type seq3*[T] = seq[seq[seq[T]]]
proc newSeq3*[T](n1, n2, n3: Natural): seq3[T] =
  newSeqWith(n1, newSeqWith(n2, newSeq[T](n3)))

#------------------------------------------------------------------------------#

proc main() =
  let n = readInt1()

  var a = newSeq[int](100)
  for i in 1..n:
    var m = i
    for j in 2..100:
      while m mod j == 0:
        m = m div j
        a[j] += 1
  a = a.filter(it => it > 0).map(it => it + 1)

  let cnt75 = a.filter(it => it >= 75).len()
  let cnt25 = a.filter(it => it >= 25).len()
  let cnt15 = a.filter(it => it >= 15).len()
  let cnt5 = a.filter(it => it >= 5).len()
  let cnt3 = a.filter(it => it >= 3).len()

  var ans = 0
  if cnt5 >= 2 and cnt3 >= 3:
    ans += cnt5 * (cnt5 - 1) div 2 * (cnt3 - 2)
  if cnt15 >= 1 and cnt5 >= 2:
    ans += cnt15 * (cnt5 - 1)
  if cnt25 >= 1 and cnt3 >= 2:
    ans += cnt25 * (cnt3 - 1)
  ans += cnt75
  echo ans

main()


#include <algorithm>
#include <cctype>
#include <complex>
#include <iomanip>
#include <iostream>
#include <queue>
#include <set>
#include <string>
#include <vector>

using std::cin;
using std::cout;
using std::endl;

using std::string;
using std::to_string;
using std::vector;
using std::set;
using std::queue;
using std::priority_queue;

using std::min;
using std::max;
using std::sort;
using std::abs;

int main(void) {
    long n, m;
    cin >> n >> m;

    const long sc = min(n, m / 2);
    n -= sc;
    m -= 2 * sc;

    const long cc = m / 4;

    cout << (sc + cc) << endl;
    return 0;
}

import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import logging
import future

const INF* = int(1e18 + 373)

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

proc readInt1*(): int = readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#

proc main() =
  let (n, m) = readInt2()
  let a = readSeq().map(parseInt)

  var tab = initTable[int, int]()

  var cum = 0
  for i in 0..<n:
    cum = (cum + a[i]) mod m
    if cum notin tab:
      tab[cum] = 0
    tab[cum] += 1

  var ans = 0
  var lcum = 0
  for i in 0..<n:
    let target = lcum mod m
    if target in tab:
      ans += tab[target]
    lcum += a[i]
    tab[lcum mod m] -= 1

  # echo tab
  echo ans


main()


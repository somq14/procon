import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import future

const INF* = int(1e18 + 373)

proc readLine*(): string =
  stdin.readLine()
proc readSeq*(): seq[string] =
  readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = readLine().strip()
proc readInt1*(): int =
  readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])
proc newSeqWith*[T](n: Natural; e: T): seq[T] =
  result = newSeq[T](n)
  for i in 0..<n:
    result[i] = e
type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] =
  newSeqWith(n1, newSeq[T](n2))
type seq3*[T] = seq[seq[seq[T]]]
proc newSeq3*[T](n1, n2, n3: Natural): seq3[T] =
  newSeqWith(n1, newSeqWith(n2, newSeq[T](n3)))

#------------------------------------------------------------------------------#
const N = 40

proc bin(v: int): string =
  result = ""
  for i in countdown(N - 1, 0):
    if (v and (1 shl i)) != 0:
      result &= "1"
    else:
      result &= "0"

proc main() =
  let n = readInt1()
  if n == 0:
    echo "0"
    return

  var bias = 0
  for i in 0..<N div 2:
    bias = bias or (1 shl (i + i + 1))

  var ans = n + bias
  for i in 0..<N div 2:
    ans = ans xor (1 shl (i + i + 1))

  var s = ""
  var oneAppeared = false
  for i in countdown(N - 1, 0):
    if (ans and (1 shl i)) != 0:
      s &= "1"
      oneAppeared = true
    else:
      if oneAppeared:
        s &= "0"

  echo s

main()


#include <algorithm>
#include <cctype>
#include <cmath>
#include <complex>
#include <iomanip>
#include <iostream>
#include <string>
#include <utility>
#include <vector>

using namespace std;

using ll = long long int;
const int MOD = 1e9 + 7;
const int INF = 1e9 + 373;

#define rep(i, n) for (int i = 0; i < (n); ++i)

template <typename T>
using vector2 = vector<vector<T>>;
template <typename T>
vector2<T> init_vector2(size_t n0, size_t n1, T e = T()) {
    return vector2<T>(n0, vector<T>(n1, e));
}

template <typename T>
using vector3 = vector<vector<vector<T>>>;
template <typename T>
vector3<T> init_vector3(size_t n0, size_t n1, size_t n2, T e = T()) {
    return vector3<T>(n0, vector2<T>(n1, vector<T>(n2, e)));
}

string solve(const string& s) {
    string ans;
    for (char c : s) {
        switch (tolower(c)) {
            case 'b':
            case 'c':
                ans.push_back('1');
                break;
            case 'd':
            case 'w':
                ans.push_back('2');
                break;
            case 't':
            case 'j':
                ans.push_back('3');
                break;
            case 'f':
            case 'q':
                ans.push_back('4');
                break;
            case 'l':
            case 'v':
                ans.push_back('5');
                break;
            case 's':
            case 'x':
                ans.push_back('6');
                break;
            case 'p':
            case 'm':
                ans.push_back('7');
                break;
            case 'h':
            case 'k':
                ans.push_back('8');
                break;
            case 'n':
            case 'g':
                ans.push_back('9');
                break;
            case 'z':
            case 'r':
                ans.push_back('0');
                break;
        }
    }

    return ans;
}

int main() {
    int n;
    cin >> n;

    vector<string> s(n);
    rep(i, n) { cin >> s[i]; }

    vector<string> ans;
    for (int i = 0; i < n; i++) {
        string t = solve(s[i]);
        if (t != "") {
            ans.push_back(t);
        }
    }

    int m = ans.size();
    for (int i = 0; i < m - 1; i++) {
        cout << ans[i] << " ";
    }
    if (m > 0) {
        cout << ans[m - 1] << endl;
    } else {
        cout << endl;
    }

    return 0;
}

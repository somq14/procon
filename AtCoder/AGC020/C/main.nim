import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import logging
import future

const INF* = int(1e18 + 373)

when not defined(release):
  addHandler(newFileLogger(stderr, lvlAll, "[ $levelname ] "))

proc readInt1*(): int = stdin.readLine().strip().parseInt()
proc readInt2*(): (int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1])
proc readInt3*(): (int, int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1], v[2])
proc readInt4*(): (int, int, int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1], v[2], v[3])

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#
const BIT_WIDTH = 64

type BitArray = ref object
  a: seq[int]
  n: int

proc initBitArray(n: int): BitArray =
  result.new()
  result.a = newSeq[int]((n + BIT_WIDTH - 1) div BIT_WIDTH)
  result.n = n

proc len(this: BitArray): int = this.n

proc `[]`(this: BitArray; i: Natural): int =
  return (this.a[i div BIT_WIDTH] shr (i mod BIT_WIDTH)) and 1

proc `[]=`(this: var BitArray; i: Natural; x: int) =
  let pat = this.a[i div BIT_WIDTH]
  let mask = 1 shl (i mod BIT_WIDTH)
  if x == 0:
    this.a[i div BIT_WIDTH] = pat and not mask
  else:
    this.a[i div BIT_WIDTH] = pat or mask

proc `shl`(this: BitArray; i: Natural): BitArray =
  result = initBitArray(this.len())
  let m = this.a.len()
  let off1 = i div BIT_WIDTH
  let off2 = i mod BIT_WIDTH

  if off2 == 0:
    for i in 0..<(m - off1):
      result.a[i + off1] = this.a[i]
    return

  for i in 0..<(m - off1):
    result.a[i + off1] = result.a[i + off1] or (this.a[i] shl off2)
    if i + off1 + 1 < m:
      result.a[i + off1 + 1] = result.a[i + off1 + 1] or (this.a[i] shr (BIT_WIDTH - off2))

proc `or`(this: BitArray; opr: BitArray): BitArray =
  let m = this.a.len()
  result = initBitArray(this.n)
  for i in 0..<m:
    result.a[i] = this.a[i] or opr.a[i]

#------------------------------------------------------------------------------#

proc main() =
  let n = readInt1()
  let a = readSeq().map(parseInt)
  let s = a.sum()
  let x = (s + 1) div 2

  var dp = initBitArray(s + 1)
  dp[0] = 1
  for i in 1..n:
    dp = dp or (dp shl a[i - 1])

  var ans = 0
  for i in x..s:
    if dp[i] == 1:
      ans = i
      break
  echo ans

main()


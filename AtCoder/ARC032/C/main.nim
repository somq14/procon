import strutils
import sequtils
import algorithm
import math
import queues

const INF = int(1e18 + 373)
const MOD = int(1e9 + 7)

proc readInt1(): int = stdin.readLine().strip().parseInt()
proc readInt2(): (int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1])
proc readInt3(): (int, int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1], v[2])
proc readInt(): seq[int] = stdin.readLine().strip().split().map(parseInt)

#------------------------------------------------------------------------------#
# FIXME: Use object
type PriorityQueue[T] = tuple[a: seq[T], n: Natural, cmp: proc(a, b: T): bool]

proc initPriorityQueue[T](cmp: proc(a, b: T): bool): PriorityQueue[T] =
    (newSeq[T](1), Natural(0), cmp)

proc len[T](this: PriorityQueue[T]): Natural = this.n

proc enqueue[T](this: var PriorityQueue[T], e: T) =
    this.n += 1
    this.a.add(e)
    var i = this.a.len() - 1
    while i > 1 and this.cmp(this.a[i], this.a[i div 2]):
        swap(this.a[i], this.a[i div 2])
        i = i div 2

proc dequeue[T](this: var PriorityQueue[T]): T =
    this.n -= 1
    result = this.a[1]
    this.a[1] = this.a.pop()
    var i = 1
    while i < this.a.len():
        var j = i
        if i * 2 < this.a.len() and this.cmp(this.a[i * 2], this.a[j]):
            j = i * 2
        if i * 2 + 1 < this.a.len() and this.cmp(this.a[i * 2 + 1], this.a[j]):
            j = i * 2 + 1
        if i == j:
            break
        swap(this.a[i], this.a[j])
        i = j

# FIXME: throw Exception when this is empty
proc front[T](this: PriorityQueue[T]): T = this.a[1]

#------------------------------------------------------------------------------#

type Work = tuple[i, a, b: int]

proc compareWork(w1, w2: Work): bool = w1.a > w2.a

proc main() =
    let n = readInt1()

    var ws = newSeq[Work](n)
    var t = 0
    for i in 0..<n:
        let (a, b) = readInt2()
        t = max(t, b)
        ws[i] = (i, a, b)

    var q = initPriorityQueue[Work](compareWork)
    for i in 0..<n:
        q.enqueue(ws[i])

    var dpOpt = newSeq[int](t + 1)
    var dpInd = newSeq[int](t + 1)
    dpOpt[t] = 0
    dpInd[t] = -1
    for i in countdown(t - 1, 0):
        # do not work
        dpOpt[i] = dpOpt[i + 1]
        dpInd[i] = dpInd[i + 1]

        # do some work
        while q.len() > 0 and q.front().a == i:
            let w = q.dequeue()
            let v = dpOpt[w.b] + 1
            if v > dpOpt[i] or (v == dpOpt[i] and w.i < dpInd[i]):
                dpOpt[i] = v
                dpInd[i] = w.i

    echo dpOpt[0]
    # echo dpOpt
    # echo dpInd

    var ans = newSeq[int](0)
    var i = 0
    while dpInd[i] >= 0:
        ans.add(dpInd[i] + 1)
        i = ws[dpInd[i]].b
    echo ans.map(proc(x: int): string = $x).join(" ")

main()


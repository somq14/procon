import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import future

const INF* = int(1e18 + 373)

proc readLine*(): string =
  stdin.readLine()
proc readSeq*(): seq[string] =
  readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = readLine().strip()
proc readInt1*(): int =
  readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])
proc newSeqWith*[T](n: Natural; e: T): seq[T] =
  result = newSeq[T](n)
  for i in 0..<n:
    result[i] = e
type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] =
  newSeqWith(n1, newSeq[T](n2))
type seq3*[T] = seq[seq[seq[T]]]
proc newSeq3*[T](n1, n2, n3: Natural): seq3[T] =
  newSeqWith(n1, newSeqWith(n2, newSeq[T](n3)))

#------------------------------------------------------------------------------#

proc buildCumTab(a: seq[int]): seq[int] =
  let n = a.len()
  var c = newSeq[int](n + 1)
  for i in 1..n:
    c[i] = c[i - 1] + a[i - 1]
  return c

# [s, t)
proc lookupCumTab(c: seq[int]; s, t: int): int = c[t] - c[s]

proc solve(n, ll: int; x: seq[int]): int =
  let xcum = x.buildCumTab()

  var ans = 0
  for i in 0..<n:
    let m = n - i
    let m0 = ((m - 1) + 1) div 2
    let m1 = 1
    let m2 = (m - 1) div 2

    var sol = 0
    sol += 2 * xcum.lookupCumTab(i, i + m0)
    if m mod 2 == 0:
      sol -= xcum.lookupCumTab(i + m0, i + m0 + m1)
    else:
      sol += xcum.lookupCumTab(i + m0, i + m0 + m1)
    sol -= 2 * xcum.lookupCumTab(i + m0 + m1, i + m0 + m1 + m2)
    sol += (n - 1 - i) * ll

    ans = max(ans, sol)

  return ans

proc main() =
  let (ll, n) = readInt2()
  let x = readSeq(n).map(parseInt)
  let solL = solve(n, ll, x)
  let solR = solve(n, ll, x.map(it => ll - it).sorted(cmp[int]))
  echo max(solL, solR)

main()


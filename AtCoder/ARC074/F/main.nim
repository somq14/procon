import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import future

const INF* = int(1e15 + 373)

proc readLine*(): string =
  stdin.readLine()
proc readSeq*(): seq[string] =
  readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = readLine().strip()
proc readInt1*(): int =
  readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])
proc newSeqWith*[T](n: Natural; e: T): seq[T] =
  result = newSeq[T](n)
  for i in 0..<n:
    result[i] = e
type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] =
  newSeqWith(n1, newSeq[T](n2))
type seq3*[T] = seq[seq[seq[T]]]
proc newSeq3*[T](n1, n2, n3: Natural): seq3[T] =
  newSeqWith(n1, newSeqWith(n2, newSeq[T](n3)))

#------------------------------------------------------------------------------#

type Edge = tuple [ u, v, w, r: int ]

proc addEdge(g: var seq2[Edge]; u, v, uvw, vuw: int) =
  let uvEdgeId = g[u].len()
  let vuEdgeId = g[v].len()
  g[u].add((u, v, uvw, vuEdgeId))
  g[v].add((v, u, vuw, uvEdgeId))

proc dinic(g: seq2[Edge]; s, t: int): int =

  proc bfs(g: var seq2[Edge]; s: int; dep: var seq[int]) =
    dep.fill(-1)

    var q = initQueue[int]()
    q.enqueue(s)
    dep[s] = 0

    while q.len() > 0:
      let u = q.dequeue()
      for e in g[u]:
        let v = e.v
        if dep[v] >= 0:
          continue
        if e.w > 0:
          dep[v] = dep[u] + 1
          q.enqueue(v)

  proc dfs(g: var seq2[Edge]; u, t, f: int; itr, dep: var seq[int]): int =
    if u == t:
      return f

    while itr[u] < g[u].len():
      let e = g[u][itr[u]]
      let v = e.v
      if e.w > 0 and dep[v] > dep[u]:
        let res = dfs(g, v, t, min(f, e.w), itr, dep)
        if res > 0:
          g[u][itr[u]].w -= res
          g[v][e.r].w += res
          return res
      itr[u] += 1

    return 0

  var g = g
  let n = g.len()

  var dep = newSeq[int](n)
  var itr = newSeq[int](n)

  var flow = 0
  while true:
    bfs(g, s, dep)

    if dep[t] == -1:
      break

    itr.fill(0)
    while true:
      let res = dfs(g, s, t, INF, itr, dep)
      if res <= 0:
        break
      flow += res

  return flow



proc main() =
  let (h, w) = readInt2()
  var a = newSeq[string](h)
  for i in 0..<h:
    a[i] = readLine()

  let n = h + w + 2
  var g = newSeq2[Edge](n, 0)

  for y in 0..<h:
    for x in 0..<w:
      if a[y][x] == '.':
        continue

      if a[y][x] == 'S':
        g.addEdge(n - 2, y, INF, 0)
        g.addEdge(n - 2, h + x, INF, 0)
        continue

      if a[y][x] == 'T':
        g.addEdge(n - 1, y, 0, INF)
        g.addEdge(n - 1, h + x, 0, INF)
        continue

      g.addEdge(y, h + x, 1, 1)

  let ans = dinic(g, n - 2, n - 1)
  if ans >= INF:
    echo -1
  else:
    echo ans

main()


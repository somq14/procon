import strutils
import sequtils
import algorithm
import math
import queues
import tables
import logging
import future

const INF* = int(1e18 + 373)

when not defined(release):
  addHandler(newFileLogger(stderr, lvlAll, "[ $levelname ] "))

proc readInt1*(): int = stdin.readLine().strip().parseInt()
proc readInt2*(): (int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1])
proc readInt3*(): (int, int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1], v[2])

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

type seq3*[T] = seq[seq[seq[T]]]

#------------------------------------------------------------------------------#
const MOD = int(1e9 + 7)

type Op1[T] = proc(a: T, b: T): T
type Op2[T] = proc(a: T, b: T): T

proc repeatedSquares[T](x: T, n: Natural, e: T, f: Op2[T]): T =
    result = e
    var xx = f(x, e)
    var i = n
    while i > 0:
        if (i and 1) != 0:
            result = f(result, xx)
        xx = f(xx, xx)
        i = i shr 1


proc addM(a, b: Natural): Natural = (a + b) mod MOD
proc subM(a, b: Natural): Natural = (a - b + MOD) mod MOD
proc mulM(a, b: Natural): Natural = a * b mod MOD
proc powM(a, b: Natural): Natural = repeatedSquares[Natural](a, b, 1, `mulM`)
proc invM(a: Natural): Natural = a.powM(MOD - 2)
proc divM(a, b: Natural): Natural = a.mulM(invM(b))


var memoFactM = @[Natural(1)]
proc factM(n: Natural): Natural =
    if n < memoFactM.len():
        return memoFactM[n]
    for i in memoFactM.len()..n:
        memoFactM.add(memoFactM[i - 1].mulM(i))
    return memoFactM[n]


proc combM(n, r: Natural): Natural =
    if r > n:
        return 0
    if r > n div 2:
        return combM(n, n - r)

    result = 1
    for i in ((n - r) + 1)..n:
        result = result.mulM(i)
    result = result.divM(factM(r))


proc multCombM(n, r: Natural): Natural = combM(n + r - 1, n - 1)

#------------------------------------------------------------------------------#
# [s, t]
type Constraint = tuple [ s, x: int ]

proc solve(r, g, b: int; rs: seq2[Constraint]; memo: var seq3[int32]): int32 =
  if memo[r][g][b] >= 0:
    return memo[r][g][b]

  if r > g or g > b:
    var rr = r
    var gg = g
    var bb = b
    if rr > gg:
      swap(rr, gg)
    if gg > bb:
      swap(gg, bb)
    if rr > gg:
      swap(rr, gg)
    memo[r][g][b] = solve(rr, gg, bb, rs, memo)
    return memo[r][g][b]

  if (r != 0 and r == g) or (g != 0 and g == b):
    memo[r][g][b] = 0
    return memo[r][g][b]

  for c in rs[b]:
    case c.x:
    of 1:
      if not (r < c.s and g < c.s):
        memo[r][g][b] = 0
        return 0
    of 2:
      if not (r < c.s and c.s <= g):
        memo[r][g][b] = 0
        return 0
    of 3:
      if not (c.s <= r and c.s <= g):
        memo[r][g][b] = 0
        return 0
    else: discard

  var ans = 0
  if g + 1 == b:
    for i in 0..<b:
      ans = ans.addM(solve(r, g, i, rs, memo))
  else:
    ans = solve(r, g, b - 1, rs, memo)

  memo[r][g][b] = ans.int32
  return memo[r][g][b]

proc main() =
  let (n, m) = readInt2()

  var rs = newSeq2[Constraint](n + 1, 0)
  for i in 0..<m:
    let (s, t, x) = readInt3()
    rs[t].add((s, x))

  var memo = newSeqWith(n + 1, newSeqWith(n + 1, newSeq[int32](n + 1)))
  for r in 0..n:
    for g in 0..n:
      for b in 0..n:
        memo[r][g][b] = -1
  memo[0][0][0] = 1

  var ans = 0
  for r in 0..n:
    for g in 0..n:
      for b in 0..n:
        if r == n or g == n or b == n:
          ans = ans.addM(solve(r, g, b, rs, memo))

  echo ans

main()


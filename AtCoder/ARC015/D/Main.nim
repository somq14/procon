import strutils
import algorithm

type Cookie = tuple[q: float, x: int, t: int]

proc `<`(a : Cookie, b : Cookie) : bool = a.t < b.t

proc cummul1(a : seq[float]) : seq[float] =
    # c[i] = c[0] * c[1] * ... * c[i-1]
    var c = newSeq[float](a.len() + 1)
    c[0] = 1
    for i in 1..a.len():
        c[i] = c[i-1] * a[i-1]
    return c

proc solve(t : int, n : int, p : float, a : seq[Cookie]) : float =

    # e[0] is not used
    var e = newSeq[float](t+1)
    e[0] = 1.0

    var sum_e : float = 0.0
    var sum_p : float = 0.0
    var j : int = 0
    for i in countdown(t, 1):
        while j < n and a[j].t >= i:
            sum_p += p * a[j].q
            sum_e += p * a[j].q * float(a[j].x)
            j += 1
        e[i] = sum_e + (1.0 - sum_p)

    let cum_e : seq[float] = cummul1(e)

    var sum : float = 0.0
    for i in 0..<t:
        sum += cum_e[i + 1]

    return sum

proc main() : void =
    let ln = stdin.readLine().split()
    let t = parseInt(ln[0])
    let n = parseInt(ln[1])
    let p = parseFloat(ln[2])

    var a = newSeq[Cookie](n)
    for i in 0..<n:
        let ln = stdin.readLine().split()
        a[i].q = ln[0].parseFloat()
        a[i].x = ln[1].parseInt()
        a[i].t = ln[2].parseInt()

    sort(a, system.cmp[Cookie], Descending)

    echo formatFloat(solve(t, n, p, a), ffDecimal)

main()


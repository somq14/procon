import strutils
import sequtils
import tables

let n = stdin.readLine().parseInt()

var ls = newSeq[string](n)
var ms = newSeq[int](n)
var ss = newSeq[string](n)

for i in 0..<n:
    let line : seq[string] = stdin.readLine().split()
    ls[i] = line[0]
    ms[i] = line[1].parseInt()
    ss[i] = line[2]

var tab = newTable[string, int]()
for s in ls:
    if tab.contains(s):
        continue
    tab[s] = tab.len()

for s in ss:
    if tab.contains(s):
        continue
    tab[s] = tab.len()

let m = tab.len()

var rtab = newSeq[string](m)
for s in ls:
    rtab[tab[s]] = s
for s in ss:
    rtab[tab[s]] = s


var g = newSeqWith(m, newSeq[float64](m))

for i in 0..<m:
    for j in 0..<m:
        g[i][j] = 0.0

for i in 0..<m:
    g[i][i] = 1.0

for i in 0..<n:
    let
        a = tab[ls[i]]
        b = tab[ss[i]]
    g[a][b] = float64(ms[i])

for k in 0..<m:
    for i in 0..<m:
        for j in 0..<m:
            if g[i][j] != 0:
                continue
            if g[i][k] != 0 and g[k][j] != 0:
                g[i][j] = g[i][k] * g[k][j]
            if g[i][k] != 0 and g[j][k] != 0:
                g[i][j] = g[i][k] / g[j][k]
            if g[k][i] != 0 and g[k][j] != 0:
                g[i][j] = g[k][j] / g[k][i]
            if g[k][i] != 0 and g[j][k] != 0:
                g[i][j] = 1.0 / g[k][i] / g[j][k]

var ans_i = 0
var ans_j = 0
for i in 0..<m:
    for j in 0..<m:
        if g[i][j] > g[ans_i][ans_j]:
            ans_i = i
            ans_j = j

echo(1, rtab[ans_i], "=", int64(g[ans_i][ans_j] + 0.01), rtab[ans_j])

#include <algorithm>
#include <complex>
#include <iomanip>
#include <iostream>
#include <queue>
#include <set>
#include <string>
#include <vector>

using std::cin;
using std::cout;
using std::endl;

using std::string;
using std::to_string;
using std::vector;
using std::set;
using std::queue;
using std::priority_queue;

using std::min;
using std::max;
using std::sort;
using std::abs;

typedef unsigned long long ll;

bool eval(int n, const vector<ll>& h, ll a, ll b, ll x) {
    const ll bias = b * x;

    ll cnt = 0;
    for (int i = 0; i < n; i++) {
        if (h[i] <= bias) {
            continue;
        }
        ll rest = h[i] - bias;
        cnt += (rest + (a - b - 1)) / (a - b);
    }
    return cnt <= x;
}

int main(void) {
    ll n, a, b;
    cin >> n >> a >> b;

    vector<ll> h(n);
    for (ll i = 0; i < n; i++) {
        cin >> h[i];
    }
    sort(h.begin(), h.end());

    // (lb, ub]
    ll ub = 1e10;
    ll lb = -1;
    while (ub - lb > 1) {
        const ll mid = lb + (ub - lb) / 2;
        if (eval(n, h, a, b, mid)) {
            ub = mid;
        } else {
            lb = mid;
        }
    }

    cout << ub << endl;

    return 0;
}

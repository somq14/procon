#include <algorithm>
#include <complex>
#include <iomanip>
#include <iostream>
#include <queue>
#include <set>
#include <string>
#include <vector>

using std::cin;
using std::cout;
using std::endl;

using std::string;
using std::to_string;
using std::vector;
using std::set;
using std::queue;
using std::priority_queue;

using std::min;
using std::max;
using std::sort;
using std::abs;

int main(void) {
    int n;
    cin >> n;

    vector<int> a(n);
    for(int i = 0; i < n; i++){
        cin >> a[i];
    }

    int sum = 0;
    for(int i = 0; i < n; i++){
        sum += a[i];
    }

    if(sum % 10 != 0){
        cout << sum << endl;
        return 0;
    }

    sort(a.begin(), a.end());
    int sub = -1;
    for(int i = 0; i < n; i++){
        if(a[i] % 10 != 0){
            sub = a[i];
            break;
        }
    }

    cout << (sub == -1 ? 0 : (sum - sub)) << endl;

    return 0;
}

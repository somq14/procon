import strutils
import sequtils
import algorithm
import math
import queues
import tables
import logging
import future

const INF* = int(1e18 + 373)

when not defined(release):
  addHandler(newFileLogger(stderr, lvlAll, "[ $levelname ] "))

proc readInt1*(): int = stdin.readLine().strip().parseInt()
proc readInt2*(): (int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1])
proc readInt3*(): (int, int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1], v[2])

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#

proc main() =
  let (n, m) = readInt2()

  var g = newSeq2[int](n, 0)
  for i in 0..<m:
    let (a, b) = readInt2()
    g[a - 1].add(b - 1)
    g[b - 1].add(a - 1)


  let q = readInt1()
  var qs = newSeq[(int, int, int)](q)
  for i in 0..<q:
    let (v, d, c) = readInt3()
    qs[i] = (v - 1, d, c)
  qs.reverse()

  var dp = newSeq2[int](n, 10 + 1)
  for i in 0..<n:
    dp[i].fill(INF)

  for i in 0..<q:
    let (v, d, c) = qs[i]
    dp[v][d] = min(dp[v][d], i)

  for d in countdown(10, 1):
    for v in 0..<n:
      dp[v][d - 1] = min(dp[v][d - 1], dp[v][d])
      for u in g[v]:
        dp[u][d - 1] = min(dp[u][d - 1], dp[v][d])

  for v in 0..<n:
    echo if dp[v][0] == INF: 0 else: qs[dp[v][0]][2]

main()



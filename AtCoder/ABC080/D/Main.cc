#include <algorithm>
#include <complex>
#include <iostream>
#include <queue>
#include <set>
#include <string>
#include <vector>

using std::cin;
using std::cout;
using std::endl;

using std::string;
using std::to_string;
using std::vector;
using std::set;
using std::queue;
using std::priority_queue;

using std::min;
using std::max;
using std::sort;
using std::abs;

class event {
   public:
    int kind;  // 0: end, 1: begin
    int time;
    int chan;

    event() : kind(0), time(0), chan(0) {}
    event(int kind, int time, int chan) : kind(kind), time(time), chan(chan) {}

    bool operator<(const event& e) const { return time < e.time; }
};

int main() {
    int n, c;
    cin >> n >> c;

    vector<event> events;

    for (int i = 0; i < n; i++) {
        int s, t, c;
        cin >> s >> t >> c;

        events.push_back(event(1, 2 * s - 1, c - 1));
        events.push_back(event(0, 2 * t, c - 1));
    }

    sort(events.begin(), events.end());

    int rec_cnt = 0;
    int on_recoder_num = 0;
    int off_recoder_num = 0;

    vector<int> recoding(c);
    for (const event& e : events) {
        if (e.kind == 0) {  // end
            recoding[e.chan]--;
            if (recoding[e.chan] == 0) {
                on_recoder_num--;
                off_recoder_num++;
            }
        } else {  // begin
            if (recoding[e.chan] > 0) {
                recoding[e.chan]++;
                continue;
            }
            if (off_recoder_num == 0) {
                rec_cnt++;
                off_recoder_num++;
            }

            off_recoder_num--;
            on_recoder_num++;
            recoding[e.chan] = 1;
        }
    }

    cout << rec_cnt << endl;

    return 0;
}

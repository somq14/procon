#include <algorithm>
#include <complex>
#include <iostream>
#include <queue>
#include <set>
#include <string>
#include <vector>

using std::cin;
using std::cout;
using std::endl;

using std::string;
using std::to_string;
using std::vector;
using std::set;
using std::queue;

using std::min;
using std::max;
using std::sort;
using std::abs;

int main() {
    int n, a, b;
    cin >> n >> a >> b;
    int ans = min(n * a, b);
    cout << ans << endl;
    return 0;
}

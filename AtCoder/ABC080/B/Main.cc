#include <algorithm>
#include <complex>
#include <iostream>
#include <queue>
#include <set>
#include <string>
#include <vector>

using std::cin;
using std::cout;
using std::endl;

using std::string;
using std::to_string;
using std::vector;
using std::set;
using std::queue;

using std::min;
using std::max;
using std::sort;
using std::abs;

int main() {
    int n;
    cin >> n;

    int sum = 0;
    for(int i = n; i > 0; i /= 10){
        sum += i % 10;
    }

    bool ans = n % sum == 0;
    cout << (ans ? "Yes" : "No") << endl;
    return 0;
}

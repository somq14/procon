#include <algorithm>
#include <complex>
#include <iostream>
#include <queue>
#include <set>
#include <string>
#include <vector>

using std::cin;
using std::cout;
using std::endl;

using std::string;
using std::to_string;
using std::vector;
using std::set;
using std::queue;

using std::min;
using std::max;
using std::sort;
using std::abs;

int eval(int n, const vector<vector<int>>& f, const vector<vector<int>>& p, int set){
    vector<int> c(n);
    for(int i = 0; i < 10; i++){
        if((set & (1 << i)) == 0){
            continue;
        }
        for(int j = 0; j < n; j++){
            if(f[j][i]){
                c[j]++;
            }
        }
    }

    int sum = 0;
    for(int i = 0; i < n; i++){
        sum += p[i][c[i]];
    }
    return sum;
}


int main() {
    int n;
    cin >> n;

    vector<vector<int>> f(n, vector<int>(10));
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < 10; j++) {
            cin >> f[i][j];
        }
    }

    vector<vector<int>> p(n, vector<int>(10 + 1));
    for (int i = 0; i < n; i++) {
        for (int j = 0; j <= 10; j++) {
            cin >> p[i][j];
        }
    }

    int ans = eval(n, f, p, 1);
    for(int set = 2; set < 1024; set++){
        ans = max(ans, eval(n, f, p, set));
    }
    cout << ans << endl;

    return 0;
}

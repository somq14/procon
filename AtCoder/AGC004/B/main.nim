import strutils
import sequtils
import algorithm
import math
import queues
import tables
import logging
import future

const INF* = int(1e18 + 373)

when not defined(release):
  addHandler(newFileLogger(stderr, lvlAll, "[ $levelname ] "))

proc readInt1*(): int = stdin.readLine().strip().parseInt()
proc readInt2*(): (int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1])
proc readInt3*(): (int, int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1], v[2])

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#

proc main() =
  let (n, x) = readInt2()
  let a = readSeq().map(parseInt)

  var dp = newSeq2[int](n + 1, n)
  for i in 0..<n:
    dp[0][i] = a[i]

  for k in 1..n:
    for i in 0..<n:
      dp[k][i] = min(dp[k - 1][i], a[(i - k + n) mod n])
  debug dp

  var ans = INF
  for k in 0..<n:
    ans = min(ans, sum(dp[k]) + k * x)
  echo ans


main()


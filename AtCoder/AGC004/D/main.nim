import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import future

const INF* = int(1e18 + 373)

proc readLine*(): string =
  stdin.readLine()
proc readSeq*(): seq[string] =
  readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = readLine().strip()
proc readInt1*(): int =
  readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])
proc newSeqWith*[T](n: Natural; e: T): seq[T] =
  result = newSeq[T](n)
  for i in 0..<n:
    result[i] = e
type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] =
  newSeqWith(n1, newSeq[T](n2))
type seq3*[T] = seq[seq[seq[T]]]
proc newSeq3*[T](n1, n2, n3: Natural): seq3[T] =
  newSeqWith(n1, newSeqWith(n2, newSeq[T](n3)))

#------------------------------------------------------------------------------#
# 頂点v以降を適切に処理したとの木の高さを返す
# 頂点1個からなる木の高さは1
# 戻り値は, 1以上k以下である
proc dfs(g: seq2[int]; k, v: int; ans: var int): int =
  if g[v].len() == 0:
    return 1

  var maxD = 0
  for u in g[v]:
    let d = dfs(g, k, u, ans)
    if d >= k:
      ans += 1
      continue
    maxD = max(maxD, d)

  return maxD + 1

proc main() =
  let (n, k) = readInt2()
  let a = readSeq().map(parseInt).map(it => it - 1)

  var g = newSeq2[int](n, 0)
  for i in 1..<n:
    g[a[i]].add(i)

  var ans = 0
  if a[0] != 0:
    ans += 1

  for u in g[0]:
    discard dfs(g, k, u, ans)

  echo ans

main()


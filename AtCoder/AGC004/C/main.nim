import strutils
import sequtils
import algorithm
import math
import queues
import tables
import logging
import future

const INF* = int(1e18 + 373)

when not defined(release):
  addHandler(newFileLogger(stderr, lvlAll, "[ $levelname ] "))

proc readInt1*(): int = stdin.readLine().strip().parseInt()
proc readInt2*(): (int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1])
proc readInt3*(): (int, int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1], v[2])

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#

proc main() =
  let (h, w) = readInt2()
  var a = newSeq[string](h)
  for i in 0..<h:
    a[i] = stdin.readLine()

  var b0 = newSeq2[char](h, w)
  var b1 = newSeq2[char](h, w)
  for i in 0..<h:
    for j in 0..<w:
      b0[i][j] = a[i][j]
      b1[i][j] = a[i][j]

  for i in 0..<w:
    b0[0][i] = '#'
    b1[h - 1][i] = '#'

  for j in 0..<w:
    if j mod 2 == 0:
      for i in 0..<h - 1:
        b0[i][j] = '#'

  for j in 0..<w:
    if j mod 2 == 1:
      for i in 1..<h:
        b1[i][j] = '#'

  for i in 0..<h:
    for j in 0..<w:
      stdout.write(b0[i][j])
    echo ""
  echo ""
  for i in 0..<h:
    for j in 0..<w:
      stdout.write(b1[i][j])
    echo ""

main()


import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import future

const INF* = int(1e18 + 373)

proc readLine*(): string =
  stdin.readLine()
proc readSeq*(): seq[string] =
  readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = readLine().strip()
proc readInt1*(): int =
  readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])
proc newSeqWith*[T](n: Natural; e: T): seq[T] =
  result = newSeq[T](n)
  for i in 0..<n:
    result[i] = e
type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] =
  newSeqWith(n1, newSeq[T](n2))
type seq3*[T] = seq[seq[seq[T]]]
proc newSeq3*[T](n1, n2, n3: Natural): seq3[T] =
  newSeqWith(n1, newSeqWith(n2, newSeq[T](n3)))

#------------------------------------------------------------------------------#

proc main() =
  let (n, k) = readInt2()
  let a = readSeq().map(parseInt)

  var c = newSeq[int](42)
  for i in 0..<42:
    var one = 0
    for j in 0..<n:
      if (a[j] and (1 shl i)) != 0:
        one += 1
    c[i] = one

  var dp = newSeq2[int](42, 2)

  dp[0][0] = max(c[0], n - c[0])
  dp[0][1] = c[0]
  if (k and (1 shl 0)) != 0:
    dp[0][1] = max(dp[0][1], n - c[0])

  for i in 1..<42:
    dp[i][0] = (1 shl i) * max(c[i], n - c[i]) + dp[i - 1][0]

    if (k and (1 shl i)) == 0:
      dp[i][1] = (1 shl i) * c[i] + dp[i - 1][1]
      continue

    dp[i][1] = max((1 shl i) * c[i] + dp[i - 1][0], (1 shl i) * (n - c[i]) + dp[i - 1][1])

  echo dp[41][1]

main()


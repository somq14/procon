import strutils
import sequtils
import algorithm
import math
import queues
import tables
import logging
import future

const INF* = int(1e18 + 373)

when not defined(release):
  addHandler(newFileLogger(stderr, lvlAll, "[ $levelname ] "))

proc readInt1*(): int = stdin.readLine().strip().parseInt()
proc readInt2*(): (int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1])
proc readInt3*(): (int, int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1], v[2])

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#
proc solve(n: int; a: seq2[int]): bool =
  var a = a

  for i in 0..<n:
    debug a
    for j in i..<n:
      if a[j][i] == 1:
        swap(a[i], a[j])
        break

    if a[i][i] == 0:
      return true

    for j in (i + 1)..<n:
      if a[j][i] == 0:
        continue
      for k in i..<n:
        a[j][k] = (a[j][k] - a[i][k] + 2) mod 2

  debug a

  return a[n - 1][n - 1] == 0

proc main() =
  let n = readInt1()

  var a = newSeq2[int](n, n)
  for i in 0..<n:
    let s = stdin.readLine().strip()
    for j in 0..<n:
      a[i][j] = if s[j] == '1': 1 else: 0

  echo if solve(n, a): "Even" else: "Odd"


main()


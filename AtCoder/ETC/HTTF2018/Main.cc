#include <algorithm>
#include <cmath>
#include <complex>
#include <cstdio>
#include <ctime>
#include <ctime>
#include <functional>
#include <iomanip>
#include <iostream>
#include <map>
#include <queue>
#include <random>
#include <set>
#include <string>
#include <utility>
#include <vector>

using namespace std;

using ll = long long int;
#define int ll

const int MOD = 1e9 + 7;
const int INF = 1e15 + 373;

#define rep(i, n) for (int i = 0; i < (n); ++i)

template <typename T>
using vector2 = vector<vector<T>>;
template <typename T>
vector2<T> init_vector2(size_t n0, size_t n1, T e = T()) {
    return vector2<T>(n0, vector<T>(n1, e));
}

template <typename T>
using vector3 = vector<vector<vector<T>>>;
template <typename T>
vector3<T> init_vector3(size_t n0, size_t n1, size_t n2, T e = T()) {
    return vector3<T>(n0, vector2<T>(n1, vector<T>(n2, e)));
}

template <typename T>
ostream& operator<<(ostream& os, const vector<T>& v) {
    os << "[";
    for (T e : v) {
        os << e << ", ";
    }
    os << "]";
    return os;
}

/********************************************************************************/
mt19937 generator(1234);

static const int dy8[] = {-1, -1, -1, 0, 0, 1, 1, 1};
static const int dx8[] = {-1, 0, 1, -1, 1, -1, 0, 1};

int sub_flat(int n, vector2<int>& a, int y, int x, int h, int v) {
    int miny = max(0LL, y - (h - 1));
    int maxy = min(n - 1, y + (h - 1));

    int prevSum = 0;
    int currSum = 0;
    for (int i = miny; i <= maxy; i++) {
        int rest_h = h - abs(i - y);
        int minx = max(0LL, x - (rest_h - 1));
        int maxx = min(n - 1, x + (rest_h - 1));
        for (int j = minx; j <= maxx; j++) {
            prevSum += abs(a[i][j]);
            a[i][j] -= v;
            currSum += abs(a[i][j]);
        }
    }

    return currSum - prevSum;
}

void add_mountain(int n, vector2<int>& a, int y, int x, int h) {
    int miny = max(0LL, y - (h - 1));
    int maxy = min(n - 1, y + (h - 1));

    for (int i = miny; i <= maxy; i++) {
        int rest_h = h - abs(i - y);
        int minx = max(0LL, x - (rest_h - 1));
        int maxx = min(n - 1, x + (rest_h - 1));
        for (int j = minx; j <= maxx; j++) {
            int v = max(0LL, rest_h - abs(x - j));
            a[i][j] += v;
        }
    }
}

void sub_mountain(int n, vector2<int>& a, int y, int x, int h) {
    int miny = max(0LL, y - (h - 1));
    int maxy = min(n - 1, y + (h - 1));

    for (int i = miny; i <= maxy; i++) {
        int rest_h = h - abs(i - y);
        int minx = max(0LL, x - (rest_h - 1));
        int maxx = min(n - 1, x + (rest_h - 1));
        for (int j = minx; j <= maxx; j++) {
            int v = max(0LL, rest_h - abs(x - j));
            a[i][j] -= v;
        }
    }
}

bool sub_mountain_ok(int n, const vector2<int>& a, int y, int x, int h) {
    int miny = max(0LL, y - (h - 1));
    int maxy = min(n - 1, y + (h - 1));

    for (int i = miny; i <= maxy; i++) {
        int rest_h = h - abs(i - y);
        int minx = max(0LL, x - (rest_h - 1));
        int maxx = min(n - 1, x + (rest_h - 1));

        for (int j = minx; j <= maxx; j++) {
            int v = max(0LL, h - abs(y - i) - abs(x - j));
            if (v > a[i][j]) {
                return false;
            }
        }
    }
    return true;
}

int nibutan(int n, const vector2<int>& a, int y, int x, int lb, int ub) {
    while (ub - lb > 1) {
        int mid = (ub + lb) / 2;
        if (sub_mountain_ok(n, a, y, x, mid)) {
            lb = mid;
        } else {
            ub = mid;
        }
    }
    return lb;
}

class Solution {
   public:
    int score;
    vector<int> ys;
    vector<int> xs;
    vector<int> hs;
    Solution() : score(0), ys(0), xs(0), hs(0) {}
};

int eval(int n, const vector2<int>& a) {
    int ans = 0;
    rep(i, n) {
        rep(j, n) { ans += abs(a[i][j]); }
    }
    return 200000000 - ans;
}

Solution trial(int n, const vector2<int>& _a) {
    vector2<int> a(_a);

    Solution ans;
    vector<int>& ys = ans.ys;
    vector<int>& xs = ans.xs;
    vector<int>& hs = ans.hs;

    int th = 90;
    while (hs.size() < 1000) {
        bool updated;
        do {
            updated = false;

            for (int i = 0; i < 100; i++) {
                int y = generator() % n;
                int x = generator() % n;
                int h = min(a[y][x], 100LL);

                // [lb, ub)
                int ub = min(a[y][x], 100LL) + 1;
                int lb = th;
                h = nibutan(n, a, y, x, lb, ub);

                if (!sub_mountain_ok(n, a, y, x, h)) {
                    continue;
                }

                sub_mountain(n, a, y, x, h);

                ys.push_back(y);
                xs.push_back(x);
                hs.push_back(h);
                updated = true;

                if (hs.size() >= 1000) {
                    goto end;
                }
            }

        } while (updated);

        th = max(th - 10, 0LL);
    }

end:
    ans.score = eval(n, a);
    return ans;
}

clock_t begin_time;

Solution yamanobori(int n, const vector2<int>& _a, const Solution& _ans) {
    vector2<int> a(_a);

    Solution ans;
    ans = _ans;
    int& score = ans.score;
    vector<int>& ys = ans.ys;
    vector<int>& xs = ans.xs;
    vector<int>& hs = ans.hs;

    int m = hs.size();
    for (int i = 0; i < m; i++) {
        sub_mountain(n, a, ys[i], xs[i], hs[i]);
    }

    bool updated;
    do {
        updated = false;
        for (int i = 0; i < 100; i++) {
            int j = generator() % m;

            // higher
            if (hs[j] < 100) {
                int v = sub_flat(n, a, ys[j], xs[j], hs[j] + 1, 1);
                if (v < 0) {
                    // good
                    score += v;
                    hs[j] += 1;
                    updated = true;
                    // cerr << "score: " << score << " y: " << ys[j] << " x: "
                    // << xs[j] << " h: " << hs[j] << "(" << v << ")" << endl;
                } else {
                    // bad
                    sub_flat(n, a, ys[j], xs[j], hs[j] + 1, -1);
                }
            }

            // lower
            if (hs[j] > 1) {
                int v = sub_flat(n, a, ys[j], xs[j], hs[j], -1);
                if (v < 0) {
                    // good
                    score += v;
                    hs[j] -= 1;
                    updated = true;
                    // cerr << "score: " << score << " y: " << ys[j] << " x: "
                    // << xs[j] << " h: " << hs[j] << "(" << v << ")" << endl;
                } else {
                    // bad
                    sub_flat(n, a, ys[j], xs[j], hs[j], +1);
                }
            }

            // move
            for (int k = 0; k < 8; k++) {
                int yy = ys[j] + dy8[k];
                int xx = xs[j] + dx8[k];
                if (!(0 <= yy && yy < n && 0 <= xx && xx < n)) {
                    continue;
                }

                add_mountain(n, a, ys[j], xs[j], hs[j]);
                sub_mountain(n, a, yy, xx, hs[j]);

                int v = eval(n, a);
                if (v > score) {
                    // good
                    // cerr << "score: " << score << " y: " << ys[j] << " x: "
                    // << xs[j] << " h: " << hs[j] << "(" << (v - score) << ")"
                    // << endl;
                    score = v;
                    ys[j] = yy;
                    xs[j] = xx;
                    updated = true;
                } else {
                    // bad
                    add_mountain(n, a, yy, xx, hs[j]);
                    sub_mountain(n, a, ys[j], xs[j], hs[j]);
                }
            }
        }
    } while (1.0 * (clock() - begin_time) / CLOCKS_PER_SEC < 1);

    return ans;
}

signed main() {
    begin_time = clock();

    int n = 100;

    vector2<int> a = init_vector2<int>(n, n);
    rep(i, n) {
        rep(j, n) { cin >> a[i][j]; }
    }

    Solution ans = trial(n, a);
    ans = yamanobori(n, a, ans);
    // cerr << "score : " << ans.score << endl;

    int m = ans.hs.size();
    cout << m << endl;
    rep(k, m) {
        cout << ans.xs[k] << " " << ans.ys[k] << " " << ans.hs[k] << endl;
    }

    cerr << ans.score << endl;

    return 0;
}

import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import logging
import future

const INF* = int(1e18 + 373)

proc readLine*(): string =
  stdin.readLine()
proc readSeq*(): seq[string] =
  readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = readLine().strip()
proc readInt1*(): int =
  readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])
type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] =
  newSeqWith(n1, newSeq[T](n2))
type seq3*[T] = seq[seq[seq[T]]]
proc newSeq3*[T](n1, n2, n3: Natural): seq3[T] =
  newSeqWith(n1, newSeqWith(n2, newSeq[T](n3)))

#------------------------------------------------------------------------------#
proc north(dice: (int, int, int)): (int, int, int) =
  (dice[1], 7 - dice[0], dice[2])

proc right(dice: (int, int, int)): (int, int, int) =
  (dice[0], dice[2], 7 - dice[1])

proc left(dice: (int, int, int)): (int, int, int) =
  result = dice.right()
  result = result.right()
  result = result.right()

proc main() =
  let n = readInt1()
  var cmd = newSeq[string](n)
  for i in 0..<n:
    cmd[i] = readLine()

  var ans = 1
  var dice = (1, 2, 3)
  for i in 0..<n:
    case cmd[i]:
    of "North":
      dice = dice.north()
    of "East":
      dice = dice.left()
      dice = dice.north()
      dice = dice.right()
    of "South":
      dice = dice.right()
      dice = dice.right()
      dice = dice.north()
      dice = dice.right()
      dice = dice.right()
    of "West":
      dice = dice.right()
      dice = dice.north()
      dice = dice.left()
    of "Right":
      dice = dice.right()
    of "Left":
      dice = dice.left()
    else:
      discard

    ans += dice[0]

  echo ans

main()


import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import logging
import future

const INF* = int(1e18 + 373)

proc readLine*(): string = stdin.readLine()
proc readSeq*(): seq[string] = readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = readLine().strip()
proc readInt1*(): int = readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])
type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))
type seq3*[T] = seq[seq[seq[T]]]
proc newSeq3*[T](n1, n2, n3: Natural): seq3[T] = newSeqWith(n1, newSeqWith(n2, newSeq[T](n3)))

#------------------------------------------------------------------------------#

proc fa(a, b, c: int): int
proc fb(a, b, c: int): int
proc fc(a, b, c: int): int

proc fa(a, b, c: int): int =
  let all = a or b or c

  if all == 0:
    return 0

  let smallest = all and not (all - 1)

  if (smallest and a) != 0:
    return fa(a xor smallest, b, c)

  if (smallest and b) != 0:
    var ans = 0
    ans += fc(a, b xor smallest, c)
    ans += 1
    ans += fa(0, 0, all xor smallest)
    return ans

  if (smallest and c) != 0:
    var ans = 0
    ans += fa(a, b, c xor smallest)
    ans += 1
    ans += fc(all xor smallest, 0, 0)
    ans += 1
    ans += fa(0, 0, all xor smallest)
    return ans

proc fb(a, b, c: int): int =
  let all = a or b or c

  if all == 0:
    return 0

  let smallest = all and not (all - 1)

  if (smallest and a) != 0:
    var ans = 0
    ans += fc(a xor smallest, b, c)
    ans += 1
    ans += fb(0, 0, all xor smallest)

  if (smallest and b) != 0:
    return fb(a, b xor smallest, c)

  if (smallest and c) != 0:
    var ans = 0
    ans += fa(a, b, c xor smallest)
    ans += 1
    ans += fb(all xor smallest, 0, 0)
    return ans

proc fc(a, b, c: int): int =
  let all = a or b or c

  if all == 0:
    return 0

  let smallest = all and not (all - 1)

  if (smallest and a) != 0:
    var ans = 0
    ans += fc(a xor smallest, b, c)
    ans += 1
    ans += fa(0, 0, all xor smallest)
    ans += 1
    ans += fc(all xor smallest, 0, 0)
    return ans

  if (smallest and b) != 0:
    var ans = 0
    ans += fa(a, b xor smallest, c)
    ans += 1
    ans += fc(all xor smallest, 0, 0)
    return ans

  if (smallest and c) != 0:
    return fc(a, b, c xor smallest)

proc main() =
  let (n, m) = readInt2()

  var x = newSeq2[int](3, 0)
  for i in 0..<3:
    let xx = readSeq().map(parseInt).map(it => it - 1)
    x[i] = xx[1..<xx.len()]

  var y = newSeq[int](3)
  for i in 0..<3:
    for j in 0..<x[i].len():
      y[i] = y[i] or (1 shl x[i][j])

  let ans = min(fa(y[0], y[1], y[2]), fc(y[0], y[1], y[2]))
  if ans <= m:
    echo ans
  else:
    echo -1

main()


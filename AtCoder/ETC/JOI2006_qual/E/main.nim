import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import logging
import future

const INF* = int(1e18 + 373)

proc readLine*(): string =
  stdin.readLine()
proc readSeq*(): seq[string] =
  readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = readLine().strip()
proc readInt1*(): int =
  readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])
type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] =
  newSeqWith(n1, newSeq[T](n2))
type seq3*[T] = seq[seq[seq[T]]]
proc newSeq3*[T](n1, n2, n3: Natural): seq3[T] =
  newSeqWith(n1, newSeqWith(n2, newSeq[T](n3)))

#------------------------------------------------------------------------------#

proc add(a: var seq[int]; v: int) =
  var p = 1
  var q = v
  for i in 0..<a.len():
    a[i] += p div q
    p = p mod q
    p *= 10

proc normalize(a: var seq[int]) =
  for i in countdown(a.len() - 1, 1):
    a[i - 1] += a[i] div 10
    a[i] = a[i] mod 10

proc main() =
  let (n, k, m, r) = readInt4()

  var ans = newSeq[int](r + 100 + 1)

  ans.add(n)
  if m == 1:
    for i in 1..<n:
      ans.add(i * n)

  ans.normalize()

  stdout.write(ans[0])
  stdout.write('.')
  for i in 1..r:
    stdout.write(ans[i])
  echo ""

main()


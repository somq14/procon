import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import logging
import future

const INF* = int(1e18 + 373)

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

proc readInt1*(): int = readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#

proc dividers(n: int): seq[int] =
  result = @[]

  for i in 1..n:
    if i * i > n:
      break
    if n mod i == 0:
      result.add(i)
      result.add(n div i)
      if i == n div i:
        discard result.pop()

  result.sort(cmp[int])

proc gcd(a, b: int): int =
  var a = a
  var b = b
  while b != 0:
    let tmp = a mod b
    a = b
    b = tmp
  return a

proc main() =
  let (n, k) = readInt2()
  let a = readSeq().map(parseInt)

  let ds = dividers(k)
  var tab = initTable[int, int]()
  for i in 0..<ds.len():
    tab[ds[i]] = 0

  for i in 0..<n:
    let gcdAiK = gcd(a[i], k)
    tab[gcdAiK] += 1

  var ans = 0
  for i in 0..<ds.len():
    for j in 0..<ds.len():
      if ds[i] * ds[j] mod k != 0:
        continue

      if i == j:
        ans += tab[ds[i]] * (tab[ds[j]] - 1)
      else:
        ans += tab[ds[i]] * tab[ds[j]]

  echo ans div 2

main()


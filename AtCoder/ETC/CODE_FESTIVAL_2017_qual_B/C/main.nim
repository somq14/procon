import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import logging
import future

const INF* = int(1e18 + 373)

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

proc readInt1*(): int = readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#

proc isBipartiteGraph(g: seq2[int]): seq[bool] =
  let n = g.len()

  var stack = newSeq[int](0)
  var mark = newSeq[int](n)
  mark.fill(0)

  for v in 0..<n:
    if mark[v] != 0:
      continue

    mark[v] = 1
    stack.add(v)

    while stack.len() > 0:
      let v = stack.pop()
      for u in g[v]:
        if mark[v] == mark[u]:
          return nil
        if mark[u] == 0:
          mark[u] = -mark[v]
          stack.add(u)

  return mark.map(it => it > 0)

proc main() =
  let (n, m) = readInt2()
  var g = newSeq2[int](n, 0)

  for i in 0..<m:
    let (a, b) = readInt2()
    g[a - 1].add(b - 1)
    g[b - 1].add(a - 1)

  let b = g.isBipartiteGraph()

  var ans = 0
  if b == nil:
    ans = n * (n - 1) div 2 - m
  else:
    var n1 = 0
    var n2 = 0
    for v in 0..<n:
      if b[v]:
        n1 += 1
      else:
        n2 += 1
    ans = n1 * n2 - m

  echo ans


main()


import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import logging
import future

const INF* = int(1e18 + 373)

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

proc readInt1*(): int = readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#

proc main() =
  let n = readInt1()
  let s = stdin.readLine()

  var dp = newSeq[int](n + 1)
  dp[n] = 0
  dp[n - 1] = 0
  dp[n - 2] = 0
  var i = n - 3
  while i >= 0:
    if not (s[i] == '1' and s[i + 1] == '0' and s[i + 2] == '1'):
      dp[i] = dp[i + 1]
      i -= 1
      continue

    var ll = i
    while ll >= 0 and s[ll] == '1':
      ll -= 1

    var rr = i + 2
    while rr < n and s[rr] == '1':
      rr += 1

    let a = i - ll
    let b = rr - (i + 2)

    dp[i] = dp[i + 1]
    for j in 0..<b:
      dp[i] = max(dp[i], (j + 1) + dp[(i + 2) + j + 1])

    for j in 0..<a:
      dp[i - j] = max(dp[i], (j + 1) + dp[i + 3])

    i -= a

  echo dp[0]

main()


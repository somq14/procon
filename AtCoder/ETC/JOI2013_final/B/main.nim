import algorithm
import future
import macros
import math
import queues
import sequtils
import sets
import strutils
import tables

const INF* = int(1e18 + 373)

proc readLine*(): string =
  stdin.readLine().strip()
proc readSeq*(): seq[string] =
  readLine().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeqWith(n, readLine())
proc readIntSeq*(): seq[int] =
  result = readSeq().map(parseInt)
proc readIntSeq*(n: Natural): seq[int] =
  result = readSeq(n).map(parseInt)
proc readInt1*(): int =
  readLine().parseInt()
proc readInt2*(): (int, int) =
  let a = readIntSeq(); return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readIntSeq(); return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readIntSeq(); return (a[0], a[1], a[2], a[3])

proc newSeqOf*[T](n: Natural; e: T): seq[T] =
  result = newSeq[T](n)
  result.fill(e)
proc newSeq*[T](n: Natural; e: T): seq[T] =
  newSeqOf[T](n, e)
type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] =
  newSeqOf(n1, newSeq[T](n2))
proc newSeq2*[T](n1, n2: Natural; e: T): seq2[T] =
  newSeqOf(n1, newSeqOf(n2, e))
type seq3*[T] = seq[seq[seq[T]]]
proc newSeq3*[T](n1, n2, n3: Natural): seq3[T] =
  newSeqOf(n1, newSeqOf(n2, newSeq[T](n3)))
proc newSeq3*[T](n1, n2, n3: Natural; e: T): seq3[T] =
  newSeqOf(n1, newSeqOf(n2, newSeqOf(n3, e)))

when defined(ENABLE_DEBUG_MACRO):
  macro debug*(args: varargs[untyped]): untyped =
    result = nnkStmtList.newTree()
    for i in 0..<args.len():
      let par1 = newIdentNode("stderr")
      let par2 = newLit(args[i].repr)
      let par3 = newLit(" = ")
      let par4 = args[i]
      result.add(newCall("write", par1, par2, par3, par4))
      if i + 1 < args.len():
        result.add(newCall("write", newIdentNode("stderr"), newLit(", ")))
    result.add(newCall("writeLine", newIdentNode("stderr"), newLit("")))
else:
  macro debug*(args: varargs[untyped]): untyped =
    result = nnkDiscardStmt.newTree(newLit(nil))

#------------------------------------------------------------------------------#
#------------------------------------------------------------------------------#

proc main() =
  let (m, n) = readInt2()
  let s = readLine()
  let t = readLine()

  var dp = newSeq3[int](m + 1, n + 1, 2)

  for i in countdown(m, 0):
    for j in countdown(n, 0):
      if i == m and j == n:
        dp[m][n][0] = -INF
        dp[m][n][1] = 0
        continue

      # dp[i][j][0]
      block:
        var opt = -INF
        if i < m and s[i] == 'I' and dp[i + 1][j][1] != -INF:
          opt = max(opt, dp[i + 1][j][1] + 1)
        if j < n and t[j] == 'I' and dp[i][j + 1][1] != -INF:
          opt = max(opt, dp[i][j + 1][1] + 1)
        dp[i][j][0] = opt

      # dp[i][j][1]
      block:
        var opt = 0
        if i < m and s[i] == 'O' and dp[i + 1][j][0] != -INF:
          opt = max(opt, dp[i + 1][j][0] + 1)
        if j < n and t[j] == 'O' and dp[i][j + 1][0] != -INF:
          opt = max(opt, dp[i][j + 1][0] + 1)
        dp[i][j][1] = opt

  var ans = 0
  for i in 0..m:
    for j in 0..n:
      ans = max(ans, dp[i][j][0])
  echo ans

main()

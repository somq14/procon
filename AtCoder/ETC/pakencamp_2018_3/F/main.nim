import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import future

const INF* = int(1e18 + 373)

proc readLine*(): string =
  stdin.readLine()
proc readSeq*(): seq[string] =
  readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = readLine().strip()
proc readInt1*(): int =
  readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])
proc newSeqWith*[T](n: Natural; e: T): seq[T] =
  result = newSeq[T](n)
  for i in 0..<n:
    result[i] = e
type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] =
  newSeqWith(n1, newSeq[T](n2))
type seq3*[T] = seq[seq[seq[T]]]
proc newSeq3*[T](n1, n2, n3: Natural): seq3[T] =
  newSeqWith(n1, newSeqWith(n2, newSeq[T](n3)))

#------------------------------------------------------------------------------#
const MOD = int(10^9 + 7)

type Mint = object
  v: int

proc initMint(v: int): Mint =
  result.v = (v mod MOD + MOD) mod MOD

proc `$`(x: Mint): string =
  $x.v

proc `$$`(x: Mint): string =
  if abs(x.v) < abs(x.v - MOD):
    $x.v
  else:
    $(x.v - MOD)

proc pow(x: Mint; n: int): Mint =
  if n < 0:
    return pow(x, -n).pow(MOD - 2)

  var m = n
  var p = 1
  var xx = x.v
  while m > 0:
    if (m and 1) != 0:
      p = p * xx mod MOD
    xx = xx * xx mod MOD
    m = m shr 1

  result.v = p

proc inv(x: Mint): Mint =
  pow(x, MOD - 2)

proc `+`(x: Mint): Mint =
  result.v = x.v
proc `-`(x: Mint): Mint =
  result.v = (-x.v + MOD) mod MOD

proc `+`(x0: Mint, x1: Mint): Mint =
  result.v = (x0.v + x1.v) mod MOD
proc `-`(x0: Mint, x1: Mint): Mint =
  result.v = (x0.v - x1.v + MOD) mod MOD
proc `*`(x0: Mint, x1: Mint): Mint =
  result.v = x0.v * x1.v mod MOD
proc `/`(x0: Mint, x1: Mint): Mint =
  result.v = x0.v * x1.inv().v mod MOD
proc `^`(x0: Mint, x1: int): Mint =
  x0 ^ x1

proc `+`(x0: Mint, x1: int): Mint =
  x0 + initMint(x1)
proc `-`(x0: Mint, x1: int): Mint =
  x0 - initMint(x1)
proc `*`(x0: Mint, x1: int): Mint =
  x0 * initMint(x1)
proc `/`(x0: Mint, x1: int): Mint =
  x0 / initMint(x1)

proc `+`(x0: int, x1: Mint): Mint =
  initMint(x0) + x1
proc `-`(x0: int, x1: Mint): Mint =
  initMint(x0) - x1
proc `*`(x0: int, x1: Mint): Mint =
  initMint(x0) * x1
proc `/`(x0: int, x1: Mint): Mint =
  initMint(x0) / x1

proc `+=`(x0: var Mint; x1: Mint) =
  x0 = x0 + x1
proc `-=`(x0: var Mint; x1: Mint) =
  x0 = x0 - x1
proc `*=`(x0: var Mint; x1: Mint) =
  x0 = x0 * x1
proc `/=`(x0: var Mint; x1: Mint) =
  x0 = x0 / x1

proc `+=`(x0: var Mint; x1: int) =
  x0 = x0 + x1
proc `-=`(x0: var Mint; x1: int) =
  x0 = x0 - x1
proc `*=`(x0: var Mint; x1: int) =
  x0 = x0 * x1
proc `/=`(x0: var Mint; x1: int) =
  x0 = x0 / x1
proc `^=`(x0: var Mint; x1: int) =
  x0 = x0 ^ x1

#------------------------------------------------------------------------------#
type Pred1[T] = T -> bool

# [lb, ub)
proc nibutanLb(lb, ub: int; f: Pred1[int]): int =
  var lb = lb
  var ub = ub
  while ub - lb > 1:
    let mid = (ub + lb) div 2
    if f(mid):
      lb = mid
    else:
      ub = mid
  return lb

# (lb, ub]
proc nibutanUb(lb, ub: int; f: Pred1[int]): int =
  var lb = lb
  var ub = ub
  while ub - lb > 1:
    let mid = (ub + lb) div 2
    if f(mid):
      ub = mid
    else:
      lb = mid
  return ub
#------------------------------------------------------------------------------#

proc ll(a: int; i: int): Mint =
  let a = a.initMint()
  let i = i.initMint()
  i * a + i * (i - 1) / 2

proc rr(b: int; i: int): Mint =
  let b = b.initMint()
  let i = i.initMint()
  i * b - i * (i - 1) / 2

proc sum1(s, t: int): Mint =
  let s = s.initMint()
  let t = t.initMint()
  let tt = t * (t + 1) / 2
  let ss = (s - 1) * ((s - 1) + 1) / 2
  return tt - ss

proc sum2(s, t: int): Mint =
  let s = s.initMint()
  let t = t.initMint()
  let tt = t * (2 * t + 1) * (t + 1) / 6
  let ss = (s - 1) * (2 * (s - 1) + 1) * ((s - 1) + 1) / 6
  return tt - ss

proc subR(a, b: int): int =
  var lb = (b - a).float() / 2.0
  var ub = (b - a).float()
  for i in 0..<100:
    let mid = (lb + ub) / 2.0
    let val = mid * mid + (a - b).float() * mid + (a - 1).float()
    if val >= 0:
      ub = mid
    else:
      lb = mid

  return ub.ceil().int()

proc subL(a, b: int): int =
  var lb = 0.0
  var ub = (b - a).float() / 2.0
  for i in 0..<100:
    let mid = (lb + ub) / 2.0
    let val = mid * mid + (a - b).float() * mid + (a - 1).float()
    if val >= 0:
      lb = mid
    else:
      ub = mid

  return ub.floor().int()

proc solve(a, b: int): Mint =
  let n = b - a + 1

  var ans = initMint(0)
  ans += rr(b, n)
  ans -= ll(a, 1)
  ans += 1

  let btm = (b - a).float() / 2.0
  if btm * btm + (a - b).float() * btm + (a - 1).float() >= 0:
    ans -= sum2(1, n - 1)
    ans -= (a - b) * sum1(1, n - 1)
    ans -= (a - 1) * (n - 1 - 1 + 1).initMint()
    return ans

  block:
    let k = subR(a, b)
    ans -= sum2(k, n - 1)
    ans -= (a - b) * sum1(k, n - 1)
    ans -= (a - 1) * (n - 1 - k + 1).initMint()

  block:
    let k = subL(a, b)
    if k >= 1:
      ans -= sum2(1, k)
      ans -= (a - b) * sum1(1, k)
      ans -= (a - 1) * (k - 1 + 1).initMint()

  return ans

proc main() =
  let q = readInt1()
  var a = newSeq[int](q)
  var b = newSeq[int](q)
  for i in 0..<q:
    (a[i], b[i]) = readInt2()

  for i in 0..<q:
    echo solve(a[i], b[i])

main()


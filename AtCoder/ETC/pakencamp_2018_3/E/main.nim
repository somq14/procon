import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import future

const INF* = int(1e18 + 373)

proc readLine*(): string =
  stdin.readLine()
proc readSeq*(): seq[string] =
  readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = readLine().strip()
proc readInt1*(): int =
  readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])
proc newSeqWith*[T](n: Natural; e: T): seq[T] =
  result = newSeq[T](n)
  for i in 0..<n:
    result[i] = e
type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] =
  newSeqWith(n1, newSeq[T](n2))
type seq3*[T] = seq[seq[seq[T]]]
proc newSeq3*[T](n1, n2, n3: Natural): seq3[T] =
  newSeqWith(n1, newSeqWith(n2, newSeq[T](n3)))

#------------------------------------------------------------------------------#
proc solve(fib: seq[int]; x: int): int =
  var x = x
  for i in countdown(fib.len() - 1, 0):
    if x >= fib[i]:
      x -= fib[i]
  return if x == 0: 1 else: 0

proc solve(a, b, x: int): int =
  var fib = newSeq[int](88)
  fib[0] = a
  fib[1] = b
  for i in 2..87:
    fib[i] = min(fib[i - 1] + fib[i - 2], INF)

  var res = 0
  res += solve(fib[1..<88], x)
  if x - a >= 0:
    res += solve(fib[2..<88], x - a)
  return res

proc main() =
  let q = readInt1()
  for i in 0..<q:
    let (a, b, x) = readInt3()
    echo solve(a, b, x)

main()


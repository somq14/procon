import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import future

const INF* = int(1e18 + 373)

proc readLine*(): string =
  stdin.readLine()
proc readSeq*(): seq[string] =
  readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = readLine().strip()
proc readInt1*(): int =
  readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])
proc newSeqWith*[T](n: Natural; e: T): seq[T] =
  result = newSeq[T](n)
  for i in 0..<n:
    result[i] = e
type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] =
  newSeqWith(n1, newSeq[T](n2))
type seq3*[T] = seq[seq[seq[T]]]
proc newSeq3*[T](n1, n2, n3: Natural): seq3[T] =
  newSeqWith(n1, newSeqWith(n2, newSeq[T](n3)))

#------------------------------------------------------------------------------#
#------------------------------------------------------------------------------#
type Pred1[T] = T -> bool

# [lb, ub)
proc nibutanLb(lb, ub: int; f: Pred1[int]): int =
  var lb = lb
  var ub = ub
  while ub - lb > 1:
    let mid = (ub + lb) div 2
    if f(mid):
      lb = mid
    else:
      ub = mid
  return lb

# (lb, ub]
proc nibutanUb(lb, ub: int; f: Pred1[int]): int =
  var lb = lb
  var ub = ub
  while ub - lb > 1:
    let mid = (ub + lb) div 2
    if f(mid):
      ub = mid
    else:
      lb = mid
  return ub
#------------------------------------------------------------------------------#
type Dat = tuple [ s, k: int, c: char ]

const BUF = 2

proc main() =
  let n = readInt1()
  let s = readLine()

  let m = readInt1()
  var ds = newSeq[char](m)
  var fs = newSeq[char](m)
  for i in 0..<m:
    let input = readLine().split()
    ds[i] = input[0][0]
    fs[i] = input[1][0]

  let q = readInt1()
  var qs = newSeq[(int, int, int)](q)
  for i in 0..<q:
    let (t, p) = readInt2()
    qs[i] = (t - 1, p - 1, i)
  qs.sort(cmp[(int, int, int)])

  var x = newSeq[Dat](BUF)
  block:
    var i = 0
    while i < n:
      var j = i
      while j < n and s[i] == s[j]:
        j += 1
      x.add((i, j - i, s[i]))
      i = j

  var xs = BUF
  var xt = x.len()
  for i in 0..<BUF:
    x.add((0, 0, '?'))

  var ans = newSeq[char](q)
  var qInd = 0

  for i in 0..<m:
    if ds[i] == 'L':
      if fs[i] == x[xs].c:
        x[xs].s -= 1
        x[xs].k += 1
      elif xt - xs >= 2:
        x[xs + 1].s = x[xs].s - 1
        x[xs + 1].k += x[xs].k + 1
        xs += 1
      else:
        x[xs - 1].s = x[xs].s - 1
        x[xs - 1].k = 1
        x[xs - 1].c = fs[i]
        xs -= 1
    else:
      if fs[i] == x[xt - 1].c:
        x[xt - 1].k += 1
      elif xt - xs >= 2:
        x[xt - 2].k += x[xt - 1].k + 1
        xt -= 1
      else:
        x[xt].s = x[xt - 1].s + x[xt - 1].k
        x[xt].k = 1
        x[xt].c = fs[i]
        xt += 1

    while qInd < q and qs[qInd][0] == i:
      let p = qs[qInd][1] + x[xs].s
      let ind = nibutanLb(xs, xt, it => x[it].s <= p)
      ans[qs[qInd][2]] = x[ind].c
      qInd += 1

  for i in 0..<q:
    echo ans[i]

main()


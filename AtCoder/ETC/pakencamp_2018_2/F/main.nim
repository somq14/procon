import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import future

const INF* = int(1e18 + 373)

proc readLine*(): string =
  stdin.readLine()
proc readSeq*(): seq[string] =
  readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = readLine().strip()
proc readInt1*(): int =
  readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])
proc newSeqWith*[T](n: Natural; e: T): seq[T] =
  result = newSeq[T](n)
  for i in 0..<n:
    result[i] = e
type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] =
  newSeqWith(n1, newSeq[T](n2))
type seq3*[T] = seq[seq[seq[T]]]
proc newSeq3*[T](n1, n2, n3: Natural): seq3[T] =
  newSeqWith(n1, newSeqWith(n2, newSeq[T](n3)))


#------------------------------------------------------------------------------#

proc main() =
  let k = readInt1()

  var n = 6
  var m = 0
  var g = newSeq2[int](6, 0)
  g[0] = @[]

  g[1] = @[0]
  m += 1

  g[2] = @[0]
  m += 1

  g[3] = @[1, 2]
  m += 2

  g[4] = @[1, 2]
  m += 2

  if (k and (1 shl 0)) != 0:
    g[5].add(2)
    m += 1

  for i in 1..59:
    g.add(@[])
    g.add(@[])
    g.add(@[])
    n += 3

    g[n - 3].add(n - 6)
    g[n - 3].add(n - 5)
    g[n - 2].add(n - 6)
    g[n - 2].add(n - 5)
    m += 4

    g[n - 1].add(n - 4)
    m += 1
    if (k and (1 shl i)) != 0:
      g[n - 1].add(n - 5)
      m += 1

  echo n, " ", m
  for v in 0..<n:
    for u in g[v]:
      echo u + 1, " ", v + 1

main()


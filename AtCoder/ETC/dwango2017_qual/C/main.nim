import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import logging
import future

const INF* = int(1e18 + 373)

proc readLine*(): string = stdin.readLine()

proc readSeq*(): seq[string] = readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = readLine().strip()

proc readInt1*(): int = readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#

proc main() =
  let n = readInt1()
  let a = readSeq(n).map(parseInt)

  var x = newSeq[int](5)
  for i in 0..<n:
    x[a[i]] += 1

  var ans = 0
  var tmp = 0

  tmp = x[4]
  ans += tmp
  x[4] -= tmp

  tmp = min(x[3], x[1])
  ans += tmp
  x[1] -= tmp
  x[3] -= tmp

  tmp = x[3]
  ans += tmp
  x[3] -= tmp

  tmp = x[2] div 2
  ans += tmp
  x[2] -= tmp * 2

  if x[2] == 1:
    let x1 = min(x[1], 2)
    ans += 1
    x[2] = 0
    x[1] -= x1

  ans += (x[1] + 3) div 4
  x[1] = 0

  echo ans

main()


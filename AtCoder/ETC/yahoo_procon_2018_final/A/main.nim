import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import logging
import future

const INF* = int(1e18 + 373)

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

proc readInt1*(): int = readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#
proc gcd(a, b: int): int =
  var a = a
  var b = b
  while b != 0:
    let tmp = a mod b
    a = b
    b = tmp
  return a

proc dividers(n: int): seq[int] =
  result = @[]

  for i in 1..n:
    if i * i > n:
      break
    if n mod i == 0:
      result.add(i)
      result.add(n div i)
      if i == n div i:
        discard result.pop()

  result.sort(cmp[int])


const MaxPrime = int(1e5 + 1)

proc calcPrimeTable(maxn: int): seq[bool] =
  result = newSeq[bool](maxn + 1)
  result.fill(true)
  result[0] = false
  result[1] = false

  for i in 2..maxn:
    if not result[i]:
      continue
    var j = i * 2
    while j <= maxn:
      result[j] = false
      j += i

let primeTable = calcPrimeTable(MaxPrime)

proc calcPrimeList(maxn: int): seq[int] =
  result = newSeq[int](0)
  for i in 0..maxn:
    if primeTable[i]:
      result.add(i)

let primeList = calcPrimeList(MaxPrime)

proc factorize(n: int): seq[int] =
  result = newSeq[int](0)
  var m = n
  for p in primeList:
    if m < p:
      break
    while m mod p == 0:
      result.add(p)
      m = m div p
  if m != 1:
    result.add(m)

proc uniqued[T](a: seq[T]): seq[T] =
  if a.len() == 0:
    return @[]

  var a = a.sorted(cmp[T])
  result = @[ a[0] ]
  for e in a:
    if result[result.len() - 1] == e:
      continue
    result.add(e)

#------------------------------------------------------------------------------#

proc bitwidth(v: int): int =
  var lb = -1
  var ub = 64
  # (lb, ub]
  while ub - lb > 1:
    let mid = lb + (ub - lb) div 2
    if v shr mid == 0:
      ub = mid
    else:
      lb = mid
  return ub

proc bitcount(v: int): int =
  var c: int64 = v
  c = (c and 0x5555555555555555) + ((c shr  1) and 0x5555555555555555);
  c = (c and 0x3333333333333333) + ((c shr  2) and 0x3333333333333333);
  c = (c and 0x0f0f0f0f0f0f0f0f) + ((c shr  4) and 0x0f0f0f0f0f0f0f0f);
  c = (c and 0x00ff00ff00ff00ff) + ((c shr  8) and 0x00ff00ff00ff00ff);
  c = (c and 0x0000ffff0000ffff) + ((c shr 16) and 0x0000ffff0000ffff);
  c = (c and 0x00000000ffffffff) + ((c shr 32) and 0x00000000ffffffff);
  return c.int()

proc main() =
  let (n, m) = readInt2()
  let a = readSeq(n).map(parseInt)

  var divCount = newSeq[int](m + 1)
  divCount[0] = n
  for i in 0..<n:
    for j in a[i].dividers():
      if j <= m:
        divCount[j] += 1

  for j in 1..m:
    var ans = 0
    let f = j.factorize().uniqued()
    for s in 0..<(1 shl f.len()):
      var sum = 1
      for i in 0..<f.len():
        if (s and (1 shl i)) != 0:
          sum *= f[i]

      if s.bitcount() mod 2 == 0:
        ans += divCount[sum]
      else:
        ans -= divCount[sum]

    echo ans

main()


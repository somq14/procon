import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import logging
import future

const INF* = int(1e18 + 373)

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

proc readInt1*(): int = readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#
type Op = tuple [ s0, t0, s1, t1, v: int ]

const NOP: Op = (0, 0, 1, 1, 0)

proc apply(a: var seq[int]; op: Op) =
  let n = a.len()
  for i in op.s0..op.t0:
    a[i] -= op.v

  for i in op.s1..op.t1:
    a[i] += op.v

proc score(a: seq[int]): int =
  let n = a.len()
  var ans = 0
  for i in 0..<n:
    ans += abs(a[i] - i)
  return ans

proc searchWidth(a: seq[int]; w: int): (int, Op) =
  let n = a.len()

  var s0 = -1
  var maxV0 = 0
  for i in 0..<n - w + 1:
    var allPositive = true
    for j in i..<i + w:
      if a[j] - j <= 0:
        allPositive = false
        break
    if not allPositive:
      continue

    var v = a[i]
    for j in i..<i + w:
      v = min(v, a[j] - j)

    if s0 == -1 or v >= maxV0:
      s0 = i
      maxV0 = v

  var s1 = -1
  var maxV1 = 0
  for i in 0..<n - w + 1:
    if i in (s0 - w + 1)..<s0 + w:
      continue

    var allNegative = true
    for j in i..<i + w:
      if a[j] - j >= 0:
        allNegative = false
        break
    if not allNegative:
      continue

    var v = a[i]
    for j in i..<i + w:
      v = min(v, j - a[j])

    if s1 == -1 or v >= maxV1:
      s1 = i
      maxV1 = v

  let maxV = min(maxV0, maxV1)
  let op = (s0, s0 + w - 1, s1, s1 + w - 1, maxV)
  return (2 * w * maxV, op)

proc search(a: seq[int]; k: int): Op =
  let n = a.len()

  var bestSol: (int, Op) = (0, NOP)

  var maxW = 4
  if k <= 1500:
    maxW = 9
  elif k <= 2000:
    maxW = 6
  elif k <= 2500:
    maxW = 5
  elif k <= 3000:
    maxW = 4

  for w in countdown(maxW, 1):
    let sol = searchWidth(a, w)
    if sol[0] >= bestSol[0]:
      bestSol = sol

  return bestSol[1]

proc solve(n, k: int; a: seq[int]): seq[Op] =
  var ans = newSeq[Op](k)

  var a = a
  for i in 0..<k:
    ans[i] = a.search(k)
    a.apply(ans[i])

  stderr.writeLine("after  : " & $score(a))
  return ans

proc main() =
  let (n, k) = readInt2()
  let a = readSeq(n).map(parseInt).map(it => it - 1)

  stderr.writeLine("before : " & $score(a))

  for op in solve(n, k, a):
    echo op.s0 + 1, " ", op.t0 + 1, " ", op.s1 + 1, " ", op.t1 + 1, " ", op.v

main()


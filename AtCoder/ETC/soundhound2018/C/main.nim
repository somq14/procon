import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import logging
import future

const INF* = int(1e18 + 373)

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

proc readInt1*(): int = readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#

type Pos = tuple[y, x: int]

proc dfs(n: int; g: seq2[bool]; v, t: int, visited: var seq[bool]; ans: var seq[int]) =
  visited[v] = true

  if v == t:
    ans.add(v)
    return

  for u in 0..<n:
    if g[v][u] and not visited[u]:
      dfs(n, g, u, t, visited, ans)
      if ans.len() > 0:
        ans.add(v)
        return

proc maxflow(n: int; g: seq2[int]; s, t: int): int =
  var g2 = newSeq2[bool](n, n)
  for v in 0..<n:
    for u in g[v]:
      g2[v][u] = true

  result = 0

  while true:
    var visited = newSeq[bool](n)
    var ans = newSeq[int](0)
    dfs(n, g2, s, t, visited, ans)

    if ans.len() == 0:
      break

    result += 1
    for i in 0..<(ans.len() - 1):
      g2[ans[i + 1]][ans[i]] = false
      g2[ans[i]][ans[i + 1]] = true

proc main() =
  let (h, w) = readInt2()

  var n = 0
  var s = newSeq2[int](h, w)
  for y in 0..<h:
    let c = stdin.readLine()
    s[y].fill(-1)
    for x in 0..<w:
      if c[x] == '.':
        s[y][x] = n
        n += 1

  var g = newSeq2[int](n + 2, 0)
  for y in 0..<h:
    for x in 0..<w:
      if s[y][x] < 0:
        continue

      if (y + x) mod 2 == 0:
        g[n].add(s[y][x])
      else:
        g[s[y][x]].add(n + 1)

      if (y + x) mod 2 == 0:
        for v in [(1, 0), (-1, 0), (0, 1), (0, -1)]:
          let ny = y + v[0]
          let nx = x + v[1]
          if ny notin 0..<h or nx notin 0..<w:
            continue
          if s[ny][nx] >= 0:
            g[s[y][x]].add(s[ny][nx])

  let ans = n - maxflow(n + 2, g, n, n + 1)
  echo ans

main()


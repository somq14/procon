import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import logging
import future

const INF* = int(1e18 + 373)

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

proc readInt1*(): int = readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#

proc main() =
  let (h, w) = readInt2()

  var p = newSeq2[int](h, 0)
  for i in 0..<h:
    p[i] = readSeq().map(parseInt)

  var f = newSeq2[int](h, 0)
  for i in 0..<h:
    f[i] = readSeq().map(parseInt)

  var dp = newSeq2[int](h, w)
  # fill dp[0]
  block:
    var dpRL = newSeq[int](w)
    dpRL[w - 1] = 0
    for j in countdown(w - 2, 0):
      dpRL[j] = max(-f[0][j + 1] + p[0][j + 1] + dpRL[j + 1] - f[0][j], 0)

    var sum = -f[0][0]
    for j in 0..<w:
      dp[0][j] = sum + p[0][j] + dpRL[j]
      if j + 1 < w:
        sum += p[0][j] - f[0][j + 1]

  for i in 1..<h:
    var dpLR = newSeq[int](w)
    dpLR[0] = 0
    for j in 1..<w:
      dpLR[j] = max(-f[i][j - 1] + p[i][j - 1] + dpLR[j - 1] - f[i][j], 0)

    var dpRL = newSeq[int](w)
    dpRL[w - 1] = 0
    for j in countdown(w - 2, 0):
      dpRL[j] = max(-f[i][j + 1] + p[i][j + 1] + dpRL[j + 1] - f[i][j], 0)

    var dpL = newSeq[int](w)
    dpL[0] = dp[i - 1][0] - f[i][0] + p[i][0]
    for j in 1..<w:
      dpL[j] = max(dp[i - 1][j] - f[i][j] + p[i][j] + dpLR[j], dpL[j - 1] - f[i][j] + p[i][j])

    var dpR = newSeq[int](w)
    dpR[w - 1] = dp[i - 1][w - 1] - f[i][w - 1] + p[i][w - 1]
    for j in countdown(w - 2, 0):
      dpR[j] = max(dp[i - 1][j] - f[i][j] + p[i][j] + dpRL[j], dpR[j + 1] - f[i][j] + p[i][j])

    for j in 0..<w:
      dp[i][j] = max(dpL[j] + dpRL[j], dpR[j] + dpLR[j])

  for j in 0..<w:
    echo dp[h - 1][j]

main()


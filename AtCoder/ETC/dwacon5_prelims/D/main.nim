import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import future

const INF* = int(1e18 + 373)

proc readLine*(): string =
  stdin.readLine()
proc readSeq*(): seq[string] =
  readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = readLine().strip()
proc readInt1*(): int =
  readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])
proc newSeqWith*[T](n: Natural; e: T): seq[T] =
  result = newSeq[T](n)
  for i in 0..<n:
    result[i] = e
type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] =
  newSeqWith(n1, newSeq[T](n2))
type seq3*[T] = seq[seq[seq[T]]]
proc newSeq3*[T](n1, n2, n3: Natural): seq3[T] =
  newSeqWith(n1, newSeqWith(n2, newSeq[T](n3)))

#------------------------------------------------------------------------------#
type Pred1[T] = T -> bool

# [lb, ub)
proc nibutanLb(lb, ub: int; f: Pred1[int]): int =
  var lb = lb
  var ub = ub
  while ub - lb > 1:
    let mid = (ub + lb) div 2
    if f(mid):
      lb = mid
    else:
      ub = mid
  return lb

# (lb, ub]
proc nibutanUb(lb, ub: int; f: Pred1[int]): int =
  var lb = lb
  var ub = ub
  while ub - lb > 1:
    let mid = (ub + lb) div 2
    if f(mid):
      ub = mid
    else:
      lb = mid
  return ub
#------------------------------------------------------------------------------#

proc buildCumTab2(a: seq2[int]; th: int): seq2[int] =
  let h = a.len()
  let w = a[0].len()
  result = newSeq2[int](h + h + 1, w + w + 1)
  for y in 1..h + h:
    for x in 1..w + w:
      result[y][x] = result[y - 1][x] + result[y][x - 1] - result[y - 1][x - 1]
      if a[(y - 1) mod h][(x - 1) mod w] > th:
        result[y][x] += 1

proc lookupCumTab2(c: seq2[int]; sy, sx, ty, tx: int): int =
  c[ty][tx] - c[sy][tx] - c[ty][sx] + c[sy][sx]

#------------------------------------------------------------------------------#
type Pos =
  tuple [ y, x: int ]

proc eval(n, d: int; t1, t2, t3: seq2[int]; sy, sx, b: int): bool =
  if t1.lookupCumTab2(sy, sx, sy + b + 1, sx + b + 1) != 0:
    return false
  if t2.lookupCumTab2(sy + b + 1, sx, sy + d, sx + b + 1) != 0:
    return false
  if t2.lookupCumTab2(sy, sx + b + 1, sy + b + 1, sx + d) != 0:
    return false
  if t3.lookupCumTab2(sy + b + 1, sx + b + 1, sy + d, sx + d) != 0:
    return false
  return true

proc main() =
  let (n, d) = readInt2()

  var ps = newSeq[Pos](n)
  for i in 0..<n:
    let (x, y) = readInt2()
    ps[i] = (y, x)

  var t = newSeq2[int](d, d)
  for i in 0..<n:
    t[ps[i].y mod d][ps[i].x mod d] += 1

  var m = 0
  for y in 0..<d:
    for x in 0..<d:
      m = max(m, t[y][x])

  var a = 0
  while (a + 1) * (a + 1) < m:
    a += 1

  let t1 = t.buildCumTab2((a + 1) * (a + 1))
  let t2 = t.buildCumTab2((a + 1) * a)
  let t3 = t.buildCumTab2(a * a)

  var ans = INF

  for y in 0..<d:
    for x in 0..<d:
      let opt = nibutanUb(-1, d - 1,
        b => eval(n, d, t1, t2, t3, y, x, b))
      ans = min(ans, opt)

  echo a * d + ans

main()


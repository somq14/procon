import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import future

const INF* = int(1e18 + 373)

proc readLine*(): string =
  stdin.readLine()
proc readSeq*(): seq[string] =
  readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = readLine().strip()
proc readInt1*(): int =
  readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])
proc newSeqWith*[T](n: Natural; e: T): seq[T] =
  result = newSeq[T](n)
  for i in 0..<n:
    result[i] = e
type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] =
  newSeqWith(n1, newSeq[T](n2))
type seq3*[T] = seq[seq[seq[T]]]
proc newSeq3*[T](n1, n2, n3: Natural): seq3[T] =
  newSeqWith(n1, newSeqWith(n2, newSeq[T](n3)))

#------------------------------------------------------------------------------#
proc bit(i: int): int =
  1 shl i

proc `[]`(v, i: int): bool =
  (v and (1 shl i)) != 0

proc contains(v, i: int): bool =
  (v and (1 shl i)) != 0

proc bitwidth(v: int): int =
  var lb = -1
  var ub = 64
  # (lb, ub]
  while ub - lb > 1:
    let mid = lb + (ub - lb) div 2
    if v shr mid == 0:
      ub = mid
    else:
      lb = mid
  return ub

proc bitcount(v: int): int =
  var c = v
  c = (c and 0x5555555555555555.int) + ((c shr  1) and 0x5555555555555555.int);
  c = (c and 0x3333333333333333.int) + ((c shr  2) and 0x3333333333333333.int);
  c = (c and 0x0f0f0f0f0f0f0f0f.int) + ((c shr  4) and 0x0f0f0f0f0f0f0f0f.int);
  c = (c and 0x00ff00ff00ff00ff.int) + ((c shr  8) and 0x00ff00ff00ff00ff.int);
  c = (c and 0x0000ffff0000ffff.int) + ((c shr 16) and 0x0000ffff0000ffff.int);
  c = (c and 0x00000000ffffffff.int) + ((c shr 32) and 0x00000000ffffffff.int);
  return c


proc buildCumTab(a: seq[int]): seq[int] =
  let n = a.len()
  var c = newSeq[int](n + 1)
  for i in 1..n:
    c[i] = c[i - 1] + a[i - 1]
  return c

# [s, t)
proc lookupCumTab(c: seq[int]; s, t: int): int = c[t] - c[s]


proc buildCumTab2(a: seq2[int]): seq2[int] =
  let h = a.len()
  let w = a[0].len()
  var c = newSeq2[int](h + 1, w + 1)
  for y in 1..h:
    for x in 1..w:
      c[y][x] = c[y - 1][x] + c[y][x - 1] - c[y - 1][x - 1] + a[y - 1][x - 1]
  return c

proc lookupCumTab2(c: seq2[int]; sy, sx, ty, tx: int): int =
  c[ty][tx] - c[sy][tx] - c[ty][sx] + c[sy][sx]


proc main() =
  let (n, k) = readInt2()
  let a = readSeq().map(parseInt)

  let c = a.buildCumTab()

  var s = newSeq[int](0)
  for i in 0..<n:
    for j in (i + 1)..n:
      s.add(c.lookupCumTab(i, j))
  s.sort(cmp[int])

  var ans = 0

  for i in countdown(60, 0):
    var cnt = 0
    for j in 0..<s.len():
      if s[j][i]:
        cnt += 1

    if cnt >= k:
      ans = ans or (1 shl i)
      s = s.filter(it => it[i])

  echo ans

main()


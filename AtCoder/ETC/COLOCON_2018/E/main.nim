import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import logging
import future

const INF* = int(1e18 + 373)

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

proc readInt1*(): int = readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#
type Pos = tuple [ x, y: int ]

const N = 1000

proc main() =
  let k = readInt1()

  var kk = k - 1

  var ans = newSeq[Pos]()
  var x = 0
  var y = 0
  ans.add((x, y))
  for i in 0..<N div 2:
    y += 2 * N + 2
    ans.add((x, y))
    x += 2
    ans.add((x, y))
    y -= 2 * N + 2
    ans.add((x, y))
    x += 2
    ans.add((x, y))

  var leftSide = false
  var rightSide = false
  while true:
    if kk < N + 1:
      rightSide = true
      break
    y += 2
    ans.add((x, y))
    x -= 2 * N + 2
    ans.add((x, y))
    kk -= N

    if kk < N + 1:
      leftSide = true
      break
    y += 2
    ans.add((x, y))
    x += 2 * N + 2
    ans.add((x, y))
    kk -= N

  y += 2
  ans.add((x, y))

  if rightSide:
    x -= 1 + 4 * ((kk - 1) div 2)
    ans.add((x, y))
    kk -= 2 * ((kk - 1) div 2)

  if leftSide:
    x += 1 + 4 * ((kk - 1) div 2)
    ans.add((x, y))
    kk -= 2 * ((kk - 1) div 2)

  y = 2 * N + 4
  ans.add((x, y))

  x = 2 * N + 4
  ans.add((x, y))

  if kk == 1:
    y = -2
    ans.add((x, y))
  else:
    y = -4
    ans.add((x, y))
    x += 2
    ans.add((x, y))
    y += 2
    ans.add((x, y))

  x = -2
  ans.add((x, y))
  y += 2
  ans.add((x, y))

  echo ans.len()
  for i in 0..<ans.len():
    echo ans[i].x, " ", ans[i].y

main()


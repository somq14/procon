import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import logging
import future

const INF* = int(1e18 + 373)

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

proc readInt1*(): int = readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#

proc main() =
  let (n, x) = readInt2()
  var t = @[-x] & readSeq(n).map(parseInt)

  var lb = newSeq[int](n + 1)
  for i in 0..n:
    lb[i] = t.lowerBound(t[i] + x)

  var dpPrev: ref seq[int] = new seq[int]
  var dp: ref seq[int] = new seq[int]
  dp[] = newSeq[int](n + 1)
  dpPrev[] = newSeq[int](n + 1)

  for k in 1..n:
    for i in countdown(n - 1, 0):
      let j = lb[i]
      var opt = -1
      if j in 0..n and t[j] >= t[i] + x:
        opt = max(opt, dpPrev[j] + x)

      if j - 1 in 0..n and t[j - 1] > t[i]:
        opt = max(opt, dpPrev[j - 1] + (t[j - 1] - t[i]))

      dp[i] = opt
    echo dp[0]
    swap(dp, dpPrev)


main()


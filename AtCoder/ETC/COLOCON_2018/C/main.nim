import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import logging
import future

const INF* = int(1e18 + 373)

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

proc readInt1*(): int = readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#
proc gcd(a, b: int): int =
  var a = a
  var b = b

  while b != 0:
    let tmp = a mod b
    a = b
    b = tmp

  return a

proc contains(bitpat, ind: int): bool =
  (bitpat and (1 shl ind)) != 0

proc main() =
  let (a, b) = readInt2()
  let n = b - a + 1

  if n == 1:
    echo 2
    return

  var x = newSeq[int](n)
  for i in 0..<n:
    x[i] = a + i

  var g = newSeq[int](n)
  for u in 0..<n:
    for v in 0..<n:
      if gcd(x[u], x[v]) != 1:
        g[u] = g[u] or (1 shl v)

  let n1 = n div 2
  let n2 = n - n1

  var dp = newSeq[int](1 shl n1)
  dp[0] = 1
  for s in 1..<(1 shl n1):
    var v = -1
    for i in 0..<n1:
      if i in s:
        v = i
        break

    let s1 = s and not (1 shl v)
    let s2 = s1 and not g[v]
    dp[s] = dp[s1] + dp[s2]

  var sum = 0
  for s in 0..<(1 shl n2):
    var conflict = false

    for v in n1..<n:
      if v notin (s shl n1):
        continue
      if ((s shl n1) and g[v]) != (1 shl v):
        conflict = true
        break

    if conflict:
      continue

    var t = 0
    for v in n1..<n:
      if v notin (s shl n1):
        continue
      t = t or g[v]

    sum += dp[(not t) and ((1 shl n1) - 1)]

  echo sum

main()


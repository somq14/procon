import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import future

const INF* = int(1e18 + 373)

proc readLine*(): string =
  stdin.readLine()
proc readSeq*(): seq[string] =
  readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = readLine().strip()
proc readInt1*(): int =
  readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])
proc newSeqWith*[T](n: Natural; e: T): seq[T] =
  result = newSeq[T](n)
  for i in 0..<n:
    result[i] = e
type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] =
  newSeqWith(n1, newSeq[T](n2))
type seq3*[T] = seq[seq[seq[T]]]
proc newSeq3*[T](n1, n2, n3: Natural): seq3[T] =
  newSeqWith(n1, newSeqWith(n2, newSeq[T](n3)))

#------------------------------------------------------------------------------#

proc solve(a, b, c: int): bool =
  let all = 100 * a + 10 * b + c
  if all mod 2 != 0:
    return false

  let x = all div 2
  var a = a
  var b = b
  var c = c

  if c < x mod 10:
    return false
  c -= x mod 10
  b += c div 10

  if b < (x div 10) mod 10:
    return false
  b -= (x div 10) mod 10
  a += b div 10

  return a >= (x div 100)

proc main() =
  let q = readInt1()
  for i in 0..<q:
    let (a, b, c) = readInt3()
    echo if solve(a, b, c): "Yes" else: "No"

main()


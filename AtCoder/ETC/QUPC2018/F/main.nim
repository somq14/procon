import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import logging
import future

const INF* = int(1e18 + 373)

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

proc readInt1*(): int = readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#
proc bitcount(v: int): int =
  var c = v
  c = (c and 0x5555555555555555.int) + ((c shr  1) and 0x5555555555555555.int);
  c = (c and 0x3333333333333333.int) + ((c shr  2) and 0x3333333333333333.int);
  c = (c and 0x0f0f0f0f0f0f0f0f.int) + ((c shr  4) and 0x0f0f0f0f0f0f0f0f.int);
  c = (c and 0x00ff00ff00ff00ff.int) + ((c shr  8) and 0x00ff00ff00ff00ff.int);
  c = (c and 0x0000ffff0000ffff.int) + ((c shr 16) and 0x0000ffff0000ffff.int);
  c = (c and 0x00000000ffffffff.int) + ((c shr 32) and 0x00000000ffffffff.int);
  return c

proc main() =
  let (n, k) = readInt2()
  let a = readSeq().map(parseInt)

  var fact = newSeq[int](10)
  fact[0] = 1
  for i in 1..9:
    fact[i] = i * fact[i - 1]

  var dp3 = newSeq2[int](1 shl n, 6 + 1)
  for s in 0..<(1 shl n):
    dp3[s][0] = 1

  for s in 0..<(1 shl n):
    let ss = s.bitcount()
    for i in 1..6:
      if ss < 3 * i:
        dp3[s][i] = 0
        continue
      var ans = 0
      for t1 in 0..<n:
        if (s and (1 shl t1)) == 0:
          continue
        for t2 in (t1 + 1)..<n:
          if (s and (1 shl t2)) == 0:
            continue
          for t3 in (t2 + 1)..<n:
            if (s and (1 shl t3)) == 0:
              continue
            if a[t1] + a[t2] + a[t3] > 3 * k:
              continue
            let t = (1 shl t1) or (1 shl t2) or (1 shl t3)
            ans += dp3[s xor t][i - 1]
      dp3[s][i] = ans

  var dp2 = newSeq2[int](1 shl n, 9 + 1)
  for s in 0..<(1 shl n):
    for i in 0..9:
      let ss = s.bitcount()

      if ss < 2 * i:
        dp2[s][i] = 0
        continue

      if i == 0:
        var ans = 0
        for k in 0..6:
          ans += dp3[s][k] div fact[k]
        dp2[s][i] = ans
        continue

      var ans = 0
      for t1 in 0..<n:
        if (s and (1 shl t1)) == 0:
          continue
        for t2 in (t1 + 1)..<n:
          if (s and (1 shl t2)) == 0:
            continue
          if a[t1] + a[t2] > 2 * k:
            continue
          let t = (1 shl t1) or (1 shl t2)
          ans += dp2[s xor t][i - 1]
      dp2[s][i] = ans

  var ans = 0
  for i in 0..9:
    ans += dp2[(1 shl n) - 1][i] div fact[i]
  echo ans

main()


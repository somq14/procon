#include <algorithm>
#include <cmath>
#include <complex>
#include <functional>
#include <iomanip>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <string>
#include <utility>
#include <vector>

using namespace std;

using ll = long long int;
#define int ll

const int MOD = 1e9 + 7;
const int INF = 1e15 + 373;

#define rep(i, n) for (int i = 0; i < (int)(n); ++i)

template <typename T>
using vector2 = vector<vector<T>>;
template <typename T>
vector2<T> init_vector2(size_t n0, size_t n1, T e = T()) {
    return vector2<T>(n0, vector<T>(n1, e));
}

template <typename T>
using vector3 = vector<vector<vector<T>>>;
template <typename T>
vector3<T> init_vector3(size_t n0, size_t n1, size_t n2, T e = T()) {
    return vector3<T>(n0, vector2<T>(n1, vector<T>(n2, e)));
}

template <typename T>
ostream& operator<<(ostream& os, const vector<T>& v) {
    os << "[";
    for (T e : v) {
        os << e << ", ";
    }
    os << "]";
    return os;
}

void dump(const vector<string>& s) {
    rep(i, s.size()) { cout << s[i] << endl; }
}

void dump(const vector2<int>& s) {
    for (int i = 0; i < (int)s.size(); i++) {
        for (int j = 0; j < (int)s[i].size(); j++) {
            printf("%3lld", s[i][j]);
        }
        printf("\n");
    }
    printf("\n");
}

struct pos_t {
    int y, x;
};

ostream& operator<<(ostream& os, const pos_t& p) {
    os << "(" << p.y << "," << p.x << ")";
    return os;
}

const int dy[] = {-1, 1, 0, 0};
const int dx[] = {0, 0, -1, 1};

signed main() {
    int h, w, xxx;
    cin >> h >> w >> xxx;

    vector<string> s(h);
    rep(i, h) { cin >> s[i]; }

    // dump(s);

    pos_t ss;
    pos_t tt;
    vector<pos_t> is;

    rep(y, h) {
        rep(x, w) {
            if (s[y][x] == 'S') {
                ss.y = y;
                ss.x = x;
                s[y][x] = '.';
            }
            if (s[y][x] == 'G') {
                tt.y = y;
                tt.x = x;
                s[y][x] = '.';
            }
            if (s[y][x] == '@') {
                pos_t p;
                p.y = y;
                p.x = x;
                is.push_back(p);
                s[y][x] = '.';
            }
        }
    }

    // cout << ss << endl;
    // cout << tt << endl;
    // cout << is << endl;
    // dump(s);

    {
        vector2<int> d = init_vector2(h, w, -1LL);
        queue<pos_t> que;
        for (pos_t p : is) {
            que.push(p);
            d[p.y][p.x] = 0;
        }

        while (que.size() > 0) {
            const pos_t p = que.front();
            que.pop();

            rep(k, 4) {
                pos_t np;
                np.y = p.y + dy[k];
                np.x = p.x + dx[k];
                if (s[np.y][np.x] == '#' || d[np.y][np.x] >= 0) {
                    continue;
                }
                d[np.y][np.x] = d[p.y][p.x] + 1;
                que.push(np);
            }
        }

        rep(y, h) {
            rep(x, w) {
                if (d[y][x] < 0) {
                    continue;
                }
                if (d[y][x] <= xxx) {
                    s[y][x] = '#';
                }
            }
        }
        //dump(d);
        //dump(s);
    }

    {
        vector2<int> d = init_vector2(h, w, -1LL);
        queue<pos_t> que;
        que.push(ss);
        d[ss.y][ss.x] = 0;
        while (que.size() > 0) {
            const pos_t p = que.front();
            que.pop();

            rep(k, 4) {
                pos_t np;
                np.y = p.y + dy[k];
                np.x = p.x + dx[k];
                if (s[np.y][np.x] == '#' || d[np.y][np.x] >= 0) {
                    continue;
                }
                d[np.y][np.x] = d[p.y][p.x] + 1;
                que.push(np);
            }
        }

        //dump(d);

        cout << d[tt.y][tt.x] << endl;
    }


    return 0;
}

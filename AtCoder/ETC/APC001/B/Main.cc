#include <algorithm>
#include <cmath>
#include <complex>
#include <functional>
#include <iomanip>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <string>
#include <utility>
#include <vector>

using namespace std;

using ll = long long int;
#define int ll

const int MOD = 1e9 + 7;
const int INF = 1e15 + 373;

#define rep(i, n) for (int i = 0; i < (n); ++i)

template <typename T>
using vector2 = vector<vector<T>>;
template <typename T>
vector2<T> init_vector2(size_t n0, size_t n1, T e = T()) {
    return vector2<T>(n0, vector<T>(n1, e));
}

template <typename T>
using vector3 = vector<vector<vector<T>>>;
template <typename T>
vector3<T> init_vector3(size_t n0, size_t n1, size_t n2, T e = T()) {
    return vector3<T>(n0, vector2<T>(n1, vector<T>(n2, e)));
}

bool solve(int n, vector<int>& a, vector<int>& b) {
    vector<int> c(n);
    rep(i, n) { c[i] = b[i] - a[i]; }

    int neg_sum = 0;
    int pos_sum = 0;
    rep(i, n) {
        if (c[i] > 0) {
            pos_sum += c[i] / 2;
        }
        if (c[i] < 0) {
            neg_sum += -c[i];
        }
    }

    return pos_sum >= neg_sum;
}

signed main() {
    int n;
    cin >> n;
    vector<int> a(n);
    rep(i, n) { cin >> a[i]; }
    vector<int> b(n);
    rep(i, n) { cin >> b[i]; }
    cout << (solve(n, a, b) ? "Yes" : "No") << endl;
    return 0;
}

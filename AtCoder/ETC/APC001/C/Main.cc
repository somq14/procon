#include <algorithm>
#include <cmath>
#include <complex>
#include <functional>
#include <iomanip>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <string>
#include <utility>
#include <vector>

using namespace std;

using ll = long long int;
#define int ll

const int MOD = 1e9 + 7;
const int INF = 1e15 + 373;

#define rep(i, n) for (int i = 0; i < (n); ++i)

template <typename T>
using vector2 = vector<vector<T>>;
template <typename T>
vector2<T> init_vector2(size_t n0, size_t n1, T e = T()) {
    return vector2<T>(n0, vector<T>(n1, e));
}

template <typename T>
using vector3 = vector<vector<vector<T>>>;
template <typename T>
vector3<T> init_vector3(size_t n0, size_t n1, size_t n2, T e = T()) {
    return vector3<T>(n0, vector2<T>(n1, vector<T>(n2, e)));
}

int query(int i) {
    string s;

    cout << i << endl;
    cin >> s;

    if (s == "Male") {
        return 0;
    }
    if (s == "Female") {
        return 1;
    }
    return -1;
}

signed main() {
    int n;
    cin >> n;

    int s = query(0);
    int t = (s + 0) % 2;

    // [lb, ub)
    int lb = 0;
    int ub = n;
    while (ub - lb > 1) {
        int mid = lb + (ub - lb) / 2;
        if ((mid + query(mid)) % 2 == t) {
            lb = mid;
        } else {
            ub = mid;
        }
    }

    return 0;
}

#include <algorithm>
#include <cmath>
#include <complex>
#include <cstdio>
#include <functional>
#include <iomanip>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <string>
#include <utility>
#include <vector>

using namespace std;

using ll = long long int;
#define int ll

const int MOD = 1e9 + 7;
const int INF = 1e15 + 373;

#define rep(i, n) for (int i = 0; i < (n); ++i)

template <typename T>
using vector2 = vector<vector<T>>;
template <typename T>
vector2<T> init_vector2(size_t n0, size_t n1, T e = T()) {
    return vector2<T>(n0, vector<T>(n1, e));
}

template <typename T>
using vector3 = vector<vector<vector<T>>>;
template <typename T>
vector3<T> init_vector3(size_t n0, size_t n1, size_t n2, T e = T()) {
    return vector3<T>(n0, vector2<T>(n1, vector<T>(n2, e)));
}

template <typename T>
void dump(const vector<T>& v) {
    cout << "[";
    for (T e : v) {
        cout << e << ", ";
    }
    cout << "]" << endl;
}

signed main() {
    int n;
    cin >> n;

    vector2<int> g(n);
    rep(i, n - 1) {
        int a, b;
        cin >> a >> b;
        g[a].push_back(b);
        g[b].push_back(a);
    }

    rep(i, n) {
        if (g[i].size() != 2) {
            continue;
        }
        int a = g[i][0];
        int b = g[i][1];

        g[i].resize(0);
        for (size_t j = 0; j < g[a].size(); j++) {
            if (g[a][j] == i) {
                g[a][j] = b;
                break;
            }
        }
        for (size_t j = 0; j < g[b].size(); j++) {
            if (g[b][j] == i) {
                g[b][j] = a;
                break;
            }
        }
    }

    int ans = 0;
    rep(i, n) {
        if (g[i].size() <= 1) {
            continue;
        }
        int cnt = g[i].size() - 1;
        for (size_t j = 0; j < g[i].size(); j++) {
            if (g[g[i][j]].size() >= 3) {
                cnt--;
            }
        }
        ans += max(cnt, 0LL);
    }

    cout << max(ans, 1LL) << endl;

    return 0;
}

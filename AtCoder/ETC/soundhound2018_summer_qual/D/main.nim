import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import logging
import future

const INF* = int(1e18 + 373)

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

proc readInt1*(): int = readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#
# FIXME: Use object
type PriorityQueue[T] = tuple[a: seq[T], n: Natural, cmp: proc(a, b: T): bool]

proc initPriorityQueue[T](cmp: proc(a, b: T): bool): PriorityQueue[T] =
    (newSeq[T](1), Natural(0), cmp)

proc len[T](this: PriorityQueue[T]): Natural = this.n

proc enqueue[T](this: var PriorityQueue[T], e: T) =
    this.n += 1
    this.a.add(e)
    var i = this.a.len() - 1
    while i > 1 and this.cmp(this.a[i], this.a[i div 2]):
        swap(this.a[i], this.a[i div 2])
        i = i div 2

proc dequeue[T](this: var PriorityQueue[T]): T =
    this.n -= 1
    result = this.a[1]
    this.a[1] = this.a.pop()
    var i = 1
    while i < this.a.len():
        var j = i
        if i * 2 < this.a.len() and this.cmp(this.a[i * 2], this.a[j]):
            j = i * 2
        if i * 2 + 1 < this.a.len() and this.cmp(this.a[i * 2 + 1], this.a[j]):
            j = i * 2 + 1
        if i == j:
            break
        swap(this.a[i], this.a[j])
        i = j

# FIXME: throw Exception when this is empty
proc front[T](this: PriorityQueue[T]): T = this.a[1]

#------------------------------------------------------------------------------#
type Edge = tuple[ frm, to, c: int ]

proc dijkstra(n: int; g: seq2[Edge]; s: int): seq[int] =
  var d = newSeq[int](n)
  d.fill(INF)
  d[s] = 0

  type P = tuple[c, v: int]
  var q = initPriorityQueue[P]((p1: P, p2: P) => p1.c < p2.c)
  q.enqueue((0, s))

  while q.len() > 0:
    let (c, v) = q.dequeue()
    if c > d[v]:
      continue
    for e in g[v]:
      let alt = d[v] + e.c
      if alt < d[e.to]:
        d[e.to] = alt
        q.enqueue((alt, e.to))

  return d

proc main() =
  var (n, m, s, t) = readInt4()
  s -= 1
  t -= 1

  var gA = newSeq2[Edge](n, 0)
  var gB = newSeq2[Edge](n, 0)
  for i in 0..<m:
    var (u, v, a, b) = readInt4()
    u -= 1
    v -= 1
    gA[u].add((u, v, a))
    gA[v].add((v, u, a))
    gB[u].add((u, v, b))
    gB[v].add((v, u, b))

  let dA = dijkstra(n, gA, s)
  let dB = dijkstra(n, gB, t)

  var ans = newSeq[int](n)
  ans[n - 1] = dA[n - 1] + dB[n - 1]
  for v in countdown(n - 2, 0):
    ans[v] = min(ans[v + 1], dA[v] + dB[v])

  for v in 0..<n:
    echo(1000000000000000 - ans[v])


main()


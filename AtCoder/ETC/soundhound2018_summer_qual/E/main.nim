import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import logging
import future

const INF* = int(1e18 + 373)

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

proc readInt1*(): int = readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#

type Edge = tuple [ frm, to, c: int ]

var oddFlag = false
var oddValue = 0

proc dfs2(n: int; g: seq2[Edge]; v: int; visited: var seq[bool]; assign: var seq[int]): bool =
  visited[v] = true

  for e in g[v]:
    let u = e.to
    if visited[u]:
      if assign[v] + assign[u] != e.c:
        return false
      continue

    assign[u] = e.c - assign[v]
    if assign[u] <= 0:
      return false
    if not dfs2(n, g, u, visited, assign):
      return false

  return true

proc dfs(n: int; g: seq2[Edge]; v, d: int; visited: var seq[bool]; phase: var seq[int]; assign: var seq[int]): bool =
  visited[v] = true
  phase[v] = d mod 2

  for e in g[v]:
    let u = e.to
    if visited[u]:
      if phase[v] == phase[u]:

        var tmp = -1
        if phase[v] == 0:
          tmp = e.c - assign[v] - assign[u]
          if tmp <= 0 or tmp mod 2 != 0:
            return false
        else:
          tmp = assign[v] + assign[u] - e.c
          if tmp <= 0 or tmp mod 2 != 0:
            return false

        oddFlag = true
        oddValue = tmp div 2
      else:
        if assign[v] + assign[u] != e.c:
          return false
      continue

    assign[u] = e.c - assign[v]
    if not dfs(n, g, u, d + 1, visited, phase, assign):
      return false

  return true

proc main() =
  let (n, m) = readInt2()

  var g = newSeq2[Edge](n, 0)
  for i in 0..<m:
    var (u, v, s) = readInt3()
    u -= 1
    v -= 1
    g[u].add((u, v, s))
    g[v].add((v, u, s))

  var visited = newSeq[bool](n)
  var phase = newSeq[int](n)
  var assign = newSeq[int](n)

  oddFlag = false
  if not dfs(n, g, 0, 0, visited, phase, assign):
    echo 0
    return

  if oddFlag:
    var visited2 = newSeq[bool](n)
    var assign2 = newSeq[int](n)
    assign2[0] = oddValue
    if dfs2(n, g, 0, visited2, assign2):
      echo 1
    else:
      echo 0
    return

  var evenMin = assign[0]
  var oddMin = assign[g[0][0].to]
  for v in 0..<n:
    if phase[v] == 0:
      evenMin = min(evenMin, assign[v])
    else:
      oddMin = min(oddMin, assign[v])

  let atLeast = 1 - evenMin # >= 1
  let atMost = max(oddMin - 1, 0)
  if atLeast > atMost:
    echo 0
    return

  echo (atMost - atLeast + 1)

main()


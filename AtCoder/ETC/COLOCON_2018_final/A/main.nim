import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import logging
import future

const INF* = int(1e18 + 373)

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

proc readInt1*(): int = readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#

proc main() =
  let n = readInt1()
  let s = stdin.readLine()

  if s.count('A') == s.len():
    let m = n * s.len()
    let ans = m * (m + 1) div 2
    echo ans
    return

  var leftA = 0
  for i in 0..<s.len():
    if s[i] == 'B':
      break
    leftA += 1

  var rightA = 0
  for i in countdown(s.len() - 1, 0):
    if s[i] == 'B':
      break
    rightA += 1

  var midDamage = 0
  var i = leftA
  while i < s.len() - rightA:
    if s[i] == 'B':
      i += 1
      continue

    var countA = 0
    var j = i
    while s[j] == 'A':
      countA += 1
      j += 1
    i = j
    midDamage += countA * (countA + 1) div 2

  var ans = 0
  ans += midDamage * n
  ans += leftA * (leftA + 1) div 2
  ans += rightA * (rightA + 1) div 2
  ans += (n - 1) * ((leftA + rightA) * (leftA + rightA + 1) div 2)
  echo ans

main()


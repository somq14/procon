GC_disable()
import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import logging
import future

const INF* = int(1e18 + 373)

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

proc readInt1*(): int = readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#
type Node = ref object
  leaf: bool
  val: string
  op: char
  args: seq[Node]

proc `$`(this: Node): string =
  if this.leaf:
    "[" & this.val & "]"
  else:
    "[" & $this.op & " " & $this.args & "]"

proc newNode(val: string): Node =
  result.new()
  result.leaf = true
  result.val = val
  result.op = '!'
  result.args = nil

proc newNode(op: char; args: seq[Node]): Node =
  result.new()
  result.leaf = false
  result.val = "Invalid"
  result.op = op
  result.args = args

proc parseNumber(s: string; i: var int): Node =
  var j = i
  while s[j] in '0'..'9':
    j += 1

  let num = s[i..<j]

  i = j
  return newNode(num)

proc parse(s: string; i: var int): Node =
  case s[i]:
  of '+', '-', '*', '/':
    let op = s[i]
    i += 1

    assert s[i] == '('
    i += 1

    var args = newSeq[Node](0)
    while s[i] != ')':
      args.add(parse(s, i))

      if s[i] == ',':
        i += 1

    i += 1

    result = newNode(op, args)
  of '0'..'9':
    result = parseNumber(s, i)
  else:
    assert false

proc encode(this: Node) =
  if this.leaf:
    stdout.write(this.val)
    return

  stdout.write('(')
  for i in 0..<this.args.len():
    if i != 0:
      stdout.write(this.op)
    this.args[i].encode()
  stdout.write(')')


proc main() =
  let s = stdin.readLine()

  var i = 0
  s.parse(i).encode()
  echo ""

main()


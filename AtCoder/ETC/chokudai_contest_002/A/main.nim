import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import future

const INF* = int(1e18 + 373)

proc readLine*(): string =
  stdin.readLine()
proc readSeq*(): seq[string] =
  readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = readLine().strip()
proc readInt1*(): int =
  readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])
proc newSeqWith*[T](n: Natural; e: T): seq[T] =
  result = newSeq[T](n)
  for i in 0..<n:
    result[i] = e
type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] =
  newSeqWith(n1, newSeq[T](n2))
type seq3*[T] = seq[seq[seq[T]]]
proc newSeq3*[T](n1, n2, n3: Natural): seq3[T] =
  newSeqWith(n1, newSeqWith(n2, newSeq[T](n3)))

#------------------------------------------------------------------------------#
proc gcd(a, b: int): int =
  var a = a
  var b = b
  while b != 0:
    let tmp = a mod b
    a = b
    b = tmp
  return a

proc lcm(a, b: int): int =
  a * b div gcd(a, b)

proc dividers(n: int): seq[int] =
  result = @[]

  for i in 1..n:
    if i * i > n:
      break
    if n mod i == 0:
      result.add(i)
      result.add(n div i)
      if i == n div i:
        discard result.pop()

  result.sort(cmp[int])


const MaxPrime = int(1e5 + 1)

proc calcPrimeTable(maxn: int): seq[bool] =
  result = newSeq[bool](maxn + 1)
  result.fill(true)
  result[0] = false
  result[1] = false

  for i in 2..maxn:
    if not result[i]:
      continue
    var j = i * 2
    while j <= maxn:
      result[j] = false
      j += i

let primeTable = calcPrimeTable(MaxPrime)

proc calcPrimeList(maxn: int): seq[int] =
  result = newSeq[int](0)
  for i in 0..maxn:
    if primeTable[i]:
      result.add(i)

let primeList = calcPrimeList(MaxPrime)

proc factorize1(n: int): seq[int] =
  result = newSeq[int](0)
  var m = n
  for p in primeList:
    if m < p:
      break
    while m mod p == 0:
      result.add(p)
      m = m div p
  if m != 1:
    result.add(m)

proc factorize2(n: int): seq[(int, int)] =
  result = newSeq[(int, int)](0)
  var m = n
  for p in primeList:
    if m < p:
      break
    var cnt = 0
    while m mod p == 0:
      cnt += 1
      m = m div p
    if cnt != 0:
      result.add((p, cnt))

  if m != 1:
    result.add((m, 1))

#------------------------------------------------------------------------------#
proc factorize3(n: int): float =
  result = 1.0
  var m = n
  for p in primeList:
    if m < p:
      break
    var cnt = 0
    while m mod p == 0:
      cnt += 1
      m = m div p
    if cnt != 0:
      result *= (cnt + 1).float

  if m != 1:
    result *= 2

proc test() =
  var ans = newSeq[(float, int)](0)

  for i in 0..<1000:
    echo i
    for j in 0..<1000:
      let n = 1000 * i + j
      ans.add((n.factorize3(), n))

  ans.sort(cmp[(float, int)])
  for i in ans.len()-1000..<ans.len():
    echo ans[i]


const N = 100
proc main() =
  let base = 997920 # = 2^5 + 3^4 + 5^1 + 7^1 + 11^1

  var primeInd = 5

  var ans = newSeq[int](N)
  for i in 0..<N:
    var v = base
    while v * primeList[primeInd] <= 10^9:
      v *= primeList[primeInd]
      primeInd += 1
    while v * 17 <= 10^9:
      v *= 17
    while v * 13 <= 10^9:
      v *= 13
    while v * 11 <= 10^9:
      v *= 11
    while v * 7 <= 10^9:
      v *= 7
    while v * 5 <= 10^9:
      v *= 5
    while v * 3 <= 10^9:
      v *= 3
    while v * 2 <= 10^9:
      v *= 2
    ans[i] = v

  var score = initSet[int]()
  for i in 0..<N:
    for d in ans[i].dividers():
      score.incl(d)
    echo ans[i]
  #echo score.len()

main()


#include <iostream>
#include <string>

using std::cin;
using std::cout;
using std::endl;

using std::string;

int main(){
    string s;
    string t = "AKIHABARA";

    cin >> s;

    bool ans = true;

    size_t it = 0;
    for(char c : t){
        if(s[it] == c){
            it++;
            continue;
        }
        if(c == 'A'){
            continue;
        }
        ans = false;
        break;
    }

    if(it != s.length()){
        ans = false;
    }

    cout << (ans ? "YES" : "NO") << endl;

    return 0;
}

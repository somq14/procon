import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import logging
import future

const INF* = int(1e18 + 373)

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

proc readInt1*(): int = readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#
type Person = tuple [ h, p: int ]

proc `<`(p1, p2: Person): bool =
  (p1.h + p1.p) < (p2.h + p2.p)

proc main() =
  let n = readInt1()
  var ps = newSeq[Person](n)
  for i in 0..<n:
    ps[i] = readInt2()
  ps.sort(cmp[Person])

  var dp: ref seq[int]
  dp.new()
  dp[] = newSeq[int](n + 1)

  dp[].fill(INF)
  dp[0] = 0

  var dpNext: ref seq[int]
  dpNext.new()
  dpNext[] = newSeq[int](n + 1)

  for i in 1..n:
    for j in 0..n:
      var opt = dp[j]
      if j > 0 and dp[j - 1] <= ps[i - 1].h:
       opt = min(opt, dp[j - 1] + ps[i - 1].p)
      dpNext[j] = opt
    swap(dpNext, dp)

  var ans = -1
  for j in countdown(n, 0):
    if dp[j] != INF:
      ans = j
      break
  echo ans

main()


#include <algorithm>
#include <iostream>
#include <vector>

using std::cin;
using std::cout;
using std::endl;

using std::vector;
using std::min;
using std::max;

int eval(const vector<int>& t) {
    int ans = 24;
    for (size_t i = 0; i < t.size(); i++) {
        for (size_t j = i + 1; j < t.size(); j++) {
            int diff = max(t[i], t[j]) - min(t[i], t[j]);
            ans = min(ans, min(diff, 24 - diff));
        }
    }
    return ans;
}

int backtrack(const vector<int>& choice, vector<int>& time, size_t depth,
              int best) {
    if (depth == choice.size()) {
        return max(best, eval(time));
    }
    time.push_back(choice[depth]);
    best = max(best, backtrack(choice, time, depth + 1, best));
    time.pop_back();

    time.push_back(24 - choice[depth]);
    best = max(best, backtrack(choice, time, depth + 1, best));
    time.pop_back();

    return best;
}

int solve(const vector<int>& d) {
    if (d[0] >= 2) {
        return 0;
    }
    if (d[12] >= 2) {
        return 0;
    }
    for (int i = 1; i < 12; i++) {
        if (d[i] >= 3) {
            return 0;
        }
    }

    vector<int> time;
    time.push_back(0);  // takahashi kun
    if (d[12] == 1) {
        time.push_back(12);
    }
    for (int i = 1; i < 12; i++) {
        if (d[i] == 2) {
            time.push_back(i);
            time.push_back(24 - i);
        }
    }

    vector<int> choice;
    for (int i = 1; i < 12; i++) {
        if (d[i] == 1) {
            choice.push_back(i);
        }
    }

    return backtrack(choice, time, 0, 0);
}

int main() {
    int n;
    cin >> n;
    vector<int> d(12 + 1, 0);

    d[0] += 1;
    for (int i = 0; i < n; i++) {
        int tmp;
        cin >> tmp;
        d[tmp] += 1;
    }

    cout << solve(d) << endl;

    return 0;
}

import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import logging
import future

const INF* = int(1e18 + 373)

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

proc readInt1*(): int = readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#

proc main() =
  let (w, h) = readInt2()
  let p = readSeq(w).map(parseInt)
  let q = readSeq(h).map(parseInt)

  var a = newSeq[(int, char)](0)
  for i in 0..<w:
    a.add((p[i], 'w'))
  for i in 0..<h:
    a.add((q[i], 'h'))
  a.sort(cmp[(int, char)])

  var hh = h
  var ww = w
  var edgeCount = 0
  var targetEdgeCount = (h + 1) * (w + 1) - 1
  var ans = 0
  for i in 0..<a.len():
    if edgeCount >= targetEdgeCount:
      break

    let (v, c) = a[i]
    case c:
    of 'w':
      let m = min(hh + 1, targetEdgeCount - edgeCount)
      ans += m * v
      edgeCount += m
      ww -= 1
    of 'h':
      let m = min(ww + 1, targetEdgeCount - edgeCount)
      ans += m * v
      edgeCount += m
      hh -= 1
    else:
      discard

  echo ans

main()


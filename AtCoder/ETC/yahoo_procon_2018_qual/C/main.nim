import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import logging
import future

const INF* = int(1e18 + 373)

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

proc readInt1*(): int = readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

proc buildCumTab(a: seq[int]): seq[int] =
  let n = a.len()
  var c = newSeq[int](n + 1)
  for i in 1..n:
    c[i] = c[i - 1] + a[i - 1]
  return c

# [s, t)
proc lookupCumTab(c: seq[int]; s, t: int): int = c[t] - c[s]


proc buildCumTab2(a: seq2[int]): seq2[int] =
  let h = a.len()
  let w = a[0].len()
  var c = newSeq2[int](h + 1, w + 1)
  for y in 1..h:
    for x in 1..w:
      c[y][x] = c[y - 1][x] + c[y][x - 1] - c[y - 1][x - 1] + a[y - 1][x - 1]
  return c

proc lookupCumTab2(c: seq2[int]; sy, sx, ty, tx: int): int =
  c[ty][tx] - c[sy][tx] - c[ty][sx] + c[sy][sx]
#------------------------------------------------------------------------------#

proc bitcount(n: int): int =
  result = 0
  var i = 1
  while i <= n:
    if (n and i) != 0:
      result += 1
    i = i shl 1

proc main() =
  let n = readInt1()
  let x = readSeq().map(parseInt)
  let c = readSeq().map(parseInt)
  let v = readSeq().map(parseInt)
  let xcum = x.buildCumTab()

  var setV = newSeq[int](1 shl n)
  var setC = newSeq[int](1 shl n)
  setV[0] = 0
  setC[0] = 0
  for s in 1..<(1 shl n):
    var item = -1
    for i in 0..<n:
      if (s and (1 shl i)) != 0:
        item = i
        break
    setV[s] = setV[s and not (1 shl item)] + v[item]
    setC[s] = setC[s and not (1 shl item)] + c[item]

  var knapsackDp = newSeq2[int](n + 1, 1 shl n)
  for i in 0..n:
    let m = xcum[i]
    for s in 0..<(1 shl n):
      if m >= setC[s]:
        knapsackDp[i][s] = setV[s]
        continue

      var opt = 0
      for j in 0..<n:
        if (s and (1 shl j)) != 0:
          opt = max(opt, knapsackDp[i][s and not (1 shl j)])
      knapsackDp[i][s] = opt

  var dp = newSeq[int](1 shl n)
  dp[0] = 0
  for s in 1..<(1 shl n):
    var opt = INF
    for i in 0..<n:
      if (s and (1 shl i)) != 0:
        opt = min(opt, dp[s and not (1 shl i)])
    dp[s] = max(opt, knapsackDp[n - s.bitcount()][s])

  echo dp[(1 shl n) - 1]

main()


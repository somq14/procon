import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import future

const INF* = int(1e18 + 373)

proc readLine*(): string =
  stdin.readLine()
proc readSeq*(): seq[string] =
  readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = readLine().strip()
proc readInt1*(): int =
  readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])
proc newSeqWith*[T](n: Natural; e: T): seq[T] =
  result = newSeq[T](n)
  for i in 0..<n:
    result[i] = e
type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] =
  newSeqWith(n1, newSeq[T](n2))
type seq3*[T] = seq[seq[seq[T]]]
proc newSeq3*[T](n1, n2, n3: Natural): seq3[T] =
  newSeqWith(n1, newSeqWith(n2, newSeq[T](n3)))

#------------------------------------------------------------------------------#
const MOD = int(10^9+7)

type Op1[T] = proc(a: T, b: T): T
type Op2[T] = proc(a: T, b: T): T

proc repeatedSquares[T](x: T, n: Natural, e: T, f: Op2[T]): T =
    result = e
    var xx = f(x, e)
    var i = n
    while i > 0:
        if (i and 1) != 0:
            result = f(result, xx)
        xx = f(xx, xx)
        i = i shr 1

proc addM(a, b: Natural): Natural = (a + b) mod MOD
proc subM(a, b: Natural): Natural = (a - b + MOD) mod MOD
proc mulM(a, b: Natural): Natural = a * b mod MOD
proc powM(a, b: Natural): Natural = repeatedSquares[Natural](a, b, 1, `mulM`)
proc invM(a: Natural): Natural = a.powM(MOD - 2)
proc divM(a, b: Natural): Natural = a.mulM(invM(b))

var memoFactM: seq[Natural] = nil
var memoFactInvM: seq[Natural] = nil

proc buildFactTable(n: Natural) =
  memoFactM = newSeq[Natural](n + 1)
  memoFactM[0] = 1
  for i in 1..n:
    memoFactM[i] = memoFactM[i - 1].mulM(i)

  memoFactInvM = newSeq[Natural](n + 1)
  memoFactInvM[n] = invM(memoFactM[n])
  for i in countdown(n - 1, 0):
    memoFactInvM[i] = memoFactInvM[i + 1].mulM(i + 1)

buildFactTable(10^6)

proc factM(n: Natural): Natural = memoFactM[n]

proc factInvM(n: Natural): Natural = memoFactInvM[n]

proc combM(n, r: Natural): Natural =
  if r > n:
    return 0
  if r > n div 2:
    return combM(n, n - r)

  result = factM(n).mulM(factInvM(n - r)).mulM(factInvM(r))

proc multCombM(n, r: Natural): Natural = combM(n + r - 1, r - 1)

#------------------------------------------------------------------------------#
#------------------------------------------------------------------------------#
const M = 2048

proc subr(n, a, b: int): int =
  if n > a + b:
    return 0

  var res = 0
  for i in 0..n:
    if i > a:
      continue
    if n - i > b:
      continue
    res = res.addM(combM(n, i))
  return res;

proc solve(n, k, x, y: int; z: seq[int]; a: seq2[int]): int =
  var zcnt = newSeq[int](M)
  for i in 0..<n:
    zcnt[z[i]] += 1

  var ans = 0
  for i in 0..<M:
    if i >= (i xor x xor y):
      continue

    var needs = newSeq[int](M)
    for j in 0..<k:
      let p = min(a[0][j] xor i xor x, a[0][j] xor i xor y)
      needs[p] += 1

    var comb = 1
    for j in 0..<M:
      if j >= (j xor x xor y):
        continue

      comb = comb.mulM(subr(needs[j], zcnt[j], zcnt[j xor x xor y]))
    ans = ans.addM(comb)
  return ans

proc main() =
  let (n, k, x, y) = readInt4()
  let z = readSeq().map(parseInt).sorted(cmp[int])
  var a = newSeq2[int](k, 0)
  for i in 0..<k:
    a[i] = readSeq().map(parseInt)

  for i in 0..<k:
    for j in 0..<k:
      if a[i][j] != (a[0][i] xor a[0][j] xor x) and
        a[i][j] != (a[0][i] xor a[0][j] xor y):
        echo "0"
        return

  echo solve(n, k, x, y, z, a)

main()

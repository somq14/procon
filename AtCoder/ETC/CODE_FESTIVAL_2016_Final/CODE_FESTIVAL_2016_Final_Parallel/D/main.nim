import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import logging
import future

const INF* = int(1e18 + 373)

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

proc readInt1*(): int = readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#

proc main() =
  let (n, m) = readInt2()
  let x = readSeq().map(parseInt)

  var hist = newSeq[int](10^5 + 1)
  for i in 0..<n:
    hist[x[i]] += 1

  # (double, single)
  var group = newSeq[(int, int)](m)
  for i in 0..<hist.len():
    group[i mod m][0] += hist[i] div 2
    group[i mod m][1] += hist[i] mod 2

  var ans = 0

  for i in 0..<m:
    let j = (m - i) mod m
    if i > j:
      break
    if i == j:
      let v = group[i][0] * 2 + group[i][1]
      ans += v div 2
      continue

    var gi = group[i]
    var gj = group[j]

    let tmp1 = min(gi[1], gj[1])
    gi[1] -= tmp1
    gj[1] -= tmp1
    ans += tmp1

    if gj[1] == 0:
      swap(gi, gj)
    assert gi[1] == 0

    let tmp2 = min(gi[0], gj[1] div 2)
    gi[0] -= tmp2
    gj[1] -= 2 * tmp2
    ans += 2 * tmp2

    ans += gi[0]
    ans += gj[0]

  echo ans


main()


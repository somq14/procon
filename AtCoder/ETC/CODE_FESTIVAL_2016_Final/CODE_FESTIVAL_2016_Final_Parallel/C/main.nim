import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import logging
import future

const INF* = int(1e18 + 373)

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

proc readInt1*(): int = readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#
type UnionFindTree = tuple[ p, h, w: seq[int] ]

proc initUnionFindTree(n: int): UnionFindTree =
    var p = newSeq[int](n)
    p.fill(-1)
    var h = newSeq[int](n)
    h.fill(0)
    var w = newSeq[int](n)
    w.fill(1)
    return (p, h, w)

proc find(this: var UnionFindTree, v: int): int =
    if this.p[v] == -1:
        return v
    this.p[v] = find(this, this.p[v])
    return this.p[v]

proc same(this: var UnionFindTree, u, v: int): bool =
    this.find(u) == this.find(v)

proc union(this: var UnionFindTree, u, v: int) =
    let uRoot = this.find(u)
    let vRoot = this.find(v)
    if uRoot == vRoot:
        return

    if this.h[uRoot] > this.h[vRoot]:
        this.p[vRoot] = uRoot
        this.w[uRoot] += this.w[vRoot]
    else:
        this.p[uRoot] = vRoot
        this.w[vRoot] += this.w[uRoot]
        if this.h[uRoot] == this.h[vRoot]:
            this.h[vRoot] += 1

#------------------------------------------------------------------------------#

proc main() =
  let (n, m) = readInt2()

  var uft = initUnionFindTree(n + m)
  for i in 0..<n:
    let ls = readSeq().map(parseInt)
    for j in 1..<ls.len():
      uft.union(i, n + ls[j] - 1)

  var ans = true
  for i in 0..<n - 1:
    if not uft.same(i, i + 1):
      ans = false
      break
  
  echo if ans: "YES" else: "NO"


main()


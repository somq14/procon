import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import logging
import future

const INF* = int(1e18 + 373)

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

proc readInt1*(): int = readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#

proc main() =
  let n = readInt1()
  let t = readSeq().map(parseInt)
  let a = readSeq().map(parseInt)

  var fixed1 = newSeq[bool](n)
  fixed1[0] = true
  for i in 1..<n:
    if t[i - 1] != t[i]:
      fixed1[i] = true

  var fixed2 = newSeq[bool](n)
  fixed2[n - 1] = true
  for i in countdown(n - 2, 0):
    if a[i + 1] != a[i]:
      fixed2[i] = true

  var ans = 1
  for i in 0..<n:
    if fixed1[i] and fixed2[i]:
      if a[i] != t[i]:
        echo 0
        return
    elif not fixed1[i] and fixed2[i]:
      if t[i] < a[i]:
        echo 0
        return
    elif fixed1[i] and not fixed2[i]:
      if t[i] > a[i]:
        echo 0
    else:
      ans = ans * min(t[i], a[i]) mod (1000000007)

  echo ans

main()


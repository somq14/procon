import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import future

const INF* = int(1e18 + 373)

proc readLine*(): string =
  stdin.readLine()
proc readSeq*(): seq[string] =
  readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = readLine().strip()
proc readInt1*(): int =
  readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])
proc newSeqWith*[T](n: Natural; e: T): seq[T] =
  result = newSeq[T](n)
  for i in 0..<n:
    result[i] = e
type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] =
  newSeqWith(n1, newSeq[T](n2))
type seq3*[T] = seq[seq[seq[T]]]
proc newSeq3*[T](n1, n2, n3: Natural): seq3[T] =
  newSeqWith(n1, newSeqWith(n2, newSeq[T](n3)))

#------------------------------------------------------------------------------#
proc keta(n: int): int =
  result = 0
  var m = n
  while m != 0:
    result += 1
    m = m div 10

iterator keta(w: int): int =
  let lb = 10 ^ (w - 1)
  let ub = 10 ^ w
  for i in lb..<ub:
    yield i

const M = 20181224

proc main() =
  var ans = newSeq[int]()

  var p3 = newSeq[seq[int]]()
  for a1 in 1..8:
    for b1 in 1..8:
      for c in 1..8:
        if a1 + b1 + c <= 8:
          p3.add(@[a1, b1, c])

  for p in p3:
    for a1 in keta(p[0]):
      for b1 in keta(p[1]):
        if pow(a1.float(), b1.float()) > M.float():
          break
        let ab1 = a1 ^ b1
        for c in keta(p[2]):
          if ab1 * c > M:
            break
          if (a1 * (10 ^ p[1]) + b1) * (10 ^ p[2]) + c == ab1 * c:
            #echo a1, " ", b1, " ", c
            ans.add(ab1 * c)

  var p5 = newSeq[seq[int]]()
  for a1 in 1..8:
    for b1 in 1..8:
      for a2 in 1..8:
        for b2 in 1..8:
          for c in 1..8:
            if a1 + b1 + a2 + b2 + c <= 8:
              p5.add(@[a1, b1, a2, b2, c])

  for p in p5:
    for a1 in keta(p[0]):
      for b1 in keta(p[1]):
        if pow(a1.float(), b1.float()) > M.float():
          break
        let ab1 = a1 ^ b1
        for a2 in keta(p[2]):
          for b2 in keta(p[3]):
            if pow(a2.float(), b2.float()) > M.float():
              break
            let ab2 = a2 ^ b2
            for c in keta(p[4]):
              if ab1 * ab2 * c > M:
                break
              if (((a1 * (10 ^ p[1]) + b1) * (10 ^ p[2]) + a2) * (10 ^ p[3]) + b2) * (10 ^ p[4]) + c == ab1 * ab2 * c:
                #echo a1, " ", b1, " ", a2, " ", b2, " ", c
                ans.add(ab1 * ab2 * c)

  var p7 = newSeq[seq[int]]()
  for a1 in 1..8:
    for b1 in 1..8:
      for a2 in 1..8:
        for b2 in 1..8:
          for a3 in 1..8:
            for b3 in 1..8:
              for c in 1..8:
                if a1 + b1 + a2 + b2 + a3 + b3 + c <= 8:
                  p7.add(@[a1, b1, a2, b2, a3, b3, c])

  for p in p7:
    for a1 in keta(p[0]):
      for b1 in keta(p[1]):
        if pow(a1.float(), b1.float()) > M.float():
          break
        let ab1 = a1 ^ b1
        for a2 in keta(p[2]):
          for b2 in keta(p[3]):
            if pow(a2.float(), b2.float()) > M.float():
              break
            let ab2 = a2 ^ b2
            for a3 in keta(p[4]):
              for b3 in keta(p[5]):
                if pow(a3.float(), b3.float()) > M.float():
                  break
                let ab3 = a3 ^ b3
                for c in keta(p[6]):
                  if ab1 * ab2 * ab3 * c > M:
                    break
                  if (((((a1 * (10 ^ p[1]) + b1) * (10 ^ p[2]) + a2) * (10 ^ p[3]) + b2) * (10 ^ p[4]) + a3) * (10 ^ p[5]) + b3) * (10 ^ p[6]) + c  == ab1 * ab2 * ab3 * c:
                    #echo a1, " ", b1, " ", a2, " ", b2, " ", a3, " ", b3, " ", c
                    ans.add(ab1 * ab2 * ab3 * c)
  ans.sort(cmp[int])
  for i in 0..<ans.len():
    echo ans[i]

main()


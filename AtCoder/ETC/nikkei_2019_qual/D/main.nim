import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import future

const INF* = int(1e18 + 373)

proc readLine*(): string =
  stdin.readLine()
proc readSeq*(): seq[string] =
  readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = readLine().strip()
proc readInt1*(): int =
  readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])
proc newSeqWith*[T](n: Natural; e: T): seq[T] =
  result = newSeq[T](n)
  for i in 0..<n:
    result[i] = e
type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] =
  newSeqWith(n1, newSeq[T](n2))
type seq3*[T] = seq[seq[seq[T]]]
proc newSeq3*[T](n1, n2, n3: Natural): seq3[T] =
  newSeqWith(n1, newSeqWith(n2, newSeq[T](n3)))

proc dfs(n: int; g: seq2[int]; v: int; dep: var seq[int]): int =
  if dep[v] != -1:
    return dep[v]

  var res = 0
  for u in g[v]:
    res = max(res, dfs(n, g, u, dep) + 1)
  dep[v] = res
  return res

proc main() =
  let (n, m) = readInt2()
  var g = newSeq2[int](n, 0)
  for i in 0..<n - 1 + m:
    let (a, b) = readInt2()
    g[a - 1].add(b - 1)

  var isRoot = newSeq[bool](n)
  isRoot.fill(true)
  for v in 0..<n:
    for u in g[v]:
      isRoot[u] = false

  var root = -1
  for v in 0..<n:
    if isRoot[v]:
      root = v
      break

  var g2 = newSeq2[int](n, 0)
  for v in 0..<n:
    for u in g[v]:
      g2[u].add(v)

  var dep = newSeq[int](n)
  dep.fill(-1)
  dep[root] = 0
  for v in 0..<n:
    dep[v] = dfs(n, g2, v, dep)

  var par = newSeq[int](n)
  for v in 0..<n:
    if g2[v].len() == 0:
      par[v] = -1
      continue

    var k = g2[v][0]
    for u in g2[v]:
      if dep[u] > dep[k]:
        k = u
    par[v] = k

  for v in 0..<n:
    echo par[v] + 1

main()


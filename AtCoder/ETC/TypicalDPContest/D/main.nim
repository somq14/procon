import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import logging
import future

const INF* = int(1e18 + 373)

when not defined(release):
  addHandler(newFileLogger(stderr, lvlAll, "[ $levelname ] "))

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

proc readInt1*(): int = readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])
type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

type seq3*[T] = seq[seq[seq[T]]]
proc newSeq3*[T](n1, n2, n3: Natural): seq3[T] = newSeqWith(n1, newSeqWith(n2, newSeq[T](n3)))

#------------------------------------------------------------------------------#

proc main() =
  let (n, d) = readInt2()

  var dd = d

  var cnt2 = 0
  while dd mod 2 == 0:
    dd = dd div 2
    cnt2 += 1

  var cnt3 = 0
  while dd mod 3 == 0:
    dd = dd div 3
    cnt3 += 1

  var cnt5 = 0
  while dd mod 5 == 0:
    dd = dd div 5
    cnt5 += 1

  if dd != 1:
    echo 0.0
    return

  debug "cnt2 = ", cnt2
  debug "cnt3 = ", cnt3
  debug "cnt5 = ", cnt5

  var dp = newSeq3[float](2 * n + 1, n + 1, n + 1)
  dp[0][0][0] = 1.0

  for i in 0..<n:
    for i2 in countdown(2 * n, 0):
      for i3 in countdown(n, 0):
        for i5 in countdown(n, 0):
          var opt = 0.0
          opt += dp[i2][i3][i5]
          if i2 >= 1:
            opt += dp[i2 - 1][i3][i5]
          if i3 >= 1:
            opt += dp[i2][i3 - 1][i5]
          if i2 >= 2:
            opt += dp[i2 - 2][i3][i5]
          if i5 >= 1:
            opt += dp[i2][i3][i5 - 1]
          if i2 >= 1 and i3 >= 1:
            opt += dp[i2 - 1][i3 - 1][i5]
          dp[i2][i3][i5] = opt / 6.0

  var ans = 0.0
  for i2 in 0..2 * n:
    for i3 in 0..n:
      for i5 in 0..n:
        if i2 >= cnt2 and i3 >= cnt3 and i5 >= cnt5:
          ans += dp[i2][i3][i5]
  echo ans

main()


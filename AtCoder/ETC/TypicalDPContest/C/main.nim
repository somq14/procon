import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import logging
import future

const INF* = int(1e18 + 373)

when not defined(release):
  addHandler(newFileLogger(stderr, lvlAll, "[ $levelname ] "))

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

proc readInt1*(): int = readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#
proc prob(r1, r2: int): float =
  1.0 / (1.0 + pow(10.0, (r2 - r1).float() / 400.0))

proc main() =
  let k = readInt1()
  let n = 1 shl k

  var r = newSeq[int](n)
  for i in 0..<n:
    r[i] = readInt1()

  var dp = newSeq2[float](k + 1, n)
  for i in 0..<n:
    dp[0][i] = 1.0

  for h in 1..k:
    let siz = (1 shl (h - 1))
    for i in 0..<n:
      # fill dp[h][i]
      let myInd = i div siz
      let rivalInd = myInd + (if myInd mod 2 == 0: +1 else: -1)
      let rivalS = rivalInd * siz
      let rivalT = rivalS + siz

      var opt = 0.0
      for j in rivalS..<rivalT:
        opt += dp[h - 1][i] * dp[h - 1][j] * prob(r[i], r[j])
      dp[h][i] = opt

  for j in 0..<n:
    echo dp[k][j]

main()


import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import logging
import future

const INF* = int(1e18 + 373)

when not defined(release):
  addHandler(newFileLogger(stderr, lvlAll, "[ $levelname ] "))

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

proc readInt1*(): int = readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#
const MOD = 10^9 + 7

proc main() =
  let (n, k)= readInt2()

  var dp = newSeq[int](n + 1 + 1)
  dp[0] = 1
  dp[1] = 0
  dp[2] = 1

  # dp[i] = sum_of dp[0] ,..., dp[i - 1]
  var cum = newSeq[int](dp.len() + 1)
  cum[0] = 0
  cum[1] = dp[0]
  cum[2] = dp[0] + dp[1]
  cum[3] = dp[0] + dp[1] + dp[2]

  for i in 3..(n + 1):
    # var sum = 0
    # for j in max(0, i - k)..<i:
    #   sum = (sum + dp[j]) mod MOD
    let sum = (cum[i] - cum[max(0, i - k)] + MOD) mod MOD
    dp[i] = sum
    cum[i + 1] = (cum[i] + dp[i]) mod MOD

  var ans = (dp[n + 1] - dp[n] + MOD) mod MOD
  echo ans

main()


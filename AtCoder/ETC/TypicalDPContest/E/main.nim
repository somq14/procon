import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import logging
import future

const INF* = int(1e18 + 373)

when not defined(release):
  addHandler(newConsoleLogger(lvlAll, "[ $levelname ] "))

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

proc readInt1*(): int = readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#
const MOD = 10^9 + 7

proc main() =
  let d = readInt1()
  let n = stdin.readLine().map(it => $it).map(parseInt).reversed()
  let k = n.len()

  var dp = newSeq2[int](k + 1, d)
  dp[0][0] = 1
  for i in 1..k:
    for j in 0..<d:
      var sum = 0
      for x in 0..9:
        sum = (sum + dp[i - 1][(j - x + 10 * d) mod d]) mod MOD
      dp[i][j] = sum

  var ans = 0
  var target = 0
  for i in countdown(k - 1, 0):
    for j in 0..<n[i]:
      ans = (ans + dp[i][(target - j + 10 * d) mod d]) mod MOD
    target = (target - n[i] + 10 * d) mod d

  if target == n[0]:
    ans = (ans + 1) mod MOD

  ans = (ans - 1 + MOD) mod MOD

  echo ans


main()


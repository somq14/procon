import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import logging
import future

const INF* = int(1e18 + 373)

when not defined(release):
  addHandler(newFileLogger(stderr, lvlAll, "[ $levelname ] "))

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

proc readInt1*(): int = readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#

proc buildCumTab(a: seq[int]): seq[int] =
  let n = a.len()
  var c = newSeq[int](n + 1)
  for i in 1..n:
    c[i] = c[i - 1] + a[i - 1]
  return c

proc main() =
  let (an, bn) = readInt2()
  let a = readSeq().map(parseInt).reversed()
  let b = readSeq().map(parseInt).reversed()

  let aSum = a.buildCumTab()
  let bSum = b.buildCumTab()

  var dp = newSeq2[int](an + 1, bn + 1)
  for i in 0..an:
    for j in 0..bn:
      var opt = 0
      if i > 0:
        opt = max(opt, a[i - 1] + (aSum[i - 1] + bSum[j]) - dp[i - 1][j])
      if j > 0:
        opt = max(opt, b[j - 1] + (aSum[i] + bSum[j - 1]) - dp[i][j - 1])
      dp[i][j] = opt

  debug dp
  echo dp[an][bn]

main()


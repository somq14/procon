import strutils
import sequtils
import algorithm
import math
import queues
import tables
import logging
import future
import sets

const INF* = int(1e18 + 373)

when not defined(release):
  addHandler(newFileLogger(stderr, lvlAll, "[ $levelname ] "))

proc readInt1*(): int = stdin.readLine().strip().parseInt()
proc readInt2*(): (int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1])
proc readInt3*(): (int, int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1], v[2])

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#
proc compress[T](a: seq[T]): Table[T, int] =
  result = initTable[T, int]()
  for e in a:
    if e notin result:
      result[e] = result.len()

#------------------------------------------------------------------------------#

proc main() =
  let (h, w) = readInt2()
  let (n, m) = readInt2()

  var a = newSeq[string](n)
  for i in 0..<n:
    a[i] = stdin.readLine()

  # cordinate compression
  var ySet = initSet[int]()
  var xSet = initSet[int]()
  for i in 0..<n:
    for j in 0..<m:
      if a[i][j] == '.':
        continue
      ySet.incl(i)
      ySet.incl(i + (h - n) + 1)

      xSet.incl(j)
      xSet.incl(j + (w - m) + 1)

  let ys = toSeq(ySet.items()).sorted(cmp[int])
  let xs = toSeq(xSet.items()).sorted(cmp[int])
  let yTab = ys.compress()
  let xTab = xs.compress()

  # 2D imos
  var c = newSeq2[int](yTab.len(), xTab.len())
  for i in 0..<n:
    for j in 0..<m:
      if a[i][j] == '.':
        continue
      let sy = yTab[i]
      let sx = xTab[j]
      let ty = yTab[i + (h - n) + 1]
      let tx = xTab[j + (w - m) + 1]
      c[sy][sx] += 1
      c[ty][sx] -= 1
      c[sy][tx] -= 1
      c[ty][tx] += 1

  for i in 0..<yTab.len():
    for j in 1..<xTab.len():
      c[i][j] += c[i][j - 1]

  for j in 0..<xTab.len():
    for i in 1..<yTab.len():
      c[i][j] += c[i - 1][j]

  # count
  var ans = 0
  for i in 0..<yTab.len():
    for j in 0..<xTab.len():
      if c[i][j] <= 0:
        continue
      let sy = ys[i]
      let sx = xs[j]
      let ty = ys[i + 1]
      let tx = xs[j + 1]
      ans += (ty - sy) * (tx - sx)

  echo ans



main()


import strutils
import sequtils
import algorithm
import math
import queues
import tables
import logging
import future

const INF* = int(1e18 + 373)

when not defined(release):
  addHandler(newFileLogger(stderr, lvlAll, "[ $levelname ] "))

proc readInt1*(): int = stdin.readLine().strip().parseInt()
proc readInt2*(): (int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1])
proc readInt3*(): (int, int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1], v[2])

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#

proc main() =
  let (n, d) = readInt2()
  let x = readSeq().map(parseInt)
  debug x

  var ans = 0
  for j in 0..<n:
    let i = x.lowerBound(x[j] - d)
    let k = x.lowerBound((x[j] + d) + 1)
    # debug "j = ", j, ", i = ", i, ", k = ", k
    # debug "jsize = ", (j - i)
    # debug "ksize = ", (k - j - 1)
    # debug ""
    ans += (j - i) * (k - j - 1)
  debug ans

  for i in 0..<n:
    let k = x.lowerBound((x[i] + d) + 1)
    let m = k - i
    if m < 3:
      continue
    debug m
    debug ((m - 1) * (m - 2) div 2) 
    debug ""
    ans -= (m - 1) * (m - 2) div 2
  echo ans

main()


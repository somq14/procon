import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import logging
import future

const INF* = int(1e18 + 373)

proc readLine*(): string =
  stdin.readLine()
proc readSeq*(): seq[string] =
  readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = readLine().strip()
proc readInt1*(): int =
  readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])
type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] =
  newSeqWith(n1, newSeq[T](n2))
type seq3*[T] = seq[seq[seq[T]]]
proc newSeq3*[T](n1, n2, n3: Natural): seq3[T] =
  newSeqWith(n1, newSeqWith(n2, newSeq[T](n3)))

#------------------------------------------------------------------------------#
proc dfs(n: int; x: seq[int]; g: var seq[int]; v: int): int =
  if g[v] >= 0:
    return g[v]

  if x[v] == 0:
    g[v] = v
    return g[v]

  if x[v] == -1:
    g[v] = 0
    return g[v]

  assert x[v] >= 1

  if v + x[v] >= n:
    g[v] = n - 1
    return g[v]

  g[v] = dfs(n, x, g, v + x[v])
  return g[v]

proc solve(n: int; x: seq[int]): float =
  var g = newSeq[int](n)
  g.fill(-1)
  for v in 0..<n:
    discard dfs(n, x, g, v)

  # [lb, ub)
  var lb = 0.0
  var ub = 1e7
  for i in 0..<100:
    let mid = (lb + ub) / 2

    var dp = newSeq[float](n)
    dp[0] = mid
    dp[n - 1] = 0

    for i in countdown(n - 2, 0):
      var p = 0.0
      for j in 1..6:
        if i + j >= n:
          p += (1.0 / 6.0) * 0
        else:
          p += (1.0 / 6.0) * dp[g[i + j]]
      dp[i] = p + 1

    if dp[0] >= mid:
      lb = mid
    else:
      ub = mid

  return lb

proc main() =
  while true:
    let n = readInt1()
    if n == 0:
      break
    let x = readSeq().map(parseInt)
    echo solve(n, x)

main()


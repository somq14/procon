import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import logging
import future

const INF* = int(1e18 + 373)

proc readLine*(): string =
  stdin.readLine()
proc readSeq*(): seq[string] =
  readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = readLine().strip()
proc readInt1*(): int =
  readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])
type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] =
  newSeqWith(n1, newSeq[T](n2))
type seq3*[T] = seq[seq[seq[T]]]
proc newSeq3*[T](n1, n2, n3: Natural): seq3[T] =
  newSeqWith(n1, newSeqWith(n2, newSeq[T](n3)))

#------------------------------------------------------------------------------#
type Pos =
  tuple [ y, x: int ]

var H = INF
var W = INF

proc setArea(h, w: int) =
  H = h
  W = w

proc valid(p: Pos): bool =
  p.y in 0..<H and p.x in 0..<W

iterator neighbors(p: Pos): Pos =
  yield (p.y + 1, p.x)
  yield (p.y - 1, p.x)
  yield (p.y, p.x + 1)
  yield (p.y, p.x - 1)

proc `[]`[T](a: seq2[T]; p: Pos): T =
  a[p.y][p.x]

proc `[]=`[T](a: var seq2[T]; p: Pos; v: T) =
  a[p.y][p.x] = v

#------------------------------------------------------------------------------#

proc solve(h, w, p: int; a: seq2[int]; ps: seq[Pos]): int =
  setArea(h, w)

  var wet = newSeq2[bool](h, w)
  var q = initQueue[Pos]()

  for i in 0..<p:
    wet[ps[i]] = true
    q.enqueue(ps[i])

  while q.len() > 0:
    let cur = q.dequeue()
    for nxt in cur.neighbors():
      if not nxt.valid():
        continue
      if a[nxt] < a[cur] and not wet[nxt]:
        wet[nxt] = true
        q.enqueue(nxt)

  var ans = 0
  for y in 0..<h:
    for x in 0..<w:
      if wet[y][x]:
        ans += 1
  return ans

proc main() =
  while true:
    let (w, h, p) = readInt3()
    if w == 0 and h == 0 and p == 0:
      break

    var a = newSeq2[int](h, 0)
    for i in 0..<h:
      a[i] = readSeq().map(parseInt)

    var ps = newSeq[Pos](p)
    for i in 0..<p:
      let (x, y) = readInt2()
      ps[i] = (y, x)

    echo solve(h, w, p, a, ps)

main()


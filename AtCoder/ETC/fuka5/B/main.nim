import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import logging
import future

const INF* = int(1e18 + 373)

proc readLine*(): string =
  stdin.readLine()
proc readSeq*(): seq[string] =
  readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = readLine().strip()
proc readInt1*(): int =
  readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])
type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] =
  newSeqWith(n1, newSeq[T](n2))
type seq3*[T] = seq[seq[seq[T]]]
proc newSeq3*[T](n1, n2, n3: Natural): seq3[T] =
  newSeqWith(n1, newSeqWith(n2, newSeq[T](n3)))

#------------------------------------------------------------------------------#
proc isUruu(y: int): bool =
  y mod 400 == 0 or (y mod 100 != 0 and y mod 4 == 0)

proc lastDayOfMonth(y, m: int): int =
  case m:
  of 1 :
    return 31
  of 2 :
    return if y.isUruu(): 29 else: 28
  of 3 :
    return 31
  of 4 :
    return 30
  of 5 :
    return 31
  of 6 :
    return 30
  of 7 :
    return 31
  of 8 :
    return 31
  of 9 :
    return 30
  of 10:
    return 31
  of 11:
    return 30
  of 12:
    return 31
  else:
    assert false

proc nextDay(y, m, d: var int) =
  let dd = lastDayOfMonth(y, m)
  if d != dd:
    d += 1
    return

  if m == 12:
    y += 1
    m = 1
    d = 1
  else:
    m += 1
    d = 1

proc nextSec(y, m, d, hh, mm, ss: var int) =
  ss += 1
  if ss >= 60:
    ss = 0
    mm += 1

  if mm >= 60:
    mm = 0
    hh += 1

  if hh >= 24:
    hh = 0
    nextDay(y, m, d)

proc main() =
  while true:
    let line = readLine()
    if line == "0":
      break
    let input = line.split(" ")
    let input0 = input[0].split("/")
    let input1 = input[1].split(":")

    var y = input0[0].parseInt()
    var m = input0[1].parseInt()
    var d = input0[2].parseInt()
    var hh = input1[0].parseInt()
    var mm = input1[1].parseInt()
    var ss = input1[2].parseInt()

    var time = (1 shl  readLine().len()) - 1
    var days = time div (24 * 60 * 60)
    var secs = time mod (24 * 60 * 60)

    for i in 0..<days:
      nextDay(y, m, d)

    for i in 0..<secs:
      nextSec(y, m, d, hh, mm, ss)

    echo intToStr(y) & "/" & intToStr(m, 2) & "/" & intToStr(d, 2) & " " & intToStr(hh, 2) & ":" & intToStr(mm, 2) & ":" & intToStr(ss, 2)

main()


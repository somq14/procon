import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import logging
import future

const INF* = int(1e18 + 373)

proc readLine*(): string =
  stdin.readLine()
proc readSeq*(): seq[string] =
  readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = readLine().strip()
proc readInt1*(): int =
  readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])
type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] =
  newSeqWith(n1, newSeq[T](n2))
type seq3*[T] = seq[seq[seq[T]]]
proc newSeq3*[T](n1, n2, n3: Natural): seq3[T] =
  newSeqWith(n1, newSeqWith(n2, newSeq[T](n3)))

#------------------------------------------------------------------------------#
proc bit(i: int): int =
  1 shl i

proc `[]`(v, i: int): bool =
  (v and (1 shl i)) != 0

proc contains(v, i: int): bool =
  (v and (1 shl i)) != 0

proc bitwidth(v: int): int =
  var lb = -1
  var ub = 64
  # (lb, ub]
  while ub - lb > 1:
    let mid = lb + (ub - lb) div 2
    if v shr mid == 0:
      ub = mid
    else:
      lb = mid
  return ub

proc bitcount(v: int): int =
  var c = v
  c = (c and 0x5555555555555555.int) + ((c shr  1) and 0x5555555555555555.int);
  c = (c and 0x3333333333333333.int) + ((c shr  2) and 0x3333333333333333.int);
  c = (c and 0x0f0f0f0f0f0f0f0f.int) + ((c shr  4) and 0x0f0f0f0f0f0f0f0f.int);
  c = (c and 0x00ff00ff00ff00ff.int) + ((c shr  8) and 0x00ff00ff00ff00ff.int);
  c = (c and 0x0000ffff0000ffff.int) + ((c shr 16) and 0x0000ffff0000ffff.int);
  c = (c and 0x00000000ffffffff.int) + ((c shr 32) and 0x00000000ffffffff.int);
  return c
#------------------------------------------------------------------------------#

proc solve(n, a, b: int; x: seq[int]): int =
  var wgt = newSeq[int](1 shl n)
  wgt[0] = 0
  for s in 1..<(1 shl n):
    for i in 0..<n:
      if i in s:
        wgt[s] = wgt[s xor bit(i)] + x[i]
        break

  var dp = newSeq[int](1 shl n)
  dp[0] = 1
  for s in 1..<(1 shl n):
    if s.bitcount() == 1:
      dp[s] = 1
      continue

    var comb = 0
    for i in 0..<n:
      if i notin s:
        continue

      let t = s xor bit(i)
      var s1 = t
      while true:
        let s2 = t xor s1
        if abs(wgt[s1] - wgt[s2]) in a..b:
          comb += dp[s1] * dp[s2]

        if s1 == 0:
          break

        s1 = (s1 - 1) and t

    dp[s] = comb

  return dp[(1 shl n) - 1]

proc main() =
  while true:
    let (n, a, b) = readInt3()
    if n == 0 and a == 0 and b == 0:
      break
    let x = readSeq().map(parseInt).sorted(cmp[int])
    echo solve(n, a, b, x)

main()


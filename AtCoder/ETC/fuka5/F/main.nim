import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import logging
import future

const INF* = int(1e18 + 373)

proc readLine*(): string =
  stdin.readLine()
proc readSeq*(): seq[string] =
  readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = readLine().strip()
proc readInt1*(): int =
  readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])
type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] =
  newSeqWith(n1, newSeq[T](n2))
type seq3*[T] = seq[seq[seq[T]]]
proc newSeq3*[T](n1, n2, n3: Natural): seq3[T] =
  newSeqWith(n1, newSeqWith(n2, newSeq[T](n3)))

#------------------------------------------------------------------------------#
type Pos =
  tuple [ y, x: float ]

proc solve(n, w: int; ps: seq[Pos]): int =
  let w = w.float()
  var rs = newSeq[(float, int)](0)
  var zero = 0
  for i in 0..<n:
    let y = ps[i].y
    let x = ps[i].x
    if y <= w and x <= w:
      zero += 1
      continue
    let th1 = arctan2(y, x)

    let d = sqrt(y * y + x * x)
    if w >= y:
      rs.add((0.0, -1))
    else:
      rs.add((th1 - arcsin(w / d), -1))

    if w >= x:
      rs.add((PI / 2, +1))
    else:
      rs.add((th1 + arcsin(w / d), +1))

  rs.sort(cmp[(float, int)])

  var ans = 0
  var sum = 0
  for r in rs:
    if r[1] == -1:
      sum += 1
    else:
      sum -= 1
    ans = max(ans, sum)

  return ans + zero

proc main() =
  while true:
    let (n, w) = readInt2()
    if n == 0 and w == 0:
      break

    var ps = newSeq[Pos](n)
    for i in 0..<n:
      let (x, y) = readInt2()
      ps[i] = (y.float(), x.float())

    let blank = readLine()

    echo solve(n, w, ps)

main()


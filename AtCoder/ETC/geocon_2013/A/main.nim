import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import future

const INF* = int(1e18 + 373)

proc readLine*(): string =
  stdin.readLine()
proc readSeq*(): seq[string] =
  readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = readLine().strip()
proc readInt1*(): int =
  readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])
proc newSeqWith*[T](n: Natural; e: T): seq[T] =
  result = newSeq[T](n)
  for i in 0..<n:
    result[i] = e
type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] =
  newSeqWith(n1, newSeq[T](n2))
type seq3*[T] = seq[seq[seq[T]]]
proc newSeq3*[T](n1, n2, n3: Natural): seq3[T] =
  newSeqWith(n1, newSeqWith(n2, newSeq[T](n3)))

#------------------------------------------------------------------------------#
type Pos = tuple [ x, y: int ]

proc `+`(p1, p2: Pos): Pos = (p1.x + p2.x, p1.y + p2.y)
proc `-`(p1, p2: Pos): Pos = (p1.x - p2.x, p1.y - p2.y)
proc dot(p1, p2: Pos): int = p1.x * p2.x + p1.y * p2.y
proc det(p1, p2: Pos): int = p1.x * p2.y - p1.y * p2.x
#------------------------------------------------------------------------------#

proc main() =
  let n = 300
  var ps = newSeq[(Pos, int)](n)
  for i in 0..<n:
    let (x, y) = readInt2()
    ps[i] = ((x, y), i)
  ps.sort(cmp[(Pos, int)])

  echo 100
  for i in 0..<n div 3:
    let ans = [ ps[i * 3][1] + 1, ps[i * 3 + 1][1] + 1, ps[i * 3 + 2][1] + 1 ]
    echo ans.map(it => $it).join(" ")

main()


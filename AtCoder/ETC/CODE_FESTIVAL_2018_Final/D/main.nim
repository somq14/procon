import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import future

const INF* = int(1e18 + 373)

proc readLine*(): string =
  stdin.readLine()
proc readSeq*(): seq[string] =
  readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = readLine().strip()
proc readInt1*(): int =
  readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])
proc newSeqWith*[T](n: Natural; e: T): seq[T] =
  result = newSeq[T](n)
  for i in 0..<n:
    result[i] = e
type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] =
  newSeqWith(n1, newSeq[T](n2))
type seq3*[T] = seq[seq[seq[T]]]
proc newSeq3*[T](n1, n2, n3: Natural): seq3[T] =
  newSeqWith(n1, newSeqWith(n2, newSeq[T](n3)))

#------------------------------------------------------------------------------#
const SIGMA = 52

proc encode(c: char): int =
  if c in 'A'..'Z':
    return (c.ord() - 'A'.ord())
  if c in 'a'..'z':
    return (c.ord() - 'a'.ord()) + (SIGMA div 2)
  assert false

proc decode(c: int): char =
  if c in 0..<(SIGMA div 2):
    return ('A'.ord() + c).char()
  if c in (SIGMA div 2)..<SIGMA:
    return ('a'.ord() + c - (SIGMA div 2)).char()

proc buildNextTable(s: string): seq2[int] =
  result = newSeq2[int](s.len() + 1, 0)

  result[s.len()] = newSeq[int](SIGMA)
  result[s.len()].fill(-1)

  for i in countdown(s.len() - 1, 0):
    result[i] = result[i + 1]
    result[i][s[i].encode()] = i

proc main() =
  let n = readInt1()
  var s = readSeq(n)

  var next = newSeq[seq2[int]](n)
  for i in 0..<n:
    next[i] = buildNextTable(s[i])

  var cnt = newSeq[int](SIGMA)
  var opt = -1
  var ans = ""
  for a in 0..<SIGMA:
    for b in 0..<SIGMA:
      cnt.fill(0)
      for i in 0..<n:
        if next[i][0][a] == -1:
          continue
        if next[i][next[i][0][a] + 1][b] == -1:
          continue
        let tab = next[i][next[i][next[i][0][a] + 1][b] + 1]
        for c in 0..<SIGMA:
          if tab[c] != -1:
            cnt[c] += 1

      var maxInd = -1
      for c in 0..<SIGMA:
        if maxInd == -1 or cnt[c] > cnt[maxInd]:
          maxInd = c

      if cnt[maxInd] == 0:
        continue

      if opt == -1 or cnt[maxInd] > opt:
        opt = cnt[maxInd]
        ans = $a.decode() & $b.decode() & $maxInd.decode()

  echo ans

main()


import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import future

const INF* = int(1e18 + 373)

proc readLine*(): string =
  stdin.readLine()
proc readSeq*(): seq[string] =
  readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = readLine().strip()
proc readInt1*(): int =
  readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])
proc newSeqWith*[T](n: Natural; e: T): seq[T] =
  result = newSeq[T](n)
  for i in 0..<n:
    result[i] = e
type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] =
  newSeqWith(n1, newSeq[T](n2))
type seq3*[T] = seq[seq[seq[T]]]
proc newSeq3*[T](n1, n2, n3: Natural): seq3[T] =
  newSeqWith(n1, newSeqWith(n2, newSeq[T](n3)))

#------------------------------------------------------------------------------#
type Pred1[T] = T -> bool

# [lb, ub)
proc nibutanLb(lb, ub: int; f: Pred1[int]): int =
  var lb = lb
  var ub = ub
  while ub - lb > 1:
    let mid = (ub + lb) div 2
    if f(mid):
      lb = mid
    else:
      ub = mid
  return lb

# (lb, ub]
proc nibutanUb(lb, ub: int; f: Pred1[int]): int =
  var lb = lb
  var ub = ub
  while ub - lb > 1:
    let mid = (ub + lb) div 2
    if f(mid):
      ub = mid
    else:
      lb = mid
  return ub
#------------------------------------------------------------------------------#
#------------------------------------------------------------------------------#
type Edge = tuple [ u, v, w: int ]

proc `<`(e1, e2: Edge): bool =
  e1.w < e2.w

proc main() =
  let (n, m) = readInt2()

  var g = newSeq2[Edge](n, 0)
  for i in 0..<m:
    let (a, b, c) = readInt3()
    g[a - 1].add((a - 1, b - 1, c))
    g[b - 1].add((b - 1, a - 1, c))

  for v in 0..<n:
    g[v].sort(cmp[Edge])

  var ans = 0

  for a in 0..<n:
    for i in 0..<g[a].len():
      let e1 = g[a][i]
      let b = e1.v
      let w1 = e1.w
      let w2 = 2540 - w1
      let s = nibutanUb(-1, g[b].len(), (x: int) => (g[b][x].w >= w2))
      let t = nibutanUb(-1, g[b].len(), (x: int) => (g[b][x].w >= w2 + 1))
      ans += t - s
      if w1 == 2540 div 2:
        ans -= 1

  echo ans div 2

main()


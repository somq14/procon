import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import future

const INF* = int(1e18 + 373)

proc readLine*(): string =
  stdin.readLine()
proc readSeq*(): seq[string] =
  readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = readLine().strip()
proc readInt1*(): int =
  readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])
proc newSeqWith*[T](n: Natural; e: T): seq[T] =
  result = newSeq[T](n)
  for i in 0..<n:
    result[i] = e
type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] =
  newSeqWith(n1, newSeq[T](n2))
type seq3*[T] = seq[seq[seq[T]]]
proc newSeq3*[T](n1, n2, n3: Natural): seq3[T] =
  newSeqWith(n1, newSeqWith(n2, newSeq[T](n3)))

#------------------------------------------------------------------------------#
type Pred1[T] = T -> bool

# [lb, ub)
proc nibutanLb(lb, ub: int; f: Pred1[int]): int =
  var lb = lb
  var ub = ub
  while ub - lb > 1:
    let mid = (ub + lb) div 2
    if f(mid):
      lb = mid
    else:
      ub = mid
  return lb

# (lb, ub]
proc nibutanUb(lb, ub: int; f: Pred1[int]): int =
  var lb = lb
  var ub = ub
  while ub - lb > 1:
    let mid = (ub + lb) div 2
    if f(mid):
      ub = mid
    else:
      lb = mid
  return ub
#------------------------------------------------------------------------------#
#------------------------------------------------------------------------------#
type Func = tuple [ s, t, a, b: int ]

proc `<`(f1, f2: Func): bool =
  f1.a < f2.a

proc main() =
  let n = readInt1()

  var f = newSeq[Func](n)
  for i in 0..<n:
    let (a, b) = readInt2()
    f[i] = (0, INF, a, b)

  let m = readInt1()
  var qq = newSeq[int](m)
  for i in 0..<m:
    qq[i] = readInt1()

  f.sort(cmp[Func])

  var g = newSeq[Func](0)
  var t = 0
  for i in 0..<n:
    g.add((t, f[i].a, 0, f[i].b))
    t = f[i].a

    if i + 1 >= n:
      break

    let d = f[i + 1].b - f[i].b
    g.add((t, t + d, 1, f[i].b - t))
    t += d

  g.add((t, INF, 1, f[n - 1].b - f[n - 1].a))

  for q in qq:
    let ind = nibutanLb(0, g.len(), (x: int) => (q >= g[x].s))
    let ans = g[ind].a * q + g[ind].b
    echo ans

main()


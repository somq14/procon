import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import future

const INF* = int(1e18 + 373)

proc readLine*(): string =
  stdin.readLine()
proc readSeq*(): seq[string] =
  readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = readLine().strip()
proc readInt1*(): int =
  readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])
proc newSeqWith*[T](n: Natural; e: T): seq[T] =
  result = newSeq[T](n)
  for i in 0..<n:
    result[i] = e
type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] =
  newSeqWith(n1, newSeq[T](n2))
type seq3*[T] = seq[seq[seq[T]]]
proc newSeq3*[T](n1, n2, n3: Natural): seq3[T] =
  newSeqWith(n1, newSeqWith(n2, newSeq[T](n3)))

#------------------------------------------------------------------------------#
var factMemo = @[ log10(1.0) ]

proc fact(n: int): float =
  if n < factMemo.len():
    return factMemo[n]

  for i in factMemo.len()..n:
    factMemo.add(factMemo[i - 1] + log10(i.float()))

  return factMemo[n]

proc comb(n, r: int): float =
  fact(n) - fact(r) - fact(n - r)

proc main() =
  let (n, m) = readInt2()
  let r = readSeq().map(parseInt)

  var k = n
  var ans = 0.0
  for i in 0..<m:
    ans += k.comb(r[i])
    k -= r[i]
  ans -= log10(m.float()) * n.float()

  ans *= -1
  echo ans.ceil().int()

main()


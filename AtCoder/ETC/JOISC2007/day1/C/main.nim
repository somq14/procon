import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import future

const INF* = int(1e10 + 373)

proc readLine*(): string =
  stdin.readLine()
proc readSeq*(): seq[string] =
  readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = readLine().strip()
proc readInt1*(): int =
  readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])
proc newSeqWith*[T](n: Natural; e: T): seq[T] =
  result = newSeq[T](n)
  for i in 0..<n:
    result[i] = e
type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] =
  newSeqWith(n1, newSeq[T](n2))
type seq3*[T] = seq[seq[seq[T]]]
proc newSeq3*[T](n1, n2, n3: Natural): seq3[T] =
  newSeqWith(n1, newSeqWith(n2, newSeq[T](n3)))

#------------------------------------------------------------------------------#
proc buildCumTab2(a: seq2[int]): seq2[int] =
  let h = a.len()
  let w = a[0].len()
  var c = newSeq2[int](h + 1, w + 1)
  for y in 1..h:
    for x in 1..w:
      c[y][x] = c[y - 1][x] + c[y][x - 1] - c[y - 1][x - 1] + a[y - 1][x - 1]
  return c

proc lookupCumTab2(c: seq2[int]; sy, sx, ty, tx: int): int =
  c[ty][tx] - c[sy][tx] - c[ty][sx] + c[sy][sx]

proc main() =
  let (w, h) = readInt2()
  let (a, b) = readInt2()

  var s = newSeq2[int](h, 0)
  for i in 0..<h:
    s[i] = readSeq().map(parseInt)

  for i in 0..<h:
    for j in 0..<w:
      if s[i][j] == -1:
        s[i][j] = INF

  let c = buildCumTab2(s)

  var ans = INF
  for i in 0..<h:
    for j in 0..<w:
      if i + b > h or j + a > w:
        continue
      let sol = c.lookupCumTab2(i, j, i + b, j + a)
      if sol < ans:
        ans = sol

  echo ans

main()


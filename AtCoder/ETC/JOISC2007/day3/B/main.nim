import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import future

const INF* = int(1e15 + 373)

proc readLine*(): string =
  stdin.readLine()
proc readSeq*(): seq[string] =
  readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = readLine().strip()
proc readInt1*(): int =
  readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])
proc newSeqWith*[T](n: Natural; e: T): seq[T] =
  result = newSeq[T](n)
  for i in 0..<n:
    result[i] = e
type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] =
  newSeqWith(n1, newSeq[T](n2))
type seq3*[T] = seq[seq[seq[T]]]
proc newSeq3*[T](n1, n2, n3: Natural): seq3[T] =
  newSeqWith(n1, newSeqWith(n2, newSeq[T](n3)))

#------------------------------------------------------------------------------#
type Pos = tuple [ x, y: int ]
proc `+`(p1, p2: Pos): Pos = (p1.x + p2.x, p1.y + p2.y)
proc `-`(p1, p2: Pos): Pos = (p1.x - p2.x, p1.y - p2.y)
proc dot(p1, p2: Pos): int = p1.x * p2.x + p1.y * p2.y
proc det(p1, p2: Pos): int = p1.x * p2.y - p1.y * p2.x

#------------------------------------------------------------------------------#

type PriorityQueue[T] = object
  a: seq[T]
  n: Natural
  cmp: (a: T, b: T) -> bool

proc initPriorityQueue[T](cmp: (a: T, b: T) -> bool): PriorityQueue[T] =
  result.a = newSeq[T](1)
  result.n = Natural(0)
  result.cmp = cmp

proc len[T](this: PriorityQueue[T]): Natural =
  this.n

proc enqueue[T](this: var PriorityQueue[T], e: T) =
  this.n += 1
  this.a.add(e)
  var i = this.a.len() - 1
  while i > 1 and this.cmp(this.a[i], this.a[i div 2]):
    swap(this.a[i], this.a[i div 2])
    i = i div 2

proc dequeue[T](this: var PriorityQueue[T]): T =
  this.n -= 1
  result = this.a[1]
  this.a[1] = this.a.pop()
  var i = 1
  while i < this.a.len():
    var j = i
    if i * 2 < this.a.len() and this.cmp(this.a[i * 2], this.a[j]):
      j = i * 2
    if i * 2 + 1 < this.a.len() and this.cmp(this.a[i * 2 + 1], this.a[j]):
      j = i * 2 + 1
    if i == j:
      break
    swap(this.a[i], this.a[j])
    i = j

proc front[T](this: PriorityQueue[T]): T =
  this.a[1]

#------------------------------------------------------------------------------#
type Edge = tuple [ a, b, c: int ]

proc dijkstra(n: int; g: seq2[Edge]; s: int): seq[int] =
  var d = newSeq[int](n)
  d.fill(INF)
  d[s] = 0

  type P = tuple[c, v: int]
  var q = initPriorityQueue[P]((p1: P, p2: P) => p1.c < p2.c)
  q.enqueue((0, s))

  while q.len() > 0:
    let (c, v) = q.dequeue()
    if c > d[v]:
      continue
    for e in g[v]:
      let alt = d[v] + e.c
      if alt < d[e.b]:
        d[e.b] = alt
        q.enqueue((alt, e.b))

  return d

proc main() =
  let (n, m) = readInt2()

  var vs = newSeq[Pos](n)
  for i in 0..<n:
    vs[i] = readInt2()

  var es = newSeq[Edge](m)
  for i in 0..<m:
    let (a, b, c) = readInt3()
    es[i] = (a - 1, b - 1, c)

  var w = newSeq2[int](n, n)
  for i in 0..<n:
    w[i].fill(INF)
  for e in es:
    w[e.a][e.b] = e.c
    w[e.b][e.a] = e.c

  var id = newSeq2[int](n, n)
  var nn = 0
  block:
    for e in es:
      id[e.a][e.b] = nn
      nn += 1
      id[e.b][e.a] = nn
      nn += 1

  var g = newSeq2[Edge](nn + 2, 0)

  for i in 0..<n:
    for j in 0..<n:
      if i == j:
        continue
      if w[i][j] >= INF:
        continue
      for k in 0..<n:
        if i == k or j == k:
          continue
        if w[j][k] >= INF:
          continue
        if (vs[i] - vs[j]).dot(vs[k] - vs[j]) <= 0:
          g[id[i][j]].add((id[i][j], id[j][k], w[j][k]))

  for i in 0..<n:
    if w[0][i] >= INF:
      continue
    g[nn].add((nn, id[0][i], w[0][i]))

  for i in 0..<n:
    if w[i][1] >= INF:
      continue
    g[id[i][1]].add((id[i][1], nn + 1, 0))

  let d = dijkstra(nn + 2, g, nn)
  let ans = if d[nn + 1] >= INF: -1 else: d[nn + 1]
  echo ans

main()


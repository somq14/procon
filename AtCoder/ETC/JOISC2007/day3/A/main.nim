import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import future

const INF* = int(1e18 + 373)

proc readLine*(): string =
  stdin.readLine()
proc readSeq*(): seq[string] =
  readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = readLine().strip()
proc readInt1*(): int =
  readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])
proc newSeqWith*[T](n: Natural; e: T): seq[T] =
  result = newSeq[T](n)
  for i in 0..<n:
    result[i] = e
type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] =
  newSeqWith(n1, newSeq[T](n2))
type seq3*[T] = seq[seq[seq[T]]]
proc newSeq3*[T](n1, n2, n3: Natural): seq3[T] =
  newSeqWith(n1, newSeqWith(n2, newSeq[T](n3)))

#------------------------------------------------------------------------------#
proc fact(n: int): int =
  result = 1
  for i in 2..n:
    result *= i

proc subr(alpha: Table[char, int]; a: char): int =
  var sum = 0
  for c in 'A'..'Z':
    sum += alpha[c]
  sum -= 1

  var comb = fact(sum)
  for c in 'A'..'Z':
    let n = if c == a: (alpha[c] - 1) else: alpha[c]
    comb = comb div fact(n)
  return comb

proc main() =
  let s = readLine()
  let n = s.len()

  var alpha = initTable[char, int]()
  for c in 'A'..'Z':
    alpha[c] = 0
  for c in s:
    alpha[c] += 1

  var ans = 0
  for i in 0..<n:
    for c in 'A'..<s[i]:
      if alpha[c] <= 0:
        continue
      ans += subr(alpha, c)
    alpha[s[i]] -= 1
  echo ans + 1

main()


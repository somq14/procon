import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import logging
import future

const INF* = int(1e18 + 373)

proc readLine*(): string = stdin.readLine()
proc readSeq*(): seq[string] = readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = readLine().strip()
proc readInt1*(): int = readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])
type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))
type seq3*[T] = seq[seq[seq[T]]]
proc newSeq3*[T](n1, n2, n3: Natural): seq3[T] = newSeqWith(n1, newSeqWith(n2, newSeq[T](n3)))

#------------------------------------------------------------------------------#
const MOD = int(10^9+7)

type Op1[T] = proc(a: T, b: T): T
type Op2[T] = proc(a: T, b: T): T

proc repeatedSquares[T](x: T, n: Natural, e: T, f: Op2[T]): T =
    result = e
    var xx = f(x, e)
    var i = n
    while i > 0:
        if (i and 1) != 0:
            result = f(result, xx)
        xx = f(xx, xx)
        i = i shr 1

proc addM(a, b: Natural): Natural = (a + b) mod MOD
proc subM(a, b: Natural): Natural = (a - b + MOD) mod MOD
proc mulM(a, b: Natural): Natural = a * b mod MOD
proc powM(a, b: Natural): Natural = repeatedSquares[Natural](a, b, 1, `mulM`)
proc invM(a: Natural): Natural = a.powM(MOD - 2)
proc divM(a, b: Natural): Natural = a.mulM(invM(b))

var memoFactM: seq[Natural] = nil
var memoFactInvM: seq[Natural] = nil

proc buildFactTable(n: Natural) =
  memoFactM = newSeq[Natural](n + 1)
  memoFactM[0] = 1
  for i in 1..n:
    memoFactM[i] = memoFactM[i - 1].mulM(i)

  memoFactInvM = newSeq[Natural](n + 1)
  memoFactInvM[n] = invM(memoFactM[n])
  for i in countdown(n - 1, 0):
    memoFactInvM[i] = memoFactInvM[i + 1].mulM(i + 1)

buildFactTable(10^6)

proc factM(n: Natural): Natural = memoFactM[n]

proc factInvM(n: Natural): Natural = memoFactInvM[n]

proc combM(n, r: Natural): Natural =
  if r > n:
    return 0
  if r > n div 2:
    return combM(n, n - r)

  result = factM(n).mulM(factInvM(n - r)).mulM(factInvM(r))

proc multCombM(n, r: Natural): Natural = combM(n + r - 1, r - 1)

#------------------------------------------------------------------------------#
type SegmentTree[T] = tuple[ n: int, a: seq[T], e: T, f: Op2[T] ]

proc initSegmentTree[T](n: int, e: T, f: Op2[T]): SegmentTree[T] =
    var m = 1
    while m < n:
        m *= 2

    var a = newSeq[T](2 * m)
    a.fill(e)
    return (m, a, e, f)

proc update[T](this: var SegmentTree[T], i: int, x: T) =
    this.a[this.n + i] = x
    var j = (this.n + i) div 2
    while j > 0:
        this.a[j] = this.f(this.a[j * 2], this.a[j * 2 + 1])
        j = j div 2

proc query[T](this: SegmentTree[T], s, t, i, ll, hh: int): T =
    if t <= ll or hh <= s:
        return this.e
    if s <= ll and hh <= t:
        return this.a[i]

    let m = (ll + hh) div 2
    let vl = this.query(s, t, i * 2, ll, m)
    let vh = this.query(s, t, i * 2 + 1, m, hh)
    return this.f(vl, vh)

proc query[T](this: SegmentTree[T], s, t: int): T = query(this, s, t, 1, 0, this.n)

#------------------------------------------------------------------------------#

proc main() =
  let n = readInt1()
  let a = readSeq().map(parseInt)

  var segtree = initSegmentTree[int](n + 1, 0, (a: int, b: int) => (a + b))
  var ans = 0
  for i in 0..<n:
    let k = (a[i] - 1) - segtree.query(0, a[i])
    let comb = k.mulM(factM(n - 1 - i))
    ans = ans.addM(comb)
    segtree.update(a[i], 1)

  echo ans.addM(1)

main()


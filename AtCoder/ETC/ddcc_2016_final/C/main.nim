import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import logging
import future

const INF* = int(1e18 + 373)

proc readLine(): string = stdin.readLine()
proc readSeq*(): seq[string] = readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

proc readInt1*(): int = readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#

proc main() =
  let (a, b, c) = readInt3()
  let t = readLine()
  let n = t.len()

  if n == 1:
    if t == "0":
      echo min(a, b + c)
    else:
      echo min(b, a + c)
    return

  # [0, i)
  var memoA = newSeq[int](n + 1)
  memoA[0] = 0
  memoA[1] = if t[0] == '0': 0 else: 1
  for i in 2..n:
    if t[i - 2] == t[i - 1]:
      memoA[i] = memoA[i - 1]
    else:
      memoA[i] = memoA[i - 1] + 1

  # [i, n)
  var memoB = newSeq[int](n + 1)
  memoB[n] = 0
  memoB[n - 1] = if t[n - 1] == '1': 0 else: 1
  for i in countdown(n - 2, 0):
    if t[i] == t[i + 1]:
      memoB[i] = memoB[i + 1]
    else:
      memoB[i] = memoB[i + 1] + 1

  # [0, i)
  var ans = INF
  for i in 0..n:
    let sol = c * max(memoA[i], memoB[i]) + i * a + (n - i) * b
    ans = min(ans, sol)
  echo ans

main()


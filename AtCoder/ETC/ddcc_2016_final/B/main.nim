import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import logging
import future

const INF* = int(1e18 + 373)

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

proc readInt1*(): int = readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#

proc main() =
  let (r, n, m) = readInt3()

  let w = 2.0 / n.float()

  var xs = newSeq[float](n - 1)
  for i in 0..<n - 1:
    let d = abs(w * (i + 1).float() - 1.0)
    xs[i] = 2 * sqrt(1.0 - d * d)

  # echo xs

  var used = newSeq[bool](n - 1)
  var ans = 0.0
  while true:
    var maxInd1 = -1
    for i in 0..<n - 1:
      if not used[i] and  (maxInd1 == -1 or xs[i] > xs[maxInd1]):
        maxInd1 = i

    if maxInd1 == -1:
      break
    used[maxInd1] = true

    var maxInd2 = -1
    for i in 0..<n - 1:
      if not used[i] and abs(i - maxInd1) >= m and (maxInd2 == -1 or xs[i] > xs[maxInd2]):
        maxInd2 = i

    if maxInd2 != -1:
      used[maxInd2] = true

    ans += xs[maxInd1]

  echo r.float() * ans

main()


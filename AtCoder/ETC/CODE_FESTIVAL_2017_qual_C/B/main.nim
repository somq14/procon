import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import logging
import future

const INF* = int(1e18 + 373)

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

proc readInt1*(): int = readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#

proc main() =
  let n = readInt1()
  let a = readSeq().map(parseInt)

  var dp = newSeq2[int](2, n + 1)
  dp[0][0] = 0
  dp[1][0] = 1
  for i in 1..n:
    if a[i - 1] mod 2 == 0:
      dp[0][i] = 3 * dp[0][i - 1] + 1 * dp[1][i - 1]
      dp[1][i] = 2 * dp[1][i - 1]
    else:
      dp[0][i] = 3 * dp[0][i - 1] + 2 * dp[1][i - 1]
      dp[1][i] = 1 * dp[1][i - 1]

  echo dp[0][n]

main()


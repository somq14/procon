import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import logging
import future

const INF* = int(1e9 + 7)

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

proc readInt1*(): int = readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#

proc main() =
  let s = stdin.readLine().map(it => it.ord() - 'a'.ord())
  let n = s.len()

  let alphabet = 'z'.ord() - 'a'.ord() + 1
  var memo = newSeq[int32](1 shl alphabet)
  memo.fill(INF)

  var cum = 0
  var dp = newSeq[int](n + 1)
  dp[0] = 0
  memo[0] = 0
  for i in 1..n:
    var opt = dp[i - 1] + 1
    cum = cum xor (1 shl s[i - 1])

    let j = memo[cum]
    if j < i:
      opt = min(opt, dp[j] + 1)

    for c in 0..<alphabet:
      let j = memo[cum xor (1 shl c)]
      if j < i:
        opt = min(opt, dp[j] + 1)

    dp[i] = opt

    if memo[cum] >= INF or dp[i] < dp[memo[cum]]:
      memo[cum] = i.int32()

  echo dp[n]

main()


import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import logging
import future

const INF* = int(1e18 + 373)

proc readLine*(): string = stdin.readLine()

proc readSeq*(): seq[string] = readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = readLine().strip()

proc readInt1*(): int = readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#
#------------------------------------------------------------------------------#
type Pred1[T] = T -> bool

# [lb, ub)
proc nibutanLb(lb, ub: int; f: Pred1[int]): int =
  var lb = lb
  var ub = ub
  while ub - lb > 1:
    let mid = (ub + lb) div 2
    if f(mid):
      lb = mid
    else:
      ub = mid
  return lb

# (lb, ub]
proc nibutanUb(lb, ub: int; f: Pred1[int]): int =
  var lb = lb
  var ub = ub
  while ub - lb > 1:
    let mid = (ub + lb) div 2
    if f(mid):
      ub = mid
    else:
      lb = mid
  return ub
#------------------------------------------------------------------------------#
const MOD = 60 * 3600

proc crossCount1(h, m, s, t: int): int =
  let initS = 3600 * s
  let initM = 3600 * m + 60 * s
  let distance = (initM - initS + MOD) mod MOD

  let moveS = 3600 * t
  let moveM = 60 * t
  let moveDiff = moveS - moveM

  if moveDiff < distance:
    return 0
  else:
    return 1 + (moveDiff - distance) div MOD

proc crossCount2(h, m, s, t: int): int =
  let initM = 3600 * m + 60 * s
  let initH = 3600 * 5 * h + 60 * 5 * m + 5 * s
  let distance = (initH - initM + MOD) mod MOD

  let moveM = 60 * t
  let moveH = 5 * t
  let moveDiff = moveM - moveH

  if moveDiff < distance:
    return 0
  else:
    return 1 + (moveDiff - distance) div MOD


proc cross(h, m, s, t: int): bool =
  let initS = 3600 * s
  let initM = 3600 * m + 60 * s
  let initH = 3600 * 5 * h + 60 * 5 * m + 5 * s
  let posS = (initS + 3600 * t) mod MOD
  let posM = (initM + 60 * t) mod MOD
  let posH = (initH + 5 * t) mod MOD
  return posS == posM or posS == posH or posM == posH

proc main() =
  var (h, m, s) = readInt3()
  let (c1, c2) = readInt2()

  h = h mod 12

  var tMax = nibutanLb(0, 10^14, it => crossCount1(h, m, s, it) <= c1 and crossCount2(h, m, s, it) <= c2)
  var tMin = nibutanUb(-1, 10^14, it => crossCount1(h, m, s, it) >= c1 and crossCount2(h, m, s, it) >= c2)

  while cross(h, m, s, tMin):
    tMin += 1

  if tMin <= tMax:
    echo tMin, " ", tMax
  else:
    echo -1

main()


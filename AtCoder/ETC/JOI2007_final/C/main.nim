import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import logging
import future

const INF* = int(1e18 + 373)

proc readLine*(): string =
  stdin.readLine()
proc readSeq*(): seq[string] =
  readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = readLine().strip()
proc readInt1*(): int =
  readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])
type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] =
  newSeqWith(n1, newSeq[T](n2))
type seq3*[T] = seq[seq[seq[T]]]
proc newSeq3*[T](n1, n2, n3: Natural): seq3[T] =
  newSeqWith(n1, newSeqWith(n2, newSeq[T](n3)))

#------------------------------------------------------------------------------#
type Pos = tuple [ x, y : int ]

proc main() =
  let n = readInt1()

  var ps = newSeq[Pos](n)
  for i in 0..<n:
    ps[i] = readInt2()

  var pset = initSet[Pos]()
  for i in 0..<n:
    pset.incl(ps[i])

  var ans = 0

  for i in 0..<n:
    for j in 0..<n:
      if i == j:
        continue
      let p0 = ps[i]
      let p1 = ps[j]
      let d: Pos = (p1.x - p0.x, p1.y - p0.y)
      let p2: Pos = (p1.x - d.y, p1.y + d.x)
      let p3: Pos = (p2.x - d.x, p2.y - d.y)

      if p2 in pset and p3 in pset:
        ans = max(ans, d.x * d.x + d.y * d.y)

  echo ans

main()


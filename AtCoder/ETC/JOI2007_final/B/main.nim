import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import logging
import future

const INF* = int(1e18 + 373)

proc readLine*(): string =
  stdin.readLine()
proc readSeq*(): seq[string] =
  readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = readLine().strip()
proc readInt1*(): int =
  readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])
type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] =
  newSeqWith(n1, newSeq[T](n2))
type seq3*[T] = seq[seq[seq[T]]]
proc newSeq3*[T](n1, n2, n3: Natural): seq3[T] =
  newSeqWith(n1, newSeqWith(n2, newSeq[T](n3)))

#------------------------------------------------------------------------------#

proc main() =
  let (n, k) = readInt2()
  let a = readSeq(k).map(parseInt).sorted(cmp[int])

  var rs = newSeq[(int, int)](0)
  block:
    var i = if a[0] == 0: 1 else: 0
    while i < k:
      var j = i
      while j < k and a[i] + (j - i) == a[j]:
        j += 1
      rs.add((a[i], a[j - 1]))
      i = j

  let m = rs.len()
  var ans = -1
  if a[0] != 0:
    for i in 0..<m:
      ans = max(ans, rs[i][1] - rs[i][0] + 1)
  else:
    for i in 0..<m:
      var opt = rs[i][1] - rs[i][0] + 1
      if rs[i][0] > 1:
        opt = max(opt, rs[i][1] - rs[i][0] + 1 + 1)
      if rs[i][1] < n:
        opt = max(opt, rs[i][1] - rs[i][0] + 1 + 1)
      if i - 1 >= 0 and rs[i - 1][1] + 2 == rs[i][0]:
        opt = max(opt, rs[i][1] - rs[i - 1][0] + 1)
      if i + 1 < m and rs[i][1] + 2 == rs[i + 1][0]:
        opt = max(opt, rs[i + 1][1] - rs[i][0] + 1)
      ans = max(ans, opt)

  echo ans


main()


import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import logging
import future

const INF* = int(1e18 + 373)

proc readLine*(): string =
  stdin.readLine()
proc readSeq*(): seq[string] =
  readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = readLine().strip()
proc readInt1*(): int =
  readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])
type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] =
  newSeqWith(n1, newSeq[T](n2))
type seq3*[T] = seq[seq[seq[T]]]
proc newSeq3*[T](n1, n2, n3: Natural): seq3[T] =
  newSeqWith(n1, newSeqWith(n2, newSeq[T](n3)))

#------------------------------------------------------------------------------#
proc gcd(a, b: int): int =
  var a = a
  var b = b
  while b != 0:
    let tmp = a mod b
    a = b
    b = tmp
  return a

proc lcm(a, b: int): int =
  a * b div gcd(a, b)

type Edge = tuple [ p, q, r, b: int ]

proc solve(g: seq[Edge]; v: int): int =
  if v == -1:
    return 1

  let weightR = solve(g, g[v].r)
  let weightB = solve(g, g[v].b)

  let powerR = g[v].p * weightR
  let powerB = g[v].q * weightB
  let balancedPower = lcm(powerR, powerB)

  return (balancedPower div g[v].p) + (balancedPower div g[v].q)

proc main() =
  let n = readInt1()

  var g = newSeq[Edge](n)
  var isChild = newSeq[bool](n)
  for i in 0..<n:
    g[i] = readInt4()
    g[i].r -= 1
    g[i].b -= 1

    if g[i].r >= 0:
      isChild[g[i].r] = true
    if g[i].b >= 0:
      isChild[g[i].b] = true

  var root = -1
  for v in 0..<n:
    if not isChild[v]:
      root = v

  echo solve(g, root)

main()


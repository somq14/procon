import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import logging
import future

const INF* = int(1e18 + 373)

proc readLine*(): string =
  stdin.readLine()
proc readSeq*(): seq[string] =
  readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = readLine().strip()
proc readInt1*(): int =
  readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])
type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] =
  newSeqWith(n1, newSeq[T](n2))
type seq3*[T] = seq[seq[seq[T]]]
proc newSeq3*[T](n1, n2, n3: Natural): seq3[T] =
  newSeqWith(n1, newSeqWith(n2, newSeq[T](n3)))

#------------------------------------------------------------------------------#
proc dfs1(g: seq2[int]; v: int; r: var int; ans: var seq[int]) =
  for i in 0..<g[v].len():
    let u = g[v][i]
    if ans[u] == -1:
      dfs1(g, u, r, ans)
  ans[v] = r
  r += 1

proc dfs2(g: seq2[int]; v: int; r: var int; ans: var seq[int]) =
  for i in countdown(g[v].len() - 1, 0):
    let u = g[v][i]
    if ans[u] == -1:
      dfs2(g, u, r, ans)
  ans[v] = r
  r += 1

proc main() =
  let n = readInt1()
  let m = readInt1()

  var g = newSeq2[int](n, 0)
  var win = newSeq[bool](n)
  for i in 0..<m:
    var (a, b) = readInt2()
    a -= 1
    b -= 1
    win[a] = true
    g[b].add(a)

  var ans1 = newSeq[int](n)
  block:
    ans1.fill(-1)
    var r = 0
    for v in 0..<n:
      if win[v]:
        continue
      dfs1(g, v, r, ans1)

  var ans2 = newSeq[int](n)
  block:
    ans2.fill(-1)
    var r = 0
    for v in countdown(n - 1, 0):
      if win[v]:
        continue
      dfs2(g, v, r, ans2)

  var rank = newSeq[int](n)
  for i in 0..<n:
    rank[ans1[i]] = i

  for i in 0..<n:
    echo rank[i] + 1

  echo if ans1 == ans2: 0 else: 1

main()


import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import future

const INF* = int(1e18 + 373)

proc readLine*(): string =
  stdin.readLine()
proc readSeq*(): seq[string] =
  readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = readLine().strip()
proc readInt1*(): int =
  readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])
proc newSeqWith*[T](n: Natural; e: T): seq[T] =
  result = newSeq[T](n)
  for i in 0..<n:
    result[i] = e
type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] =
  newSeqWith(n1, newSeq[T](n2))
type seq3*[T] = seq[seq[seq[T]]]
proc newSeq3*[T](n1, n2, n3: Natural): seq3[T] =
  newSeqWith(n1, newSeqWith(n2, newSeq[T](n3)))

#------------------------------------------------------------------------------#
proc dfs(g: seq2[int]; v: int; visited: var seq[bool]): int =
  visited[v] = true

  var sum = 1
  for u in g[v]:
    if visited[u]:
      continue
    sum += dfs(g, u, visited)

  return sum

proc main() =
  let (n, m) = readInt2()

  var g = newSeq2[int](n, 0)
  for i in 0..<m:
    let (u, v) = readInt2()
    g[u - 1].add(v - 1)
    g[v - 1].add(u - 1)

  var visited = newSeq[bool](n)

  var w0 = dfs(g, 0, visited)
  var w1 = dfs(g, 1, visited)
  if w0 > w1:
    swap(w0, w1)
  w1 = n - w0

  let emax = w1 * (w1 - 1) div 2 + w0 * (w0 - 1) div 2
  let ans = emax - m
  echo ans

main()


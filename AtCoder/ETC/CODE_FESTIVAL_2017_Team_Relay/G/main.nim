import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import future

const INF* = int(1e18 + 373)

proc readLine*(): string =
  stdin.readLine()
proc readSeq*(): seq[string] =
  readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = readLine().strip()
proc readInt1*(): int =
  readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])
proc newSeqWith*[T](n: Natural; e: T): seq[T] =
  result = newSeq[T](n)
  for i in 0..<n:
    result[i] = e
type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] =
  newSeqWith(n1, newSeq[T](n2))
type seq3*[T] = seq[seq[seq[T]]]
proc newSeq3*[T](n1, n2, n3: Natural): seq3[T] =
  newSeqWith(n1, newSeqWith(n2, newSeq[T](n3)))

#------------------------------------------------------------------------------#

proc main() =
  let ll = readInt1()
  var s = readLine()
  var t = readLine()

  var scnt1 = -1
  var tcnt1 = -1
  for i in countdown(ll, 0):
    if s.len() * i <= ll and (ll - s.len() * i) mod t.len() == 0:
      scnt1 = i
      tcnt1 = (ll - s.len() * i) div t.len()
      break

  var scnt2 = -1
  var tcnt2 = -1
  for i in countdown(ll, 0):
    if t.len() * i <= ll and (ll - t.len() * i) mod s.len() == 0:
      tcnt2 = i
      scnt2 = (ll - t.len() * i) div s.len()
      break

  var sol1 = newStringOfCap(ll + 1)
  for i in 0..<scnt1:
    sol1 &= s
  for i in 0..<tcnt1:
    sol1 &= t

  var sol2 = newStringOfCap(ll + 1)
  for i in 0..<tcnt2:
    sol2 &= t
  for i in 0..<scnt2:
    sol2 &= s

  echo min(sol1, sol2)

main()


import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import logging
import future

const INF* = int(1e18 + 373)

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

proc readInt1*(): int = readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#
proc mux[T](cond: bool; v1, v2: T): T =
  if cond:
    v1
  else:
    v2

proc dump[T](a: seq[T]) =
  for e in a:
    echo e
  echo ""

proc main() =
  let (h, w) = readInt2()
  var s = newSeq[string](h)
  for i in 0..<h:
    s[i] = stdin.readLine()

  var memoL = newSeq2[int](h, w)
  var memoR = newSeq2[int](h, w)
  var memoU = newSeq2[int](h, w)
  var memoD = newSeq2[int](h, w)

  for y in 0..<h:
    memoL[y][0] = mux(s[y][0] == '.', 1, 0)
    for x in 1..<w:
      memoL[y][x] = mux(s[y][x] == '.', memoL[y][x - 1] + 1 , 0)

  for y in 0..<h:
    memoR[y][w - 1] = mux(s[y][w - 1] == '.', 1, 0)
    for x in countdown(w - 2, 0):
      memoR[y][x] = mux(s[y][x] == '.', memoR[y][x + 1] + 1 , 0)

  for x in 0..<w:
    memoU[0][x] = mux(s[0][x] == '.', 1, 0)
    for y in 1..<h:
      memoU[y][x] = mux(s[y][x] == '.', memoU[y - 1][x] + 1 , 0)

  for x in 0..<w:
    memoD[h - 1][x] = mux(s[h - 1][x] == '.', 1, 0)
    for y in countdown(h - 2, 0):
      memoD[y][x] = mux(s[y][x] == '.', memoD[y + 1][x] + 1 , 0)

  var ans = 0
  for y in 0..<h:
    for x in 0..<w:
      if s[y][x] == '#':
        continue

      let ll = memoL[y][x]
      let rr = memoR[y][x]
      let uu = memoU[y][x]
      let dd = memoD[y][x]
      ans += (ll - 1) * (uu - 1)
      ans += (uu - 1) * (rr - 1)
      ans += (rr - 1) * (dd - 1)
      ans += (dd - 1) * (ll - 1)

  echo ans


main()


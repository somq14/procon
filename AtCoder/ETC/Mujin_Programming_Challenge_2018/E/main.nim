import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import logging
import future

const INF* = int(1e18 + 373)

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

proc readInt1*(): int = readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

# FIXME: Use object
type PriorityQueue[T] = tuple[a: seq[T], n: Natural, cmp: proc(a, b: T): bool]

proc initPriorityQueue[T](cmp: proc(a, b: T): bool): PriorityQueue[T] =
    (newSeq[T](1), Natural(0), cmp)

proc len[T](this: PriorityQueue[T]): Natural = this.n

proc enqueue[T](this: var PriorityQueue[T], e: T) =
    this.n += 1
    this.a.add(e)
    var i = this.a.len() - 1
    while i > 1 and this.cmp(this.a[i], this.a[i div 2]):
        swap(this.a[i], this.a[i div 2])
        i = i div 2

proc dequeue[T](this: var PriorityQueue[T]): T =
    this.n -= 1
    result = this.a[1]
    this.a[1] = this.a.pop()
    var i = 1
    while i < this.a.len():
        var j = i
        if i * 2 < this.a.len() and this.cmp(this.a[i * 2], this.a[j]):
            j = i * 2
        if i * 2 + 1 < this.a.len() and this.cmp(this.a[i * 2 + 1], this.a[j]):
            j = i * 2 + 1
        if i == j:
            break
        swap(this.a[i], this.a[j])
        i = j

# FIXME: throw Exception when this is empty
proc front[T](this: PriorityQueue[T]): T = this.a[1]

#------------------------------------------------------------------------------#

type Point = tuple [ y, x: int ]

const dirC = @['U', 'D', 'L', 'R']
const dirV: seq[Point] = @[(-1, 0), (1, 0), (0, -1), (0, 1)]

proc main() =
  let (h, w, k) = readInt3()
  var d = stdin.readLine()
  d = d & d

  var s = newSeq2[char](h, 0)
  for i in 0..<h:
    s[i] = stdin.readLine().map(it => it)

  var dp = newSeq2[int](4, 2 * k + 1)
  dp[0][2 * k] = -1
  dp[1][2 * k] = -1
  dp[2][2 * k] = -1
  dp[3][2 * k] = -1
  for i in countdown(2 * k - 1, 0):
    for j in 0..<4:
      if d[i] == dirC[j]:
        dp[j][i] = 1
      elif dp[j][i + 1] >= 0:
        dp[j][i] = dp[j][i + 1] + 1
      else:
        dp[j][i] = -1

  var ss: Point
  var tt: Point
  for y in 0..<h:
    for x in 0..<w:
      if s[y][x] == 'S':
        ss = (y, x)
        s[y][x] = '.'
      if s[y][x] == 'G':
        tt = (y, x)
        s[y][x] = '.'

  var dis = newSeq2[int](h, w)
  for y in 0..<h:
    dis[y].fill(INF)
  dis[ss.y][ss.x] = 0

  type P = tuple[c: int, v: Point]
  var q = initPriorityQueue[P]((p1: P, p2: P) => p1.c < p2.c)
  q.enqueue((0, ss))

  while q.len() > 0:
    let (c, v) = q.dequeue()
    if c > dis[v.y][v.x]:
      continue
    for j in 0..<4:
      let nextV: Point = (v.y + dirV[j].y, v.x + dirV[j].x)
      if nextV.y notin 0..<h or nextV.x notin 0..<w or
        s[nextV.y][nextV.x] == '#':
        continue
      if dp[j][c mod k] == -1:
        continue
      let alt = dis[v.y][v.x] + dp[j][c mod k]
      if alt < dis[nextV.y][nextV.x]:
        dis[nextV.y][nextV.x] = alt
        q.enqueue((alt, nextV))

  echo if dis[tt.y][tt.x] >= INF: -1 else: dis[tt.y][tt.x]

main()


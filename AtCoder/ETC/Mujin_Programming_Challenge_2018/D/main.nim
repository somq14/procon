import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import logging
import future

const INF* = int(1e18 + 373)

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

proc readInt1*(): int = readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#
proc rev(x: int): int =
  result = 0
  var x = x
  while x != 0:
    result = result * 10 + (x mod 10)
    x = x div 10

const MAX = 999

type State = enum
  UNDEF, FINITE, INFINITE, ROUTE

proc dfs(y, x: int; memo: var seq2[State]): State =
  if memo[y][x] == FINITE:
    return FINITE

  if memo[y][x] == INFINITE:
    return INFINITE

  if memo[y][x] == ROUTE:
    memo[y][x] = INFINITE
    return INFINITE

  assert memo[y][x] == UNDEF
  memo[y][x] = ROUTE

  var nextY = y
  var nextX = x
  if nextY >= nextX:
    nextX = rev(x)
  else:
    nextY = rev(y)

  if nextY >= nextX:
    nextY = nextY - nextX
  else:
    nextX = nextX - nextY

  memo[y][x] = dfs(nextY, nextX, memo)
  return memo[y][x]

proc main() =
  let (w, h) = readInt2()

  var memo = newSeq2[State](MAX + 1, MAX + 1)
  for y in 0..MAX:
    memo[y].fill(UNDEF)
  for y in 0..MAX:
    memo[y][0] = FINITE
  for x in 0..MAX:
    memo[0][x] = FINITE

  var ans = 0
  for y in 1..h:
    for x in 1..w:
      if dfs(y, x, memo) == INFINITE:
        ans += 1

  echo ans

main()


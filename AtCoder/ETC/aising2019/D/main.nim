import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import future

const INF* = int(1e18 + 373)

proc readLine*(): string =
  stdin.readLine()
proc readSeq*(): seq[string] =
  readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = readLine().strip()
proc readInt1*(): int =
  readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])
proc newSeqWith*[T](n: Natural; e: T): seq[T] =
  result = newSeq[T](n)
  for i in 0..<n:
    result[i] = e
type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] =
  newSeqWith(n1, newSeq[T](n2))
type seq3*[T] = seq[seq[seq[T]]]
proc newSeq3*[T](n1, n2, n3: Natural): seq3[T] =
  newSeqWith(n1, newSeqWith(n2, newSeq[T](n3)))

#------------------------------------------------------------------------------#

proc buildCumTab(a: seq[int]): seq[int] =
  let n = a.len()
  var c = newSeq[int](n + 1)
  for i in 1..n:
    c[i] = c[i - 1] + a[i - 1]
  return c

# [s, t)
proc lookupCumTab(c: seq[int]; s, t: int): int = c[t] - c[s]

#------------------------------------------------------------------------------#
proc eval(n: int; a, acum1, acum2: seq[int]; x, k: int): int =
  let s = max(n - k - k, 0)
  let t = n - k
  if s > 0 and abs(a[s - 1] - x) <= abs(a[t - 1] - x):
    return 1
  return -1

proc solve(n: int; a, acum1, acum2: seq[int]; x: int): int =
  var ub = (n + 1) div 2
  var lb = 0
  while ub - lb > 1:
    let mid = (ub + lb) div 2
    let res = eval(n, a, acum1, acum2, x, mid)
    #echo "mid = ", mid
    #echo "res = ", res
    if res == -1:
      ub = mid
    else:
      lb = mid

  let k = ub
  var ans = 0
  ans += acum1.lookupCumTab(n - k, n)
  ans += acum2[max(n - k - k, 0)]
  return ans

proc main() =
  let (n, q) = readInt2()
  let a = readSeq().map(parseInt)
  let qq = readSeq(q).map(parseInt)

  let acum1 = a.buildCumTab()
  var acum2 = newSeq[int](n + 1)
  acum2[0] = 0
  acum2[1] = a[0]
  acum2[2] = a[1]
  for i in 3..n:
    acum2[i] = a[i - 1] + acum2[i - 2]

  for i in 0..<q:
    let ans = solve(n, a, acum1, acum2, qq[i])
    echo ans

main()


import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import future

const INF* = int(1e18 + 373)

proc readLine*(): string =
  stdin.readLine()
proc readSeq*(): seq[string] =
  readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = readLine().strip()
proc readInt1*(): int =
  readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])
proc newSeqWith*[T](n: Natural; e: T): seq[T] =
  result = newSeq[T](n)
  for i in 0..<n:
    result[i] = e
type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] =
  newSeqWith(n1, newSeq[T](n2))
type seq3*[T] = seq[seq[seq[T]]]
proc newSeq3*[T](n1, n2, n3: Natural): seq3[T] =
  newSeqWith(n1, newSeqWith(n2, newSeq[T](n3)))

#------------------------------------------------------------------------------#
proc toRootedTree(g: seq2[int]; v, p: int; res: var seq2[int]) =
  for u in g[v]:
    if u == p:
      continue
    res[v].add(u)
    toRootedTree(g, u, v, res)

proc partialTreeWeight(g: seq2[int]; v: int; res: var seq[int]): int =
  var sum = 1
  for u in g[v]:
    sum += partialTreeWeight(g, u, res)
  res[v] = sum
  return sum

proc dfs(g: seq2[int]; a, w: seq[int]; v: int; dp1, dp2: var seq2[int]) =
  if g[v].len() == 0:
    dp1[v] = @[ (if a[v] > 0: a[v] else: INF) ]
    dp2[v] = @[ a[v] ]
    return

  for u in g[v]:
    dfs(g, a, w, u, dp1, dp2)

  let deg = g[v].len()

  # dp1
  if a[v] < 0:
    dp1[v] = newSeq[int](w[v])
    dp1[v].fill(INF)
  else:
    var subDp1 = newSeq2[int](deg + 1, 0)

    subDp1[0] = @[ a[v] ]

    for i in 1..deg:
      let u = g[v][i - 1]
      subDp1[i] = newSeq[int](subDp1[i - 1].len() + w[g[v][i - 1]])
      subDp1[i].fill(INF)
      for jL in 0..<subDp1[i - 1].len():
        for jR in 0..<w[u]:
          if subDp1[i - 1][jL] != INF and dp1[u][jR] != INF:
            subDp1[i][jL + jR] = min(subDp1[i][jL + jR], subDp1[i - 1][jL] + dp1[u][jR])
          if subDp1[i - 1][jL] != INF and (dp1[u][jR] != INF or dp2[u][jR] < 0):
            subDp1[i][jL + jR + 1] = min(subDp1[i][jL + jR + 1], subDp1[i - 1][jL])

    dp1[v] = subDp1[deg]

  # dp2
  var subDp2 = newSeq2[int](deg + 1, 0)
  subDp2[0] = @[ a[v] ]

  for i in 1..deg:
    let u = g[v][i - 1]
    subDp2[i] = newSeq[int](subDp2[i - 1].len() + w[u])
    subDp2[i].fill(INF)
    for jL in 0..<subDp2[i - 1].len():
      for jR in 0..<w[u]:
        if subDp2[i - 1][jL] != INF and dp2[u][jR] != INF:
          subDp2[i][jL + jR] = min(subDp2[i][jL + jR], subDp2[i - 1][jL] + dp2[u][jR])
        if subDp2[i - 1][jL] != INF and (dp2[u][jR] < 0 or dp1[u][jR] != INF):
          subDp2[i][jL + jR + 1] = min(subDp2[i][jL + jR + 1], subDp2[i - 1][jL])

  dp2[v] = subDp2[deg]

proc main() =
  let n = readInt1()
  let a = readSeq().map(parseInt)

  var g = newSeq2[int](n, 0)
  for i in 0..<n - 1:
    let (u, v) = readInt2()
    g[u - 1].add(v - 1)
    g[v - 1].add(u - 1)

  var gg = newSeq2[int](n, 0)
  toRootedTree(g, 0, -1, gg)

  var w = newSeq[int](n)
  discard partialTreeWeight(gg, 0, w)

  var dp1 = newSeq2[int](n, 0)
  var dp2 = newSeq2[int](n, 0)
  dfs(gg, a, w, 0, dp1, dp2)

  var ans = INF
  for i in countdown(n - 1, 0):
    if dp1[0][i] != INF:
      ans = min(ans, i)

  for i in countdown(n - 1, 0):
    if dp2[0][i] != INF and dp2[0][i] < 0:
      ans = min(ans, i)

  echo ans

main()


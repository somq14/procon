import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import future

const INF* = int(1e18 + 373)

proc readLine*(): string =
  stdin.readLine()
proc readSeq*(): seq[string] =
  readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = readLine().strip()
proc readInt1*(): int =
  readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])
proc newSeqWith*[T](n: Natural; e: T): seq[T] =
  result = newSeq[T](n)
  for i in 0..<n:
    result[i] = e
type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] =
  newSeqWith(n1, newSeq[T](n2))
type seq3*[T] = seq[seq[seq[T]]]
proc newSeq3*[T](n1, n2, n3: Natural): seq3[T] =
  newSeqWith(n1, newSeqWith(n2, newSeq[T](n3)))

#------------------------------------------------------------------------------#
const dy = [ -1, 1, 0, 0 ]
const dx = [ 0, 0, -1, 1 ]
proc dfs(h, w: int; s: seq[string]; y, x, id: int; a: var seq2[int]) =
  a[y][x] = id

  for k in 0..<4:
    let ny = y + dy[k]
    let nx = x + dx[k]
    if ny notin 0..<h or nx notin 0..<w:
      continue
    if a[ny][nx] >= 0:
      continue
    if s[ny][nx] != s[y][x]:
      dfs(h, w, s, ny, nx, id, a)

proc main() =
  let (h, w) = readInt2()
  var s = newSeq[string](h)
  for i in 0..<h:
    s[i] = readLine()

  var a = newSeq2[int](h, w)
  for y in 0..<h:
    a[y].fill(-1)

  var id = 0
  for y in 0..<h:
    for x in 0..<w:
      if a[y][x] >= 0:
        continue
      dfs(h, w, s, y, x, id, a)
      id += 1

  var hist = newSeq[int](id)
  for y in 0..<h:
    for x in 0..<w:
      if s[y][x] == '.':
        hist[a[y][x]] += 1

  var ans = 0
  for y in 0..<h:
    for x in 0..<w:
      if s[y][x] == '#':
        ans += hist[a[y][x]]
  echo ans

main()


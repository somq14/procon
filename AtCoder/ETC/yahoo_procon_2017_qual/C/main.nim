import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import logging
import future

const INF* = int(1e18 + 373)

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

proc readInt1*(): int = readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#

proc main() =
  let (n, k) = readInt2()
  let a = readSeq().map(parseInt)
  let s = readSeq(n)

  var hit = newSeq[bool](n)
  for i in 0..<k:
    hit[a[i] - 1] = true

  var s1 = newSeq[string](0)
  var s2 = newSeq[string](0)
  for i in 0..<n:
    if hit[i]:
      s1.add(s[i])
    else:
      s2.add(s[i])

  if s2.len() == 0:
    echo ""
    return

  var t = ""
  block:
    var i = 0
    while true:
      var same = true
      for e in s1:
        if i >= e.len() or e[i] != s1[0][i]:
          same = false
          break

      if not same:
        t = s1[0][0..<i]
        break
      i += 1

  var maxMatch = 0
  for e in s2:
    var j = 0
    while j < e.len() and j < t.len() and e[j] == t[j]:
      j += 1
    maxMatch = max(maxMatch, j)

  if maxMatch >= t.len():
    echo -1
  else:
    echo t[0..maxMatch]

main()


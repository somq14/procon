import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import logging
import future

const INF* = int(1e18 + 373)

proc readLine*(): string =
  stdin.readLine()
proc readSeq*(): seq[string] =
  readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = readLine().strip()
proc readInt1*(): int =
  readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])
type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] =
  newSeqWith(n1, newSeq[T](n2))
type seq3*[T] = seq[seq[seq[T]]]
proc newSeq3*[T](n1, n2, n3: Natural): seq3[T] =
  newSeqWith(n1, newSeqWith(n2, newSeq[T](n3)))

#------------------------------------------------------------------------------#
type Test = tuple [ i, j, k, r: int ]

proc main() =
  let (a, b, c) = readInt3()
  let m = readInt1()
  var ts = newSeq[Test](m)
  for i in 0..<m:
    ts[i] = readInt4()
    ts[i].i -= 1
    ts[i].j -= 1
    ts[i].k -= 1

  let n = a + b + c
  var x = newSeq[int](n)
  x.fill(2)
  for i in 0..<m:
    let t = ts[i]
    if t.r == 1:
      x[t.i] = 1
      x[t.j] = 1
      x[t.k] = 1

  for i in 0..<m:
    let t = ts[i]
    if t.r == 0 and x[t.i] == 2 and x[t.j] == 1 and x[t.k] == 1:
      x[t.i] = 0
    if t.r == 0 and x[t.i] == 1 and x[t.j] == 2 and x[t.k] == 1:
      x[t.j] = 0
    if t.r == 0 and x[t.i] == 1 and x[t.j] == 1 and x[t.k] == 2:
      x[t.k] = 0

  for i in 0..<n:
    echo x[i]

main()


import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import logging
import future

const INF* = int(1e18 + 373)

proc readLine*(): string =
  stdin.readLine()
proc readSeq*(): seq[string] =
  readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = readLine().strip()
proc readInt1*(): int =
  readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])
type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] =
  newSeqWith(n1, newSeq[T](n2))
type seq3*[T] = seq[seq[seq[T]]]
proc newSeq3*[T](n1, n2, n3: Natural): seq3[T] =
  newSeqWith(n1, newSeqWith(n2, newSeq[T](n3)))

#------------------------------------------------------------------------------#
var tmp = @[ 0 ]
proc shuffle(a: var seq[int]) =
  if tmp.len() != a.len():
    tmp = newSeq[int](a.len())

  let n = a.len()
  for i in 0..<n:
    tmp[i] = a[i]

  for i in 0..<n div 2:
    a[i * 2] = tmp[i]

  for i in 0..<n div 2:
    a[i * 2 + 1] = tmp[n div 2 + i]

proc cut(a: var seq[int]; k: int) =
  if tmp.len() != a.len():
    tmp = newSeq[int](a.len())

  let n = a.len()
  for i in 0..<n:
    tmp[i] = a[i]

  for i in 0..<k:
    a[n - k + i] = tmp[i]

  for i in k..<n:
    a[i - k] = tmp[i]

proc main() =
  let n = readInt1()

  var a  = newSeq[int](n + n)
  for i in 0..<n + n:
    a[i] = i + 1

  let m = readInt1()
  for i in 0..<m:
    let k = readInt1()
    if k == 0:
      shuffle(a)
    else:
      cut(a, k)

  for i in 0..<n + n:
    echo a[i]

main()


import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import logging
import future

const INF* = int(1e18 + 373)

proc readLine*(): string =
  stdin.readLine()
proc readSeq*(): seq[string] =
  readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = readLine().strip()
proc readInt1*(): int =
  readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])
type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] =
  newSeqWith(n1, newSeq[T](n2))
type seq3*[T] = seq[seq[seq[T]]]
proc newSeq3*[T](n1, n2, n3: Natural): seq3[T] =
  newSeqWith(n1, newSeqWith(n2, newSeq[T](n3)))

#------------------------------------------------------------------------------#
proc main() =
  let (w, h) = readInt2()
  let n = readInt1()
  var a = newSeq2[int](h, w)
  var b = newSeq2[bool](h, w)
  for i in 0..<n:
    var (x, y) = readInt2()
    x -= 1
    y -= 1
    b[y][x] = true

  a[0][0] = 1
  for y in 0..<h:
    for x in 0..<w:
      if y + 1 in 0..<h and not b[y + 1][x]:
        a[y + 1][x] += a[y][x]
      if x + 1 in 0..<w and not b[y][x + 1]:
        a[y][x + 1] += a[y][x]

  echo a[h - 1][w - 1]

main()


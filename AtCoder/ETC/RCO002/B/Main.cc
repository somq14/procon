#include <algorithm>
#include <cmath>
#include <complex>
#include <functional>
#include <iomanip>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <string>
#include <utility>
#include <vector>

using namespace std;

using ll = long long int;
#define int ll

const int MOD = 1e9 + 7;
const int INF = 1e15 + 373;

#define rep(i, n) for (int i = 0; i < (n); ++i)

template <typename T>
using vector2 = vector<vector<T>>;
template <typename T>
vector2<T> init_vector2(size_t n0, size_t n1, T e = T()) {
    return vector2<T>(n0, vector<T>(n1, e));
}

template <typename T>
using vector3 = vector<vector<vector<T>>>;
template <typename T>
vector3<T> init_vector3(size_t n0, size_t n1, size_t n2, T e = T()) {
    return vector3<T>(n0, vector2<T>(n1, vector<T>(n2, e)));
}

template <typename T>
ostream& operator<<(ostream& os, const vector<T>& v) {
    os << "[";
    for (T e : v) {
        os << e << ", ";
    }
    os << "]";
    return os;
}

int CLASS_NUM = 16;
int CLASS_NUM_SQRT = 4;
int H, W, D, K;

static vector<int> class_table;
void init_class_table() {
    class_table = vector<int>(CLASS_NUM);
    rep(i, CLASS_NUM_SQRT) {
        rep(j, CLASS_NUM_SQRT) {
            if (i % 2 == 0) {
                class_table[i * CLASS_NUM_SQRT + j] = i * CLASS_NUM_SQRT + j;
            } else {
                class_table[i * CLASS_NUM_SQRT + j] =
                    i * CLASS_NUM_SQRT + (CLASS_NUM_SQRT - 1 - j);
            }
        }
    }
}

int clasify_ind(int ind) { return ind / (D / CLASS_NUM); }

int clasify_pos(int y, int x) {
    int cy = y / (H / CLASS_NUM_SQRT);
    int cx = x / (W / CLASS_NUM_SQRT);
    int ind = cy * CLASS_NUM_SQRT + cx;
    return class_table[ind];
}

int eval_class_pair(const pair<int, int>& p) {
    int i = p.first;
    int j = p.second;
    int iy = i / CLASS_NUM_SQRT;
    int ix = i % CLASS_NUM_SQRT;
    int jy = j / CLASS_NUM_SQRT;
    int jx = j % CLASS_NUM_SQRT;
    return abs(iy - jy) + abs(ix - jx);
}

bool compare_class_pair(const pair<int, int>& p1, const pair<int, int>& p2) {
    return eval_class_pair(p1) > eval_class_pair(p2);
}

vector<pair<int, int>> generate_class_pair() {
    vector<pair<int, int>> ans;
    rep(i, CLASS_NUM) {
        rep(j, CLASS_NUM) {
            if (i >= j) {
                continue;
            }
            ans.push_back(make_pair(i, j));
        }
    }

    sort(ans.begin(), ans.end(), compare_class_pair);

    int m = ans.size();
    rep(i, m) {
        ans[i].first = class_table[ans[i].first];
        ans[i].second = class_table[ans[i].second];
    }
    return ans;
}

int eval(const vector<int>& y, const vector<int>& x, int a, int b) {
    return abs(y[a] - y[b]) + abs(x[a] - x[b]);
}

signed main() {
    init_class_table();
    cin >> H >> W >> D >> K;

    vector<int> y(D);
    vector<int> x(D);
    rep(i, D) { cin >> y[i] >> x[i]; }

    vector2<vector<int>> a = init_vector2<vector<int>>(CLASS_NUM, CLASS_NUM);

    rep(i, D) {
        int curr_area = clasify_pos(y[i], x[i]);
        int dest_area = clasify_ind(i);
        if (curr_area == dest_area) {
            continue;
        }
        a[curr_area][dest_area].push_back(i);
    }

    vector<pair<int, int>> class_pair = generate_class_pair();

    int cnt = 0;
    while (cnt < K) {
        bool update = false;

        for (const pair<int, int>& p : class_pair) {
            int i = p.first;
            int j = p.second;
            while (cnt < K && !a[i][j].empty() && !a[j][i].empty()) {
                int d0 = a[i][j].back();
                int d1 = a[j][i].back();
                for (size_t l = 0; l < a[j][i].size(); l++) {
                    int it = a[j][i][l];
                    if (eval(y, x, it, (d0 + 1) % D) +
                            eval(y, x, it, (d0 - 1 + D) % D) <
                        eval(y, x, d1, (d0 + 1) % D) +
                            eval(y, x, d1, (d0 - 1 + D) % D)) {
                        d1 = it;
                        swap(a[j][i][a[j][i].size() - 1], a[j][i][l]);
                    }
                }
                a[i][j].pop_back();
                a[j][i].pop_back();
                cout << y[d0] << " " << x[d0] << " " << y[d1] << " " << x[d1]
                     << endl;

                update = true;
                cnt++;
            }
        }

        if (!update) {
            break;
        }
    }

    return 0;
}

#include <algorithm>
#include <cmath>
#include <complex>
#include <functional>
#include <iomanip>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <string>
#include <utility>
#include <vector>

using namespace std;

using ll = long long int;
#define int ll

const int MOD = 1e9 + 7;
const int INF = 1e15 + 373;

#define rep(i, n) for (int i = 0; i < (n); ++i)

template <typename T>
using vector2 = vector<vector<T>>;
template <typename T>
vector2<T> init_vector2(size_t n0, size_t n1, T e = T()) {
    return vector2<T>(n0, vector<T>(n1, e));
}

template <typename T>
using vector3 = vector<vector<vector<T>>>;
template <typename T>
vector3<T> init_vector3(size_t n0, size_t n1, size_t n2, T e = T()) {
    return vector3<T>(n0, vector2<T>(n1, vector<T>(n2, e)));
}

template <typename T>
ostream& operator<<(ostream& os, const vector<T>& v) {
    os << "[";
    for (T e : v) {
        os << e << ", ";
    }
    os << "]";
    return os;
}

class pos {
   public:
    int y;
    int x;

    pos() : y(0), x(0) {}
    pos(int y, int x) : y(y), x(x) {}

    bool operator==(const pos& p) const { return y == p.y && x == p.x; }
    bool operator!=(const pos& p) const { return y != p.y || x != p.x; }
    pos operator+(const pos& p) const { return pos(y + p.y, x + p.x); }
    pos operator-(const pos& p) const { return pos(y - p.y, x - p.x); }
};

ostream& operator<<(ostream& os, const pos& p) {
    os << "(" << p.y << "," << p.x << ")";
    return os;
}

typedef vector<string> field;

int N, K, H, W, T;

const vector<pos> dir = {pos(-1, 0), pos(1, 0), pos(0, -1), pos(0, 1)};

pos search(const field& a, char c) {
    rep(i, H) {
        rep(j, W) {
            if (a[i][j] == c) {
                return pos(i, j);
            }
        }
    }
    return pos(-1, -1);
}

vector<pos> bfs(const field& a, const pos& me) {
    const pos UNDEF = pos(-1, -1);
    vector2<pos> par = init_vector2(H, W, UNDEF);

    queue<pos> q;
    q.push(me);
    par[me.y][me.x] = pos(0, 0);

    pos it = UNDEF;
    while (!q.empty()) {
        pos p = q.front();
        q.pop();

        for (const pos& d : dir) {
            const pos next = p + d;
            if (par[next.y][next.x] != UNDEF) {
                continue;
            }

            if (a[next.y][next.x] == 'o') {
                par[next.y][next.x] = d;
                it = next;
                goto out;
            }

            if (a[next.y][next.x] == '.') {
                par[next.y][next.x] = d;
                q.push(next);
            }
        }
    }
out:
    if (it == UNDEF) {
        return vector<pos>();
    }

    vector<pos> ans;
    while (it != me) {
        ans.push_back(par[it.y][it.x]);
        it = it - par[it.y][it.x];
    }
    reverse(ans.begin(), ans.end());
    return ans;
}

vector<pos> generate_path(const field& _a) {
    field a(_a);

    vector<pos> ans;

    pos me = search(a, '@');
    a[me.y][me.x] = '.';

    int step = 0;
    while (step < T) {
        // greedy
        bool greedy_found = false;
        for (const pos& d : dir) {
            pos next = me + d;
            if (a[next.y][next.x] == 'o') {
                ans.push_back(d);
                me = next;
                step++;
                a[next.y][next.x] = '.';
                greedy_found = true;
                break;
            }
        }
        if (greedy_found) {
            continue;
        }

        // search coin
        vector<pos> path = bfs(a, me);
        int len = path.size();
        if (path.size() == 0 || len + step > T) {
            break;
        }
        rep(i, len) {
            me = me + path[i];
            step++;
            ans.push_back(path[i]);
        }
        a[me.y][me.x] = '.';
    }

    return ans;
}

struct ans {
    int score;
    vector<int> ans_map;
    vector<pos> ans_steps;
};

ans solve(const vector<field>& _a, int s) {
    vector<field> a(_a);

    vector<pos> steps = generate_path(a[s]);

    vector<pos> me(N);
    rep(k, N) { me[k] = search(a[k], '@'); }

    vector<pair<int, int>> field_score(N);
    rep(i, N) { field_score[i] = make_pair(0, i); }

    for (const pos& step : steps) {
        rep(k, N) {
            const field& f = a[k];
            const pos& p = me[k];

            if (f[p.y][p.x] == 'x') {
               continue;
            }

            const pos q = p + step;
            if (f[q.y][q.x] == '#') {
                continue;
            }
            if (f[q.y][q.x] == 'o') {
                field_score[k].first++;
                a[k][q.y][q.x] = '.';
                me[k] = q;
            }
            if (f[q.y][q.x] == '.') {
                me[k] = q;
            }
            if (f[q.y][q.x] == 'x') {
                me[k] = q;
            }
        }
    }

    sort(field_score.begin(), field_score.end(), greater<pair<int, int>>());

    vector<int> ans_map;
    rep(i, K) { ans_map.push_back(field_score[i].second); }

    int score = 0;
    rep(i, K) { score += field_score[i].first; }

    return {score, ans_map, steps};
}

signed main() {
    cin >> N >> K >> H >> W >> T;

    vector<field> a(N, field(H));
    rep(k, N) {
        rep(i, H) { cin >> a[k][i]; }
    }

    ans best;
    best.score = -1;
    rep(i, N) {
        ans candi = solve(a, i);
        if (candi.score > best.score) {
            best = candi;
        }
    }

    rep(i, K - 1) { cout << best.ans_map[i] << " "; }
    cout << best.ans_map[K - 1] << endl;

    rep(i, T) {
        if (best.ans_steps[i] == pos(-1, 0)) {
            cout << "U";
        } else if (best.ans_steps[i] == pos(1, 0)) {
            cout << "D";
        } else if (best.ans_steps[i] == pos(0, -1)) {
            cout << "L";
        } else if (best.ans_steps[i] == pos(0, 1)) {
            cout << "R";
        }
    }
    cout << endl;

    return 0;
}

import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import logging
import future

const INF* = int(1e18 + 373)

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

proc readInt1*(): int = readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#

proc main() =
  let n = readInt1()
  var x = readSeq().map(parseInt)
  var a = @[-1] & readSeq().map(parseInt).map(it => it - 1)

  var g = newSeq[int](n)
  for v in 1..<n:
    g[a[v]] = g[a[v]] or (1 shl v)

  var disk = newSeq[int](1 shl n)
  disk[0] = 0
  for s in 1..<(1 shl n):
    var sum = 0
    for i in 0..<n:
      if (s and (1 shl i)) != 0 and (s and (1 shl a[i])) == 0:
        sum += x[i]
    disk[s] = sum

  var dp = newSeq[int](1 shl n)
  dp[0] = 0
  for s in 1..<(1 shl n):
    var opt = INF
    for i in 0..<n:
      if (s and (1 shl i)) == 0:
        continue
      if (g[i] and s) == g[i]:
        opt = min(opt, max(dp[s xor (1 shl i)], disk[s xor (1 shl i)] + x[i]))
    dp[s] = opt

  echo dp[(1 shl n) - 1]

main()


import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import logging
import future

const INF* = int(1e18 + 373)

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

proc readInt1*(): int = readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#
proc query(q: seq[int]): int =
  echo("? " & q.map(it => $it).join(" "))
  return readInt1()

proc main() =
  let n = readInt1()

  var ans = newSeq[bool](n)

  var i = 0
  while i < n:
    var q = newSeq[int](n)
    q.fill(0)
    for j in 0..<min(6, n - i):
      q[i + j] = 6 ^ j

    var res = query(q)
    for j in 0..<min(6, n - i):
      ans[i + j] = res mod 2 != 0
      res = (res - 8) div 6

    i += min(6, n - i)

  echo "! " & ans.map(it => (if it: "1" else: "0")).join(" ")

main()


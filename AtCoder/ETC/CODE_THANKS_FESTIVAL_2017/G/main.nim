import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import logging
import future

const INF* = int(1e18 + 373)

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

proc readInt1*(): int = readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#
proc countTrailingZeroBits(x: int): int =
  var x = x and not (x - 1)

  # [lb, ub)
  var ub = 64
  var lb = 0
  while ub - lb > 1:
    let mid = (ub + lb) div 2
    if x shr mid != 0:
      lb = mid
    else:
      ub = mid

  return lb

proc main() =
  let (n, m) = readInt2()

  var g = newSeq[int](n)
  for i in 0..<n:
    g[i] = 1 shl i
  for i in 0..<m:
    var (a, b) = readInt2()
    a -= 1
    b -= 1
    g[a] = g[a] or (1 shl b)
    g[b] = g[b] or (1 shl a)

  # echo "g = ", g

  let n1 = n div 2
  let n2 = n - n1
  # echo "n1 = ", n1
  # echo "n2 = ", n2

  var dp1 = newSeq[int](1 shl n1)
  dp1[0] = 0
  for s in 1..<(1 shl n1):
    let v = s.countTrailingZeroBits()
    dp1[s] = max(dp1[s and not (1 shl v)], 1 + dp1[s and not g[v]])

  # echo "dp1 = ", dp1

  var dp2 = newSeq[int](1 shl n2)
  dp2[0] = 0
  for s in 1..<(1 shl n2):
    let v = s.countTrailingZeroBits()
    dp2[s] = max(dp2[s and not (1 shl v)], 1 + dp2[s and not ((g[v + n1]) shr n1)])

  # echo "dp2 = ", dp2

  var ans = 0
  for s in 0..<(1 shl n2):
    var neighbors = 0
    for i in 0..<n2:
      if (s and (1 shl i)) != 0:
        neighbors = neighbors or g[n1 + i]

    ans = max(ans, dp1[((1 shl n1) - 1) and not neighbors] + dp2[s])

  echo ans


main()


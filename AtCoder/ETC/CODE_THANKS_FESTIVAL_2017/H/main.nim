import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import logging
import future

const INF* = int(1e18 + 373)

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

proc readInt1*(): int = readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#
#------------------------------------------------------------------------------#
type UnionFindTree = tuple[ p, h, w: seq[int] ]

proc initUnionFindTree(n: int): UnionFindTree =
    var p = newSeq[int](n)
    p.fill(-1)
    var h = newSeq[int](n)
    h.fill(0)
    var w = newSeq[int](n)
    w.fill(1)
    return (p, h, w)

proc find(this: var UnionFindTree, v: int): int =
    if this.p[v] == -1:
        return v
    this.p[v] = find(this, this.p[v])
    return this.p[v]

proc same(this: var UnionFindTree, u, v: int): bool =
    this.find(u) == this.find(v)

proc union(this: var UnionFindTree, u, v: int) =
    let uRoot = this.find(u)
    let vRoot = this.find(v)
    if uRoot == vRoot:
        return

    if this.h[uRoot] > this.h[vRoot]:
        this.p[vRoot] = uRoot
        this.w[uRoot] += this.w[vRoot]
    else:
        this.p[uRoot] = vRoot
        this.w[vRoot] += this.w[uRoot]
        if this.h[uRoot] == this.h[vRoot]:
            this.h[vRoot] += 1

#------------------------------------------------------------------------------#
type Edge = tuple [ u, v: int ]

const MAX_TREE_DEPTH = 20

proc buildParentTable(ps: seq[int]): seq2[int] =
  let n = ps.len()
  var t = newSeq2[int](MAX_TREE_DEPTH + 1, n)

  for v in 0..<n:
    t[0][v] = ps[v]

  for i in 1..MAX_TREE_DEPTH:
    for v in 0..<n:
      t[i][v] = t[i - 1][t[i - 1][v]]

  return t

proc parent(pt: seq2[int]; v, i: int): int =
  var p = v
  for j in 0..MAX_TREE_DEPTH:
    if (i and (1 shl j)) == 0:
      continue
    p = pt[j][p]
  return p

proc depth(pt: seq2[int]; v: int): int =
  var lb = -1
  var ub = 1 shl MAX_TREE_DEPTH
  while ub - lb > 1:
    let mid = (ub + lb) div 2
    if pt.parent(v, mid) == pt.parent(v, mid + 1):
      ub = mid
    else:
      lb = mid
  return ub

proc lca(pt: seq2[int]; u, v: int): int =
  if pt[MAX_TREE_DEPTH][u] != pt[MAX_TREE_DEPTH][v]:
    return -1

  var u = u
  var v = v
  let du = pt.depth(u)
  let dv = pt.depth(v)
  if du < dv:
    swap(u, v)
  u = pt.parent(u, abs(du - dv))

  if u == v:
    return u

  var lb = -1
  var ub = 1 shl MAX_TREE_DEPTH
  while ub - lb > 1:
    let mid = (ub + lb) div 2
    if pt.parent(u, mid) == pt.parent(v, mid):
      ub = mid
    else:
      lb = mid

  return pt.parent(u, ub)

proc main() =
  let (n, m) = readInt2()

  var es = newSeq[Edge](m)
  for i in 0..<m:
    let (u, v) = readInt2()
    es[i] = (u - 1, v - 1)

  let q = readInt1()
  var qs = newSeq[Edge](q)
  for i in 0..<q:
    let (x, y) = readInt2()
    qs[i] = (x - 1, y - 1)

  var uft = initUnionFindTree(n)

  var ps = newSeq[int](n)
  var ts = newSeq[int](n)
  var rt = newSeq[int](n)

  ts.fill(0)
  for i in 0..<n:
    rt[i] = i
    ps[i] = i

  for i in 0..<m:
    var u = es[i].u
    var v = es[i].v
    if uft.same(u, v):
      continue

    u = uft.find(u)
    v = uft.find(v)
    uft.union(u, v)

    let w = ps.len()
    ps.add(w)
    ts.add(i + 1)
    ps[rt[u]] = w
    ps[rt[v]] = w
    rt[u] = w
    rt[v] = w

  let pt = buildParentTable(ps)
  for i in 0..<q:
    let u = qs[i].u
    let v = qs[i].v
    let c = pt.lca(u, v)
    echo if c == -1: -1 else: ts[c]

main()


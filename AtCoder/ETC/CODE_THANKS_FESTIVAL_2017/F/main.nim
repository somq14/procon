import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import logging
import future

const INF* = int(1e18 + 373)

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

proc readInt1*(): int = readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#
const MOD = 10^9 + 7

proc bitwid(n: int): int =
  result = 0
  var n = n
  while n != 0:
    result += 1
    n = n shr 1

proc main() =
  let (n, k) = readInt2()
  let a = readSeq(n).map(parseInt)

  var x = newSeq2[int](17 + 1, 0)
  for i in 0..<n:
    x[a[i].bitwid()].add(a[i])

  var dp = new seq[int]
  var nextDp = new seq[int]
  dp[] = newSeq[int](1 shl 17)
  nextDp[] = newSeq[int](1 shl 17)

  dp[0] = 1
  for w in countdown(17, 1):
    for v in x[w]:
      for i in 0..<(1 shl w):
        nextDp[i] = (dp[i] + dp[i xor v]) mod MOD
      swap(dp, nextDp)

    nextDp[] = newSeq[int](1 shl (w - 1))
    if (k and (1 shl (w - 1))) != 0:
      for i in 0..<(1 shl (w - 1)):
        nextDp[i] = dp[1 shl (w - 1) + i]
    else:
      for i in 0..<(1 shl (w - 1)):
        nextDp[i] = dp[i]
    dp[] = newSeq[int](1 shl (w - 1))
    swap(dp, nextDp)

  echo dp[0]

main()


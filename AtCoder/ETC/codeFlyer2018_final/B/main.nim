import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import logging
import future

const INF* = int(1e18 + 373)

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

proc readInt1*(): int = readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#
proc buildCumTab(a: seq[int]): seq[int] =
  let n = a.len()
  var c = newSeq[int](n + 1)
  for i in 1..n:
    c[i] = c[i - 1] + a[i - 1]
  return c

# [s, t)
proc lookupCumTab(c: seq[int]; s, t: int): int = c[t] - c[s]


proc buildCumTab2(a: seq2[int]): seq2[int] =
  let h = a.len()
  let w = a[0].len()
  var c = newSeq2[int](h + 1, w + 1)
  for y in 1..h:
    for x in 1..w:
      c[y][x] = c[y - 1][x] + c[y][x - 1] - c[y - 1][x - 1] + a[y - 1][x - 1]
  return c

proc lookupCumTab2(c: seq2[int]; sy, sx, ty, tx: int): int =
  c[ty][tx] - c[sy][tx] - c[ty][sx] + c[sy][sx]

proc main() =
  let (n, q) = readInt2()
  let x = readSeq().map(parseInt)

  var cs = newSeq[int](q)
  var ds = newSeq[int](q)
  for i in 0..<q:
    (cs[i], ds[i]) = readInt2()

  let t = x.buildCumTab()
  for i in 0..<q:
    let c = cs[i]
    let d = ds[i]

    let s1 = x.lowerBound(c - d)
    let t1 = x.lowerBound(c - 1 + 1)
    let s2 = x.lowerBound(c + 1)
    let t2 = x.lowerBound(c + d + 1)
    #echo "c = ", c, " d = ", d
    #echo "s1 = ", s1, " t1 = ", t1
    #echo "s2 = ", s2, " t2 = ", t2

    var ans = 0
    ans += d * (n - (t2 - s1))
    ans += c * (t1 - s1) - t.lookupCumTab(s1, t1)
    ans += t.lookupCumTab(s2, t2) - c * (t2 - s2)
    echo ans

main()


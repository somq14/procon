GC_disable()
import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import logging
import future

const INF* = int(1e18 + 373)

proc readLine*(): string = stdin.readLine()
proc readSeq*(): seq[string] = readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = readLine().strip()
proc readInt1*(): int = readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])
type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))
type seq3*[T] = seq[seq[seq[T]]]
proc newSeq3*[T](n1, n2, n3: Natural): seq3[T] = newSeqWith(n1, newSeqWith(n2, newSeq[T](n3)))

#------------------------------------------------------------------------------#
#------------------------------------------------------------------------------#
const MOD = int(10^9+7)

type Op1[T] = proc(a: T, b: T): T
type Op2[T] = proc(a: T, b: T): T

proc repeatedSquares[T](x: T, n: Natural, e: T, f: Op2[T]): T =
    result = e
    var xx = f(x, e)
    var i = n
    while i > 0:
        if (i and 1) != 0:
            result = f(result, xx)
        xx = f(xx, xx)
        i = i shr 1

proc addM(a, b: Natural): Natural = (a + b) mod MOD
proc subM(a, b: Natural): Natural = (a - b + MOD) mod MOD
proc mulM(a, b: Natural): Natural = a * b mod MOD
proc powM(a, b: Natural): Natural = repeatedSquares[Natural](a, b, 1, `mulM`)
proc invM(a: Natural): Natural = a.powM(MOD - 2)
proc divM(a, b: Natural): Natural = a.mulM(invM(b))

var memoFactM: seq[Natural] = nil
var memoFactInvM: seq[Natural] = nil

proc buildFactTable(n: Natural) =
  memoFactM = newSeq[Natural](n + 1)
  memoFactM[0] = 1
  for i in 1..n:
    memoFactM[i] = memoFactM[i - 1].mulM(i)

  memoFactInvM = newSeq[Natural](n + 1)
  memoFactInvM[n] = invM(memoFactM[n])
  for i in countdown(n - 1, 0):
    memoFactInvM[i] = memoFactInvM[i + 1].mulM(i + 1)

buildFactTable(10^6)

proc factM(n: Natural): Natural = memoFactM[n]

proc factInvM(n: Natural): Natural = memoFactInvM[n]

proc combM(n, r: Natural): Natural =
  if r > n:
    return 0
  if r > n div 2:
    return combM(n, n - r)

  result = factM(n).mulM(factInvM(n - r)).mulM(factInvM(r))

proc multCombM(n, r: Natural): Natural = combM(n + r - 1, r - 1)

#------------------------------------------------------------------------------#
type Node = ref object
  op: char
  ll: Node
  rr: Node
  ind: int
  cache: int

proc initNode(): Node =
  result.new()
  result.op = 'a'
  result.ll = nil
  result.rr = nil

proc initNode(op: char; ll, rr: Node): Node =
  result.new()
  result.op = op
  result.ll = ll
  result.rr = rr
  result.ind = -1
  if op == '+':
    result.cache = ll.cache.addM(rr.cache)
  if op == '-':
    result.cache = ll.cache.subM(rr.cache)
  if op == '*':
    result.cache = ll.cache.mulM(rr.cache)

proc `$`(node: Node): string =
  if node.op == 'a':
    return "a(" & $node.cache & ')'
  else:
    return '(' & node.op & '(' & $node.cache & ')' & ' ' & $node.ll & ' ' & $node.rr & ')'

proc parseAddExpr(s: string; i: var int; a, aInd: seq[int]): Node
proc parseMulExpr(s: string; i: var int; a, aInd: seq[int]): Node
proc parseTermExpr(s: string; i: var int; a, aInd: seq[int]): Node

proc parseAddExpr(s: string; i: var int; a, aInd: seq[int]): Node =
  var res = parseMulExpr(s, i, a, aInd)
  while i < s.len() and (s[i] == '+' or s[i] == '-'):
    let op = s[i]
    i += 1
    res = initNode(op, res, parseMulExpr(s, i, a, aInd))
  return res

proc parseMulExpr(s: string; i: var int; a, aInd: seq[int]): Node =
  var res = parseTermExpr(s, i, a, aInd)
  while i < s.len() and s[i] == '*':
    i += 1
    res = initNode('*', res, parseTermExpr(s, i, a, aInd))
  return res

proc parseTermExpr(s: string; i: var int; a, aInd: seq[int]): Node =
  if s[i] == '(':
    i += 1
    let res = parseAddExpr(s, i, a, aInd)
    assert s[i] == ')'
    i += 1
    return res

  if s[i] == 'a':
    var res = initNode()
    res.ind = aInd[i]
    res.cache = a[aInd[i]]
    i += 1
    return res

  assert false

proc solve(node: Node; a: int; aa: var seq[int]) =
  if node.op == 'a':
    aa[node.ind] = a
    return

  if node.op == '*':
    solve(node.ll, a.mulM(node.rr.cache), aa)
    solve(node.rr, a.mulM(node.ll.cache), aa)
    return

  if node.op == '+':
    solve(node.ll, a, aa)
    solve(node.rr, a, aa)
    return

  if node.op == '-':
    solve(node.ll, a, aa)
    solve(node.rr, 0.subM(a), aa)
    return


proc main() =
  let s = readLine()
  let n = s.len()
  let q = readInt1()
  let a = readSeq().map(parseInt)
  var b = newSeq[int](q)
  var x = newSeq[int](q)
  for i in 0..<q:
    (b[i], x[i]) = readInt2()
    b[i] -= 1

  var aInd = newSeq[int](n)
  block:
    var cnt = 0
    for i in 0..<n:
      if s[i] == 'a':
        aInd[i] = cnt
        cnt += 1

  var i = 0
  let root = parseAddExpr(s, i, a, aInd)

  var aa = newSeq[int](a.len())
  solve(root, 1, aa)

  let bb = root.cache
  for i in 0..<q:
    echo bb.addM(aa[b[i]].mulM(x[i].subM(a[b[i]])))


main()

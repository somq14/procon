import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import logging
import future

const INF* = int(1e18 + 373)

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

proc readInt1*(): int = readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#

proc top[T](a: seq[T]): T = a[a.len() - 1]

proc subr(s: string): int =
  if s.len() == 0:
    return 0
  let n = s.len()

  var ans = 0
  var stack = newSeq[int](0)
  for i in 0..<n:
    case s[i]:
    of '(':
      stack.add(i)
    of ')':
      var elem = 0
      while stack.top() < 0:
        elem += 1
        discard stack.pop()
      discard stack.pop()
      ans += elem * (elem + 1) div 2
      stack.add(-1)
    else:
      discard

  ans += stack.len() * (stack.len() + 1) div 2
  return ans

proc main() =
  let s = stdin.readLine()
  let n = s.len()

  var deadParen = newSeq[int](0)
  var stack = newSeq[int](0)
  for i in 0..<n:
    case s[i]:
    of '(':
      stack.add(i)
    of ')':
      if stack.len() == 0:
        deadParen.add(i)
      else:
        discard stack.pop()
    else:
      discard

  while stack.len() > 0:
    deadParen.add(stack.pop())

  deadParen.add(-1)
  deadParen.add(n)
  deadParen.sort(cmp[int])

  var ans = 0
  for i in 0..<deadParen.len() - 1:
    ans += subr(s[(deadParen[i] + 1)..(deadParen[i + 1] - 1)])
  echo ans

main()


import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import logging
import future

const INF* = int(1e18 + 373)

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

proc readInt1*(): int = readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#
proc solve(h, w: int; a: seq[string]): bool =
  var table = newSeq[int](26)
  for i in 0..<h:
    for j in 0..<w:
      table[a[i][j].ord() - 'a'.ord()] += 1

  var count4 = 0
  var count2 = 0
  var count1 = 0
  for i in 0..<26:
    if table[i] == 0:
      continue

    count4 += table[i] div 4
    count2 += (table[i] mod 4) div 2
    count1 += (table[i] mod 2)

  let need4 = (h div 2) * (w div 2)
  if count4 < need4:
    return false

  count4 -= need4
  count2 += count4 * 2
  count4 = 0

  let need2 = (if h mod 2 == 1: w div 2 else: 0) + (if w mod 2 == 1: h div 2 else: 0)
  if count2 < need2:
    return false
  count2 -= need2
  count1 += count2 * 2
  count2 = 0

  return true


proc main() =
  let (h, w) = readInt2()

  var a = newSeq[string](h)
  for i in 0..<h:
    a[i] = stdin.readLine()

  echo if solve(h, w, a): "Yes" else : "No"

main()


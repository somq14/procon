import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import logging
import future

const INF* = int(1e18 + 373)

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

proc readInt1*(): int = readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#

proc main() =
  let (h, w, d) = readInt3()

  var s = newSeq2[char](h, w)

  for i in 0..<h:
    for j in 0..<w:
      let x = (i + j) mod (2 * d)
      let y = (i - j + w * 2 * d) mod (2 * d)

      if x < d and y < d:
        s[i][j] = 'B'
      elif x < d and y >= d:
        s[i][j] = 'Y'
      elif x >= d and y < d:
        s[i][j] = 'G'
      elif x >= d and y >= d:
        s[i][j] = 'R'

  for i in 0..<h:
    echo s[i].map(it => $it).join()

main()


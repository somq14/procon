import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import future

const INF* = int(1e18 + 373)

proc readLine*(): string =
  stdin.readLine()
proc readSeq*(): seq[string] =
  readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = readLine().strip()
proc readInt1*(): int =
  readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])
proc newSeqWith*[T](n: Natural; e: T): seq[T] =
  result = newSeq[T](n)
  for i in 0..<n:
    result[i] = e
type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] =
  newSeqWith(n1, newSeq[T](n2))
type seq3*[T] = seq[seq[seq[T]]]
proc newSeq3*[T](n1, n2, n3: Natural): seq3[T] =
  newSeqWith(n1, newSeqWith(n2, newSeq[T](n3)))

#------------------------------------------------------------------------------#

proc main() =
  let (n, m, k) = readInt3()
  let s = readLine()

  var g = newSeq2[int](n, 0)
  var neighborBlue = newSeq[bool](n)
  var neighborRed = newSeq[bool](n)
  for i in 0..<m:
    var (a, b) = readInt2()
    a -= 1
    b -= 1

    g[a].add(b)
    g[b].add(a)

    if s[a] == 'B':
      neighborBlue[b] = true
    if s[b] == 'B':
      neighborBlue[a] = true

    if s[a] == 'R':
      neighborRed[b] = true
    if s[b] == 'R':
      neighborRed[a] = true

  for v in 0..<n:
    if k mod 2 == 1:
      echo (if neighborBlue[v]: "First" else: "Second")
      continue

    if s[v] == 'R':
      echo "Second"
      continue

    var ans = false
    for u in g[v]:
      if not neighborRed[u]:
        ans = true
        break
    echo (if ans: "First" else: "Second")

main()


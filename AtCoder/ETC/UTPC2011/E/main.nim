import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import logging
import future

const INF* = int(1e18 + 373)

proc readLine*(): string =
  stdin.readLine()
proc readSeq*(): seq[string] =
  readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = readLine().strip()
proc readInt1*(): int =
  readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])
type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] =
  newSeqWith(n1, newSeq[T](n2))
type seq3*[T] = seq[seq[seq[T]]]
proc newSeq3*[T](n1, n2, n3: Natural): seq3[T] =
  newSeqWith(n1, newSeqWith(n2, newSeq[T](n3)))
type seq4*[T] = seq[seq[seq[seq[T]]]]
proc newSeq4*[T](n1, n2, n3, n4: Natural): seq4[T] =
  newSeqWith(n1, newSeqWith(n2, newSeqWith(n3, newSeq[T](n4))))

#------------------------------------------------------------------------------#
proc compare(p1, p2: (int, int)): int =
  if p1[1] == p2[1]:
    return p1[0] - p2[0]
  return p1[1] - p2[1]

proc main() =
  let n = readInt1()
  var x = newSeq[(int, int)](n)
  for i in 0..<n:
    (x[i][0], x[i][1]) = readInt2()
  x.sort(compare)

  var dp = newSeq2[int](n + 1, n + 1)
  dp[0].fill(INF)
  dp[0][0] = 0
  for i in 1..n:
    for j in 1..n:
      dp[i][j] = dp[i - 1][j]
      if dp[i - 1][j - 1] + x[i - 1][0] <= x[i - 1][1]:
        dp[i][j] = min(dp[i][j], dp[i - 1][j - 1] + x[i - 1][0])

  var ans = -1
  for i in countdown(n, 0):
    if dp[n][i] != INF:
      ans = i
      break
  echo ans

main()


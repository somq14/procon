import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import logging
import future

const INF* = int(1e18 + 373)

proc readLine*(): string =
  stdin.readLine()
proc readSeq*(): seq[string] =
  readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = readLine().strip()
proc readInt1*(): int =
  readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])
type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] =
  newSeqWith(n1, newSeq[T](n2))
type seq3*[T] = seq[seq[seq[T]]]
proc newSeq3*[T](n1, n2, n3: Natural): seq3[T] =
  newSeqWith(n1, newSeqWith(n2, newSeq[T](n3)))

#------------------------------------------------------------------------------#
proc valid(s: string): bool =
  let n = s.len()

  var found = false
  for i in 0..<n - 2:
    if s[i] == 'i' and s[i + 1] == 'w' and s[i + 2] == 'i':
      found = true
      break

  if not found:
    return false

  for i in 0..n div 2:
    let a = s[i]
    let b = s[n - 1 - i]
    case a:
    of 'i':
      if b != 'i':
        return false
    of 'w':
      if b != 'w':
        return false
    of '[':
      if b != ']':
        return false
    of ']':
      if b != '[':
        return false
    of '(':
      if b != ')':
        return false
    of ')':
      if b != '(':
        return false
    of '{':
      if b != '}':
        return false
    of '}':
      if b != '{':
        return false
    else:
      assert false

  return true

proc main() =
  let a = readLine()
  let n = a.len()
  var ans = 3
  for s in 0..<(1 shl n):
    var t = ""
    for i in 0..<n:
      if (s and (1 shl i)) != 0:
        t.add(a[i])
    if t.valid():
      ans = max(ans, t.len())

  echo ans

main()


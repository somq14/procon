import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import logging
import future

const INF* = int(1e18 + 373)

proc readLine*(): string =
  stdin.readLine()
proc readSeq*(): seq[string] =
  readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = readLine().strip()
proc readInt1*(): int =
  readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])
type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] =
  newSeqWith(n1, newSeq[T](n2))
type seq3*[T] = seq[seq[seq[T]]]
proc newSeq3*[T](n1, n2, n3: Natural): seq3[T] =
  newSeqWith(n1, newSeqWith(n2, newSeq[T](n3)))
type seq4*[T] = seq[seq[seq[seq[T]]]]
proc newSeq4*[T](n1, n2, n3, n4: Natural): seq4[T] =
  newSeqWith(n1, newSeqWith(n2, newSeqWith(n3, newSeq[T](n4))))

#------------------------------------------------------------------------------#
type State =
  tuple [ y, x, d, m: int ]

const dy = [  0,  0,  1, -1 ]
const dx = [  1, -1,  0,  0 ]
const R = 0
const L = 1
const D = 2
const U = 3

proc main() =
  let (h, w) = readInt2()
  var s = newSeq[string](h)
  for i in 0..<h:
    s[i] = readLine()

  var visited = newSeq4[bool](h, w, 4, 16)
  var q = initQueue[State]()
  q.enqueue((0, 0, 0, 0))
  visited[0][0][0][0] = true
  while q.len() > 0:
    var (y, x, d, m) = q.dequeue()

    case s[y][x]:
    of '<':
      d = L
    of '>':
      d = R
    of '^':
      d = U
    of 'v':
      d = D
    of '_':
      if m == 0:
        d = R
      else:
        d = L
    of '|':
      if m == 0:
        d = D
      else:
        d = U
    of '?':
      discard
    of '.':
      discard
    of '@':
      discard
    of '0':
      m = 0
    of '1':
      m = 1
    of '2':
      m = 2
    of '3':
      m = 3
    of '4':
      m = 4
    of '5':
      m = 5
    of '6':
      m = 6
    of '7':
      m = 7
    of '8':
      m = 8
    of '9':
      m = 9
    of '+':
      m = (m + 1) mod 16
    of '-':
      m = (m - 1 + 16) mod 16
    else:
      discard

    if s[y][x] != '?':
      let ny = (y + dy[d] + h + h) mod h
      let nx = (x + dx[d] + w + w) mod w
      if not visited[ny][nx][d][m]:
        visited[ny][nx][d][m] = true
        q.enqueue((ny, nx, d, m))
      continue

    for k in 0..<4:
      let ny = (y + dy[k] + h + h) mod h
      let nx = (x + dx[k] + w + w) mod w
      if not visited[ny][nx][k][m]:
        visited[ny][nx][k][m] = true
        q.enqueue((ny, nx, k, m))

  var ans = false
  for y in 0..<h:
    for x in 0..<w:
      if s[y][x] != '@':
        continue
      for d in 0..<4:
        for m in 0..<16:
          if visited[y][x][d][m]:
            ans = true

  echo if ans: "YES" else: "NO"

main()


import algorithm
import future
import macros
import math
import queues
import sequtils
import sets
import strutils
import tables

const INF* = int(1e18 + 373)

proc readLine*(): string =
  stdin.readLine().strip()
proc readSeq*(): seq[string] =
  readLine().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeqWith(n, readLine())
proc readIntSeq*(): seq[int] =
  result = readSeq().map(parseInt)
proc readIntSeq*(n: Natural): seq[int] =
  result = readSeq(n).map(parseInt)
proc readInt1*(): int =
  readLine().parseInt()
proc readInt2*(): (int, int) =
  let a = readIntSeq(); return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readIntSeq(); return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readIntSeq(); return (a[0], a[1], a[2], a[3])

proc newSeqOf*[T](n: Natural; e: T): seq[T] =
  result = newSeq[T](n)
  result.fill(e)
proc newSeq*[T](n: Natural; e: T): seq[T] =
  newSeqOf[T](n, e)
type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] =
  newSeqOf(n1, newSeq[T](n2))
proc newSeq2*[T](n1, n2: Natural; e: T): seq2[T] =
  newSeqOf(n1, newSeqOf(n2, e))
type seq3*[T] = seq[seq[seq[T]]]
proc newSeq3*[T](n1, n2, n3: Natural): seq3[T] =
  newSeqOf(n1, newSeqOf(n2, newSeq[T](n3)))
proc newSeq3*[T](n1, n2, n3: Natural; e: T): seq3[T] =
  newSeqOf(n1, newSeqOf(n2, newSeqOf(n3, e)))

when defined(ENABLE_DEBUG_MACRO):
  macro debug*(args: varargs[untyped]): untyped =
    result = nnkStmtList.newTree()
    for i in 0..<args.len():
      let par1 = newIdentNode("stderr")
      let par2 = newLit(args[i].repr)
      let par3 = newLit(" = ")
      let par4 = args[i]
      result.add(newCall("write", par1, par2, par3, par4))
      if i + 1 < args.len():
        result.add(newCall("write", newIdentNode("stderr"), newLit(", ")))
    result.add(newCall("writeLine", newIdentNode("stderr"), newLit("")))
else:
  macro debug*(args: varargs[untyped]): untyped =
    result = nnkDiscardStmt.newTree(newLit(nil))

#------------------------------------------------------------------------------#
#------------------------------------------------------------------------------#
type PriorityQueue*[T] = object
  a: seq[T]
  n: Natural
  cmp: (a: T, b: T) -> bool

proc initPriorityQueue*[T](cmp: (a: T, b: T) -> bool): PriorityQueue[T] =
  result.a = newSeq[T](1)
  result.n = Natural(0)
  result.cmp = cmp

proc len*[T](this: PriorityQueue[T]): Natural =
  this.n

proc enqueue*[T](this: var PriorityQueue[T], e: T) =
  this.n += 1
  this.a.add(e)
  var i = this.a.len() - 1
  while i > 1 and this.cmp(this.a[i], this.a[i div 2]):
    swap(this.a[i], this.a[i div 2])
    i = i div 2

proc dequeue*[T](this: var PriorityQueue[T]): T =
  this.n -= 1
  result = this.a[1]
  this.a[1] = this.a.pop()
  var i = 1
  while i < this.a.len():
    var j = i
    if i * 2 < this.a.len() and this.cmp(this.a[i * 2], this.a[j]):
      j = i * 2
    if i * 2 + 1 < this.a.len() and this.cmp(this.a[i * 2 + 1], this.a[j]):
      j = i * 2 + 1
    if i == j:
      break
    swap(this.a[i], this.a[j])
    i = j

proc front*[T](this: PriorityQueue[T]): T =
  this.a[1]

#------------------------------------------------------------------------------#
#------------------------------------------------------------------------------#
type Range = tuple [ s, t: int ]

proc main() =
  let (n, m) = readInt2()
  let a = readIntSeq()
  var r = newSeq[Range](m)
  for i in 0..<m:
    let (ll, rr) = readInt2()
    r[i] = (ll - 1, rr)
  r.sort(cmp[Range])

  debug(r)

  var x = newSeq[int](n)
  block:
    var q = initPriorityQueue[int]((i: int, j: int) => r[i].s < r[j].s)
    var j = 0
    for i in 0..<n:
      while j < m and r[j].s <= i:
        q.enqueue(j)
        j += 1

      while q.len() > 0 and r[q.front()].t <= i:
        discard q.dequeue()

      debug(i, j, q.len())
      if q.len() == 0:
        x[i] = i - 1
        continue

      x[i] = r[q.front()].s - 1

  debug(x)

  var dp = newSeq[int](n + 1)
  dp[0] = 0
  for i in 1..n:
    var opt = dp[i - 1]
    opt = max(opt, dp[x[i - 1] + 1] + a[i - 1])
    dp[i] = opt

  echo dp[n]

main()

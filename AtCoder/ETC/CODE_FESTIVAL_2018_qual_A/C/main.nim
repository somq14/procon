import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import logging
import future

const INF* = int(1e18 + 373)

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

proc readInt1*(): int = readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

type seq3*[T] = seq[seq[seq[T]]]
proc newSeq3*[T](n1, n2, n3: Natural): seq3[T] = newSeqWith(n1, newSeqWith(n2, newSeq[T](n3)))

#------------------------------------------------------------------------------#
proc bitwidth(v: int): int =
  var lb = -1
  var ub = 64
  # (lb, ub]
  while ub - lb > 1:
    let mid = lb + (ub - lb) div 2
    if v shr mid == 0:
      ub = mid
    else:
      lb = mid
  return ub

const MOD = 10^9 + 7

proc solve(n, k: int; a: seq[int]): int =
  let maxk = min(k, a.sum() + 1)

  # 0 を出してはいけないルールでの組み合わせ
  var dp0 = newSeq2[int](n + 1, maxk + 1)
  dp0[0][0] = 1
  for i in 1..n:
    for j in 0..maxk:
      var opt = 0
      for ll in 0..<a[i - 1]:
        if j - ll >= 0:
          opt = (opt + dp0[i - 1][j - ll]) mod MOD
      dp0[i][j] = opt

  var dp1 = newSeq2[int](n + 1, maxk + 1)
  dp1[0][0] = 1
  for i in 1..n:
    for j in 0..maxk:
      var opt = 0
      for ll in 0..a[i - 1]:
        if j - ll >= 0:
          opt = (opt + dp1[i - 1][j - ll]) mod MOD

      for ll in (a[i - 1] + 1)..j:
        opt = (opt + dp0[i - 1][j - ll]) mod MOD
      dp1[i][j] = opt

  return dp1[n][maxk]


proc main() =
  let (n, k) = readInt2()
  let a = readSeq().map(parseInt)

  var b = newSeq[int](n)
  for i in 0..<n:
    b[i] = a[i].bitwidth()

  echo solve(n, k, b)

main()


import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import future

const INF* = int(1e18 + 373)

proc readLine*(): string =
  stdin.readLine()
proc readSeq*(): seq[string] =
  readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = readLine().strip()
proc readInt1*(): int =
  readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])
proc newSeqWith*[T](n: Natural; e: T): seq[T] =
  result = newSeq[T](n)
  for i in 0..<n:
    result[i] = e
type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] =
  newSeqWith(n1, newSeq[T](n2))
type seq3*[T] = seq[seq[seq[T]]]
proc newSeq3*[T](n1, n2, n3: Natural): seq3[T] =
  newSeqWith(n1, newSeqWith(n2, newSeq[T](n3)))

#------------------------------------------------------------------------------#

type Pos = tuple [ x, y: int ]

proc solve(s: string; g: Pos, dir: Table[char, Pos]): bool =
  var cur: Pos = (0, 0)
  for i in 0..<s.len():
    if cur == g:
      return true
    cur.x += dir[s[i]].x
    cur.y += dir[s[i]].y
    if cur == g:
      return true

  return false

proc main() =
  let s = readLine()
  let (gx, gy) = readInt2()

  let ll = (-1, 0)
  let rr = (+1, 0)
  let uu = (0, -1)
  let dd = (0, +1)
  let dir = [ ll, rr, uu, dd ]

  var ans = false
  var t = initTable[char, Pos]()
  block loop:
    for i0 in 0..<4:
      for i1 in 0..<4:
        for i2 in 0..<4:
          for i3 in 0..<4:
            if i0 == i1 or i0 == i2 or i0 == i3:
              continue
            if i1 == i2 or i1 == i3:
              continue
            if i2 == i3:
              continue

            t['W'] = dir[i0]
            t['X'] = dir[i1]
            t['Y'] = dir[i2]
            t['Z'] = dir[i3]

            if solve(s, (gx, gy), t):
              ans = true
              break loop

  echo if ans: "Yes" else: "No"

main()


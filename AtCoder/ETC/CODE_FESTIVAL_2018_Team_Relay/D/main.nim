import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import future

const INF* = int(1e18 + 373)

proc readLine*(): string =
  stdin.readLine()
proc readSeq*(): seq[string] =
  readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = readLine().strip()
proc readInt1*(): int =
  readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])
proc newSeqWith*[T](n: Natural; e: T): seq[T] =
  result = newSeq[T](n)
  for i in 0..<n:
    result[i] = e
type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] =
  newSeqWith(n1, newSeq[T](n2))
type seq3*[T] = seq[seq[seq[T]]]
proc newSeq3*[T](n1, n2, n3: Natural): seq3[T] =
  newSeqWith(n1, newSeqWith(n2, newSeq[T](n3)))

#------------------------------------------------------------------------------#
proc intersect(s1, t1, s2, t2: float): (float, float) =
  if t1 <= s2 or t2 <= s1:
    (0.0, 0.0)
  else:
    (max(s1, s2), min(t1, t2))

proc solve(n: int; x, w: seq[float]; k: float): (float, float) =
  var s = x[0]
  var t = x[n - 1]
  for i in 0..<n:
    (s, t) = intersect(s, t, x[i] - k / w[i], x[i] + k / w[i])
  return (s, t)

proc main() =
  let n = readInt1()

  var x = newSeq[float](n)
  var w = newSeq[float](n)
  for i in 0..<n:
    let (xx, ww) = readInt2()
    x[i] = xx.float()
    w[i] = ww.float()

  if n == 1:
    echo x[0]
    return

  var lb = 0.0
  var ub = 1e15
  var opt = (0.0, 0.0)
  for i in 0..<2048:
    let mid = (ub + lb) / 2
    let sol = solve(n, x, w, mid)
    if sol[1] > sol[0]:
      opt = sol
      ub = mid
    else:
      lb = mid

  echo opt[0]

main()


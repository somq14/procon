import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import future

const INF* = int(1e18 + 373)

proc readLine*(): string =
  stdin.readLine()
proc readSeq*(): seq[string] =
  readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = readLine().strip()
proc readInt1*(): int =
  readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])
proc newSeqWith*[T](n: Natural; e: T): seq[T] =
  result = newSeq[T](n)
  for i in 0..<n:
    result[i] = e
type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] =
  newSeqWith(n1, newSeq[T](n2))
type seq3*[T] = seq[seq[seq[T]]]
proc newSeq3*[T](n1, n2, n3: Natural): seq3[T] =
  newSeqWith(n1, newSeqWith(n2, newSeq[T](n3)))

#------------------------------------------------------------------------------#

proc main() =
  let (n, k) = readInt2()
  let t = readSeq(n).map(parseInt)

  var dp = newSeq2[float](n + 1, k)
  dp[n].fill(0.0)

  for i in countdown(n - 1, 0):
    for j in countdown(k - 1, 0):
      let p = 1.0 / float(k - j)
      let tt = (j + t[i]) mod k

      var exp = 0.0
      if j + 1 < k:
        exp += (1.0 - p) * float(1.0 + dp[i][j + 1])

      let q = float(tt) / float(k)
      if i + 1 < n:
        exp += p * (1.0 - q) * (float(t[i]) + dp[i + 1][tt])
        exp += p * q * (float(t[i]) + float(k - tt) + dp[i + 1][0])
      else:
        exp += p * float(t[i])

      dp[i][j] = exp

  echo dp[0][0]

main()


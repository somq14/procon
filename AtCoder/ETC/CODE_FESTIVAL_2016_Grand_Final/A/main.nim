import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import logging
import future

const INF* = int(1e18 + 373)

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

proc readInt1*(): int = readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#
const MOD = 10^9 + 7

proc main() =
  let n = readInt1()
  let a = readSeq(n).map(parseInt)
  let b = readSeq(n).map(parseInt)

  var xs = newSeq[(int, char)](0)
  for i in 0..<n:
    xs.add((a[i], 'C'))
  for i in 0..<n:
    xs.add((b[i], 'P'))
  xs.sort(cmp[(int, char)])

  var ans = 1
  var cable = 0
  for i in 0..<(n + n):
    if cable > 0 and xs[i][1] == 'C':
      ans = ans * abs(cable) mod MOD
    if cable < 0 and xs[i][1] == 'P':
      ans = ans * abs(cable) mod MOD

    if xs[i][1] == 'P':
      cable += 1
    if xs[i][1] == 'C':
      cable -= 1

  assert cable == 0
  echo ans

main()


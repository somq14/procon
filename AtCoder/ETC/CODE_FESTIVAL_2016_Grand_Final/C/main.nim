import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import logging
import future

const INF* = int(1e18 + 373)

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

proc readInt1*(): int = readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#
proc countTrailingZeros(x: int): int =
  if x == 0:
    return -1

  var mask = 1
  var count = 0
  while (mask and x) == 0:
    mask = mask shl 1
    count += 1

  return count

proc solve(n: int; a: seq[int]): int =
  var op = newSeq[bool](32)
  for i in 0..<n:
    op[countTrailingZeros(a[i])] = true

  var sum = 0
  for i in 0..<n:
    sum = sum xor a[i]

  var ans = 0
  for i in countdown(31, 0):
    if (sum and (1 shl i)) == 0:
      continue
    if not op[i]:
      return -1
    sum = sum xor ((1 shl (i + 1)) - 1)
    ans += 1

  return ans

proc main() =
  let n = readInt1()
  let a = readSeq(n).map(parseInt)
  echo solve(n, a)

main()


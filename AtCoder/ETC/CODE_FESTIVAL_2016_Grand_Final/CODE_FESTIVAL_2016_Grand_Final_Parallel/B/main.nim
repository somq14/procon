import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import logging
import future

const INF* = int(1e18 + 373)

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

proc readInt1*(): int = readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#
type Pos = tuple [ x, y: float ]

proc `+`(p1, p2: Pos): Pos = (p1.x + p2.x, p1.y + p2.y)
proc `-`(p1, p2: Pos): Pos = (p1.x - p2.x, p1.y - p2.y)
proc `*`(k: float, p: Pos): Pos = (k * p.x, k * p.y)
proc dot(p1, p2: Pos): float = p1.x * p2.x + p1.y * p2.y
proc det(p1, p2: Pos): float = p1.x * p2.y - p1.y * p2.x
proc abs(p: Pos): float = sqrt(p.x * p.x + p.y * p.y)
proc arg(p: Pos): float = arctan2(p.y, p.x)
#------------------------------------------------------------------------------#

proc intersection(p1, p2, q1, q2: Pos): Pos =
  let dp = p2 - p1
  let dq = q2 - q1
  let t: float = dq.det(q1 - p1) / dq.det(dp)
  return p1 + t * dp

proc main() =
  let (x1, y1) = readInt2()
  let (x2, y2) = readInt2()
  let (x3, y3) = readInt2()

  let a = (x1.float(), y1.float())
  let b = (x2.float(), y2.float())
  let c = (x3.float(), y3.float())

  let thA = ((b - a).arg() + (c - a).arg()) / 2.0
  let thB = ((c - b).arg() + (a - b).arg()) / 2.0
  let thC = ((a - c).arg() + (b - c).arg()) / 2.0
  let va: Pos = (cos(thA), sin(thA))
  let vb: Pos = (cos(thB), sin(thB))
  let vc: Pos = (cos(thC), sin(thC))

  let center = intersection(a, a + va, b, b + vb)
  let grand = intersection(a, b, center, center + (-(b - a).y, (b - a).x))

  let r = (center - grand).abs()
  let d = max(@[ (a - b).abs(), (b - c).abs(), (c - a).abs() ])
  let ans = r * d / (2 * r + d)
  echo ans


main()


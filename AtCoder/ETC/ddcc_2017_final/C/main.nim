import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import future

const INF* = int(1e18 + 373)

proc readLine*(): string =
  stdin.readLine()
proc readSeq*(): seq[string] =
  readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = readLine().strip()
proc readInt1*(): int =
  readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])
proc newSeqWith*[T](n: Natural; e: T): seq[T] =
  result = newSeq[T](n)
  for i in 0..<n:
    result[i] = e
type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] =
  newSeqWith(n1, newSeq[T](n2))
type seq3*[T] = seq[seq[seq[T]]]
proc newSeq3*[T](n1, n2, n3: Natural): seq3[T] =
  newSeqWith(n1, newSeqWith(n2, newSeq[T](n3)))

#------------------------------------------------------------------------------#
type Edge = tuple [ src, dst, cost: int ]

proc detectCycleHelper(g: seq2[Edge]; v, s: int; visited: var seq[bool]; cycle: var seq[Edge]): bool =
  if v == s:
    return true

  if visited[v]:
    return false

  visited[v] = true
  for e in g[v]:
    if detectCycleHelper(g, e.dst, s, visited, cycle):
      cycle.add(e)
      return true

  return false

proc detectCycle(g: seq2[Edge]; e: Edge): seq[Edge] =
  let n = g.len()
  var cycle = newSeq[Edge](0)

  var visited = newSeq[bool](n)
  visited[e.src] = true
  discard detectCycleHelper(g, e.dst, e.src, visited, cycle)
  cycle.add(e)
  cycle.reverse()
  return cycle

proc findBadCycle(g: seq2[Edge]): seq[Edge] =
  let n = g.len()

  var fixedEdge = initSet[Edge]()
  for v in 0..<n:
    for e in g[v]:
      if e in fixedEdge:
        continue

      let cycle = detectCycle(g, e)

      var sum = 0
      for e in cycle:
        sum += e.cost

      if sum != 0:
        return cycle

      for e in cycle:
        fixedEdge.incl(e)

  return nil

proc dfs(g: seq2[Edge]; v, s: int; a: var seq[int]; visited: var seq[bool]): bool =
  if visited[v]:
    return true

  a[v] = s
  visited[v] = true

  for e in g[v]:
    if not dfs(g, e.dst, s + e.cost, a, visited):
      return false
    if a[v] + e.cost != a[e.dst]:
      return false

  return true

proc eval(g: seq2[Edge]): bool =
  let n = g.len()

  var visited = newSeq[bool](n)
  var a = newSeq[int](n)
  a.fill(-1)

  return dfs(g, 0, 0, a, visited)

proc solve(g: seq2[Edge]): bool =
  let n = g.len()

  let cycle = findBadCycle(g)
  if cycle == nil:
    return true

  var sum = 0
  for e in cycle:
    sum += e.cost

  for e in cycle:
    var gCopy = g
    for ee in gCopy[e.src].mitems():
      if ee == e:
        ee.cost -= sum

    if eval(gCopy):
      return true

  return false

proc main() =
  let (n, m) = readInt2()
  var g = newSeq2[Edge](n, 0)
  for i in 0..<m:
    let (a, b, c) = readInt3()
    g[a - 1].add((a - 1, b - 1, c))

  echo if solve(g): "Yes" else: "No"

main()


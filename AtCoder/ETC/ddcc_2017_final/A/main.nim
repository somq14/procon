import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import future

const INF* = int(1e18 + 373)

proc readLine*(): string =
  stdin.readLine()
proc readSeq*(): seq[string] =
  readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = readLine().strip()
proc readInt1*(): int =
  readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])
proc newSeqWith*[T](n: Natural; e: T): seq[T] =
  result = newSeq[T](n)
  for i in 0..<n:
    result[i] = e
type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] =
  newSeqWith(n1, newSeq[T](n2))
type seq3*[T] = seq[seq[seq[T]]]
proc newSeq3*[T](n1, n2, n3: Natural): seq3[T] =
  newSeqWith(n1, newSeqWith(n2, newSeq[T](n3)))

#------------------------------------------------------------------------------#
proc inside(x, y, r, cy, cx: int): bool =
  (y - cy) * (y - cy) + (x - cx) * (x - cx) <= r * r

proc solve(r, k: int): int =
  var ans = 0

  let m = r div k
  let r = r * 2
  let k = k * 2

  for i in 0..m:
    for j in 0..m:
      let y = i * k
      let x = j * k
      if inside(y, x, r div 2, (k * m) div 2, (k * m) div 2) and
        inside(y + k, x, r div 2, (k * m) div 2, (k * m) div 2) and
        inside(y, x + k, r div 2, (k * m) div 2, (k * m) div 2) and
        inside(y + k, x + k, r div 2, (k * m) div 2, (k * m) div 2):
        ans += 1

  return ans

proc main() =
  let k = readInt1()
  echo solve(200, k), " ", solve(300, k)

main()


import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import logging
import future

const INF* = int(1e18 + 373)

proc readLine*(): string = stdin.readLine()
proc readSeq*(): seq[string] = readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = readLine().strip()
proc readInt1*(): int = readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])
type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] =
  newSeqWith(n1, newSeq[T](n2))
type seq3*[T] = seq[seq[seq[T]]]
proc newSeq3*[T](n1, n2, n3: Natural): seq3[T] =
  newSeqWith(n1, newSeqWith(n2, newSeq[T](n3)))

#------------------------------------------------------------------------------#
GC_disable()

proc solve(g: seq2[int]; v, d: int; visited: var seq[bool]): int =
  visited[v] = true
  var ans = d
  for u in g[v]:
    if visited[u]:
      continue
    ans = max(ans, solve(g, u, d + 1, visited))
  visited[v] = false
  return ans

proc main() =
  let m = readInt1()
  var es = newSeq[(int, int)](m)
  var n = 0
  for i in 0..<m:
    let (u, v) = readInt2()
    n = max(n, u)
    n = max(n, v)
    es.add((u - 1, v - 1))

  var g = newSeq2[int](n, 0)
  for e in es:
    g[e[0]].add(e[1])
    g[e[1]].add(e[0])

  var ans = 1
  for v in 0..<n:
    var visited = newSeq[bool](n)
    ans = max(ans, solve(g, v, 1, visited))
  echo ans

main()


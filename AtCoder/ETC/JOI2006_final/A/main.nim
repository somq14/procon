import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import logging
import future

const INF* = int(1e18 + 373)

proc readLine*(): string = stdin.readLine()
proc readSeq*(): seq[string] = readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = readLine().strip()
proc readInt1*(): int = readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])
type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] =
  newSeqWith(n1, newSeq[T](n2))
type seq3*[T] = seq[seq[seq[T]]]
proc newSeq3*[T](n1, n2, n3: Natural): seq3[T] =
  newSeqWith(n1, newSeqWith(n2, newSeq[T](n3)))

#------------------------------------------------------------------------------#

proc compare(p1, p2: (int, int)): int =
  if p1[0] == p2[0]:
    return p1[1] - p2[1]
  return p2[0] - p1[0]

proc main() =
  let (n, m) = readInt2()
  var a = newSeq2[int](n, 0)
  for i in 0..<n:
    a[i] = readSeq().map(parseInt)

  var ans = newSeq[(int, int)](0)
  for j in 0..<m:
    var cnt = 0
    for i in 0..<n:
      if a[i][j] == 1:
        cnt += 1
    ans.add((cnt, j + 1))

  ans.sort(compare)

  echo ans.map(it => $it[1]).join(" ")

main()


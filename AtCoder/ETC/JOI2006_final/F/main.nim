import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import logging
import future

const INF* = int(1e18 + 373)

proc readLine*(): string = stdin.readLine()
proc readSeq*(): seq[string] = readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = readLine().strip()
proc readInt1*(): int = readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])
type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] =
  newSeqWith(n1, newSeq[T](n2))
type seq3*[T] = seq[seq[seq[T]]]
proc newSeq3*[T](n1, n2, n3: Natural): seq3[T] =
  newSeqWith(n1, newSeqWith(n2, newSeq[T](n3)))

#------------------------------------------------------------------------------#
type Event = tuple [ y, s, t: int ]

const N = 10010
proc main() =
  let (h, w) = (N, N)
  let (n, r) = readInt2()

  var es0 = newSeq[Event](0)
  var es1 = newSeq[Event](0)
  for i in 0..<n:
    var (x1, y1, x2, y2) = readInt4()
    x1 += 1
    x2 += 1
    y1 += 1
    y2 += 1

    es0.add((min(y1, y2), min(x1, x2), max(x1, x2)))
    es1.add((max(y1, y2), min(x1, x2), max(x1, x2)))

  es0.sort(cmp[Event])
  es0.reverse()
  es1.sort(cmp[Event])
  es1.reverse()

  var a: ref seq[int]
  a.new()
  a[] = newSeq[int](w)

  var aPrev: ref seq[int]
  aPrev.new()
  aPrev[] = newSeq[int](w)

  var ss = 0
  var ll = 0

  for y in 0..<h:
    for x in 0..<w:
      a[x] = aPrev[x]

    while es0.len() > 0 and es0[es0.len() - 1].y == y:
      let e = es0.pop()
      for x in e.s..<e.t:
        a[x] += 1

    while es1.len() > 0 and es1[es1.len() - 1].y == y:
      let e = es1.pop()
      for x in e.s..<e.t:
        a[x] -= 1

    for x in 0..<w - 1:
      if a[x] > 0:
        ss += 1
      if (a[x] == 0 and a[x + 1] > 0) or (a[x] > 0 and a[x + 1] == 0):
        ll += 1
      if (aPrev[x] == 0 and a[x] > 0) or (aPrev[x] > 0 and a[x] == 0):
        ll += 1

    swap(a, aPrev)

  if r == 1:
    echo ss
  else:
    echo ss
    echo ll

main()


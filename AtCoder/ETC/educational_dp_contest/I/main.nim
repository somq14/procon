import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import future

const INF* = int(1e18 + 373)

proc readLine*(): string =
  stdin.readLine()
proc readSeq*(): seq[string] =
  readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = readLine().strip()
proc readInt1*(): int =
  readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])
proc newSeqWith*[T](n: Natural; e: T): seq[T] =
  result = newSeq[T](n)
  for i in 0..<n:
    result[i] = e
type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] =
  newSeqWith(n1, newSeq[T](n2))
type seq3*[T] = seq[seq[seq[T]]]
proc newSeq3*[T](n1, n2, n3: Natural): seq3[T] =
  newSeqWith(n1, newSeqWith(n2, newSeq[T](n3)))

#------------------------------------------------------------------------------#
const MOD = int(10^9 + 7)

proc main() =
  let n = readInt1()
  let ps = readSeq()
  var p = newSeq[float](n)
  for i in 0..<n:
    p[i] = ps[i].parseFloat()

  var dp = newSeq2[float](n + 1, n + 1)
  dp[n].fill(0)
  dp[n][0] = 1.0
  for i in countdown(n - 1, 0):
    for j in 0..n:
      var prob = 0.0
      if j > 0:
        prob += p[i] * dp[i + 1][j - 1]
      prob += (1 - p[i]) * dp[i + 1][j]
      dp[i][j] = prob

  var ans = 0.0
  for i in 0..n:
    if i > n - i:
      ans += dp[0][i]
  echo ans

main()


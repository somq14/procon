import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import future

const INF* = int(1e18 + 373)

proc readLine*(): string =
  stdin.readLine()
proc readSeq*(): seq[string] =
  readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = readLine().strip()
proc readInt1*(): int =
  readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])
proc newSeqWith*[T](n: Natural; e: T): seq[T] =
  result = newSeq[T](n)
  for i in 0..<n:
    result[i] = e
type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] =
  newSeqWith(n1, newSeq[T](n2))
type seq3*[T] = seq[seq[seq[T]]]
proc newSeq3*[T](n1, n2, n3: Natural): seq3[T] =
  newSeqWith(n1, newSeqWith(n2, newSeq[T](n3)))

#------------------------------------------------------------------------------#
const MOD = int(10^9 + 7)

proc solve(n, i0, i1, i2, i3: int; dp: var seq3[float]): float =
  if dp[i1][i2][i3] >= 0.0:
    return dp[i1][i2][i3]

  var exp = 0.0
  let p0 = i0.float() / n.float()
  exp += p0 / (1.0 - p0)

  let p1 = i1.float() / (i1 + i2 + i3).float()
  let p2 = i2.float() / (i1 + i2 + i3).float()
  let p3 = i3.float() / (i1 + i2 + i3).float()
  if i1 > 0:
    exp += p1 * (1.0 + solve(n, i0 + 1, i1 - 1, i2, i3, dp))
  if i2 > 0:
    exp += p2 * (1.0 + solve(n, i0, i1 + 1, i2 - 1, i3, dp))
  if i3 > 0:
    exp += p3 * (1.0 + solve(n, i0, i1, i2 + 1, i3 - 1, dp))
  dp[i1][i2][i3] = exp
  return exp

proc main() =
  let n = readInt1()
  let a = readSeq().map(parseInt)
  var a1 = 0
  var a2 = 0
  var a3 = 0
  for i in 0..<n:
    if a[i] == 1:
      a1 += 1
    if a[i] == 2:
      a2 += 1
    if a[i] == 3:
      a3 += 1

  var dp = newSeq3[float](n + 1, n + 1, n + 1)
  for i1 in 0..n:
    for i2 in 0..n:
      for i3 in 0..n:
        dp[i1][i2][i3] = -1.0
  dp[0][0][0] = 0
  echo solve(n, 0, a1, a2, a3, dp)

main()


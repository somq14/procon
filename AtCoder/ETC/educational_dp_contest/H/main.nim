import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import future

const INF* = int(1e18 + 373)

proc readLine*(): string =
  stdin.readLine()
proc readSeq*(): seq[string] =
  readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = readLine().strip()
proc readInt1*(): int =
  readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])
proc newSeqWith*[T](n: Natural; e: T): seq[T] =
  result = newSeq[T](n)
  for i in 0..<n:
    result[i] = e
type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] =
  newSeqWith(n1, newSeq[T](n2))
type seq3*[T] = seq[seq[seq[T]]]
proc newSeq3*[T](n1, n2, n3: Natural): seq3[T] =
  newSeqWith(n1, newSeqWith(n2, newSeq[T](n3)))

#------------------------------------------------------------------------------#
const MOD = int(10^9 + 7)

proc main() =
  let (h, w) = readInt2()
  var a = newSeq[string](h)
  for i in 0..<h:
    a[i] = readLine()

  var dp = newSeq2[int](h, w)
  for y in 0..<h:
    for x in 0..<w:
      if y == 0 and x == 0:
        dp[0][0] = 1
        continue

      if a[y][x] == '#':
        dp[y][x] = 0
        continue

      var comb = 0
      if y > 0:
        comb = (comb + dp[y - 1][x]) mod MOD
      if x > 0:
        comb = (comb + dp[y][x - 1]) mod MOD
      dp[y][x] = comb

  echo dp[h - 1][w - 1]

main()


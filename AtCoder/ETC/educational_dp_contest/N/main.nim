import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import future

const INF* = int(1e18 + 373)

proc readLine*(): string =
  stdin.readLine()
proc readSeq*(): seq[string] =
  readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = readLine().strip()
proc readInt1*(): int =
  readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])
proc newSeqWith*[T](n: Natural; e: T): seq[T] =
  result = newSeq[T](n)
  for i in 0..<n:
    result[i] = e
type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] =
  newSeqWith(n1, newSeq[T](n2))
type seq3*[T] = seq[seq[seq[T]]]
proc newSeq3*[T](n1, n2, n3: Natural): seq3[T] =
  newSeqWith(n1, newSeqWith(n2, newSeq[T](n3)))

#------------------------------------------------------------------------------#
proc buildCumTab(a: seq[int]): seq[int] =
  let n = a.len()
  var c = newSeq[int](n + 1)
  for i in 1..n:
    c[i] = c[i - 1] + a[i - 1]
  return c

# [s, t)
proc lookupCumTab(c: seq[int]; s, t: int): int = c[t] - c[s]
#------------------------------------------------------------------------------#

proc solve(a, c: seq[int]; s, t: int; dp: var seq2[int]): int =
  if dp[s][t] >= 0:
    return dp[s][t]

  if t  - s == 0:
    dp[s][t] = 0
    return 0

  if t - s == 1:
    dp[s][t] = 0
    return 0

  var opt = INF
  for i in (s + 1)..<t:
    let lcost = solve(a, c, s, i, dp)
    let rcost = solve(a, c, i, t, dp)
    let sol = lcost + rcost + c.lookupCumTab(s, t)
    opt = min(opt, sol)

  dp[s][t] = opt
  return opt

proc main() =
  let n = readInt1()
  let a = readSeq().map(parseInt)
  let c = a.buildCumTab()

  var dp = newSeq2[int](n + 1, n + 1)
  for i in 0..n:
    for j in 0..n:
      dp[i][j] = -1

  echo solve(a, c, 0, n, dp)

main()


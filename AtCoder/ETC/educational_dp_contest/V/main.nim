import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import future

const INF* = int(1e18 + 373)

proc readLine*(): string =
  stdin.readLine()
proc readSeq*(): seq[string] =
  readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = readLine().strip()
proc readInt1*(): int =
  readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])
proc newSeqWith*[T](n: Natural; e: T): seq[T] =
  result = newSeq[T](n)
  for i in 0..<n:
    result[i] = e
type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] =
  newSeqWith(n1, newSeq[T](n2))
type seq3*[T] = seq[seq[seq[T]]]
proc newSeq3*[T](n1, n2, n3: Natural): seq3[T] =
  newSeqWith(n1, newSeqWith(n2, newSeq[T](n3)))

#------------------------------------------------------------------------------#
var MOD = -1

proc toRooted(g: seq2[int]; v, p: int; res: var seq2[int]) =
  for u in g[v]:
    if u == p:
      continue
    res[v].add(u)
    toRooted(g, u, v, res)

proc dfs1(g: seq2[int]; v, p: int; dp: var Table[(int, int), int]): int =
  var res = 1
  for u in g[v]:
    res = res * dfs1(g, u, v, dp) mod MOD

  res = (res + 1) mod MOD
  dp[(p, v)] = res
  return res

proc dfs2(g: seq2[int]; v, p: int; dp: var Table[(int, int), int]) =
  let k = g[v].len()

  if k == 0:
    return

  var cum = newSeq[int](k + 1)
  cum[k] = 1
  for i in countdown(k - 1, 0):
    cum[i] = cum[i + 1] * dp[(v, g[v][i])] mod MOD

  let par = dp[(v, p)]
  var mul = 1
  for i in 0..<k:
    var comb = 1
    comb = comb * par mod MOD
    comb = comb * mul mod MOD
    comb = comb * cum[i + 1] mod MOD
    comb = (comb + 1) mod MOD
    dp[(g[v][i], v)] = comb

    mul = mul * dp[(v, g[v][i])] mod MOD

  for u in g[v]:
    dfs2(g, u, v, dp)

proc main() =
  let (n, m) = readInt2()
  MOD = m

  var g = newSeq2[int](n, 0)
  for i in 0..<n - 1:
    let (x, y) = readInt2()
    g[x - 1].add(y - 1)
    g[y - 1].add(x - 1)

  var gg = newSeq2[int](n, 0)
  toRooted(g, 0, -1, gg)

  var dp = initTable[(int, int), int]()
  discard dfs1(gg, 0, -1, dp)
  dp[(0, -1)] = 1

  dfs2(gg, 0, -1, dp)

  for v in 0..<n:
    var ans = 1
    for u in g[v]:
      ans = ans * dp[(v, u)] mod MOD
    echo ans

main()


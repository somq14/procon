import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import future

const INF* = int(1e18 + 373)

proc readLine*(): string =
  stdin.readLine()
proc readSeq*(): seq[string] =
  readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = readLine().strip()
proc readInt1*(): int =
  readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])
proc newSeqWith*[T](n: Natural; e: T): seq[T] =
  result = newSeq[T](n)
  for i in 0..<n:
    result[i] = e
type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] =
  newSeqWith(n1, newSeq[T](n2))
type seq3*[T] = seq[seq[seq[T]]]
proc newSeq3*[T](n1, n2, n3: Natural): seq3[T] =
  newSeqWith(n1, newSeqWith(n2, newSeq[T](n3)))

#------------------------------------------------------------------------------#
type LazySegmentTree[T] = object
  n: int
  e: T
  f: (T, T) -> T
  a: seq[T]
  lazy: seq[T]
  hasLazy: seq[bool]

proc initLazySegmentTree[T](n: int; e: T; f: (T, T) -> T): LazySegmentTree[T] =
  result.n = nextPowerOfTwo(n)
  result.e = e
  result.f = f

  result.a = newSeqWith(result.n * 2, e)
  result.hasLazy = newSeqWith(result.n * 2, false)
  result.lazy = newSeqWith(result.n * 2, 0)

proc len[T](this: LazySegmentTree[T]): int =
  this.n

proc propagate[T](this: var LazySegmentTree[T]; i, s, t: int) =
  if not this.hasLazy[i]:
    return

  if t - s >= 2:
    this.hasLazy[i * 2] = true
    this.lazy[i * 2] += this.lazy[i]

    this.hasLazy[i * 2 + 1] = true
    this.lazy[i * 2 + 1] += this.lazy[i]

  this.a[i] += this.lazy[i] # ??
  this.hasLazy[i] = false
  this.lazy[i] = 0

proc add[T](this: var LazySegmentTree[T]; sq, tq, x: int; i, s, t: int) =
  this.propagate(i, s, t)

  if tq <= s or t <= sq:
    return

  if sq <= s and t <= tq:
    this.hasLazy[i] = true
    this.lazy[i] += x # ??
    this.propagate(i, s, t)
    return

  let m = s + (t - s) div 2
  this.add(sq, tq, x, i * 2, s, m)
  this.add(sq, tq, x, i * 2 + 1, m, t)
  this.a[i] = this.f(this.a[i * 2], this.a[i * 2 + 1])

proc add[T](this: var LazySegmentTree[T]; sq, tq, x: int) =
  this.add(sq, tq, x, 1, 0, this.n)

proc add[T](this: var LazySegmentTree[T]; i, x: int) =
  this.add(i, i + 1, x, 1, 0, this.n)

proc update[T](this: var LazySegmentTree[T], sq, tq: int; x: T; i, s, t: int) =
  this.propagate(i, s, t)

  if tq <= s or t <= sq:
    return

  if t - s == 1:
    this.a[i] = x
    return

  let m = s + (t - s) div 2
  this.update(sq, tq, x, i * 2, s, m)
  this.update(sq, tq, x, i * 2 + 1, m, t)
  this.a[i] = this.f(this.a[i * 2], this.a[i * 2 + 1])

proc update[T](this: var LazySegmentTree[T]; i: int; x: T) =
  this.update(i, i + 1, x, 1, 0, this.n)

proc query[T](this: var LazySegmentTree[T]; sq, tq: int; i, s, t: int): T =
  if tq <= s or t <= sq:
    return this.e

  if sq <= s and t <= tq:
    this.propagate(i, s, t)
    return this.a[i]

  this.propagate(i, s, t)
  let m = s + (t - s) div 2
  let vl = this.query(sq, tq, i * 2, s, m)
  let vh = this.query(sq, tq, i * 2 + 1, m, t)
  return this.f(vl, vh)

proc query[T](this: var LazySegmentTree[T]; sq, tq: int): T =
  this.query(sq, tq, 1, 0, this.n)

proc query[T](this: var LazySegmentTree[T]; i: int): T =
  this.query(i, i + 1, 1, 0, this.n)

proc `$`[T](this: var LazySegmentTree[T]): string =
  result = "["
  for i in 0..<this.n:
    let v = this.query(i)
    result = result & $v
    if i != this.n - 1:
      result = result & ", "
  result = result & "]"

#------------------------------------------------------------------------------#

type Event = tuple [ i, t, ll, rr, a: int ]

proc main() =
  let (n, m) = readInt2()

  var es = newSeq[Event](0)
  for i in 0..<m:
    var (ll, rr, a) = readInt3()
    es.add((ll,  1, ll, rr, a))
    es.add((rr + 1, -1, ll, rr, a))
  es.sort(cmp[Event])

  var ans = 0
  var ep = 0
  var segtree = initLazySegmentTree[int](n + 1, -INF, (a: int, b: int) => max(a, b))
  for i in 0..n:
    segtree.update(i, 0)

  for i in 1..n:
    while ep < es.len() and es[ep].i <= i:
      let e = es[ep]
      if e.t == 1:
        segtree.add(0, e.ll, e.a)
      elif e.t == -1:
        segtree.add(0, e.ll, -e.a)
      else:
        assert false
      ep += 1

    let opt = segtree.query(0, i)
    ans = max(opt, ans)
    segtree.update(i, opt)

  echo ans

main()

import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import future

const INF* = int(1e18 + 373)

proc readLine*(): string =
  stdin.readLine()
proc readSeq*(): seq[string] =
  readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = readLine().strip()
proc readInt1*(): int =
  readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])
proc newSeqWith*[T](n: Natural; e: T): seq[T] =
  result = newSeq[T](n)
  for i in 0..<n:
    result[i] = e
type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] =
  newSeqWith(n1, newSeq[T](n2))
type seq3*[T] = seq[seq[seq[T]]]
proc newSeq3*[T](n1, n2, n3: Natural): seq3[T] =
  newSeqWith(n1, newSeqWith(n2, newSeq[T](n3)))

#------------------------------------------------------------------------------#
const MOD = int(10^9 + 7)

proc main() =
  let k = readLine()
  let d = readInt1()

  let n = k.len()
  var dp = newSeq2[int](n + 1, d)
  dp[0].fill(0)
  dp[0][0] = 1
  for i in 1..n:
    for j in 0..<d:
      var comb = 0
      for c in 0..9:
        comb = (comb + dp[i - 1][(j - c mod d + d) mod d]) mod MOD
      dp[i][j] = comb

  var ans = 0
  var sum = 0
  for i in 0..<n:
    let m = k[i].ord() - '0'.ord()
    for c in 0..<m:
      let comb = dp[n - 1 - i][(d - (sum + c) mod d) mod d]
      ans = (ans + comb) mod MOD
    sum += m

  if sum mod d == 0: # k
    ans = (ans + 1) mod MOD

  ans = (ans - 1 + MOD) mod MOD # 0
  echo ans

main()


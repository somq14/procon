import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import future

const INF* = int(1e18 + 373)

proc readLine*(): string =
  stdin.readLine()
proc readSeq*(): seq[string] =
  readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = readLine().strip()
proc readInt1*(): int =
  readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])
proc newSeqWith*[T](n: Natural; e: T): seq[T] =
  result = newSeq[T](n)
  for i in 0..<n:
    result[i] = e
type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] =
  newSeqWith(n1, newSeq[T](n2))
type seq3*[T] = seq[seq[seq[T]]]
proc newSeq3*[T](n1, n2, n3: Natural): seq3[T] =
  newSeqWith(n1, newSeqWith(n2, newSeq[T](n3)))

#------------------------------------------------------------------------------#
proc solve(t, i, j: int; a: seq[int]; dp: var seq3[int]): int =
  if dp[t][i][j] != -INF:
    return dp[t][i][j]

  if i == j:
    dp[t][i][j] = 0
    return 0

  if t == 0:
    let vl = solve(1, i + 1, j, a, dp) + a[i]
    let vr = solve(1, i, j - 1, a, dp) + a[j - 1]
    let ans = max(vl, vr)
    dp[t][i][j] = ans
    return ans
  else:
    let vl = solve(0, i + 1, j, a, dp) - a[i]
    let vr = solve(0, i, j - 1, a, dp) - a[j - 1]
    let ans = min(vl, vr)
    dp[t][i][j] = ans
    return ans


proc main() =
  let n = readInt1()
  let a = readSeq().map(parseInt)

  var dp = newSeq3[int](2, n + 1, n + 1)
  for t in 0..1:
    for i in 0..n:
      for j in 0..n:
        dp[t][i][j] = -INF

  echo solve(0, 0, n, a, dp)

main()


import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import future

const INF* = int(1e18 + 373)

proc readLine*(): string =
  stdin.readLine()
proc readSeq*(): seq[string] =
  readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = readLine().strip()
proc readInt1*(): int =
  readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])
proc newSeqWith*[T](n: Natural; e: T): seq[T] =
  result = newSeq[T](n)
  for i in 0..<n:
    result[i] = e
type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] =
  newSeqWith(n1, newSeq[T](n2))
type seq3*[T] = seq[seq[seq[T]]]
proc newSeq3*[T](n1, n2, n3: Natural): seq3[T] =
  newSeqWith(n1, newSeqWith(n2, newSeq[T](n3)))

#------------------------------------------------------------------------------#

proc main() =
  let n = readInt1()

  var a = newSeq2[int](n, 0)
  for i in 0..<n:
    a[i] = readSeq().map(parseInt)

  var cost = newSeq[int](1 shl n)
  for s in 0..<(1 shl n):
    var sum = 0
    for i in 0..<n:
      if (s and (1 shl i)) == 0:
        continue
      for j in (i + 1)..<n:
        if (s and (1 shl j)) == 0:
          continue
        sum += a[i][j]
    cost[s] = sum

  var dp = newSeq[int](1 shl n)
  dp[0] = 0
  for s in 1..<(1 shl n):
    var sub = s
    var opt = 0
    while sub > 0:
      let sol = cost[sub] + dp[s xor sub]
      opt = max(opt, sol)
      sub = (sub - 1) and s
    dp[s] = opt

  echo dp[(1 shl n) - 1]

main()


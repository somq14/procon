import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import future

const INF* = int(1e18 + 373)

proc readLine*(): string =
  stdin.readLine()
proc readSeq*(): seq[string] =
  readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = readLine().strip()
proc readInt1*(): int =
  readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])
proc newSeqWith*[T](n: Natural; e: T): seq[T] =
  result = newSeq[T](n)
  for i in 0..<n:
    result[i] = e
type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] =
  newSeqWith(n1, newSeq[T](n2))
type seq3*[T] = seq[seq[seq[T]]]
proc newSeq3*[T](n1, n2, n3: Natural): seq3[T] =
  newSeqWith(n1, newSeqWith(n2, newSeq[T](n3)))

#------------------------------------------------------------------------------#

proc main() =
  let s = readLine()
  let t = readLine()
  let n = s.len()
  let m = t.len()

  var dp = newSeq2[int](n + 1, m + 1)
  var dpMem = newSeq2[int](n + 1, m + 1)

  for i in 1..n:
    for j in 1..m:
      var opt = dp[i - 1][j]
      var mem = 0
      if dp[i][j - 1] > opt:
        opt = dp[i][j - 1]
        mem = 1
      if s[i - 1] == t[j - 1] and dp[i][j - 1] + 1 > opt:
        opt = dp[i - 1][j - 1] + 1
        mem = 2

      dp[i][j] = opt
      dpMem[i][j] = mem

  var ans = ""
  block:
    var i = n
    var j = m
    while i > 0 and j > 0:
      if dpMem[i][j] == 0:
        i -= 1
      elif dpMem[i][j] == 1:
        j -= 1
      else:
        ans.add(s[i - 1])
        i -= 1
        j -= 1

  if ans.len() == 0:
    echo ""
    return

  ans.reverse()
  echo ans

main()


import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import future

const INF* = int(1e18 + 373)

proc readLine*(): string =
  stdin.readLine()
proc readSeq*(): seq[string] =
  readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = readLine().strip()
proc readInt1*(): int =
  readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])
proc newSeqWith*[T](n: Natural; e: T): seq[T] =
  result = newSeq[T](n)
  for i in 0..<n:
    result[i] = e
type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] =
  newSeqWith(n1, newSeq[T](n2))
type seq3*[T] = seq[seq[seq[T]]]
proc newSeq3*[T](n1, n2, n3: Natural): seq3[T] =
  newSeqWith(n1, newSeqWith(n2, newSeq[T](n3)))

#------------------------------------------------------------------------------#

proc main() =
  let (n, k) = readInt2()
  var w = newSeq[int](n)
  var v = newSeq[int](n)
  for i in 0..<n:
    (w[i], v[i]) = readInt2()

  var dp = newSeq2[int](n + 1, 1000 * n + 1)
  dp[n].fill(INF)
  dp[n][0] = 0
  for i in countdown(n - 1, 0):
    for j in 0..1000 * n:
      var opt = dp[i + 1][j]
      if j >= v[i] and dp[i + 1][j - v[i]] != INF:
        opt = min(opt, dp[i + 1][j - v[i]] + w[i])
      dp[i][j] = opt

  var ans = -1
  for j in countdown(1000 * n, 0):
    if dp[0][j] <= k:
      ans = j
      break

  echo ans

main()


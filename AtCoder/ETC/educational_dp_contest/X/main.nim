import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import future

const INF* = int(1e18 + 373)

proc readLine*(): string =
  stdin.readLine()
proc readSeq*(): seq[string] =
  readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = readLine().strip()
proc readInt1*(): int =
  readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])
proc newSeqWith*[T](n: Natural; e: T): seq[T] =
  result = newSeq[T](n)
  for i in 0..<n:
    result[i] = e
type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] =
  newSeqWith(n1, newSeq[T](n2))
type seq3*[T] = seq[seq[seq[T]]]
proc newSeq3*[T](n1, n2, n3: Natural): seq3[T] =
  newSeqWith(n1, newSeqWith(n2, newSeq[T](n3)))

#------------------------------------------------------------------------------#
type Block = tuple [ w, s, v: int ]

const WMAX = 20000

proc main() =
  let n = readInt1()
  var b = newSeq[Block](n)

  for i in 0..<n:
    let (w, s, v) = readInt3()
    b[i] = (w, s, v)
  b.sort((b1: Block, b2: Block) => (b1.w + b1.s) - (b2.w + b2.s))

  var dp = newSeq2[int](n + 1, WMAX + 1)
  for i in 1..n:
    for j in 0..WMAX:
      var opt = dp[i - 1][j]
      if j - b[i - 1].w >= 0 and j - b[i - 1].w <= b[i - 1].s:
        opt = max(opt, dp[i - 1][j - b[i - 1].w] + b[i - 1].v)
      dp[i][j] = opt

  echo dp[n].max()

main()


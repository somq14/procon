import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import future

const INF* = int(1e18 + 373)

proc readLine*(): string =
  stdin.readLine()
proc readSeq*(): seq[string] =
  readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = readLine().strip()
proc readInt1*(): int =
  readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])
proc newSeqWith*[T](n: Natural; e: T): seq[T] =
  result = newSeq[T](n)
  for i in 0..<n:
    result[i] = e
type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] =
  newSeqWith(n1, newSeq[T](n2))
type seq3*[T] = seq[seq[seq[T]]]
proc newSeq3*[T](n1, n2, n3: Natural): seq3[T] =
  newSeqWith(n1, newSeqWith(n2, newSeq[T](n3)))

#------------------------------------------------------------------------------#
const MOD = int(10^9 + 7)

proc buildCumTab(a: seq[int]): seq[int] =
  let n = a.len()
  var c = newSeq[int](n + 1)
  for i in 1..n:
    c[i] = (c[i - 1] + a[i - 1]) mod MOD
  return c

# [s, t)
proc lookupCumTab(c: seq[int]; s, t: int): int =
  (c[t] - c[s] + MOD) mod MOD

proc main() =
  let (n, k) = readInt2()
  let a = readSeq().map(parseInt)

  var dp = newSeq2[int](n + 1, k + 1)
  dp[n].fill(0)
  dp[n][0] = 1

  var cum = dp[n].buildCumTab()
  for i in countdown(n - 1, 0):
    for j in 0..k:
      let s = max(j - a[i], 0)
      let t = j + 1
      dp[i][j] = cum.lookupCumTab(s, t)
    cum = dp[i].buildCumTab()

  let ans = cum.lookupCumTab(k, k + 1)
  echo ans

main()


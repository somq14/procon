import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import future

const INF* = int(1e18 + 373)

proc readLine*(): string =
  stdin.readLine()
proc readSeq*(): seq[string] =
  readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = readLine().strip()
proc readInt1*(): int =
  readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])
proc newSeqWith*[T](n: Natural; e: T): seq[T] =
  result = newSeq[T](n)
  for i in 0..<n:
    result[i] = e
type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] =
  newSeqWith(n1, newSeq[T](n2))
type seq3*[T] = seq[seq[seq[T]]]
proc newSeq3*[T](n1, n2, n3: Natural): seq3[T] =
  newSeqWith(n1, newSeqWith(n2, newSeq[T](n3)))

#------------------------------------------------------------------------------#
const MOD = int(10^9 + 7)

proc mul(n: int; a: seq2[int]; b: seq2[int]): seq2[int] =
  result = newSeq2[int](n, n)
  for i in 0..<n:
    for j in 0..<n:
      var sum = 0
      for k in 0..<n:
        sum = (sum + (a[i][k] * b[k][j] mod MOD)) mod MOD
      result[i][j] = sum

const P = 59

proc main() =
  let (n, k) = readInt2()

  var a = newSeq2[int](n, 0)
  for i in 0..<n:
    a[i] = readSeq().map(parseInt)

  var p = newSeq[seq2[int]](P + 1)
  p[0] = a
  for i in 1..P:
    p[i] = mul(n, p[i - 1], p[i - 1])

  var ak = newSeq2[int](n, n)
  for i in 0..<n:
    ak[i][i] = 1

  for i in 0..P:
    if (k and (1 shl i)) != 0:
      ak = mul(n, ak, p[i])

  var ans = 0
  for i in 0..<n:
    for j in 0..<n:
      ans = (ans + ak[i][j]) mod MOD
  echo ans

main()


import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import future

const INF* = int(1e18 + 373)

proc readLine*(): string =
  stdin.readLine()
proc readSeq*(): seq[string] =
  readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = readLine().strip()
proc readInt1*(): int =
  readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])
proc newSeqWith*[T](n: Natural; e: T): seq[T] =
  result = newSeq[T](n)
  for i in 0..<n:
    result[i] = e
type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] =
  newSeqWith(n1, newSeq[T](n2))
type seq3*[T] = seq[seq[seq[T]]]
proc newSeq3*[T](n1, n2, n3: Natural): seq3[T] =
  newSeqWith(n1, newSeqWith(n2, newSeq[T](n3)))

#------------------------------------------------------------------------------#
const MOD = int(10^9 + 7)

# c == 0 : white
# c == 1 : black
proc solve(g: seq2[int]; v, c, p: int; dp: var seq2[int]): int =
  if dp[v][c] >= 0:
    return dp[v][c]

  if g[v].len() == 1 and g[v][0] == p:
    dp[v][c] = 1
    return 1

  if c == 0:
    var comb = 1
    for u in g[v]:
      if u == p:
        continue
      let sum = (solve(g, u, 0, v, dp) + solve(g, u, 1, v, dp)) mod MOD
      comb = comb * sum mod MOD
    dp[v][c] = comb
    return comb

  assert c == 1
  var comb = 1
  for u in g[v]:
    if u == p:
      continue
    comb = comb * solve(g, u, 0, v, dp) mod MOD
  dp[v][c] = comb
  return comb

proc main() =
  let n = readInt1()

  var g = newSeq2[int](n, 0)
  for i in 0..<n - 1:
    let (x, y) = readInt2()
    g[x - 1].add(y - 1)
    g[y - 1].add(x - 1)

  var dp = newSeq2[int](n, 2)
  for v in 0..<n:
    dp[v][0] = -1
    dp[v][1] = -1

  let ans = (solve(g, 0, 0, -1, dp) + solve(g, 0, 1, -1, dp)) mod MOD
  echo ans

main()


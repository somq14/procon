import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import future
import macros

const INF* = int(1e18 + 373)

proc readLine*(): string =
  stdin.readLine().strip()
proc readSeq*(): seq[string] =
  readLine().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeqWith(n, readLine())
proc readIntSeq*(): seq[int] =
  result = readSeq().map(parseInt)
proc readIntSeq*(n: Natural): seq[int] =
  result = readSeq(n).map(parseInt)
proc readInt1*(): int =
  readLine().parseInt()
proc readInt2*(): (int, int) =
  let a = readIntSeq(); return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readIntSeq(); return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readIntSeq(); return (a[0], a[1], a[2], a[3])

proc newSeqOf*[T](n: Natural; e: T): seq[T] =
  result = newSeq[T](n)
  result.fill(e)
proc newSeq*[T](n: Natural; e: T): seq[T] =
  newSeqOf[T](n, e)
type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] =
  newSeqOf(n1, newSeq[T](n2))
proc newSeq2*[T](n1, n2: Natural; e: T): seq2[T] =
  newSeqOf(n1, newSeqOf(n2, e))
type seq3*[T] = seq[seq[seq[T]]]
proc newSeq3*[T](n1, n2, n3: Natural): seq3[T] =
  newSeqOf(n1, newSeqOf(n2, newSeq[T](n3)))
proc newSeq3*[T](n1, n2, n3: Natural; e: T): seq3[T] =
  newSeqOf(n1, newSeqOf(n2, newSeqOf(n3, e)))
#------------------------------------------------------------------------------#

proc updateMax[T](x: var T, y: T) =
  x = max(x, y)

proc main() =
  let n = readInt1()
  let a = readIntSeq(n)

  var ans = 0
  var dp = newSeq2[int](n, n + 1, -INF)
  for i in 0..<n:
    dp[i][n] = 0

  for m in countdown(n, 1):
    for s in 0..<n:
      let al = a[s]
      let ar = a[(s + m - 1) mod n]

      if m == 1:
        ans.updateMax(dp[s][m] + al)
        continue

      if m == 2:
        ans.updateMax(dp[s][m] + max(al, ar))
        continue

      block:
        let ss = (s + 1) mod n

        if a[ss] > a[(ss + m - 2) mod n]:
          dp[(ss + 1) mod n][m - 2].updateMax(dp[s][m] + al)
        else:
          dp[ss][m - 2].updateMax(dp[s][m] + al)

      block:
        let ss = s

        if a[ss] > a[(ss + m - 2) mod n]:
          dp[(ss + 1) mod n][m - 2].updateMax(dp[s][m] + ar)
        else:
          dp[ss][m - 2].updateMax(dp[s][m] + ar)

  echo ans

main()


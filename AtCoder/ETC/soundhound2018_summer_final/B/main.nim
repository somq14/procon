import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import logging
import future

const INF* = int(1e18 + 373)

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

proc readInt1*(): int = readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#

proc main() =
  let (n, k) = readInt2()
  let b = readSeq(n).map(parseInt)

  var dp1 = newSeq[int](n + 1)
  var dp2 = newSeq[int](n + 1)
  dp1[0] = 0
  dp2[0] = 0
  for i in 1..n:
    dp1[i] = dp1[i - 1] + b[i - 1]
    if i - k >= 0:
      dp1[i] = max(dp1[i], dp2[i - k])
    dp2[i] = max(dp2[i - 1], dp1[i - 1] + b[i - 1])

  echo dp1[n]


main()


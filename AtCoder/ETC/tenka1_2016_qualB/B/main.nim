import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import logging
import future

const INF* = int(1e18 + 373)

proc readLine*(): string = stdin.readLine()

proc readSeq*(): seq[string] = readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = readLine().strip()

proc readInt1*(): int = readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

type seq3*[T] = seq[seq[seq[T]]]
proc newSeq3*[T](n1, n2, n3: Natural): seq3[T] = newSeqWith(n1, newSeqWith(n2, newSeq[T](n3)))

#------------------------------------------------------------------------------#

proc main() =
  var s = readLine()
  let n = s.len()

  var dp = newSeq2[int](n + 1, n + 1)
  dp[0].fill(INF)
  dp[0][0] = 0
  for i in 1..n:
    for j in 0..n:
      var opt = INF
      case s[i - 1]:
      of '(':
        if j > 0:
          opt = min(opt, dp[i - 1][j - 1])
        if j < n:
          opt = min(opt, dp[i - 1][j + 1] + 1)
      of ')':
        if j > 0:
          opt = min(opt, dp[i - 1][j - 1] + 1)
        if j < n:
          opt = min(opt, dp[i - 1][j + 1])
      else:
        assert false
      dp[i][j] = opt

  var ans = n - 1 + dp[n][0]
  var sum = 0
  for i in countdown(n - 1, 0):
    case s[i]:
    of '(':
      sum -= 1
    of ')':
      sum += 1
    else:
      assert false

    if sum < 0:
      break

    let sol = max(i - 1, 0) + dp[i][sum]
    ans = min(ans, sol)

  echo ans

main()


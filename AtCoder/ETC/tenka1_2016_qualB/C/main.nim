import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import logging
import future

const INF* = int(1e18 + 373)

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

proc readInt1*(): int = readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#
#------------------------------------------------------------------------------#
const MOD = int(1e9 + 7)

type Op1[T] = proc(a: T, b: T): T
type Op2[T] = proc(a: T, b: T): T

proc repeatedSquares[T](x: T, n: Natural, e: T, f: Op2[T]): T =
    result = e
    var xx = f(x, e)
    var i = n
    while i > 0:
        if (i and 1) != 0:
            result = f(result, xx)
        xx = f(xx, xx)
        i = i shr 1


proc addM(a, b: Natural): Natural = (a + b) mod MOD
proc subM(a, b: Natural): Natural = (a - b + MOD) mod MOD
proc mulM(a, b: Natural): Natural = a * b mod MOD
proc powM(a, b: Natural): Natural = repeatedSquares[Natural](a, b, 1, `mulM`)
proc invM(a: Natural): Natural = a.powM(MOD - 2)
proc divM(a, b: Natural): Natural = a.mulM(invM(b))


var memoFactM = @[Natural(1)]
proc factM(n: Natural): Natural =
    if n < memoFactM.len():
        return memoFactM[n]
    for i in memoFactM.len()..n:
        memoFactM.add(memoFactM[i - 1].mulM(i))
    return memoFactM[n]


proc combM(n, r: Natural): Natural =
    if r > n:
        return 0
    if r > n div 2:
        return combM(n, n - r)

    result = 1
    for i in ((n - r) + 1)..n:
        result = result.mulM(i)
    result = result.divM(factM(r))


proc multCombM(n, r: Natural): Natural = combM(n + r - 1, n - 1)

#------------------------------------------------------------------------------#
type P = tuple [ a, b: int ]

proc `<`(p1, p2: P): bool =
  if p1.a == p2.a:
    return p1.b > p2.b
  return p1.a < p2.a

# n試合中s回実際に勝っている人がt回勝ったと記録される確率
proc subr(n, s, t, p, q: int): float =
  let corrProb = 1.0 * p.float() / q.float()
  let missProb = 1.0 - corrProb

  var ans = 0.0
  for i in 0..s:
    let j = t - (s - i)
    if j notin 0..(n - s):
      continue
    let term1 = s.combM(i).float() * missProb.pow(i.float()) * corrProb.pow((s - i).float())
    let term2 = (n - s).combM(j).float() * missProb.pow(j.float()) * corrProb.pow((n - s - j).float())
    ans += term1 * term2
  return ans

proc main() =
  let input = readSeq()
  let n = input[0].parseInt()
  let p = input[1].split("/")[0].parseInt()
  let q = input[1].split("/")[1].parseInt()

  var a = newSeq2[int](n, 0)
  for i in 0..<n:
    a[i] = readSeq().map(parseInt)

  # P[0] : final win
  # P[1] : qual rank
  var ps = newSeq[P](n)
  for i in 0..<n:
    ps[i] = (a[i].sum(), i)
  ps.sort(cmp[P])

  var dp = newSeq2[float](n, n)
  for j in 0..<n:
    dp[0][j] = subr(n - 1, a[ps[0][1]].sum(), j, p, q)

  for i in 1..<n:
    for j in 0..<n:
      var prob = 0.0
      let coef = subr(n - 1, a[ps[i][1]].sum(), j, p, q)
      for k in 0..<j:
        prob += coef * dp[i - 1][k]
      if ps[i][1] < ps[i - 1][1]:
        prob += coef * dp[i - 1][j]
      dp[i][j] = prob

  echo dp[n - 1].sum()

main()


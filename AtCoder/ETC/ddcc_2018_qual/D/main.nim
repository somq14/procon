import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import future

const INF* = int(1e18 + 373)

proc readLine*(): string =
  stdin.readLine()
proc readSeq*(): seq[string] =
  readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = readLine().strip()
proc readInt1*(): int =
  readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])
proc newSeqWith*[T](n: Natural; e: T): seq[T] =
  result = newSeq[T](n)
  for i in 0..<n:
    result[i] = e
type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] =
  newSeqWith(n1, newSeq[T](n2))
type seq3*[T] = seq[seq[seq[T]]]
proc newSeq3*[T](n1, n2, n3: Natural): seq3[T] =
  newSeqWith(n1, newSeqWith(n2, newSeq[T](n3)))

#------------------------------------------------------------------------------#
proc gcd(a, b: int): int =
  var a = a
  var b = b
  while b != 0:
    let tmp = a mod b
    a = b
    b = tmp
  return a

proc lcm(a, b: int): int =
  a * (b div gcd(a, b))

# x * a + y * b = gcd(a, b)
# result[0] = gcd(a, b)
# result[1] = x
# result[2] = y
proc euclid(a, b: int): (int, int, int) =
  if b == 0:
    return (a, 1, 0)

  let (d, x, y) = euclid(b, a mod b)
  # 商と剰余の性質により
  # a = b * (a div b) + (a mod b)
  # (a mod b) = a - b * (a div b)

  # 再帰の戻り値の契約により
  # gcd(a, b) = x * b + y * (a mod b)
  # gcd(a, b) = x * b + y * (a - b * (a div b))
  # gcd(a, b) = x * b + y * a - y * (a div b) * b
  # gcd(a, b) = y * a + (x - y * (a div b)) * b
  return (d, y, x - y * (a div b))

# x mod m1 = b1
# x mod m2 = b2
proc chineseRemainder(m1, b1, m2, b2: int): (bool, int) =
  let (d, p, q) = euclid(m1, m2)
  # ここで
  # d = gcd(m1, m2)
  # p * m1 + q * m2 = d

  if (b2 - b1) mod d != 0:
    return (false, 0)

  let s = (b2 - b1) div d
  # s = (b2 - b1) div d
  # s * d = b2 - b1
  # s * (p * m1 + q * m2) = b2 - b1
  # s * p * m1 + s * q * m2 = b2 - b1
  # b1 + s * p * m1 = b2 - s * q * m2

  # ここで
  # (b1 + s * p * m1) mod m1 = b1
  # (b2 - s * q * m2) mod m2 = b2

  # したがって
  # x = b1 + s * p * m1 = b2 - s * q * m2
  # が解
  let m = m1 * (m2 div d) # lcm
  let x = b1 + s * m1 mod m * p mod m
  return (true, (x mod m + m) mod m)

proc sumDigit(n, b: int): int =
  var n = n
  while n > 0:
    result += n mod b
    n = n div b

proc eval(a: seq[int]; n: int): bool =
  for i in 2..30:
    if n.sumDigit(i) != a[i]:
      return false
  return true

proc solve(a: seq[int]): int =
  var x = 0
  var m = 1

  block:
    for i in 2..20:
      var found: bool
      (found, x) = chineseRemainder(m, x, i - 1, a[i] mod (i - 1))

      if not found:
        return -1

      m = lcm(m, i - 1)

  var ans = x
  while ans <= 10^12:
    if eval(a, ans):
      return ans
    ans += m

  return -1

proc main() =
  var a = newSeq[int](31)
  for i in 2..30:
    a[i] = readInt1()

  let ans = solve(a)
  if ans >= 0:
    echo ans
  else:
    echo "invalid"

main()


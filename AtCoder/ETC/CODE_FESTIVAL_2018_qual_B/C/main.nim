import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import logging
import future

const INF* = int(1e18 + 373)

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

proc readInt1*(): int = readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#

proc main() =
  let n = readInt1()
  let m = n + 2

  var s = newSeq2[char](m, m)
  for i in 0..<m:
    s[i].fill('.')

  var que = initQueue[(int, int)]()
  que.enqueue((0, 0))
  s[0][0] = 'X'

  var dy = @[1, -1,  2, -2]
  var dx = @[2, -2, -1,  1]
  while que.len() > 0:
    let (y, x) = que.dequeue()
    for k in 0..<4:
      let nextY = y + dy[k]
      let nextX = x + dx[k]
      if nextY notin 0..<m or nextX notin 0..<m:
        continue
      if s[nextY][nextX] != 'X':
        s[nextY][nextX] = 'X'
        que.enqueue((nextY, nextX))

  for i in 0..<m:
    for j in 0..<m:
      if not (i == m - 1 or j == m - 1 or i == 0 or j == 0):
        continue
      if s[i][j] != 'X':
        continue
      var y = i
      var x = j
      if y == 0:
        y += 1
      if y == m - 1:
        y -= 1
      if x == 0:
        x += 1
      if x == m - 1:
        x -= 1
      s[y][x] = 'X'

  var cnt = 0
  for i in 1..<m - 1:
    for j in 1..<m - 1:
      if s[i][j] == 'X':
        cnt += 1
      stdout.write(s[i][j])
    echo ""
  stderr.writeLine(cnt)

main()


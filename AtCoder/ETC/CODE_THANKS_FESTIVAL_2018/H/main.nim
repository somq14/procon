import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import future

const INF* = int(1e18 + 373)

proc readLine*(): string =
  stdin.readLine()
proc readSeq*(): seq[string] =
  readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = readLine().strip()
proc readInt1*(): int =
  readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])
proc newSeqWith*[T](n: Natural; e: T): seq[T] =
  result = newSeq[T](n)
  for i in 0..<n:
    result[i] = e
type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] =
  newSeqWith(n1, newSeq[T](n2))
type seq3*[T] = seq[seq[seq[T]]]
proc newSeq3*[T](n1, n2, n3: Natural): seq3[T] =
  newSeqWith(n1, newSeqWith(n2, newSeq[T](n3)))

#------------------------------------------------------------------------------#
#------------------------------------------------------------------------------#
type Pred1[T] = T -> bool

# [lb, ub)
proc nibutanLb(lb, ub: int; f: Pred1[int]): int =
  var lb = lb
  var ub = ub
  while ub - lb > 1:
    let mid = (ub + lb) div 2
    if f(mid):
      lb = mid
    else:
      ub = mid
  return lb

# (lb, ub]
proc nibutanUb(lb, ub: int; f: Pred1[int]): int =
  var lb = lb
  var ub = ub
  while ub - lb > 1:
    let mid = (ub + lb) div 2
    if f(mid):
      ub = mid
    else:
      lb = mid
  return ub
#------------------------------------------------------------------------------#
#------------------------------------------------------------------------------#

proc compress[T](a: seq[T]): Table[T, int] =
  result = initTable[T, int]()
  for e in a:
    if e notin result:
      result[e] = result.len()

proc uniqued[T](a: seq[T]): seq[T] =
  if a.len() == 0:
    return @[]

  var a = a.sorted(cmp[T])
  result = @[ a[0] ]
  for e in a:
    if result[result.len() - 1] == e:
      continue
    result.add(e)

#------------------------------------------------------------------------------#
proc eval(n: int; a: seq[int]; k: int): int =
  var dp = newSeq2[int](n, 2)

  for i in countdown(n - 1, 0):
    # dp[i][0]
    block:
      var s = 0
      var opt = -INF
      for j in i..<n - 1:
        s += a[j]
        if s >= k:
          opt = max(opt, dp[j + 1][1] + 1)
        else:
          opt = max(opt, dp[j + 1][1] - 1)

      s += a[n - 1]
      if s >= k:
        opt = max(opt, 1)
      else:
        opt = max(opt, -1)

      dp[i][0] = opt

    # dp[i][1]
    block:
      var s = 0
      var opt = +INF
      for j in i..<n - 1:
        s += a[j]
        if s >= k:
          opt = min(opt, dp[j + 1][0] + 1)
        else:
          opt = min(opt, dp[j + 1][0] - 1)

      s += a[n - 1]
      if s >= k:
        opt = min(opt, 1)
      else:
        opt = min(opt, -1)

      dp[i][1] = opt

  return dp[0][0]

proc main() =
  let n = readInt1()
  let a = readSeq().map(parseInt)

  var lb = -(10^13) - 100
  var ub = +(10^13) + 100
  while ub - lb > 1:
    let mid = (ub + lb) div 2
    let res = eval(n, a, mid)
    if res >= 0:
      lb = mid
    else:
      ub = mid

  echo lb

main()


import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import future

const INF* = int(1e18 + 373)

proc readLine*(): string =
  stdin.readLine()
proc readSeq*(): seq[string] =
  readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = readLine().strip()
proc readInt1*(): int =
  readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])
proc newSeqWith*[T](n: Natural; e: T): seq[T] =
  result = newSeq[T](n)
  for i in 0..<n:
    result[i] = e
type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] =
  newSeqWith(n1, newSeq[T](n2))
type seq3*[T] = seq[seq[seq[T]]]
proc newSeq3*[T](n1, n2, n3: Natural): seq3[T] =
  newSeqWith(n1, newSeqWith(n2, newSeq[T](n3)))

#------------------------------------------------------------------------------#

const MAX_DEPTH = 300

proc dfsBan(g: seq2[int]; v, t, d: int; found: bool; ban: var seq[bool]): int =
  if v == t or found:
    ban[v] = true
    for u in g[v]:
      discard dfsBan(g, u, t, d, true, ban)
    return d

  for u in g[v]:
    let ret = dfsBan(g, u, t, d + 1, found, ban)
    if ret >= 0:
      return ret
  return -1

proc dfsDepth(g: seq2[int]; v, d: int; depth: var seq[int]; ban: seq[bool]) =
  if ban[v]:
    return

  depth[d] += 1

  for u in g[v]:
    if ban[u]:
      continue
    dfsDepth(g, u, d + 1, depth, ban)


proc eval(n, m, k, r: int; g: seq2[int]; ban: var seq[bool]; t: int): int =
  var banCopy = ban

  let tDepth = dfsBan(g, r, t, 0, false, banCopy)
  var kUse = 0
  kUse += 1 + tDepth
  if kUse > k:
    return -1

  var emptyCount = 0
  for v in 0..<n:
    if not banCopy[v]:
      emptyCount += 1

  if emptyCount < m - 1:
    return -1

  var depth = newSeq[int](MAX_DEPTH + 1)
  dfsDepth(g, r, 0, depth, banCopy)

  var minK = 0
  block:
    var putCnt = 0
    for i in 0..MAX_DEPTH:
      let c = min(depth[i], (m - 1) - putCnt)
      putCnt += c
      minK += (i + 1) * c

  var maxK = 0
  block:
    var putCnt = 0
    for i in countdown(MAX_DEPTH, 0):
      let c = min(depth[i], (m - 1) - putCnt)
      putCnt += c
      maxK += (i + 1) * c

  if k - kUse notin minK..maxK:
    return -1

  ban = banCopy
  return kUSe

proc solve(n, m, k, r: int; g: seq2[int]): seq[int] =
  var ans = newSeq[int](m)
  var ban = newSeq[bool](n)

  var kk = k
  var mm = m

  for i in 0..<m:
    var found = false
    for j in 0..<n:
      if ban[j]:
        continue

      let kUse = eval(n, mm, kk, r, g, ban, j)
      if kUse >= 1:
        found = true
        ans[i] = j

        mm -= 1
        kk -= kUse
        break

    if not found:
      return @[]

  return ans

proc main() =
  let (n, m, k) = readInt3()
  let p = readSeq().map(parseInt)

  var g = newSeq2[int](n, 0)
  var root = -1
  for i in 0..<n:
    if p[i] == 0:
      root = i
      continue
    g[p[i] - 1].add(i)

  let ans = solve(n, m, k, root, g)
  if ans.len() == 0:
    echo -1
  else:
    echo ans.map(it => $(it + 1)).join(" ")

main()


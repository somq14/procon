import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import future

const INF* = int(1e18 + 373)

proc readLine*(): string =
  stdin.readLine()
proc readSeq*(): seq[string] =
  readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = readLine().strip()
proc readInt1*(): int =
  readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])
proc newSeqWith*[T](n: Natural; e: T): seq[T] =
  result = newSeq[T](n)
  for i in 0..<n:
    result[i] = e
type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] =
  newSeqWith(n1, newSeq[T](n2))
type seq3*[T] = seq[seq[seq[T]]]
proc newSeq3*[T](n1, n2, n3: Natural): seq3[T] =
  newSeqWith(n1, newSeqWith(n2, newSeq[T](n3)))

#------------------------------------------------------------------------------#

const M = 15

proc main() =
  let n = readInt1()
  let a = readSeq().map(parseInt)

  var dp1 = newSeq2[int](n + 1, M + 1)
  block:
    dp1[n].fill(0)
    for j in 0..M:
      dp1[n - 1][j] = 2 * j

    for i in countdown(n - 2, 0):
      for j in 0..M:
        let a0 = a[i] * (4 ^ j)

        var a1 = a[i + 1]
        var cnt = 0
        while a0 > a1:
          a1 *= 4
          cnt += 1

        dp1[i][j] = 2 * j + dp1[i + 1][min(cnt, M)] + 2 * (n - 1 - i) * max(cnt - M, 0)

  var dp2 = newSeq2[int](n + 1, M + 1)
  block:
    dp2[0].fill(0)

    for j in 0..M:
      dp2[1][j] = 2 * j + 1

    for i in 2..n:
      for j in 0..M:
        let a0 = -2 * a[i - 1] * (4 ^ j)

        var a1 = -2 * a[i - 2]
        var cnt = 0
        while a1 > a0:
          a1 *= 4
          cnt += 1

        dp2[i][j] = 2 * j + 1 + dp2[i - 1][min(cnt, M)] + 2 * (i - 1) * max(cnt - M, 0)

  var ans = INF
  for i in 0..n:
    ans = min(ans, dp1[i][0] + dp2[i][0])
  echo ans

main()


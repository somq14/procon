import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import logging
import future

const INF* = int(1e18 + 373)

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

proc readInt1*(): int = readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#
proc bitwid(n: int): int =
  result = 0
  var n = n
  while n != 0:
    n = n shr 1
    result += 1

proc subr(n: int; a, b: seq[int]; p: int): int =
  result = 0
  for i in 0..<n:
    if (a[i] or p) == p:
      result += b[i]

proc main() =
  let (n, k) = readInt2()
  var a = newSeq[int](n)
  var b = newSeq[int](n)
  for i in 0..<n:
    (a[i], b[i]) = readInt2()

  var ans = subr(n, a, b, k)
  for i in 0..<k.bitwid():
    if (k and (1 shl i)) == 0:
      continue
    let p = (k and not ((1 shl i) - 1)) - 1
    ans = max(ans, subr(n, a, b, p))

  echo ans

main()


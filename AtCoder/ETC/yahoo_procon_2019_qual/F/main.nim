import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import future

const INF* = int(1e18 + 373)

proc readLine*(): string =
  stdin.readLine()
proc readSeq*(): seq[string] =
  readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = readLine().strip()
proc readInt1*(): int =
  readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])
proc newSeqWith*[T](n: Natural; e: T): seq[T] =
  result = newSeq[T](n)
  for i in 0..<n:
    result[i] = e
type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] =
  newSeqWith(n1, newSeq[T](n2))
type seq3*[T] = seq[seq[seq[T]]]
proc newSeq3*[T](n1, n2, n3: Natural): seq3[T] =
  newSeqWith(n1, newSeqWith(n2, newSeq[T](n3)))

#------------------------------------------------------------------------------#
const MOD = int(998244353)

type Op1[T] = proc(a: T, b: T): T
type Op2[T] = proc(a: T, b: T): T

proc repeatedSquares[T](x: T, n: Natural, e: T, f: Op2[T]): T =
    result = e
    var xx = f(x, e)
    var i = n
    while i > 0:
        if (i and 1) != 0:
            result = f(result, xx)
        xx = f(xx, xx)
        i = i shr 1

proc addM(a, b: Natural): Natural = (a + b) mod MOD
proc subM(a, b: Natural): Natural = (a - b + MOD) mod MOD
proc mulM(a, b: Natural): Natural = a * b mod MOD
proc powM(a, b: Natural): Natural = repeatedSquares[Natural](a, b, 1, `mulM`)
proc invM(a: Natural): Natural = a.powM(MOD - 2)
proc divM(a, b: Natural): Natural = a.mulM(invM(b))

var memoFactM: seq[Natural] = nil
var memoFactInvM: seq[Natural] = nil

proc buildFactTable(n: Natural) =
  memoFactM = newSeq[Natural](n + 1)
  memoFactM[0] = 1
  for i in 1..n:
    memoFactM[i] = memoFactM[i - 1].mulM(i)

  memoFactInvM = newSeq[Natural](n + 1)
  memoFactInvM[n] = invM(memoFactM[n])
  for i in countdown(n - 1, 0):
    memoFactInvM[i] = memoFactInvM[i + 1].mulM(i + 1)

buildFactTable(10^6)

proc factM(n: Natural): Natural = memoFactM[n]

proc factInvM(n: Natural): Natural = memoFactInvM[n]

proc combM(n, r: Natural): Natural =
  if r > n:
    return 0
  if r > n div 2:
    return combM(n, n - r)

  result = factM(n).mulM(factInvM(n - r)).mulM(factInvM(r))

proc multCombM(n, r: Natural): Natural = combM(n + r - 1, r - 1)

#------------------------------------------------------------------------------#
proc solve(n: int; s: string; a, b: seq[int]; i, j: int; dp:var seq2[int]): int =
  if dp[i][j] >= 0:
    return dp[i][j]

  let aused = j
  let bused = i - j

  var arest = a[i] - aused
  var brest = b[i] - bused

  var ans = 0
  if arest < 0:
    if s[i] == '0':
      arest += 1
    if s[i] == '1':
      arest += 1

    if arest > 0:
      ans = ans.addM(solve(n, s, a, b, i + 1, j + 1, dp))
    if brest > 0:
      ans = ans.addM(solve(n, s, a, b, i + 1, j, dp))
  elif brest < 0:
    if s[i] == '1':
      brest += 1
    if s[i] == '2':
      brest += 1

    if arest > 0:
      ans = ans.addM(solve(n, s, a, b, i + 1, j + 1, dp))
    if brest > 0:
      ans = ans.addM(solve(n, s, a, b, i + 1, j, dp))
  else:
    if arest > 0 or (i < n and (s[i] == '0' or s[i] == '1')):
      ans = ans.addM(solve(n, s, a, b, i + 1, j + 1, dp))
    if brest > 0 or (i < n and (s[i] == '1' or s[i] == '2')):
      ans = ans.addM(solve(n, s, a, b, i + 1, j, dp))
  dp[i][j] = ans

  return ans

proc main() =
  let s = readLine()
  let n = s.len()

  var a = newSeq[int](2 * n)
  var b = newSeq[int](2 * n)
  a[0] = 0
  b[0] = 0

  var cnt = 0
  for i in 1..n:
    case s[i - 1]
    of '0':
      a[i] = a[i - 1] + 2
      b[i] = b[i - 1]
      acnt += 2
    of '1':
      a[i] = a[i - 1] + 1
      b[i] = b[i - 1] + 1
      cnt += 1
    of '2':
      a[i] = a[i - 1]
      b[i] = b[i - 1] + 2
    else:
      assert false

  for i in n + 1..<2 * n:
    a[i] = a[i - 1]
    b[i] = b[i - 1]

  var dp = newSeq2[int](2 * n + 1, 2 * n + 1)
  dp[2 * n][cnt] = 1
  for i in 0..<2 * n:
    for j in 0..2 * n:
      dp[i][j] = -1

  echo solve(n, s, a, b, 0, 0, dp)

main()


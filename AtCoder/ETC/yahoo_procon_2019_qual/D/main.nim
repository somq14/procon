import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import future

const INF* = int(1e18 + 373)

proc readLine*(): string =
  stdin.readLine()
proc readSeq*(): seq[string] =
  readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = readLine().strip()
proc readInt1*(): int =
  readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])
proc newSeqWith*[T](n: Natural; e: T): seq[T] =
  result = newSeq[T](n)
  for i in 0..<n:
    result[i] = e
type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] =
  newSeqWith(n1, newSeq[T](n2))
type seq3*[T] = seq[seq[seq[T]]]
proc newSeq3*[T](n1, n2, n3: Natural): seq3[T] =
  newSeqWith(n1, newSeqWith(n2, newSeq[T](n3)))

#------------------------------------------------------------------------------#
proc buildCumTab(a: seq[int]): seq[int] =
  let n = a.len()
  var c = newSeq[int](n + 1)
  for i in 1..n:
    c[i] = c[i - 1] + a[i - 1]
  return c

# [s, t)
proc lookupCumTab(c: seq[int]; s, t: int): int = c[t] - c[s]

proc main() =
  let n = readInt1()
  let a = readSeq(n).map(parseInt)
  let c = a.buildCumTab()

  var dpR = newSeq2[int](n + 1, 2)
  dpR[n][0] = 0
  dpR[n][1] = 0
  for i in countdown(n - 1, 0):
    #dpR[i][0]
    block:
      var opt = c.lookupCumTab(i, n)
      if a[i] mod 2 == 1:
        opt = min(opt, dpR[i + 1][0])
        opt = min(opt, dpR[i + 1][1])
      elif a[i] == 0:
        opt = min(opt, dpR[i + 1][1] + 1)
        opt = min(opt, dpR[i + 1][0] + 1)
      elif a[i] mod 2 == 0:
        opt = min(opt, dpR[i + 1][1])
        opt = min(opt, dpR[i + 1][0] + 1)
      else:
        assert false
      dpR[i][0] = opt

    #dpR[i][1]
    block:
      var opt = c.lookupCumTab(i, n)
      if a[i] == 0:
        opt = min(opt, dpR[i + 1][1] + 2)
      elif a[i] mod 2 == 0:
        opt = min(opt, dpR[i + 1][1] + 0)
      elif a[i] mod 2 == 1:
        opt = min(opt, dpR[i + 1][1] + 1)
      dpR[i][1] = opt

  var dpL = newSeq2[int](n + 1, 2)
  dpL[0][0] = 0
  dpL[0][1] = 0
  for i in 1..n:
    #dpL[i][0]
    block:
      var opt = c.lookupCumTab(0, i)
      if a[i - 1] mod 2 == 1:
        opt = min(opt, dpL[i - 1][0])
        opt = min(opt, dpL[i - 1][1])
      elif a[i - 1] == 0:
        opt = min(opt, dpL[i - 1][1] + 1)
        opt = min(opt, dpL[i - 1][0] + 1)
      elif a[i - 1] mod 2 == 0:
        opt = min(opt, dpL[i - 1][1])
        opt = min(opt, dpL[i - 1][0] + 1)
      else:
        assert false
      dpL[i][0] = opt

    #dpL[i][1]
    block:
      var opt = c.lookupCumTab(0, i)
      if a[i - 1] == 0:
        opt = min(opt, dpL[i - 1][1] + 2)
      elif a[i - 1] mod 2 == 0:
        opt = min(opt, dpL[i - 1][1] + 0)
      elif a[i - 1] mod 2 == 1:
        opt = min(opt, dpL[i - 1][1] + 1)
      dpL[i][1] = opt

  var ans = INF
  for i in 0..n:
    var sol = 0

    sol = dpL[i][1] + dpR[i][0]
    ans = min(ans, sol)

    sol = dpL[i][0] + dpR[i][1]
    ans = min(ans, sol)

  echo ans

main()


import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import logging
import future

const INF* = int(1e18 + 373)

proc readLine*(): string = stdin.readLine()
proc readSeq*(): seq[string] = readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = readLine().strip()
proc readInt1*(): int = readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])
type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] =
  newSeqWith(n1, newSeq[T](n2))
type seq3*[T] = seq[seq[seq[T]]]
proc newSeq3*[T](n1, n2, n3: Natural): seq3[T] =
  newSeqWith(n1, newSeqWith(n2, newSeq[T](n3)))

#------------------------------------------------------------------------------#
type Pos = tuple [ u, v: int ]

proc buildCumTab(a: seq[int]): seq[int] =
  let n = a.len()
  var c = newSeq[int](n + 1)
  for i in 1..n:
    c[i] = c[i - 1] + a[i - 1]
  return c

# [s, t)
proc lookupCumTab(c: seq[int]; s, t: int): int = c[t] - c[s]

proc main() =
  let (h, w) = readInt2()
  var s = newSeq[string](h)
  for i in 0..<h:
    s[i] = stdin.readLine()

  var ps = newSeq[Pos](0)
  for y in 0..<h:
    for x in 0..<w:
      if s[y][x] == '.':
        continue
      ps.add((x + y, x - y + 299))

  var uList = newSeq2[Pos](600, 0)
  var vList = newSeq2[Pos](600, 0)
  for i in 0..<ps.len():
    uList[ps[i].u].add(ps[i])
    vList[ps[i].v].add(ps[i])

  # u : [0, 299 + 299]
  # v : [0, 299 + 299]
  var uTab = newSeq2[int](600, 600)
  var vTab = newSeq2[int](600, 600)
  for i in 0..<ps.len():
    uTab[ps[i].u][ps[i].v] = 1
    vTab[ps[i].v][ps[i].u] = 1

  var uTabCum = newSeq2[int](600, 0)
  var vTabCum = newSeq2[int](600, 0)
  for i in 0..<600:
    uTabCum[i] = uTab[i].buildCumTab()
    vTabCum[i] = vTab[i].buildCumTab()

  var ans1 = 0
  var ans2 = 0

  for u in 0..<600:
    let k = uList[u].len()
    for i in 0..<k:
      for j in (i + 1)..<k:
        let p1 = uList[u][i]
        let p2 = uList[u][j]
        let d = abs(p1.v - p2.v)
        if u - d in 0..<600:
          ans1 += uTab[u - d][p1.v]
          ans1 += uTab[u - d][p2.v]
          ans2 += uTabCum[u - d].lookupCumTab(min(p1.v, p2.v) + 1, max(p1.v, p2.v))
        if u + d in 0..<600:
          ans1 += uTab[u + d][p1.v]
          ans1 += uTab[u + d][p2.v]
          ans2 += uTabCum[u + d].lookupCumTab(min(p1.v, p2.v) + 1, max(p1.v, p2.v))

  for v in 0..<600:
    let k = vList[v].len()
    for i in 0..<k:
      for j in (i + 1)..<k:
        let p1 = vList[v][i]
        let p2 = vList[v][j]
        let d = abs(p1.u - p2.u)
        if v - d in 0..<600:
          ans1 += vTab[v - d][p1.u]
          ans1 += vTab[v - d][p2.u]
          ans2 += vTabCum[v - d].lookupCumTab(min(p1.u, p2.u) + 1, max(p1.u, p2.u))
        if v + d in 0..<600:
          ans1 += vTab[v + d][p1.u]
          ans1 += vTab[v + d][p2.u]
          ans2 += vTabCum[v + d].lookupCumTab(min(p1.u, p2.u) + 1, max(p1.u, p2.u))

  echo ans1 div 2 + ans2

main()


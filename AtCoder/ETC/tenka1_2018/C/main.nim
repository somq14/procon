import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import logging
import future

const INF* = int(1e18 + 373)

proc readLine*(): string = stdin.readLine()
proc readSeq*(): seq[string] = readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = readLine().strip()
proc readInt1*(): int = readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])
type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] =
  newSeqWith(n1, newSeq[T](n2))
type seq3*[T] = seq[seq[seq[T]]]
proc newSeq3*[T](n1, n2, n3: Natural): seq3[T] =
  newSeqWith(n1, newSeqWith(n2, newSeq[T](n3)))

#------------------------------------------------------------------------------#
proc eval(a: seq[int]): int =
  var ans = 0
  for i in 0..<a.len() - 1:
    ans += abs(a[i] - a[i + 1])
  return ans


proc main() =
  let n = readInt1()
  let a = readSeq(n).map(parseInt).sorted(cmp[int])

  if n mod 2 == 0:
    var b = newSeq[int](n)
    for i in 0..<n div 2:
      if i mod 2 == 0:
        b[n div 2 + i] = a[(n - 1) - i]
        b[n div 2 - 1 - i] = a[i]
      else:
        b[n div 2 + i] = a[i]
        b[n div 2 - 1 - i] = a[(n - 1) - i]
    echo eval(b)
  else:
    var b1 = newSeq[int](n)
    block:
      b1[n div 2] = a[0]
      var ll = 1
      var rr = n - 1
      for i in 1..n div 2:
        if i mod 2 == 1:
          b1[n div 2 - i] = a[rr]
          b1[n div 2 + i] = a[rr - 1]
          rr -= 2
        else:
          b1[n div 2 - i] = a[ll]
          b1[n div 2 + i] = a[ll + 1]
          ll += 2
    var b2 = newSeq[int](n)
    block:
      b2[n div 2] = a[n - 1]
      var ll = 0
      var rr = n - 2
      for i in 1..n div 2:
        if i mod 2 == 0:
          b2[n div 2 - i] = a[rr]
          b2[n div 2 + i] = a[rr - 1]
          rr -= 2
        else:
          b2[n div 2 - i] = a[ll]
          b2[n div 2 + i] = a[ll + 1]
          ll += 2
    echo max(eval(b1), eval(b2))

main()


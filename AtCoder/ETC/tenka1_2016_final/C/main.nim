import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import logging
import future

const INF* = int(1e18 + 373)

proc readLine*(): string = stdin.readLine()
proc readSeq*(): seq[string] = readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = readLine().strip()
proc readInt1*(): int = readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])
type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))
type seq3*[T] = seq[seq[seq[T]]]
proc newSeq3*[T](n1, n2, n3: Natural): seq3[T] = newSeqWith(n1, newSeqWith(n2, newSeq[T](n3)))

#------------------------------------------------------------------------------#
type TrieNode[T] = ref object
  child: array['a'..'z', TrieNode[T]]
  valid: bool
  value: T

type Trie[T] = object
  root: TrieNode[T]

proc initTrieNode[T](): TrieNode[T] =
  result.new()

proc initTrie[T](): Trie[T] =
  result.root = initTrieNode[T]()

proc `$`[T](this: TrieNode[T]): string =
  var res = "["
  res &= $this.value
  res &= ", "
  for c in 'a'..'z':
    if this.child[c] == nil:
      continue
    res &= $c
    res &= ":"
    res &= $this.child[c]
    res &= ", "
  res &= "]"
  return res

proc `$`[T](this: Trie[T]): string =
  $this.root

proc `[]`[T](this: Trie[T]; key: string): T =
  var p = this.root
  var i = 0
  while p != nil and i < key.len():
    p = p.child[key[i]]
    i += 1

  if p == nil or i < key.len():
    raise newException(IndexError, "Invalid Key")

  if not p.valid:
    raise newException(IndexError, "Invalid Key")

  return p.value

proc `[]=`[T](this: Trie[T]; key: string; val: T) =
  var p = this.root
  var i = 0
  while p.child[key[i]] != nil and i < key.len():
    p = p.child[key[i]]
    i += 1

  while i < key.len():
    p.child[key[i]] = initTrieNode[T]()
    p = p.child[key[i]]
    i += 1

  p.valid = true
  p.value = val

iterator matches[T](this: Trie[T]; prefix: string; base: int): int =
  var p = this.root
  var i = 0
  while p != nil and base + i < prefix.len():
    if p.valid:
      yield p.value
    p = p.child[prefix[base + i]]
    i += 1

  if p != nil and p.valid:
    yield p.value

proc main() =
  let s = readLine()
  let n = s.len()
  let m = readInt1()

  var ps = newSeq[string](m)
  for i in 0..<m:
    ps[i] = readLine()

  var ws = newSeq[int](m)
  for i in 0..<m:
    ws[i] = readInt1()

  var trie = initTrie[int]()
  for i in 0..<m:
    trie[ps[i]] = i

  var dp = newSeq[int](n + 1)
  dp[n] = 0
  for i in countdown(n - 1, 0):
    var opt = dp[i + 1]
    for j in trie.matches(s, i):
      opt = max(opt, dp[i + ps[j].len()] + ws[j])
    dp[i] = opt

  echo dp[0]

main()


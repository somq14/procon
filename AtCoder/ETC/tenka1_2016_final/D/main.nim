import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import logging
import future

const INF* = int(1e18 + 373)

proc readLine*(): string = stdin.readLine()
proc readSeq*(): seq[string] = readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = readLine().strip()
proc readInt1*(): int = readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])
type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))
type seq3*[T] = seq[seq[seq[T]]]
proc newSeq3*[T](n1, n2, n3: Natural): seq3[T] = newSeqWith(n1, newSeqWith(n2, newSeq[T](n3)))

#------------------------------------------------------------------------------#
let (n, m, c, d) = readInt4()

var cost = newSeq2[int](n, 2)
for i in 0..<n:
  (cost[i][0], cost[i][1]) = readInt2()

var depend = newSeq[int](n)
for i in 0..<m:
  var (x, y) = readInt2()
  x -= 1
  y -= 1
  depend[y] = depend[y] or (1 shl x)

var memo = initTable[(int, int, int), int]()
proc solve(s, r, k: int): int =
  if s == 0:
    return 0

  if (s, r, k) in memo:
    return memo[(s, r, k)]

  let done = ((1 shl n) - 1) xor s

  var ss = 0
  var cc = 0
  for i in 0..<n:
    if (s and (1 shl i)) == 0:
      continue
    if (depend[i] and done) != depend[i]:
      continue
    if cost[i][r] <= cost[i][r xor 1]:
      ss = ss or (1 shl i)
      cc += cost[i][r]

  if ss != 0:
    return solve(s xor ss, r, k) + cc

  var ans = INF

  for i in 0..<n:
    if (s and (1 shl i)) == 0:
      continue
    if (depend[i] and done) != depend[i]:
      continue
    ans = min(ans, solve(s xor (1 shl i), r, k) + cost[i][r])
    ans = min(ans, solve(s xor (1 shl i), r xor 1, k + 1) + (c * k + d) + cost[i][r xor 1])

  memo[(s, r, k)] = ans
  return ans

let sol0 = solve((1 shl n) - 1, 0, 0)
let sol1 = solve((1 shl n) - 1, 1, 0)
echo min(sol0, sol1)

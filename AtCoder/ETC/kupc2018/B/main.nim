import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import logging
import future

const INF* = int(1e18 + 373)

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

proc readInt1*(): int = readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#

proc main() =
  let (h, w) = readInt2()
  var s = newSeq[string](h)
  for i in 0..<h:
    s[i] = stdin.readLine()

  var dp = newSeq2[char](h, w)
  for i in 0..<h:
    dp[i].fill('x')
  for i in 0..<w:
    if s[h - 1][i] == 's':
      dp[0][i] = 'o'
      break

  for t in 0..<h - 1:
    for i in 0..<w:
      if dp[t][i] != 'x':
        dp[t + 1][i] = 'S'
        if i + 1 < w:
          dp[t + 1][i + 1] = 'R'
        if i - 1 >= 0:
          dp[t + 1][i - 1] = 'L'

    for i in 0..<w:
      if s[(h - 2) - t][i] == 'x':
        dp[t + 1][i] = 'x'

  var p = -1
  for i in 0..<w:
    if dp[h - 1][i] != 'x':
      p = i
      break

  if p == -1:
    echo "impossible"
    return

  var cmd = newSeq[string](h - 1)
  for i in countdown(h - 1, 1):
    cmd[i - 1] = $dp[i][p]
    case dp[i][p]:
    of 'R':
      p -= 1
    of 'L':
      p += 1
    of 'S':
      discard
    else:
      assert false

  echo cmd.join("")


main()


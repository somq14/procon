import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import logging
import future

const INF* = int(1e18 + 373)

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

proc readInt1*(): int = readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#
var qCnt = 0
proc query(q: int): int =
  qCnt += 1
  echo "?", " ", q
  let r = stdin.readLine()
  return if r == "even": 0 else: 1

proc answer(ans: int) =
  echo "!", " ", ans

proc main() =
  let evenOrOdd = query(2)
  # [lb, ub)
  var ub = 10^9 + 1
  var lb = 1
  while ub - lb > 2:
    var mid = (((lb div 2) + (ub div 2)) div 2) * 2 + 1
    if mid * 2 < ub:
      mid += 2

    if query(mid) == evenOrOdd:
      ub = mid
    else:
      lb = mid

  answer(if evenOrOdd == 0: lb + 1 else: lb)
  stderr.writeLine("query = ", qCnt)


proc testQuery(p, q: int): int =
  p mod q mod 2

proc testAnswer(p, q: int) =
  if p != q:
    echo "error:", p, " ", q

proc trial(p: int) =
  let evenOrOdd = testQuery(p, 2)
  # [lb, ub)
  var ub = 10^9 + 1
  var lb = 1
  while ub - lb > 2:
    var mid = (((lb div 2) + (ub div 2)) div 2) * 2 + 1
    if mid * 2 < ub:
      mid += 2

    if testQuery(p, mid) == evenOrOdd:
      ub = mid
    else:
      lb = mid

  testAnswer(p, if evenOrOdd == 0: lb + 1 else: lb)

proc test() =
  let (lb, ub) = readInt2()
  for i in lb..ub:
    trial(i)

main()
#test()


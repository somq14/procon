import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import logging
import future

const INF* = int(1e18 + 373)

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

proc readInt1*(): int = readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

type seq3*[T] = seq[seq[seq[T]]]
proc newSeq3*[T](n1, n2, n3: Natural): seq3[T] = newSeqWith(n1, newSeqWith(n2, newSeq[T](n3)))

#------------------------------------------------------------------------------#
proc solveMax(a: string; s, t, m: int): int

var minMemo: seq3[int]
var minMemoValid: seq3[bool]
proc solveMin(a: string; s, t, m: int): int =
  let n = t - s

  if n mod 2 == 0:
    return +INF

  if n == 1:
    if m == 0:
      return if a[s] in '0'..'9': (a[s].ord() - '0'.ord()).int() else: +INF
    else:
      return 0

  if minMemoValid[s][t][m]:
    return minMemo[s][t][m]

  var opt = +INF

  if a[t - 1] == '+':
    for i in (s + 1)..<t - 1:
      for k in 0..m:
        let opt1 = solveMin(a, s, i, k)
        let opt2 = solveMin(a, i, t - 1, m - k)
        if opt1 >= +INF or opt2 >= +INF:
          continue
        opt = min(opt,  opt1 + opt2)

  if a[t - 1] == '-':
    for i in (s + 1)..<t - 1:
      for k in 0..m:
        let opt1 = solveMin(a, s, i, k)
        let opt2 = solveMax(a, i, t - 1, m - k)
        if opt1 >= +INF or opt2 <= -INF:
          continue
        opt = min(opt,  opt1 - opt2)

  if m > 0:
    for i in (s + 1)..<t - 1:
      for k in 0..<m:
        let opt1 = solveMin(a, s, i, k)
        let opt2 = solveMin(a, i, t - 1, (m - 1) - k)
        if opt1 >= +INF or opt2 >= +INF:
          continue
        opt = min(opt,  opt1 + opt2)

    for i in (s + 1)..<t - 1:
      for k in 0..<m:
        let opt1 = solveMin(a, s, i, k)
        let opt2 = solveMax(a, i, t - 1, (m - 1) - k)
        if opt1 >= +INF or opt2 <= -INF:
          continue
        opt = min(opt,  opt1 - opt2)

  minMemoValid[s][t][m] = true
  minMemo[s][t][m] = opt
  return opt

var maxMemo: seq3[int]
var maxMemoValid: seq3[bool]
proc solveMax(a: string; s, t, m: int): int =
  let n = t - s

  if n mod 2 == 0:
    return -INF

  if n == 1:
    if m == 0:
      return if a[s] in '0'..'9': (a[s].ord() - '0'.ord()).int() else: -INF
    else:
      return 9

  if maxMemoValid[s][t][m]:
    return maxMemo[s][t][m]

  var opt = -INF
  if a[t - 1] == '+':
    for i in (s + 1)..<t - 1:
      for k in 0..m:
        let opt1 = solveMax(a, s, i, k)
        let opt2 = solveMax(a, i, t - 1, m - k)
        if opt1 <= -INF or opt2 <= -INF:
          continue
        opt = max(opt,  opt1 + opt2)

  if a[t - 1] == '-':
    for i in (s + 1)..<t - 1:
      for k in 0..m:
        let opt1 = solveMax(a, s, i, k)
        let opt2 = solveMin(a, i, t - 1, m - k)
        if opt1 <= -INF or opt2 >= INF:
          continue
        opt = max(opt,  opt1 - opt2)

  if m > 0:
    for i in (s + 1)..<t - 1:
      for k in 0..<m:
        let opt1 = solveMax(a, s, i, k)
        let opt2 = solveMax(a, i, t - 1, (m - 1) - k)
        if opt1 <= -INF or opt2 <= -INF:
          continue
        opt = max(opt,  opt1 + opt2)

    for i in (s + 1)..<t - 1:
      for k in 0..<m:
        let opt1 = solveMax(a, s, i, k)
        let opt2 = solveMin(a, i, t - 1, (m - 1) - k)
        if opt1 <= -INF or opt2 >= INF:
          continue
        opt = max(opt,  opt1 - opt2)

  maxMemoValid[s][t][m] = true
  maxMemo[s][t][m] = opt
  return opt

proc main() =
  let m = readInt1()
  let a = stdin.readLine()
  let n = a.len()

  maxMemoValid = newSeq3[bool](n, n + 1, m + 1)
  maxMemo = newSeq3[int](n, n + 1, m + 1)
  minMemoValid = newSeq3[bool](n, n + 1, m + 1)
  minMemo = newSeq3[int](n, n + 1, m + 1)

  let ans = solveMax(a, 0, n, m)
  echo if ans <= -INF: "NG" else: "OK"
  if ans > -INF:
    echo ans

main()


import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import logging
import future

const INF* = int(1e18 + 373)

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

proc readInt1*(): int = readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#

proc main() =
  let (h, w) = readInt2()
  let (a, b) = readInt2()

  var s = newSeq[string](h)
  for y in 0..<h:
    s[y] = stdin.readLine()

  var pat1 = 0
  var pat2X = 0
  var pat2Y = 0
  var pat2Z = 0
  var pat3 = 0
  var pat4 = 0
  for y in 0..<h div 2:
    for x in 0..<w div 2:
      let p0 = if s[y][x] == 'S': 1 else: 0
      let p1 = if s[h - 1 - y][x] == 'S': 1 else: 0
      let p2 = if s[y][w - 1 - x] == 'S': 1 else: 0
      let p3 = if s[h - 1 - y][w - 1 - x] == 'S': 1 else: 0
      case p0 + p1 + p2 + p3:
      of 1:
        pat1 += 1
      of 2:
        if p0 == p1 and p2 == p3:
          pat2X += 1
        elif p0 == p2 and p1 == p3:
          pat2Y += 1
        else:
          pat2Z += 1
      of 3:
        pat3 += 1
      of 4:
        pat4 += 1
      else:
        discard

  # echo "pat1  = ", pat1
  # echo "pat2X = ", pat2X
  # echo "pat2Y = ", pat2Y
  # echo "pat2Z = ", pat2Z
  # echo "pat3  = ", pat3
  # echo "pat4  = ", pat4

  var ansX = 0
  if pat1 == 0 and pat2Y == 0 and pat2Z == 0 and pat3 == 0:
    ansX -= a
  if pat1 == 0 and pat2X == 0 and pat2Z == 0 and pat3 == 0:
    ansX -= b
  ansX += (pat2X + pat3) * a
  ansX += pat4 * (max(a, b) + a + b)
  ansX += a + b

  var ansY = 0
  if pat1 == 0 and pat2Y == 0 and pat2Z == 0 and pat3 == 0:
    ansY -= a
  if pat1 == 0 and pat2X == 0 and pat2Z == 0 and pat3 == 0:
    ansY -= b
  ansY += (pat2Y + pat3) * b
  ansY += pat4 * (max(a, b) + a + b)
  ansY += a + b

  echo max(ansX, ansY)

main()


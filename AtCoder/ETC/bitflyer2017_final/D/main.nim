import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import logging
import future

const INF* = int(1e18 + 373)

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

proc readInt1*(): int = readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#
proc bitwidth(v: int): int =
  var lb = -1
  var ub = 64
  # (lb, ub]
  while ub - lb > 1:
    let mid = lb + (ub - lb) div 2
    if v shr mid == 0:
      ub = mid
    else:
      lb = mid
  return ub

#------------------------------------------------------------------------------#

# proc dump(a: seq[int]) =
#   var w = 0
#   for i in 0..<a.len():
#     w = max(w, a[i].bitwidth())
# 
#   for i in 0..<a.len():
#     for j in countdown(w - 1, 0):
#       stdout.write(if (a[i] and (1 shl j)) != 0: '1' else: '0')
#     stdout.write('\n')

proc normalize(a: seq[int]): seq[int] =
  var a = a
  var w = 0
  for i in 0..<a.len():
    w = max(w, a[i].bitwidth())

  var k = 0
  for j in countdown(w - 1, 0):
    var pib = -1
    for i in k..<a.len():
      if (a[i] and (1 shl j)) != 0:
        pib = i
        break

    if pib == -1:
      continue

    swap(a[k], a[pib])

    for i in 0..<a.len():
      if i == k:
        continue
      if (a[i] and (1 shl j)) != 0:
        a[i] = a[i] xor a[k]

    k += 1

  a.sort(cmp[int])
  a.reverse()
  return a

proc main() =
  let n = readInt1()
  let a = readSeq().map(parseInt)
  let b = readSeq().map(parseInt)
  echo if a.normalize() == b.normalize(): "Yes" else: "No"

main()


import strutils
import sequtils
import algorithm
import math
import queues
import tables
import logging
import future
import streams

const INF* = int(1e18 + 373)

when not defined(release):
  addHandler(newFileLogger(stderr, lvlAll, "[ $levelname ] "))

proc readInt1*(): int = stdin.readLine().strip().parseInt()
proc readInt2*(): (int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1])
proc readInt3*(): (int, int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1], v[2])

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#
let dy = [-1, +1, 0, 0]
let dx = [0, 0, -1, +1]
proc ok(h, w: int; a: seq2[bool]; y, x: int): bool =
  if a[y][x]:
    return false

  for k in 0..<4:
    let nextY = y + dy[k]
    let nextX = x + dx[k]
    if nextY notin 0..<h or nextX notin 0..<w:
      continue
    if a[nextY][nextX]:
      return true

  return false

proc main() =
  let (h, w) = readInt2()

  if h == 1 and w == 2:
    echo "First"
    echo 1, " ", 2
    return
  if h == 2 and w == 1:
    echo "First"
    echo 2, " ", 1
    return

  var rest = h * w - 2
  if h - 2 in 0..<h:
    rest -= 1
  if w - 2 in 0..<w:
    rest -= 1

  var turn = rest mod 2 != 0
  if turn:
    echo "First"
  else:
    echo "Second"

  var a = newSeq2[bool](h, w)
  a[0][0] = true

  while true:
    if not turn:
      let (y, x) = readInt2()
      if x == -1 and y == -1:
        break
      a[y - 1][x - 1] = true
      turn = true
      continue

    if ok(h, w, a, h - 1, w - 1):
      echo(h, " ", w)
      turn = false
      continue

    block loop:
      for y in 0..<h:
        for x in 0..<w:
          if a[y][x]:
            continue
          if y == h - 2 and x == w - 1:
            continue
          if y == h - 1 and x == w - 2:
            continue
          if ok(h, w, a, y, x):
            a[y][x] = true
            echo(y + 1, " ", x + 1)
            break loop
    turn = false

main()


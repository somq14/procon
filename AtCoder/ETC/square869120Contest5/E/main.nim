import strutils
import sequtils
import algorithm
import math
import queues
import tables
import logging
import future
import streams

const INF* = int(1e18 + 373)

when not defined(release):
  addHandler(newFileLogger(stderr, lvlAll, "[ $levelname ] "))

proc readInt1*(): int = stdin.readLine().strip().parseInt()
proc readInt2*(): (int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1])
proc readInt3*(): (int, int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1], v[2])

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

# FIXME: Use object
type PriorityQueue[T] = tuple[a: seq[T], n: Natural, cmp: proc(a, b: T): bool]

proc initPriorityQueue[T](cmp: proc(a, b: T): bool): PriorityQueue[T] =
    (newSeq[T](1), Natural(0), cmp)

proc len[T](this: PriorityQueue[T]): Natural = this.n

proc enqueue[T](this: var PriorityQueue[T], e: T) =
    this.n += 1
    this.a.add(e)
    var i = this.a.len() - 1
    while i > 1 and this.cmp(this.a[i], this.a[i div 2]):
        swap(this.a[i], this.a[i div 2])
        i = i div 2

proc dequeue[T](this: var PriorityQueue[T]): T =
    this.n -= 1
    result = this.a[1]
    this.a[1] = this.a.pop()
    var i = 1
    while i < this.a.len():
        var j = i
        if i * 2 < this.a.len() and this.cmp(this.a[i * 2], this.a[j]):
            j = i * 2
        if i * 2 + 1 < this.a.len() and this.cmp(this.a[i * 2 + 1], this.a[j]):
            j = i * 2 + 1
        if i == j:
            break
        swap(this.a[i], this.a[j])
        i = j

# FIXME: throw Exception when this is empty
proc front[T](this: PriorityQueue[T]): T = this.a[1]

#------------------------------------------------------------------------------#

type Pos = tuple[ y, x: int ]
type Edge = tuple[ to, c: int ]

proc dijkstra(n: int; g: seq2[Edge]; s: int): seq[Table[int, int]] =
  var d = newSeq[Table[int, int]](n)
  for i in 0..<n:
    d[i] = initTable[int, int]()

  type P = tuple[c, k, v: int]
  var q = initPriorityQueue[P]((p1: P, p2: P) => p1.c < p2.c)

  for k in 0..n:
    d[s][k] = 0
    q.enqueue((0, k, s))

  while q.len() > 0:
    let (c, k, v) = q.dequeue()
    if c > d[v][k]:
      continue

    if k <= 0:
      continue

    for e in g[v]:
      let alt = d[v][k] + e.c * k
      if k - 1 notin d[e.to] or alt < d[e.to][k - 1]:
        d[e.to][k - 1] = alt
        q.enqueue((alt, k - 1, e.to))
  return d

#------------------------------------------------------------------------------#

proc main() =
  let (h, w) = readInt2()

  var c = newSeq[string](h)
  for i in 0..<h:
    c[i] = stdin.readLine()

  var gy = 0
  var gx = 0
  block searchGoal:
    for y in 0..<h:
      for x in 0..<w:
        if c[y][x] == 'G':
          gy = y
          gx = x
          c[y][x] = '#'
          break searchGoal

  var ctable = initTable[Pos, int]()
  var n = 0
  for y in 0..<h:
    for x in 0..<w:
      if c[y][x] == '#':
        n += 1
        ctable[(y, x)] = ctable.len()

  var row = newSeq2[int](h, 0)
  for y in 0..<h:
    for x in 0..<w:
      if c[y][x] == '#':
        row[y].add(x)

  var col = newSeq2[int](w, 0)
  for x in 0..<w:
    for y in 0..<h:
      if c[y][x] == '#':
        col[x].add(y)

  var g = newSeq2[Edge](n, 0)
  for y in 0..<h:
    for i in 1..<row[y].len():
      let x1 = row[y][i - 1]
      let x2 = row[y][i]
      let v1 = ctable[(y, x1)]
      let v2 = ctable[(y, x2)]
      let d = abs(x1 - x2)
      g[v1].add((v2, d))
      g[v2].add((v1, d))

  for x in 0..<w:
    for i in 1..<col[x].len():
      let y1 = col[x][i - 1]
      let y2 = col[x][i]
      let v1 = ctable[(y1, x)]
      let v2 = ctable[(y2, x)]
      let d = abs(y1 - y2)
      g[v1].add((v2, d))
      g[v2].add((v1, d))

  let d = dijkstra(n, g, ctable[(gy, gx)])

  var ans = newSeq2[int](h, w)
  for y in 0..<h:
    for x in 0..<w:
      ans[y][x] = INF

  for y in 0..<h:
    for x in 0..<w:
      let px = row[y].lowerBound(x)
      if px in 0..<row[y].len() and row[y][px] == x:
        let v = ctable[(y, x)]
        if 0 in d[v]:
          ans[y][x] = min(ans[y][x], d[v][0])
        continue

      if px in 0..<row[y].len():
        let v = ctable[(y, row[y][px])]
        if 1 in d[v]:
          let c = abs(x - row[y][px])
          ans[y][x] = min(ans[y][x], d[v][1] + c)

      if px - 1 in 0..<row[y].len():
        let v = ctable[(y, row[y][px - 1])]
        if 1 in d[v]:
          let c = abs(x - row[y][px - 1])
          ans[y][x] = min(ans[y][x], d[v][1] + c)

  for x in 0..<w:
    for y in 0..<h:
      let py = col[x].lowerBound(y)
      if py in 0..<col[x].len() and col[x][py] == y:
        let v = ctable[(y, x)]
        if 0 in d[v]:
          ans[y][x] = min(ans[y][x], d[v][0])
        continue

      if py in 0..<col[x].len():
        let v = ctable[(col[x][py], x)]
        if 1 in d[v]:
          let c = abs(y - col[x][py])
          ans[y][x] = min(ans[y][x], d[v][1] + c)

      if py - 1 in 0..<col[x].len():
        let v = ctable[(col[x][py - 1], x)]
        if 1 in d[v]:
          let c = abs(y - col[x][py - 1])
          ans[y][x] = min(ans[y][x], d[v][1] + c)

  for y in 0..<h:
    for x in 0..<w:
      if ans[y][x] >= INF div 2:
        ans[y][x] = -1
    echo ans[y].map(it => $it).join(" ")


main()


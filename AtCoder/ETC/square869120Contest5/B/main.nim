import strutils
import sequtils
import algorithm
import math
import queues
import tables
import logging
import future

const INF* = int(1e18 + 373)

when not defined(release):
  addHandler(newFileLogger(stderr, lvlAll, "[ $levelname ] "))

proc readInt1*(): int = stdin.readLine().strip().parseInt()
proc readInt2*(): (int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1])
proc readInt3*(): (int, int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1], v[2])

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#
type Circle = tuple[ y, x, r: float ]

proc conflict(c1, c2: Circle): bool =
  let dy = c1.y - c2.y
  let dx = c1.x - c2.x
  let d = sqrt(dy * dy + dx * dx)
  return d < c1.r + c2.r

proc eval(n, m: int; cs: seq[Circle]; x: float): bool =
  var cs = cs
  for i in n..<(n + m):
    cs[i].r = x

  for i in 0..<(n + m):
    for j in (i + 1)..<(n + m):
      if conflict(cs[i], cs[j]):
        return false
  return true

proc main() =
  let (n, m) = readInt2()

  var cs = newSeq[Circle](n + m)
  for i in 0..<n:
    let (x, y, r) = readInt3()
    cs[i] = (y.float(), x.float(), r.float())

  for i in n..<(n + m):
    let (x, y) = readInt2()
    cs[i] = (y.float(), x.float(), 0.0)

  var minR = INF.float()
  for i in 0..<n:
    minR = min(minR, cs[i].r)

  if m == 0:
    echo minR
    return

  var ub = 800.0
  var lb = 0.0
  # [lb, ub)
  for i in 0..2048:
    let mid = (ub + lb) / 2
    if eval(n, m, cs, mid):
      lb = mid
    else:
      ub = mid

  if n == 0:
    echo lb
  else:
    echo min(minR, lb)

main()


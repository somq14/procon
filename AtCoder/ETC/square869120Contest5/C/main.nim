import strutils
import sequtils
import algorithm
import math
import queues
import tables
import logging
import future

const INF* = int(1e18 + 373)

when not defined(release):
  addHandler(newFileLogger(stderr, lvlAll, "[ $levelname ] "))

proc readInt1*(): int = stdin.readLine().strip().parseInt()
proc readInt2*(): (int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1])
proc readInt3*(): (int, int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1], v[2])

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#
var memo: Table[(int, int, int, int, int), bool]

proc solve(s: string; i, a, b, acnt, bcnt: int): bool =
  if i >= s.len():
    return a == 0 and b == 0 and acnt == bcnt

  if a + b > s.len() - i:
    return false
  if acnt + a > s.len() div 2:
    return false
  if bcnt + b > s.len() div 2:
    return false

  let state = (i, a, b, acnt, bcnt)
  if state in memo:
    return memo[state]


  if s[i] == '(' and solve(s, i + 1, a + 1, b, acnt, bcnt):
    memo[state] = true
    return true
  if s[i] == '(' and b > 0 and solve(s, i + 1, a, b - 1, acnt, bcnt + 1):
    memo[state] = true
    return true
  if s[i] == ')' and solve(s, i + 1, a, b + 1, acnt, bcnt):
    memo[state] = true
    return true
  if s[i] == ')' and a > 0 and solve(s, i + 1, a - 1, b, acnt + 1, bcnt):
    memo[state] = true
    return true

  memo[state] = false
  return false

proc main() =
  let q = readInt1()
  for i in 0..<q:
    let s = stdin.readLine()
    memo = initTable[(int, int, int, int, int), bool]()
    if solve(s, 0, 0, 0, 0, 0):
      echo "Yes"
    else:
      echo "No"

main()


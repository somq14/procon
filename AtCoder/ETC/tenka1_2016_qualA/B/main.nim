import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import future

const INF* = int(1e18 + 373)

proc readLine*(): string =
  stdin.readLine()
proc readSeq*(): seq[string] =
  readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = readLine().strip()
proc readInt1*(): int =
  readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])
proc newSeqWith*[T](n: Natural; e: T): seq[T] =
  result = newSeq[T](n)
  for i in 0..<n:
    result[i] = e
type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] =
  newSeqWith(n1, newSeq[T](n2))
type seq3*[T] = seq[seq[seq[T]]]
proc newSeq3*[T](n1, n2, n3: Natural): seq3[T] =
  newSeqWith(n1, newSeqWith(n2, newSeq[T](n3)))

#------------------------------------------------------------------------------#
proc dfs(g: seq2[int]; x: seq[int]; v: int; ans: var int): int =
  if g[v].len() == 0:
    return x[v]

  var res = newSeq[int](0)
  for u in g[v]:
    res.add(dfs(g, x, u, ans))

  let m = min(res)
  for i in 0..<g[v].len():
    ans += res[i] - m

  return m

proc solve(g: seq2[int]; x: seq[int]): int =
  var ans = 0
  let res = dfs(g, x, 0, ans)
  ans += res * g[0].len()
  return ans

proc main() =
  let (n, m) = readInt2()
  var g = newSeq2[int](n, 0)
  for i in 1..<n:
    let p = readInt1()
    g[p].add(i)

  var x = newSeq[int](n)
  for i in 0..<m:
    let (b, c) = readInt2()
    x[b] = c

  echo solve(g, x)

main()


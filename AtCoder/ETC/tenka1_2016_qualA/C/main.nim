import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import logging
import future

const INF* = int(1e18 + 373)

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

proc readInt1*(): int = readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#
proc solve(n: int; ss: seq[(string, string)]): string =
  let m = 'z'.ord() - 'a'.ord() + 1
  var g = newSeq[int](m)
  for i in 0..<n:
    let s = ss[i]

    var p = 0
    while p < min(s[0].len(), s[1].len()) and s[0][p] == s[1][p]:
      p += 1

    if p >= min(s[0].len(), s[1].len()):
      if s[0].len() > s[1].len():
        return "-1"
      continue

    g[s[1][p].ord() - 'a'.ord()] = g[s[1][p].ord() - 'a'.ord()] or (1 shl (s[0][p].ord() - 'a'.ord()))

  var ans = newSeq[int]()
  var used = newSeq[bool](m)
  while ans.len() < m:
    var target = -1
    for c in 0..<m:
      if used[c]:
        continue
      if g[c] == 0:
        target = c
        break

    if target == -1:
      return "-1"

    used[target] = true
    ans.add(target)
    for c in 0..<m:
      g[c] = g[c] and not (1 shl target)

  var res = ""
  for i in 0..<m:
    res = res & $('a'.ord() + ans[i]).char()
  return res

proc main() =
  let n = readInt1()
  var ss = newSeq[(string, string)](n)
  for i in 0..<n:
    let s = stdin.readLine().split()
    ss[i] = (s[0], s[1])
  echo solve(n, ss)

main()


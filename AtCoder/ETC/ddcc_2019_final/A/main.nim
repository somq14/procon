import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import future

const INF* = int(1e18 + 373)

proc readLine*(): string =
  stdin.readLine()
proc readSeq*(): seq[string] =
  readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = readLine().strip()
proc readInt1*(): int =
  readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])
proc newSeqWith*[T](n: Natural; e: T): seq[T] =
  result = newSeq[T](n)
  for i in 0..<n:
    result[i] = e
type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] =
  newSeqWith(n1, newSeq[T](n2))
type seq3*[T] = seq[seq[seq[T]]]
proc newSeq3*[T](n1, n2, n3: Natural): seq3[T] =
  newSeqWith(n1, newSeqWith(n2, newSeq[T](n3)))

#------------------------------------------------------------------------------#
proc eval(ll: int): float =
  var ans = 0.0
  for k in 0..<ll:
    ans += 1.0 / (k.float() + 2.0)
  return ans

proc main() =
  let n = readInt1()
  let s = readLine()

  var snow = 0
  for i in 0..<n:
    if s[i] == '-':
      snow += 1

  var t = initTable[int, int]()
  block:
    var i = 0
    while i < n:
      while i < n and s[i] == '-':
        i += 1

      var j = i
      while j < n and s[j] == '>':
        j += 1

      let m = j - i
      if m notin t:
        t[m] = 0
      t[m] += 1

      i = j

  var maxLen = 0
  for ll in t.keys():
    maxLen = max(maxLen, ll)

  t[maxLen] -= 1
  if (maxLen + 1) notin t:
    t[maxLen + 1] = 0
  t[maxLen + 1] += 1

  var ans = (snow - 1).float()
  for ll in t.keys():
    ans += t[ll].float() * eval(ll)
  echo ans

main()


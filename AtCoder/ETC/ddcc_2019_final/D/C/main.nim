import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import future

const INF* = int(1e18 + 373)

proc readLine*(): string =
  stdin.readLine()
proc readSeq*(): seq[string] =
  readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = readLine().strip()
proc readInt1*(): int =
  readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])
proc newSeqWith*[T](n: Natural; e: T): seq[T] =
  result = newSeq[T](n)
  for i in 0..<n:
    result[i] = e
type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] =
  newSeqWith(n1, newSeq[T](n2))
type seq3*[T] = seq[seq[seq[T]]]
proc newSeq3*[T](n1, n2, n3: Natural): seq3[T] =
  newSeqWith(n1, newSeqWith(n2, newSeq[T](n3)))

#------------------------------------------------------------------------------#
const DISCO = "DISCO"
const MOD = 1 shl 32

proc addM(a, b: Natural): Natural = (a + b) mod MOD
proc subM(a, b: Natural): Natural = (a - b + MOD) mod MOD
proc mulM(a, b: Natural): Natural = a * b mod MOD

#------------------------------------------------------------------------------#
proc solve(dp: seq3[int]; ll, rr, s, t: int): int =
  let m = t - s
  if m == 0:
    return 1
  if m == 1:
    return dp[rr][s][t] - dp[ll][s][t]

  var ans = dp[rr][s][t]
  for i in 1..m:
    let comb = dp[ll][s][s + i].mulM(solve(dp, ll, rr, s + i, t))
    ans = ans.subM(comb)
  return ans

proc main() =
  let a = readLine()
  let n = a.len()
  let q = readInt1()
  var ss = newSeq[int](q)
  var tt = newSeq[int](q)
  for i in 0..<q:
    var (ll, rr) = readInt2()
    ll -= 1
    rr -= 1
    ss[i] = ll
    tt[i] = rr + 1

  var dp = newSeq3[int](n + 1, 6, 6)
  for s in 0..5:
    for t in s..5:
      if s == t:
        dp[0][s][t] = 1

  for i in 1..n:
    for s in 0..5:
      for t in s..5:
        if s == t:
          dp[i][s][t] = 1
          continue
        dp[i][s][t] = dp[i - 1][s][t]
        if a[i - 1] == DISCO[t - 1]:
          dp[i][s][t] = dp[i][s][t].addM(dp[i - 1][s][t - 1])

  for i in 0..<q:
    var ans = 0
    let s = ss[i]
    let t = tt[i]
    echo solve(dp, s, t, 0, 5)

main()


import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import future

const INF* = int(1e18 + 373)

proc readLine*(): string =
  stdin.readLine()
proc readSeq*(): seq[string] =
  readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = readLine().strip()
proc readInt1*(): int =
  readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])
proc newSeqWith*[T](n: Natural; e: T): seq[T] =
  result = newSeq[T](n)
  for i in 0..<n:
    result[i] = e
type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] =
  newSeqWith(n1, newSeq[T](n2))
type seq3*[T] = seq[seq[seq[T]]]
proc newSeq3*[T](n1, n2, n3: Natural): seq3[T] =
  newSeqWith(n1, newSeqWith(n2, newSeq[T](n3)))

#------------------------------------------------------------------------------#

const MAX_TREE_DEPTH = 20

# pre-cond
#   ps[v] = v           ( v is root )
#   ps[v] = parent of v ( v is not root )
#
# post-cond
#   pt[i][v] = (2^i)-th parent of v
proc buildParentTable(ps: seq[int]): seq2[int] =
  let n = ps.len()
  result = newSeq2[int](MAX_TREE_DEPTH + 1, n)

  for v in 0..<n:
    result[0][v] = ps[v]

  for i in 1..MAX_TREE_DEPTH:
    for v in 0..<n:
      result[i][v] = result[i - 1][result[i - 1][v]]

proc parent(pt: seq2[int]; v, i: int): int =
  var p = v
  var j = i
  var k = 0

  while j > 0:
    if (j and 1) == 1:
      p = pt[k][p]
    j = j shr 1
    k += 1

  return p

proc buildDepthTableHelper(ps: seq[int]; v: int; dep: var seq[int]): int =
  if dep[v] >= 0:
    return dep[v]
  dep[v] = buildDepthTableHelper(ps, ps[v], dep) + 1
  return dep[v]

proc buildDepthTable(ps: seq[int]): seq[int] =
  let n = ps.len()

  var root = -1
  for v in 0..<n:
    if ps[v] == v:
      root = v
      break

  result = newSeq[int](n)
  result.fill(-1)
  result[root] = root
  for v in 0..<n:
    discard buildDepthTableHelper(ps, v, result)

proc depth(pt: seq2[int]; v: int): int =
  var lb = -1
  var ub = 1 shl MAX_TREE_DEPTH
  while ub - lb > 1:
    let mid = (ub + lb) div 2
    if pt.parent(v, mid) == pt.parent(v, mid + 1):
      ub = mid
    else:
      lb = mid
  return ub

proc lca(pt: seq2[int]; dep: seq[int]; u, v: int): int =
  if pt[MAX_TREE_DEPTH][u] != pt[MAX_TREE_DEPTH][v]:
    return -1

  var u = u
  var v = v
  let du = dep[u]
  let dv = dep[v]
  if du < dv:
    swap(u, v)
  u = pt.parent(u, abs(du - dv))

  if u == v:
    return u

  var lb = -1
  var ub = 1 shl MAX_TREE_DEPTH
  while ub - lb > 1:
    let mid = (ub + lb) div 2
    if pt.parent(u, mid) == pt.parent(v, mid):
      ub = mid
    else:
      lb = mid

  return pt.parent(u, ub)

#------------------------------------------------------------------------------#

proc main() =
  let q = readInt1()
  var a = newSeq[(int, int)]()
  var b = newSeq[(int, int)]()

  var n = 0
  for _ in 0..<q:
    let input = readSeq()
    let c = input[0][0]
    let i = input[1].parseInt()
    let j = input[2].parseInt()
    n = max(n, j)

    if c == 'A':
      a.add((i - 1, j - 1))
    else:
      b.add((i - 1, j - 1))

  var ps = newSeq[int](n)
  for i in 0..<a.len():
    ps[a[i][1]] = a[i][0]

  let pt = ps.buildParentTable()
  let dep = ps.buildDepthTable()
  for i in 0..<b.len():
    let anc = pt.lca(dep, b[i][0], b[i][1])
    let ans = (pt.depth(b[i][0]) - pt.depth(anc)) + (pt.depth(b[i][1]) - pt.depth(anc)) - 1
    echo ans

main()


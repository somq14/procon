import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import future

const INF* = int(1e18 + 373)

proc readLine*(): string =
  stdin.readLine()
proc readSeq*(): seq[string] =
  readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = readLine().strip()
proc readInt1*(): int =
  readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])
proc newSeqWith*[T](n: Natural; e: T): seq[T] =
  result = newSeq[T](n)
  for i in 0..<n:
    result[i] = e
type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] =
  newSeqWith(n1, newSeq[T](n2))
type seq3*[T] = seq[seq[seq[T]]]
proc newSeq3*[T](n1, n2, n3: Natural): seq3[T] =
  newSeqWith(n1, newSeqWith(n2, newSeq[T](n3)))

#------------------------------------------------------------------------------#
type Pos =
  tuple [ y, x: int ]

const R: Pos = (0, +1)
const L: Pos = (0, -1)
const U: Pos = (-1, 0)
const D: Pos = (+1, 0)
const dir = [ R, D, L, U ]

proc `+`(p1, p2: Pos): Pos =
  (p1.y + p2.y, p1.x + p2.x)

proc `-`(p1, p2: Pos): Pos =
  (p1.y - p2.y, p1.x - p2.x)

var H = INF
var W = INF

proc setArea(h, w: int) =
  H = h
  W = w

proc valid(p: Pos): bool =
  p.y in 0..<H and p.x in 0..<W

iterator neighbors(p: Pos): Pos =
  yield p + R
  yield p + D
  yield p + L
  yield p + U

proc `[]`[T](a: seq2[T]; p: Pos): T =
  a[p.y][p.x]

proc `[]=`[T](a: var seq2[T]; p: Pos; v: T) =
  a[p.y][p.x] = v

#------------------------------------------------------------------------------#

type State = tuple [ p: Pos; d, a, b: int ]

proc main() =
  let (a, b) = readInt2()
  let (h, w) = readInt2()

  var c = newSeq2[char](h, 0)
  for i in 0..<h:
    c[i] = readLine().map(it => it)

  var q = initQueue[State]()
  var visited = initSet[State]()

  q.enqueue(((1, 1), 1, a, b))
  visited.incl(((1, 1), 1, a, b))

  while q.len() > 0:
    let (p, d, a, b) = q.dequeue()

    block:
      let nextState: State = (p + dir[d], d, a, b)
      if c[nextState.p] == '.' and nextState notin visited:
        q.enqueue(nextState)
        visited.incl(nextState)

    block:
      let nextState: State = (p + dir[(d + 1) mod 4], (d + 1) mod 4, a, b - 1)
      if b > 0 and c[nextState.p] == '.' and nextState notin visited:
        q.enqueue(nextState)
        visited.incl(nextState)

    block:
      let nextState: State = (p + dir[(d - 1 + 4) mod 4], (d - 1 + 4) mod 4, a - 1, b)
      if a > 0 and c[nextState.p] == '.' and nextState notin visited:
        q.enqueue(nextState)
        visited.incl(nextState)

  let ans = ((h - 2, w - 2), 0, 0, 0) in visited or
    ((h - 2, w - 2), 1, 0, 0) in visited or
    ((h - 2, w - 2), 2, 0, 0) in visited or
    ((h - 2, w - 2), 3, 0, 0) in visited
  echo if ans: "Yes" else: "No"

main()


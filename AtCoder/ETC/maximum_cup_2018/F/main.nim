import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import future

const INF* = int(1e18 + 373)

proc readLine*(): string =
  stdin.readLine()
proc readSeq*(): seq[string] =
  readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = readLine().strip()
proc readInt1*(): int =
  readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])
proc newSeqWith*[T](n: Natural; e: T): seq[T] =
  result = newSeq[T](n)
  for i in 0..<n:
    result[i] = e
type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] =
  newSeqWith(n1, newSeq[T](n2))
type seq3*[T] = seq[seq[seq[T]]]
proc newSeq3*[T](n1, n2, n3: Natural): seq3[T] =
  newSeqWith(n1, newSeqWith(n2, newSeq[T](n3)))

#-----------------------------------------------------------------------------#
const MOD = int(10^9+7)

type Op1[T] = proc(a: T, b: T): T
type Op2[T] = proc(a: T, b: T): T

proc repeatedSquares[T](x: T, n: Natural, e: T, f: Op2[T]): T =
    result = e
    var xx = f(x, e)
    var i = n
    while i > 0:
        if (i and 1) != 0:
            result = f(result, xx)
        xx = f(xx, xx)
        i = i shr 1

proc addM(a, b: Natural): Natural = (a + b) mod MOD
proc subM(a, b: Natural): Natural = (a - b + MOD) mod MOD
proc mulM(a, b: Natural): Natural = a * b mod MOD
proc powM(a, b: Natural): Natural = repeatedSquares[Natural](a, b, 1, `mulM`)
proc invM(a: Natural): Natural = a.powM(MOD - 2)
proc divM(a, b: Natural): Natural = a.mulM(invM(b))

var memoFactM: seq[Natural] = nil
var memoFactInvM: seq[Natural] = nil

proc buildFactTable(n: Natural) =
  memoFactM = newSeq[Natural](n + 1)
  memoFactM[0] = 1
  for i in 1..n:
    memoFactM[i] = memoFactM[i - 1].mulM(i)

  memoFactInvM = newSeq[Natural](n + 1)
  memoFactInvM[n] = invM(memoFactM[n])
  for i in countdown(n - 1, 0):
    memoFactInvM[i] = memoFactInvM[i + 1].mulM(i + 1)

buildFactTable(10^6)

proc factM(n: Natural): Natural = memoFactM[n]

proc factInvM(n: Natural): Natural = memoFactInvM[n]

proc combM(n, r: Natural): Natural =
  if r > n:
    return 0
  if r > n div 2:
    return combM(n, n - r)

  result = factM(n).mulM(factInvM(n - r)).mulM(factInvM(r))

proc multCombM(n, r: Natural): Natural = combM(n + r - 1, r - 1)

#------------------------------------------------------------------------------#
#------------------------------------------------------------------------------#
proc toString(k, p: int): string =
  result = ""
  var p = p
  for i in 0..<k:
    let v = (p and 7)
    result &= $v
    p = p shr 3

proc eval(k, p: int): (int, int, int) =
  result = (0, 0, 0)

  var p = p
  for i in 0..<k:
    if (p and 1) > 0:
      result[0] += 1
    if (p and 2) > 0:
      result[1] += 1
    if (p and 4) > 0:
      result[2] += 1
    p = p shr 3

proc enumerate(k, i, pat, ll: int; res: var seq[int]) =
  if i == 0:
    let cnt = eval(k, pat)
    if cnt[0] <= ll and cnt[1] <= ll and cnt[2] <= ll:
      res.add(pat)
    return

  enumerate(k, i - 1, (pat shl 3) or 1, ll, res)
  enumerate(k, i - 1, (pat shl 3) or 2, ll, res)
  enumerate(k, i - 1, (pat shl 3) or 3, ll, res)
  enumerate(k, i - 1, (pat shl 3) or 4, ll, res)
  enumerate(k, i - 1, (pat shl 3) or 5, ll, res)
  enumerate(k, i - 1, (pat shl 3) or 6, ll, res)
  enumerate(k, i - 1, (pat shl 3) or 7, ll, res)

proc solve(n, k: int; g: seq2[int]): int =
  let m = g.len()

  if m == 0:
    return 0

  var dp: ref seq[int]
  dp.new()
  dp[] = newSeq[int](m)
  dp[].fill(1)

  var dpNext: ref seq[int]
  dpNext.new()
  dpNext[] = newSeq[int](m)

  for i in k..<n:
    dpNext[].fill(0)
    for v in 0..<m:
      for u in g[v]:
        dpNext[u] = dpNext[u].addM(dp[v])
    swap(dpNext, dp)

  var res = 0
  for i in 0..<m:
    res = res.addM(dp[i])
  return res

proc main() =
  let (n, k, ll) = readInt3()

  var pat = newSeq[int](0)
  enumerate(k, k, 0, ll, pat)

  let m = pat.len()
  var patId = initTable[int, int]()
  block:
    var id = 0
    for i in 0..<m:
      patId[pat[i]] = id
      id += 1

  var g = newSeq2[int](m, 0)
  let mask = (1 shl (3 * k)) - 1
  for p in pat:
    for i in 1..7:
      let nextP = ((p shl 3) or i) and mask
      let cnt = eval(k, nextP)
      if cnt[0] <= ll and cnt[1] <= ll and cnt[2] <= ll:
        g[patId[p]].add(patId[nextP])

  echo solve(n, k, g)

main()


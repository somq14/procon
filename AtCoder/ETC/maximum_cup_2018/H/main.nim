import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import future

const INF* = int(1e18 + 373)

proc readLine*(): string =
  stdin.readLine()
proc readSeq*(): seq[string] =
  readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = readLine().strip()
proc readInt1*(): int =
  readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])
proc newSeqWith*[T](n: Natural; e: T): seq[T] =
  result = newSeq[T](n)
  for i in 0..<n:
    result[i] = e
type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] =
  newSeqWith(n1, newSeq[T](n2))
type seq3*[T] = seq[seq[seq[T]]]
proc newSeq3*[T](n1, n2, n3: Natural): seq3[T] =
  newSeqWith(n1, newSeqWith(n2, newSeq[T](n3)))

#------------------------------------------------------------------------------#
type Pred1[T] = T -> bool

# [lb, ub)
proc nibutanLb(lb, ub: int; f: Pred1[int]): int =
  var lb = lb
  var ub = ub
  while ub - lb > 1:
    let mid = (ub + lb) div 2
    if f(mid):
      lb = mid
    else:
      ub = mid
  return lb

# (lb, ub]
proc nibutanUb(lb, ub: int; f: Pred1[int]): int =
  var lb = lb
  var ub = ub
  while ub - lb > 1:
    let mid = (ub + lb) div 2
    if f(mid):
      ub = mid
    else:
      lb = mid
  return ub
#------------------------------------------------------------------------------#

type Edge3 =
  tuple [ frm, to, w: int ]

proc warshallFloyd(g: seq2[Edge3]): seq2[int] =
  let n = g.len()

  result = newSeq2[int](n, n)
  for v in 0..<n:
    result[v].fill(INF)
    result[v][v] = 0

  for v in 0..<n:
    for e in g[v]:
      result[e.frm][e.to] = e.w

  for k in 0..<n:
    for i in 0..<n:
      for j in 0..<n:
        if result[i][k] == INF or result[k][j] == INF:
          continue
        let kpath = result[i][k] + result[k][j]
        result[i][j] = min(result[i][j], kpath)

#------------------------------------------------------------------------------#

type Edge4 = tuple [ u, v, w, r: int ]

proc addEdge(g: var seq2[Edge4]; u, v, uvw, vuw: int) =
  let uvEdgeId = g[u].len()
  let vuEdgeId = g[v].len()
  g[u].add((u, v, uvw, vuEdgeId))
  g[v].add((v, u, vuw, uvEdgeId))

proc dinic(g: seq2[Edge4]; s, t: int): int =

  proc bfs(g: var seq2[Edge4]; s: int; dep: var seq[int]) =
    dep.fill(-1)

    var q = initQueue[int]()
    q.enqueue(s)
    dep[s] = 0

    while q.len() > 0:
      let u = q.dequeue()
      for e in g[u]:
        let v = e.v
        if dep[v] >= 0:
          continue
        if e.w > 0:
          dep[v] = dep[u] + 1
          q.enqueue(v)

  proc dfs(g: var seq2[Edge4]; u, t, f: int; itr, dep: var seq[int]): int =
    if u == t:
      return f

    while itr[u] < g[u].len():
      let e = g[u][itr[u]]
      let v = e.v
      if e.w > 0 and dep[v] > dep[u]:
        let res = dfs(g, v, t, min(f, e.w), itr, dep)
        if res > 0:
          g[u][itr[u]].w -= res
          g[v][e.r].w += res
          return res
      itr[u] += 1

    return 0

  var g = g
  let n = g.len()

  var dep = newSeq[int](n)
  var itr = newSeq[int](n)

  var flow = 0
  while true:
    bfs(g, s, dep)

    if dep[t] == -1:
      break

    itr.fill(0)
    while true:
      let res = dfs(g, s, t, INF, itr, dep)
      if res <= 0:
        break
      flow += res

  return flow

#------------------------------------------------------------------------------#

proc eval(d: seq2[int]; a, b: seq[int]; x: int): bool =
  let n = d.len()
  let k = a.len()
  let q = b.len()

  var bad = 0
  for i in 1..<k:
    if d[a[i - 1]][a[i]] > x:
      bad += 1

  if bad > q:
    return false

  var g = newSeq2[Edge4](q + bad + 2, 0)
  let s = g.len() - 2
  let t = g.len() - 1

  var v = 0
  for i in 1..<k:
    if d[a[i - 1]][a[i]] <= x:
      continue

    g.addEdge(s, q + v, 1, 0)
    for j in 0..<q:
      if d[b[j]][a[i]] <= x:
        g.addEdge(q + v, j, 1, 0)
    v += 1

  for i in 0..<q:
    g.addEdge(i, t, 1, 0)

  return dinic(g, s, t) >= bad

proc main() =
  let (n, m) = readInt2()

  var g = newSeq2[Edge3](n, 0)
  for _ in 0..<m:
    var (v, u, w) = readInt3()
    v -= 1
    u -= 1
    g[v].add((v, u, w))
    g[u].add((u, v, w))

  let k = readInt1()
  let a = readSeq().map(parseInt).map(it => it - 1)
  let q = readInt1()
  let b = readSeq().map(parseInt).map(it => it - 1)

  let d = warshallFloyd(g)
  let ans = nibutanUb(-1, 10^9 * 300 + 1, it => eval(d, a, b, it))
  echo ans

main()


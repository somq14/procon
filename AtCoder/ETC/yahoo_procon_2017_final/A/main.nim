import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import logging
import future

const INF* = int(1e18 + 373)

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

proc readInt1*(): int = readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#

proc main() =
  let s = stdin.readLine()
  let n = s.len()

  let yahoo = "yahoo"

  var dp = newSeq2[int](n + 1, 5)
  for i in 0..n:
    dp[i].fill(INF)

  dp[n][0] = 0
  dp[n][1] = 1
  dp[n][2] = 2
  dp[n][3] = 2
  dp[n][4] = 1

  for i in countdown(n - 1, 0):
    for k in 0..<20:
      let j = k mod 5

      var opt = dp[i + 1][j] + 1 # delete it
      opt = min(opt, dp[i + 1][(j + 1) mod 5] + 1) # change it
      opt = min(opt, dp[i][(j + 1) mod 5] + 1) # insert it

      if s[i] == yahoo[j]:
        opt = min(opt, dp[i + 1][(j + 1) mod 5])

      dp[i][j] = opt

  echo dp[0][0]

main()


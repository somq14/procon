import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import logging
import future

const INF* = int(1e18 + 373)

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

proc readInt1*(): int = readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#
type Pred1[T] = T -> bool

# [lb, ub)
proc nibutanLb(lb, ub: int; f: Pred1[int]): int =
  var lb = lb
  var ub = ub
  while ub - lb > 1:
    let mid = (ub + lb) div 2
    if f(mid):
      lb = mid
    else:
      ub = mid
  return lb

# (lb, ub]
proc nibutanUb(lb, ub: int; f: Pred1[int]): int =
  var lb = lb
  var ub = ub
  while ub - lb > 1:
    let mid = (ub + lb) div 2
    if f(mid):
      ub = mid
    else:
      lb = mid
  return ub
#------------------------------------------------------------------------------#
proc eval(a, b: seq[int]; k, t: int): bool =
  var i = a.len()
  var j = b.len()

  var cnt = 0
  while cnt < k:
    if i == 0 or j == 0:
      return false

    let diff = abs(a[i - 1] - b[j - 1])
    if diff > t:
      if a[i - 1] >= b[j - 1]:
        i -= 1
      else:
        j -= 1
      continue

    cnt += 1
    i -= 1
    j -= 1

  return true

proc main() =
  let (n, m, k) = readInt3()
  let a = readSeq().map(parseInt).sorted(cmp[int])
  let b = readSeq().map(parseInt).sorted(cmp[int])
  echo nibutanUb(-1, 10^10, it => eval(a, b, k, it))

main()


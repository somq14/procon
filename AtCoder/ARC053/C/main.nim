import strutils
import sequtils
import algorithm
import math
import queues
import tables
import logging
import future

const INF* = int(1e18 + 373)

when not defined(release):
  addHandler(newFileLogger(stderr, lvlAll, "[ $levelname ] "))

proc readInt1*(): int = stdin.readLine().strip().parseInt()
proc readInt2*(): (int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1])
proc readInt3*(): (int, int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1], v[2])

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#
type Magic = tuple [a, b: int]

proc compareDecMagic(m1, m2: Magic): int =
  if m1.a < m2.a:
    return -1
  elif m1.a > m2.a:
    return 1
  else:
    return 0
proc compareIncMagic(m1, m2: Magic): int =
  if m1.b > m2.b:
    return -1
  elif m1.b < m2.b:
    return 1
  else:
    return 0

proc solve(n: int; ms: seq[Magic]): int =
  var mInc = newSeq[Magic](0)
  var mDec = newSeq[Magic](0)
  var mKeep = newSeq[Magic](0)

  for m in ms:
    if m.a > m.b:
      mInc.add(m)
    elif m.a < m.b:
      mDec.add(m)
    else:
      mKeep.add(m)

  mDec.sort(compareDecMagic)
  debug "mDec ", mDec
  mInc.sort(compareIncMagic)
  debug "mInc ", mInc

  var currTemp = 0
  result = 0
  for m in concat(mDec, mKeep, mInc):
    currTemp += m.a
    result = max(result, currTemp)
    currTemp -= m.b

proc main() =
  let n = readInt1()
  var ms = newSeq[Magic](n)
  for i in 0..<n:
    ms[i] = readInt2()

  echo solve(n, ms)



main()


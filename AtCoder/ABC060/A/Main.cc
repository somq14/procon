#include <algorithm>
#include <complex>
#include <iomanip>
#include <iostream>
#include <queue>
#include <set>
#include <string>
#include <vector>

using std::cin;
using std::cout;
using std::endl;

using std::string;
using std::to_string;
using std::vector;
using std::set;
using std::queue;
using std::priority_queue;

using std::min;
using std::max;
using std::sort;
using std::abs;

int main(void) {
    string a, b, c;
    cin >> a;
    cin >> b;
    cin >> c;
    bool ans = (a.back() == b.front()) && (b.back() == c.front());
    cout << (ans ? "YES" : "NO") << endl;
    return 0;
}

#include <algorithm>
#include <complex>
#include <iomanip>
#include <iostream>
#include <queue>
#include <set>
#include <string>
#include <vector>

using std::cin;
using std::cout;
using std::endl;

using std::string;
using std::to_string;
using std::vector;
using std::set;
using std::queue;
using std::priority_queue;

using std::min;
using std::max;
using std::sort;
using std::abs;

int main(void) {
    int n, w;
    cin >> n >> w;

    vector<vector<int>> item(4);

    int w0, v0;
    cin >> w0 >> v0;

    item[0].push_back(v0);
    for (int i = 1; i < n; i++) {
        int w, v;
        cin >> w >> v;
        item[w - w0].push_back(v);
    }


    for(int i = 0; i < 4; i++){
        sort(item[i].begin(), item[i].end(), std::greater<int>());
        item[i].insert(item[i].begin(), 0);
    }

    long best_v = 0;

    long sum_v0 = 0;
    for (size_t i0 = 0; i0 < item[0].size(); i0++) {
        sum_v0 += item[0][i0];

        long sum_v1 = 0;
        for (size_t i1 = 0; i1 < item[1].size(); i1++) {
            sum_v1 += item[1][i1];

            long sum_v2 = 0;
            for (size_t i2 = 0; i2 < item[2].size(); i2++) {
                sum_v2 += item[2][i2];

                long sum_v3 = 0;
                for (size_t i3 = 0; i3 < item[3].size(); i3++) {
                    sum_v3 += item[3][i3];

                    long sum_w = w0 * i0 + (w0 + 1) * i1 + (w0 + 2) * i2 + (w0 + 3) * i3;
                    long sum_v = sum_v0 + sum_v1 + sum_v2 + sum_v3;
                    if(sum_w <= w){
                        best_v = max(best_v, sum_v);
                    }
                }
            }
        }
    }

    cout << best_v << endl;

    return 0;
}

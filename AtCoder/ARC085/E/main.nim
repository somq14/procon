import strutils
import sequtils
import algorithm
import math
import queues
import tables
import logging
import future

const INF* = int(1e18 + 373)

when not defined(release):
  addHandler(newFileLogger(stderr, lvlAll, "[ $levelname ] "))

proc readInt1*(): int = stdin.readLine().strip().parseInt()
proc readInt2*(): (int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1])
proc readInt3*(): (int, int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1], v[2])

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#
proc backtrack(n: int; a: seq[int]; i: int; b: seq[bool]; score: int): int =
  if i > n div 2:
    return score

  var ans = backtrack(n, a, i + 1, b, score)

  if b[i] or a[i] >= 0:
    return ans

  if i * 2 <= n and i * 3 > n:
    if b[i * 2]:
      return max(ans, backtrack(n, a, i + 1, b, score - a[i]))
    elif a[i * 2] <= abs(a[i]):
      return max(ans, backtrack(n, a, i + 1, b, score - a[i] - a[i * 2]))
    else:
      return ans

  var bb = b
  var diff = 0

  var j = i
  while j <= n:
    if not bb[j]:
      diff -= a[j]
    bb[j] = true
    j += i

  return max(ans, backtrack(n, a, i + 1, bb, score + diff))

proc main() =
  let n = readInt1()
  var a = @[ 0 ] & readSeq().map(parseInt)

  let m = n div 2 + 1
  for i in m..n:
    if a[i] < 0:
      a[i] = 0

  if a.all(it => it >= 0):
    echo sum(a)
    return

  if a.all(it => it <= 0):
    echo 0
    return

  echo backtrack(n, a, 1, newSeq[bool](n + 1), sum(a))

main()


import strutils
import sequtils
import algorithm
import math
import queues
import tables
import logging
import future

const INF* = int(1e18 + 373)

when not defined(release):
  addHandler(newFileLogger(stderr, lvlAll, "[ $levelname ] "))

proc readInt1*(): int = stdin.readLine().strip().parseInt()
proc readInt2*(): (int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1])
proc readInt3*(): (int, int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1], v[2])
proc readInt4*(): (int, int, int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1], v[2], v[3])

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#

const MOD = int(1e9) + 7

proc is_xyz(a, x, y, z: int): bool =
  var a = a
  if (a and (1 shl (x - 1))) == 0:
    return false

  a = a shr x
  if (a and (1 shl (y - 1))) == 0:
    return false

  a = a shr y
  if (a and (1 shl (z - 1))) == 0:
    return false

  return true

proc main() =
  let (n, x, y, z) = readInt4()
  let m = (x + y + z - 1)

  var dp = newSeq[int](1 shl m)
  var dpNext = newSeq[int](1 shl m)
  dp[0] = 1

  for i in 0..<n:
    for j in 0..<(1 shl m):
      if dp[j] == 0:
        continue

      for k in 1..10:

        var nextJ = (j shl k) or (1 shl (k - 1))
        if is_xyz(nextJ, x, y, z):
          continue

        nextJ = nextJ and ((1 shl m) - 1)

        dpNext[nextJ] = (dpNext[nextJ] + dp[j]) mod MOD
    dp = dpNext
    dpNext.fill(0)

  var ans = 1
  for i in 0..<n:
    ans = (ans * 10) mod MOD

  for c in dp:
    ans = (ans - c + MOD) mod MOD

  echo ans

main()


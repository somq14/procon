import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import logging
import future

const INF* = int(1e18 + 373)

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

proc readInt1*(): int = readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#

proc main() =
  let n = readInt1()
  let s = stdin.readLine()

  var tab = initTable[(string, string), int]()

  for p in 0..<(1 shl n):
    var red = ""
    var blue = ""
    for i in 0..<n:
      if (p and (1 shl i)) != 0:
        red = red & s[i]
      else:
        blue = s[i] & blue
    if (red, blue) notin tab:
      tab[(red, blue)] = 0
    tab[(red, blue)] += 1

  var ans = 0
  for p in 0..<(1 shl n):
    var red = ""
    var blue = ""
    for i in 0..<n:
      if (p and (1 shl i)) != 0:
        red = red & s[i + n]
      else:
        blue = s[i + n] & blue

    if (blue, red) in tab:
      ans += tab[(blue, red)]

  echo ans

main()


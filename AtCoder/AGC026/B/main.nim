import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import logging
import future

const INF* = int(1e18 + 373)

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

proc readInt1*(): int = readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#
proc gcd(a, b: int): int =
  var a = a
  var b = b
  while b != 0:
    let tmp = a mod b
    a = b
    b = tmp
  return a

proc solve(a, b, c, d: int): bool =
  if b > d:
    return false

  if b == d:
    var aa = a
    if a > c:
      aa = (c + 1) + (a - (c + 1)) mod b
    return aa >= b

  assert b < d
  var aa = a
  if a <= c:
    if b > a:
      return false
    aa = (c + 1) + ((c + 1) - a + (d - b - 1)) div (d - b) * (d - b)

  aa = (c + 1) + (a - (c + 1)) mod b

  if c + 1 >= b:
    return true

  if gcd(d, b) == 1:
    return false

  let step = gcd(d, b)
  if step != 0:
    aa = (c + 1) + (aa - (c + 1)) mod step
  return aa >= b



proc main() =
  let q = readInt1()
  var qs = newSeq[(int, int, int, int)](q)
  for i in 0..<q:
    qs[i] = readInt4()

  for i in 0..<q:
    echo if solve(qs[i][0], qs[i][1], qs[i][2], qs[i][3]): "Yes" else: "No"

main()


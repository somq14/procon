import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import logging
import future

const INF* = int(1e18 + 373)

when not defined(release):
  addHandler(newFileLogger(stderr, lvlAll, "[ $levelname ] "))

proc readInt1*(): int = stdin.readLine().strip().parseInt()
proc readInt2*(): (int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1])
proc readInt3*(): (int, int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1], v[2])
proc readInt4*(): (int, int, int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1], v[2], v[3])

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#
proc solve(g: seq2[int]; a: seq[int]; v, p: int): int =
  if g[v].len() <= 1:
    return a[v]

  var s = 0
  var m = 0
  for u in g[v]:
    if u == p:
      continue
    let c = solve(g, a, u, v)
    if c < 0:
      return -1
    s += c
    m = max(m, c)

  let rest = a[v] * 2 - s
  if rest < 0:
    return -1

  m = max(m, rest)

  if m > a[v]:
    return -1

  return rest

proc main() =
  let n = readInt1()
  let a = readSeq().map(parseInt)

  var g = newSeq2[int](n, 0)
  for i in 0..<n - 1:
    let (a, b) = readInt2()
    g[a - 1].add(b - 1)
    g[b - 1].add(a - 1)

  if n <= 2:
    echo if a[0] == a[1]: "YES" else: "NO"
    return

  var root = -1
  for v in 0..<n:
    if g[v].len() >= 2:
      root = v
      break

  echo if solve(g, a, root, -1) == 0: "YES" else: "NO"

main()


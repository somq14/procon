import strutils
import sequtils
import algorithm
import math
import queues
import tables
import logging
import future

const INF* = int(1e18 + 373)

when not defined(release):
  addHandler(newFileLogger(stderr, lvlAll, "[ $levelname ] "))

proc readInt1*(): int = stdin.readLine().strip().parseInt()
proc readInt2*(): (int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1])
proc readInt3*(): (int, int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1], v[2])

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#
proc solve(n: int; a: seq[int]): bool =
  let s = sum(a)
  let nn = n * (n + 1) div 2
  if s mod nn != 0:
    return false

  var d = newSeq[int](n)
  for i in 0..<n:
    d[i] = a[(i + 1) mod n] - a[i] - (s div nn)

  debug d
  var cnt = 0
  for i in 0..<n:
    if d[i] > 0:
      return false
    if abs(d[i]) mod n != 0:
      return false
    cnt += abs(d[i]) div n

  return cnt == (s div nn)

proc main() =
  let n = readInt1()
  let a = readSeq().map(parseInt)
  echo if solve(n, a): "YES" else: "NO"


main()


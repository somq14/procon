#include <algorithm>
#include <complex>
#include <iostream>
#include <queue>
#include <set>
#include <string>
#include <vector>

using std::cin;
using std::cout;
using std::endl;

using std::string;
using std::to_string;
using std::vector;
using std::set;
using std::queue;
using std::priority_queue;

using std::min;
using std::max;
using std::sort;
using std::abs;

int main() {
    int h, w, n;
    cin >> h >> w >> n;

    vector<int> a;
    for (int i = 0; i < n; i++) {
        int x;
        cin >> x;
        for (int j = 0; j < x; j++) {
            a.push_back(i + 1);
        }
    }

    vector<vector<int>> b(h, vector<int>(w));

    int k = 0;
    for (int i = 0; i < h; i++) {
        for (int j = 0; j < w; j++) {
            if (i % 2 == 0) {
                b[i][j] = a[k++];
            } else {
                b[i][w - 1 - j] = a[k++];
            }
        }
    }

    for (int i = 0; i < h; i++) {
        for (int j = 0; j < w - 1; j++) {
            cout << b[i][j] << " ";
        }
        cout << b[i][w - 1] << endl;
    }

    return 0;
}

#include <algorithm>
#include <complex>
#include <iostream>
#include <queue>
#include <set>
#include <string>
#include <vector>

using std::cin;
using std::cout;
using std::endl;

using std::string;
using std::to_string;
using std::vector;
using std::set;
using std::queue;
using std::priority_queue;

using std::min;
using std::max;
using std::sort;
using std::abs;

int factorize2(int x){
    int cnt = 0;
    while(x % 2 == 0){
        cnt++;
        x /= 2;
    }
    return cnt;
}

int main(void) {
    int n;
    cin >> n;

    vector<int> a(3);
    for(int i = 0; i < n; i++){
        int x;
        cin >> x;
        int f = factorize2(x);
        if(f == 0){
            a[0]++;
        }else if(f == 1){
            a[1]++;
        }else{
            a[2]++;
        }
    }

    bool ans = a[1] == 0 ? a[0] <= a[2] + 1 : a[0] <= a[2];
    cout << (ans ? "Yes" : "No") << endl;

    return 0;
}

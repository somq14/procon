import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import logging
import future

const INF* = int(1e18 + 373)

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

proc readInt1*(): int = readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#

proc main() =
  let (h, w) = readInt2()
  var a = newSeq2[int](h, 0)
  for i in 0..<h:
    a[i] = readSeq().map(parseInt)

  var ans = newSeq[(int, int, int, int)]()
  for y in 0..<h:
    if y mod 2 == 0:
      for x in 0..<w - 1:
        if a[y][x] mod 2 == 1:
          a[y][x] -= 1
          a[y][x + 1] += 1
          ans.add((y, x, y, x + 1))
      if a[y][w - 1] mod 2 == 1 and y != h - 1:
        a[y][w - 1] -= 1
        a[y + 1][w - 1] += 1
        ans.add((y, w - 1, y + 1, w - 1))
    else:
      for x in countdown(w - 1, 1):
        if a[y][x] mod 2 == 1:
          a[y][x] -= 1
          a[y][x - 1] += 1
          ans.add((y, x, y, x - 1))
      if a[y][0] mod 2 == 1 and y != h - 1:
        a[y][0] -= 1
        a[y + 1][0] += 1
        ans.add((y, 0, y + 1, 0))

  echo ans.len()
  for i in 0..<ans.len():
    let (py, px, qy, qx) = ans[i]
    echo py + 1, " ", px + 1, " ", qy + 1, " ", qx + 1

main()


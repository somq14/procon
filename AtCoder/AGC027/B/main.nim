import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import logging
import future

const INF* = int(1e18 + 373)

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

proc readInt1*(): int = readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#
proc buildCumTab(a: seq[int]): seq[int] =
  let n = a.len()
  var c = newSeq[int](n + 1)
  for i in 1..n:
    c[i] = c[i - 1] + a[i - 1]
  return c

# [s, t)
proc lookupCumTab(c: seq[int]; s, t: int): int = c[t] - c[s]

#------------------------------------------------------------------------------#
proc solve(n, c, k: int; xcum: seq[int]): int =
  var ans = n * c + k * c
  if ans < 0:
    return INF

  ans += 2 * xcum.lookupCumTab(n - k, n)
  if ans < 0:
    return INF

  var i = n
  var j = 1
  while i > 0:
    ans += (2 * j + 1) * xcum.lookupCumTab(max(i - k, 0), i)
    if ans < 0:
      return INF
    i -= k
    j += 1

  return ans

proc main() =
  let (n, c) = readInt2()
  let x = readSeq().map(parseInt)
  let xcum = x.buildCumTab()

  var ans = INF
  for k in 1..n:
    ans = min(ans, solve(n, c, k, xcum))
  echo ans

main()


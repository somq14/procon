import strutils
import sequtils
import algorithm
import math
import queues
import tables
import logging
import future

const INF* = int(1e18 + 373)

when not defined(release):
  addHandler(newFileLogger(stderr, lvlAll, "[ $levelname ] "))

proc readInt1*(): int = stdin.readLine().strip().parseInt()
proc readInt2*(): (int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1])
proc readInt3*(): (int, int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1], v[2])
proc readInt4*(): (int, int, int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1], v[2], v[3])

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#

proc main() =
  let s = stdin.readLine()
  let t = stdin.readLine()
  let q = readInt1()
  var a = newSeq[int](q)
  var b = newSeq[int](q)
  var c = newSeq[int](q)
  var d = newSeq[int](q)
  for i in 0..<q:
    (a[i], b[i], c[i], d[i]) = readInt4()
    a[i] -= 1
    c[i] -= 1
    # [a[i], b[i])
    # [c[i], d[i])

  var sumS = newSeq[int](s.len() + 1)
  sumS[0] = 0
  for i in 1..s.len():
    sumS[i] = sumS[i - 1]
    if s[i - 1] == 'A':
      sumS[i] += 1

  var sumT = newSeq[int](t.len() + 1)
  sumT[0] = 0
  for i in 1..t.len():
    sumT[i] = sumT[i - 1]
    if t[i - 1] == 'A':
      sumT[i] += 1

  for i in 0..<q:
    let cntAS = sumS[b[i]] - sumS[a[i]]
    let cntBS = (b[i] - a[i]) - cntAS
    let cntAT = sumT[d[i]] - sumT[c[i]]
    let cntBT = (d[i] - c[i]) - cntAT

    if ((cntAS + 0) mod 3 == cntAT mod 3) and ((cntBS + 0) mod 3 == cntBT mod 3) or
      ((cntAS + 2) mod 3 == cntAT mod 3) and ((cntBS + 2) mod 3 == cntBT mod 3) or
      ((cntAS + 4) mod 3 == cntAT mod 3) and ((cntBS + 4) mod 3 == cntBT mod 3):
      echo "YES"
    else:
      echo "NO"
main()


#include <algorithm>
#include <complex>
#include <iomanip>
#include <iostream>
#include <queue>
#include <set>
#include <string>
#include <vector>
#include <cctype>

using std::cin;
using std::cout;
using std::endl;

using std::string;
using std::to_string;
using std::vector;
using std::set;
using std::queue;
using std::priority_queue;

using std::min;
using std::max;
using std::sort;
using std::abs;


int main(void) {
    string s;
    cin >> s;

    int len = s.length();
    s[0] = toupper(s[0]);
    for(int i = 1; i < len; i++){
        s[i] = tolower(s[i]);
    }

    cout << s << endl;

    return 0;
}

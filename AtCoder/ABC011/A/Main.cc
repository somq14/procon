#include <algorithm>
#include <complex>
#include <iomanip>
#include <iostream>
#include <queue>
#include <set>
#include <string>
#include <vector>

using std::cin;
using std::cout;
using std::endl;

using std::string;
using std::to_string;
using std::vector;
using std::set;
using std::queue;
using std::priority_queue;

using std::min;
using std::max;
using std::sort;
using std::abs;


int main(void) {
    int n;
    cin >> n;
    if(n == 12){
        cout << 1 << endl;
    }else{
        cout << n + 1 << endl;
    }
    return 0;
}

#include <algorithm>
#include <cctype>
#include <complex>
#include <iomanip>
#include <iostream>
#include <queue>
#include <set>
#include <string>
#include <vector>

using std::cin;
using std::cout;
using std::endl;

using std::string;
using std::to_string;
using std::vector;
using std::set;
using std::queue;
using std::priority_queue;

using std::min;
using std::max;
using std::sort;
using std::abs;

bool ok(int ng1, int ng2, int ng3, int n) {
    return n != ng1 && n != ng2 && n != ng3;
}

int main(void) {
    int n;
    int ng1, ng2, ng3;
    cin >> n >> ng1 >> ng2 >> ng3;

    if (!ok(ng1, ng2, ng3, n)) {
        cout << "NO" << endl;
        return 0;
    }

    for (int i = 0; i < 100; i++) {
        if(n == 0){
            break;
        }
        if(n - 3 >= 0 && ok(ng1, ng2, ng3, n - 3)){
            n -= 3;
            continue;
        }
        if(n - 2 >= 0 && ok(ng1, ng2, ng3, n - 2)){
            n -= 2;
            continue;
        }
        if(n - 1 >= 0 && ok(ng1, ng2, ng3, n - 1)){
            n -= 1;
            continue;
        }
    }

    cout << (n == 0 ? "YES" : "NO") << endl;

    return 0;
}

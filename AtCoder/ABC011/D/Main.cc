#include <algorithm>
#include <cctype>
#include <cmath>
#include <complex>
#include <iomanip>
#include <iostream>
#include <queue>
#include <set>
#include <string>
#include <vector>
#include <iomanip>

using std::cin;
using std::cout;
using std::endl;

using std::string;
using std::to_string;
using std::vector;
using std::set;
using std::queue;
using std::priority_queue;

using std::min;
using std::max;
using std::sort;
using std::abs;

long double fact(int n) {
    long double ans = 1.0;
    for (int i = 1; i <= n; i++) {
        ans *= i;
    }
    return ans;
}

long double comb(int n, int r) { return fact(n) / fact(r) / fact(n - r); }

long double solve(int n, int d, int xx, int yy) {
    if (xx % d != 0 || yy % d != 0) {
        return 0;
    }

    xx /= d;
    yy /= d;

    vector< vector<long double> > p(n + 1, vector<long double>(2001, 0));
    p[0][0 + 1000] = 1;
    for (int i = 1; i <= n; i++) {
        for (int x = -1000; x <= 1000; x++) {
            p[i][x + 1000] += x > -1000 ? 0.5 * p[i - 1][x - 1 + 1000] : 0;
            p[i][x + 1000] += x <  1000 ? 0.5 * p[i - 1][x + 1 + 1000] : 0;
        }
    }

    long double prob = 0;
    for (int i = 0; i <= n; i++) {
        prob += comb(n, i) * p[i][xx + 1000] * p[n - i][yy + 1000] / pow(2, n);
    }

    return prob;
}

int main(void) {
    int n, d;
    int x, y;
    cin >> n >> d >> x >> y;

    cout << std::fixed << std::setprecision(30) << solve(n, d, x, y) << endl;

    return 0;
}

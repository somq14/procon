import strutils
import sequtils
import algorithm
import math
import queues

const INF = int(1e18 + 373)
const MOD = int(1e9 + 7)

proc readInt1() : int = stdin.readLine().strip().parseInt()
proc readInt2() : (int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1])
proc readInt3() : (int, int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1], v[2])

#------------------------------------------------------------------------------#

proc main() =
    let n = readInt1()
    let a = stdin.readLine().strip().split().map(parseInt)
    let s = a.sorted(system.cmp[int]).reversed()

    var asum = 0
    var bsum = 0
    for i in 0..<n:
        if i mod 2 == 0:
            asum += s[i]
        else:
            bsum += s[i]
    echo asum - bsum

main()

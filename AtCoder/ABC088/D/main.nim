import strutils
import sequtils
import algorithm
import math
import queues

const INF = int(1e18 + 373)
const MOD = int(1e9 + 7)

proc readInt1() : int = stdin.readLine().strip().parseInt()
proc readInt2() : (int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1])
proc readInt3() : (int, int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1], v[2])

#------------------------------------------------------------------------------#
type Pos = tuple[y: int, x: int]

const dy4 = @[0, 0, 1, -1]
const dx4 = @[1, -1, 0, 0]

proc solve(h, w: int, s: seq[string]): int =
    var q = initQueue[Pos]()
    q.enqueue((0, 0))

    var dep = newSeq[int](h * w)
    dep.fill(INF)
    dep[0] = 0

    while q.len() > 0:
        let p = q.dequeue()
        let d = dep[p.y * w + p.x]

        for k in 0..<4:
            let pNext : Pos = (p.y + dy4[k], p.x + dx4[k])
            if pNext.y notin 0..<h or pNext.x notin 0..<w:
                continue
            if s[pNext.y][pNext.x] == '.' and dep[pNext.y * w + pNext.x] == INF:
                dep[pNext.y * w + pNext.x] = d + 1
                q.enqueue(pNext)
                continue

    return dep[h * w - 1]

proc main() =
    let (h, w) = readInt2()
    var s = newSeq[string](h)
    for i in 0..<h:
        s[i] = stdin.readLine().strip()

    var cnt = 0
    for i in 0..<h:
        for j in 0..<w:
            if s[i][j] == '#':
                cnt += 1

    let d = solve(h, w, s)
    if d == INF:
        echo -1
    else:
        let ans = (h * w - cnt - 2) - (d - 1)
        echo ans

main()

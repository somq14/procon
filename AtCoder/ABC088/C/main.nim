import strutils
import sequtils
import algorithm
import math
import queues

const INF = int(1e18 + 373)
const MOD = int(1e9 + 7)

proc readInt1() : int = stdin.readLine().strip().parseInt()
proc readInt2() : (int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1])
proc readInt3() : (int, int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1], v[2])

#------------------------------------------------------------------------------#

const N = 101

proc valid(c: seq[seq[int]], a: seq[int], b: seq[int]): bool =
    for i in 0..<3:
        for j in 0..<3:
            if a[i] + b[j] != c[i][j]:
                return false
    return true

proc main() =
    var c = newSeqWith(3, newSeq[int](3))
    for i in 0..<3:
        (c[i][0], c[i][1], c[i][2]) = readInt3()

    var ans = false
    block outer:
        for a0 in -N..N:
            let b0 = c[0][0] - a0
            for a1 in -N..N:
                let b1 = c[1][1] - a1
                if a0 + b1 != c[0][1]: continue
                if a1 + b0 != c[1][0]: continue
                for a2 in -N..N:
                    let b2 = c[2][2] - a2
                    if a2 + b0 == c[2][0] and a0 + b2 == c[0][2] and
                       a1 + b2 == c[1][2] and a2 + b1 == c[2][1]:
                        ans = true
                        break outer


    echo if ans: "Yes" else: "No"

main()

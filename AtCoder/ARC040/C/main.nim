import strutils
import sequtils
import algorithm
import math
import queues
import tables

const INF* = int(1e18 + 373)

proc readInt1*(): int = stdin.readLine().strip().parseInt()
proc readInt2*(): (int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1])
proc readInt3*(): (int, int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1], v[2])

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#
proc findLast[T, S](a: T; item: S): int =
  for i in countdown(a.len() - 1, 0):
    if a[i] == item:
      return i
  return -1

proc main() =
  let n = readInt1()

  var s = newSeq[string](n)
  for i in 0..<n:
    s[i] = stdin.readLine()

  var ans = 0

  for i in 0..<n:
    var ind = s[i].findLast('.')

    if ind == -1:
      continue

    ans += 1
    s[i].fill(0, ind, 'o')
    if i + 1 < n:
      s[i + 1].fill(ind, n - 1, 'o')

  echo ans

main()


import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import logging
import future

const INF* = int(1e18 + 373)

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

proc readInt1*(): int = readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#
proc s(n: int): int =
  var ans = 0
  var m = n
  while m > 0:
    ans += m mod 10
    m = m div 10
  return ans

proc keta(n: int): int =
  var ans = 0
  var m = n
  while m > 0:
    ans += 1
    m = m div 10
  return ans

proc isSnuke(n: int): bool =
  var k = keta(n)
  for i in 0..<k:
    let m = n + 10 ^ i
    if n * s(m) > m * s(n):
      return false
  return true

proc main() =
  let k = readInt1()

  var ans = @[ 1 ]
  while ans.len() <= k:
    let n = ans[ans.len() - 1]
    let k = keta(n)
    for i in 0..k:
      let m = n + 10 ^ i
      if isSnuke(m):
        ans.add(m)
        break

  for i in 0..<k:
    echo ans[i]

main()


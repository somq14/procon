import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import logging
import future

const INF* = int(1e18 + 373)

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

proc readInt1*(): int = readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#
#------------------------------------------------------------------------------#
type UnionFindTree = tuple[ p, h, w: seq[int] ]

proc initUnionFindTree(n: int): UnionFindTree =
    var p = newSeq[int](n)
    p.fill(-1)
    var h = newSeq[int](n)
    h.fill(0)
    var w = newSeq[int](n)
    w.fill(1)
    return (p, h, w)

proc find(this: var UnionFindTree, v: int): int =
    if this.p[v] == -1:
        return v
    this.p[v] = find(this, this.p[v])
    return this.p[v]

proc same(this: var UnionFindTree, u, v: int): bool =
    this.find(u) == this.find(v)

proc union(this: var UnionFindTree, u, v: int) =
    let uRoot = this.find(u)
    let vRoot = this.find(v)
    if uRoot == vRoot:
        return

    if this.h[uRoot] > this.h[vRoot]:
        this.p[vRoot] = uRoot
        this.w[uRoot] += this.w[vRoot]
    else:
        this.p[uRoot] = vRoot
        this.w[vRoot] += this.w[uRoot]
        if this.h[uRoot] == this.h[vRoot]:
            this.h[vRoot] += 1

proc count[T](a: seq[T]; x: T): int =
  result = 0
  for e in a:
    if e == x:
      result += 1

#------------------------------------------------------------------------------#
proc dfs(n, m: int; g: seq2[bool]; vs: int; mark: var seq[int]): (int, int) =
  var stack = @[ vs ]
  var visited = @[ vs ]
  mark[vs] = 1
  while stack.len() > 0:
    let v = stack.pop()
    for u in 0..<n:
      if not g[v][u]:
        continue

      if mark[u] == 0:
        mark[u] = -mark[v]
        visited.add(u)
        stack.add(u)
      elif mark[v] == mark[u]:
        return (-1, -1)

  var s = 0
  var t = 0
  for v in visited:
    if mark[v] == 1:
      s += 1
    if mark[v] == -1:
      t += 1
  return (min(s, t), max(s, t))

proc solve(n, m: int; g: seq2[bool]): int =
  var uft = initUnionFindTree(n)
  for v in 0..<n:
    for u in 0..<n:
      if g[v][u]:
        uft.union(v, u)

  var comp = newSeq[(int, int)](0)
  var mark = newSeq[int](n)
  for v in 0..<n:
    if uft.find(v) == v:
      let (s, t) = dfs(n, m, g, v, mark)
      if (s, t) == (-1, -1):
        return -1
      comp.add((s, t))

  let k = comp.len()
  var dp = newSeq2[bool](k + 1, n + 1)
  dp[0][0] = true
  for i in 1..k:
    for j in 0..n:
      var opt = false
      if j >= comp[i - 1][0]:
        opt = opt or dp[i - 1][j - comp[i - 1][0]]
      if j >= comp[i - 1][1]:
        opt = opt or dp[i - 1][j - comp[i - 1][1]]
      dp[i][j] = opt

  var ans = INF
  for j in 0..n:
    if dp[k][j]:
      let c0 = j
      let c1 = n - j
      ans = min(ans, c0 * (c0 - 1) div 2 + c1 * (c1 - 1) div 2)
  return ans

proc main() =
  let (n, m) = readInt2()

  var g = newSeq2[bool](n, n)
  for _ in 0..<m:
    let (a, b) = readInt2()
    g[a - 1][b - 1] = true
    g[b - 1][a - 1] = true

  for v in 0..<n:
    for u in 0..<n:
      g[v][u] = not g[v][u]

  for v in 0..<n:
    g[v][v] = false

  echo solve(n, m, g)

main()


import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import logging
import future

const INF* = int(1e18 + 373)

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

proc readInt1*(): int = readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#
proc s(n: int): int =
  var m = n
  var ans = 0
  while m > 0:
    ans += m mod 10
    m = m div 10
  return ans

proc main() =
  let k = readInt1()
  for i in 1..<k:
    var ok = true
    for j in (i + 1)..<(i + 10 * k):
      if i * s(j) > j * s(i):
        if i + 1 != j:
          echo "out ", i, " ", j
        ok = false
        break
    if ok:
      echo i, " = ", s(i)



main()


import strutils
import sequtils
import algorithm
import math
import queues
import tables
import logging
import future

const INF* = int(1e18 + 373)

when not defined(release):
  addHandler(newFileLogger(stderr, lvlAll, "[ $levelname ] "))

proc readInt1*(): int = stdin.readLine().strip().parseInt()
proc readInt2*(): (int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1])
proc readInt3*(): (int, int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1], v[2])

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#

type Op2[T] = proc(a, b: T): T
type SegmentTree[T] = tuple[ n: int, a: seq[T], e: T, f: Op2[T] ]

proc initSegmentTree[T](n: int, e: T, f: Op2[T]): SegmentTree[T] =
    var m = 1
    while m < n:
        m *= 2

    var a = newSeq[T](2 * m)
    a.fill(e)
    return (m, a, e, f)

proc update[T](this: var SegmentTree[T], i: int, x: T) =
    this.a[this.n + i] = x
    var j = (this.n + i) div 2
    while j > 0:
        this.a[j] = this.f(this.a[j * 2], this.a[j * 2 + 1])
        j = j div 2

proc query[T](this: SegmentTree[T], s, t, i, ll, hh: int): T =
    if t <= ll or hh <= s:
        return this.e
    if s <= ll and hh <= t:
        return this.a[i]

    let m = (ll + hh) div 2
    let vl = this.query(s, t, i * 2, ll, m)
    let vh = this.query(s, t, i * 2 + 1, m, hh)
    return this.f(vl, vh)

proc query[T](this: SegmentTree[T], s, t: int): T = query(this, s, t, 1, 0, this.n)

#------------------------------------------------------------------------------#

proc tentou(n: int; a: seq[int]): seq[int] =
  var segtree = initSegmentTree[int](n, 0, (x: int, y: int) => x + y)

  result = newSeq[int](n)
  for i in 0..<n:
    result[i] = segtree.query(a[i], n)
    segtree.update(a[i], 1)

proc solve(n: int; a: seq[int]): seq[int] =
  debug sum(tentou(n, a))
  let ts = tentou(n, a)

  let tentou = sum(ts)
  if tentou mod 2 != 0:
    return nil

  let targetTentou = tentou div 2

  var sum = 0
  var p = 0
  while p < n:
    if sum + ts[p] > targetTentou:
      break
    sum += ts[p]
    p += 1

  result = a[0..<p]
  result.sort(cmp[int])
  for i in p..<n:
    result.add(a[i])

  for i in p..<n:
    for j in countdown(i - 1, 0):
      if sum >= targetTentou:
        return
      if result[j] < result[j + 1]:
        break
      swap(result[j], result[j + 1])
      sum += 1

proc main() =
  let n = readInt1()
  let a = readSeq().mapIt(it.parseInt() - 1)
  let b = readSeq().mapIt(it.parseInt() - 1)

  var conv = newSeq[int](n)
  for i in 0..<n:
    conv[a[i]] = i

  let convA = a.mapIt(conv[it])
  let convB = b.mapIt(conv[it])

  let ans = solve(n, convB)
  if ans == nil:
    echo -1
  else:
    debug sum(tentou(n, ans))
    echo ans.mapIt($(a[it] + 1)).join(" ")

main()


import strutils
import sequtils
import algorithm
import math
import queues

const INF = int(1e18 + 373)
const MOD = int(1e9 + 7)

proc readInt1(): int = stdin.readLine().strip().parseInt()
proc readInt2(): (int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1])
proc readInt3(): (int, int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1], v[2])
proc readInt(): seq[int] = stdin.readLine().strip().split().map(parseInt)

#------------------------------------------------------------------------------#

proc main() =
    let (x, y) = readInt2()
    let n = readInt1()

    var ts = newSeq[int](n)
    var hs = newSeq[int](n)
    for i in 0..<n:
        (ts[i], hs[i]) = readInt2()

    var dp: ref seq[seq[int]] = new seq[seq[int]]
    dp[] = newSeqWith(x + 1, newSeq[int](y + 1))

    var nextDp: ref seq[seq[int]] = new seq[seq[int]]
    nextDp[] = newSeqWith(x + 1, newSeq[int](y + 1))

    for j in 0..x:
        for k in 0..y:
            dp[j][k] = 0

    for i in 0..<n:
        for j in 0..x:
            for k in 0..y:
                var opt = dp[j][k]
                if j > 0 and j + k >= ts[i]:
                    let normalUsage = min(k, ts[i] - 1)
                    let specialUsage = ts[i] - normalUsage
                    opt = max(opt, dp[j - specialUsage][k - normalUsage] + hs[i])
                nextDp[j][k] = opt
        swap(dp, nextDp)

    echo dp[x][y]

main()


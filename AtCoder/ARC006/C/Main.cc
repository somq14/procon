#include <algorithm>
#include <cmath>
#include <complex>
#include <iomanip>
#include <iostream>
#include <string>
#include <utility>
#include <vector>

using namespace std;

using ll = long long int;
const int MOD = 1e9 + 7;
const int INF = 1e9 + 373;

#define rep(i, n) for (int i = 0; i < (n); ++i)

template <typename T>
using vector2 = vector<vector<T>>;
template <typename T>
vector2<T> init_vector2(size_t n0, size_t n1, T e = T()) {
    return vector2<T>(n0, vector<T>(n1, e));
}

template <typename T>
using vector3 = vector<vector<vector<T>>>;
template <typename T>
vector3<T> init_vector3(size_t n0, size_t n1, size_t n2, T e = T()) {
    return vector3<T>(n0, vector2<T>(n1, vector<T>(n2, e)));
}

int main() {
    int n;
    cin >> n;

    vector<int> w(n);
    rep(i, n) { cin >> w[i]; }

    vector<int> a;
    rep(i, n) {
        int p = -1;
        for (size_t j = 0; j < a.size(); j++) {
            if (w[i] <= a[j] && (p == -1 || a[j] < a[p])) {
                p = j;
            }
        }
        if (p == -1) {
            a.push_back(w[i]);
        } else {
            a[p] = w[i];
        }
    }

    cout << a.size() << endl;

    return 0;
}

#include <algorithm>
#include <cmath>
#include <complex>
#include <iomanip>
#include <iostream>
#include <string>
#include <utility>
#include <vector>

using namespace std;

using ll = long long int;
const int MOD = 1e9 + 7;
const int INF = 1e9 + 373;

#define rep(i, n) for (int i = 0; i < (n); ++i)

template <typename T>
using vector2 = vector<vector<T>>;
template <typename T>
vector2<T> init_vector2(size_t n0, size_t n1, T e = T()) {
    return vector2<T>(n0, vector<T>(n1, e));
}

template <typename T>
using vector3 = vector<vector<vector<T>>>;
template <typename T>
vector3<T> init_vector3(size_t n0, size_t n1, size_t n2, T e = T()) {
    return vector3<T>(n0, vector2<T>(n1, vector<T>(n2, e)));
}

vector2<int> build_cumsum2(int h, int w, const vector2<int>& a) {
    vector2<int> c = init_vector2(h + 1, w + 1, 0);
    for (int y = 1; y <= h; y++) {
        for (int x = 1; x <= w; x++) {
            c[y][x] =
                c[y - 1][x] + c[y][x - 1] - c[y - 1][x - 1] + a[y - 1][x - 1];
        }
    }
    return c;
}

int cumsum2(const vector2<int>& c, int sy, int sx, int ty, int tx) {
    return c[ty][tx] - c[ty][sx] - c[sy][tx] + c[sy][sx];
}

vector2<int> rotate(vector2<int> pat) {
    vector2<int> ans = init_vector2(7, 7, 0);
    rep(i, 7) {
        rep(j, 7) { ans[6 - j][i] = pat[i][j]; }
    }
    return ans;
}

int count_pat(int h, int w, const vector2<int>& c, const vector2<int>& pat, int k) {
    const int pat_siz = 7 * k;
    int pat_wei = 0;
    for (int y = 0; y < 7; y++) {
        for (int x = 0; x < 7; x++) {
            pat_wei += pat[y][x];
        }
    }
    pat_wei *= k * k;

    int ans = 0;
    for (int y = 0; y + pat_siz <= h; y++) {
        for (int x = 0; x + pat_siz <= w; x++) {
            if (cumsum2(c, y, x, y + pat_siz, x + pat_siz) != pat_wei) {
                continue;
            }
            bool match = true;
            rep(i, 7) {
                rep(j, 7) {
                    int v = cumsum2(c, y + k * i, x + k * j, y + k * (i + 1), x + k * (j + 1));
                    if (pat[i][j] == 1 && v != k * k) {
                        match = false;
                        goto out;
                    }
                    if (pat[i][j] == 0 && v != 0) {
                        match = false;
                        goto out;
                    }
                }
            }
        out:
            if (match) {
                ans++;
            }
        }
    }

    return ans;
}

int main() {
    vector2<int> a_pat = {
        {0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 1, 0, 0, 0},
        {0, 0, 1, 0, 1, 0, 0},
        {0, 1, 0, 0, 0, 1, 0},
        {0, 1, 1, 1, 1, 1, 0},
        {0, 1, 0, 0, 0, 1, 0},
        {0, 0, 0, 0, 0, 0, 0},
    };
    vector2<int> b_pat = {
        {0, 0, 0, 0, 0, 0, 0},
        {0, 1, 1, 1, 1, 0, 0},
        {0, 1, 0, 0, 0, 1, 0},
        {0, 1, 1, 1, 1, 0, 0},
        {0, 1, 0, 0, 0, 1, 0},
        {0, 1, 1, 1, 1, 0, 0},
        {0, 0, 0, 0, 0, 0, 0},
    };
    vector2<int> c_pat = {
        {0, 0, 0, 0, 0, 0, 0},
        {0, 0, 1, 1, 1, 0, 0},
        {0, 1, 0, 0, 0, 1, 0},
        {0, 1, 0, 0, 0, 0, 0},
        {0, 1, 0, 0, 0, 1, 0},
        {0, 0, 1, 1, 1, 0, 0},
        {0, 0, 0, 0, 0, 0, 0},
    };

    int h, w;
    cin >> h >> w;

    vector<string> a_(h);
    rep(i, h) { cin >> a_[i]; }

    vector2<int> a = init_vector2(h, w, 0);
    rep(i, h) {
        rep(j, w) { a[i][j] = a_[i][j] == '.' ? 0 : 1; }
    }

    vector2<int> c = build_cumsum2(h, w, a);

    int a_ans = 0;
    rep(i, 4) {
        vector2<int> pat = a_pat;
        rep(j, i) { pat = rotate(pat); }
        for (int k = 1; k <= 142; k++) {
            a_ans += count_pat(h, w, c, pat, k);
        }
    }
    int b_ans = 0;
    rep(i, 4) {
        vector2<int> pat = b_pat;
        rep(j, i) { pat = rotate(pat); }
        for (int k = 1; k <= 142; k++) {
            b_ans += count_pat(h, w, c, pat, k);
        }
    }
    int c_ans = 0;
    rep(i, 4) {
        vector2<int> pat = c_pat;
        rep(j, i) { pat = rotate(pat); }
        for (int k = 1; k <= 142; k++) {
            c_ans += count_pat(h, w, c, pat, k);
        }
    }

    cout << a_ans << " " << b_ans << " " << c_ans << endl;

    return 0;
}

#include <algorithm>
#include <cmath>
#include <complex>
#include <iomanip>
#include <iostream>
#include <queue>
#include <set>
#include <stack>
#include <string>
#include <utility>
#include <vector>

using std::cin;
using std::cout;
using std::endl;
using std::cerr;

using std::string;
using std::to_string;
using std::vector;
using std::set;
using std::queue;
using std::stack;
using std::priority_queue;
using std::pair;
using std::make_pair;

using std::min;
using std::max;
using std::sort;
using std::abs;

using std::fixed;
using std::setprecision;
using std::setw;

typedef long long int ll;
const int MOD = 1e9 + 7;
const int INF = 1e9 + 314;

int main() {
    ll n, h;
    cin >> n >> h;

    vector<ll> a(n);
    vector<ll> b(n);
    for(ll i = 0; i < n; i++){
        cin >> a[i] >> b[i];
    }

    sort(a.begin(), a.end(), std::greater<ll>());
    sort(b.begin(), b.end(), std::greater<ll>());

    ll sum_b = 0;
    ll ans = (h + a[0] - 1) / a[0];
    for (ll i = 0; i < n; i++) {
        sum_b += b[i];

        ll j = h - sum_b;
        if(j <= 0){
            ans = min(ans, i + 1);
        }else{
            ans = min(ans, i + 1 + (j + a[0] - 1) / a[0]);
        }
    }

    cout << ans << endl;

    return 0;
}

#include <algorithm>
#include <cmath>
#include <complex>
#include <iomanip>
#include <iostream>
#include <queue>
#include <set>
#include <stack>
#include <string>
#include <utility>
#include <vector>

using std::cin;
using std::cout;
using std::endl;
using std::cerr;

using std::string;
using std::to_string;
using std::vector;
using std::set;
using std::queue;
using std::stack;
using std::priority_queue;
using std::pair;
using std::make_pair;

using std::min;
using std::max;
using std::sort;
using std::abs;

using std::fixed;
using std::setprecision;
using std::setw;

typedef long long int ll;
const int MOD = 1e9 + 7;
const int INF = 1e9 + 314;

int main() {
    int n, m;
    cin >> n >> m;

    if (m % 1000 != 0) {
        cout << "-1 -1 -1" << endl;
        return 0;
    }
    m /= 1000;

    for (int x = 0; x <= n; x++) {
        for (int y = 0; y <= n; y++) {
            int z = m - 10 * x - 5 * y;
            if (x >= 0 && y >= 0 && z >= 0 && x + y + z == n &&
                10 * x + 5 * y + z == m) {
                cout << x << " " << y << " " << z << endl;
                return 0;
            }
        }
    }

    cout << "-1 -1 -1" << endl;

    return 0;
}

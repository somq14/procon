import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import logging
import future

const INF* = int(1e18 + 373)

when not defined(release):
  addHandler(newFileLogger(stderr, lvlAll, "[ $levelname ] "))

proc readInt1*(): int = stdin.readLine().strip().parseInt()
proc readInt2*(): (int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1])
proc readInt3*(): (int, int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1], v[2])
proc readInt4*(): (int, int, int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1], v[2], v[3])

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#

proc main() =
  let (n, m, q) = readInt3()

  var s = newSeq[string](n)
  for i in 0..<n:
    s[i] = stdin.readLine()

  var vcum = newSeq2[int](n + 1, m + 1)
  for y in 1..n:
    for x in 1..m:
      vcum[y][x] = vcum[y - 1][x] + vcum[y][x - 1] - vcum[y - 1][x - 1]
      if s[y - 1][x - 1] == '1':
        vcum[y][x] += 1

  var ecumy = newSeq2[int](n + 1, m + 1)
  for y in 1..n:
    for x in 1..m:
      ecumy[y][x] = ecumy[y - 1][x] + ecumy[y][x - 1] - ecumy[y - 1][x - 1]
      if s[y - 1][x - 1] == '1' and y >= 2 and s[y - 2][x - 1] == '1':
        ecumy[y][x] += 1

  var ecumx = newSeq2[int](n + 1, m + 1)
  for y in 1..n:
    for x in 1..m:
      ecumx[y][x] = ecumx[y - 1][x] + ecumx[y][x - 1] - ecumx[y - 1][x - 1]
      if s[y - 1][x - 1] == '1' and x >= 2 and s[y - 1][x - 2] == '1':
        ecumx[y][x] += 1

  for i in 0..<q:
    var (y1, x1, y2, x2) = readInt4()
    y1 -= 1
    x1 -= 1
    y2 -= 1
    x2 -= 1

    let vcount = vcum[y2 + 1][x2 + 1] - vcum[y2 + 1][x1] - vcum[y1][x2 + 1] + vcum[y1][x1]
    var ecount = 0
    ecount += ecumy[y2 + 1][x2 + 1] - ecumy[y1 + 1][x2 + 1] - ecumy[y2 + 1][x1] + ecumy[y1 + 1][x1]
    ecount += ecumx[y2 + 1][x2 + 1] - ecumx[y1][x2 + 1] - ecumx[y2 + 1][x1 + 1] + ecumx[y1][x1 + 1]
    let ans = vcount - ecount
    echo ans

main()


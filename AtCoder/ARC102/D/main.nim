import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import logging
import future

const INF* = int(1e18 + 373)

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

proc readInt1*(): int = readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#
proc base3(n: int): seq[int] = 
  result = newSeq[int](0)
  var i = n
  while i > 0:
    result.add(i mod 3)
    i = i div 3

proc main() =
  let ll = readInt1()
  let a = base3(ll)

  var ans = newSeq[(int, int, int)](0)
  for i in 1..<13:
    ans.add((i, i + 1, 0 * 3 ^ (12 - i)))
    ans.add((i, i + 1, 1 * 3 ^ (12 - i)))
    ans.add((i, i + 1, 2 * 3 ^ (12 - i)))

  var sum = 0
  for i in countdown(a.len() - 1, 0):
    case a[i]:
    of 0:
      discard
    of 1:
      ans.add((0, 13 - i, sum))
    of 2:
      ans.add((0, 13 - i, sum))
      ans.add((0, 13 - i, sum + 3 ^ i))
    else:
      discard
    sum += a[i] * 3 ^ i

  echo 14, " ", ans.len()
  for i in 0..<ans.len():
    let e = ans[i]
    echo e[0] + 1, " ", e[1] + 1, " ", e[2]

main()


import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import logging
import future

const INF* = int(1e18 + 373)

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

proc readInt1*(): int = readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#
const MOD = int(998244353)

type Op1[T] = proc(a: T, b: T): T
type Op2[T] = proc(a: T, b: T): T

proc repeatedSquares[T](x: T, n: Natural, e: T, f: Op2[T]): T =
    result = e
    var xx = f(x, e)
    var i = n
    while i > 0:
        if (i and 1) != 0:
            result = f(result, xx)
        xx = f(xx, xx)
        i = i shr 1

proc addM(a, b: Natural): Natural = (a + b) mod MOD
proc subM(a, b: Natural): Natural = (a - b + MOD) mod MOD
proc mulM(a, b: Natural): Natural = a * b mod MOD
proc powM(a, b: Natural): Natural = repeatedSquares[Natural](a, b, 1, `mulM`)
proc invM(a: Natural): Natural = a.powM(MOD - 2)
proc divM(a, b: Natural): Natural = a.mulM(invM(b))

var memoFactM: seq[Natural] = nil
var memoFactInvM: seq[Natural] = nil

proc buildFactTable(n: Natural) =
  memoFactM = newSeq[Natural](n + 1)
  memoFactM[0] = 1
  for i in 1..n:
    memoFactM[i] = memoFactM[i - 1].mulM(i)

  memoFactInvM = newSeq[Natural](n + 1)
  memoFactInvM[n] = invM(memoFactM[n])
  for i in countdown(n - 1, 0):
    memoFactInvM[i] = memoFactInvM[i + 1].mulM(i + 1)

buildFactTable(10^6)

proc factM(n: Natural): Natural = memoFactM[n]

proc factInvM(n: Natural): Natural = memoFactInvM[n]

proc combM(n, r: Natural): Natural =
  if r > n:
    return 0
  if r > n div 2:
    return combM(n, n - r)

  result = factM(n).mulM(factInvM(n - r)).mulM(factInvM(r))

proc multCombM(n, r: Natural): Natural = combM(n + r - 1, r - 1)

#------------------------------------------------------------------------------#
proc solve(k, n, i: int): int =
  var m = 0
  for j in 1..k:
    if j > i - j:
      break
    if (i - j) in 1..k:
      m += 1

  var ans = 0

  for j in 0..m:
    if n - 2 * j < 0:
      continue
    let comb = m.combM(j).mulM((n - 2 * j).multCombM(k))
    if j mod 2 == 0:
      ans = ans.addM(comb)
    else:
      ans = ans.subM(comb)

  # echo "i = ", i, " m = ", m, " ans = ", ans
  return ans

proc main() =
  let (k, n) = readInt2()

  for i in 2..(2 * k):
    echo solve(k, n, i)

main()


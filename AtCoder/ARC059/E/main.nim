import strutils
import sequtils
import algorithm
import math
import queues
import tables
import logging
import future

const INF* = int(1e18 + 373)

when not defined(release):
  addHandler(newFileLogger(stderr, lvlAll, "[ $levelname ] "))

proc readInt1*(): int = stdin.readLine().strip().parseInt()
proc readInt2*(): (int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1])
proc readInt3*(): (int, int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1], v[2])

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#
#------------------------------------------------------------------------------#
const MOD = int(1e9 + 7)

type Op1[T] = proc(a: T, b: T): T
type Op2[T] = proc(a: T, b: T): T

proc repeatedSquares[T](x: T, n: Natural, e: T, f: Op2[T]): T =
    result = e
    var xx = f(x, e)
    var i = n
    while i > 0:
        if (i and 1) != 0:
            result = f(result, xx)
        xx = f(xx, xx)
        i = i shr 1


proc addM(a, b: Natural): Natural = (a + b) mod MOD
proc subM(a, b: Natural): Natural = (a - b + MOD) mod MOD
proc mulM(a, b: Natural): Natural = a * b mod MOD
proc powM(a, b: Natural): Natural = repeatedSquares[Natural](a, b, 1, `mulM`)
proc invM(a: Natural): Natural = a.powM(MOD - 2)
proc divM(a, b: Natural): Natural = a.mulM(invM(b))


var memoFactM = @[Natural(1)]
proc factM(n: Natural): Natural =
    if n < memoFactM.len():
        return memoFactM[n]
    for i in memoFactM.len()..n:
        memoFactM.add(memoFactM[i - 1].mulM(i))
    return memoFactM[n]


proc combM(n, r: Natural): Natural =
    if r > n:
        return 0
    if r > n div 2:
        return combM(n, n - r)

    result = 1
    for i in ((n - r) + 1)..n:
        result = result.mulM(i)
    result = result.divM(factM(r))


proc multCombM(n, r: Natural): Natural = combM(n + r - 1, n - 1)

#------------------------------------------------------------------------------#

proc solve(n, c: int; a, b: seq[int]): int =
  # dp[cc][x] = \sum{i = 0}^{x} i^cc
  var dpSub = newSeq2[int](400 + 1, 400 + 1)
  for cc in 0..400:
    for x in 1..400:
      dpSub[cc][x] = dpSub[cc][x - 1].addM(x.powM(cc))

  var dp = newSeq2[int](n, c + 1)
  for i in 0..c:
    dp[0][i] = dpSub[i][b[0]].subM(dpSub[i][a[0] - 1])

  for i in 1..<n:
    for j in 0..c:
      # calc dp[i][j]
      var sum = 0
      for k in 0..j:
        let f0 = dp[i - 1][j - k]
        let f1 = dpSub[k][b[i]].subM(dpSub[k][a[i] - 1])
        sum = sum.addM(f0.mulM(f1))
      dp[i][j] = sum

  return dp[n - 1][c]

proc main() =
  let (n, c) = readInt2()

  var a = readSeq().map(parseInt)
  var b = readSeq().map(parseInt)

  echo solve(n, c, a, b)

main()


#include <algorithm>
#include <complex>
#include <iostream>
#include <queue>
#include <set>
#include <stack>
#include <string>
#include <vector>

using std::cin;
using std::cout;
using std::endl;
using std::cerr;

using std::string;
using std::to_string;
using std::vector;
using std::set;
using std::queue;
using std::stack;
using std::priority_queue;

using std::min;
using std::max;
using std::sort;
using std::abs;

typedef long long int ll;
const int MOD = 1e9 + 7;

int main() {
    string s;
    string ans;
    cin >> s;
    for(char c : s){
        if(c == 'B'){
            if(ans.length() > 0){
                ans.pop_back();
            }
        }else{
            ans.push_back(c);
        }
    }
    cout << ans << endl;
    return 0;
}

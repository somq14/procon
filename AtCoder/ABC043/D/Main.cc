#include <algorithm>
#include <complex>
#include <iostream>
#include <queue>
#include <set>
#include <stack>
#include <string>
#include <vector>

using std::cin;
using std::cout;
using std::endl;
using std::cerr;

using std::string;
using std::to_string;
using std::vector;
using std::set;
using std::queue;
using std::stack;
using std::priority_queue;

using std::min;
using std::max;
using std::sort;
using std::abs;

typedef long long int ll;
const int MOD = 1e9 + 7;
const int INF = 1e9 + 7;

int main() {
    string s;
    cin >> s;

    for(size_t i = 0; i < s.length() - 1; i++){
        if(s[i] == s[i+1]){
            cout << (i + 1) << " " << (i + 2) << endl;
            return 0;
        }
    }

    for(size_t i = 0; i < s.length() - 2; i++){
        if(s[i] == s[i+1] || s[i] == s[i+2] || s[i+1] == s[i+2]){
            cout << (i + 1) << " " << (i + 3) << endl;
            return 0;
        }
    }

    cout << -1 << " " << -1 << endl;
    return 0;
}

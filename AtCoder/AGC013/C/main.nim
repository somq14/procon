import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import logging
import future

const INF* = int(1e18 + 373)

when not defined(release):
  addHandler(newFileLogger(stderr, lvlAll, "[ $levelname ] "))

proc readInt1*(): int = stdin.readLine().strip().parseInt()
proc readInt2*(): (int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1])
proc readInt3*(): (int, int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1], v[2])
proc readInt4*(): (int, int, int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1], v[2], v[3])

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#

proc main() =
  let (n, l, t) = readInt3()
  var x = newSeq[int](n)
  var w = newSeq[int](n)
  for i in 0..<n:
    (x[i], w[i]) = readInt2()

  var y = newSeq[int](n)
  for i in 0..<n:
    case w[i]:
    of 1:
      y[i] = (x[i] + t) mod l
    of 2:
      y[i] = (x[i] - (t mod l) + l) mod l
    else: discard

  let pos = y[0]
  y.sort(cmp[int])

  var collisionCount = 0
  for i in 1..<n:
    if w[0] == w[i]:
      continue
    case w[0]:
    of 1:
      let d = x[i] - x[0]
      if 2 * t >= d:
        collisionCount += 1 + (2 * t - d) div l
    of 2:
      let d = l - (x[i] - x[0])
      if 2 * t >= d:
        collisionCount += 1 + (2 * t - d) div l
    else: discard

  collisionCount = collisionCount mod n
  let num = if w[0] == 1: collisionCount else: (n - collisionCount) mod n
  var ind = 0
  if w[0] == 2:
    for i in 0..<n:
      if y[i] == pos:
        ind = i
        break
  else:
    for i in countdown(n - 1, 0):
      if y[i] == pos:
        ind = i
        break

  var ans = newSeq[int](n)
  for i in 0..<n:
    ans[(num + i) mod n] = y[(ind + i) mod n]

  for i in 0..<n:
    echo ans[i]

main()


#include <algorithm>
#include <cctype>
#include <complex>
#include <iomanip>
#include <iostream>
#include <queue>
#include <set>
#include <string>
#include <vector>

using std::cin;
using std::cout;
using std::endl;

using std::string;
using std::to_string;
using std::vector;
using std::set;
using std::queue;
using std::priority_queue;

using std::min;
using std::max;
using std::sort;
using std::abs;

int main() {
    int sx, sy;
    int tx, ty;

    cin >> sx >> sy >> tx >> ty;

    const int dx = tx - sx;
    const int dy = ty - sy;

    string ans;

    for(int i = 0; i < dx; i++){
        ans.push_back('R');
    }
    for(int i = 0; i < dy; i++){
        ans.push_back('U');
    }

    for(int i = 0; i < dx; i++){
        ans.push_back('L');
    }
    for(int i = 0; i < dy; i++){
        ans.push_back('D');
    }

    ans.push_back('D');
    for(int i = 0; i < dx; i++){
        ans.push_back('R');
    }
    for(int i = 0; i < dy; i++){
        ans.push_back('U');
    }
    ans.push_back('L');

    ans.push_back('U');
    for(int i = 0; i < dx; i++){
        ans.push_back('L');
    }
    for(int i = 0; i < dy; i++){
        ans.push_back('D');
    }
    ans.push_back('R');

    cout << ans << endl;

    return 0;
}

#include <algorithm>
#include <cctype>
#include <complex>
#include <iomanip>
#include <iostream>
#include <queue>
#include <set>
#include <string>
#include <vector>

using std::cin;
using std::cout;
using std::endl;

using std::string;
using std::to_string;
using std::vector;
using std::set;
using std::queue;
using std::priority_queue;

using std::min;
using std::max;
using std::sort;
using std::abs;

int main() {
    string s;
    cin >> s;

    int len = s.length();
    for(int i = 0; i < len; i++){
        s[i] = s[i] == ',' ? ' ' : s[i];
    }
    cout << s << endl;
    return 0;
}

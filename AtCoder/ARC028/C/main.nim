import strutils
import sequtils
import algorithm
import math
import queues
import tables

const INF = int(1e18 + 373)
const MOD = int(1e9 + 7)

proc readInt1(): int = stdin.readLine().strip().parseInt()
proc readInt2(): (int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1])
proc readInt3(): (int, int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1], v[2])
proc readInt(): seq[int] = stdin.readLine().strip().split().map(parseInt)

#------------------------------------------------------------------------------#
type Edge = tuple[ a, b: int ]

proc dfs(n: int, g: seq[seq[int]], p, v: int, memo: var TableRef[Edge, int]) : int =
    var childSum = 0
    for u in g[v]:
        if u == p:
            continue
        let c = dfs(n, g, v, u, memo)
        memo[(v, u)] =  c
        childSum += c
    memo[(v, p)] = n - (childSum + 1)
    return childSum + 1

proc solve(n: int, g: seq[seq[int]]): seq[int] =
    var memo = new Table[Edge, int]
    memo[] = initTable[Edge, int]()
    discard dfs(n, g, -1, 0, memo)

    var balance = newSeq[int](n)
    balance.fill(0)
    for v in 0..<n:
        for u in g[v]:
            balance[v] = max(balance[v], memo[(v, u)])
    return balance

proc main() =
    let n = readInt1()

    var g = newSeqWith(n, newSeq[int](0))
    for i in 1..<n:
        let p = readInt1()
        g[i].add(p)
        g[p].add(i)

    let ans = solve(n, g)
    for i in 0..<n:
        echo ans[i]

main()


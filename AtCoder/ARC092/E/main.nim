import strutils
import sequtils
import algorithm
import math
import queues
import tables
import logging
import future

const INF* = int(1e18 + 373)

when not defined(release):
  addHandler(newFileLogger(stderr, lvlAll, "[ $levelname ] "))

proc readInt1*(): int = stdin.readLine().strip().parseInt()
proc readInt2*(): (int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1])
proc readInt3*(): (int, int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1], v[2])

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#

proc main() =
  let n = readInt1()
  var a = readSeq().map(parseInt)

  var odd_score = 0
  var evn_score = 0
  for i in 0..<n:
    if i mod 2 == 0 and a[i] >= 0:
      evn_score += a[i]
    if i mod 2 != 0 and a[i] >= 0:
      odd_score += a[i]

  let score = max(odd_score, evn_score)
  if score <= 0:
    echo max(a)
    echo(a.len() - 1)
    let ind = a.find(max(a))
    for i in countdown(a.len() - 1, ind + 1):
      echo(i + 1)
    for i in 0..<ind:
      echo(0 + 1)
    return

  var ans: seq[int] = @[]

  if odd_score > evn_score:
    debug a
    ans.add(0)
    a.delete(0, 0)

  # let's gather even
  while a.len() > 1:
    debug a
    let tail = a.len() - 1
    if tail mod 2 == 1:
      ans.add(tail)
      a.delete(tail)
      continue

    if a[tail] <= 0:
      ans.add(tail)
      a.delete(tail)
      continue

    if a[tail - 2] >= 0:
      ans.add(tail - 1)
      a[tail - 2] = a[tail - 2] + a[tail]
      a.delete(tail - 1, tail)
      continue
    else:
      if tail - 2 == 0:
        ans.add(0)
        ans.add(0)
        a.delete(0, 1)
        continue
      else:
        ans.add(tail - 2)
        a[tail - 3] = a[tail - 3] + a[tail - 1]
        a.delete(tail - 2, tail - 1)
        continue
  debug a

  echo score
  echo ans.len()
  for v in ans:
    echo(v + 1)

main()


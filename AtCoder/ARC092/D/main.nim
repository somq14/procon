import strutils
import sequtils
import algorithm
import math
import queues
import tables
import logging
import future

const INF* = int(1e18 + 373)

when not defined(release):
  addHandler(newFileLogger(stderr, lvlAll, "[ $levelname ] "))

proc readInt1*(): int = stdin.readLine().strip().parseInt()
proc readInt2*(): (int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1])
proc readInt3*(): (int, int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1], v[2])

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#

proc main() =
  let n = readInt1()
  let a = readSeq().map(parseInt)
  let b = readSeq().map(parseInt)

  var ans = 0
  for k in 0..28:
    let k2 = 1 shl k

    let mask = (1 shl (k + 1)) - 1
    let c = a.map(it => it and mask).sorted(cmp[int]).reversed()
    let d = b.map(it => it and mask).sorted(cmp[int])

    var cnt = 0

    var s0 = 0
    var t0 = 0
    var s1 = 0
    var t1 = 0
    for i in 0..<n:
      while s0 < n and d[s0] < 1 * k2 - c[i]:
        s0 += 1
      while t0 < n and d[t0] < 2 * k2 - c[i]:
        t0 += 1
      cnt += t0 - s0

      while s1 < n and d[s1] < 3 * k2 - c[i]:
        s1 += 1
      while t1 < n and d[t1] < 4 * k2 - c[i]:
        t1 += 1
      cnt += t1 - s1

    if (cnt mod 2) == 1:
      ans = ans or (1 shl k)

  echo ans

main()


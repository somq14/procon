import strutils
import sequtils
import algorithm
import math
import queues
import tables
import logging
import future

const INF* = int(1e18 + 373)

when not defined(release):
  addHandler(newFileLogger(stderr, lvlAll, "[ $levelname ] "))

proc readInt1*(): int = stdin.readLine().strip().parseInt()
proc readInt2*(): (int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1])
proc readInt3*(): (int, int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1], v[2])

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#
type Pos = tuple[y, x: int]

proc dfs(n: int, g: seq2[bool], v, t: int, visited: var seq[bool]; ans: var seq[int]) =
  visited[v] = true

  if v == t:
    ans.add(v)
    return

  for u in 0..<n:
    if g[v][u] and not visited[u]:
      dfs(n, g, u, t, visited, ans)
      if ans.len() > 0:
        ans.add(v)
        return

proc maxflow(n: int, g: seq2[bool], s, t: int): int =
  var g = g

  result = 0

  while true:
    var visited = newSeq[bool](n)
    var ans = newSeq[int](0)
    dfs(n, g, s, t, visited, ans)

    if ans.len() == 0:
      break

    result += 1
    for i in 0..<(ans.len() - 1):
      g[ans[i + 1]][ans[i]] = false
      g[ans[i]][ans[i + 1]] = true


proc main() =
  let n = readInt1()

  var red = newSeq[Pos](n)
  for i in 0..<n:
    let (a, b) = readInt2()
    red[i] = (a, b)

  var blue = newSeq[Pos](n)
  for i in 0..<n:
    let (c, d) = readInt2()
    blue[i] = (c, d)

  var g = newSeq2[bool](2 * n + 2, 2 * n + 2)
  for i in 0..<n:
    for j in 0..<n:
      g[i][n + j] = (red[i].y < blue[j].y and red[i].x < blue[j].x)

  for i in 0..<n:
    g[2 * n][i] = true
    g[n + i][2 * n + 1] = true

  echo maxflow(2 * n + 2, g, 2 * n, 2 * n + 1)

main()


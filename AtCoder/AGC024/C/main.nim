import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import logging
import future

const INF* = int(1e18 + 373)

when not defined(release):
  addHandler(newFileLogger(stderr, lvlAll, "[ $levelname ] "))

proc readInt1*(): int = stdin.readLine().strip().parseInt()
proc readInt2*(): (int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1])
proc readInt3*(): (int, int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1], v[2])
proc readInt4*(): (int, int, int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1], v[2], v[3])

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#

proc solve(n: int; a: seq[int]): int =
  for i in 0..<n:
    if a[i] > i:
      return -1

  for i in 0..<n - 1:
    if a[i] + 1 < a[i + 1]:
      return -1

  var ans = 0
  var i = n - 1
  while i >= 0:
    ans += a[i]
    i -= 1
    while i >= 0 and a[i + 1] == a[i] + 1:
      i -= 1

  return ans

proc main() =
  let n = readInt1()
  let a = readSeq(n).map(parseInt)
  echo solve(n, a)

main()


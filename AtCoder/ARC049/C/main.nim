import strutils
import sequtils
import algorithm
import math
import queues
import tables
import logging
import future

const INF* = int(1e18 + 373)

when not defined(release):
  addHandler(newFileLogger(stderr, lvlAll, "[ $levelname ] "))

proc readInt1*(): int = stdin.readLine().strip().parseInt()
proc readInt2*(): (int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1])
proc readInt3*(): (int, int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1], v[2])

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#
proc contains(s, i: int): bool = (s and (1 shl i)) > 0

proc solve(n: int, inEdge, outEdge: seq2[int]): int =
  var q = initQueue[int]()

  var visited = newSeq[bool](n)
  for v in 0..<n:
    if inEdge[v].len() == 0:
      visited[v] = true
      q.enqueue(v)

  var cnt = newSeq[int](n)

  while q.len() > 0:
    let v = q.dequeue()
    for u in outEdge[v]:
      cnt[u] += 1
      if cnt[u] == inEdge[u].len() and not visited[u]:
        visited[u] = true
        q.enqueue(u)

  result = 0
  for v in 0..<n:
    if visited[v]:
      result += 1

proc main() =
  let n = readInt1()

  let a = readInt1()
  var x = newSeq[int](a)
  var y = newSeq[int](a)
  for i in 0..<a:
    (x[i], y[i]) = readInt2()
    x[i] -= 1
    y[i] -= 1

  let b = readInt1()
  var u = newSeq[int](b)
  var v = newSeq[int](b)
  for i in 0..<b:
    (u[i], v[i]) = readInt2()
    u[i] -= 1
    v[i] -= 1

  var ans = 0
  for s in 0..<(1 shl b):
    var inEdge = newSeq2[int](n, 0)
    var outEdge = newSeq2[int](n, 0)
    for i in 0..<a:
      inEdge[x[i]].add(y[i])
      outEdge[y[i]].add(x[i])

    for i in 0..<b:
      if i in s:
        outEdge[u[i]].add(u[i])
        inEdge[u[i]].add(u[i])
      else:
        outEdge[u[i]].add(v[i])
        inEdge[v[i]].add(u[i])

    ans = max(solve(n, inEdge, outEdge), ans)

  echo ans

main()


#include <algorithm>
#include <complex>
#include <iomanip>
#include <iostream>
#include <queue>
#include <set>
#include <string>
#include <vector>

using std::cin;
using std::cout;
using std::endl;

using std::string;
using std::to_string;
using std::vector;
using std::set;
using std::queue;
using std::priority_queue;

using std::min;
using std::max;
using std::sort;
using std::abs;

int main(void) {
    int n;
    cin >> n;

    vector<int> a(n);
    for (int i = 0; i < n; i++) {
        cin >> a[i];
        a[i]--;
    }

    vector<bool> visited(n);

    int i;
    int cnt = 0;
    for (i = 0; i != 1 && !visited[i]; i = a[i]) {
        visited[i] = true;
        cnt++;
    }

    cout << (i == 1 ? cnt : -1) << endl;


    return 0;
}

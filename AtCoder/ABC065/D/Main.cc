#include <algorithm>
#include <complex>
#include <iomanip>
#include <iostream>
#include <queue>
#include <set>
#include <string>
#include <vector>

using std::cin;
using std::cout;
using std::endl;

using std::string;
using std::to_string;
using std::vector;
using std::set;
using std::queue;
using std::priority_queue;

using std::min;
using std::max;
using std::sort;
using std::abs;

class point {
   public:
    int id;
    int x, y;
    point() : id(-1), x(), y() {}
    point(int id, int x, int y) : id(id), x(x), y(y) {}
};

bool compare_x(const point& p1, const point& p2) { return p1.x < p2.x; }
bool compare_y(const point& p1, const point& p2) { return p1.y < p2.y; }

class edge {
   public:
    int u, v, c;
    edge() : u(), v(), c() {}
    edge(int u, int v, int c) : u(u), v(v), c(c) {}

    bool operator<(const edge& e) const { return c < e.c; }
};

class unionfind {
   private:
    vector<int> a;
    vector<int> h;

   public:
    unionfind(int n) : a(n, -1), h(n) {}

    int find(int v) {
        if (a[v] < 0) {
            return v;
        }
        return a[v] = find(a[v]);
    }

    bool same(int u, int v) { return find(u) == find(v); }

    void unite(int u, int v) {
        const int urt = find(u);
        const int vrt = find(v);

        if (urt == vrt) {
            return;
        }

        if (h[urt] > h[vrt]) {
            a[vrt] = urt;
        } else if (h[urt] < h[vrt]) {
            a[urt] = vrt;
        } else {
            a[vrt] = urt;
            h[urt]++;
        }
    }
};

int eval(const point& p1, const point& p2) {
    return min(abs(p1.x - p2.x), abs(p1.y - p2.y));
}

int main(void) {
    int n;
    cin >> n;

    vector<point> p(n);
    for (int i = 0; i < n; i++) {
        int x, y;
        cin >> x >> y;
        p[i] = point(i, x, y);
    }

    vector<edge> es;

    sort(p.begin(), p.end(), compare_x);
    for (int i = 0; i < n - 1; i++) {
        es.push_back(edge(p[i].id, p[i + 1].id, eval(p[i], p[i + 1])));
    }

    sort(p.begin(), p.end(), compare_y);
    for (int i = 0; i < n - 1; i++) {
        es.push_back(edge(p[i].id, p[i + 1].id, eval(p[i], p[i + 1])));
    }

    unionfind uf(n);
    sort(es.begin(), es.end());

    int ans = 0;
    int cnt = 0;
    for (const edge& e : es) {
        if (cnt >= n - 1) {
            break;
        }
        if (uf.same(e.u, e.v)) {
            continue;
        }
        uf.unite(e.u, e.v);
        ans += e.c;
        cnt++;
    }

    cout << ans << endl;

    return 0;
}

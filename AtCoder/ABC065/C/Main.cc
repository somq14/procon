#include <algorithm>
#include <complex>
#include <iomanip>
#include <iostream>
#include <queue>
#include <set>
#include <string>
#include <vector>

using std::cin;
using std::cout;
using std::endl;

using std::string;
using std::to_string;
using std::vector;
using std::set;
using std::queue;
using std::priority_queue;

using std::min;
using std::max;
using std::sort;
using std::abs;

const unsigned long long MOD = 1e9 + 7;
typedef unsigned long long ull;

ull fact(int n){
    ull ans = 1;
    for(int i = 1; i <= n; i++){
        ans *= i;
        ans %= MOD;
    }
    return ans;
}

int main(void) {
    int n, m;
    cin >> n >> m;

    const int a = min(n, m);
    const int b = max(n, m);

    ull ans;
    if(a == b){
        ans = fact(a) * fact(b) % MOD;
        ans = ans * 2 % MOD;
    }else if(a == b - 1){
        ans = fact(a) * fact(b) % MOD;
    }else{
        ans = 0;
    }

    cout << ans << endl;
    return 0;
}

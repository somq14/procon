#include <iostream>
#include <string>
#include <vector>

using std::cin;
using std::cout;
using std::endl;

using std::vector;

using std::string;

bool ismine(vector<string> ss, int w, int h, int i, int j) {
    if (!(0 <= i && i < h)) {
        return false;
    }
    if (!(0 <= j && j < w)) {
        return false;
    }
    return ss[i][j] == '#';
}

int main(void) {
    int h, w;
    cin >> h >> w;
    vector<string> ss(h);

    for (int i = 0; i < h; i++) {
        string s;
        cin >> s;
        ss[i] = s;
    }

    for (int i = 0; i < h; i++) {
        for (int j = 0; j < w; j++) {
            if (ss[i][j] != '.') {
                continue;
            }
            int cnt = 0;
            cnt += ismine(ss, w, h, i + 1, j + 1);
            cnt += ismine(ss, w, h, i + 1, j);
            cnt += ismine(ss, w, h, i + 1, j - 1);
            cnt += ismine(ss, w, h, i, j + 1);
            cnt += ismine(ss, w, h, i, j - 1);
            cnt += ismine(ss, w, h, i - 1, j + 1);
            cnt += ismine(ss, w, h, i - 1, j);
            cnt += ismine(ss, w, h, i - 1, j - 1);
            ss[i][j] = cnt + '0';
        }
    }
    for (int i = 0; i < h; i++) {
        cout << ss[i] << endl;
    }
    return 0;
}

import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import future

const INF* = int(1e18 + 373)

proc readLine*(): string =
  stdin.readLine()
proc readSeq*(): seq[string] =
  readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = readLine().strip()
proc readInt1*(): int =
  readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])
proc newSeqWith*[T](n: Natural; e: T): seq[T] =
  result = newSeq[T](n)
  for i in 0..<n:
    result[i] = e
type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] =
  newSeqWith(n1, newSeq[T](n2))
type seq3*[T] = seq[seq[seq[T]]]
proc newSeq3*[T](n1, n2, n3: Natural): seq3[T] =
  newSeqWith(n1, newSeqWith(n2, newSeq[T](n3)))

#------------------------------------------------------------------------------#

proc toSeq(n, w: int): seq[int] =
  var n = n
  result = newSeq[int](w)
  for i in 0..<w:
    result[i] = n mod 10
    n = n div 10

proc width(a: int): int =
  result = 0
  var a = a
  while a != 0:
    a = a div 10
    result += 1

var memo: Table[(int, int, bool), int]

proc solve(d, w: int; head: bool): int =
  if w <= 0:
    return if d == 0: 1 else: 0
  if w <= 1:
    return if d == 0: 10 else: 0

  if (d, w, head) in memo:
    return memo[(d, w, head)]

  let lsd = d mod 10

  var ans = 0
  for i in 0..9:
    for j in 0..9:
      if head and i == 0:
        continue
      if (i - j + 10) mod 10 != (lsd + 10) mod 10:
        continue
      var nextD = d
      nextD -= i - j
      nextD -= (10 ^ (w - 1)) * (j - i)
      ans += solve(nextD div 10, w - 2, false)

  memo[(d, w, head)] = ans
  return ans

proc main() =
  let d = readInt1()

  var ans = 0
  for w in 2..18:
    memo = initTable[(int, int, bool), int]()
    ans += solve(d, w, true)
  echo ans

main()


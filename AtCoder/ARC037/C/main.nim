import strutils
import sequtils
import algorithm
import math
import queues
import tables
import future

const INF* = int(1e18 + 373)

proc readInt1*(): int = stdin.readLine().strip().parseInt()
proc readInt2*(): (int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1])
proc readInt3*(): (int, int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1], v[2])

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#
type Predicate1[T] = T -> bool

# [lb, ub)
proc nibutanLb(lb, ub: int; f: Predicate1[int]): int =
  var lb = lb
  var ub = ub
  while ub - lb > 1:
    let mid = (ub + lb) div 2
    if f(mid):
      lb = mid
    else:
      ub = mid
  return lb

# (lb, ub]
proc nibutanUb(lb, ub: int; f: Predicate1[int]): int =
  var lb = lb
  var ub = ub
  while ub - lb > 1:
    let mid = (ub + lb) div 2
    if f(mid):
      ub = mid
    else:
      lb = mid
  return ub

proc countLessThan(n: int; a, b: seq[int], x: int): int =
  result = 0
  for i in 0..<n:
    result += lowerBound(b, 1 + x div a[i])

proc main() =
    let (n, k) = readInt2()
    let a = readSeq().map(parseInt).sorted(cmp[int])
    let b = readSeq().map(parseInt).sorted(cmp[int])

    let f = (x: int) => countLessThan(n, a, b, x) >= k
    echo nibutanUb(0, int(1e18 + 1), f)

main()


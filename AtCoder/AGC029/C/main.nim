import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import future

const INF* = int(1e18 + 373)

proc readLine*(): string =
  stdin.readLine()
proc readSeq*(): seq[string] =
  readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = readLine().strip()
proc readInt1*(): int =
  readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])
proc newSeqWith*[T](n: Natural; e: T): seq[T] =
  result = newSeq[T](n)
  for i in 0..<n:
    result[i] = e
type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] =
  newSeqWith(n1, newSeq[T](n2))
type seq3*[T] = seq[seq[seq[T]]]
proc newSeq3*[T](n1, n2, n3: Natural): seq3[T] =
  newSeqWith(n1, newSeqWith(n2, newSeq[T](n3)))

#------------------------------------------------------------------------------#
type Pred1[T] = T -> bool

# [lb, ub)
proc nibutanLb(lb, ub: int; f: Pred1[int]): int =
  var lb = lb
  var ub = ub
  while ub - lb > 1:
    let mid = (ub + lb) div 2
    if f(mid):
      lb = mid
    else:
      ub = mid
  return lb

# (lb, ub]
proc nibutanUb(lb, ub: int; f: Pred1[int]): int =
  var lb = lb
  var ub = ub
  while ub - lb > 1:
    let mid = (ub + lb) div 2
    if f(mid):
      ub = mid
    else:
      lb = mid
  return ub

#------------------------------------------------------------------------------#
type D =
  tuple [ a, b, c: int ]

proc increment(n: int; a: seq[int]; x: int; s: var seq[D]): bool =
  var carryCnt = 0

  while s.len() > 0:
    s[s.len() - 1].b -= 1
    s[s.len() - 1].c -= 1
    let poped = s[s.len() - 1].a

    if s[s.len() - 1].b <= 0:
      discard s.pop()

    if poped < x - 1:
      if s.len() == 0:
        s.add((poped + 1, 1, 1))
      elif s[s.len() - 1].a == poped + 1:
        s[s.len() - 1].b += 1
        s[s.len() - 1].c += 1
      else:
        s.add((poped + 1, 1, s[s.len() - 1].c + 1))
      break

    carryCnt += 1

  if s.len() == 0:
    return false

  if carryCnt == 0:
    return true

  if s[s.len() - 1].a == 0:
    s[s.len() - 1].b += carryCnt
    s[s.len() - 1].c += carryCnt
  else:
    s.add((0, carryCnt, s[s.len() - 1].c + carryCnt))

  return true

proc append(n: int; a: seq[int]; x: int; s: var seq[D]; i: int) =
  let d = a[i] - a[i - 1]
  if s[s.len() - 1].a == 0:
    s[s.len() - 1].b += d
    s[s.len() - 1].c += d
  else:
    s.add((0, d, s[s.len() - 1].c + d))

proc trim(n: int; a: seq[int]; x: int; s: var seq[D]; i: int) =
  var lb = -1
  var ub = s.len() - 1
  while ub - lb > 1:
    let mid = (ub + lb) div 2
    if s[mid].c >= a[i]:
      ub = mid
    else:
      lb = mid

  s.setLen(ub + 1)

  let d = s[s.len() - 1].c - a[i]
  assert(d >= 0)
  if d == 0:
    return

  s[s.len() - 1].b -= d
  s[s.len() - 1].c -= d
  if s[s.len() - 1].b == 0:
    discard s.pop()

proc eval(n: int; a: seq[int]; x: int): bool =
  var s = newSeq[D](0)
  s.add((0, a[0], a[0]))

  for i in 1..<n:
    if a[i - 1] == a[i]:
      if not increment(n, a, x, s):
        return false
    elif a[i - 1] < a[i]:
      append(n, a, x, s, i)
    elif a[i - 1] > a[i]:
      trim(n, a, x, s, i)
      if not increment(n, a, x, s):
        return false
    else:
      assert(false)

  return true

proc main() =
  let n = readInt1()
  let a = readSeq().map(parseInt)

  var incr = true
  for i in 0..<n - 1:
    if a[i] >= a[i + 1]:
      incr = false
      break

  if incr:
    echo 1
    return

  let ans = nibutanUb(1, n, it => eval(n, a, it))
  echo ans

main()


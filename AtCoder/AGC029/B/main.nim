import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import future

const INF* = int(1e18 + 373)

proc readLine*(): string =
  stdin.readLine()
proc readSeq*(): seq[string] =
  readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = readLine().strip()
proc readInt1*(): int =
  readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])
proc newSeqWith*[T](n: Natural; e: T): seq[T] =
  result = newSeq[T](n)
  for i in 0..<n:
    result[i] = e
type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] =
  newSeqWith(n1, newSeq[T](n2))
type seq3*[T] = seq[seq[seq[T]]]
proc newSeq3*[T](n1, n2, n3: Natural): seq3[T] =
  newSeqWith(n1, newSeqWith(n2, newSeq[T](n3)))

#------------------------------------------------------------------------------#

proc main() =
  let n = readInt1()
  let a = readSeq().map(parseInt).sorted(cmp[int])

  var t = initTable[int, int]()
  for i in 0..<n:
    if a[i] notin t:
      t[a[i]] = 0
    t[a[i]] += 1

  var ans = 0
  for i in countdown(n - 1, 0):
    if t[a[i]] == 0:
      continue

    var target = -1
    for j in 0..<33:
      if (1 shl j) > a[i]:
        target = (1 shl j) - a[i]
        break

    if target > a[i]:
      continue

    if target == a[i]:
      ans += t[a[i]] div 2
      t[a[i]] = t[a[i]] mod 2
      continue

    if target in t and t[target] > 0:
      t[a[i]] -= 1
      t[target] -= 1
      ans += 1

  echo ans

main()


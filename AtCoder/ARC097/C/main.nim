import strutils
import sequtils
import algorithm
import math
import queues
import tables
import logging
import future

const INF* = int(1e18 + 373)

when not defined(release):
  addHandler(newFileLogger(stderr, lvlAll, "[ $levelname ] "))

proc readInt1*(): int = stdin.readLine().strip().parseInt()
proc readInt2*(): (int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1])
proc readInt3*(): (int, int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1], v[2])

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#

proc main() =
  let s = stdin.readLine()
  let n = s.len()
  let k = readInt1()

  var a = newSeq[string](0)
  for i in 0..<n:
    for j in 1..k:
      if i + j <= n:
        a.add(s[i..<i + j])
  a.sort(cmp[string])

  let m = a.len()
  var b = @[ a[0] ]
  for i in 0..<m:
    if b[b.len() - 1] != a[i]:
      b.add(a[i])

  echo b[k - 1]

main()


import strutils
import sequtils
import algorithm
import math
import queues
import tables
import logging
import future

const INF* = int(1e18 + 373)

when not defined(release):
  addHandler(newFileLogger(stderr, lvlAll, "[ $levelname ] "))

proc readInt1*(): int = stdin.readLine().strip().parseInt()
proc readInt2*(): (int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1])
proc readInt3*(): (int, int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1], v[2])

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

type seq3*[T] = seq[seq[seq[T]]]
proc newSeq3*[T](n1, n2, n3: Natural): seq3[T] = newSeqWith(n1, newSeqWith(n2, newSeq[T](n3)))

#------------------------------------------------------------------------------#
type Ball = tuple [ c, x: int ]

proc main() =
  let n = readInt1()

  var a = newSeq[Ball](2 * n)
  for i in 0..<2 * n:
    let line = stdin.readLine().split()
    let c = if line[0] == "B": 0 else: 1
    let x = line[1].parseInt() - 1
    a[i] = (c, x)

  var cost = newSeq3[int](2, n + 1, 2 * n + 1)
  for c in 0..1:
    for x in 0..n:
      for p in 1..2 * n:
        var opt = cost[c][x][p - 1]
        if a[p - 1].c == c and a[p - 1].x >= x:
          opt += 1
        cost[c][x][p] = opt

  var pos = newSeq2[int](2, n)
  for i in 0..<2 * n:
    pos[a[i].c][a[i].x] = i

  var dp = newSeq2[int](n + 1, n + 1)
  for i in 0..n:
    for j in 0..n:
      if i == 0 and j == 0:
        dp[0][0] = 0
        continue

      var opt = INF
      if i > 0:
        let p = pos[0][i - 1]
        var opt0 = dp[i - 1][j]
        opt0 += cost[0][i - 1][p]
        opt0 += cost[1][j][p]
        opt = min(opt, opt0)

      if j > 0:
        let p = pos[1][j - 1]
        var opt1 = dp[i][j - 1]
        opt1 += cost[1][j - 1][p]
        opt1 += cost[0][i][p]
        opt = min(opt, opt1)
      dp[i][j] = opt

  debug dp
  echo dp[n][n]

main()


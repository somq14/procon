import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import future

const INF* = int(1e18 + 373)

proc readLine*(): string =
  stdin.readLine()
proc readSeq*(): seq[string] =
  readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = readLine().strip()
proc readInt1*(): int =
  readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])
proc newSeqWith*[T](n: Natural; e: T): seq[T] =
  result = newSeq[T](n)
  for i in 0..<n:
    result[i] = e
type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] =
  newSeqWith(n1, newSeq[T](n2))
type seq3*[T] = seq[seq[seq[T]]]
proc newSeq3*[T](n1, n2, n3: Natural): seq3[T] =
  newSeqWith(n1, newSeqWith(n2, newSeq[T](n3)))

#------------------------------------------------------------------------------#

proc dfs1(g: seq2[int]; c: string; v, p: int; ans: var seq2[int]): bool =
  var allBlack = true
  for u in g[v]:
    if p == u:
      continue
    if not dfs1(g, c, u, v, ans):
      allBlack = false
      ans[v].add(u)
      ans[u].add(v)
  return allBlack and c[v] == 'B'

proc dfs2(g: seq2[int]; c: string; v, p: int): int =
  var res = 0

  for u in g[v]:
    if u == p:
      continue
    res += dfs2(g, c, u, v) + 2

  if c[v] == 'W' xor g[v].len() mod 2 == 1:
    res += 1

  return res

proc dfs3(g: seq2[int]; c: string; v, p: int): (int, int) =
  var opt = -1
  var optV = -1

  var k = 0
  for u in g[v]:
    if u == p:
      continue
    var res = dfs3(g, c, u, v)
    if res[0] > opt:
      (opt, optV) = res
    k += 1

  if k == 0:
    return (0, v)

  if c[v] == 'W' xor g[v].len() mod 2 == 1:
    opt += 1
  return (opt, optV)

proc main() =
  let n = readInt1()

  var g = newSeq2[int](n, 0)
  for i in 0..<n - 1:
    let (a, b) = readInt2()
    g[a - 1].add(b - 1)
    g[b - 1].add(a - 1)

  let c = readLine()

  if c.count('W') == 0:
    echo 0
    return

  if c.count('W') == 1:
    echo 1
    return

  var root = -1
  for v in 0..<n:
    if c[v] == 'W':
      root = v
      break

  var gg = newSeq2[int](n, 0)
  discard dfs1(g, c, root, -1, gg)

  let base = dfs2(gg, c, root, -1)

  var leaf = -1
  for v in 0..<n:
    if gg[v].len() == 1:
      leaf = v
      break

  let res1 = dfs3(gg, c, leaf, -1)
  let res2 = dfs3(gg, c, res1[1], -1)
  let ans = base - res2[0] * 2
  echo ans

main()


import strutils
import sequtils
import algorithm
import math
import queues
import tables
import logging
import future
import sets

const INF* = int(1e18 + 373)

when not defined(release):
  addHandler(newFileLogger(stderr, lvlAll, "[ $levelname ] "))

proc readInt1*(): int = stdin.readLine().strip().parseInt()
proc readInt2*(): (int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1])
proc readInt3*(): (int, int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1], v[2])

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#
type UnionFindTree = tuple[ p, h, w: seq[int] ]

proc initUnionFindTree(n: int): UnionFindTree =
    var p = newSeq[int](n)
    p.fill(-1)
    var h = newSeq[int](n)
    h.fill(0)
    var w = newSeq[int](n)
    w.fill(1)
    return (p, h, w)

proc find(this: var UnionFindTree, v: int): int =
    if this.p[v] == -1:
        return v
    this.p[v] = find(this, this.p[v])
    return this.p[v]

proc same(this: var UnionFindTree, u, v: int): bool =
    this.find(u) == this.find(v)

proc union(this: var UnionFindTree, u, v: int) =
    let uRoot = this.find(u)
    let vRoot = this.find(v)
    if uRoot == vRoot:
        return

    if this.h[uRoot] > this.h[vRoot]:
        this.p[vRoot] = uRoot
        this.w[uRoot] += this.w[vRoot]
    else:
        this.p[uRoot] = vRoot
        this.w[vRoot] += this.w[uRoot]
        if this.h[uRoot] == this.h[vRoot]:
            this.h[vRoot] += 1

type P = tuple [ id, pos: int ]

proc eval(n: int; g: seq[P]): int =
  var s = initSet[int]()
  for i in 0..<n:
    s.incl(g[i].id)

  var ans = 0
  for i in 0..<n:
    if g[i].pos in s:
      ans += 1
  return ans

proc main() =
  let (n, m) = readInt2()
  let p = readSeq().map(parseInt).map(it => it - 1)

  var x = newSeq[int](m)
  var y = newSeq[int](m)
  for i in 0..<m:
    (x[i], y[i]) = readInt2()
    x[i] -= 1
    y[i] -= 1

  var uft = initUnionFindTree(n)
  for i in 0..<m:
    uft.union(x[i], y[i])

  for i in 0..<n:
    discard uft.find(i)

  var k = 0
  for i in 0..<n:
    k = max(k, uft.find(i) + 1)

  var g = newSeq2[P](k, 0)
  for i in 0..<n:
    g[uft.find(i)].add((p[i], i))


  var ans = 0
  for i in 0..<k:
    ans += eval(g[i].len(), g[i])

  echo ans

main()


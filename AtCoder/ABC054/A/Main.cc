#include <algorithm>
#include <cctype>
#include <complex>
#include <iomanip>
#include <iostream>
#include <queue>
#include <set>
#include <string>
#include <vector>

using std::cin;
using std::cout;
using std::endl;

using std::string;
using std::to_string;
using std::vector;
using std::set;
using std::queue;
using std::priority_queue;

using std::min;
using std::max;
using std::sort;
using std::abs;

int main(void) {
    vector<int> table(14);
    table[1] = 12;
    table[2] = 0;
    table[3] = 1;
    table[4] = 2;
    table[5] = 3;
    table[6] = 4;
    table[7] = 5;
    table[8] = 6;
    table[9] = 7;
    table[10] = 8;
    table[11] = 9;
    table[12] = 10;
    table[13] = 11;

    int a, b;
    cin >> a >> b;
    if (table[a] == table[b]) {
        cout << "Draw" << endl;
    } else if (table[a] > table[b]) {
        cout << "Alice" << endl;
    } else {
        cout << "Bob" << endl;
    }
    return 0;
}

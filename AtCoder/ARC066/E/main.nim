import strutils
import sequtils
import algorithm
import math
import queues
import tables
import logging
import future

const INF* = int(1e18 + 373)

when not defined(release):
  addHandler(newFileLogger(stderr, lvlAll, "[ $levelname ] "))

proc readInt1*(): int = stdin.readLine().strip().parseInt()
proc readInt2*(): (int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1])
proc readInt3*(): (int, int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1], v[2])

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#
proc solve(n: int; op: seq[char]; num: seq[int]): int =
  var dp = newSeq2[int](n + 1, 3)
  dp[0][0] = 0
  dp[0][1] = -INF
  dp[0][2] = -INF
  for i in 1..n:
    let a = num[i - 1]
    let dp0 = dp[i - 1][0]
    let dp1 = dp[i - 1][1]
    let dp2 = dp[i - 1][2]

    case op[i - 1]:
    of '+':
      dp[i][0] = max([dp0 + a, dp1 + a, dp2 + a])
      dp[i][1] = max([dp1 - a, dp2 + a])
      dp[i][2] = dp2 + a
    of '-':
      dp[i][0] = max([dp0 - a, dp1 + a, dp2 + a])
      dp[i][1] = max([dp0 - a, dp1 + a, dp2 + a])
      dp[i][2] = max([dp1 + a, dp2 + a])
    else: discard

  return max([ dp[n][0], dp[n][1], dp[n][2] ])

proc main() =
  let n = readInt1()
  let a = stdin.readLine().split()

  var op = newSeq[char](n)
  var num = newSeq[int](n)

  op[0] = '+'
  num[0] = a[0].parseInt()
  var p = 1
  for i in 1..<n:
    op[i] = a[p][0]
    num[i] = a[p + 1].parseInt()
    p += 2

  echo solve(n, op, num)

main()


import strutils
import sequtils
import algorithm
import math
import queues
import tables
import logging

const INF* = int(1e18 + 373)

when not defined(release):
  addHandler(newFileLogger(stderr, lvlAll, "[ $levelname ] "))

proc readInt1*(): int = stdin.readLine().strip().parseInt()
proc readInt2*(): (int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1])
proc readInt3*(): (int, int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1], v[2])

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#

type Node = ref object
    y, x: int
    l, r, u, d: Node

var nodeTable = initTable[(int, int), Node]()

proc getNode(y, x: int): Node =
  let p = (y, x)
  if p in nodeTable:
    return nodeTable[p]

  result = new Node
  result.y = y
  result.x = x
  result.l = nil
  result.r = nil
  result.u = nil
  result.d = nil
  nodeTable[p] = result

proc visit(this: Node) =
  if this.l == nil:
    this.l = getNode(this.y, this.x - 1)
  if this.r == nil:
    this.r = getNode(this.y, this.x + 1)
  if this.u == nil:
    this.u = getNode(this.y + 1, this.x)
  if this.d == nil:
    this.d = getNode(this.y - 1, this.x)

  this.l.r = this.r
  this.r.l = this.l
  this.u.d = this.d
  this.d.u = this.u

proc `$`(this: Node): string = "(" & $this.y & ", " & $this.x & ")"

proc main() =
  let k = readInt1()
  let s = stdin.readLine()

  var pos = getNode(0, 0)
  pos.visit()

  for i in 0..<k:
    debug pos
    case s[i]:
    of 'L':
      pos.l.visit()
      pos = pos.l
    of 'R':
      pos.r.visit()
      pos = pos.r
    of 'U':
      pos.u.visit()
      pos = pos.u
    of 'D':
      pos.d.visit()
      pos = pos.d
    else: assert(false)

  echo pos.x, " ", pos.y

main()


import strutils
import sequtils
import algorithm
import math
import queues
import tables
import logging
import future

const INF* = int(1e18 + 373)

when not defined(release):
  addHandler(newFileLogger(stderr, lvlAll, "[ $levelname ] "))

proc readInt1*(): int = stdin.readLine().strip().parseInt()
proc readInt2*(): (int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1])
proc readInt3*(): (int, int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1], v[2])

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#

proc main() =
  let a = readSeq().map(parseInt)
  var i = a[0]
  var o = a[1]
  var t = a[2]
  var j = a[3]
  var l = a[4]
  var s = a[5]
  var z = a[6]

  var ans = o

  # lji
  case l mod 2 + j mod 2 + i mod 2:
  of 3:
    l -= 1
    j -= 1
    i -= 1
    ans += 3
  of 2:
    if l > 0 and j > 0 and i > 0:
      l -= 1
      j -= 1
      i -= 1
      ans += 3
  else: discard

  ans += (l div 2 + j div 2 + i div 2) * 2
  echo ans

main()


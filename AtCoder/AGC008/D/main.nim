import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import future

const INF* = int(1e18 + 373)

proc readLine*(): string =
  stdin.readLine()
proc readSeq*(): seq[string] =
  readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = readLine().strip()
proc readInt1*(): int =
  readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])
proc newSeqWith*[T](n: Natural; e: T): seq[T] =
  result = newSeq[T](n)
  for i in 0..<n:
    result[i] = e
type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] =
  newSeqWith(n1, newSeq[T](n2))
type seq3*[T] = seq[seq[seq[T]]]
proc newSeq3*[T](n1, n2, n3: Natural): seq3[T] =
  newSeqWith(n1, newSeqWith(n2, newSeq[T](n3)))

#------------------------------------------------------------------------------#
type PriorityQueue[T] = object
  a: seq[T]
  n: Natural
  cmp: (a: T, b: T) -> bool

proc initPriorityQueue[T](cmp: (a: T, b: T) -> bool): PriorityQueue[T] =
  result.a = newSeq[T](1)
  result.n = Natural(0)
  result.cmp = cmp

proc len[T](this: PriorityQueue[T]): Natural =
  this.n

proc enqueue[T](this: var PriorityQueue[T], e: T) =
  this.n += 1
  this.a.add(e)
  var i = this.a.len() - 1
  while i > 1 and this.cmp(this.a[i], this.a[i div 2]):
    swap(this.a[i], this.a[i div 2])
    i = i div 2

proc dequeue[T](this: var PriorityQueue[T]): T =
  this.n -= 1
  result = this.a[1]
  this.a[1] = this.a.pop()
  var i = 1
  while i < this.a.len():
    var j = i
    if i * 2 < this.a.len() and this.cmp(this.a[i * 2], this.a[j]):
      j = i * 2
    if i * 2 + 1 < this.a.len() and this.cmp(this.a[i * 2 + 1], this.a[j]):
      j = i * 2 + 1
    if i == j:
      break
    swap(this.a[i], this.a[j])
    i = j

proc front[T](this: PriorityQueue[T]): T =
  this.a[1]

#------------------------------------------------------------------------------#

type Work = tuple [ s, t, v: int ]

proc main() =
  let n = readInt1()
  let x = readSeq().map(parseInt).map(it => it - 1)

  var ws = newSeq[Work]()
  for i in 0..<n:
    for _ in 0..<i:
      ws.add((0, x[i], i))

    ws.add((x[i], x[i] + 1, i))

    for _ in (i + 1)..<n:
      ws.add((x[i] + 1, n * n, i))

  ws.sort(cmp[Work])
  ws.reverse()

  var que = initPriorityQueue[Work]((w1: Work, w2: Work) => (w1.t < w2.t))
  var a = newSeq[int](n * n)
  var ans = true
  for i in 0..<n * n:
    while ws.len() > 0 and ws[ws.len() - 1].s <= i:
      que.enqueue(ws.pop())

    if que.len() == 0:
      ans = false
      break

    let w = que.dequeue()
    if i >= w.t:
      ans = false
      break

    a[i] = w.v

  if ans:
    echo "Yes"
    echo a.map(it => $(it + 1)).join(" ")
  else:
    echo "No"

main()

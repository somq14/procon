#include <iostream>

using std::cin;
using std::cout;
using std::endl;

int main(void) {
    int n;
    cin >> n;

    int sum = 0;
    for(int i = 0; i < n; i++){
        int l, r;
        cin >> l >> r;
        sum += r - l + 1;
    }
    cout << sum << endl;
    return 0;
}

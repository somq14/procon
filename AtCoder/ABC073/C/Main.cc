#include <iostream>
#include <set>

using std::cin;
using std::cout;
using std::endl;

using std::set;

int main(void) {
    int n;
    cin >> n;

    set<int> s;
    for(int i = 0; i < n; i++){
        int v;
        cin >> v;
        if(s.count(v) == 0){
            s.insert(v);
        }else{
            s.erase(v);
        }
    }
    cout << s.size() << endl;

    return 0;
}

import strutils
import sequtils
import algorithm
import math
import queues
import tables
import logging
import future

const INF* = int(1e18 + 373)

when not defined(release):
  addHandler(newFileLogger(stderr, lvlAll, "[ $levelname ] "))

proc readInt1*(): int = stdin.readLine().strip().parseInt()
proc readInt2*(): (int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1])
proc readInt3*(): (int, int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1], v[2])

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#
proc eval(h, w, i, j: int; s: seq[string]): bool =
  i in 0..<h and j in 0..<w and s[i][j] == '#'

proc main() =
  let (h, w) = readInt2()
  var s = newSeq[string](h)
  for i in 0..<h:
    s[i] = stdin.readLine()

  var ans = true
  block outer:
    for i in 0..<h:
      for j in 0..<w:
        if s[i][j] == '#' and not (eval(h, w, i - 1, j, s) or eval(h, w, i + 1, j, s) or eval(h, w, i, j - 1, s) or eval(h, w, i, j + 1, s)):
          ans = false
          break outer

  echo if ans: "Yes" else: "No"

main()


import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import logging
import future

const INF* = int(1e5)

when not defined(release):
  addHandler(newFileLogger(stderr, lvlAll, "[ $levelname ] "))

proc readInt1*(): int = stdin.readLine().strip().parseInt()
proc readInt2*(): (int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1])
proc readInt3*(): (int, int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1], v[2])
proc readInt4*(): (int, int, int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1], v[2], v[3])

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#
proc transpose(a: seq2[int]): seq2[int] =
  result = newSeq2[int](a[0].len(), a.len())
  for i in 0..<a.len():
    for j in 0..<a[0].len():
      result[j][i] = a[i][j]

proc solve(H, W, h, w: int): seq2[int] =
  debug "H = ", H
  debug "W = ", W
  debug "h = ", h
  debug "w = ", w
  var s = newSeq[int](W + 1)
  s[0] = 0
  for i in 1..W:
    if i - w < 0:
      s[i] = INF
      continue
    s[i] = s[i - w] - 1

  debug "s = ", s

  var a = newSeq[int](W)
  for i in 0..<W:
    a[i] = s[i + 1] - s[i]

  debug "a = ", a 

  result = newSeq2[int](H, 0)
  for i in 0..<H:
    result[i] = a

proc main() =
  let (H, W, h, w) = readInt4()
  if H mod h == 0 and W mod w == 0:
    echo "No"
    return

  var ans: seq2[int] = nil
  if W mod w != 0:
    ans = solve(H, W, h, w)
  else:
    ans = solve(W, H, w, h).transpose()

  echo "Yes"
  for i in 0..<H:
    echo ans[i].map(it => $it).join(" ")

main()


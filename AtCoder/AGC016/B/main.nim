import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import logging
import future

const INF* = int(1e18 + 373)

when not defined(release):
  addHandler(newFileLogger(stderr, lvlAll, "[ $levelname ] "))

proc readInt1*(): int = stdin.readLine().strip().parseInt()
proc readInt2*(): (int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1])
proc readInt3*(): (int, int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1], v[2])
proc readInt4*(): (int, int, int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1], v[2], v[3])

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#

proc solve(n: int; a: seq[int]): bool =
  let a = a.sorted(cmp[int])
  let m = a[0]

  for i in 0..<n:
    if m != a[i] and m + 1 != a[i]:
      return false

  var cnt0 = 0
  var cnt1 = 0
  for i in 0..<n:
    if a[i] == m:
      cnt0 += 1
    if a[i] == m + 1:
      cnt1 += 1

  if cnt0 == n:
    if n <= m:
      return false
    if n == m + 1:
      return true
    return n >= m * 2

  let c = m + 1
  return cnt0 < c and cnt1 >= 2 * (c - cnt0)

proc main() =
  let n = readInt1()
  let a = readSeq().map(parseInt)
  echo if solve(n, a): "Yes" else: "No"

main()


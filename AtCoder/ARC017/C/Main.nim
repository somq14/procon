import strutils
import sequtils
import algorithm
import math

const INF = int(1e18 + 373)

proc readInt1() : int = stdin.readLine().strip().parseInt()
proc readInt2() : (int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1])
proc readInt3() : (int, int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1], v[2])

#------------------------------------------------------------------------------#

proc contains(s : int, i : int) : bool =
    return (s and (1 shl i)) != 0

proc main() : void =
    let (n, x) = readInt2()

    var w = newSeq[int](n)
    for i in 0..<n:
        w[i] = readInt1()

    let n1 = int(n / 2)
    var ws1 = newSeq[int](0)
    for s in 0..<(1 shl n1):
        var sum = 0
        for i in 0..<n1:
            if s.contains(i):
                sum += w[i]
        ws1.add(sum)
    ws1.sort(system.cmp[int])

    let n2 = n - n1
    var ws2 = newSeq[int](0)
    for s in 0..<(1 shl n2):
        var sum = 0
        for i in 0..<n2:
            if s.contains(i):
                sum += w[n1 + i]
        ws2.add(sum)
    ws2.sort(system.cmp[int])

    var ans = 0
    for w1 in ws1:
        if w1 > x:
            continue
        let rest = x - w1
        ans += lowerBound(ws2, rest + 1) - lowerBound(ws2, rest)

    echo ans


main()

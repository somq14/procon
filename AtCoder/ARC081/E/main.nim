import strutils
import sequtils
import algorithm
import math
import queues
import tables
import logging
import future

const INF* = int(1e18 + 373)

when not defined(release):
  addHandler(newFileLogger(stderr, lvlAll, "[ $levelname ] "))

proc readInt1*(): int = stdin.readLine().strip().parseInt()
proc readInt2*(): (int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1])
proc readInt3*(): (int, int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1], v[2])

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#
proc toInd(c: char): int = c.ord() - 'a'.ord()

proc main() =
  let a = stdin.readLine()
  let n = a.len()
  var next = newSeq2[int](n, 'z'.ord() - 'a'.ord() + 1)
  for i in 0..<n:
    next[i].fill(n)

  next[n - 1][a[n - 1].toInd()] = n - 1
  for i in countdown(n - 2, 0):
    next[i] = next[i + 1]
    next[i][a[i].toInd()] = i

  var dp = newSeq[int](n + 1)
  var dpNext = newSeq[int](n + 1)
  var dpChar= newSeq[char](n + 1)
  dp[n] = 1
  dpNext[n] = n
  dpChar[n] = 'a'

  for i in countdown(n - 1, 0):
    var minNext = -1
    var minLeng = INF
    var minChar = '!'

    for c in 'a'..'z':
      if next[i][c.toInd()] >= n:
        minLeng = 1
        minNext = n
        minChar = c
        break

      let leng = 1 + dp[next[i][c.toInd()] + 1]
      if leng < minLeng:
        minLeng = leng
        minNext = next[i][c.toInd()]
        minChar = c

    dp[i] = minLeng
    dpNext[i] = minNext
    dpChar[i] = minChar

  var ans = newSeq[char](0)

  var i = 0
  while i < n:
    ans.add(dpChar[i])
    i = dpNext[i] + 1

  echo ans.map(it => $it).join("")

main()


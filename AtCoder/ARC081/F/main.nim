import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import logging
import future

const INF* = int(1e6)

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

proc readInt1*(): int = readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#
proc dump(a: seq2[int]) =
  let h = a.len()
  for i in 0..<h:
    echo a[i].map(it => $it).join(" ")


# stack
proc push[T](a: var seq[T]; e: T) = a.add(e)
proc top[T](a: seq[T]): T = a[a.len() - 1]

proc maxRectangle(a: seq[int]): int =
  var a = a
  a.add(0)

  let n = a.len()

  type Rect = tuple [ val, ind : int ]
  var stack = newSeq[Rect](0)
  var ans = 0
  for i in 0..<n:
    if stack.len() == 0:
      stack.push((a[i], i))
    elif a[i] > stack.top().val:
      stack.push((a[i], i))
    elif a[i] < stack.top().val:
      var p = -1
      while stack.len() > 0 and a[i] <= stack.top().val:
        let e = stack.pop()
        p = e.ind
        ans = max(ans, (e.val + 1) * (i - e.ind + 1))
      stack.push((a[i], p))

  return ans

proc main() =
  let (h, w) = readInt2()
  var s = newSeq2[int](h, 0)
  for i in 0..<h:
    s[i] = stdin.readLine().map(it => (if it == '#': 1 else: 0))

  var a = newSeq2[int](h - 1, w - 1)
  for i in 0..<h - 1:
    for j in 0..<w - 1:
      a[i][j] = (s[i][j] + s[i + 1][j] + s[i][j + 1] + s[i + 1][j + 1]) mod 2

  var b = newSeq2[int](h - 1, w - 1)
  for j in 0..<w - 1:
    b[0][j] = if a[0][j] == 1: 0 else: 1
  for i in 1..<h - 1:
    for j in 0..<w - 1:
      b[i][j] = if a[i][j] == 1: 0 else: b[i - 1][j] + 1

  var ans = 0
  for i in 0..<h - 1:
    ans = max(ans, b[i].maxRectangle())
  echo max(ans, w, h)

main()


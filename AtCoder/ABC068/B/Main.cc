#include <algorithm>
#include <complex>
#include <iomanip>
#include <iostream>
#include <queue>
#include <set>
#include <string>
#include <vector>

using std::cin;
using std::cout;
using std::endl;

using std::string;
using std::to_string;
using std::vector;
using std::set;
using std::queue;
using std::priority_queue;

using std::min;
using std::max;
using std::sort;
using std::abs;

int eval(int x) {
    int cnt = 0;
    while (x % 2 == 0) {
        cnt++;
        x /= 2;
    }
    return cnt;
}

int main(void) {
    int n;
    cin >> n;

    int ans = 1;
    for(int i = 1; i <= n; i++){
        if(eval(i) > eval(ans)){
            ans = i;
        }
    }
    cout << ans << endl;


    return 0;
}

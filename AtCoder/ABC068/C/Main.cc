#include <algorithm>
#include <complex>
#include <iomanip>
#include <iostream>
#include <queue>
#include <set>
#include <string>
#include <utility>
#include <vector>

using std::cin;
using std::cout;
using std::endl;

using std::string;
using std::to_string;
using std::vector;
using std::set;
using std::queue;
using std::priority_queue;

using std::min;
using std::max;
using std::sort;
using std::abs;
using std::upper_bound;
using std::lower_bound;
using std::binary_search;

int main(void) {
    int n, m;
    cin >> n >> m;

    vector<int> fst;
    vector<int> sec;
    for (int i = 0; i < m; i++) {
        int a, b;
        cin >> a >> b;
        if(a == 1){
            fst.push_back(b);
        }else if(b == n){
            sec.push_back(a);
        }
    }

    sort(fst.begin(), fst.end());

    bool ans = false;
    for(int s : sec){
        if(binary_search(fst.begin(), fst.end(), s)){
            ans = true;
        }
    }

    cout << (ans ? "POSSIBLE" : "IMPOSSIBLE") << endl;

    return 0;
}

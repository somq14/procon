#include <algorithm>
#include <complex>
#include <iomanip>
#include <iostream>
#include <queue>
#include <set>
#include <string>
#include <utility>
#include <vector>
#include <numeric>

using std::cin;
using std::cout;
using std::endl;

using std::string;
using std::to_string;
using std::vector;
using std::set;
using std::queue;
using std::priority_queue;

using std::min;
using std::max;
using std::sort;
using std::abs;
using std::upper_bound;
using std::lower_bound;
using std::binary_search;

using std::iota;

typedef unsigned long long ll;

int main(void) {
    ll k;
    cin >> k;

    ll n = 50;
    vector<ll> a(n);
    iota(a.begin(), a.end(), k / n);

    ll m = k % n;
    for(ll i = 0; i < m; i++){
        a[i] += n;
        a[i] -= (m - 1);
    }
    for(ll i = m; i < n; i++){
        a[i] -= m;
    }

    cout << n << endl;
    for(ll i = 0; i < n - 1; i++){
        cout << a[i] << " ";
    }
    cout << a[n-1] << endl;

    return 0;
}

import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import logging
import future

const INF* = int(1e18 + 373)

when not defined(release):
  addHandler(newFileLogger(stderr, lvlAll, "[ $levelname ] "))

proc readInt1*(): int = stdin.readLine().strip().parseInt()
proc readInt2*(): (int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1])
proc readInt3*(): (int, int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1], v[2])
proc readInt4*(): (int, int, int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1], v[2], v[3])

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#

proc main() =
  let n = readInt1()
  let p = readSeq().map(parseInt).map(it => it - 1)

  var x = newSeq[int](n)
  for i in 0..<n:
    x[p[i]] = i

  var a = newSeq[int](n)
  var b = newSeq[int](n)
  a[0] = x[0]
  b[0] = 0
  for i in 1..<n:
    let d = x[i] - x[i - 1]
    if d > 0:
      a[i] = a[i - 1] + d + 1
      b[i] = b[i - 1] - 1
    else:
      a[i] = a[i - 1] + 1
      b[i] = b[i - 1] + d - 1

  debug a
  debug b
  debug zip(a, b).map(it => it[0] + it[1])

  a = a.map(it => it + 1)
  let minB = b.min()
  b = b.map(it => it - minB + 1)

  echo a.map(it => $it).join(" ")
  echo b.map(it => $it).join(" ")

main()


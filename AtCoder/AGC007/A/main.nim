import strutils
import sequtils
import algorithm
import math
import queues
import tables
import logging
import future

const INF* = int(1e18 + 373)

when not defined(release):
  addHandler(newFileLogger(stderr, lvlAll, "[ $levelname ] "))

proc readInt1*(): int = stdin.readLine().strip().parseInt()
proc readInt2*(): (int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1])
proc readInt3*(): (int, int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1], v[2])

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#

proc main() =
  let (h, w) = readInt2()
  var a = newSeq[string](h)
  for i in 0..<h:
    a[i] = stdin.readLine()

  var y = 0
  var x = 0
  while true:
    a[y][x] = '.'
    if x < w - 1 and a[y][x + 1] == '#':
      x += 1
      continue
    if y < h - 1 and a[y + 1][x] == '#':
      y += 1
      continue
    break

  var ans = true
  for i in 0..<h:
    for j in 0..<w:
      if a[i][j] == '#':
        ans = false
        break

  echo if ans: "Possible" else: "Impossible"

main()


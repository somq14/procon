#include <algorithm>
#include <complex>
#include <iostream>
#include <queue>
#include <set>
#include <stack>
#include <string>
#include <vector>

using std::cin;
using std::cout;
using std::endl;
using std::cerr;

using std::string;
using std::to_string;
using std::vector;
using std::set;
using std::queue;
using std::stack;
using std::priority_queue;

using std::min;
using std::max;
using std::sort;
using std::abs;

typedef long long int ll;
const int MOD = 1e9 + 7;

int main() {
    string w;
    cin >> w;

    vector<int> a('z' + 1);
    for (size_t i = 0; i < w.length(); i++) {
        a[w[i]]++;
    }

    bool beautiful = true;
    for (int c : a) {
        if(c % 2 != 0){
            beautiful = false;
            break;
        }
    }

    cout << (beautiful ? "Yes" : "No") << endl;

    return 0;
}

#include <algorithm>
#include <complex>
#include <iostream>
#include <queue>
#include <set>
#include <stack>
#include <string>
#include <vector>

using std::cin;
using std::cout;
using std::endl;
using std::cerr;

using std::string;
using std::to_string;
using std::vector;
using std::set;
using std::queue;
using std::stack;
using std::priority_queue;

using std::min;
using std::max;
using std::sort;
using std::abs;

typedef long long int ll;
const int MOD = 1e9 + 7;

ll f(ll b, ll n) { return n < b ? n : f(b, n / b) + n % b; }

ll solve(ll n, ll s) {
    if (n < s) {
        return -1;
    }

    // keta = 1
    if (n == s) {
        return n + 1;
    }

    // keta > 2
    for (ll b = 2; b * b <= n; b++) {
        if (f(b, n) == s) {
            return b;
        }
    }

    // keta = 2
    ll ans = -1;
    for (ll p = 1; p * p < n; p++) {
        ll b = 1 + (n - s) / p;
        if (b >= 2 && f(b, n) == s) {
            ans = b;
        }
    }

    return ans;
}

int main() {
    ll n, s;
    cin >> n >> s;
    cout << solve(n, s) << endl;
    return 0;
}

#include <algorithm>
#include <complex>
#include <iostream>
#include <queue>
#include <set>
#include <stack>
#include <string>
#include <vector>

using std::cin;
using std::cout;
using std::endl;
using std::cerr;

using std::string;
using std::to_string;
using std::vector;
using std::set;
using std::queue;
using std::stack;
using std::priority_queue;

using std::min;
using std::max;
using std::sort;
using std::abs;

typedef long long int ll;
const int MOD = 1e9 + 7;

int main() {
    int n, a;
    cin >> n >> a;

    vector<int> x(n);
    for (int i = 0; i < n; i++) {
        cin >> x[i];
    }

    vector<vector<vector<ll>>> dp(
        n + 1, vector<vector<ll>>(n + 1, vector<ll>(2500 + 1)));

    for(int i = 0; i <= n; i++){
        dp[i][0][0] = 1;
    }

    for (int i = 1; i <= n; i++) {
        for (int j = 1; j <= n; j++) {
            for (int k = 1; k <= 2500; k++) {
                dp[i][j][k] = dp[i - 1][j][k];
                if (k - x[i - 1] >= 0) {
                    dp[i][j][k] += dp[i - 1][j - 1][k - x[i - 1]];
                }
            }
        }
    }

    ll ans = 0;
    for (int i = 1; i <= n; i++) {
        ans += dp[n][i][i * a];
    }
    cout << ans << endl;

    return 0;
}

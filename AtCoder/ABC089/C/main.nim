import strutils
import sequtils
import algorithm
import math
import queues
import tables

const INF* = int(1e18 + 373)

proc readInt1*(): int = stdin.readLine().strip().parseInt()
proc readInt2*(): (int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1])
proc readInt3*(): (int, int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1], v[2])
proc readInt*(): seq[int] = stdin.readLine().strip().split().map(parseInt)

#------------------------------------------------------------------------------#

proc main() =
  let n = readInt1()
  var s = newSeq[string](n)
  for i in 0..<n:
    s[i] = stdin.readLine().strip()

  var t = initTable[char, int]()
  for c in 'A'..'Z':
    t[c] = 0

  for i in 0..<n:
    t[s[i][0]] += 1

  let a = @[ t['M'], t['A'], t['R'], t['C'], t['H'] ]

  var ans = 0
  for i in 0..<a.len():
    for j in (i + 1)..<a.len():
      for k in (j + 1)..<a.len():
        ans += a[i] * a[j] * a[k]

  echo ans


main()


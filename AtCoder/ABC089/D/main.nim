import strutils
import sequtils
import algorithm
import math
import queues
import tables

const INF* = int(1e18 + 373)

proc readInt1*(): int = stdin.readLine().strip().parseInt()
proc readInt2*(): (int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1])
proc readInt3*(): (int, int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1], v[2])

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#

type Pos = tuple[y, x, i: int]

proc `<`(p1, p2: Pos): bool = p1.i < p2.i

proc distance(p1, p2: Pos): int = abs(p1.y - p2.y) + abs(p1.x - p2.x)

proc main() =
  let (h, w, d) = readInt3()
  var a = newSeq2[int](h, w)
  for i in 0..<h:
    a[i] = readSeq().map(parseInt)

  for i in 0..<h:
    for j in 0..<w:
      a[i][j] -= 1

  let q = readInt1()

  var ls = newSeq[int](q)
  var rs = newSeq[int](q)
  for i in 0..<q:
    (ls[i], rs[i]) = readInt2()
    ls[i] -= 1
    rs[i] -= 1

  var ps: seq[Pos] = @[]
  for i in 0..<h:
    for j in 0..<w:
      ps.add((i, j, a[i][j]))
  ps.sort(cmp[Pos])

  var dp = newSeq[int](h * w)
  dp.fill(0)
  for i in countdown((h * w - 1) - d, 0):
    dp[i] = dp[i + d] + distance(ps[i], ps[i + d])

  for k in 0..<q:
    let s = ls[k]
    let t = rs[k]
    echo dp[s] - dp[t]

main()


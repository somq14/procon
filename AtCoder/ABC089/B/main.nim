import strutils
import sequtils
import algorithm
import math
import queues
import tables

const INF* = int(1e18 + 373)

proc readInt1*(): int = stdin.readLine().strip().parseInt()
proc readInt2*(): (int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1])
proc readInt3*(): (int, int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1], v[2])
proc readInt*(): seq[int] = stdin.readLine().strip().split().map(parseInt)

#------------------------------------------------------------------------------#

proc main() =
  let n = readInt1()
  let a = stdin.readLine().strip().split()
  let ind = a.find("Y")
  echo if ind == -1: "Three" else: "Four"


main()


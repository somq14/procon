#include <algorithm>
#include <cctype>
#include <complex>
#include <iomanip>
#include <iostream>
#include <queue>
#include <set>
#include <string>
#include <vector>

using std::cin;
using std::cout;
using std::endl;

using std::string;
using std::to_string;
using std::vector;
using std::set;
using std::queue;
using std::priority_queue;

using std::min;
using std::max;
using std::sort;
using std::abs;

typedef long long ll;
const ll MOD = 1000000007;

int main(void) {
    ll n, m;
    cin >> n >> m;

    vector<ll> x(n);
    for(ll i = 0; i < n; i++){
        cin >> x[i];
    }

    vector<ll> y(m);
    for(ll i = 0; i < m; i++){
        cin >> y[i];
    }

    ll sumx = 0;
    for(ll i = 0; i < n; i++){
        sumx = sumx + ((2 * i - n + 1) * x[i]) % MOD;
        sumx %= MOD;
    }

    ll sumy = 0;
    for(ll i = 0; i < m; i++){
        sumy = sumy + ((2 * i - m + 1) * y[i]) % MOD;
        sumy %= MOD;
    }

    ll sum = (sumx * sumy) % MOD;

    cout << sum << endl;

    return 0;
}

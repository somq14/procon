#include <algorithm>
#include <complex>
#include <iostream>
#include <string>
#include <vector>

using std::cin;
using std::cout;
using std::endl;

using std::min;
using std::max;
using std::abs;

using std::string;
using std::to_string;

using std::sort;

using std::vector;

const long MOD = 1000000007;

int main(void) {
    int n;
    cin >> n;

    string s1, s2;
    cin >> s1;
    cin >> s2;


    int i = 0;
    int left_pat = 0;
    long comb = 1;
    while (i < n) {
        if (s1[i] == s2[i]) {
            int c;
            if (left_pat == 0) {
                c = 3;
            } else if (left_pat == 1) {
                c = 2;
            } else {
                c = 1;
            }
            comb = comb * c % MOD;
            left_pat = 1;
            i += 1;
        } else {
            int c;
            if (left_pat == 0) {
                c = 6;
            } else if (left_pat == 1) {
                c = 2;
            } else {
                c = 3;
            }
            comb = comb * c % MOD;
            left_pat = 2;
            i += 2;
        }
    }

    cout << comb << endl;

    return 0;
}

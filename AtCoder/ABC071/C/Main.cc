#include <algorithm>
#include <complex>
#include <iostream>
#include <string>
#include <vector>

using std::cin;
using std::cout;
using std::endl;

using std::min;
using std::max;
using std::abs;

using std::string;
using std::to_string;

using std::sort;

using std::vector;

int main(void) {
    int n;
    cin >> n;

    vector<int> a(n);
    for(int i = 0; i < n; i++){
        cin >> a[i];
    }

    sort(a.begin(), a.end());

    vector<int> b;
    for(int j = n - 1; j > 0; j--){
        if(a[j] == a[j-1]){
            b.push_back(a[j]);
            j--;
            if(b.size() >= 2){
                break;
            }
        }
    }

    if(b.size() < 2){
        cout << 0 << endl;
    }else{
        cout << (long)b[0] * (long)b[1] << endl;
    }


    return 0;
}

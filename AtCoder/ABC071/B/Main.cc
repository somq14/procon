#include <iostream>
#include <algorithm>
#include <vector>
#include <complex>
#include <string>

using std::cin;
using std::cout;
using std::endl;

using std::min;
using std::max;
using std::abs;

using std::string;
using std::to_string;

using std::vector;

int main(void) {
    vector<bool> t('z' + 1, false);
    string s;

    cin >> s;
    for(char c : s){
        t[c] = true;
    }

    char ans = '?';
    for(char c = 'a'; c <= 'z'; c++){
        if(!t[c]){
            ans = c;
            break;
        }
    }

    if(ans == '?'){
        cout << "None" << endl;
    }else{
        cout << ans << endl;
    }

    return 0;
}


#include <algorithm>
#include <cctype>
#include <complex>
#include <iomanip>
#include <iostream>
#include <queue>
#include <set>
#include <string>
#include <vector>

using std::cin;
using std::cout;
using std::endl;

using std::string;
using std::to_string;
using std::vector;
using std::set;
using std::queue;
using std::priority_queue;

using std::min;
using std::max;
using std::sort;
using std::abs;

int main() {
    int n;
    cin >> n;

    vector<int> a(1e5 + 1);

    for (int i = 0; i < n; i++) {
        int x;
        cin >> x;
        a[x - 1]++;
    }

    int cnt2 = 0;
    int cnt1 = 0;
    for (int i = 0; i <= 1e5; i++) {
        if (a[i] == 0) {
            continue;
        }
        if(a[i] % 2 == 0){
            cnt2++;
        }else{
            cnt1++;
        }
    }

    int cnt = cnt1 + cnt2 - (cnt2 % 2 == 0 ? 0 : 1);
    cout << cnt << endl;


    return 0;
}
/*
int main() {
    int n;
    cin >> n;

    vector<int> a(1e5 + 1);

    for (int i = 0; i < n; i++) {
        int x;
        cin >> x;
        a[x - 1]++;
    }

    int l = 0, r = 1e5;
    while(l < r){
        while(l < n && a[l] <= 1){
            l++;
        }
        while(r >= 0 && a[r] <= 1){
            r--;
        }
        if(l >= r){
            break;
        }
        a[l]--;
        a[r]--;
    }

    if(l == r && a[l] > 0){
        while(a[l] >= 3){
            a[l] -= 2;
        }
        if(a[l] == 2){
            a[l] = 0;
        }
    }

    int cnt = 0;
    for(int i = 0; i <= 1e5; i++){
        if(a[i] == 0){
            continue;
        }
        cnt++;
        // cout << i << " : " << a[i] << endl;
    }
    cout << cnt << endl;
    return 0;
}
*/

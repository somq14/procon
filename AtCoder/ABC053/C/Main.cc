#include <algorithm>
#include <cctype>
#include <complex>
#include <iomanip>
#include <iostream>
#include <queue>
#include <set>
#include <string>
#include <vector>

using std::cin;
using std::cout;
using std::endl;

using std::string;
using std::to_string;
using std::vector;
using std::set;
using std::queue;
using std::priority_queue;

using std::min;
using std::max;
using std::sort;
using std::abs;

int main() {
    long x;
    cin >> x;

    long cnt = 2 * (x / 11);
    if(x % 11 == 0){
        cnt += 0;
    }else if(x % 11 <= 6){
        cnt += 1;
    }else{
        cnt += 2;
    }

    cout << cnt << endl;

    return 0;
}

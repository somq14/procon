#include <algorithm>
#include <cctype>
#include <complex>
#include <iomanip>
#include <iostream>
#include <queue>
#include <set>
#include <string>
#include <vector>

using std::cin;
using std::cout;
using std::endl;

using std::string;
using std::to_string;
using std::vector;
using std::set;
using std::queue;
using std::priority_queue;

using std::min;
using std::max;
using std::sort;
using std::abs;

int main() {
    string s;
    cin >> s;

    int len = s.length();

    int ap = -1;
    for(int i = 0; i < len; i++){
        if(s[i] == 'A'){
            ap = i;
            break;
        }
    }

    int zp = -1;
    for(int i = len-1; i >= 0; i--){
        if(s[i] == 'Z'){
            zp = i;
            break;
        }
    }

    cout << (zp - ap + 1) << endl;

    return 0;
}

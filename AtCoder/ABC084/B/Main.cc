#include <algorithm>
#include <cmath>
#include <complex>
#include <iomanip>
#include <iostream>
#include <queue>
#include <set>
#include <stack>
#include <string>
#include <utility>
#include <vector>

using std::cin;
using std::cout;
using std::endl;
using std::cerr;

using std::string;
using std::to_string;
using std::vector;
using std::set;
using std::queue;
using std::stack;
using std::priority_queue;
using std::pair;
using std::make_pair;

using std::min;
using std::max;
using std::sort;
using std::abs;

using std::fixed;
using std::setprecision;
using std::setw;

typedef long long int ll;
const int MOD = 1e9 + 7;
const int INF = 1e9 + 314;

int main() {
    int a, b;
    cin >> a >> b;
    string s;
    cin >> s;

    bool ans = true;
    for (int i = 0; i < a; i++) {
        if (!('0' <= s[i] && s[i] <= '9')) {
            ans = false;
        }
    }

    if (!(s[a] == '-')) {
        ans = false;
    }

    for (int i = a + 1; i < a + b + 1; i++) {
        if (!('0' <= s[i] && s[i] <= '9')) {
            ans = false;
        }
    }

    cout << (ans ? "Yes" : "No") << endl;

    return 0;
}

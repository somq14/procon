#include <algorithm>
#include <cmath>
#include <complex>
#include <iomanip>
#include <iostream>
#include <queue>
#include <set>
#include <stack>
#include <string>
#include <utility>
#include <vector>

using std::cin;
using std::cout;
using std::endl;
using std::cerr;

using std::string;
using std::to_string;
using std::vector;
using std::set;
using std::queue;
using std::stack;
using std::priority_queue;
using std::pair;
using std::make_pair;

using std::min;
using std::max;
using std::sort;
using std::abs;

using std::fixed;
using std::setprecision;
using std::setw;

typedef long long int ll;
const int MOD = 1e9 + 7;
const int INF = 1e9 + 314;

bool is_prime(ll n) {
    if (n <= 1) {
        return false;
    }

    if (n == 2) {
        return true;
    }

    if (n % 2 == 0) {
        return false;
    }

    for (ll i = 3; i * i <= n; i += 2) {
        if (n % i == 0) {
            return false;
        }
    }
    return true;
}

int main() {
    vector<bool> p(1e5 + 1);
    for (int i = 0; i <= 1e5; i++) {
        p[i] = is_prime(i);
    }

    vector<bool> s(1e5 + 1);
    for (int i = 0; i <= 1e5; i++) {
        if (i % 2 == 0) {
            continue;
        }
        s[i] = is_prime(i) && is_prime((i + 1) / 2);
    }

    vector<int> a(1e5 + 1);
    a[0] = 0;
    for (int i = 1; i <= 1e5; i++) {
        a[i] = a[i - 1];
        if (s[i]) {
            a[i]++;
        }
    }

    int q;
    cin >> q;
    for (int i = 0; i < q; i++) {
        int l, r;
        cin >> l >> r;
        cout << a[r] - a[l - 1] << endl;
    }

    return 0;
}

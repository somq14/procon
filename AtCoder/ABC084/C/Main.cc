#include <algorithm>
#include <cmath>
#include <complex>
#include <iomanip>
#include <iostream>
#include <queue>
#include <set>
#include <stack>
#include <string>
#include <utility>
#include <vector>

using std::cin;
using std::cout;
using std::endl;
using std::cerr;

using std::string;
using std::to_string;
using std::vector;
using std::set;
using std::queue;
using std::stack;
using std::priority_queue;
using std::pair;
using std::make_pair;

using std::min;
using std::max;
using std::sort;
using std::abs;

using std::fixed;
using std::setprecision;
using std::setw;

typedef long long int ll;
const int MOD = 1e9 + 7;
const int INF = 1e9 + 314;

int main() {
    int n;
    cin >> n;

    vector<int> c(n - 1);
    vector<int> s(n - 1);
    vector<int> f(n - 1);
    for (int i = 0; i < n - 1; i++) {
        cin >> c[i] >> s[i] >> f[i];
    }

    for(int k = 0; k < n; k++){
        int t = 0;
        int i = k;
        while (i < n - 1) {
            if (t < s[i]) {
                t = s[i];
            }
            if (t % f[i] == 0) {
                t += c[i];
                i++;
            }else{
                t++;
            }
        }
        cout << t << endl;
    }


    return 0;
}

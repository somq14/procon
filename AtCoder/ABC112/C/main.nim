import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import logging
import future

const INF* = int(1e18 + 373)

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

proc readInt1*(): int = readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#
type P = tuple [ x, y, h: int ]

proc subr(y, x, h: int; ps: seq[P]): bool = 
  for i in 0..<ps.len():
    if ps[i].h != max(h - abs(ps[i].y - y) - abs(ps[i].x - x), 0):
      return false
  return true

proc main() =
  let n = readInt1()
  var ps = newSeq[P](n)
  for i in 0..<n:
    ps[i] = readInt3()

  for y in 0..100:
    for x in 0..100:
      var h = -1
      for i in 0..<ps.len():
        if ps[i].h != 0:
          h = ps[i].h + abs(ps[i].y - y) + abs(ps[i].x - x)
          break
      if subr(y, x, h, ps):
        echo x, " ", y, " ", h
        return


main()


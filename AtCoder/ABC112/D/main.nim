import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import logging
import future

const INF* = int(1e18 + 373)

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

proc readInt1*(): int = readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#
#------------------------------------------------------------------------------#
proc gcd(a, b: int): int =
  var a = a
  var b = b
  while b != 0:
    let tmp = a mod b
    a = b
    b = tmp
  return a

proc dividers(n: int): seq[int] =
  result = @[]

  for i in 1..n:
    if i * i > n:
      break
    if n mod i == 0:
      result.add(i)
      result.add(n div i)
      if i == n div i:
        discard result.pop()

  result.sort(cmp[int])


const MaxPrime = int(1e5 + 1)

proc calcPrimeTable(maxn: int): seq[bool] =
  result = newSeq[bool](maxn + 1)
  result.fill(true)
  result[0] = false
  result[1] = false

  for i in 2..maxn:
    if not result[i]:
      continue
    var j = i * 2
    while j <= maxn:
      result[j] = false
      j += i

let primeTable = calcPrimeTable(MaxPrime)

proc calcPrimeList(maxn: int): seq[int] =
  result = newSeq[int](0)
  for i in 0..maxn:
    if primeTable[i]:
      result.add(i)

let primeList = calcPrimeList(MaxPrime)

proc factorize1(n: int): seq[int] =
  result = newSeq[int](0)
  var m = n
  for p in primeList:
    if m < p:
      break
    while m mod p == 0:
      result.add(p)
      m = m div p
  if m != 1:
    result.add(m)

proc factorize2(n: int): seq[(int, int)] =
  result = newSeq[(int, int)](0)
  var m = n
  for p in primeList:
    if m < p:
      break
    var cnt = 0
    while m mod p == 0:
      cnt += 1
      m = m div p
    if cnt != 0:
      result.add((p, cnt))

  if m != 1:
    result.add((m, 1))

#------------------------------------------------------------------------------#

proc main() =
  let (n, m) = readInt2()
  let dd = m.dividers()
  var ans = 0
  for d in dd:
    let kk = m div d
    if kk >= n:
      ans = d
  echo ans


main()


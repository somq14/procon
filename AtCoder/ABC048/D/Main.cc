#include <algorithm>
#include <cctype>
#include <complex>
#include <iostream>
#include <queue>
#include <set>
#include <stack>
#include <string>
#include <vector>

using std::cin;
using std::cout;
using std::endl;
using std::cerr;

using std::string;
using std::to_string;
using std::vector;
using std::set;
using std::queue;
using std::stack;
using std::priority_queue;

using std::min;
using std::max;
using std::sort;
using std::abs;

typedef long long int ll;
const int MOD = 1e9 + 7;

int main() {
    string s;
    cin >> s;

    const int len = s.length();
    const char fst = s[0];
    const char lst = s[len-1];

    if(fst == lst){
        cout << (len % 2 == 0 ? "First" : "Second") << endl;
    }
    else{
        cout << (len % 2 != 0 ? "First" : "Second") << endl;
    }
    return 0;
}

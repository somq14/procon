#include <algorithm>
#include <cctype>
#include <complex>
#include <iostream>
#include <queue>
#include <set>
#include <stack>
#include <string>
#include <vector>

using std::cin;
using std::cout;
using std::endl;
using std::cerr;

using std::string;
using std::to_string;
using std::vector;
using std::set;
using std::queue;
using std::stack;
using std::priority_queue;

using std::min;
using std::max;
using std::sort;
using std::abs;

typedef long long int ll;
const int MOD = 1e9 + 7;

int main() {
    ll n, x;
    cin >> n >> x;

    vector<ll> a(n);
    for (ll i = 0; i < n; i++) {
        cin >> a[i];
    }

    ll cnt = 0;
    for (ll i = 0; i < n - 1; i++) {
        if (a[i] + a[i + 1] <= x) {
            continue;
        }

        ll sub = a[i] + a[i + 1] - x;
        cnt += sub;

        a[i + 1] -= min(sub, a[i + 1]);
        if (a[i] + a[i + 1] <= x) {
            continue;
        }

        sub = a[i] + a[i + 1] - x;
        a[i] -= sub;
    }

    cout << cnt << endl;

    return 0;
}

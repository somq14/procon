#include <algorithm>
#include <cctype>
#include <complex>
#include <iomanip>
#include <iostream>
#include <queue>
#include <set>
#include <string>
#include <vector>

using std::cin;
using std::cout;
using std::endl;

using std::string;
using std::to_string;
using std::vector;
using std::set;
using std::queue;
using std::priority_queue;

using std::min;
using std::max;
using std::sort;
using std::abs;

const int INF = 1e9;

int keta(long n){
    int cnt = 0;
    while(n > 0){
        cnt++;
        n /= 10;
    }
    return cnt;
}

int main(void) {
    long n;
    cin >> n;

    int ans = INF;
    for(long i = 1; i * i <= n; i++){
        if(n % i != 0){
            continue;
        }
        long a = i;
        long b = n / i;
        long c = max(a, b);
        ans = min(ans, keta(c));
    }

    cout << ans << endl;

    return 0;
}

import strutils
import sequtils
import algorithm
import math
import queues

const INF = int(1e18 + 373)

proc readInt1() : int = stdin.readLine().strip().parseInt()
proc readInt2() : (int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1])
proc readInt3() : (int, int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1], v[2])

#------------------------------------------------------------------------------#
type Pos = tuple[y: int, x: int]

proc search(r, c: int, g: seq[string], x: char): Pos =
    for i in 0..<r:
        for j in 0..<c:
            if g[i][j] == x:
                return (i, j)
    return (-1, -1)

proc bfs(r, c: int, g: seq[string], k: int, s: Pos): seq[seq[seq[int]]] =
    const dy4 = @[1, -1, 0, 0]
    const dx4 = @[0, 0, 1, -1]

    var dp = newSeqWith(r, newSeqWith(c, newSeq[int](k + 1)))
    for y in 0..<r:
        for x in 0..<c:
            for i in 0..k:
                dp[y][x][i] = INF
    dp[s.y][s.x][0] = 0

    var q = initQueue[Pos]()
    q.enqueue(s)

    while q.len() > 0:
        let p = q.dequeue()

        for d in zip(dy4, dx4):
            let pp : Pos = (p.y + d[0], p.x + d[1])

            if pp.y notin 0..<r or pp.x notin 0..<c:
                continue

            if g[pp.y][pp.x] == 'T':
                continue

            var update = false
            let off = if g[pp.y][pp.x] == 'E': 1 else: 0
            for i in 0..(k - off):
                if dp[p.y][p.x][i] + 1 < dp[pp.y][pp.x][i + off]:
                    dp[pp.y][pp.x][i + off] = dp[p.y][p.x][i] + 1
                    update = true
            if update:
                q.enqueue(pp)

    for y in 0..<r:
        for x in 0..<c:
            for i in 1..k:
                dp[y][x][i] = min(dp[y][x][i], dp[y][x][i-1])
    return dp

proc solve(r, c: int, g: seq[string], k: int): int =
    var g = g

    let spos : Pos = search(r, c, g, 'S')
    let cpos : Pos = search(r, c, g, 'C')
    let gpos : Pos = search(r, c, g, 'G')

    g[spos.y][spos.x] = '.'
    g[cpos.y][cpos.x] = '.'
    g[gpos.y][gpos.x] = '.'

    let sDp = bfs(r, c, g, k, spos)
    let cDp = bfs(r, c, g, k, cpos)
    let gDp = bfs(r, c, g, k, gpos)

    result = INF
    for y in 0..<r:
        for x in 0..<c:
            if g[y][x] == 'T':
                continue
            for k0 in 0..k:
                for k1 in 0..k:
                    if k0 + k1 > k:
                        continue
                    let k2 = k - k0 - k1
                    if g[y][x] == '.':
                        let v = sDp[y][x][k0] + 2 * cDp[y][x][k1] + gDp[y][x][k2]
                        result = min(result, v)
                    elif g[y][x] == 'E':
                        let v = sDp[y][x][k0] + 2 * cDp[y][x][min(k1+1,k)] + gDp[y][x][min(k2+1,k)]
                        result = min(result, v)


proc main() =
    let (r, c, k) = readInt3()

    var g = newSeq[string](r)
    for i in 0..<r:
        g[i] = stdin.readLine()

    let ans = solve(r, c, g, k)
    if ans >= INF:
        echo -1
    else:
        echo ans

main()

import strutils
import sequtils
import algorithm
import math
import queues

const INF = int(1e18 + 373)
const MOD = int(1e9 + 7)

proc readInt1() : int = stdin.readLine().strip().parseInt()
proc readInt2() : (int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1])
proc readInt3() : (int, int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1], v[2])

#------------------------------------------------------------------------------#
proc eval(n, k: int, a, d: seq[int], x: int): bool =
    var cnt = 0
    for i in 0..<n:
        if cnt >= k:
            return true
        if a[i] > x:
            continue
        cnt += 1 + (x - a[i]) div d[i]
    return cnt >= k

proc solve(n, k: int, a, d: seq[int]): int =
    # (lb, ub]
    var ub = 1e15.int()
    var lb = 0
    while ub - lb > 1:
        let mid = (ub + lb) div 2
        if eval(n, k, a, d, mid):
            ub = mid
        else:
            lb = mid

    let x = ub

    result = 0
    var cnt = 0
    for i in 0..<n:
        if a[i] > x:
            continue
        let j = 1 + (x - a[i]) div d[i]
        cnt += j
        result += a[i] * j + d[i] * (j * (j - 1) div 2)
    result -= (cnt - k) * x

proc main() =
    let k = readInt1()
    let n = readInt1()

    var a = newSeq[int](n)
    var d = newSeq[int](n)
    for i in 0..<n:
        (a[i], d[i]) = readInt2()

    echo solve(n, k, a, d)

main()

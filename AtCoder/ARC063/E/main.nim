import strutils
import sequtils
import algorithm
import math
import queues
import tables
import logging
import future

const INF* = int(1e18 + 373)

when not defined(release):
  addHandler(newFileLogger(stderr, lvlAll, "[ $levelname ] "))

proc readInt1*(): int = stdin.readLine().strip().parseInt()
proc readInt2*(): (int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1])
proc readInt3*(): (int, int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1], v[2])

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#
const INVALID = int(0x373373373373373)

proc dfs1(g: seq2[int]; a: seq[int]; v, p: int; ss, tt: var seq[int]): bool =
  ss[v] = -INF
  tt[v] = +INF
  if a[v] != INVALID:
    ss[v] = a[v]
    tt[v] = a[v]

  for u in g[v]:
    if u == p:
      continue
    if not dfs1(g, a, u, v, ss, tt):
      return false

    ss[v] = max(ss[v], ss[u] - 1)
    tt[v] = min(tt[v], tt[u] + 1)

  return ss[v] <= tt[v]

proc dfs2(g: seq2[int]; a: var seq[int]; v, p: int; ss, tt: seq[int]): bool =
  if a[v] == INVALID:
    for candi in @[ a[p] + 1, a[p] - 1 ]:
      if candi in ss[v]..tt[v]:
        a[v] = candi
        break

  if a[v] == INVALID:
    return false

  for u in g[v]:
    if u == p:
      continue

    if not dfs2(g, a, u, v, ss, tt):
      return false

  return true

proc main() =
  let n = readInt1()

  var g = newSeq2[int](n, 0)
  for i in 0..<n - 1:
    let (a, b) = readInt2()
    g[a - 1].add(b - 1)
    g[b - 1].add(a - 1)

  let k = readInt1()

  var a = newSeq[int](n)
  a.fill(INVALID)

  for i in 0..<k:
    let (v, p) = readInt2()
    a[v - 1] = p

  var r = -1
  for i in 0..<n:
    if a[i] != INVALID:
      r = i
      break

  var ss = newSeq[int](n)
  var tt = newSeq[int](n)
  if not dfs1(g, a, r, -1, ss, tt):
    echo "No"
    return

  if not dfs2(g, a, r, -1, ss, tt):
    echo "No"
    return

  echo "Yes"
  for i in 0..<n:
    echo a[i]

main()


import strutils
import sequtils
import algorithm
import math
import queues
import sets

const INF = int(1e18 + 373)
const MOD = int(1e9 + 7)

proc readInt1(): int = stdin.readLine().strip().parseInt()
proc readInt2(): (int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1])
proc readInt3(): (int, int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1], v[2])
proc readInt(): seq[int] = stdin.readLine().strip().split().map(parseInt)

#------------------------------------------------------------------------------#

proc main() =
    let (n, k) = readInt2()
    let x = stdin.readLine().strip()

    var a = newSeq[int](ord('z') - ord('a') + 1)
    for i in 0..<(k-1):
        a[ord(x[i]) - ord('a')] += 1

    var ans = false

    var s = initSet[seq[int]]()
    var q = initQueue[seq[int]]()
    for i in (k-1)..<n:
        a[ord(x[i]) - ord('a')] += 1
        if i != k - 1:
            a[ord(x[i - k]) - ord('a')] -= 1
        if s.contains(a):
            ans = true
            break
        q.enqueue(a)
        while q.len() > k - 1:
            s.incl(q.dequeue())

    echo if ans: "YES" else: "NO"

main()


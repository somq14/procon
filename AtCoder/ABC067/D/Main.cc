#include <algorithm>
#include <complex>
#include <iomanip>
#include <iostream>
#include <queue>
#include <set>
#include <stack>
#include <string>
#include <vector>

using std::cin;
using std::cout;
using std::endl;

using std::string;
using std::to_string;
using std::vector;
using std::set;
using std::queue;
using std::priority_queue;
using std::stack;

using std::min;
using std::max;
using std::sort;
using std::abs;

const int BLACK = 1;
const int WHITE = -1;
const int NONE = 0;

bool dfs(const vector<vector<int>>& g, int s, int t, vector<bool>& visited,
         vector<int>& route) {
    if (s == t) {
        route.push_back(s);
        return true;
    }

    visited[s] = true;
    for (int u : g[s]) {
        if (visited[u]) {
            continue;
        }
        if (dfs(g, u, t, visited, route)) {
            route.push_back(s);
            return true;
        }
    }
    visited[s] = false;

    return false;
}

void bfs(int n, const vector<vector<int>>& g, vector<int>& mark, int color) {
    queue<int> q;
    for (int i = 0; i < n; i++) {
        if (mark[i] == color) {
            q.push(i);
            mark[i] = NONE;
        }
    }
    while (!q.empty()) {
        const int v = q.front();
        q.pop();

        if (mark[v] != NONE) {
            continue;
        }
        mark[v] = color;

        for (int u : g[v]) {
            if (mark[u] != NONE) {
                continue;
            }
            q.push(u);
        }
    }
}

int main(void) {
    int n;
    cin >> n;

    vector<vector<int>> g(n);
    for (int i = 0; i < n - 1; i++) {
        int a, b;
        cin >> a >> b;
        g[a - 1].push_back(b - 1);
        g[b - 1].push_back(a - 1);
    }

    vector<int> route;
    vector<bool> visited(n);
    dfs(g, 0, n - 1, visited, route);

    const int route_size = route.size();
    vector<int> mark(n, NONE);
    for (int i = 0; i < route_size; i++) {
        mark[route[i]] = BLACK;
    }
    for (int i = 0; i < route_size / 2; i++) {
        mark[route[i]] = WHITE;
    }

    bfs(n, g, mark, BLACK);
    bfs(n, g, mark, WHITE);

    int bcnt = count(mark.begin(), mark.end(), BLACK);
    int wcnt = count(mark.begin(), mark.end(), WHITE);
    /*
    for(int i = 0; i < n; i++){
        cout << mark[i] << " ";
    }
    cout << endl;
    for(int i = 0; i < route.size(); i++){
        cout << route[i] << " ";
    }
    cout << endl;
    cout << bcnt << " " << wcnt << endl;
    */
    cout << (bcnt > wcnt ? "Fennec" : "Snuke") << endl;

    return 0;
}

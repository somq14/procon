import strutils
import sequtils
import algorithm
import math
import queues
import tables
import logging

const INF* = int(1e18 + 373)

when not defined(release):
  addHandler(newFileLogger(stderr, lvlAll, "[ $levelname ] "))

proc readInt1*(): int = stdin.readLine().strip().parseInt()
proc readInt2*(): (int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1])
proc readInt3*(): (int, int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1], v[2])

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#

proc countCountinuous[T](a: seq[T]; s, t: Natural; e: T): Natural =
  result = 0
  var i = s
  while i < t and a[i] == e:
    result += 1
    i += 1

proc solve(n, l: int; xs: seq[int]; ds: seq[char]): int =
  var xs = xs

  result = 0

  for i in countdown(n - 1, 1):
    if ds[i] == 'R' and ds[i - 1] == 'R':
      result += (xs[i] - 1) - xs[i - 1]
      xs[i - 1] = xs[i] - 1

  for i in countup(0, n - 2):
    if ds[i] == 'L' and ds[i + 1] == 'L':
      result += (xs[i + 1] - 1) - xs[i]
      xs[i + 1] = xs[i] + 1

  var i = 0
  while i < n:
    let rind = i
    let rcnt = countCountinuous(ds, i, n, 'R')
    i += rcnt

    let lind = i
    let lcnt = countCountinuous(ds, i, n, 'L')
    i += lcnt

    if rcnt == 0:
      result += lcnt * (xs[lind] - 1)
    elif lcnt == 0:
      result += rcnt * (l - (xs[rind] + rcnt - 1))
    else:
      let dist = xs[lind] - (xs[rind] + rcnt)
      result += max(rcnt, lcnt) * dist

proc main() =
  let (n, l) = readInt2()

  var xs = newSeq[int](n)
  var ds = newSeq[char](n)
  for i in 0..<n:
    let v = readSeq();
    xs[i] = parseInt(v[0])
    ds[i] = v[1][0]

  echo solve(n, l, xs, ds)

main()


import strutils
import sequtils
import algorithm
import math
import queues
import tables
import logging
import future

const INF* = int(1e18 + 373)

when not defined(release):
  addHandler(newFileLogger(stderr, lvlAll, "[ $levelname ] "))

proc readInt1*(): int = stdin.readLine().strip().parseInt()
proc readInt2*(): (int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1])
proc readInt3*(): (int, int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1], v[2])

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#

proc main() =
  let n = readInt1()
  let blue = readSeq(n)

  let m = readInt1()
  let red = readSeq(m)

  var t = initTable[string, int]()
  for i in 0..<n:
    if blue[i] notin t:
      t[blue[i]] = 0
    t[blue[i]] += 1

  for i in 0..<m:
    if red[i] notin t:
      t[red[i]] = 0
    t[red[i]] -= 1

  var ans = -INF
  for i in 0..<n:
    ans = max(ans, t[blue[i]])
  for i in 0..<m:
    ans = max(ans, t[red[i]])
  echo max(ans, 0)

main()


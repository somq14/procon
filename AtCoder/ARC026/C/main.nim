import strutils
import sequtils
import algorithm
import math
import queues

const INF = int(1e18 + 373)
const MOD = int(1e9 + 7)

proc readInt1(): int = stdin.readLine().strip().parseInt()
proc readInt2(): (int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1])
proc readInt3(): (int, int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1], v[2])
proc readInt(): seq[int] = stdin.readLine().strip().split().map(parseInt)

#------------------------------------------------------------------------------#

# FIXME: Use object
type PriorityQueue[T] = tuple[a: seq[T], n: Natural, cmp: proc(a, b: T): bool]

proc initPriorityQueue[T](cmp: proc(a, b: T): bool): PriorityQueue[T] =
    (newSeq[T](1), Natural(0), cmp)

proc len[T](this: PriorityQueue[T]): Natural = this.n

proc enqueue[T](this: var PriorityQueue[T], e: T) =
    this.n += 1
    this.a.add(e)
    var i = this.a.len() - 1
    while i > 1 and this.cmp(this.a[i], this.a[i div 2]):
        swap(this.a[i], this.a[i div 2])
        i = i div 2

proc dequeue[T](this: var PriorityQueue[T]): T =
    this.n -= 1
    result = this.a[1]
    this.a[1] = this.a.pop()
    var i = 1
    while i < this.a.len():
        var j = i
        if i * 2 < this.a.len() and this.cmp(this.a[i * 2], this.a[j]):
            j = i * 2
        if i * 2 + 1 < this.a.len() and this.cmp(this.a[i * 2 + 1], this.a[j]):
            j = i * 2 + 1
        if i == j:
            break
        swap(this.a[i], this.a[j])
        i = j

# FIXME: throw Exception when this is empty
proc front[T](this: PriorityQueue[T]): T = this.a[1]

#------------------------------------------------------------------------------#

proc less[T](a, b: T): bool = a < b

type EventType = enum Begin, End
type Event = tuple[ p, i: int,  t: EventType]
proc `<`(e1, e2: Event): bool = e1.p < e2.p

type Value = tuple[ c, i: int ]
proc `<`(v1, v2: Value): bool = v1.c < v2.c

proc solve(n, m: int, ls, rs, cs: seq[int]): int =
    var act = newSeq[bool](n)
    act.fill(false)

    var eq = initPriorityQueue[Event](less)
    for i in 0..<n:
        eq.enqueue((ls[i], i, Begin))
        eq.enqueue((rs[i] + 1, i, End))

    var vq = initPriorityQueue[Value](less)
    var dp = newSeq[int](m)
    for i in 0..<m:
        while eq.len() > 0 and eq.front().p <= i:
            let e = eq.dequeue()
            case e.t:
            of Begin:
                act[e.i] = true
                if ls[e.i] == 0:
                    vq.enqueue((cs[e.i], e.i))
                else:
                    vq.enqueue((dp[ls[e.i] - 1] + cs[e.i], e.i))
            of End:
                act[e.i] = false

        while vq.len() > 0 and not act[vq.front().i]:
            discard vq.dequeue()
        dp[i] = vq.front().c

    return dp[m - 1]

proc main() =
    let (n, m) = readInt2()

    var ls = newSeq[int](n)
    var rs = newSeq[int](n)
    var cs = newSeq[int](n)
    for i in 0..<n:
        (ls[i], rs[i], cs[i]) = readInt3()
        rs[i] -= 1

    echo solve(n, m, ls, rs, cs)


main()


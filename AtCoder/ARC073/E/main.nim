import strutils
import sequtils
import algorithm
import math
import queues
import tables
import logging
import future

const INF* = int(1e18 + 373)

when not defined(release):
  addHandler(newFileLogger(stderr, lvlAll, "[ $levelname ] "))

proc readInt1*(): int = stdin.readLine().strip().parseInt()
proc readInt2*(): (int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1])
proc readInt3*(): (int, int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1], v[2])

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#
type Node[T] = ref object
  lNode: Node[T]
  rNode: Node[T]
  value: T
  height: int

proc initNode[T](x: T): Node[T] =
  result.new()
  result.lNode = nil
  result.rNode = nil
  result.value = x
  result.height = 1

proc contains[T](this: Node[T]; x: T): bool =
  if this == nil:
    return false

  if x < this.value:
    return this.lNode.contains(x)
  if x > this.value:
    return this.rNode.contains(x)

  return true

# return positive value when right partial tree is taller than left one.
proc balance[T](this: Node[T]): int =
  if this == nil:
    return 0
  let lh = if this.lNode == nil: 0 else: this.lNode.height
  let rh = if this.rNode == nil: 0 else: this.rNode.height
  return rh - lh

proc updateHeight[T](this: var Node[T]) =
  if this == nil:
    return
  let lh = if this.lNode == nil: 0 else: this.lNode.height
  let rh = if this.rNode == nil: 0 else: this.rNode.height
  this.height = max(lh, rh) + 1

# Right Rotatation
#
# Before
#       [this]
#         |
#     |-------|
#   [root]   [t3]
#     |
#   |---|
# [t1] [t2]
#
# After
#       [root]
#         |
#     |-------|
#   [t1]    [this]
#             |
#           |---|
#         [t2] [t3]
proc rotateRight[T](this: var Node[T]): Node[T] =
  var root = this.lNode
  let t1 = root.lNode
  let t2 = root.rNode
  let t3 = this.rNode
  root.lNode = t1
  root.rNode = this
  this.lNode = t2
  this.rNode = t3

  this.updateHeight()
  root.updateHeight()

  return root

# Left Rotatation
#
# Before
#       [this]
#         |
#     |-------|
#   [t3]    [root]
#             |
#           |---|
#         [t2] [t1]
#
# After
#       [root]
#         |
#     |-------|
#  [this]    [t1]
#     |
#   |---|
# [t3] [t2]
proc rotateLeft[T](this: var Node[T]): Node[T] =
  var root = this.rNode
  let t1 = root.rNode
  let t2 = root.lNode
  let t3 = this.lNode
  root.lNode = this
  this.lNode = t3
  this.rNode = t2
  root.rNode = t1

  this.updateHeight()
  root.updateHeight()

  return root

proc rebalance[T](this: var Node[T]): Node[T] =
  if this.balance() == -2:
    if this.lNode.balance() == 1:
      this.lNode = this.lNode.rotateLeft()
    return this.rotateRight()

  if this.balance() == +2:
    if this.rNode.balance() == -1:
      this.rNode = this.rNode.rotateRight()
    return this.rotateLeft()

  return this

proc add[T](this: var Node[T]; x: T): Node[T] =
  if this == nil:
    return initNode(x)

  if x < this.value:
    this.lNode = this.lNode.add(x)
    this.updateHeight()
    return this.rebalance()

  if x > this.value:
    this.rNode = this.rNode.add(x)
    this.updateHeight()
    return this.rebalance()

  return this


proc max[T](this: Node[T]): T =
  assert this != nil
  var p = this
  while p.rNode != nil:
    p = p.rNode
  return p.value

proc min[T](this: Node[T]): T =
  assert this != nil
  var p = this
  while p.lNode != nil:
    p = p.lNode
  return p.value

proc remove[T](this: var Node[T]; x: T): Node[T] =
  if this == nil:
    return nil

  if x == this.value:
    if this.lNode != nil:
      this.value = max(this.lNode)
      this.lNode = this.lNode.remove(this.value)
      this.updateHeight()
      return this.rebalance()

    if this.rNode != nil:
      this.value = min(this.rNode)
      this.rNode = this.rNode.remove(this.value)
      this.updateHeight()
      return this.rebalance()

    return nil

  if x < this.value:
    this.lNode = this.lNode.remove(x)
    this.updateHeight()
    return this.rebalance()

  if x > this.value:
    this.rNode = this.rNode.remove(x)
    this.updateHeight()
    return this.rebalance()

  return this


proc `$`[T](this: Node[T]): string =
  if this.lNode == nil and this.rNode == nil:
    return $this.value

  let ls = if this.lNode == nil: "" else: $this.lNode
  let rs = if this.rNode == nil: "" else: $this.rNode
  return $this.value & "(" & ls & ", " & rs & ")"

#------------------------------------------------------------------------------#
type BalancedTree[T] = object
  n: int
  root: Node[T]

proc initBalancedTree[T](): BalancedTree[T] =
  result.n = 0
  result.root = nil

proc len[T](this: BalancedTree[T]): int = this.n

proc contains[T](this: BalancedTree[T]; x: T): bool =
  this.root.contains(x)

proc add[T](this: var BalancedTree[T]; x: T): bool =
  if this.contains(x):
    return false

  this.root = this.root.add(x)
  this.root.updateHeight()
  this.n += 1
  return true

proc remove[T](this: var BalancedTree[T]; x: T): bool =
  if not this.contains(x):
    return false

  this.root = this.root.remove(x)
  this.root.updateHeight()
  this.n -= 1
  return true

proc min[T](this: BalancedTree[T]): T = min(this.root)
proc max[T](this: BalancedTree[T]): T = max(this.root)

proc `$`[T](this: BalancedTree[T]): string =
  if this.root == nil:
    return "[]"
  return "[" & $this.root & "]"

#------------------------------------------------------------------------------#

# import strutils
# 
# var s = initBalancedTree[int]()
# while true:
#   let cmd = stdin.readLine().split()
#   if cmd[0] == "+":
#     discard s.add(cmd[1].parseInt())
#   if cmd[0] == "-":
#     discard s.remove(cmd[1].parseInt())
#   if cmd[0] == "q":
#     break
#   echo $s
# 

type Pair = tuple [x, y: int]

type Recode = tuple [key, value: int]

proc `<`(r1, r2: Recode): bool = r1.key < r2.key
proc `==`(r1, r2: Recode): bool = r1.key == r2.key

proc search[T](this: Node[T]; x: T): T =
  if x < this.value:
    return this.lNode.search(x)
  if x > this.value:
    return this.rNode.search(x)
  return this.value

proc pattern1(n: int; ps: seq[Pair]; gmin, gmax: int): int =
  let bmin = gmin
  let bmax = gmax

  var s = initBalancedTree[Recode]()
  for i in 0..<n:
    if (ps[i].x, 0) in s:
      let (_, cnt) = s.root.search((ps[i].x, 0))
      discard s.remove((ps[i].x, 0))
      discard s.add((ps[i].x, cnt + 1))
    else:
      discard s.add((ps[i].x, 1))

  var ans = s.max().key - s.min().key
  for i in 0..<n:
    if (ps[i].x, 0) in s:
      let (_, cnt) = s.root.search((ps[i].x, 0))
      discard s.remove((ps[i].x, 0))
      if cnt - 1 >= 1:
        discard s.add((ps[i].x, cnt - 1))

    if (ps[i].y, 0) in s:
      let (_, cnt) = s.root.search((ps[i].y, 0))
      discard s.remove((ps[i].y, 0))
      discard s.add((ps[i].y, cnt + 1))
    else:
      discard s.add((ps[i].y, 1))

    ans = min(ans, s.max().key - s.min().key)

  return ans * (gmax - gmin)

proc pattern2(n: int; ps: seq[Pair]; gmin, gmax: int): int =
  var rmin = ps[0].x
  var rmax = ps[0].x
  var bmin = ps[0].y
  var bmax = ps[0].y

  for i in 0..<n:
    rmin = min(rmin, ps[i].x)
    rmax = max(rmax, ps[i].x)
    bmin = min(bmin, ps[i].y)
    bmax = max(bmax, ps[i].y)

  return (bmax - bmin) * (rmax - rmin)

proc solve(n: int; ps: seq[Pair]): int =
  var gmin = INF
  for i in 0..<n:
    gmin = min(gmin, ps[i].x)
    gmin = min(gmin, ps[i].y)

  var gmax = -1
  for i in 0..<n:
    gmax = max(gmax, ps[i].x)
    gmax = max(gmax, ps[i].y)

  return min(pattern1(n, ps, gmin, gmax), pattern2(n, ps, gmin, gmax))

proc main() =
  let n = readInt1()

  var ps = newSeq[Pair](n)
  for i in 0..<n:
    var (x, y) = readInt2();
    if x > y:
      swap(x, y)
    ps[i] = (x, y)

  ps.sort(cmp[Pair])
  echo solve(n, ps)

main()


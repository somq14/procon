import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import future

const INF* = int(1e18 + 373)

proc readLine*(): string =
  stdin.readLine()
proc readSeq*(): seq[string] =
  readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = readLine().strip()
proc readInt1*(): int =
  readSeq().map(parseInt)[0]
proc readInt2*(): (int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1])
proc readInt3*(): (int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2])
proc readInt4*(): (int, int, int, int) =
  let a = readSeq().map(parseInt)
  return (a[0], a[1], a[2], a[3])
proc newSeqWith*[T](n: Natural; e: T): seq[T] =
  result = newSeq[T](n)
  for i in 0..<n:
    result[i] = e
type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] =
  newSeqWith(n1, newSeq[T](n2))
type seq3*[T] = seq[seq[seq[T]]]
proc newSeq3*[T](n1, n2, n3: Natural): seq3[T] =
  newSeqWith(n1, newSeqWith(n2, newSeq[T](n3)))

#------------------------------------------------------------------------------#
var grundySet = initSet[int]()

proc calcGrundy(g: seq2[int]; assign: seq[int]; v: int): int =
  for u in g[v]:
    grundySet.incl(assign[u])

  var grundy = 0
  while grundy in grundySet:
    grundy += 1

  for u in g[v]:
    grundySet.excl(assign[u])

  return grundy

proc dfs(g: seq2[int]; v: int; visited: var seq[bool]; assign: var seq[int]) =
  visited[v] = true

  for u in g[v]:
    if visited[u]:
      continue
    dfs(g, u, visited, assign)

  for u in g[v]:
    if assign[u] == -1:
      return

  assign[v] = calcGrundy(g, assign, v)

proc trial(g: seq2[int]; assign, cycle: seq[int]; a: int): bool =
  var assign = assign

  assign[cycle[0]] = a
  for i in countdown(cycle.len() - 1, 1):
    assign[cycle[i]] = calcGrundy(g, assign, cycle[i])

  for i in 0..<cycle.len():
    let v = cycle[i]
    let u = cycle[(i + 1) mod cycle.len()]
    if assign[v] == assign[u]:
      return false

  return calcGrundy(g, assign, cycle[0]) == assign[cycle[0]]

proc main() =
  let n = readInt1()
  let p = readSeq().map(parseInt).map(it => it - 1)

  var g = newSeq2[int](n, 0)
  for i in 0..<n:
    g[p[i]].add(i)

  var assign = newSeq[int](n)
  assign.fill(-1)
  var visited = newSeq[bool](n)

  for v in 0..<n:
    if visited[v]:
      continue
    dfs(g, v, visited, assign)

  var cycle = newSeq[int](0)
  block:
    var v = -1
    for u in 0..<n:
      if assign[u] == -1:
        v = u
        break

    let begin = v
    while true:
      cycle.add(v)

      for u in g[v]:
        if assign[u] == -1:
          v = u
          break

      if v == begin:
        break

  var grundySet = initSet[int]()
  for v in g[cycle[0]]:
    if assign[v] != -1:
      grundySet.incl(assign[v])

  var a0 = 0
  while a0 in grundySet:
    a0 += 1

  var a1 = a0 + 1
  while a1 in grundySet:
    a1 += 1

  let ans = trial(g, assign, cycle, a0) or trial(g, assign, cycle, a1)
  echo if ans: "POSSIBLE" else: "IMPOSSIBLE"

main()


import strutils
import sequtils
import algorithm
import math
import queues
import tables
import logging
import future

const INF* = int(1e18 + 373)

when not defined(release):
  addHandler(newFileLogger(stderr, lvlAll, "[ $levelname ] "))

proc readInt1*(): int = stdin.readLine().strip().parseInt()
proc readInt2*(): (int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1])
proc readInt3*(): (int, int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1], v[2])

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#
#------------------------------------------------------------------------------#
type Pred1[T] = T -> bool

# [lb, ub)
proc nibutanLb(lb, ub: int; f: Pred1[int]): int =
  var lb = lb
  var ub = ub
  while ub - lb > 1:
    let mid = (ub + lb) div 2
    if f(mid):
      lb = mid
    else:
      ub = mid
  return lb

# (lb, ub]
proc nibutanUb(lb, ub: int; f: Pred1[int]): int =
  var lb = lb
  var ub = ub
  while ub - lb > 1:
    let mid = (ub + lb) div 2
    if f(mid):
      ub = mid
    else:
      lb = mid
  return ub

#------------------------------------------------------------------------------#

const M = 20

proc step(next: seq2[int]; p, t: int): int =
  var p = p

  for i in 0..M:
    if (t and (1 shl i)) > 0:
      p = next[i][p]

  return p

proc main() =
  let n = readInt1()
  let x = readSeq().map(parseInt)
  let d = readInt1()

  let q = readInt1()
  var a = newSeq[int](q)
  var b = newSeq[int](q)

  for i in 0..<q:
    (a[i], b[i]) = readInt2()
    a[i] -= 1
    b[i] -= 1
    if a[i] > b[i]:
      swap(a[i], b[i])

  var next = newSeq2[int](M + 1, n)
  for i in 0..<n:
    next[0][i] = nibutanLb(0, n, (p: int) => (x[p] <= x[i] + d))

  for t in 1..M:
    for i in 0..<n:
      next[t][i] = next[t - 1][next[t - 1][i]]

  for i in 0..<q:
    let ans = nibutanUb(-1, n, (t: int) => step(next, a[i], t) >= b[i])
    echo ans

main()


import strutils
import sequtils
import algorithm
import math
import queues
import tables
import logging
import future

const INF* = int(1e18 + 373)

when not defined(release):
  addHandler(newFileLogger(stderr, lvlAll, "[ $levelname ] "))

proc readInt1*(): int = stdin.readLine().strip().parseInt()
proc readInt2*(): (int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1])
proc readInt3*(): (int, int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1], v[2])

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#

proc main() =
  let (n, c) = readInt2()

  var x = newSeq[int](n)
  var v = newSeq[int](n)
  for i in 0..<n:
    (x[i], v[i]) = readInt2()

  var dpL = newSeq[int](n + 1)
  var sumL = 0
  dpL[0] = 0
  for i in 1..n:
    sumL += v[i - 1]
    dpL[i] = sumL - x[i - 1]

  var dpR = newSeq[int](n + 1)
  var sumR = 0
  dpR[0] = 0
  for i in 1..n:
    sumR += v[n - i]
    dpR[i] = sumR - (c - x[n - i])

  var maxL = newSeq[int](n + 1)
  maxL[0] = dpL[0]
  for i in 1..n:
    maxL[i] = max(maxL[i - 1], dpL[i])

  var maxR = newSeq[int](n + 1)
  maxR[0] = dpR[0]
  for i in 1..n:
    maxR[i] = max(maxR[i - 1], dpR[i])

  debug dpL
  debug dpR
  debug maxL
  debug maxR

  var ans = max([max(dpL), max(dpR)])
  for i in 0..<n:
    var cost = dpL[i + 1] - x[i] + maxR[n - i - 1]
    ans = max(ans, cost)

  for i in 0..<n:
    var cost = dpR[i + 1] - (c - x[n - i - 1]) + maxL[n - i - 1]
    ans = max(ans, cost)

  echo ans

main()


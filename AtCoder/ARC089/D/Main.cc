#include <algorithm>
#include <cmath>
#include <complex>
#include <iomanip>
#include <iostream>
#include <string>
#include <utility>
#include <vector>

using namespace std;

using ll = long long int;
const int MOD = 1e9 + 7;
const int INF = 1e9 + 373;

#define rep(i, n) for (int i = 0; i < (n); ++i)

template <typename T>
using vector2 = vector<vector<T>>;
template <typename T>
vector2<T> init_vector2(size_t n0, size_t n1, T e = T()) {
    return vector2<T>(n0, vector<T>(n1, e));
}

template <typename T>
using vector3 = vector<vector<vector<T>>>;
template <typename T>
vector3<T> init_vector3(size_t n0, size_t n1, size_t n2, T e = T()) {
    return vector3<T>(n0, vector2<T>(n1, vector<T>(n2, e)));
}

template <typename T>
void dump(int n, const vector2<T>& a) {
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < n; j++) {
            cout << a[i][j];
        }
        cout << endl;
    }
    cout << endl;
}

vector2<int> count_table(int n, const vector2<int>& a) {
    vector2<int> ans = init_vector2(n + 1, n + 1, 0);
    for (int y = 1; y <= n; y++) {
        for (int x = 1; x <= n; x++) {
            ans[y][x] = ans[y - 1][x] + ans[y][x - 1] - ans[y - 1][x - 1] +
                        a[y - 1][x - 1];
        }
    }
    return ans;
}

int access_table(const vector2<int>& a, int sy, int sx, int ty, int tx) {
    return a[ty][tx] - a[ty][sx] - a[sy][tx] + a[sy][sx];
}

int eval(int k, const vector2<int>& btab, const vector2<int>& wtab, int cy,
         int cx) {
    int ans = 0;
    ans += access_table(btab, cy, cx, cy + k, cx + k);
    ans += access_table(btab, cy + k, cx + k, cy + 2 * k, cx + 2 * k);
    ans += access_table(wtab, cy + k, cx, cy + 2 * k, cx + k);
    ans += access_table(wtab, cy, cx + k, cy + k, cx + 2 * k);
    return ans;
}

int main() {
    int n, k;
    cin >> n >> k;

    vector2<int> b = init_vector2(4 * k, 4 * k, 0);
    vector2<int> w = init_vector2(4 * k, 4 * k, 0);

    rep(i, n) {
        int x, y;
        char c;
        cin >> x >> y >> c;

        x %= 2 * k;
        y %= 2 * k;

        if (c == 'B') {
            b[y][x]++;
            b[y + 2 * k][x]++;
            b[y][x + 2 * k]++;
            b[y + 2 * k][x + 2 * k]++;
        } else {
            w[y][x]++;
            w[y + 2 * k][x]++;
            w[y][x + 2 * k]++;
            w[y + 2 * k][x + 2 * k]++;
        }
    }

    // dump(4 * k, a);

    vector2<int> btab = count_table(4 * k, b);
    vector2<int> wtab = count_table(4 * k, w);

    // dump(4 * k + 1, btab);
    // dump(4 * k + 1, wtab);

    int ans = 0;
    for (int cy = 0; cy < 2 * k; cy++) {
        for (int cx = 0; cx < 2 * k; cx++) {
            ans = max(ans, eval(k, btab, wtab, cy, cx));
        }
    }
    cout << ans << endl;

    return 0;
}

import strutils
import sequtils
import algorithm
import math
import queues
import tables
import logging
import future

const INF* = int(1e18 + 373)

when not defined(release):
  addHandler(newFileLogger(stderr, lvlAll, "[ $levelname ] "))

proc readInt1*(): int = stdin.readLine().strip().parseInt()
proc readInt2*(): (int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1])
proc readInt3*(): (int, int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1], v[2])

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#
type Plane = tuple [ p, q, r: int ]

proc eval(a, b, p, q, r: int; d: seq2[int]): bool =
  for x in 1..a:
    for y in 1..b:
      if p * x + q * y + r < d[x - 1][y - 1]:
        return false
  return true

proc adjust(a, b, p, q: int; d: seq2[int]): int =
  for i in 0..100:
    if eval(a, b, p, q, i, d):
      return i
  return 100

type Edge = tuple [ to, cost: int ]
const Y = -1
const X = -2

proc constructGraph(planes: seq[Plane]): seq2[Edge] =
  var g = newSeq2[Edge](202, 0)
  let s = 0
  let t = 101

  for i in 0..<100:
    g[i].add((i + 1, X))

  for i in countdown(201, 102):
    g[i].add((i - 1, Y))

  for plane in planes:
    g[plane.p].add((plane.q + 101, plane.r))

  return g

proc solve(a, b: int; d: seq2[int]): seq2[Edge] =
  var planes: seq[Plane] = @[]

  # p * x + q * x + r
  for p in 0..100:
    for q in 0..100:
      let plane = (p, q, adjust(a, b, p, q, d))
      planes.add(plane)

  for x in 1..a:
    for y in 1..b:
      let v = planes.map(it => it.p * x + it.q * y + it.r).min()
      if d[x - 1][y - 1] != v:
        return nil

  return constructGraph(planes)

proc main() =
  let (a, b) = readInt2()

  var d = newSeq[seq[int]](a)
  for i in 0..<a:
    d[i] = readSeq().map(parseInt)

  let g = solve(a, b, d)
  if g == nil:
    echo "Impossible"
    return

  echo "Possible"

  let n = g.len()
  let m = g.map(it => it.len()).sum()

  echo n, " ", m
  for i in 0..<n:
    for e in g[i]:
      if e.cost == X:
        echo ((i + 1), " ", (e.to + 1), " ", "X")
      elif e.cost == Y:
        echo ((i + 1), " ", (e.to + 1), " ", "Y")
      else:
        echo ((i + 1), " ", (e.to + 1), " ", e.cost)
  echo "1 102"

main()


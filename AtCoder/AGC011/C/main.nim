import strutils
import sequtils
import algorithm
import math
import queues
import tables
import sets
import logging
import future

const INF* = int(1e18 + 373)

when not defined(release):
  addHandler(newFileLogger(stderr, lvlAll, "[ $levelname ] "))

proc readInt1*(): int = stdin.readLine().strip().parseInt()
proc readInt2*(): (int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1])
proc readInt3*(): (int, int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1], v[2])
proc readInt4*(): (int, int, int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1], v[2], v[3])

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#
type UnionFindTree = tuple[ p, h, w: seq[int] ]

proc initUnionFindTree(n: int): UnionFindTree =
    var p = newSeq[int](n)
    p.fill(-1)
    var h = newSeq[int](n)
    h.fill(0)
    var w = newSeq[int](n)
    w.fill(1)
    return (p, h, w)

proc find(this: var UnionFindTree, v: int): int =
    if this.p[v] == -1:
        return v
    this.p[v] = find(this, this.p[v])
    return this.p[v]

proc same(this: var UnionFindTree, u, v: int): bool =
    this.find(u) == this.find(v)

proc union(this: var UnionFindTree, u, v: int) =
    let uRoot = this.find(u)
    let vRoot = this.find(v)
    if uRoot == vRoot:
        return

    if this.h[uRoot] > this.h[vRoot]:
        this.p[vRoot] = uRoot
        this.w[uRoot] += this.w[vRoot]
    else:
        this.p[uRoot] = vRoot
        this.w[vRoot] += this.w[uRoot]
        if this.h[uRoot] == this.h[vRoot]:
            this.h[vRoot] += 1
#------------------------------------------------------------------------------#
proc detectOddCircuit(g: seq2[int]; v: int; a: var seq[int]): bool =
  var foundZero = false
  var foundOne = false
  for u in g[v]:
    if a[u] < 0:
      continue
    if a[u] == 0:
      foundZero = true
    if a[u] == 1:
      foundOne = true

  if foundZero and foundOne:
    return true

  a[v] = if foundZero: 1 else: 0

  for u in g[v]:
    if a[u] < 0:
      if detectOddCircuit(g, u, a):
        return true
  return false

proc main() =
  let (n, m) = readInt2()
  var g = newSeq2[int](n, 0)
  for i in 0..<m:
    let (u, v) = readInt2()
    g[u - 1].add(v - 1)
    g[v - 1].add(u - 1)

  var uft = initUnionFindTree(n)
  for v in 0..<n:
    for u in g[v]:
      uft.union(v, u)

  var a = newSeq[int](n)
  a.fill(-1)

  var siz = newSeq[int](0)
  var odd = newSeq[bool](0)
  for v in 0..<n:
    if uft.find(v) != v:
      continue
    siz.add(uft.w[v])
    odd.add(detectOddCircuit(g, v, a))


  let k = siz.len()
  var oddCount = 0
  var evenCount = 0
  var singleCount = 0
  for i in 0..<k:
    if siz[i] == 1:
      singleCount += 1
    elif odd[i]:
      oddCount += 1
    else:
      evenCount += 1

  debug "k = ", k
  debug "size = ", siz
  debug "odd = ", odd
  debug "oddCount = ", oddCount
  debug "evenCount = ", evenCount
  debug "singleCount = ", singleCount

  var ans = 0
  ans += singleCount * singleCount
  ans += oddCount * oddCount
  ans += 2 * evenCount * evenCount
  ans += 2 * evenCount * oddCount
  ans += 2 * (n - singleCount) * singleCount
  echo ans

main()


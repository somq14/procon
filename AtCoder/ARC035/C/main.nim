import strutils
import sequtils
import algorithm
import math
import queues
import tables

const INF* = int(1e18 + 373)

proc readInt1*(): int = stdin.readLine().strip().parseInt()
proc readInt2*(): (int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1])
proc readInt3*(): (int, int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1], v[2])
proc readInt*(): seq[int] = stdin.readLine().strip().split().map(parseInt)

#------------------------------------------------------------------------------#
proc warshallFloyd(n: int; g: seq[seq[int]]): seq[seq[int]] =
  var d = g
  for k in 0..<n:
    for i in 0..<n:
      for j in 0..<n:
        d[i][j] = min(d[i][j], d[i][k] + d[k][j])
  return d

proc calcS(n: int; d: seq[seq[int]]): int =
  result = 0
  for i in 0..<n:
    for j in (i + 1)..<n:
      result += d[i][j]

proc main() =
  let (n, m) = readInt2()

  var g = newSeqWith(n, newSeq[int](n))
  for i in 0..<n:
    g[i].fill(INF)
    g[i][i] = 0

  for i in 0..<m:
    let (a, b, c) = readInt3()
    g[a - 1][b - 1] = c
    g[b - 1][a - 1] = c

  var d = warshallFloyd(n, g)

  let k = readInt1()
  for _ in 0..<k:
    let (x, y, z) = readInt3()
    d[x - 1][y - 1] = min(d[x - 1][y - 1], z)
    d[y - 1][x - 1] = min(d[y - 1][x - 1], z)
    for i in 0..<n:
      for j in 0..<n:
        d[i][j] = min(d[i][j], d[i][x - 1] + d[x - 1][y - 1] + d[y - 1][j])
        d[i][j] = min(d[i][j], d[i][y - 1] + d[y - 1][x - 1] + d[x - 1][j])
    echo calcS(n, d)


main()


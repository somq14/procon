#include <algorithm>
#include <complex>
#include <iostream>
#include <queue>
#include <set>
#include <string>
#include <vector>

using std::cin;
using std::cout;
using std::endl;

using std::string;
using std::to_string;
using std::vector;
using std::set;
using std::queue;
using std::priority_queue;

using std::min;
using std::max;
using std::sort;
using std::abs;

typedef unsigned long long ll;

ll gcd(ll a, ll b){
    while(b != 0){
        ll m = a % b;
        a = b;
        b = m;
    }
    return a;
}

int main(void) {
    int n;
    cin >> n;

    vector<ll> t(n);
    for(int i = 0; i < n; i++){
        cin >> t[i];
    }

    ll ans = 1;
    for(int i = 0; i < n; i++){
        ans = t[i] / gcd(t[i], ans) * ans;
    }

    cout << ans << endl;

    return 0;
}

#include <algorithm>
#include <complex>
#include <iostream>
#include <queue>
#include <set>
#include <string>
#include <vector>

using std::cin;
using std::cout;
using std::endl;

using std::string;
using std::to_string;
using std::vector;
using std::set;
using std::queue;
using std::priority_queue;

using std::min;
using std::max;
using std::sort;
using std::abs;

int main(void) {
    int a, b, c, d;
    cin >> a >> b >> c >> d;

    int cnt = 0;
    for(int i = 0; i < 300; i++){
        if(a <= i && i < b && c <= i && i < d){
            cnt++;
        }
    }

    cout << cnt << endl;

    return 0;
}

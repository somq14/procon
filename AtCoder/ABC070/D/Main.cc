#include <algorithm>
#include <complex>
#include <iostream>
#include <queue>
#include <set>
#include <string>
#include <vector>

using std::cin;
using std::cout;
using std::endl;

using std::string;
using std::to_string;
using std::vector;
using std::set;
using std::queue;
using std::priority_queue;

using std::min;
using std::max;
using std::sort;
using std::abs;

typedef long long ll;

class edge {
   public:
    int to;
    ll cost;

    edge() : to(), cost() {}
    edge(int to, ll cost) : to(to), cost(cost) {}
};

vector<ll> bfs(int n, const vector<vector<edge>>& g, int s) {
    vector<ll> d(n, -1);
    d[s] = 0;

    queue<int> q;
    q.push(s);

    while (!q.empty()) {
        int v = q.front();
        q.pop();

        for (const edge& e : g[v]) {
            if (d[e.to] == -1) {
                d[e.to] = d[v] + e.cost;
                q.push(e.to);
            }
        }
    }

    return d;
}

int main(void) {
    int n;
    cin >> n;

    vector<vector<edge>> e(n);
    for (int i = 0; i < n - 1; i++) {
        int a, b, c;
        cin >> a >> b >> c;
        e[a - 1].push_back(edge(b - 1, c));
        e[b - 1].push_back(edge(a - 1, c));
    }

    int q, k;
    cin >> q >> k;

    vector<ll> d = bfs(n, e, k - 1);

    for (int i = 0; i < q; i++) {
        int x, y;
        cin >> x >> y;
        cout << d[x - 1] + d[y - 1] << endl;
    }

    return 0;
}

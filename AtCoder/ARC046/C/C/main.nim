import strutils
import sequtils
import algorithm
import math
import queues
import tables
import logging
import future

const INF* = int(1e18 + 373)

when not defined(release):
  addHandler(newFileLogger(stderr, lvlAll, "[ $levelname ] "))

proc readInt1*(): int = stdin.readLine().strip().parseInt()
proc readInt2*(): (int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1])
proc readInt3*(): (int, int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1], v[2])

proc readSeq*(): seq[string] = stdin.readLine().strip().split()
proc readSeq*(n: Natural): seq[string] =
  result = newSeq[string](n)
  for i in 0..<n:
    result[i] = stdin.readLine().strip()

type seq2*[T] = seq[seq[T]]
proc newSeq2*[T](n1, n2: Natural): seq2[T] = newSeqWith(n1, newSeq[T](n2))

#------------------------------------------------------------------------------#
type Op2[T] = proc(a, b: T): T
type SegmentTree[T] = tuple[ n: int, a: seq[T], e: T, f: Op2[T] ]

proc initSegmentTree[T](n: int, e: T, f: Op2[T]): SegmentTree[T] =
    var m = 1
    while m < n:
        m *= 2

    var a = newSeq[T](2 * m)
    a.fill(e)
    return (m, a, e, f)

proc update[T](this: var SegmentTree[T], i: int, x: T) =
    this.a[this.n + i] = x
    var j = (this.n + i) div 2
    while j > 0:
        this.a[j] = this.f(this.a[j * 2], this.a[j * 2 + 1])
        j = j div 2

proc query[T](this: SegmentTree[T], s, t, i, ll, hh: int): T =
    if t <= ll or hh <= s:
        return this.e
    if s <= ll and hh <= t:
        return this.a[i]

    let m = (ll + hh) div 2
    let vl = this.query(s, t, i * 2, ll, m)
    let vh = this.query(s, t, i * 2 + 1, m, hh)
    return this.f(vl, vh)

proc query[T](this: SegmentTree[T], s, t: int): T = query(this, s, t, 1, 0, this.n)

#------------------------------------------------------------------------------#
type Pred1[T] = T -> bool

# [lb, ub)
proc nibutanLb(lb, ub: int; f: Pred1[int]): int =
  var lb = lb
  var ub = ub
  while ub - lb > 1:
    let mid = (ub + lb) div 2
    if f(mid):
      lb = mid
    else:
      ub = mid
  return lb

# (lb, ub]
proc nibutanUb(lb, ub: int; f: Pred1[int]): int =
  var lb = lb
  var ub = ub
  while ub - lb > 1:
    let mid = (ub + lb) div 2
    if f(mid):
      ub = mid
    else:
      lb = mid
  return ub
#------------------------------------------------------------------------------#

type Person = tuple[a, b: int; man: bool]

proc compress(a: seq[int]): Table[int, int] =
  let a = a.sorted(cmp[int])
  var t = initTable[int, int](nextPowerOfTwo(a.len() * 2))
  for e in a:
    if e notin t:
      t[e] = t.len()
  return t

proc `<`(p0, p1: Person): bool =
  if p0.a == p1.a:
    return p0.man < p1.man
  return p0.a < p1.a

proc f(a, b: int): int = min(a, b)

proc main() =
  let (n, m) = readInt2()

  var ps = newSeq[Person](n + m)
  for i in 0..<n:
    let (a, b) = readInt2()
    ps[i] = (a, b, true)

  for i in 0..<m:
    let (c, d) = readInt2()
    ps[n + i] = (d, c, false)

  var tmp = newSeq[int](0)
  for p in ps:
    tmp.add(p.a)
    tmp.add(p.b)
  let comp = compress(tmp)

  for i in 0..<(n + m):
    ps[i].a = comp[ps[i].a]
    ps[i].b = comp[ps[i].b]

  ps.sort(cmp[Person])

  var count = newSeq[int](comp.len())
  var segtree = initSegmentTree[int](comp.len(), INF, f)

  var ans = 0

  for p in ps:
    if not p.man:
      if count[p.b] == 0:
        segtree.update(p.b, p.b)
      count[p.b] += 1
    else:
      let i = segtree.query(p.b, comp.len())
      if i != INF:
        ans += 1
        count[i] -= 1
        if count[i] == 0:
          segtree.update(i, INF)

  echo ans

main()


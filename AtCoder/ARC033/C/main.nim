import strutils
import sequtils
import algorithm
import math
import queues

const INF = int(1e18 + 373)
const MOD = int(1e9 + 7)

proc readInt1(): int = stdin.readLine().strip().parseInt()
proc readInt2(): (int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1])
proc readInt3(): (int, int, int) =
    let v = stdin.readLine().strip().split().map(parseInt)
    return (v[0], v[1], v[2])
proc readInt(): seq[int] = stdin.readLine().strip().split().map(parseInt)

#------------------------------------------------------------------------------#

type SegmentTree[T] = tuple[ n: int, a: seq[T], e: T, f: proc(a, b: T): T ]

proc initSegmentTree[T](n: int, e: T, f: proc(a, b: T): T): SegmentTree[T] =
    var m = 1
    while m < n:
        m *= 2

    var a = newSeq[T](2 * m)
    a.fill(e)
    return (m, a, e, f)

proc update[T](this: var SegmentTree[T], i: int, x: T) =
    this.a[this.n + i] = x
    var j = (this.n + i) div 2
    while j > 0:
        this.a[j] = this.f(this.a[j * 2], this.a[j * 2 + 1])
        j = j div 2

proc query[T](this: SegmentTree[T], s, t, i, ll, hh: int): T =
    if t <= ll or hh <= s:
        return this.e
    if s <= ll and hh <= t:
        return this.a[i]

    let m = (ll + hh) div 2
    let vl = this.query(s, t, i * 2, ll, m)
    let vh = this.query(s, t, i * 2 + 1, m, hh)
    return this.f(vl, vh)

proc query[T](this: SegmentTree[T], s, t: int): T = query(this, s, t, 1, 0, this.n)

#------------------------------------------------------------------------------#

proc f(a, b: int): int = a + b
proc solve(segtree: SegmentTree[int], x: int): int =
  var j = 1
  var x = x
  while j < segtree.n:
    if x > segtree.a[j * 2]:
      x -= segtree.a[j * 2]
      j = j * 2 + 1
    else:
      j = j * 2
  return j - segtree.n

proc main() =
  var segtree = initSegmentTree[int](200001, 0, f)

  let q = readInt1()
  for i in 0..<q:
    let (t, x) = readInt2()
    case t:
    of 1:
      segtree.update(x, 1)
    of 2:
      let n = solve(segtree, x)
      echo n
      segtree.update(n, 0)
    else:
      discard

main()


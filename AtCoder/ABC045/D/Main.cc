#include <algorithm>
#include <complex>
#include <iostream>
#include <queue>
#include <set>
#include <stack>
#include <string>
#include <vector>

using std::cin;
using std::cout;
using std::endl;
using std::cerr;

using std::string;
using std::to_string;
using std::vector;
using std::set;
using std::queue;
using std::stack;
using std::priority_queue;

using std::min;
using std::max;
using std::sort;
using std::abs;

using std::binary_search;

typedef long long int ll;
const int MOD = 1e9 + 7;

class point {
   public:
    int x, y;
    point() {}
    point(int x, int y) : x(x), y(y) {}

    bool operator<(const point& p) const {
        return x == p.x ? y < p.y : x < p.x;
    }
    bool operator==(const point& p) const {
        return x == p.x && y == p.y;
    }
};

void subr(vector<point>& pp, int w, int h, int x, int y) {
    if (1 < x && x < w && 1 < y && y < h) {
        pp.push_back(point(x, y));
    }
}

int main() {
    int h, w, n;
    cin >> h >> w >> n;

    vector<point> bp(n);
    for (int i = 0; i < n; i++) {
        int a, b;
        cin >> a >> b;
        bp[i] = point(b, a);
    }
    sort(bp.begin(), bp.end());

    vector<point> pp;
    for (const point& p : bp) {
        subr(pp, w, h, p.x + 0, p.y + 0);
        subr(pp, w, h, p.x + 0, p.y + 1);
        subr(pp, w, h, p.x + 0, p.y - 1);
        subr(pp, w, h, p.x + 1, p.y + 0);
        subr(pp, w, h, p.x + 1, p.y + 1);
        subr(pp, w, h, p.x + 1, p.y - 1);
        subr(pp, w, h, p.x - 1, p.y + 0);
        subr(pp, w, h, p.x - 1, p.y + 1);
        subr(pp, w, h, p.x - 1, p.y - 1);
    }

    sort(pp.begin(), pp.end());
    pp.erase(unique(pp.begin(), pp.end()), pp.end());

    vector<ll> ans(10);
    for (const point& p : pp) {
        int cnt = 0;
        cnt += binary_search(bp.begin(), bp.end(), point(p.x + 0, p.y + 0)) ? 1 : 0;
        cnt += binary_search(bp.begin(), bp.end(), point(p.x + 0, p.y + 1)) ? 1 : 0;
        cnt += binary_search(bp.begin(), bp.end(), point(p.x + 0, p.y - 1)) ? 1 : 0;
        cnt += binary_search(bp.begin(), bp.end(), point(p.x + 1, p.y + 0)) ? 1 : 0;
        cnt += binary_search(bp.begin(), bp.end(), point(p.x + 1, p.y + 1)) ? 1 : 0;
        cnt += binary_search(bp.begin(), bp.end(), point(p.x + 1, p.y - 1)) ? 1 : 0;
        cnt += binary_search(bp.begin(), bp.end(), point(p.x - 1, p.y + 0)) ? 1 : 0;
        cnt += binary_search(bp.begin(), bp.end(), point(p.x - 1, p.y + 1)) ? 1 : 0;
        cnt += binary_search(bp.begin(), bp.end(), point(p.x - 1, p.y - 1)) ? 1 : 0;
        ans[cnt]++;
    }

    ll sum = 0;
    for(int i = 1; i <= 9; i++){
        sum += ans[i];
    }
    ans[0] = (ll)(w - 2) * (ll)(h - 2) - sum;

    for(int i = 0; i <= 9; i++){
        cout << ans[i] << endl;
    }

    return 0;
}

#include <algorithm>
#include <complex>
#include <iostream>
#include <queue>
#include <set>
#include <stack>
#include <string>
#include <vector>

using std::cin;
using std::cout;
using std::endl;
using std::cerr;

using std::string;
using std::to_string;
using std::vector;
using std::set;
using std::queue;
using std::stack;
using std::priority_queue;

using std::min;
using std::max;
using std::sort;
using std::abs;

typedef long long int ll;
const int MOD = 1e9 + 7;

int main() {
    string s;
    cin >> s;

    int n = s.length();

    ll sum = 0;
    for (int set = 0; set < (1 << (n - 1)); set++) {
        ll acc = 0;
        for(int i = 0; i < n; i++){
            acc = 10 * acc + (s[i] - '0');
            if(set & (1 << i)){
                sum += acc;
                acc = 0;
            }
        }
        sum += acc;
    }

    cout << sum << endl;
    return 0;
}

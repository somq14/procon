import java.util.Scanner;
class Main{
    static final int MAX_VALUE = 2000 * 1000;

    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);

        int n = sc.nextInt();
        int[] cnt = new int[MAX_VALUE+1];
        for(int i = 0; i < n; i++){
            cnt[sc.nextInt()]++;
        }

        StringBuilder sb = new StringBuilder();
        for(int i = 0; i <= MAX_VALUE; i++){
            for(int j = 0; j < cnt[i]; j++){
                sb.append(i);
                sb.append(' ');
            }
        }
        sb.setLength(sb.length() - 1);
        System.out.println(sb.toString());
    }
}

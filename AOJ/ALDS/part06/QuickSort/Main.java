import java.util.Scanner;

class Main{
    static class Pair<K extends Comparable<K>, V> implements Comparable<Pair<K, V>>{
        private final K k;
        private final V v;

        Pair(K k, V v){
            this.k = k;
            this.v = v;
        }

        K key(){
            return k;
        }

        V val(){
            return v;
        }

        @Override
        public int compareTo(Pair<K, V> p){
            return key().compareTo(p.key());
        }
    }

    static <T> void swap(T[] a, int i, int j){
        T tmp = a[i];
        a[i] = a[j];
        a[j] = tmp;
    }

    static <T extends Comparable<T>> int partition(T[] a, int p, int r){
        T x = a[r];
        int i = p; // a[p] ~ a[i-1] are less than or equal to x
        for(int j = p; j < r; j++){
            if(a[j].compareTo(x) <= 0){
                swap(a, i++, j);
            }
        }
        swap(a, i, r);
        return i;
    }

    static <T extends Comparable<T>> void quickSort(T[] a, int p, int r){
        if(p < r){
            int q = partition(a, p, r);
            quickSort(a, p, q-1);
            quickSort(a, q+1, r);
        }
    }

    @SuppressWarnings("unchecked")
    static <T extends Comparable<T>> void merge(T[] a, int l, int m, int r){
        int nl = m - l;
        T[] al = (T[])new Comparable[nl];
        for(int i = 0; i < nl; i++){
            al[i] = a[l+i];
        }

        int nr = r - m;
        T[] ar = (T[])new Comparable[nr];
        for(int i = 0; i < nr; i++){
            ar[i] = a[m+i];
        }

        int j = nl - 1;
        int k = r - 1;
        for(int i = nr - 1; i >= 0; i--){
            for(;j >= 0 && al[j].compareTo(ar[i]) > 0; j--){
                a[k--] = al[j];
            }
            a[k--] = ar[i];
        }

    }

    static <T extends Comparable<T>> void mergeSort(T[] a, int l, int r){
        if(r-l <= 1){
            return;
        }
        int m = (l + r) / 2;
        mergeSort(a, l, m);
        mergeSort(a, m, r);
        merge(a, l, m, r);
    }

    @SuppressWarnings("unchecked")
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);

        int n = sc.nextInt();
        Pair<Integer,String>[] a = new Pair[n];

        for(int i = 0; i < n; i++){
            String v = sc.next();
            Integer k = sc.nextInt();
            a[i] = new Pair(k, v);
        }

        Pair<Integer,String>[] b = a.clone();

        quickSort(a, 0, n-1);
        mergeSort(b, 0, n);

        boolean stable = true;
        for(int i = 0; i < n; i++){
            if(!a[i].val().equals(b[i].val())){
                stable = false;
                break;
            }
        }

        System.out.println(stable ? "Stable" : "Not stable");
        for(int i = 0; i < n; i++){
            System.out.println(a[i].val() + " " + a[i].key());
        }
    }
}

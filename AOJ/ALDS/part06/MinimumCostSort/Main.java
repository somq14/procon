import java.util.Scanner;
class Main{

    public static int MAX_VALUE = 10000;

    static void swap(int[] a, int i, int j){
        int tmp = a[i];
        a[i] = a[j];
        a[j] = tmp;
    }

    static int partition(int[] a, int p, int r){
        int x = a[r];
        int i = p; // a[p] ~ a[i-1] are less than or equal to x
        for(int j = p; j < r; j++){
            if(a[j] <= x){
                swap(a, i++, j);
            }
        }
        swap(a, i, r);
        return i;
    }

    static void quickSort(int[] a, int p, int r){
        if(p < r){
            int q = partition(a, p, r);
            quickSort(a, p, q-1);
            quickSort(a, q+1, r);
        }
    }

    static String join(int[] a, int n, String deli){
        StringBuilder sb = new StringBuilder();
        for(int i = 0; i < n; i++){
            sb.append(String.format("%3d", a[i]));
            sb.append(deli);
        }
        sb.setLength(sb.length() - deli.length());
        return sb.toString();
    }

    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        final int n = sc.nextInt();
        final int[] w = new int[n];
        for(int i = 0; i < n; i++){
            w[i] = sc.nextInt();
        }

        final int[] wSorted = w.clone();
        quickSort(wSorted, 0, n-1);

        final int[] wInv = new int[MAX_VALUE+1];
        for(int i = 0; i <= MAX_VALUE; i++){
            wInv[i] = -1;
        }
        for(int i = 0; i < n; i++){
            wInv[w[i]] = i;
        }

        int cost = 0;
        for(int i = 0; i < n; i++){
            final int wMin = wSorted[i];
            final int wMinDest = i;

            int wMinIdx = wInv[wMin];
            int sumWSwap = 0;
            int swapTimes = 0;
            while(wMinIdx != wMinDest){
                int wSwap = wSorted[wMinIdx];
                int wSwapIdx = wInv[wSwap];

                swap(w, wMinIdx, wSwapIdx);

                sumWSwap += wSwap;
                swapTimes++;

                wInv[wMin] = wSwapIdx;
                wInv[wSwap] = wMinIdx;
                wMinIdx = wSwapIdx;
            }
            if(swapTimes > 0){
                int direct = (wMin * swapTimes) + sumWSwap;
                int indirect = (wSorted[0] + wMin) + (wSorted[0] * (swapTimes + 1)) + (sumWSwap + wMin);
                cost += direct < indirect ? direct : indirect;
            }
        }

        System.out.println(cost);
    }

}

import java.util.Scanner;

class Main{
    static void swap(int[] a, int i, int j){
        int tmp = a[i];
        a[i] = a[j];
        a[j] = tmp;
    }
    static int partition(int[] a, int p, int r){
        int x = a[r];
        int i = p; // a[p] ~ a[i-1] are less than or equal to x
        for(int j = p; j < r; j++){
            if(a[j] <= x){
                swap(a, i++, j);
            }
        }
        swap(a, i, r);
        return i;
    }
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int[] a = new int[n];
        for(int i = 0; i < n; i++){
            a[i] = sc.nextInt();
        }
        int q = partition(a, 0, n-1);

        StringBuilder sb = new StringBuilder();
        for(int i = 0; i < n; i++){
            if(i == q){
                sb.append('[');
            }
            sb.append(a[i]);
            if(i == q){
                sb.append(']');
            }
            sb.append(' ');
        }
        sb.setLength(sb.length()-1);
        System.out.println(sb);
    }
}

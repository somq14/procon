import java.util.Scanner;

class Main{

    static boolean isPossible(int[] w, int n, int k, int p){

        int capa = 0;
        int truck = k;

        for(int i = 0; i < n; i++){
            while(capa < w[i]){
                // come on! next truck!
                if(truck == 0){
                    return false;
                }
                truck--;
                capa = p;
            }

            capa -= w[i];
        }

        return true;
    }

    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        final int n = sc.nextInt(); // # of baggage
        final int k = sc.nextInt(); // # of truck
        final int[] w = new int[n];

        int maxWeight = -1;
        int sumWeight = 0;
        for(int i = 0; i < n; i++){
            w[i] = sc.nextInt();

            if(w[i] > maxWeight){
                maxWeight = w[i];
            }
            sumWeight += w[i];
        }

        int estm1 = maxWeight;
        int estm2 = sumWeight / k;

        int l = estm1 > estm2 ? estm1 : estm2;
        int r = sumWeight;
        int min = -1;
        while(l <= r){
            int m = (l + r) / 2;
            if(isPossible(w, n, k, m)){
                r = m - 1;
                min = m;
            }else{
                l = m + 1;
            }
        }

        System.out.println(min);
    }
}


import java.util.Scanner;

class Main{
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);

        final int n = sc.nextInt();
        final int[] s = new int[n];
        for(int i = 0; i < n; i++){
            s[i] = sc.nextInt();
        }

        final int q = sc.nextInt();
        final int[] t = new int[q];
        for(int i = 0; i < q; i++){
            t[i] = sc.nextInt();
        }

        int cnt = 0;
        for(int i = 0; i < q; i++){
            final int target = t[i];
            for(int j = 0; j < n; j++){
                if(s[j] == target){
                    cnt++;
                    break;
                }
            }
        }

        System.out.println(cnt);
    }
}

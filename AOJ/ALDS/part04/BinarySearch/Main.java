import java.util.Scanner;

class Main{

    // a is sorted
    static int search(int a[], int n, int target){
        int l = 0;
        int r = n-1;
        while(l <= r){
            final int m = (l + r) / 2;
            final int v = a[m];

            if(target < v){
                r = m - 1;
            }
            else if(v < target){
                l = m + 1;
            }else{
                assert(v == target);
                return m; // found!!
            }
        }
        return -1; // not found ...
    }

    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);

        final int n = sc.nextInt();
        final int[] s = new int[n];
        for(int i = 0; i < n; i++){
            s[i] = sc.nextInt();
        }

        final int q = sc.nextInt();
        final int[] t = new int[q];
        for(int i = 0; i < q; i++){
            t[i] = sc.nextInt();
        }

        int cnt = 0;
        for(int i = 0; i < q; i++){
            if(search(s, n, t[i]) >= 0){
                cnt++;
            }
        }

        System.out.println(cnt);
    }
}

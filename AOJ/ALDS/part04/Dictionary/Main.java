import java.io.BufferedReader;
import java.io.InputStreamReader;

class Main{

    static final int MAX_LEN = 12;
    static final int ENTRY_NUM = (1 << 21);
    static Node[] table = new Node[ENTRY_NUM];

    private static class Node{
        final Node next;
        final int val;

        Node(Node next, int val){
            this.next = next;
            this.val = val;
        }
    }

    static int encode(String s){
        final int len = s.length();
        int code = 0;
        for(int i = 0; i < len; i++){
            code <<= 2;
            switch(s.charAt(i)){
                case 'A':
                    code |= 0;
                    break;
                case 'C':
                    code |= 1;
                    break;
                case 'G':
                    code |= 2;
                    break;
                case 'T':
                    code |= 3;
                    break;
                default:
                    throw new IllegalStateException();
            }
        }
        code <<= 4;
        code |= len;
        return code;
    }

    static int hash(int code){
        return code & (ENTRY_NUM - 1);
    }

    static void insert(String s){
        int code = encode(s);
        int h = hash(code);

        Node p = new Node(table[h], code);
        table[h] = p;
    }

    static boolean find(String s){
        int code = encode(s);
        int h = hash(code);
        Node p = table[h];
        while(p != null){
            if(p.val == code){
                return true;
            }
            p = p.next;
        }
        return false;
    }

    public static void main(String[] args) throws Exception{
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        final int n = Integer.parseInt(br.readLine());


        int cnt = 0;
        for(int i = 0; i < n; i++){
            String[] cmd = br.readLine().split(" ");

            switch(cmd[0]){
                case "insert":
                    insert(cmd[1]);
                    break;
                case "find":
                    System.out.println(find(cmd[1]) ? "yes" : "no");
                    break;
                default:
                    throw new IllegalStateException();
            }
        }

    }
}

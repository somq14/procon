import java.util.Scanner;
import java.io.PrintWriter;
import java.io.BufferedWriter;
import java.io.OutputStreamWriter;

public class Main{
    static int calcTable(char[] p, int pl, int l, char c){
        for(int i = l; i >= 0; i--){
            int beginInd = l - i;

            if(i >= pl || p[i] != c){
                continue;
            }
            boolean match = true;
            for(int j = 0; j < i; j++){
                if(p[beginInd+j] != p[j]){
                    match = false;
                    break;
                }
            }
            if(match){
                return i+1;
            }
        }
        return 0;
    }

    static int[][] createTable(char[] p, int pl){
        int[][] table = new int[pl+1][128];


        boolean[] exists = new boolean[128];
        for(int i = 0; i < pl; i++){
            exists[p[i]] = true;
        }

        for(int l = 0; l <= pl; l++){
            for(char c = '0'; c  <= '9'; c++){
                table[l][c] = exists[c] ? calcTable(p, pl, l, c) : 0;
            }
            for(char c = 'A'; c  <= 'Z'; c++){
                table[l][c] = exists[c] ? calcTable(p, pl, l, c) : 0;
            }
            for(char c = 'a'; c  <= 'z'; c++){
                table[l][c] = exists[c] ? calcTable(p, pl, l, c) : 0;
            }
        }
        return table;
    }

    static PrintWriter pw = new PrintWriter(new BufferedWriter(new OutputStreamWriter(System.out)));

    static void search(char[] t, int tl, char[] p, int pl){
        int[][] table = createTable(p, pl);

        int l = 0;
        for(int i = 0; i < tl; i++){
            l = table[l][t[i]];
            if(l == pl){
                pw.println(i-pl+1);
            }
        }
    }

    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        String ts = sc.next();
        String ps = sc.next();

        int tl = ts.length();
        char[] t = ts.toCharArray();

        int pl = ps.length();
        char[] p = ps.toCharArray();

        search(t, tl, p, pl);
        pw.flush();
    }
}

import java.util.Scanner;
 
public class Main{
    static final int EXIST_TABLE_LEN = 128;
    static boolean[] createExistTable(char[] p, int pl){
        boolean[] existTable = new boolean[EXIST_TABLE_LEN];
        for(int i = 0; i < pl; i++){
            existTable[p[i]] = true;
        }
        return existTable;
    }
 
    static void search(char[] t, int tl, char[] p, int pl){
        boolean[] existTable = createExistTable(p, pl);
        int i = 0;
        while(i + pl <= tl){
            boolean match = true;
            for(int j = 0; j < pl; j++){
                if(t[i + j] != p[j]){
                    match = false;
                    if(!existTable[t[i + j]]){
                        i += j;
                    }
                    break;
                }
            }
            if(match){
                System.out.println(i);
            }
            i++;
        }
    }
 
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        String ts = sc.next();
        String ps = sc.next();
 
        int tl = ts.length();
        char[] t = ts.toCharArray();
 
        int pl = ps.length();
        char[] p = ps.toCharArray();
 
        search(t, tl, p, pl);
    }
}

import java.util.Scanner;

public class Main{

    static int time = 0;

    static int dfs(boolean[][] g, int n, int v, int[] d, int[] f, boolean[] visited){
        d[v] = ++time;
        visited[v] = true;

        for(int i = 0; i < n; i++){
            if(g[v][i] && !visited[i]){
                dfs(g, n, i, d, f, visited);
            }
        }

        f[v] = ++time;
        return 0;
    }

    static String graphToString(boolean[][] g, int n){
        StringBuilder sb = new StringBuilder();
        for(int i = 0; i < n; i++){
            for(int j = 0; j < n; j++){
                sb.append(g[i][j] ? "1 " : "0 ");
            }
            sb.setLength(sb.length()-1);
            sb.append('\n');
        }
        return sb.toString();
    }

    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);

        final int n = sc.nextInt();
        final boolean[][] g = new boolean[n][n];
        for(int i = 0; i < n; i++){
            int id = sc.nextInt() - 1;
            int degree = sc.nextInt();
            for(int j = 0; j < degree; j++){
                int v = sc.nextInt() - 1;
                g[id][v] = true;
            }
        }

        int[] d = new int[n];
        int[] f = new int[n];
        boolean[] visited = new boolean[n];
        for(int i = 0; i < n; i++){
            if(!visited[i]){
                dfs(g, n, i, d, f, visited);
            }
        }

        for(int i = 0; i < n; i++){
            System.out.println(String.format("%d %d %d", i+1, d[i], f[i]));
        }

    }
}

import java.util.Scanner;

public class Main{

    static void init(int[] a, int n){
        for(int i = 0; i < n; i++){
            a[i] = -1;
        }
    }

    static void union(int[] a, int n, int s, int t){
        int sRoot = find(a, n, s);
        int tRoot = find(a, n, t);

        if(sRoot == tRoot){
            return;
        }

        if(a[sRoot] < a[tRoot]){
            a[tRoot] = sRoot;
            return;
        }

        if(a[sRoot] > a[tRoot]){
            a[sRoot] = tRoot;
            return;
        }

        assert(a[sRoot] == a[tRoot]);
        a[sRoot] = tRoot;
        a[tRoot]--;
    }

    static int find(int[] a, int n, int v){
        if(a[v] < 0){
            return v;
        }
        else{
            a[v] = find(a, n, a[v]);
            return a[v];
        }
    }

    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);

        final int n = sc.nextInt();
        int[] a = new int[n];
        init(a, n);

        final int m = sc.nextInt();
        for(int i = 0; i < m; i++){
            final int s = sc.nextInt();
            final int t = sc.nextInt();
            union(a, n, s, t);
        }

        final int q = sc.nextInt();
        for(int i = 0; i < q; i++){
            int s = sc.nextInt();
            int t = sc.nextInt();
            System.out.println(find(a, n, s) == find(a, n, t) ? "yes" : "no");
        }
    }
}

import java.util.Scanner;
import java.util.ArrayDeque;
import java.util.Queue;

public class Main{

    static void solve(boolean[][] g, int n){
        boolean[] visited = new boolean[n];
        int[] depth = new int[n];
        for(int i = 0; i < n; i++){
            depth[i] = -1;
        }

        Queue<Integer> q = new ArrayDeque<>();
        visited[0] = true;
        depth[0] = 0;
        q.offer(0);

        int d = 1;
        while(!q.isEmpty()){
            Queue<Integer> tmp = new ArrayDeque<>();
            while(!q.isEmpty()){
                int v = q.poll();

                for(int i = 0; i < n; i++){
                    if(g[v][i] && !visited[i]){
                        visited[i] = true;
                        depth[i] = d;
                        tmp.offer(i);
                    }
                }
            }
            q.addAll(tmp);
            d++;
        }

        for(int i = 0; i < n; i++){
            System.out.println(String.format("%d %d", i+1, depth[i]));
        }
    }

    static String graphToString(boolean[][] g, int n){
        StringBuilder sb = new StringBuilder();
        for(int i = 0; i < n; i++){
            for(int j = 0; j < n; j++){
                sb.append(g[i][j] ? "1 " : "0 ");
            }
            sb.setLength(sb.length()-1);
            sb.append('\n');
        }
        return sb.toString();
    }

    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);

        final int n = sc.nextInt();
        final boolean[][] g = new boolean[n][n];
        for(int i = 0; i < n; i++){
            int id = sc.nextInt() - 1;
            int degree = sc.nextInt();
            for(int j = 0; j < degree; j++){
                int v = sc.nextInt() - 1;
                g[id][v] = true;
            }
        }

        solve(g, n);
    }
}

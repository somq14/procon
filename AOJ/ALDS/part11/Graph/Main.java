import java.util.Scanner;

public class Main{

    static int solve(){
        return 0;
    }

    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);

        final int n = sc.nextInt();
        final boolean[][] g = new boolean[n][n];
        for(int i = 0; i < n; i++){
            int id = sc.nextInt() - 1;
            int degree = sc.nextInt();
            for(int j = 0; j < degree; j++){
                int v = sc.nextInt() - 1;
                g[id][v] = true;
            }
        }

        StringBuilder sb = new StringBuilder();
        for(int i = 0; i < n; i++){
            for(int j = 0; j < n; j++){
                sb.append(g[i][j] ? "1 " : "0 ");
            }
            sb.setLength(sb.length()-1);
            sb.append('\n');
        }
        System.out.print(sb);
    }
}

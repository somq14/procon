import java.util.Scanner;

public class Main{
    static int left(int i){
        return i * 2;
    }

    static int right(int i){
        return i * 2 + 1;
    }

    static void maxHeapify(int[] a, int n, int i){
        final int v = a[i];

        int j = i;
        while(j < n){
            int max = v;
            int maxInd = j;

            int l = left(j);
            if(l <= n && a[l] > max){
                max = a[l];
                maxInd = l;
            }
            int r = right(j);
            if(r <= n && a[r] > max){
                max = a[r];
                maxInd = r;
            }

            if(maxInd == j){
                break;
            }

            a[j] = max;
            j = maxInd;
        }
        a[j] = v;
    }

    static void buildMaxHeap(int[] a, int n){
        for(int i = n/2; i >= 1; i--){
            maxHeapify(a, n, i);
        }
    }

    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);

        final int n = sc.nextInt();
        final int[] a = new int[n+1];
        for(int i = 1; i <= n; i++){
            a[i] = sc.nextInt();
        }

        buildMaxHeap(a, n);

        StringBuilder sb = new StringBuilder();
        for(int i = 1; i <= n; i++){
            sb.append(' ').append(a[i]);
        }
        System.out.println(sb);
    }
}

import java.util.Scanner;

public class Main{

    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);

        final int n = sc.nextInt();
        final int[] a = new int[n+1];
        for(int i = 1; i <= n; i++){
            a[i] = sc.nextInt();
        }

        StringBuilder sb = new StringBuilder();
        for(int i = 1; i <= n; i++){
            sb.append(String.format("node %d: ", i));
            sb.append(String.format("key = %d, ", a[i]));

            int parent = i / 2;
            if(parent >= 1){
                sb.append(String.format("parent key = %d, ", a[parent]));
            }
            int left = i * 2;
            if(left <= n){
                sb.append(String.format("left key = %d, ", a[left]));
            }
            int right = i * 2 + 1;
            if(right <= n){
                sb.append(String.format("right key = %d, ", a[right]));
            }
            sb.append('\n');
        }
        sb.setLength(sb.length() - 1);

        System.out.println(sb);
    }
}

import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.PrintWriter;

public class Main{
    static int left(int i){
        return i * 2;
    }

    static int right(int i){
        return i * 2 + 1;
    }

    static int parent(int i){
        return i / 2;
    }

    static void maxHeapify(int[] a, int n, int i){
        final int v = a[i];

        int j = i;
        while(j < n){
            int max = v;
            int maxInd = j;

            int l = left(j);
            if(l <= n && a[l] > max){
                max = a[l];
                maxInd = l;
            }
            int r = right(j);
            if(r <= n && a[r] > max){
                max = a[r];
                maxInd = r;
            }

            if(maxInd == j){
                break;
            }

            a[j] = max;
            j = maxInd;
        }
        a[j] = v;
    }

    static void buildMaxHeap(int[] a, int n){
        for(int i = n/2; i >= 1; i--){
            maxHeapify(a, n, i);
        }
    }

    static void insert(int[] a, int n, int v){
        int i = n + 1;
        while(i > 1){
            int p = parent(i);

            if(a[p] > v){
                break;
            }

            a[i] = a[p];
            i = p;
        }
        a[i] = v;
    }

    static int extract(int[] a, int n){
        int max = a[1];
        a[1] = a[n];
        maxHeapify(a, n-1, 1);
        return max;
    }

    static final int MAX_ELM = 2000 * 1000;
    public static void main(String[] args) throws Exception{
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        PrintWriter pw = new PrintWriter(new BufferedWriter(new OutputStreamWriter(System.out)));

        int[] heap = new int[MAX_ELM + 1];
        int n = 0;

        loop:while(true){
            String[] cmd = br.readLine().split(" ");
            switch(cmd[0]){
                case "insert":
                    insert(heap, n++, Integer.parseInt(cmd[1]));
                    break;
                case "extract":
                    pw.println(extract(heap, n--));
                    break;
                case "end":
                    break loop;
                default:
                    throw new IllegalStateException();
            }
        }

        pw.flush();
    }
}

import java.util.Scanner;

import java.util.List;
import java.util.ArrayList;

class Node{
    private final int id;
    private final int left;
    private final int right;

    Node(int id, int left, int right){
        this.id = id;
        this.left = left;
        this.right = right;
    }

    int id(){
        return id;
    }

    int left(){
        return left;
    }

    int right(){
        return right;
    }

}

public class Main{

    static void dfs(Node[] nodes, int i, int type){
        if(i < 0){
            return;
        }
        Node n = nodes[i];
        if(type == 0){
            System.out.print(" " + n.id());
        }
        dfs(nodes, n.left(), type);
        if(type == 1){
            System.out.print(" " + n.id());
        }
        dfs(nodes, n.right(), type);
        if(type == 2){
            System.out.print(" " + n.id());
        }
    }

    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);

        final int n = sc.nextInt();
        final Node[] nodes = new Node[n];
        final boolean hasInDegree[] = new boolean[n];

        for(int i = 0; i < n; i++){
            final int id = sc.nextInt();
            final int left = sc.nextInt();
            final int right = sc.nextInt();

            if(left >= 0){
                hasInDegree[left] = true;
            }
            if(right >= 0){
                hasInDegree[right] = true;
            }

            nodes[id] = new Node(id, left, right);
        }

        // search root node
        int root = -1;
        for(int i = 0; i < n; i++){
            if(!hasInDegree[i]){
                root = i;
                break;
            }
        }

        System.out.println("Preorder");
        dfs(nodes, root, 0);
        System.out.println();

        System.out.println("Inorder");
        dfs(nodes, root, 1);
        System.out.println();

        System.out.println("Postorder");
        dfs(nodes, root, 2);
        System.out.println();
    }
}

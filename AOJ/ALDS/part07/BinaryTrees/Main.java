import java.util.Scanner;

import java.util.List;
import java.util.ArrayList;

class Node{
    private final int id;
    private final int left;
    private final int right;

    private int parent = -1;
    private int depth = -1;
    private int sibling = -1;
    private int height = -1;
    private String type = "unknown";

    Node(int id, int left, int right){
        this.id = id;
        this.left = left;
        this.right = right;
    }

    int left(){
        return left;
    }

    int right(){
        return right;
    }

    int parent(){
        return parent;
    }

    void parent(int parent){
        this.parent = parent;
    }

    int depth(){
        return depth;
    }

    void depth(int depth){
        this.depth = depth;
    }

    int height(){
        return height;
    }

    void height(int height){
        this.height = height;
    }

    int sibling(){
        return sibling;
    }

    void sibling(int sibling){
        this.sibling = sibling;
    }

    String type(){
        return type;
    }

    void type(String type){
        this.type = type;
    }

    int inDegree(){
        return parent == -1 ? 0 : 1;
    }

    int outDegree(){
        int l = left  == -1 ? 0 : 1;
        int r = right == -1 ? 0 : 1;
        return l + r;
    }


    @Override
    public String toString(){
        return String.format("node %d: parent = %d, sibling = %d, degree = %d, depth = %d, height = %d, %s"
                , id, parent, sibling, outDegree(), depth, height, type);
    }
}

public class Main{

    static int dfs(Node[] nodes, int i, int p, int s, int d){
        Node n = nodes[i];
        n.parent(p);
        n.sibling(s);
        n.depth(d);
        if(d == 0){
            n.type("root");
        }
        else if(n.outDegree() == 0){
            n.type("leaf");
        }
        else{
            n.type("internal node");
        }

        int lheight = 0;
        if(n.left() >= 0){
            lheight = dfs(nodes, n.left(), i, n.right(), d+1);
        }
        int rheight = 0;
        if(n.right() >= 0){
            rheight = dfs(nodes, n.right(), i, n.left(), d+1);
        }
        n.height(lheight > rheight ? lheight : rheight);
        return n.height() + 1;
    }

    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);

        final int n = sc.nextInt();
        final Node[] nodes = new Node[n];
        final boolean hasInDegree[] = new boolean[n];

        for(int i = 0; i < n; i++){
            final int id = sc.nextInt();
            final int left = sc.nextInt();
            final int right = sc.nextInt();

            if(left >= 0){
                hasInDegree[left] = true;
            }
            if(right >= 0){
                hasInDegree[right] = true;
            }

            nodes[id] = new Node(id, left, right);
        }

        // search root node
        int root = -1;
        for(int i = 0; i < n; i++){
            if(!hasInDegree[i]){
                root = i;
                break;
            }
        }

        dfs(nodes, root, -1, -1, 0);

        for(Node node : nodes){
            System.out.println(node);
        }
    }
}

import java.util.Scanner;

import java.util.List;
import java.util.ArrayList;

class Node{
    private final int id;
    private final List<Integer> children = new ArrayList<>();

    private int parent = -1;
    private int depth = -1;
    private String type = "unknown";

    Node(int id){
        this.id = id;
    }

    int parent(){
        return parent;
    }

    void parent(int parent){
        this.parent = parent;
    }

    int depth(){
        return depth;
    }

    void depth(int depth){
        this.depth = depth;
    }

    String type(){
        return type;
    }

    void type(String type){
        this.type = type;
    }

    void addChild(int id){
        assert(id >= 0);
        children.add(id);
    }

    void removeChild(int id){
        assert(id >= 0);
        children.remove(id);
    }

    int inDegree(){
        return parent == -1 ? 0 : 1;
    }

    int outDegree(){
        return children.size();
    }

    int child(int i){
        return children.get(i);
    }

    @Override
    public String toString(){
        return String.format("node %d: parent = %d, depth = %d, %s, %s"
                , id, parent, depth, type, children.toString());
    }
}

public class Main{

    static void dfs(Node[] nodes, int i, int p, int d){
        Node n = nodes[i];
        n.parent(p);
        n.depth(d);
        if(d == 0){
            n.type("root");
        }
        else if(n.outDegree() == 0){
            n.type("leaf");
        }
        else{
            n.type("internal node");
        }

        for(int j = 0; j < n.outDegree(); j++){
            dfs(nodes, n.child(j), i, d+1);
        }
    }


    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);

        final int n = sc.nextInt();
        final Node[] nodes = new Node[n];
        final boolean hasInDegree[] = new boolean[n];

        for(int i = 0; i < n; i++){
            final int id = sc.nextInt();
            Node node = new Node(id);

            final int k = sc.nextInt();
            for(int j = 0; j < k; j++){
                final int c = sc.nextInt();
                hasInDegree[c] = true;
                node.addChild(c);
            }
            nodes[id] = node;
        }

        // search root node
        int root = -1;
        for(int i = 0; i < n; i++){
            if(!hasInDegree[i]){
                root = i;
                break;
            }
        }

        dfs(nodes, root, -1, 0);

        for(Node node : nodes){
            System.out.println(node);
        }
    }
}

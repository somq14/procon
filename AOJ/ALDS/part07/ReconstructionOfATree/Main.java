import java.util.Scanner;

class Main{
    static class Node{
        private final int id;
        private final Node left;
        private final Node right;

        Node(int id, Node left, Node right){
            this.id = id;
            this.left = left;
            this.right = right;
        }

        int id(){
            return id;
        }

        Node left(){
            return left;
        }

        Node right(){
            return right;
        }
    }

    static int[] a1;
    static int i;
    static Node reconstruction(int[] a2, int l, int r){
        if(r - l <= 0){
            return null;
        }
        int parentId = a1[i++];

        int m = -1;
        for(int j = l; j < r; j++){
            if(parentId == a2[j]){
                m = j;
                break;
            }
        }
        Node leftChild = reconstruction(a2, l, m);
        Node rightChild = reconstruction(a2, m+1, r);
        return new Node(parentId, leftChild, rightChild);
    }

    static void dfs(Node node, StringBuilder sb){
        if(node == null){
            return;
        }
        dfs(node.left(), sb);
        dfs(node.right(), sb);

        sb.append(node.id());
        sb.append(" ");
    }

    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);

        final int n = sc.nextInt();

        a1 = new int[n];
        for(int i = 0; i < n; i++){
            a1[i] = sc.nextInt();
        }

        int[] a2 = new int[n];
        for(int i = 0; i < n; i++){
            a2[i] = sc.nextInt();
        }

        i = 0;
        Node root = reconstruction(a2, 0, n);

        StringBuilder sb = new StringBuilder();
        dfs(root, sb);
        sb.setLength(sb.length()-1);
        System.out.println(sb);
    }
}

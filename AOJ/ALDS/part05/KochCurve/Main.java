import java.util.Scanner;

class Main{
    static void printPoint(double x, double y){
        System.out.println(String.format("%9.5f %9.5f", x, y));
    }
    static void printKochCurve(int n, double x1, double y1, double x2, double y2){
        if(n == 0){
            printPoint(x2, y2);
            return;
        }

        double vx = x2 - x1;
        double vy = y2 - y1;

        double ux = -vy;
        double uy = vx;

        double ax = x1 + vx/3;
        double ay = y1 + vy/3;

        double bx = x1 + vx/2 + (Math.sqrt(3) / 6) * ux;
        double by = y1 + vy/2 + (Math.sqrt(3) / 6) * uy;

        double cx = x1 + vx*2/3;
        double cy = y1 + vy*2/3;

        printKochCurve(n-1, x1, y1, ax, ay);
        printKochCurve(n-1, ax, ay, bx, by);
        printKochCurve(n-1, bx, by, cx, cy);
        printKochCurve(n-1, cx, cy, x2, y2);
    }

    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);

        int n = sc.nextInt();
        printPoint(0, 0);
        printKochCurve(n, 0, 0, 100, 0);
    }
}

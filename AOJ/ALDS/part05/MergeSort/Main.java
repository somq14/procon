import java.util.Scanner;

class Main{
    static int cnt = 0;
    
    static void merge(int[] a, int l, int m, int r){
        int nl = m - l;
        int nr = r - m;

        int[] al = new int[nl + 1];
        for(int i = 0; i < nl; i++){
            al[i] = a[l + i];
        }
        al[nl] = Integer.MAX_VALUE;

        int[] ar = new int[nr + 1];
        for(int i = 0; i < nr; i++){
            ar[i] = a[m + i];
        }
        ar[nr] = Integer.MAX_VALUE;

        int i = 0;
        int j = 0;
        for(int k = l; k < r; k++){
            cnt++;
            a[k] = (al[i] <= ar[j]) ? al[i++] : ar[j++];
        }
    }

    static void mergeSort(int[] a, int l, int r){
        if(r - l > 1){
            int m = (l + r) / 2;
            mergeSort(a, l, m);
            mergeSort(a, m, r);
            merge(a, l, m, r);
        }
    }


    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);

        int n = sc.nextInt();
        int[] a = new int[n];
        for(int i = 0; i < n; i++){
            a[i] = sc.nextInt();
        }

        mergeSort(a, 0, n);
        for(int i = 0; i < n; i++){
            if(i != 0){
                System.out.print(' ');
            }
            System.out.print(a[i]);
        }
        System.out.println();
        System.out.println(cnt);
    }
}

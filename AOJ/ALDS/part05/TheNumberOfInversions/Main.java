import java.util.Scanner;

class Main{
    static long cnt = 0;

    static void merge(int[] a, int l, int m, int r){

        int nl = m - l;
        int[] al = new int[nl];
        for(int i = 0; i < nl; i++){
            al[i] = a[l+i];
        }

        int nr = r - m;
        int[] ar = new int[nr];
        for(int i = 0; i < nr; i++){
            ar[i] = a[m+i];
        }

        int j = nl - 1;
        int k = r - 1;
        for(int i = nr - 1; i >= 0; i--){
            for(;j >= 0 && al[j] > ar[i]; j--){
                a[k--] = al[j];
            }
            cnt += (nl - 1) - j;
            a[k--] = ar[i];
        }
    }

    static void mergeSort(int[] a, int l, int r){
        if(r-l <= 1){
            return;
        }
        int m = (l + r) / 2;
        mergeSort(a, l, m);
        mergeSort(a, m, r);
        merge(a, l, m, r);
    }

    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int[] a = new int[n];
        for(int i = 0; i < n; i++){
            a[i] = sc.nextInt();
        }

        mergeSort(a, 0, n);

        // for(int i = 0; i < n; i++){
        //     System.out.print(a[i] + " ");
        // }
        System.out.println(cnt);
    }
}

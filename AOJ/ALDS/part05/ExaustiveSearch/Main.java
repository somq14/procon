import java.util.Scanner;

class Main{

    static boolean solve(int[] a, int n, int m, int sum, int d){
        if(d == n){
            return sum == m;
        }
        // prun
        if(sum > m){
            return false;
        }
        return solve(a, n, m, sum + a[d], d+1) || solve(a, n, m, sum, d+1);
    }

    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);

        final int n = sc.nextInt();
        int[] a = new int[n];
        for(int i = 0; i < n; i++){
            a[i] = sc.nextInt();
        }

        final int q = sc.nextInt();
        for(int i = 0; i < q; i++){
            int m = sc.nextInt();
            System.out.println(solve(a, n, m, 0, 0) ? "yes" : "no");
        }
    }
}

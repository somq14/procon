import java.util.Scanner;

class Main{

    static int insertionSort(int[] a, int n, int g){
        int cnt = 0;
        for(int i = g; i < n; i++){
            int v = a[i];
            int j = i - g;
            while(j >= 0 && a[j] > v){
                a[j+g] = a[j];
                j = j - g;
                cnt++;
            }
            a[j+g] = v;
        }
        return cnt;
    }

    static void shellSort(int[] a, int n){
        int cnt = 0;

        int g = 1;
        int m = 1;
        while(g * 2 < n){
            g *= 2;
            m++;
        }

        System.out.println(m);
        while(m > 0){
            System.out.print(g);
            if(m > 1){
                System.out.print(" ");
            }
            cnt += insertionSort(a, n, g);

            g /= 2;
            m--;
        }
        System.out.println();
        System.out.println(cnt);

        for(int i = 0; i < n; i++){
            System.out.println(a[i]);
        }
    }

    public static void main(String[] args){
        Scanner scn = new Scanner(System.in);

        int n = scn.nextInt();
        int[] a = new int[n];
        for(int i = 0; i < n; i++){
            a[i] = scn.nextInt();
        }

        shellSort(a, n);
    }
}

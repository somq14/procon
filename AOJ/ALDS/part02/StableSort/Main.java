import java.util.Scanner;

class Main{

    static void bubbleSort(String[] a, int n){
        for(int i = 0; i < n; i++){
            for(int j = n-1; j >= i+1; j--){
                if(a[j].charAt(1) < a[j-1].charAt(1)){
                    String tmp = a[j];
                    a[j] = a[j-1];
                    a[j-1] = tmp;
                }
            }
        }
    }
    static void selectionSort(String[] a, int n){
        for(int i = 0; i < n; i++){
            int min = i;
            for(int j = i; j < n; j++){
                if(a[j].charAt(1) < a[min].charAt(1)){
                    min = j;
                }
            }
            String tmp = a[min];
            a[min] = a[i];
            a[i] = tmp;
        }
    }


    static void printArray(String[] a, int n){
        for(int i = 0; i < n-1; i++){
            System.out.print(a[i] + " ");
        }
        System.out.println(a[n-1]);
    }

    public static void main(String[] args){
        Scanner scn = new Scanner(System.in);

        int n = scn.nextInt();
        String[] a = new String[n];
        for(int i = 0; i < n; i++){
            a[i] = scn.next();
        }

        String[] b = a.clone();
        bubbleSort(b, n);
        printArray(b, n);
        System.out.println("Stable");

        String[] c = a.clone();
        selectionSort(c, n);
        printArray(c, n);

        boolean equal = true;
        for(int i = 0; i < n; i++){
            if(!b[i].equals(c[i])){
                equal = false;
                break;
            }
        }
        System.out.println(equal ? "Stable" : "Not stable");
    }
}

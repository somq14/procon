import java.util.Scanner;

class Main{

    static int selectionSort(int[] a, int n){
        int cnt = 0;
        for(int i = 0; i < n; i++){
            int min = i;
            for(int j = i; j < n; j++){
                if(a[j] < a[min]){
                    min = j;
                }
            }
            if(min != i){
                int tmp = a[min];
                a[min] = a[i];
                a[i] = tmp;
                cnt++;
            }
        }
        return cnt;
    }

    public static void main(String[] args){
        Scanner scn = new Scanner(System.in);

        int n = scn.nextInt();
        int[] a = new int[n];
        for(int i = 0; i < n; i++){
            a[i] = scn.nextInt();
        }

        int cnt = selectionSort(a, n);

        for(int i = 0; i < n-1; i++){
            System.out.print(a[i] + " ");
        }
        System.out.println(a[n-1]);
        System.out.println(cnt);
    }
}

import java.util.Scanner;
class Main{

    static boolean isPrime(int n){
        if(n == 2){
            return true;
        }
        if(n % 2 == 0){
            return false;
        }
        for(int i = 3; i * i <= n; i += 2){
            if(n % i == 0){
                return false;
            }
        }
        return true;
    }

    public static void main(String[] args){
        Scanner scn = new Scanner(System.in);

        int n = scn.nextInt();

        int cnt = 0;
        for(int i = 0; i < n; i++){
            if(isPrime(scn.nextInt())){
                cnt++;
            }
        }
        System.out.println(cnt);
    }
}

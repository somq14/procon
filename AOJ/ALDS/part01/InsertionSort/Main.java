import java.util.Scanner;

class Main{

    static void printArray(int[] a, int n){
        for(int i = 0; i < n-1; i++){
            System.out.print(a[i] + " ");
        }
        System.out.println(a[n-1]);
    }

    static void insertSort(int[] a, int n){
        for(int i = 1; i < n; i++){
            printArray(a, n);

            int v = a[i];
            int j = i - 1;
            while(j >= 0 && a[j] > v){
                a[j + 1] = a[j];
                j--;
            }
            a[j + 1] = v;
        }
        printArray(a, n);
    }

    public static void main(String[] args){
        Scanner scn = new Scanner(System.in);

        final int n = scn.nextInt();

        int[] a = new int[n];
        for(int i = 0; i < n; i++){
            a[i] = scn.nextInt();
        }

        insertSort(a, n);
    }
}

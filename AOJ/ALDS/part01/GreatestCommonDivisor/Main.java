import java.util.Scanner;
class Main{
    public static void main(String[] args){
        Scanner scn = new Scanner(System.in);
        int x = scn.nextInt();
        int y = scn.nextInt();

        while(y > 0){
            int tmp = x % y;
            x = y;
            y = tmp;
        }

        System.out.println(x);
    }
}

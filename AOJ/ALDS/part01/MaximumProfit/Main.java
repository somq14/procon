import java.util.Scanner;

class Main{
    public static void main(String[] args){
        Scanner scn = new Scanner(System.in);

        int n = scn.nextInt();
        int[] r = new int[n];
        for(int i = 0; i < n; i++){
            r[i] = scn.nextInt();
        }

        int min = r[0];
        int profit = Integer.MIN_VALUE;
        for(int i = 1; i < n; i++){
            if(r[i] - min > profit){
                profit = r[i] - min;
            }
            if(r[i] < min){
                min = r[i];
            }
        }
        System.out.println(profit);
    }
}

import java.util.Scanner;

public class Main{

    static int solve(char[] x, int m, char[] y, int n){
        int[][] dp = new int[m+1][n+1];

        for(int i = 1; i <= m; i++){
            for(int j = 1; j <= n; j++){
                dp[i][j] = (x[i-1] == y[j-1]) ? dp[i-1][j-1] + 1 : Math.max(dp[i-1][j], dp[i][j-1]);
            }
        }
        return dp[m][n];
    }

    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);

        final int n = sc.nextInt();
        sc.nextLine();
        for(int i = 0; i < n; i++){
            String x = sc.next();
            String y = sc.next();
            int ans = solve(x.toCharArray(), x.length() ,y.toCharArray(), y.length());
            System.out.println(ans);
        }

    }
}

import java.util.Scanner;
import java.util.Arrays;

class Matrix{
    private int r;
    private int c;

    Matrix(int r, int c){
        this.r = r;
        this.c = c;
    }

    int row(){
        return r;
    }

    int col(){
        return c;
    }
}

public class Main{

    static int solve(Matrix m[], int n){
        int[][] dp = new int[n][n+1];

        for(int len = 2; len <= n; len++){ // length
            for(int beginIdx = 0; (beginIdx + len) <= n; beginIdx++){ // begin point
                final int endIdx = beginIdx + len - 1;
                // divide the sequence
                int min = Integer.MAX_VALUE;
                for(int p = 1; p < len; p++){
                    int leftCnt = dp[beginIdx][p];
                    int rightCnt = dp[beginIdx + p][len - p];
                    int mulCnt = m[beginIdx].row() * m[beginIdx + p - 1].col() * m[endIdx].col();
                    int cnt = leftCnt + mulCnt + rightCnt;
                    if(cnt < min){
                        min = cnt;
                    }
                }
                dp[beginIdx][len] = min;
            }
        }

        return dp[0][n];
    }

    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);

        final int n = sc.nextInt();
        final Matrix[] m = new Matrix[n];

        for(int i = 0; i < n; i++){
            int r = sc.nextInt();
            int c = sc.nextInt();
            m[i] = new Matrix(r, c);
        }
        System.out.println(solve(m, n));
    }
}

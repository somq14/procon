import java.util.Scanner;
import java.util.Collections;
import java.util.List;
import java.util.ArrayList;

class Edge implements Comparable<Edge>{
    final int u;
    final int v;
    final int w;
    Edge(int u, int v, int w){
        this.u = u;
        this.v = v;
        this.w = w;
    }
    int getU(){
        return u;
    }
    int getV(){
        return v;
    }

    int getW(){
        return w;
    }

    @Override
    public int compareTo(Edge e){
        if(w > e.w){
            return 1;
        }
        else if(w < e.w){
            return -1;
        }
        return 0;
    }
}

public class Main{

    // Union-Find
    static void init(int[] a, int n){
        for(int i = 0; i < n; i++){
            a[i] = -1;
        }
    }

    static void union(int[] a, int s, int t){
        int sRoot = find(a, s);
        int tRoot = find(a, t);

        if(sRoot == tRoot){
            return;
        }

        if(a[sRoot] < a[tRoot]){
            a[tRoot] = sRoot;
            return;
        }

        if(a[sRoot] > a[tRoot]){
            a[sRoot] = tRoot;
            return;
        }

        assert(a[sRoot] == a[tRoot]);
        a[sRoot] = tRoot;
        a[tRoot]--;
    }

    static int find(int[] a, int v){
        if(a[v] < 0){
            return v;
        }
        else{
            a[v] = find(a, a[v]);
            return a[v];
        }
    }


    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);

        final int n = sc.nextInt();
        List<Edge> edgeList = new ArrayList<>();
        for(int i = 0; i < n; i++){
            for(int j = 0; j < n; j++){
                int w = sc.nextInt();
                if(w != -1 && i <= j){
                    // edge i, j
                    edgeList.add(new Edge(i, j, w));
                }
            }
        }
        Collections.sort(edgeList);

        int[] uf = new int[n];
        init(uf, n);

        int sumW = 0;
        int edgeCnt = 0;
        for(Edge e : edgeList){
            int uRoot = find(uf, e.getU());
            int vRoot = find(uf, e.getV());
            if(uRoot != vRoot){
                // adopt it
                union(uf, uRoot, vRoot);
                sumW += e.getW();
                edgeCnt++;
                if(edgeCnt >= n - 1){
                    break;
                }
            }
        }
        System.out.println(sumW);
    }
}

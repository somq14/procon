import java.util.Scanner;

class Heap{
    private final int maxSize;
    private final int[] heap;
    private final int[] indices;
    private final int[] value;
    private int size;

    Heap(int maxSize){
        this.maxSize = maxSize;
        this.indices = new int[maxSize];
        this.value = new int[maxSize];
        this.heap = new int[maxSize+1];
        this.size = 0;
        java.util.Arrays.fill(indices, -1);
    }

    int size(){
        return size;
    }

    boolean isEmpty(){
        return size == 0;
    }

    private int left(int i){
        return i * 2;
    }

    private int right(int i){
        return i * 2 + 1;
    }

    private int parent(int i){
        return i / 2;
    }

    private void percolateUp(int id){
        int i = indices[id];
        int v = value[id];
        while(i > 1){
            int j = parent(i);
            int p = heap[j];
            if(value[p] < v){
                break;
            }
            indices[p] = i;
            heap[i] = p;
            i = j;
        }
        heap[i] = id;
        indices[id] = i;
    }

    private void percolateDown(int id){
        int i = indices[id];
        while(true){
            int l = left(i);
            int r = right(i);

            int minInd = i;
            if(l <= size && value[heap[l]] < value[heap[minInd]]){
                minInd = l;
            }

            if(r <= size && value[heap[r]] < value[heap[minInd]]){
                minInd = r;
            }

            if(minInd == i){
                break;
            }

            int minId = heap[minInd];
            heap[i] = minId;
            indices[minId] = i;

            i = minInd;
        }
        heap[i] = id;
        indices[id] = i;
    }

    void insert(int id, int val){
        size++;
        heap[size] = id;
        indices[id] = size;
        value[id] = val;
        percolateUp(id);
    }

    int remove(int id){
        if(indices[id] == -1){
            return id;
        }

        int moveId = heap[size];
        if(moveId == id){
            size--;
            return id;
        }

        heap[indices[id]] = moveId;
        indices[moveId] = indices[id];
        indices[id] = -1;
        size--;
        percolateDown(moveId);
        return id;
    }

    int removeMin(){
        return remove(heap[1]);
    }
}

public class Main{

    static final int INF = 2000 * 1000 * 1000;

    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);

        final int n = sc.nextInt();
        final int[][] neighbor = new int[n][];
        final int[][] weight = new int[n][];

        for(int i = 0; i < n; i++){
            int id = sc.nextInt();
            int degree = sc.nextInt();
            neighbor[id] = new int[degree];
            weight[id] = new int[degree];
            for(int j = 0; j < degree; j++){
                int v = sc.nextInt();
                int c = sc.nextInt();
                neighbor[id][j] = v;
                weight[id][j] = c;
            }
        }

        Heap h = new Heap(n);
        int[] dist = new int[n];

        h.insert(0, 0);
        dist[0] = 0;
        for(int i = 1; i < n; i++){
            h.insert(i, INF);
            dist[i] = INF;
        }

        while(!h.isEmpty()){
            final int v = h.removeMin();
            final int degree = neighbor[v].length;

            //System.out.println("v : " + v);
            for(int i = 0; i < degree; i++){
                final int u = neighbor[v][i];
                //System.out.println("u : " + u);
                final int d = dist[v] + weight[v][i];
                if(d < dist[u]){
                    dist[u] = d;
                    h.remove(u);
                    h.insert(u, d);
                }
            }
        }

        for(int i = 0; i < n; i++){
            System.out.println(String.format("%d %d", i, dist[i]));
        }
    }
}

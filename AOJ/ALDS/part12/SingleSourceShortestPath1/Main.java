import java.util.Scanner;

public class Main{

    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);

        final int n = sc.nextInt();
        final int[][] g = new int[n][n];
        for(int i = 0; i < n; i++){
            for(int j = 0; j < n; j++){
                g[i][j] = -1; // no edge
            }
        }

        for(int i = 0; i < n; i++){
            int id = sc.nextInt();
            int degree = sc.nextInt();
            for(int j = 0; j < degree; j++){
                int v = sc.nextInt();
                int c = sc.nextInt();
                g[id][v] = c;
            }
        }

        int[] dist = new int[n];
        for(int i = 0; i < n; i++){
            dist[i] = Integer.MAX_VALUE;
        }
        dist[0] = 0;

        boolean[] visited = new boolean[n];
        while(true){
            int v = -1;
            int min = Integer.MAX_VALUE;
            for(int i = 0; i < n; i++){
                if(!visited[i] && dist[i] <= min){
                    v = i;
                    min = dist[i];
                }
            }

            if(v == -1){
                break;
            }

            visited[v] = true;
            for(int i = 0; i < n; i++){
                if(g[v][i] > 0 && dist[v] + g[v][i] < dist[i]){
                    dist[i] = dist[v] + g[v][i];
                }
            }
        }

        for(int i = 0; i < n; i++){
            System.out.println(String.format("%d %d", i, dist[i]));
        }
    }
}

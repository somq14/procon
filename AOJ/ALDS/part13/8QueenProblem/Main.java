import java.util.Scanner;

public class Main{

    static boolean possibleToAttack(int x1, int y1, int x2, int y2){
        if(x1 == x2 || y1 == y2){
            return true;
        }
        int dx = x2 - x1;
        int dy = y2 - y1;
        return Math.abs(dx) == Math.abs(dy);
    }

    static boolean solve(int[] x, int[] y, int n){
        if(n == 8){
            // print
            boolean[][] b = new boolean[8][8];
            for(int i = 0; i < n; i++){
                b[x[i]][y[i]] = true;
            }
            for(int i = 0; i < 8; i++){
                for(int j = 0; j < 8; j++){
                    System.out.print(b[i][j] ? 'Q' : '.');
                }
                System.out.println();
            }
            return true;
        }
        for(int i = 0; i < 8; i++){
            for(int j = 0; j < 8; j++){
                boolean possibleToPlace = true;
                for(int k = 0; k < n; k++){
                    if(possibleToAttack(i, j, x[k], y[k])){
                        possibleToPlace = false;
                        break;
                    }
                }
                if(possibleToPlace){
                    x[n] = i;
                    y[n] = j;
                    if(solve(x, y, n+1)){
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);

        final int k = sc.nextInt();
        final int[] x = new int[8];
        final int[] y = new int[8];
        for(int i = 0; i < k; i++){
            x[i] = sc.nextInt();
            y[i] = sc.nextInt();
        }

        solve(x, y, k);
    }
}

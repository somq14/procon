import java.util.Scanner;
import java.util.Set;
import java.util.HashSet;
import java.util.Queue;
import java.util.ArrayDeque;

class Puzzle{
    private final long pat;
    private final int emptyPos;

    Puzzle(long pat, int emptyPos){
        this.pat = pat;
        this.emptyPos = emptyPos;
    }

    private static long swap(long pat, int zeroInd, int otherInd){
        long other = (pat >>> (otherInd * 4)) & 0xFL;
        return (pat | (other << (zeroInd * 4))) & ~(0xFL << (otherInd * 4));
    }

    Puzzle moveToLeft(){
        if(emptyPos % 3 == 0){
            return null;
        }
        int swapPos = emptyPos - 1;
        return new Puzzle(swap(pat, emptyPos, swapPos), swapPos);
    }

    Puzzle moveToRight(){
        if(emptyPos % 3 == 2){
            return null;
        }
        int swapPos = emptyPos + 1;
        return new Puzzle(swap(pat, emptyPos, swapPos), swapPos);
    }

    Puzzle moveToUpper(){
        if(emptyPos / 3 == 0){
            return null;
        }
        int swapPos = emptyPos - 3;
        return new Puzzle(swap(pat, emptyPos, swapPos), swapPos);
    }

    Puzzle moveToLower(){
        if(emptyPos / 3 == 2){
            return null;
        }
        int swapPos = emptyPos + 3;
        return new Puzzle(swap(pat, emptyPos, swapPos), swapPos);
    }

    @Override
    public boolean equals(Object obj){
        if(!(obj instanceof Puzzle)){
            return false;
        }
        Puzzle p = (Puzzle)obj;
        return pat == p.pat;
    }

    @Override
    public int hashCode(){
        return (int)pat;
    }

    @Override
    public String toString(){
        StringBuilder sb = new StringBuilder(12);

        long w = pat;
        sb.append((w >>>  0) & 0xF);
        sb.append((w >>>  4) & 0xF);
        sb.append((w >>>  8) & 0xF);
        sb.append('\n');
        sb.append((w >>> 12) & 0xF);
        sb.append((w >>> 16) & 0xF);
        sb.append((w >>> 20) & 0xF);
        sb.append('\n');
        sb.append((w >>> 24) & 0xF);
        sb.append((w >>> 28) & 0xF);
        sb.append((w >>> 32) & 0xF);
        sb.append('\n');
        return sb.toString();
    }
}

class Main{

    static int bfs(Puzzle initPuzzle, Puzzle finalPuzzle){
        Set<Puzzle> visited = new HashSet<>();
        Queue<Puzzle> queue = new ArrayDeque<>();
        Queue<Puzzle> tmp = new ArrayDeque<>();

        queue.offer(initPuzzle);
        visited.add(initPuzzle);

        int depth = 0;
        while(!queue.isEmpty()){
            tmp.clear();
            while(!queue.isEmpty()){
                Puzzle v = queue.poll();
                if(v.equals(finalPuzzle)){
                    return depth;
                }

                Puzzle u;
                u = v.moveToLeft();
                if(u != null && !visited.contains(u)){
                    visited.add(u);
                    tmp.offer(u);
                }
                u = v.moveToRight();
                if(u != null && !visited.contains(u)){
                    visited.add(u);
                    tmp.offer(u);
                }
                u = v.moveToUpper();
                if(u != null && !visited.contains(u)){
                    visited.add(u);
                    tmp.offer(u);
                }
                u = v.moveToLower();
                if(u != null && !visited.contains(u)){
                    visited.add(u);
                    tmp.offer(u);
                }
            }
            queue.addAll(tmp);
            depth++;
        }
        return -1;
    }

    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);

        long p = 0L;
        int e = 8;
        p |= (1L <<  0);
        p |= (2L <<  4);
        p |= (3L <<  8);
        p |= (4L << 12);
        p |= (5L << 16);
        p |= (6L << 20);
        p |= (7L << 24);
        p |= (8L << 28);
        p |= (0L << 32);
        Puzzle finalPuzzle = new Puzzle(p, e);

        p = 0L;
        e = -1;
        for(int i = 0; i < 9; i++){
            long v = sc.nextLong();
            if(v == 0){
                e = i;
            }
            p |= v <<  (4 * i);
        }
        Puzzle initPuzzle = new Puzzle(p, e);

        System.out.println(bfs(initPuzzle, finalPuzzle));
    }
}

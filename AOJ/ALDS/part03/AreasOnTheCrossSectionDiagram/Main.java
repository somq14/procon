import java.util.Scanner;

class Main{
    static final int MAX_LEN = 20000;

    private static class Stack<E>{
        private final E[] stack;
        private int sp;

        @SuppressWarnings("unchecked")
        Stack(int size){
            this.stack = (E[])new Object[size];
            this.sp = 0;
        }

        int size(){
            return sp;
        }

        void push(E v){
            stack[sp++] = v;
        }

        E pop(){
            return stack[--sp];
        }

        E peek(){
            return stack[sp-1];
        }
    }

    private static class Pair<K, V>{
        private final K k;
        private final V v;

        Pair(K key, V val){
            this.k = key;
            this.v = val;
        }

        K key(){
            return k;
        }

        V val(){
            return v;
        }
    }

    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);

        Stack<Integer> downHill = new Stack<>(MAX_LEN);

        // beginPos, size
        Stack<Pair<Integer, Integer>> pools = new Stack<>(MAX_LEN);

        int x = 0;
        for(char c : sc.nextLine().toCharArray()){
            switch(c){
                case '\\':
                    downHill.push(x);
                    break;

                case '/' :
                    if(downHill.size() > 0){
                        final int begin = downHill.pop();
                        final int end = x;

                        int poolSize = end - begin;
                        while(pools.size() > 0 && pools.peek().key() > begin){
                            poolSize += pools.pop().val();
                        }

                        pools.push(new Pair<>(begin, poolSize));
                    }
                    break;

                case '_' :
                    break;

                default:
                    throw new IllegalStateException();
            }
            x++;
        }

        Stack<Integer> toReverse = new Stack<>(MAX_LEN);
        int sum = 0;
        while(pools.size() > 0){
            int v = pools.pop().val();
            sum += v;
            toReverse.push(v);
        }


        System.out.println(sum);

        System.out.print(toReverse.size());
        while(toReverse.size() > 0){
            System.out.print(' ');
            System.out.print(toReverse.pop());
        }
        System.out.println();
    }
}

import java.util.Scanner;

class Main{

    static class Proc{
        final String name;
        int time;

        Proc(String name, int time){
            this.name = name;
            this.time = time;
        }
    }

    static Proc[] queue = new Proc[100001];
    static int head = 0;
    static int tail = 0;

    static void enque(Proc p){
        queue[tail] = p;
        tail = (tail + 1) % queue.length;
    }

    static Proc deque(){
        Proc p = queue[head];
        head = (head + 1) % queue.length;
        return p;
    }

    static int size(){
        return (queue.length + tail - head) % queue.length;
    }

    public static void main(String[] args){
        Scanner scn = new Scanner(System.in);
        int n = scn.nextInt();
        int q = scn.nextInt();

        for(int i = 0; i < n; i++){
            String name = scn.next();
            int time = scn.nextInt();
            enque(new Proc(name, time));
        }

        int curTime = 0;
        while(size() > 0){
            Proc p = deque();

            if(p.time <= q){
                curTime += p.time;
                p.time = 0;
                System.out.println(p.name + " " + curTime);
            }
            else{
                curTime += q;
                p.time -= q;
                enque(p);
            }
        }
    }
}

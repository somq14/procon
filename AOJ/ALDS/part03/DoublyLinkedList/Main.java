import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.BufferedOutputStream;

class Main{
    static BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
    static PrintWriter out = new PrintWriter(new BufferedOutputStream(System.out));

    static class Node{
        Node next;
        Node prev;
        final int x;

        Node(int x){
            this.x = x;
        }
    }

    static Node head;
    static Node tail;
    static{
        head = new Node(-1);
        tail = new Node(-1);

        head.next = tail;
        head.prev = null;

        tail.next = null;
        tail.prev = head;
    }

    static void insert(int x){
        Node n = new Node(x);
        n.next = head.next;
        n.prev = head;

        head.next.prev = n;
        head.next = n;
    }

    static void delete(int x){
        Node p = head;
        while(p != null){
            if(p.x == x){
                p.next.prev = p.prev;
                p.prev.next = p.next;
                break;
            }
            p = p.next;
        }
    }

    static void deleteFirst(){
        head.next = head.next.next;
        head.next.prev = head;
    }

    static void deleteLast(){
        tail.prev = tail.prev.prev;
        tail.prev.next = tail;
    }

    static void printList(){
        Node p = head.next;
        while(p != tail){
            out.print(p.x);
            if(p.next != tail){
                out.print(" ");
            }
            p = p.next;
        }
        out.println();
    }

    static void printListRev(){
        Node p = tail.prev;
        while(p != head){
            out.print(p.x);
            if(p.prev != head){
                out.print(" ");
            }
            p = p.prev;
        }
        out.println();
    }

    public static void main(String[] args) throws Exception{
        int n = Integer.parseInt(in.readLine());

        for(int i = 0; i < n; i++){
            String[] cmd = in.readLine().split(" ");
            if(cmd[0].equals("insert")){
                insert(Integer.parseInt(cmd[1]));
            }
            else if(cmd[0].equals("deleteFirst")){
                deleteFirst();
            }
            else if(cmd[0].equals("deleteLast")){
                deleteLast();
            }
            else if(cmd[0].equals("delete")){
                delete(Integer.parseInt(cmd[1]));
            }
        }

        printList();
        out.flush();
    }
}

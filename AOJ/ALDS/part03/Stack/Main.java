import java.util.Scanner;

class Main{
    static int[] stack = new int[100];
    static int sp = 0;

    static void push(int x){
        stack[sp++] = x;
    }
    static int pop(){
        return stack[--sp];
    }

    public static void main(String[] args){
        Scanner scn = new Scanner(System.in);
        String[] token = scn.nextLine().split(" ");


        for(String t : token){
            if(t.equals("+")){
                int a = pop();
                int b = pop();
                push(b + a);
            }
            else if(t.equals("-")){
                int a = pop();
                int b = pop();
                push(b - a);
            }
            else if(t.equals("*")){
                int a = pop();
                int b = pop();
                push(b * a);
            }
            else{
                int v = Integer.parseInt(t);
                push(v);
            }
        }
        System.out.println(pop());
    }
}

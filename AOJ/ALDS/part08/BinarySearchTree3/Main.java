import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.BufferedWriter;
import java.io.PrintWriter;
import java.io.OutputStreamWriter;


class Tree{
    private int v;
    private Tree right;
    private Tree left;

    Tree(int v){
        this.v = v;
    }

    int val(){
        return v;
    }

    void val(int v){
        this.v = v;
    }

    Tree right(){
        return right;
    }

    void right(Tree right){
        this.right = right;
    }

    Tree left(){
        return left;
    }

    void left(Tree left){
        this.left = left;
    }
}


public class Main{
    static BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    static PrintWriter pw = new PrintWriter(new BufferedWriter(new OutputStreamWriter(System.out)));

    static Tree insert(Tree t, int v){
        if(t == null){
            return new Tree(v);
        }

        Tree p = t;
        Tree q = null; // parent of p
        while(p != null){
            q = p;
            p = v < p.val() ? p.left() : p.right();
        }

        if(v < q.val()){
            q.left(new Tree(v));
        }
        else{
            q.right(new Tree(v));
        }

        return t;
    }

    static void print(Tree t, int pos){
        if(t == null){
            return;
        }
        if(pos == 0){
            pw.print(' ');
            pw.print(t.val());
        }
        print(t.left(), pos);
        if(pos == 1){
            pw.print(' ');
            pw.print(t.val());
        }
        print(t.right(), pos);
        if(pos == 2){
            pw.print(' ');
            pw.print(t.val());
        }
    }

    static boolean find(Tree t, int v){
        if(t == null){
            return false;
        }
        if(t.val() == v){
            return true;
        }
        if(v < t.val()){
            return find(t.left(), v);
        }
        else{
            return find(t.right(), v);
        }
    }

    static Tree delete(Tree t, int v){
        if(t == null){
            return null;
        }

        if(v < t.val()){
            t.left(delete(t.left(), v));
            return t;
        }

        if(v > t.val()){
            t.right(delete(t.right(), v));
            return t;
        }

        assert(t.val() == v);
        // delete it
        if(t.left() == null){
            return t.right();
        }

        if(t.right() == null){
            return t.left();
        }

        Tree p = t.right();
        while(p.left() != null){
            p = p.left();
        }
        t.val(p.val());
        t.right(delete(t.right(), p.val()));
        return t;
    }

    public static void main(String[] args) throws Exception{
        int n = Integer.parseInt(br.readLine());

        Tree root = null;
        for(int i = 0; i < n; i++){
            String[] cmd = br.readLine().split(" ");
            switch(cmd[0]){
                case "insert":
                    root = insert(root, Integer.parseInt(cmd[1]));
                    break;
                case "find":
                    pw.println(find(root, Integer.parseInt(cmd[1])) ? "yes" : "no");
                    break;
                case "delete":
                    root = delete(root, Integer.parseInt(cmd[1]));
                    break;
                case "print":
                    print(root, 1);
                    pw.println();

                    print(root, 0);
                    pw.println();
                    break;
                default:
                    throw new IllegalStateException();
            }
        }

        pw.flush();
    }
}

#include <algorithm>
#include <iostream>
#include <queue>
#include <string>
#include <utility>
#include <vector>

using std::cin;
using std::cout;
using std::endl;

using std::to_string;

using std::sort;
using std::min;
using std::max;

using std::vector;

using std::pair;
using std::make_pair;

using std::priority_queue;

/**************************************************************/
/**************************************************************/

const int INF = 1e9;

class edge {
   public:
    int u, v;
    int cost;

    edge() : u(0), v(0), cost(INF) {}
    edge(int u, int v, int cost) : u(u), v(v), cost(cost) {}

    bool operator<(const edge& e) const { return cost < e.cost; }
};

class unionfind {
   private:
    vector<int> a;
    vector<int> h;

   public:
    unionfind(int n) : a(n, -1), h(n) {}

    int find(int v) {
        if (a[v] < 0) {
            return v;
        }
        return a[v] = find(a[v]);
    }

    bool same(int u, int v) { return find(u) == find(v); }

    void unite(int u, int v) {
        const int urt = find(u);
        const int vrt = find(v);

        if (urt == vrt) {
            return;
        }

        if (h[urt] > h[vrt]) {
            a[vrt] = urt;
        } else if (h[urt] < h[vrt]) {
            a[urt] = vrt;
        } else {
            a[vrt] = urt;
            h[urt]++;
        }
    }
};

vector<edge> kruskal(size_t n, const vector<edge>& g) {
    vector<edge> sorted_e(g);
    sort(sorted_e.begin(), sorted_e.end());

    unionfind uf(n);
    vector<edge> st;
    for (const edge& e : sorted_e) {
        if (st.size() >= n) {
            break;
        }
        if (!uf.same(e.u, e.v)) {
            uf.unite(e.u, e.v);
            st.push_back(e);
        }
    }
    return st;
}

int main(void) {
    int n, m;
    cin >> n >> m;

    vector<edge> g(n);
    for (int i = 0; i < m; i++) {
        int s, t, w;
        cin >> s >> t >> w;
        g.push_back(edge(s, t, w));
    }

    vector<edge> ans = kruskal(n, g);
    int sum = 0;
    for (const edge& e : ans) {
        sum += e.cost;
    }
    cout << sum << endl;

    return 0;
}

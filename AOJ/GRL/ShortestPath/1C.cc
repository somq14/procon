#include <algorithm>
#include <iostream>
#include <queue>
#include <string>
#include <utility>
#include <vector>

using std::cin;
using std::cout;
using std::endl;

using std::to_string;

using std::min;
using std::max;

using std::vector;

using std::pair;
using std::make_pair;

using std::priority_queue;

/**************************************************************/
/**************************************************************/

const long INF = 1e18;

vector<vector<long>> warshallfloyd(const vector<vector<long>>& g) {
    vector<vector<long>> d(g);

    const int n = g.size();
    for (int i = 0; i < n; i++) {
        d[i][i] = 0;
    }

    for (int k = 0; k < n; k++) {
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if(d[i][k] == INF || d[k][j] == INF){
                    continue;
                }
                d[i][j] = min(d[i][j], d[i][k] + d[k][j]);
            }
        }
    }

    for (int i = 0; i < n; i++) {
        if (d[i][i] < 0) {
            return vector<vector<long>>(0);
        }
    }

    return d;
}

int main(void) {
    int n, m;
    cin >> n >> m;

    vector<vector<long>> g(n, vector<long>(n, INF));
    for (int i = 0; i < m; i++) {
        int s, t, d;
        cin >> s >> t >> d;
        g[s][t] = d;
    }

    vector<vector<long>> dist = warshallfloyd(g);
    if (dist.empty()) {
        cout << "NEGATIVE CYCLE" << endl;
    } else {
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                long d = dist[i][j];
                cout << (d >= INF ? "INF" : to_string(d));
                if(j != n - 1){
                    cout << " ";
                }
            }
            cout << endl;
        }
    }

    return 0;
}

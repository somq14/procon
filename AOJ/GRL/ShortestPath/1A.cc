#include <algorithm>
#include <iostream>
#include <queue>
#include <string>
#include <utility>
#include <vector>

using std::cin;
using std::cout;
using std::endl;

using std::to_string;

using std::min;
using std::max;

using std::vector;

using std::pair;
using std::make_pair;

using std::priority_queue;

/**************************************************************/
/**************************************************************/

const int INF = 1e9;

struct edge {
    int to;
    int cost;

    edge() : to(0), cost(0) {}
    edge(int to, int cost = INF) : to(to), cost(cost) {}
};

typedef vector<vector<edge>> graph;

vector<int> dijkstra(const graph& g, int s) {
    const int n = g.size();

    vector<int> d(n, INF);
    d[s] = 0;

    typedef pair<int, int> P;
    priority_queue<P, vector<P>, std::greater<P>> q;
    for (int i = 0; i < n; i++) {
        q.push(make_pair(INF, i));
    }
    q.push(make_pair(0, s));

    while (!q.empty()) {
        const int v = q.top().second;
        const int c = q.top().first;
        q.pop();

        if (c > d[v]) {
            continue;
        }

        for (const edge e : g[v]) {
            const int alt = d[v] + e.cost;
            if (alt < d[e.to]) {
                d[e.to] = alt;
                q.push(make_pair(alt, e.to));
            }
        }
    }

    return d;
}

int main(void) {
    int n, m, r;
    cin >> n >> m >> r;

    graph g(n);
    for (int i = 0; i < m; i++) {
        int s, t, c;
        cin >> s >> t >> c;
        g[s].push_back(edge(t, c));
    }

    vector<int> dist = dijkstra(g, r);

    for (int c : dist) {
        cout << (c >= INF ? "INF" : to_string(c)) << endl;
    }

    return 0;
}

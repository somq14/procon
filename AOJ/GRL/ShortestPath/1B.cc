#include <algorithm>
#include <iostream>
#include <queue>
#include <string>
#include <utility>
#include <vector>

using std::cin;
using std::cout;
using std::endl;

using std::to_string;

using std::min;
using std::max;

using std::vector;

using std::pair;
using std::make_pair;

using std::priority_queue;

/**************************************************************/
/**************************************************************/

const int INF = 1e9;

struct edge {
    int u;
    int v;
    int cost;

    edge() : u(0), v(0), cost(0) {}
    edge(int u, int v, int cost = INF) : u(u), v(v), cost(cost) {}
};

vector<int> bellmanford(int n, const vector<edge>& g, int s) {
    vector<int> d(n, INF);
    d[s] = 0;

    for (int i = 0; i < n; i++) {
        bool update = false;
        for (const edge& e : g) {
            if (d[e.u] < INF && d[e.u] + e.cost < d[e.v]) {
                d[e.v] = d[e.u] + e.cost;
                update = true;
            }
        }

        if (!update) {
            break;
        }

        if (update && i == n - 1) {
            return vector<int>(0);
        }
    }

    return d;
}

int main(void) {
    int n, m, r;
    cin >> n >> m >> r;

    vector<edge> g;
    for (int i = 0; i < m; i++) {
        int s, t, d;
        cin >> s >> t >> d;
        g.push_back(edge(s, t, d));
    }

    vector<int> dist = bellmanford(n, g, r);
    if (dist.empty()) {
        cout << "NEGATIVE CYCLE" << endl;
    } else {
        for (int d : dist) {
            cout << (d == INF ? "INF" : to_string(d)) << endl;
        }
    }

    return 0;
}

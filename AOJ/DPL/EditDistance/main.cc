#include <algorithm>
#include <cmath>
#include <complex>
#include <functional>
#include <iomanip>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <string>
#include <utility>
#include <vector>

using namespace std;

using ll = long long int;
#define int ll

const int INF = 1e15 + 373;

#define rep(i, n) for (int i = 0; i < (int)(n); ++i)

template <typename T>
using vector2 = vector<vector<T>>;

template <typename T>
vector2<T> init_vector2(size_t n0, size_t n1, T e = T()) {
    return vector2<T>(n0, vector<T>(n1, e));
}

template <typename T>
using vector3 = vector<vector<vector<T>>>;

template <typename T>
vector3<T> init_vector3(size_t n0, size_t n1, size_t n2, T e = T()) {
    return vector3<T>(n0, vector2<T>(n1, vector<T>(n2, e)));
}

int readInt() {
    int ret;
    cin >> ret;
    return ret;
}

vector<int> readInt(int n) {
    vector<int> ret(n);
    rep(i, n) { cin >> ret[i]; }
    return ret;
}

string readString() {
    string ret;
    cin >> ret;
    return ret;
}

vector<string> readString(int n) {
    vector<string> ret(n);
    rep(i, n) { cin >> ret[i]; }
    return ret;
}

#ifdef ENABLE_DEBUG
#include "debug.h"
#else
#define debug(v) ;
#endif

int levenshteinDistance(const string& s1, const string& s2) {
    const int n1 = s1.size();
    const int n2 = s2.size();
    auto dp = init_vector2(n1 + 1, n2 + 1, 0LL);

    for (int i = 0; i <= n1; i++) {
        dp[i][0] = i;
    }
    for (int j = 0; j <= n2; j++) {
        dp[0][j] = j;
    }

    for (int i = 1; i <= n1; i++) {
        for (int j = 1; j <= n2; j++) {
            if (s1[i - 1] == s2[j - 1]) {
                dp[i][j] = dp[i - 1][j - 1];
                continue;
            }
            int opt = INF;
            opt = min(opt, dp[i][j - 1] + 1);
            opt = min(opt, dp[i - 1][j] + 1);
            opt = min(opt, dp[i - 1][j - 1] + 1);
            dp[i][j] = opt;
        }
    }
    return dp[n1][n2];
}

signed main() {
    string s1 = readString();
    string s2 = readString();
    cout << levenshteinDistance(s1, s2) << endl;
    return 0;
}

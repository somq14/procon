type LazySegmentTree[T] = object
  n: int
  e: T
  f: (T, T) -> T
  a: seq[T]
  lazy: seq[T]
  hasLazy: seq[bool]

proc initLazySegmentTree[T](n: int; e: T; f: (T, T) -> T): LazySegmentTree[T] =
  result.n = nextPowerOfTwo(n)
  result.e = e
  result.f = f

  result.a = newSeqWith(result.n * 2, e)
  result.hasLazy = newSeqWith(result.n * 2, false)
  result.lazy = newSeqWith(result.n * 2, 0) # ??

proc len[T](this: LazySegmentTree[T]): int =
  this.n

proc propagate[T](this: var LazySegmentTree[T]; i, s, t: int) =
  if not this.hasLazy[i]:
    return

  if t - s >= 2:
    this.hasLazy[i * 2] = true
    this.lazy[i * 2] += this.lazy[i]

    this.hasLazy[i * 2 + 1] = true
    this.lazy[i * 2 + 1] += this.lazy[i]

  this.a[i] += this.lazy[i] # ??
  this.hasLazy[i] = false
  this.lazy[i] = 0

proc add[T](this: var LazySegmentTree[T]; sq, tq, x: int; i, s, t: int) =
  this.propagate(i, s, t)

  if tq <= s or t <= sq:
    return

  if sq <= s and t <= tq:
    this.hasLazy[i] = true
    this.lazy[i] += x # ??
    this.propagate(i, s, t)
    return

  let m = s + (t - s) div 2
  this.add(sq, tq, x, i * 2, s, m)
  this.add(sq, tq, x, i * 2 + 1, m, t)
  this.a[i] = this.f(this.a[i * 2], this.a[i * 2 + 1])

proc add[T](this: var LazySegmentTree[T]; sq, tq, x: int) =
  this.add(sq, tq, x, 1, 0, this.n)

proc add[T](this: var LazySegmentTree[T]; i, x: int) =
  this.add(i, i + 1, x, 1, 0, this.n)

proc update[T](this: var LazySegmentTree[T], sq, tq: int; x: T; i, s, t: int) =
  this.propagate(i, s, t)

  if tq <= s or t <= sq:
    return

  if t - s == 1:
    this.a[i] = x
    return

  let m = s + (t - s) div 2
  this.update(sq, tq, x, i * 2, s, m)
  this.update(sq, tq, x, i * 2 + 1, m, t)
  this.a[i] = this.f(this.a[i * 2], this.a[i * 2 + 1])

proc update[T](this: var LazySegmentTree[T]; i: int; x: T) =
  this.update(i, i + 1, x, 1, 0, this.n)

proc query[T](this: var LazySegmentTree[T]; sq, tq: int; i, s, t: int): T =
  if tq <= s or t <= sq:
    return this.e

  if sq <= s and t <= tq:
    this.propagate(i, s, t)
    return this.a[i]

  this.propagate(i, s, t)
  let m = s + (t - s) div 2
  let vl = this.query(sq, tq, i * 2, s, m)
  let vh = this.query(sq, tq, i * 2 + 1, m, t)
  return this.f(vl, vh)

proc query[T](this: var LazySegmentTree[T]; sq, tq: int): T =
  this.query(sq, tq, 1, 0, this.n)

proc query[T](this: var LazySegmentTree[T]; i: int): T =
  this.query(i, i + 1, 1, 0, this.n)

proc `$`[T](this: var LazySegmentTree[T]): string =
  result = "["
  for i in 0..<this.n:
    let v = this.query(i)
    result = result & $v
    if i != this.n - 1:
      result = result & ", "
  result = result & "]"


type Edge = tuple[ frm, to, c: int ]

proc dijkstra(n: int; g: seq2[Edge]; s: int): seq[int] =
  var d = newSeq[int](n)
  d.fill(INF)
  d[s] = 0

  type P = tuple[c, v: int]
  var q = initPriorityQueue[P]((p1: P, p2: P) => p1.c < p2.c)
  q.enqueue((0, s))

  while q.len() > 0:
    let (c, v) = q.dequeue()
    if c > d[v]:
      continue
    for e in g[v]:
      let alt = d[v] + e.c
      if alt < d[e.to]:
        d[e.to] = alt
        q.enqueue((alt, e.to))

  return d

#------------------------------------------------------------------------------#

type Edge = tuple [ w, u, v: int ]

proc minimumSpanningTree(n: int; es: seq[Edge]): int =
  assert es.isSorted(cmp[Edge])
  var uft = initUnionFindTree(n)

  var sum = 0
  for e in es:
    if uft.same(e.u, e.v):
      continue
    uft.union(e.u, e.v)
    sum += e.w
  return sum

#------------------------------------------------------------------------------#

proc fordfulkerson(g: seq2[int]; s, t: int): int =
  proc dfs(g: var seq2[int]; v, t, f: int; visited: var seq[bool]): int =
    if v == t:
      return f

    visited[v] = true

    let n = g.len()
    for u in 0..<n:
      if g[v][u] <= 0 or visited[u]:
        continue

      let ret = dfs(g, u, t, min(f, g[v][u]), visited)
      if ret > 0:
        g[v][u] -= ret
        g[u][v] += ret
        return ret

    visited[v] = false

    return 0

  let n = g.len()
  var g = g

  result = 0
  var visited = newSeq[bool](n)

  while true:
    visited.fill(false)
    let flow = dfs(g, s, t, INF, visited)
    if flow == 0:
      break
    result += flow

#------------------------------------------------------------------------------#

type Edge = tuple [ u, v, w, r: int ]

proc addEdge(g: var seq2[Edge]; u, v, uvw, vuw: int) =
  let uvEdgeId = g[u].len()
  let vuEdgeId = g[v].len()
  g[u].add((u, v, uvw, vuEdgeId))
  g[v].add((v, u, vuw, uvEdgeId))

proc dinic(g: seq2[Edge]; s, t: int): int =

  proc bfs(g: var seq2[Edge]; s: int; dep: var seq[int]) =
    dep.fill(-1)

    var q = initQueue[int]()
    q.enqueue(s)
    dep[s] = 0

    while q.len() > 0:
      let u = q.dequeue()
      for e in g[u]:
        let v = e.v
        if dep[v] >= 0:
          continue
        if e.w > 0:
          dep[v] = dep[u] + 1
          q.enqueue(v)

  proc dfs(g: var seq2[Edge]; u, t, f: int; itr, dep: var seq[int]): int =
    if u == t:
      return f

    while itr[u] < g[u].len():
      let e = g[u][itr[u]]
      let v = e.v
      if e.w > 0 and dep[v] > dep[u]:
        let res = dfs(g, v, t, min(f, e.w), itr, dep)
        if res > 0:
          g[u][itr[u]].w -= res
          g[v][e.r].w += res
          return res
      itr[u] += 1

    return 0

  var g = g
  let n = g.len()

  var dep = newSeq[int](n)
  var itr = newSeq[int](n)

  var flow = 0
  while true:
    bfs(g, s, dep)

    if dep[t] == -1:
      break

    itr.fill(0)
    while true:
      let res = dfs(g, s, t, INF, itr, dep)
      if res <= 0:
        break
      flow += res

  return flow



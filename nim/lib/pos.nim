#------------------------------------------------------------------------------#
type Pos =
  tuple [ y, x: int ]

const R: Pos = (0, +1)
const L: Pos = (0, -1)
const U: Pos = (-1, 0)
const D: Pos = (+1, 0)
const dir = [ R, D, L, U ]

proc `+`(p1, p2: Pos): Pos =
  (p1.y + p2.y, p1.x + p2.x)

proc `-`(p1, p2: Pos): Pos =
  (p1.y - p2.y, p1.x - p2.x)

var H = INF
var W = INF

proc setArea(h, w: int) =
  H = h
  W = w

proc valid(p: Pos): bool =
  p.y in 0..<H and p.x in 0..<W

iterator neighbors(p: Pos): Pos =
  yield p + R
  yield p + D
  yield p + L
  yield p + U

proc `[]`[T](a: seq2[T]; p: Pos): T =
  a[p.y][p.x]

proc `[]=`[T](a: var seq2[T]; p: Pos; v: T) =
  a[p.y][p.x] = v

#------------------------------------------------------------------------------#

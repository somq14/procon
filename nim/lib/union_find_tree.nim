#------------------------------------------------------------------------------#
type UnionFindTree = object
  p: seq[int]
  h: seq[int]

proc initUnionFindTree(n: int): UnionFindTree =
  result.p = newSeq[int](n)
  result.p.fill(-1)
  result.h = newSeq[int](n)
  result.h.fill(0)

proc find(this: var UnionFindTree, v: int): int =
  if this.p[v] == -1:
    return v
  this.p[v] = find(this, this.p[v])
  return this.p[v]

proc same(this: var UnionFindTree, u, v: int): bool =
  this.find(u) == this.find(v)

proc union(this: var UnionFindTree, u, v: int) =
  var uRoot = this.find(u)
  var vRoot = this.find(v)
  if uRoot == vRoot:
    return

  if this.h[uRoot] < this.h[vRoot]:
    swap(uRoot, vRoot)

  this.p[vRoot] = uRoot
  if this.h[uRoot] == this.h[vRoot]:
    this.h[uRoot] += 1

#------------------------------------------------------------------------------#

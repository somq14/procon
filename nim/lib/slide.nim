# [i - w, i)の範囲の最大値
proc buildSlideMaximum(a: seq[int]; w: int): seq[int] =
  let n = a.len()

  var ans = newSeq[int](n + 1)

  var deq = initDeque[int](w)

  for i in 1..n:
    while deq.len() > 0 and a[deq.peekLast()] <= a[i - 1]:
      discard deq.popLast()
    deq.addLast(i - 1)

    while deq.len() > 0 and deq.peekFirst() < i - w:
      discard deq.popFirst()

    ans[i] = a[deq.peekFirst()]

  return ans


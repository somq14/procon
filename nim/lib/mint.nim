#------------------------------------------------------------------------------#
const MOD = int(10^9 + 7)

type Mint = object
  v: int

proc initMint(v: int): Mint =
  result.v = (v mod MOD + MOD) mod MOD

proc `$`(x: Mint): string =
  $x.v

proc `$$`(x: Mint): string =
  if abs(x.v) < abs(x.v - MOD):
    $x.v
  else:
    $(x.v - MOD)

proc pow(x: Mint; n: int): Mint =
  if n < 0:
    return pow(x, -n).pow(MOD - 2)

  var m = n
  var p = 1
  var xx = x.v
  while m > 0:
    if (m and 1) != 0:
      p = p * xx mod MOD
    xx = xx * xx mod MOD
    m = m shr 1

  result.v = p

proc inv(x: Mint): Mint =
  pow(x, MOD - 2)

proc `+`(x: Mint): Mint =
  result.v = x.v
proc `-`(x: Mint): Mint =
  result.v = (-x.v + MOD) mod MOD

proc `+`(x0: Mint, x1: Mint): Mint =
  result.v = (x0.v + x1.v) mod MOD
proc `-`(x0: Mint, x1: Mint): Mint =
  result.v = (x0.v - x1.v + MOD) mod MOD
proc `*`(x0: Mint, x1: Mint): Mint =
  result.v = x0.v * x1.v mod MOD
proc `/`(x0: Mint, x1: Mint): Mint =
  result.v = x0.v * x1.inv().v mod MOD
proc `^`(x0: Mint, x1: int): Mint =
  x0 ^ x1

proc `+`(x0: Mint, x1: int): Mint =
  x0 + initMint(x1)
proc `-`(x0: Mint, x1: int): Mint =
  x0 - initMint(x1)
proc `*`(x0: Mint, x1: int): Mint =
  x0 * initMint(x1)
proc `/`(x0: Mint, x1: int): Mint =
  x0 / initMint(x1)

proc `+`(x0: int, x1: Mint): Mint =
  initMint(x0) + x1
proc `-`(x0: int, x1: Mint): Mint =
  initMint(x0) - x1
proc `*`(x0: int, x1: Mint): Mint =
  initMint(x0) * x1
proc `/`(x0: int, x1: Mint): Mint =
  initMint(x0) / x1

proc `+=`(x0: var Mint; x1: Mint) =
  x0 = x0 + x1
proc `-=`(x0: var Mint; x1: Mint) =
  x0 = x0 - x1
proc `*=`(x0: var Mint; x1: Mint) =
  x0 = x0 * x1
proc `/=`(x0: var Mint; x1: Mint) =
  x0 = x0 / x1

proc `+=`(x0: var Mint; x1: int) =
  x0 = x0 + x1
proc `-=`(x0: var Mint; x1: int) =
  x0 = x0 - x1
proc `*=`(x0: var Mint; x1: int) =
  x0 = x0 * x1
proc `/=`(x0: var Mint; x1: int) =
  x0 = x0 / x1
proc `^=`(x0: var Mint; x1: int) =
  x0 = x0 ^ x1

#------------------------------------------------------------------------------#

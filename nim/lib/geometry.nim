#------------------------------------------------------------------------------#
type Pos = tuple [ x, y: float ]

proc `+`(p1, p2: Pos): Pos = (p1.x + p2.x, p1.y + p2.y)
proc `-`(p1, p2: Pos): Pos = (p1.x - p2.x, p1.y - p2.y)
proc dot(p1, p2: Pos): float = p1.x * p2.x + p1.y * p2.y
proc det(p1, p2: Pos): float = p1.x * p2.y - p1.y * p2.x
proc abs(p: Pos): float = sqrt(p.x * p.x + p.y * p.y)
proc arg(p: Pos): float = arctan2(p.y, p.x)
#------------------------------------------------------------------------------#

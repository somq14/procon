
proc buildCumTab(a: seq[int]): seq[int] =
  let n = a.len()
  var c = newSeq[int](n + 1)
  for i in 1..n:
    c[i] = c[i - 1] + a[i - 1]
  return c

# [s, t)
proc lookupCumTab(c: seq[int]; s, t: int): int = c[t] - c[s]


proc buildCumTab2(a: seq2[int]): seq2[int] =
  let h = a.len()
  let w = a[0].len()
  var c = newSeq2[int](h + 1, w + 1)
  for y in 1..h:
    for x in 1..w:
      c[y][x] = c[y - 1][x] + c[y][x - 1] - c[y - 1][x - 1] + a[y - 1][x - 1]
  return c

proc lookupCumTab2(c: seq2[int]; sy, sx, ty, tx: int): int =
  c[ty][tx] - c[sy][tx] - c[ty][sx] + c[sy][sx]

import tables
import future
import algorithm

#------------------------------------------------------------------------------#

proc compress[T](a: seq[T]): Table[T, int] =
  result = initTable[T, int]()
  for e in a:
    if e notin result:
      result[e] = result.len()

proc uniqued[T](a: seq[T]): seq[T] =
  if a.len() == 0:
    return @[]

  var a = a.sorted(cmp[T])
  result = @[ a[0] ]
  for e in a:
    if result[result.len() - 1] == e:
      continue
    result.add(e)

# require segment tree
proc numberOfInversion(a: seq[int]): int =
  let n = a.len()

  var ind = newSeq[int](n)
  for i in 0..<n:
    ind[a[i]] = i

  result = 0
  var segtree = initSegmentTree[int](n, 0, (x0: int, x1: int) => x0 + x1)
  for i in 0..<n:
    result += segtree.query(ind[i], n)
    segtree.update(ind[i], 1)

proc numberOfInversion[T](a, b: seq[T]): int =
  assert a.len() == b.len()
  assert a.sorted(cmp[T]) == b.sorted(cmp[T])

  let t = b.compress()
  let c = a.map((it: T) => t[it])
  return c.numberOfInversion()

#------------------------------------------------------------------------------#


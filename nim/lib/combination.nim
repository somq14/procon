#------------------------------------------------------------------------------#
const MOD = int(10^9+7)

type Op1[T] = proc(a: T, b: T): T
type Op2[T] = proc(a: T, b: T): T

proc repeatedSquares[T](x: T, n: Natural, e: T, f: Op2[T]): T =
    result = e
    var xx = f(x, e)
    var i = n
    while i > 0:
        if (i and 1) != 0:
            result = f(result, xx)
        xx = f(xx, xx)
        i = i shr 1

proc addM(a, b: Natural): Natural = (a + b) mod MOD
proc subM(a, b: Natural): Natural = (a - b + MOD) mod MOD
proc mulM(a, b: Natural): Natural = a * b mod MOD
proc powM(a, b: Natural): Natural = repeatedSquares[Natural](a, b, 1, `mulM`)
proc invM(a: Natural): Natural = a.powM(MOD - 2)
proc divM(a, b: Natural): Natural = a.mulM(invM(b))

var memoFactM: seq[Natural] = nil
var memoFactInvM: seq[Natural] = nil

proc buildFactTable(n: Natural) =
  memoFactM = newSeq[Natural](n + 1)
  memoFactM[0] = 1
  for i in 1..n:
    memoFactM[i] = memoFactM[i - 1].mulM(i)

  memoFactInvM = newSeq[Natural](n + 1)
  memoFactInvM[n] = invM(memoFactM[n])
  for i in countdown(n - 1, 0):
    memoFactInvM[i] = memoFactInvM[i + 1].mulM(i + 1)

buildFactTable(10^6)

proc factM(n: Natural): Natural = memoFactM[n]

proc factInvM(n: Natural): Natural = memoFactInvM[n]

proc combM(n, r: Natural): Natural =
  if r > n:
    return 0
  if r > n div 2:
    return combM(n, n - r)

  result = factM(n).mulM(factInvM(n - r)).mulM(factInvM(r))

proc multCombM(n, r: Natural): Natural = combM(n + r - 1, r - 1)

#------------------------------------------------------------------------------#

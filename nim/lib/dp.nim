proc longestIncreaseSequence(a: seq[int]): int =
  let n = a.len()

  var dp = newSeq[int](n + 1)
  dp.fill(INF)
  dp[0] = -1
  for i in 0..<n:
    # [lb, ub)
    var lb = 0
    var ub = n
    while ub - lb > 1:
      let mid = (ub + lb) div 2
      if dp[mid] < a[i]:
        lb = mid
      else:
        ub = mid
    dp[lb + 1] = a[i]

  var ans = -1
  for i in countdown(n, 0):
    if dp[i] != INF:
      ans = i
      break
  return ans

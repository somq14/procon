proc bit(i: int): int =
  1 shl i

proc `[]`(v, i: int): bool =
  (v and (1 shl i)) != 0

proc contains(v, i: int): bool =
  (v and (1 shl i)) != 0

proc bitwidth(v: int): int =
  var lb = -1
  var ub = 64
  # (lb, ub]
  while ub - lb > 1:
    let mid = lb + (ub - lb) div 2
    if v shr mid == 0:
      ub = mid
    else:
      lb = mid
  return ub

proc bitcount(v: int): int =
  var c = v
  c = (c and 0x5555555555555555.int) + ((c shr  1) and 0x5555555555555555.int);
  c = (c and 0x3333333333333333.int) + ((c shr  2) and 0x3333333333333333.int);
  c = (c and 0x0f0f0f0f0f0f0f0f.int) + ((c shr  4) and 0x0f0f0f0f0f0f0f0f.int);
  c = (c and 0x00ff00ff00ff00ff.int) + ((c shr  8) and 0x00ff00ff00ff00ff.int);
  c = (c and 0x0000ffff0000ffff.int) + ((c shr 16) and 0x0000ffff0000ffff.int);
  c = (c and 0x00000000ffffffff.int) + ((c shr 32) and 0x00000000ffffffff.int);
  return c

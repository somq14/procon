type Deque[T] = object
  a: seq[T]
  cap: int
  siz: int
  s: int
  t: int

proc initDeque[T](init: int = 16): Deque[T] =
  result.a = newSeq[T](init.nextPowerOfTwo())
  result.cap = init.nextPowerOfTwo()
  result.siz = 0
  result.s = 0
  result.t = 0

proc len[T](this: Deque[T]): int = this.siz

proc ensureCapacity[T](this: var Deque[T]) =
  if this.siz < this.cap - 1:
    return

  var aa = newSeq[int](this.cap * 2)
  for i in 0..<this.siz:
    aa[i] = this.a[(this.s + i) and (this.cap - 1)]

  this.a = aa
  this.cap = this.cap * 2
  this.siz = this.siz
  this.s = 0
  this.t = this.siz

proc addFirst[T](this: var Deque[T]; e: T) =
  this.ensureCapacity()
  this.s = (this.s - 1) and (this.cap - 1)
  this.a[this.s] = e
  this.siz += 1

proc addLast[T](this: var Deque[T]; e: T) =
  this.ensureCapacity()
  this.a[this.t] = e
  this.t = (this.t + 1) and (this.cap - 1)
  this.siz += 1

proc popFirst[T](this: var Deque[T]): T =
  if this.siz <= 0:
    raise newException(IndexError, "Deque Is Empty")
  let res = this.a[this.s]
  this.s = (this.s + 1) and (this.cap - 1)
  this.siz -= 1
  return res

proc popLast[T](this: var Deque[T]): T =
  if this.siz <= 0:
    raise newException(IndexError, "Deque Is Empty")
  this.t = (this.t - 1) and (this.cap - 1)
  this.siz -= 1
  return this.a[this.t]

proc peekFirst[T](this: Deque[T]): T =
  if this.siz <= 0:
    raise newException(IndexError, "Deque Is Empty")
  return this.a[this.s]

proc peekLast[T](this: Deque[T]): T =
  if this.siz <= 0:
    raise newException(IndexError, "Deque Is Empty")
  return this.a[(this.t - 1) and (this.cap - 1)]

proc clear[T](this: var Deque[T]) =
  this.s = 0
  this.t = 0
  this.siz = 0


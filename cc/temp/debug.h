#ifndef __DEBUG_H__
#define __DEBUG_H__

#define debug(v)                          \
    do {                                  \
        cerr << #v << " = " << v << endl; \
    } while (0)

template <typename T1, typename T2>
ostream& operator<<(ostream& os, const pair<T1, T2>& p) {
    os << "(" << p.first << ", " << p.second << ")";
    return os;
}

template <typename T>
ostream& operator<<(ostream& os, const vector<T>& v) {
    os << "[";
    for (size_t i = 0; i < v.size(); i++) {
        os << v[i];
        if (i != v.size() - 1) {
            os << ", ";
        }
    }
    os << "]";
    return os;
}

template <>
ostream& operator<<(ostream& os, const vector<int>& v) {
    os << "[";
    for (size_t i = 0; i < v.size(); i++) {
        os << setw(3) << v[i];
        if (i != v.size() - 1) {
            os << ",";
        }
    }
    os << "]";
    return os;
}

template <>
ostream& operator<<(ostream& os, const vector<string>& v) {
    os << "[" << endl;
    for (size_t i = 0; i < v.size(); i++) {
        os << "  " << v[i] << endl;
    }
    os << "]";
    return os;
}

template <typename T>
ostream& operator<<(ostream& os, const vector2<T>& v) {
    os << "[" << endl;
    for (size_t i = 0; i < v.size(); i++) {
        os << "  " << v[i];
        os << endl;
    }
    os << "]";
    return os;
}

#endif

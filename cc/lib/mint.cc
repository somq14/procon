// NOT VERIFIED

const int MOD = (int)(1e9 + 7);

class mint {
   private:
    int x;

    static int safe_mod(int x) { return (x % MOD + MOD) % MOD; }

   public:
    mint() : x(0) {}
    mint(int x) : x(safe_mod(x)) {}

    int get() const { return x; }
    void set(int x) { this->x = safe_mod(x); }

    mint add(mint x) const { return (this->x + x.x) % MOD; }
    mint sub(mint x) const { return (this->x - x.x + MOD) % MOD; }
    mint mul(mint x) const { return this->x * x.x % MOD; }
    mint div(mint x) const { return this->x * x.inv().x % MOD; }
    mint inv() const { return pow(MOD - 2); }

    mint pow(int y) const {
        if (y < 0) {
            return pow(-y).inv();
        }

        int res = 1;
        int xx = x;
        for (int yy = y; yy > 0; yy >>= 1) {
            if (yy & 1) {
                res = res * xx % MOD;
            }
            xx = xx * xx % MOD;
        }
        return res;
    }

    mint operator+() const { return this->x; }
    mint operator-() const { return -this->x; }
    mint operator+(mint x) const { return this->add(x); }
    mint operator-(mint x) const { return this->sub(x); }
    mint operator*(mint x) const { return this->mul(x); }
    mint operator/(mint x) const { return this->div(x); }
    mint operator+=(mint x) { return *this = this->add(x); }
    mint operator-=(mint x) { return *this = this->sub(x); }
    mint operator*=(mint x) { return *this = this->mul(x); }
    mint operator/=(mint x) { return *this = this->div(x); }

    friend istream& operator>>(istream& is, mint& x) {
        is >> x.x;
        return is;
    }
    friend ostream& operator<<(ostream& os, const mint& x) {
        os << x.x;
        return os;
    }
};

mint fact(int n) {
    static vector<mint> fact_memo;

    if (n < (int)fact_memo.size()) {
        return fact_memo[n];
    }
    if (fact_memo.size() == 0) {
        fact_memo.push_back(1);
    }
    for (int i = fact_memo.size(); i <= n; i++) {
        fact_memo.push_back(fact_memo[i - 1].mul(i));
    }
    return fact_memo[n];
}

mint comb(int n, int r) { return fact(n) / fact(r) / fact(n - r); }

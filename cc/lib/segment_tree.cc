
template <typename T>
class segment_tree {
   private:
    static int calc_size(int n) {
        int m = 1;
        while (m < n) {
            m *= 2;
        }
        return m;
    }

    T query(int s, int t, int i, int l, int r) const {
        if (t <= l || r <= s) {
            return e;
        }

        if (s <= l && r <= t) {
            return a[i];
        }

        int m = l + (r - l) / 2;
        T vl = query(s, t, i * 2, l, m);
        T vr = query(s, t, i * 2 + 1, m, r);
        return f(vl, vr);
    }

   public:
    int n;
    vector<T> a;
    T e;
    function<T(T, T)> f;

    segment_tree(int n, T e, function<T(T, T)> f)
        : n(calc_size(n)), a(calc_size(n) * 2, e), e(e), f(f) {}

    void update(int i, const T& x) {
        a[i + n] = x;
        for (int j = (i + n) / 2; j > 0; j /= 2) {
            a[j] = f(a[j * 2], a[j * 2 + 1]);
        }
    }

    T query(int s, int t) const { return query(s, t, 1, 0, n); }
};

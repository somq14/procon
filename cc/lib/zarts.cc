
map<int, int> compress(const vector<int>& a) {
    vector<int> b(a);
    sort(b.begin(), b.end());
    b.erase(unique(b.begin(), b.end()), b.end());

    map<int, int> c;
    int m = b.size();
    rep(i, m) { c[b[i]] = i; }

    return c;
}

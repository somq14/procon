
bool is_prime(ll n) {
    if (n <= 1) {
        return false;
    }
    if (n == 2) {
        return true;
    }
    if (n % 2 == 0) {
        return false;
    }
    for (ll i = 3; i * i <= n; i += 2) {
        if (n % i == 0) {
            return false;
        }
    }

    return true;
}

vector<pair<ll, ll>> factorize(ll n) {
    vector<pair<ll, ll>> ans;

    for (ll i = 2; i * i <= n; i++) {
        if (n % i != 0) {
            continue;
        }

        ll cnt = 0;
        while (n % i == 0) {
            n /= i;
            cnt++;
        }
        ans.push_back(make_pair(i, cnt));
    }

    if (n != 1) {
        ans.push_back(make_pair(n, 1));
    }

    return ans;
}

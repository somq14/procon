
vector2<int> build_cumsum2(int h, int w, const vector2<int>& a) {
    vector2<int> c = init_vector2(h + 1, w + 1, 0);
    for (int y = 1; y <= h; y++) {
        for (int x = 1; x <= w; x++) {
            c[y][x] =
                c[y - 1][x] + c[y][x - 1] - c[y - 1][x - 1] + a[y - 1][x - 1];
        }
    }
    return c;
}

int cumsum2(const vector2<int>& c, int sy, int sx, int ty, int tx) {
    return c[ty][tx] - c[ty][sx] - c[sy][tx] + c[sy][sx];
}

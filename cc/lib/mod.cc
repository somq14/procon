const int MOD = 1e9 + 7;

int pow_mod(int x, int n) {
    int ans = 1;
    int xx = x;
    for (int m = n; m > 0; m >>= 1) {
        if (m & 1) {
            ans = ans * xx % MOD;
        }
        xx = xx * xx % MOD;
    }
    return ans;
}

int inv_mod(int n) {
    int ans = pow_mod(n, MOD - 2);
    return ans;
}

int fact_mod(int n) {
    static vector<int> memo = {1};
    if (n < (int)memo.size()) {
        return memo[n];
    }
    for (int i = memo.size(); i <= n; i++) {
        memo.push_back(i * memo[i - 1] % MOD);
    }
    return memo[n];
}


class rect {
   public:
    pos s;
    pos t;

    rect() : s(pos()), t(pos()) {}
    rect(const pos& s, const pos& t) : s(s), t(t) {}

    bool contains(const pos& p) const {
        return s.x <= p.x && p.x <= t.x && s.y <= p.y && p.y <= t.y;
    }
};


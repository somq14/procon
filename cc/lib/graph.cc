
class edge {
   public:
    int u;
    int v;
    int w;
    edge() : u(0), v(0), w(0) {}
    edge(int u, int v, int w) : u(u), v(v), w(w) {}

    bool operator==(const edge& e) const {
        return u == e.u && v == e.v && w == e.w;
    }

    bool operator<(const edge& e) const {
        if (w != e.w) {
            return w < e.w;
        }
        return u == e.u ? v < e.v : u < e.u;
    }
};

pair<vector<int>, vector<int>> dijkstra(int n, const vector2<int>& g, int s) {
    vector<int> p(n, -1);
    p[s] = s;

    vector<int> d(n, INF);
    d[s] = 0;

    typedef pair<double, int> P;
    priority_queue<P, vector<P>, greater<P>> q;
    q.push(make_pair(0, s));

    while (not q.empty()) {
        int c = q.top().first;
        int v = q.top().second;
        q.pop();

        if (c > d[v]) {
            continue;
        }

        rep(u, n) {
            int alt_c = c + g[v][u];
            if (alt_c < d[u]) {
                d[u] = alt_c;
                p[u] = v;
                q.push(make_pair(alt_c, u));
            }
        }
    }

    return make_pair(d, p);
}

